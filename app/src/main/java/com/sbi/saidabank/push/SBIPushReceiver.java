package com.sbi.saidabank.push;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;

import java.util.List;

/**
 * SBIPushReceiver : 푸쉬 리시버
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class SBIPushReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        MLog.d();
        if (DataUtil.isNotNull(context)
                && DataUtil.isNotNull(intent.getAction())
                && intent.getAction().equals(context.getPackageName() + Const.ACTION_PUSH_RECEIVE)) {
            ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> list = activityManager.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo rap : list) {
                // 현재 앱이 실행중인지 확인
                if (rap.processName.equals(context.getPackageName())) {
                    SaidaApplication mApplicationClass = (SaidaApplication) context.getApplicationContext();
                    if (mApplicationClass.getActivitySize() > 0) {
                        final Activity activity = mApplicationClass.getOpenActivity(".Main2Activity");
                        if (DataUtil.isNotNull(activity)) {
                            ((Main2Activity) activity).runOnUiThread(
                                    new Runnable() {
                                        @Override
                                        public void run() {
                                            ((Main2Activity) activity).showPushNotiFragment();
                                        }
                                    }
                            );
                        }
                    }
                    return;
                }
            }
        }
    }
}
