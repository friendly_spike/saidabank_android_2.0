package com.sbi.saidabank.push;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.common.SchemeEntryActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

/**
 * SBIMessageReceiver : 메시지 수신 리시버
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class SBIMessageReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        MLog.d();
        try {
            if (DataUtil.isNotNull(context) && DataUtil.isNotNull(intent.getAction())) {
                MLog.i("intent.getAction() >> " + intent.getAction());
                if (intent.getAction().equals(context.getPackageName() + Const.ACTION_PUSH_MESSAGE)) {
                    if (intent.hasExtra(Const.INTENT_PUSH_REGID) && intent.hasExtra(Const.INTENT_PUSH_DATA)) {
                        String regID = intent.getStringExtra(Const.INTENT_PUSH_REGID);
                        FLKPushMessage flkPushMessage = (FLKPushMessage) intent.getSerializableExtra(Const.INTENT_PUSH_DATA);
                        FLKPushAgentSender.sendToReadMsgPushAgent(context, context.getPackageName(), FLKPushAgentSender.mHostName, regID, flkPushMessage);
                    }
                    // 로그인 상태면 현재 앱 상태 그대로 띄움
                    if (LoginUserInfo.getInstance().isLogin()) {
                        Intent webMainIntent = new Intent(context, SchemeEntryActivity.class);
                        webMainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(webMainIntent);
                    }
                    // 로그인 상태가 아니고 앱이 실행 중이 아니면, 시작페이지로 이동 후 로그인하면 알림함으로 이동
                    else {
                        SaidaApplication mApplicationClass = (SaidaApplication) context.getApplicationContext();
                        if (DataUtil.isNotNull(mApplicationClass)) {
                            MLog.i("Activity Size >> " + mApplicationClass.getActivitySize());
                            if (mApplicationClass.getActivitySize() > 0) {
                                Intent loginIntent = new Intent(context, SchemeEntryActivity.class);
                                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(loginIntent);
                            } else {
                                LoginUserInfo.clearInstance();
                                LoginUserInfo.getInstance().setLogin(false);
                                Intent loginIntent = new Intent(context, IntroActivity.class);
                                loginIntent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0060200.getServiceUrl());
                                loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                context.startActivity(loginIntent);
                            }
                        } else {
                            MLog.i("Application is Null");
                            LoginUserInfo.clearInstance();
                            LoginUserInfo.getInstance().setLogin(false);
                            Intent loginIntent = new Intent(context, IntroActivity.class);
                            loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(loginIntent);
                        }
                    }
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }
}
