package com.sbi.saidabank.push;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

/**
 * FLKPushAgentReceiver : 3rd Party App의 AgentReceiver 클레스
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class FLKPushAgentReceiver extends BroadcastReceiver {

    public static int mBadgeCount = 0;

    @Override
    public void onReceive(final Context context, Intent intent) {
        Logs.e("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        Logs.e(intent.getAction());

        if (DataUtil.isNotNull(intent) && DataUtil.isNotNull(intent.getAction())) {
            try {
                // Agent로 부터 Registration ID를 요청받을 준비가 되었음을 받음.
                if (intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".REQUEST_READY_FOR_AGENT")) {
                    // 일단 무조건 RID를 재발급 받는다.
                    FLKPushAgentSender.sendToRegistration(context);
                }
                // Registration ID를 받음.
                else if (intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_APP_REG_ID")) {
                    Uri revUri = Uri.parse(intent.getDataString());
                    String regID = revUri.getQueryParameter("regid");
                    String oldRegID = Prefer.getPushRegID(context);
                    Prefer.setPushRegID(context, regID);
                    // 기존에 RegID가 있음에도 AID복구에 실패해서 RID를 재발급 받았을 경우 처리.
                    if (DataUtil.isNotNull(oldRegID) && !oldRegID.equals(regID)) {
                        MLog.i("AID복구 실패로 인해 RID 재발급");
                    }
                    MLog.i("regidReceived " + regID);

                    // ID를 새로 받으면 서버에 전송해준다.
                    Intent pushintent = new Intent(context.getPackageName() + Const.ACTION_REFRESH_FCM_TOKEN);
                    context.sendBroadcast(pushintent);
                }
                // Push Message를 받음.
                else if (intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_APP_MSG_INFO")) {
                    FLKPushMessage pushMessage = new FLKPushMessage();
                    pushMessage.mSubTime = intent.getStringExtra("subtime");
                    pushMessage.mReplyType = intent.getIntExtra("replytype", 0);
                    pushMessage.mPushId = intent.getStringExtra("pushid");
                    pushMessage.mLegacyPort = intent.getIntExtra("legacyport", 0);
                    final byte[] revLegacyIP = intent.getByteArrayExtra("legacyip");
                    final byte[] revMessage = intent.getByteArrayExtra("msg");
                    final byte[] revData = intent.getByteArrayExtra("data");
                    pushMessage.mLegacyIp = new String(revLegacyIP, StandardCharsets.UTF_8);
                    pushMessage.mMessage = new String(revMessage, StandardCharsets.UTF_8);
                    pushMessage.mData = new String(revData, StandardCharsets.UTF_8);

                    // Push Message 저장 및 Notification 등록과 같은 3rd App에서 필요한 메시지에 대한 이벤트 처리
                    String notiMsg = DataUtil.isNotNull(pushMessage.mMessage) ? pushMessage.mMessage : Const.EMPTY;
                    String channelId = Const.EMPTY;
                    MLog.i("Msg >> " + notiMsg);
                    MLog.i("Data >> " + pushMessage.mData);
                    JSONObject object = new JSONObject(pushMessage.mData);
                    if (object.has("dataContent"))
                        notiMsg = object.optString("dataContent");
                    if (object.has("channelId"))
                        channelId = object.optString("channelId");
                    if (DataUtil.isNull(channelId))
                        channelId = "SBK";
                    try {
                        Intent pendingIntent = new Intent(context, SBIMessageReceiver.class);
                        pendingIntent.putExtra(Const.INTENT_PUSH_DATA, pushMessage);
                        pendingIntent.putExtra(Const.INTENT_PUSH_REGID, Prefer.getPushRegID(context));
                        pendingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK) ;
                        pendingIntent.setAction(context.getPackageName() + Const.ACTION_PUSH_MESSAGE);
                        sendNotification(context, pendingIntent, context.getString(R.string.app_name) + " 알림", notiMsg, channelId);
                    } catch (ClassCastException e) {
                        MLog.e(e);
                    }
                    mBadgeCount++;
                    updateBadge(context, mBadgeCount);
                    Prefer.setFlagShowFirstNotiMsgBox(context.getApplicationContext(), true);
                }
                // Push Message 읽음확인 Ack를 받음.
                else if (intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_APP_READ_MSG_ACK")) {
                    Uri revUri = Uri.parse(intent.getDataString());
                    String pushID = revUri.getQueryParameter("pushid");
                    // Message 읽음 확인 응답에 대한 3rd App에서 필요한 처리를 한다.
                    MLog.i("receivedPushID >> " + pushID);
                }
                // Agent 연동 실패 에러.
                else if (intent.getAction().equals(FLKPushAgentSender.PushAgentPName + ".RECEIVED_ERROR")) {
                    Uri revUri = Uri.parse(intent.getDataString());
                    String errorCode = revUri.getQueryParameter("errorcode");
                    // 에러 코드 확인
                    MLog.i("receivedRegParamError >> " + errorCode);
                }
            } catch (JSONException e) {
                MLog.e(e);
            }
        }
        Logs.e("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
    }

    /**
     * Notification을 보낸다.
     *
     * @param context
     * @param intent  : Noti 메세지 탭 시, 실행 할 intent
     * @param title   : 타이틀
     * @param msg     : 메세지
     */
    public void sendNotification(Context context, Intent intent, String title, String msg, String channelId) {
        MLog.d();
        try {
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            int mLastId = new Random().nextInt(1000000) + 1;
            // 오레오 이상 버전 noti 구분처리
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
                notificationChannel.setDescription(channelId);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.enableVibration(false);
                //notificationChannel.setVibrationPattern(new long[]{100, 200, 100, 200});
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
                nm.createNotificationChannel(notificationChannel);
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, channelId)
                        .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_launcher))
                        .setSmallIcon(R.drawable.flk_service_icon)
                        .setColor(0xff009beb)
                        .setColorized(true)
                        //.setContentTitle(title)
                        .setContentText(msg)
                        .setContentIntent(pendingIntent)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(msg))
                        .setAutoCancel(true);
                nm.notify(mLastId, mBuilder.build());
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Notification.Builder mBuilder = new Notification.Builder(context);
                mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_launcher));
                mBuilder.setSmallIcon(R.drawable.flk_service_icon);
                mBuilder.setColor(0xff009beb);
                mBuilder.setContentText(msg);
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setWhen(System.currentTimeMillis());
                mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                mBuilder.setStyle(new Notification.BigTextStyle().bigText(msg));
                mBuilder.setAutoCancel(true);
                mBuilder.setPriority(Notification.PRIORITY_MAX);
                nm.notify(mLastId, mBuilder.build());
            } else {
                Notification.Builder mBuilder = new Notification.Builder(context);
                mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ico_launcher));
                mBuilder.setSmallIcon(R.drawable.flk_service_icon);
                mBuilder.setColor(0xff009beb);
                mBuilder.setContentTitle(title);
                mBuilder.setContentText(msg);
                mBuilder.setContentIntent(pendingIntent);
                mBuilder.setWhen(System.currentTimeMillis());
                mBuilder.setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE);
                mBuilder.setStyle(new Notification.BigTextStyle().bigText(msg));
                mBuilder.setAutoCancel(true);
                mBuilder.setPriority(Notification.PRIORITY_MAX);
                nm.notify(mLastId, mBuilder.build());
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    public static void updateBadge(Context context, int count) {
        MLog.d();
        try {
            if (DataUtil.isNull(getLauncherClassName(context)))
                return;
            Intent intent = new Intent("android.intent.action.BADGE_COUNT_UPDATE");
            intent.putExtra("badge_count", count);
            intent.putExtra("badge_count_package_name", context.getPackageName());
            intent.putExtra("badge_count_class_name", getLauncherClassName(context));
            context.sendBroadcast(intent);
            if(count > 0){
                Intent intentReecive = new Intent(context.getPackageName() + Const.ACTION_PUSH_RECEIVE);
                context.sendBroadcast(intentReecive);
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    public static String getLauncherClassName(Context context) {
        MLog.d();
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            List<ResolveInfo> resolveInfos = packageManager.queryIntentActivities(intent, 0);
            for (ResolveInfo resolveInfo : resolveInfos) {
                String pkgName = resolveInfo.activityInfo.applicationInfo.packageName;
                if (pkgName.equalsIgnoreCase(context.getPackageName())) {
                    return resolveInfo.activityInfo.name;
                }
            }
        } catch (Exception e) {
            return null;
        }
        return null;
    }
}
