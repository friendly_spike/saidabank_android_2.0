package com.sbi.saidabank.push;

import java.io.Serializable;

/**
 * FLKPushMessage : 3rd Party App의 MessageData 클레스
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-05-12
 */
public class FLKPushMessage implements Serializable {
    // 메시지의 고유ID
    public String mPushId;

    // 메시지를 받은 시간(yyyyMMddHHmmss)
    public String mSubTime;

    // Push 메시지
    public String mMessage;

    // 메시지 이외의 사용자 데이터
    public String mData;

    // 읽었는지 여부
    public boolean mIsRead;

    // 읽음확인을 위한 서버 아이피
    public String mLegacyIp;

    // 읽음확인을 위한 서버 포트
    public int mLegacyPort;

    // 0 : 수신 ACK, 읽음 확인 응답 모두 필요 없음
    // 1 : 수신 ACK만 필요
    // 2 : 읽음 확인 응답만 필요
    // 3 : 수신 ACK, 읽음 확인 응답 모두 필요
    public int mReplyType;
}
