package com.sbi.saidabank.push;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * FLKPushAgentSender : 3rd Party App의 Sender 클레스
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class FLKPushAgentSender {

    public static final String KEY_APP_ID = "SBK0000001";
    public static final String PHONE_NUMBER_ID = "phone_number_id";
    public static final String mAppName = "SBK0000001";
    public static final String mHostName = "sbi_pushagent_uri";
    public static final String PushAgentPName = "com.feelingk.pushagent.sbi";
    public static final String PushAgentHName = "sbi_push://sbi_pushagent_uri";

    public FLKPushAgentSender() { }

    /**
     * Agent Service를 시작한다.
     *
     * @param context  Context
     * @param isWakeup WakeUp 여부 (PushServer로부터 GCM 메시지 수신시에 true값 세팅하여 사용)
     */
    public static void sendToStart(Context context, boolean isWakeup) {
        MLog.d();
        Intent intent = new Intent(PushAgentPName + ".SERVICE_START");
        intent.setPackage(context.getPackageName());
        intent.setFlags((int) 32);
        intent.putExtra("wakeUp", isWakeup);
        intent.putExtra("userInfo", Const.EMPTY);
        intent.putExtra("useUserInfo", true);
        intent.putExtra("serverInfo", SaidaUrl.ReqUrl.URL_FLKPUSH.getReqUrl());
        intent.putExtra("ra_port", "9000");
        intent.putExtra("rm_port", "8000");
        intent.putExtra("rm_port_ssl", "8001");
        intent.putExtra("ri_port", "9001");
        intent.putExtra("rmr_port", "9600");
        context.sendBroadcast(intent);
    }

    public static void sendToStart(Context context, boolean useUserInfo, String userInfo) {
        MLog.d();
        Intent intent = new Intent(PushAgentPName + ".SERVICE_START");
        intent.setPackage(context.getPackageName());
        intent.setFlags((int) 32);
        intent.putExtra("userInfo", userInfo);
        intent.putExtra("useUserInfo", useUserInfo);
        context.sendBroadcast(intent);
    }

    /**
     * TEST용 API로 실제 운영시 사용금지.
     *
     * @param context Context
     * @Method 설명 : Agent Service를 시작한다.
     */
    public static void sendToStart(Context context, String ip) {
        MLog.d();
        Intent intent = new Intent(PushAgentPName + ".SERVICE_START");
        intent.setPackage(context.getPackageName());
        intent.setFlags((int) 32);
        intent.putExtra("serverInfo", ip);
        intent.putExtra("wakeUp", false);
        context.sendBroadcast(intent);
    }

	/**
	 * sendToRegistration(Context context, String pName, String hName, String appID) 호출 메서드
	 *
	 * @param context Context
	 */
    public static void sendToRegistration(Context context) {
        MLog.d();
        String regID = Prefer.getPushRegID(context);
        if (DataUtil.isNull(regID)) {
            try {
                FLKPushAgentSender.sendToRegistration(context, context.getPackageName(), FLKPushAgentSender.mHostName, mAppName);
            } catch (UnsupportedEncodingException e) {
                MLog.e(e);
            }
        }
    }

    /**
     * Registration ID를 발급받는다.
     *
     * @param context Context
     * @param pName   3rd Party App의 패키지 이름
     * @param hName   메세지를 전달 받을 데이터 url 호스트 이름(manifest의 설정이름과 맞아야함)
     * @param appName 3rd Party App의 이름 (무조건 10자리 고정)
     * @throws UnsupportedEncodingException
     */
    public static void sendToRegistration(Context context, String pName, String hName, String appName) throws UnsupportedEncodingException {
        MLog.d();
        StringBuilder sb = new StringBuilder(PushAgentHName);
        sb.append("?pname=").append(URLEncoder.encode(pName, Const.UTF_8));
        sb.append("&hname=").append(URLEncoder.encode(hName, Const.UTF_8));
        sb.append("&appname=").append(URLEncoder.encode(appName, Const.UTF_8));
        Intent intent = new Intent(PushAgentPName + ".APP_REGISTRATION", Uri.parse(sb.toString()));
        intent.setPackage(context.getPackageName());
        intent.setFlags((int) 32);
        context.sendBroadcast(intent);
    }

    /**
     * 읽음확인 결과 전송을 위한 정보를 Agent로 전달
     *
     * @param context     Context
     * @param pName       3rd Party App의 패키지 이름
     * @param hName       메세지를 전달 받을 데이터 url 호스트 이름(manifest의 설정이름과 맞아야함)
     * @param regID       Agent로 부터 발급받은 Registration ID
     * @param pushMessage RECEIVED_APP_MSG_INFO 때 받은 FLKPushMessage 클레스데이터
     */
    @SuppressLint("SimpleDateFormat")
    public static void sendToReadMsgPushAgent(Context context, String pName, String hName, String regID, FLKPushMessage pushMessage) {
        MLog.d();
        try {
            if (pushMessage.mIsRead)
                return;
            if (pushMessage.mReplyType == 2 || pushMessage.mReplyType == 3) {
                StringBuilder sb = new StringBuilder(PushAgentHName);
                sb.append("?pname=").append(URLEncoder.encode(pName, Const.UTF_8));
                sb.append("&hname=").append(URLEncoder.encode(hName, Const.UTF_8));
                sb.append("&pushid=").append(URLEncoder.encode(pushMessage.mPushId, Const.UTF_8));
                sb.append("&deviceid=").append(URLEncoder.encode(regID, Const.UTF_8));
                sb.append("&readtime=").append(URLEncoder.encode(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()), Const.UTF_8));
                sb.append("&legacyip=").append(URLEncoder.encode(pushMessage.mLegacyIp, Const.UTF_8));
                sb.append("&legacyport=").append(URLEncoder.encode(String.valueOf(pushMessage.mLegacyPort), Const.UTF_8));
                Intent intent = new Intent(PushAgentPName + ".RESPONSE_READ_MSG", Uri.parse(sb.toString()));
                intent.setPackage(context.getPackageName());
                intent.setFlags((int) 32);
                context.sendBroadcast(intent);
            }
        } catch (UnsupportedEncodingException e) {
            MLog.e(e);
        }
    }

//	public static void sendToAgentPause(Context context, boolean flag) throws UnsupportedEncodingException {
//		MLog.d();
//		if (flag) {
//			Intent intent = new Intent(PushAgentPName + ".AGENT_PAUSE");
//			intent.setPackage(context.getPackageName());
//			intent.setFlags((int) 32);
//			context.sendBroadcast(intent);
//		} else {
//			Intent intent = new Intent(PushAgentPName + ".AGENT_RESUME");
//			intent.setPackage(context.getPackageName());
//			intent.setFlags((int) 32);
//			context.sendBroadcast(intent);
//		}
//	}
}
