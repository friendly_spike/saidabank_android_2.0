package com.sbi.saidabank.push;

import android.content.Intent;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;

/**
 * SBIFcmListenerService : Fcm Listener Service
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class SBIFcmListenerService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String refreshedToken) {
        super.onNewToken(refreshedToken);
        if (DataUtil.isNotNull(refreshedToken)) {
            String saveToken = Prefer.getPushGCMToken(getApplicationContext());
            MLog.line();
            MLog.i("Refresh Token >> " + refreshedToken);
            MLog.i("Save Token >> " + saveToken);
            MLog.line();
            if (!refreshedToken.equals(saveToken)) {
                Prefer.setPushGCMToken(getApplicationContext(), refreshedToken);
                String flkPushId = Prefer.getPushRegID(getApplicationContext());
                if (DataUtil.isNotNull(flkPushId)) {
                    Intent intent = new Intent(getApplicationContext().getPackageName() + Const.ACTION_REFRESH_FCM_TOKEN);
                    getApplicationContext().sendBroadcast(intent);
                }
            }
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        super.onMessageReceived(message);
        if (DataUtil.isNotNull(message) && !message.getData().isEmpty()) {
            MLog.line();
            MLog.i("From >> " + message.getFrom());
            MLog.i("Data >> " + message.getData());
            MLog.line();
            FLKPushAgentSender.sendToStart(this, true);
        }
    }
}
