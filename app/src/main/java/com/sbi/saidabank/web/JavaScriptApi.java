package com.sbi.saidabank.web;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * JavaScriptApi
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class JavaScriptApi {

	/** Web Page와의 인터페이스 함수 선언 */
	public static final int API_UNKNOWN = -1;

	/** 다이얼로그 관련 100번대 */
	public static final int API_100 = 100;      // Alert Dialog
	public static final int API_101 = 101;      // Comfirm Dialog 호출
	public static final int API_102 = 102;      // Progress Dialog 호출
	public static final int API_103 = 103;      // Logout Dialog 호출
	public static final int API_104 = 104;      // 세션 끊겨 로그아웃
	public static final int API_105 = 105;      // Intro부터 시작
	public static final int API_106 = 106;      // 은행선택/계좌번호 입력 Dialog호출
	public static final int API_107 = 107;      // 계좌번호 입력 Dialog 단독 호출
	public static final int API_108 = 108;      // 계좌선택 Dialog.


	/** 보안키패드 관련 200번대 */
	public static final int API_200 = 200;      // 1줄 보안키패드
	public static final int API_201 = 201;      // dot형 보안키패드 호출 등록
	public static final int API_202 = 202;      // dot형 보안키패드 호출 인증
	public static final int API_203 = 203;      // 타기관 OTP 비밀번호 등록
	public static final int API_204 = 204;      // 타기관 OTP 비밀번호 인증 - 비밀번호만 리턴
	public static final int API_205 = 205;      // 모바일 OTP 비밀번호 등록
	public static final int API_206 = 206;      // 모바일 OTP 비밀번호 인증 - 비밀번호만 리턴
	public static final int API_207 = 207;      // 타기관 OTP 비밀번호 인증 - 인증완료후 결과 리턴
	public static final int API_208 = 208;      // 모바일 OTP 비밀번호 인증 - 인증완료후 결과 리턴

	/** datapicker 호출 */
	public static final int API_250 = 250;      // DataPicker 호출

	public static final int API_300 = 300;      // 고객정보 재확인 일주일간 열지 않음
	public static final int API_301 = 301;      // Web->Native 화면 호출
	public static final int API_302 = 302;      // 새창으로 Web 호출
	public static final int API_304 = 304;      // 푸시 앱 아이디
	public static final int API_305 = 305;      // 푸시 설정 상태
	public static final int API_306 = 306;      // 푸시 설정으로 가기

	//웹에서 Preferance에 값 저장하기 위해 사용
	public static final int API_350 = 350;      // 저장
	public static final int API_351 = 351;      // 조회


	/** 챗봇 */
	public static final int API_400 = 400;      // 챗봇 열기
	public static final int API_401 = 401;      // 챗봇에서 신규화면 열기
	public static final int API_402 = 402;      // 챗봇에서 고객번호 가져가기
	public static final int API_403 = 403;      // 챗봇 닫기

	/** 통합이체 */
	public static final int API_450 = 450;      // 통합이체
	public static final int API_451 = 451;      // 통합이체 내보내기
	public static final int API_452 = 452;      // 통합이체 가져오기
	public static final int API_453 = 453;      // 계좌별칭변경할때


	/** FIDO, 핀코드 */
	public static final int API_500 = 500;      // pin 전자서명
	public static final int API_501 = 501;      // 지문 전자서명
	public static final int API_502 = 502;      // 휴대폰 본인인증
	public static final int API_503 = 503;      // 타행계좌 인증

	/** 스크래핑 */
	public static final int API_600 = 600;      // 스크래핑 건강보험 납부내역,건강보험 자격득실
	public static final int API_601 = 601;      // 스크래핑 건강보험 납부내역,국민연금 정산용 가입내역확인서,국세청 부가세표준증명

	/** MOTP 전문 */
	public static final int API_700 = 700;      // MOTP TA 버전요청
	public static final int API_701 = 701;      // MOTP 발급정보저장
	public static final int API_702 = 702;      // MOTP 일련번호요청
	public static final int API_703 = 703;      // MOTP 번호요청
	public static final int API_704 = 704;      // MOTP 폐기요청

	/** 비대면 */
	public static final int API_800 = 800;      // 비대면 사진촬영 실행

	/** FDS */
	public static final int API_801 = 801;      //

	/** 기기정보 IMEI, UUID */
	public static final int API_802 = 802;      //

	/** 기타작업 관련 */
	public static final int API_900 = 900;      // 화면캡쳐
	public static final int API_901 = 901;      // 암호화
	public static final int API_902 = 902;      // 복호화
	public static final int API_903 = 903;      // PDF뷰어
	public static final int API_904 = 904;      // 전화걸기
	public static final int API_905 = 905;      // 앱버전 정보
	public static final int API_906 = 906;      // 세션 동기화
	public static final int API_907 = 907;      // 프로필 사진 가져오기
	public static final int API_908 = 908;      // 외부 브라우저로 url 열기
	public static final int API_910 = 910;      // SNS 공유하기
	public static final int API_911 = 911;      // 클립보드 복사
	public static final int API_912 = 912;      // 카카오톡 보내기
	public static final int API_913 = 913;      // AppsFlyer
	public static final int API_914 = 914;      // 메인 이벤트 팝업 닫고 페이지웹 이동
	public static final int API_915 = 915;      // 주소록 검색 결과값 리턴
	public static final int API_920 = 920;      // Tab Bar 이동
	public static final int API_930 = 930;      // 휴대폰 주소록 호출
	public static final int API_931 = 931;      // 커플 멤버선택 휴대폰 주소록 호출
	public static final int API_932 = 932;      // SMS보내기
	/**
	 * Web과 Interface하면 URL encoding할 경우 캐릭터 타입으로 특수문자가 들어가는 경우가 발생하여 제거해주는 함수를 추가함
	 *
	 * @param jsonEncodeString
	 * @return String
	 */
	public static String makeNewJsonString(String jsonEncodeString) {
		if (DataUtil.isNotNull(jsonEncodeString)) {
			try {
				String jsonString = URLDecoder.decode(jsonEncodeString, Const.UTF_8);
				int bodyPos = jsonString.indexOf("body");
				if (bodyPos > 0) {
					String headerStr = jsonString.substring(0, bodyPos);
					String bodyStr = jsonString.substring(bodyPos, jsonString.length());
					bodyStr = bodyStr.replace("\"\"{", "{");
					bodyStr = bodyStr.replace("}\"\"}", "}}");
					return headerStr + bodyStr;
				} else {
					return jsonString;
				}

			} catch (UnsupportedEncodingException e) {
				MLog.e(e);
				return jsonEncodeString;
			}
		}
		return Const.EMPTY;
	}
}
