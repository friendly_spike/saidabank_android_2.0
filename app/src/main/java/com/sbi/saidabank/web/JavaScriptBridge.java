package com.sbi.saidabank.web;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;

import androidx.core.content.FileProvider;
import androidx.fragment.app.FragmentActivity;

import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ButtonObject;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.message.template.TextTemplate;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.WebTermsActivity;
import com.sbi.saidabank.activity.certification.CertAuthPWActivity;
import com.sbi.saidabank.activity.certification.CertListActivity;
import com.sbi.saidabank.activity.common.AnotherBankAccountVerifyActivity;
import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.activity.common.CoupleContactsPickActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtherOtpRegActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwRegActivity;
import com.sbi.saidabank.activity.login.CertifyPhonePersonalInfoActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.login.PersonalInfoWithFullIdNumberActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.activity.mtranskey.TransKey2LineActivity;
import com.sbi.saidabank.activity.setting.ManageAuthWaysActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.ITransferSalaryActivity;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.activity.transaction.TransferPhoneActivity;
import com.sbi.saidabank.activity.transaction.TransferSafeDealSelectActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.SlidingAccountInputDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectSenderAccountDialog;
import com.sbi.saidabank.common.dialog.TransferNeedAccountDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.ParseUtil;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;
import com.sbi.saidabank.push.FLKPushAgentSender;
import com.sbi.saidabank.solution.appsflyer.AppsFlyerManager;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.jex.JexAESEncrypt;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.softsecurity.transkey.TransKeyActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * JavaScriptBridge : 웹뷰 브릿지
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class JavaScriptBridge {

    public static final String CALL_NAME = "Android";
    private static final long MIN_PROGRESS_DISMISS_INTERVAL = 1000;
    private Activity mContext;
    private Handler mUiHandler;
    private HashMap<Integer, String> mCallbackFuncName;
    private HashMap<Integer, String> mJsonObjectParam;
    private FrameLayout mWebViewContainer;
    private OnWebActionListener mOnWebActionListener;


    public JavaScriptBridge(Activity activity, FrameLayout webViewContainer,OnWebActionListener listener) {
        mContext = (Activity) activity;
        mWebViewContainer = webViewContainer;
        mUiHandler = new Handler();
        mCallbackFuncName = new HashMap<>();
        mJsonObjectParam = new HashMap<>();
        mOnWebActionListener = listener;
    }

/*
    public JavaScriptBridge(Activity activity, FrameLayout webViewContainer, int webViewFragmentType) {
        mContext = (Activity) activity;
        mWebViewContainer = webViewContainer;
        mUiHandler = new Handler();
        mCallbackFuncName = new HashMap<>();
        mJsonObjectParam = new HashMap<>();
        mWebViewFragmentType = webViewFragmentType;
    }
*/

    public void destroyJavaScriptBrigdge() {
        //activityReference = null;
    }

    public OnWebActionListener getWebActionListener(){
        return mOnWebActionListener;
    }

    /**
     * 전송받은 데이타의 전체 내용을 보여준다. debug 용이다.
     *
     * @param jsonObjStr 전송 데이타의 JSON Object
     */
    private void printInterfaceData(String jsonObjStr) {
        try {
            MLog.i(" ############### JavascriptInterface Start ###############");
            ParseUtil.parseJSONObject(new JSONObject(jsonObjStr));
            MLog.i(" ############### JavascriptInterface End  ###############");
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 네이티브 기능을 수행하기 위한 자바스크립트 인터페이스 메소드
     *
     * @param jsonEncodeString json 문자열 {"header":{ "api":"100", "call_back":"" }, "body":{ //각 호출 함수에 맞게 파라미터값 }
     */
    @JavascriptInterface
    public void callApi(String jsonEncodeString) {
        String jsonString = JavaScriptApi.makeNewJsonString(jsonEncodeString);
        printInterfaceData(jsonString);
        try {
            final JSONObject json = new JSONObject(jsonString);
            mUiHandler.post(new Runnable() {
                public void run() {
                    try {
                        switch (getApiNum(json)) {
                            //======================================================================
                            // 다이얼로그
                            //======================================================================
                            case JavaScriptApi.API_100:         // Alert Dialog
                                api_100_AlertDialog(json);
                                break;
                            case JavaScriptApi.API_101:         // Confirm Dialog
                                api_101_ConfirmDialog(json);
                                break;
                            case JavaScriptApi.API_102:         // Progress Dialog
                                api_102_ProgressDialog(json);
                                break;
                            case JavaScriptApi.API_103:         // Logout Dialog
                                api_103_LogoutDialog();
                                break;
                            case JavaScriptApi.API_104:         // 세션 끊겨 로그아웃
                                api_104_Logout();
                                break;
                            case JavaScriptApi.API_105:         // Intro부터 시작
                                api_105_startIntro();
                                break;
                            case JavaScriptApi.API_106:         // 은행선택 계좌번호 입력 Dialog
                                api_106_BankStockAccountDialog(json);
                                break;
                            case JavaScriptApi.API_107:         // 계좌번호 입력 Dialog 단독 호출
                                api_107_InputAccountDialog(json);
                                break;
                            case JavaScriptApi.API_108:         // 계좌선택 Dialog 단독 호출
                                api_108_SelectAccountDialog(json);
                                break;
                            //======================================================================
                            // 보안키패드
                            //======================================================================
                            case JavaScriptApi.API_200:         // 1줄 보안키패드 호출
                                api_200_1LineShowTransKey(json);
                                break;
                            case JavaScriptApi.API_201:         // dot타입 보안키패드 호출 등록
                                api_201_showDotTypeTransKey(json);
                                break;

                            case JavaScriptApi.API_202:         // dot타입 보안키패드 호출 인증
                                api_202_showDotTypeTransKey(json);
                                break;
                            case JavaScriptApi.API_203:         // 타기관OTP 비밀번호 등록
                                api_203_regOtherOTP(json);
                                break;
                            case JavaScriptApi.API_204:         // 타기관OTP 비밀번호 인증 - 비밀번호만 리턴
                                api_204_authOtherOTP(json);
                                break;
                            case JavaScriptApi.API_205:         // 모바일OTP 비밀번호 등록
                                api_205_regMOTP(json);
                                break;
                            case JavaScriptApi.API_206:         // 모바일OTP 비밀번호 인증 - 비밀번호만 리턴
                                api_206_authMOTP(json);
                                break;
                            case JavaScriptApi.API_207:         // 모바일OTP 비밀번호 인증 - 인증완료후 결과 리턴
                                api_207_authOtherOTP(json);
                                break;
                            case JavaScriptApi.API_208:         // 모바일OTP 비밀번호 인증 - 인증완료후 결과 리턴
                                api_208_authMOTP(json);
                                break;
                            //======================================================================
                            // DataPicker
                            //======================================================================
                            case JavaScriptApi.API_250:         // DataPicker 호출
                                api_250_showDataPicker(json);
                                break;
                            //======================================================================
                            // cdd/edd 고객정보 재확인
                            //======================================================================
                            case JavaScriptApi.API_300:         // Cdd,Edd 고객정보 재확인 일주일간 열지 않음
                                api_300_notOpenFor1Week();
                                break;
                            //======================================================================
                            // Web->Native 화면 호출
                            //======================================================================
                            case JavaScriptApi.API_301:         // Web->Native 화면 호출
                                api_301_callNativeScreen(json);
                                break;
                            //======================================================================
                            // 새창으로 Web 호출
                            //======================================================================
                            case JavaScriptApi.API_302:         // 새창으로 Web 호출
                                api_302_callWebView(json);
                                break;
                            //======================================================================
                            // 푸시
                            //======================================================================
                            case JavaScriptApi.API_304:         // 푸시 앱 아이디
                                api_304_requestPushAppID();
                                break;
                            case JavaScriptApi.API_305:         // 푸시 설정 상태
                                api_305_requestPushNotiState();
                                break;
                            case JavaScriptApi.API_306:         // 푸시 설정으로 가기
                                api_306_openPushNotiSettingMenu();
                                break;
                            //======================================================================
                            // Preferance에 값저장및 조회
                            //======================================================================
                            case JavaScriptApi.API_350:         // 푸시 설정으로 가기
                                api_350_savePreferance(json);
                                break;
                            case JavaScriptApi.API_351:         // 푸시 설정으로 가기
                                api_351_getPreferance(json);
                                break;

                            //======================================================================
                            // 챗봇
                            //======================================================================
                            case JavaScriptApi.API_400:         // 챗봇 열기
                                api_400_openChatBot(json);
                                break;
                            case JavaScriptApi.API_401:         // 챗봇에서 신규 화면 열기
                                api_401_openChatBotNewWebView(json);
                                break;
                            case JavaScriptApi.API_402:         // 챗봇에서 고객번호 가져가기
                                api_402_requestChatBotCustomNumber();
                                break;
                            case JavaScriptApi.API_403:         // 챗봇 닫기
                                api_403_closeChatBot();
                                break;
                            //======================================================================
                            // 통합이체
                            //======================================================================
                            case JavaScriptApi.API_450:         // 통합이체
                                api_450_transferMain(json);
                                break;
                            case JavaScriptApi.API_451:         // 통합이체 내보내기
                                api_451_transferExport(json);
                                break;
                            case JavaScriptApi.API_452:         // 통합이체 가져오기
                                api_452_transferImport(json);
                                break;
                            case JavaScriptApi.API_453:         // 통장 별칭 변경
                                api_453_changeAccountAlas(json);
                                break;
                            //======================================================================
                            // 지문,핀코드 인증
                            //======================================================================
                            case JavaScriptApi.API_500:
                                api_500_pinSign(json);
                                break;

                            case JavaScriptApi.API_501:
                                api_501_fingerprintSign(json);
                                break;

                            case JavaScriptApi.API_502:
                                api_502_selfCertify(json);
                                break;

                            case JavaScriptApi.API_503:
                                api_503_certifyAnotherBank(json);
                                break;
                            //======================================================================
                            // 스크래핑
                            //======================================================================
                            case JavaScriptApi.API_600:
                                api_600_scraping(json);
                                break;

                            case JavaScriptApi.API_601:
                                api_601_scraping(json);
                                break;
                            //======================================================================
                            // M-OTP
                            //======================================================================
                            case JavaScriptApi.API_700:
                                api_700_reqMOTPTAVersion();
                                break;
                            case JavaScriptApi.API_701:
                                api_701_saveMOTPIssueInfo(json);
                                break;
                            case JavaScriptApi.API_702:
                                api_702_reqMOTPSerialNum();
                                break;
                            case JavaScriptApi.API_703:
                                api_703_reqMOTPCode(json);
                                break;
                            case JavaScriptApi.API_704:
                                api_704_reqMOTPDissue(json);
                                break;
                            //======================================================================
                            // 비대면/FDS
                            //======================================================================
                            case JavaScriptApi.API_800:
                                api_800_runOcrCamera(json);
                                break;
                            case JavaScriptApi.API_801:
                                api_801_searchFDSInfo(json);
                                break;
                            case JavaScriptApi.API_802:
                                api_802_DeviceInfo(json);
                                break;
                            //======================================================================
                            // 기타여러작업
                            //======================================================================
                            case JavaScriptApi.API_900:
                                api_900_screenCaptureSave(json);
                                break;
                            case JavaScriptApi.API_901:
                                api_901_jexEnc(json);
                                break;
                            case JavaScriptApi.API_902:
                                api_902_jexDec(json);
                                break;
                            case JavaScriptApi.API_903:
                                api_903_pdfVIew(json);
                                break;
                            case JavaScriptApi.API_904:
                                api_904_makeCall(json);
                                break;
                            case JavaScriptApi.API_905:
                                api_905_appVersionInfo();
                                break;
                            case JavaScriptApi.API_906:
                                api_906_sessionSync();
                                break;
                            case JavaScriptApi.API_907:
                                api_907_getProfileImage(json);
                                break;
                            case JavaScriptApi.API_908:
                                api_908_openUrl(json);
                                break;
                            case JavaScriptApi.API_910:
                                api_910_shareSNS(json);
                                break;
                            case JavaScriptApi.API_911:
                                api_911_copyClipborad(json);
                                break;
                            case JavaScriptApi.API_912:
                                api_912_sendKakaoTalk(json);
                                break;
                            case JavaScriptApi.API_913:
                                api_913_sendAppsflyerEvent(json);
                                break;
                            case JavaScriptApi.API_915:
                                api_915_returnSearchAddress(json);
                                break;
                            case JavaScriptApi.API_920:
                                api_920_selectTab(json);
                                break;
                            case JavaScriptApi.API_930:
                                api_930_requestContacts();
                                break;
                            case JavaScriptApi.API_931:
                                api_931_requestCoupleContacts();
                                break;
                            case JavaScriptApi.API_932:
                                api_932_sendSMS(json);
                            default:
                                break;
                        }
                    } catch (JSONException e) {
                        MLog.e(e);
                    }

                }
            });
        } catch (JSONException e) {
            MLog.e(e);
        }
    }

    /**
     * 자바스크립트 함수를 호출한다.
     *`
     * @param funcName 자바스크립트 함수명
     * @param jsonObj  함수 파라미터 목록
     */
    public void callJavascriptFunc(String funcName, JSONObject jsonObj) {
        if (DataUtil.isNull(funcName))
            return;
        String func = Const.EMPTY;
        if (DataUtil.isNull(jsonObj)) {
            func = String.format("%s()", funcName);
        } else {
            func = String.format("%s(%s)", funcName, jsonObj);
        }
        callJavascript(func);
    }

    /**
     * 자바스크립를 실행한다.
     *
     * @param javascript 자바스크립트 코드
     */
    public void callJavascript(String javascript) {
        MLog.line();
        MLog.i("script >> " + javascript);
        MLog.line();
        BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
        webView.loadUrl("javascript:" + javascript);
        //웹 인터페이스 더블클릭 방지에서 더블클릭을 하지 아니하였지만 더블클릭으로 인식하여 다음 화면 안뜨는 문제.
        Utils.clearPreviousBtnClickTime();
    }

    /**
     * JSON 요청 문자열에서 API 구분값을 얻어온다.
     *
     * @param json JSON 요청 문자열
     * @return API 구분
     * }
     */
    private int getApiNum(JSONObject json) {
        int api = JavaScriptApi.API_UNKNOWN;
        try {
            JSONObject header = json.getJSONObject("header");
            String apiStr = header.getString("api");
            if (DataUtil.isNull(apiStr)) {
                Utils.showToast(mContext, "안드로이드 호출 함수 번호가 없습니다.");
                return -1;
            } else {
                api = Integer.parseInt(apiStr);
                String callbackFuncName = header.optString("callback", Const.EMPTY);
                setCallbackFunc(api, callbackFuncName);
            }
        } catch (NumberFormatException | JSONException e) {
            MLog.e(e);
        }
        return api;
    }

    /**
     * 콜백함수를 스택으로 구성하여 여러번 호출되어도 됨.
     *
     * @param apiNum
     * @param callBackName
     */
    private void setCallbackFunc(int apiNum, String callBackName) {
        mCallbackFuncName.put(apiNum, callBackName);
    }

    public String getCallBackFunc(int apiNum) {
        String callBackName = mCallbackFuncName.get(apiNum);
        mCallbackFuncName.remove(apiNum);
        return callBackName;
    }

    public String getCallBackFunc(int apiNum, boolean clear) {
        String callBackName = mCallbackFuncName.get(apiNum);
        if (clear)
            mCallbackFuncName.remove(apiNum);
        return callBackName;
    }

    /**
     * Json 파라미더를 를 스택으로 구성하여 여러번 호출되어도 값을 저장하여 꺼내올수 있도록 구성.
     *
     * @param apiNum
     * @param param
     */
    public void setJsonObjecParam(int apiNum, String param) {
        mJsonObjectParam.put(apiNum, param);
    }

    public String getJsonObjecParam(int apiNum) {
        String param = mJsonObjectParam.get(apiNum);
        mJsonObjectParam.remove(apiNum);
        return param;
    }

    // 로딩 프로그레스바 깜박임 방지
    private Handler progressDismissDelayHandler = new Handler();
    private Runnable progressDismissRunnable = new Runnable() {
        @Override
        public void run() {
            if (DataUtil.isNull(mContext))
                return;
            MLog.d();
            Logs.e("progressDismissRunnable");
            mOnWebActionListener.dismissProgress();
        }
    };

    /**
     * Alert 다이얼로그 호출 요청을 처리한다.
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_100_AlertDialog(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String title = body.optString("title");
        if (TextUtils.isEmpty(title))
            title = mContext.getString(R.string.common_notice);
        String msg = body.getString("msg");
        String btnText = body.optString("ok_btn");
        if (TextUtils.isEmpty(btnText))
            btnText = mContext.getString(R.string.common_confirm);
        if (TextUtils.isEmpty(msg)) return;
        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_100), null);
            }
        };

        //알럿 나타난후도 프로그레스바 보여서 추가함.
        mOnWebActionListener.dismissProgress();
        DialogUtil.alert(mContext, title, msg, btnText, okClick);
    }

    /**
     * Conform 다이얼로그 호출 요청을 처리한다.
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_101_ConfirmDialog(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String title = body.optString("title");
        if (TextUtils.isEmpty(title))
            title = mContext.getString(R.string.common_notice);
        String msg = body.getString("msg");
        String cancel = body.optString("cancel_btn");
        if (TextUtils.isEmpty(cancel))
            cancel = mContext.getString(R.string.common_no);
        String ok = body.optString("ok_btn");
        if (TextUtils.isEmpty(cancel))
            ok = mContext.getString(R.string.common_yes);
        if (TextUtils.isEmpty(msg)) return;
        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("click_btn", "ok");
                } catch (JSONException e) {
                    MLog.e(e);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_101), jsonObj);
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("click_btn", "cancel");
                } catch (JSONException e) {
                    MLog.e(e);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_101), jsonObj);
            }
        };
        //컨펌 나타난후도 프로그레스바 보여서 추가함.
        mOnWebActionListener.dismissProgress();
        DialogUtil.alert(mContext, title, ok, cancel, msg, okClick, cancelClick);
    }

    /**
     * Progress 다이얼로그 호출 요청을 처리한다.
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_102_ProgressDialog(JSONObject json) throws JSONException {
         MLog.d();
        JSONObject body = json.getJSONObject("body");
        boolean show = body.getBoolean("show");
        if (DataUtil.isNull(mContext))
            return;
        if (show) {
            progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
            mOnWebActionListener.showProgress();
        } else {
            progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
        }
    }

    /**
     * Logout 다이얼로그 호출 요청을 처리한다.
     */
    private void api_103_LogoutDialog() {
        MLog.d();
        if ((Activity) mContext instanceof Main2Activity || (Activity) mContext instanceof WebMainActivity) {
            ((BaseActivity) mContext).showLogoutDialog();
        }
    }

    /**
     * 세션 끊겨 로그아웃
     */
    private void api_104_Logout() {
        MLog.d();
        if (DataUtil.isNull(mContext))
            return;
        LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
        if (Utils.isAppOnForeground(mContext)) {
            SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
            mApplicationClass.allActivityFinish(true);
            LoginUserInfo.clearInstance();
            LoginUserInfo.getInstance().setLogin(false);
            Intent intent = new Intent(mContext, LogoutActivity.class);
            intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_FORCE);
            mContext.startActivity(intent);
        } else {
            LoginUserInfo.getInstance().setLogin(false);
            LogoutTimeChecker.getInstance(mContext).setBackground(true);
        }
    }

    /**
     * Intro부터 시작 : 회원 탈퇴 시 호출
     */
    private void api_105_startIntro() {
        MLog.d();
        String motpSerial = LoginUserInfo.getInstance().getMOTPSerialNumber();
        if (DataUtil.isNotNull(motpSerial)) {
            MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
            motpManager.reqDissue(new MOTPManager.MOTPEventListener() {
                @Override
                public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                    MLog.i("resultCode >> " + resultCode);
                }
            }, motpSerial);
        }
        SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
        mApplicationClass.allActivityFinish(true);
        LoginUserInfo.clearInstance();
        LoginUserInfo.getInstance().setLogin(false);
        Prefer.clearAuthSharedPreferences(mContext);
        Intent intent = new Intent(mContext, IntroActivity.class);
        mContext.startActivity(intent);
    }

    /**
     * 은행/증권사및 계좌번호를 받기위한 다이얼로그
     */
    private void api_106_BankStockAccountDialog(final JSONObject json) throws JSONException {
        MLog.d();
        //107에서 사용할수 있어서 저장해둔다.
        Prefer.setOpenBankList(mContext,json.toString());

        JSONObject body = json.getJSONObject("body");
        String bankListStr = body.optString("bank_list");
        String stockListStr = body.optString("stock_list");
        String cardListStr = body.optString("card_list");

        ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
        ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);
        ArrayList<RequestCodeInfo> cardList = SaidaCodeUtil.getCodeArrayList(cardListStr);

        final SlidingAccountInputDialog.OnFinishAccountInputListener accountInputListener = new SlidingAccountInputDialog.OnFinishAccountInputListener() {
            @Override
            public void onClickBack() {
                try {
                    String openBankStr = Prefer.getOpenBankList(mContext);
                    api_106_BankStockAccountDialog(new JSONObject(openBankStr));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onEditFinishListener(String bankCode, String bankName, String accountNum) {
                //String msg = "bankCdoe : " + bankCode + " , bankName : " + bankName + " , accountNum " + accountNum;
                //Logs.showToast(mContext,msg);
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("bank_code", bankCode);
                    jsonObj.put("bank_name", bankName);
                    jsonObj.put("account_num", accountNum);
                } catch (JSONException e) {
                    MLog.e(e);
                }

                String callBackFunc = getCallBackFunc(JavaScriptApi.API_106);
                if(!TextUtils.isEmpty(callBackFunc)){
                    callJavascriptFunc(callBackFunc, jsonObj);
                }else{
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_107), jsonObj);
                }
            }
        };

        final SlidingExpandBankStockDialog.OnSelectBankStockListener bankStockListener = new SlidingExpandBankStockDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bank, String code) {
                SlidingAccountInputDialog accountInputDialog = new SlidingAccountInputDialog(mContext, code, bank,true,"", accountInputListener);
                accountInputDialog.show();
            };
        };


        SlidingExpandBankStockDialog bankStockDialog = new SlidingExpandBankStockDialog(mContext, bankList, stockList, cardList, bankStockListener);
        bankStockDialog.show();

    }

    /**
     * 은행/증권사및 계좌번호를 받기위한 다이얼로그
     */
    private void api_107_InputAccountDialog(final JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String bank_name = body.optString("bank_name");
        String bank_code = body.optString("bank_code");
        String account_num = body.optString("account_num");


        final SlidingAccountInputDialog.OnFinishAccountInputListener accountInputListener = new SlidingAccountInputDialog.OnFinishAccountInputListener() {
            @Override
            public void onClickBack() {
                try {
                    String openBankStr = Prefer.getOpenBankList(mContext);
                    api_106_BankStockAccountDialog(new JSONObject(openBankStr));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onEditFinishListener(String bankCode, String bankName, String accountNum) {
                String msg = "bankCdoe : " + bankCode + " , bankName : " + bankName + " , accountNum " + accountNum;
                Logs.showToast(mContext,msg);
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("bank_code", bankCode);
                    jsonObj.put("bank_name", bankName);
                    jsonObj.put("account_num", accountNum);
                } catch (JSONException e) {
                    MLog.e(e);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_107), jsonObj);

            }
        };

        SlidingAccountInputDialog accountInputDialog = new SlidingAccountInputDialog(mContext, bank_code, bank_name,true,account_num,accountInputListener);
        accountInputDialog.show();

    }

    /**
     * 계좌선택 다이얼로그를  출력.
     */
    private void api_108_SelectAccountDialog(final JSONObject json) throws JSONException {
        SlidingExpandSelectSenderAccountDialog dialog = new SlidingExpandSelectSenderAccountDialog(mContext, false,new SlidingExpandSelectSenderAccountDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bankCd, String accountNo, Object objInfo) {
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("bank_code", bankCd);
                    jsonObj.put("account_num", accountNo);

                    if(bankCd.equalsIgnoreCase("028")){
                        MyAccountInfo info = (MyAccountInfo)objInfo;
                        jsonObj.put("inst_dvcd","");
                        jsonObj.put("dtls_fnlt_cd", "");
                        jsonObj.put("oba_acco_kncd", "");
                        jsonObj.put("blnc_amt", info.getACCO_BLNC());
                        jsonObj.put("wtch_posb_amt", info.getWTCH_POSB_AMT());
                        jsonObj.put("prod_nm", info.getPROD_NM());
                        jsonObj.put("acco_als", info.getACCO_ALS());


                    }else{
                        OpenBankAccountInfo info = (OpenBankAccountInfo)objInfo;
                        jsonObj.put("inst_dvcd", info.INST_DVCD);
                        jsonObj.put("dtls_fnlt_cd", info.DTLS_FNLT_CD);
                        jsonObj.put("oba_acco_kncd", info.OBA_ACCO_KNCD);
                        jsonObj.put("blnc_amt", info.BLNC_AMT);
                        jsonObj.put("wtch_posb_amt", info.WTCH_POSB_AMT);
                        jsonObj.put("prod_nm", info.PROD_NM);
                        jsonObj.put("acco_als", info.ACCO_ALS);
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_108), jsonObj);
            }
        });
        dialog.show();
    }

    /**
     * 1줄 Transkeypad호출
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_200_1LineShowTransKey(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String type = body.getString("type");
        String title = body.getString("title");
        String label = body.getString("label");
        String input_id = body.getString("input_id");
        int min = body.getInt("min_length");
        int max = body.getInt("max_length");
        int keyType = (type.equals("number")) ? TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER : TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;
        Intent intentKey = new Intent(mContext, CertAuthPWActivity.class);
        intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
        intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, title);
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, label);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, min);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, max);
        intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID1, input_id);
        mContext.startActivityForResult(intentKey, JavaScriptApi.API_200);
        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 2줄 Transkeypad호출
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_201_2LineShowTransKey(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String type = body.getString("type");
        String title = body.getString("title");
        String label1 = body.getString("label1");
        String label2 = body.getString("label2");
        String input_id1 = body.getString("input_id1");
        String input_id2 = body.getString("input_id2");
        int min = body.getInt("min_length");
        int max = body.getInt("max_length");
        int keyType = (type.equals("number")) ? TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER : TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;
        Intent intentKey = new Intent(mContext, TransKey2LineActivity.class);
        intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
        intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, title);
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, label1);
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL2, label2);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, min);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, max);
        intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID1, input_id1);
        intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID2, input_id2);
        mContext.startActivityForResult(intentKey, JavaScriptApi.API_201);
    }

    /**
     * dot형 보안키패드 호출 등록
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_201_showDotTypeTransKey(JSONObject json) throws JSONException {
        MLog.d();
        // 연타 클릭 방지
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.getString("title");
            int max = body.getInt("max_length");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_REG_PW);
            commonUserInfo.setOtpTitle(title);
            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtpPwRegActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_201);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * dot형 보안키패드 호출 인증
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_202_showDotTypeTransKey(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.getString("title");
            int max = body.getInt("max_length");
            boolean forgetAuth = body.getBoolean("use_forget_auth");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_PW);
            commonUserInfo.setOtpUseForgetAuth(forgetAuth);
            commonUserInfo.setOtpTitle(title);
            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtpPwAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_202);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 타기관 OTP 비밀번호 등록
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_203_regOtherOTP(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.getString("title");
            int max = body.getInt("max_length");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_REG_OTP);
            commonUserInfo.setOtpTitle(title);
            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtherOtpRegActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_203);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 타기관OTP 비밀번호 인증
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_204_authOtherOTP(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.getString("title");
            int max = body.getInt("max_length");
            boolean forgetAuth = body.getBoolean("use_forget_auth");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);
            commonUserInfo.setOtpUseForgetAuth(forgetAuth);
            commonUserInfo.setOtpTitle(title);
            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtherOtpAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_204);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 모바일OTP 비밀번호 등록
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_205_regMOTP(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.getString("title");
            int max = body.getInt("max_length");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_REG_MOTP);
            commonUserInfo.setOtpTitle(title);
            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtpPwRegActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_205);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 모바일OTP 비밀번호 인증
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_206_authMOTP(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.getString("title");
            int max = body.getInt("max_length");
            boolean forgetAuth = body.getBoolean("use_forget_auth");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_WEB);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP);
            commonUserInfo.setOtpUseForgetAuth(forgetAuth);
            commonUserInfo.setOtpTitle(title);
            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtpPwAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_206);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 타기관OTP 비밀번호 인증
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_207_authOtherOTP(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
//            JSONObject body = json.getJSONObject("body");
//            String title = body.getString("title");
//            int max = body.getInt("max_length");
//            boolean forgetAuth = body.getBoolean("use_forget_auth");
            CommonUserInfo commonUserInfo = new CommonUserInfo();
            //OTP_PW_ENTRY_WEB를 넘기면 단지 비밀번호만 리턴한다.
            //OTP_PW_ENTRY_NATIVE를 넘겨야 OTP인증을 모두 하고 결과값을 리턴한다.
            commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
            commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);
//            commonUserInfo.setOtpUseForgetAuth(forgetAuth);
//            commonUserInfo.setOtpTitle(title);
//            commonUserInfo.setOtpPWLength(max);
            Intent intent = new Intent(mContext, OtherOtpAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_207);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 모바일OTP 비밀번호 인증
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_208_authMOTP(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");


            MOTPManager.getInstance(mContext).getSerialNum(new MOTPManager.MOTPEventListener() {
                @Override
                public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {

                    if ("0000".equals(resultCode)) {
                        CommonUserInfo commonUserInfo = new CommonUserInfo();
                        //OTP_PW_ENTRY_WEB를 넘기면 단지 비밀번호만 리턴한다.
                        //OTP_PW_ENTRY_NATIVE를 넘겨야 OTP인증을 모두 하고 결과값을 리턴한다.
                        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP);
                        Intent intent = new Intent(mContext, OtpPwAuthActivity.class);
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        intent.putExtra(Const.INTENT_OTP_SERIAL_NUMBER, reqData1);
                        intent.putExtra(Const.INTENT_OTP_TA_VERSION, reqData4);

                        mContext.startActivityForResult(intent, JavaScriptApi.API_208);
                        ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                    }
                }
            });
        }
    }

    /**
     * datapicker 호출
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_250_showDataPicker(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String workType = body.optString("work_type");
        String pickerType = body.optString("picker_type");
        if (DataUtil.isNull(pickerType))
            return;
        if ("date".equals(pickerType)) {
            String startDate = body.optString("startdate");
            String endDate = body.optString("enddate");
            String selectedDate = body.optString("selecteddate");
            final int webPickerType = (TextUtils.isEmpty(workType) || "ymd".equals(workType)) ? Const.PICKER_TYPE_DATE_WEB_YMD : Const.PICKER_TYPE_DATE_WEB_YM;
            if (webPickerType == Const.PICKER_TYPE_DATE_WEB_YM) {
                SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(mContext, webPickerType, startDate, endDate, selectedDate);
                slidingDateTimerPickerDialog.setOnWebCallConfirmListener(new SlidingDateTimerPickerDialog.OnWebCallConfirmListener() {
                    @Override
                    public void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name) {
                        JSONObject jsonObj = new JSONObject();
                        try {
                            if (webPickerType == Const.PICKER_TYPE_DATE_WEB_YMD) {
                                jsonObj.put("date", String.format("%04d", year) + Const.EMPTY + String.format("%02d", month) + Const.EMPTY + String.format("%02d", day));
                            } else {
                                jsonObj.put("date", String.format("%04d", year) + Const.EMPTY + String.format("%02d", month));
                            }
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                    }
                });
                slidingDateTimerPickerDialog.show();
            } else {
                Calendar calendar = Calendar.getInstance();
                Calendar minDate = Calendar.getInstance();
                Calendar maxxDate = Calendar.getInstance();
                if (!TextUtils.isEmpty(selectedDate)) {
                    selectedDate = selectedDate.replaceAll("[^0-9]", Const.EMPTY);
                    int startYear = Integer.parseInt(selectedDate.substring(0, 4));
                    int startMon = Integer.parseInt(selectedDate.substring(4, 6));
                    int startDay = Integer.parseInt(selectedDate.substring(6, 8));
                    calendar.set(startYear, startMon - 1, startDay);
                }
                if (!TextUtils.isEmpty(startDate)) {
                    startDate = startDate.replaceAll("[^0-9]", Const.EMPTY);
                    int startYear = Integer.parseInt(startDate.substring(0, 4));
                    int startMon = Integer.parseInt(startDate.substring(4, 6));
                    int startDay = Integer.parseInt(startDate.substring(6, 8));
                    minDate.set(startYear, startMon - 1, startDay);
                } else {
                    minDate.set(1950, 0, 1);
                }
                if (!TextUtils.isEmpty(endDate)) {
                    endDate = endDate.replaceAll("[^0-9]", Const.EMPTY);
                    int endYear = Integer.parseInt(endDate.substring(0, 4));
                    int endMon = Integer.parseInt(endDate.substring(4, 6));
                    int endDay = Integer.parseInt(endDate.substring(6, 8));
                    maxxDate.set(endYear, endMon - 1, endDay);
                } else {
                    maxxDate.set(2100, 11, 31);
                }
                DatePickerFragmentDialog.newInstance(DateTimeBuilder.get().withSelectedDate(calendar.getTimeInMillis()).withMinDate(minDate.getTimeInMillis()).withMaxDate(maxxDate.getTimeInMillis()).withTheme(R.style.datepickerCustom)).show(((FragmentActivity) mContext).getSupportFragmentManager(), "DatePickerFragmentDialog");
            }
        } else if ("cycle".equals(pickerType)) {
            final boolean hasDaily;
            final boolean hasWeek;
            JSONObject selectedDate = body.optJSONObject("selectedcycle");
            if (TextUtils.isEmpty(workType)) {
                hasDaily = true;
                hasWeek = true;
            } else {
                if ("mw".equals(workType)) {
                    hasDaily = false;
                    hasWeek = true;
                } else if ("m".equals(workType)) {
                    hasDaily = false;
                    hasWeek = false;
                } else {
                    hasDaily = true;
                    hasWeek = true;
                }
            }
            SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(mContext, Const.PICKER_TYPE_DATE_CYCLE_WEB_YM, true, hasDaily, hasWeek, selectedDate);
            slidingDateTimerPickerDialog.setOnWebCallConfirmListener(new SlidingDateTimerPickerDialog.OnWebCallConfirmListener() {
                @Override
                public void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name) {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        switch (selectedType) {
                            case 0:
                                jsonObj.put("cycle", "01");
                                jsonObj.put("code", String.format("%02d", selectedIndex));
                                break;
                            case 1:
                                jsonObj.put("cycle", "07");
                                jsonObj.put("code", String.valueOf(selectedIndex));
                                break;
                            case 2:
                                jsonObj.put("cycle", "99");
                                jsonObj.put("code", Const.EMPTY);
                                break;
                            default:
                                break;
                        }
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                }
            });
            slidingDateTimerPickerDialog.show();
        } else if ("data".equals(pickerType)) {
            JSONArray jsonarray = body.optJSONArray("user_custom");
            String selectedData = body.optString("selecteddata");
            if (jsonarray.length() <= 0) {
                return;
            }
            SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(mContext, Const.PICKER_TYPE_CUSTOM_DATA_WEB, jsonarray, selectedData);
            slidingDateTimerPickerDialog.setOnWebCallConfirmListener(new SlidingDateTimerPickerDialog.OnWebCallConfirmListener() {
                @Override
                public void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name) {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("code", code);
                        jsonObj.put("name", name);
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                }
            });
            slidingDateTimerPickerDialog.show();
        }
    }

    /**
     * cdd/edd 고객정보 재확인 일주일간 열지 않기
     */
    private void api_300_notOpenFor1Week() {
        MLog.d();
        try {
            progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
            mOnWebActionListener.showProgress();
            HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
                @Override
                public void endHttpRequest(String ret) {
                    progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);

                    Calendar calendar = Calendar.getInstance();
                    String curTime = String.valueOf(calendar.get(Calendar.YEAR))
                            + String.valueOf(calendar.get(Calendar.MONTH) + 1)
                            + String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));

                    JSONObject object = null;
                    try {
                        object = new JSONObject(ret);
                        JSONObject objectHead = null;
                        objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    } catch (JSONException e) {
                        MLog.e(e);
                    }

                    String valueStr = object.optString("SYS_DTTM");
                    if (!TextUtils.isEmpty(valueStr)) {
                        curTime = valueStr.substring(0, 8);
                    }
                    Prefer.setCDDEDDNotOpenFor1Week(mContext, curTime);
                    // 고객정보 재확인 안내 화면 종료하고 메인으로 이동
                    Intent intent = new Intent(mContext, Main2Activity.class);
                    mContext.startActivity(intent);
                    progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
                    ((BaseActivity) mContext).dismissProgressDialog();
                    mContext.finish();
                }
            });
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * Web->Native 화면 호출
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_301_callNativeScreen(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String screenName = body.getString("screen_name");
            if (DataUtil.isNull(screenName))
                return;

            Intent intent;
            String param = null;

            if (body.has("param")) {
                param = body.optString("param");
            }
            if ("main".equals(screenName) || "mainupdate".equals(screenName)) {
                if ((Activity) mContext instanceof WebMainActivity) {
                    SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
                    if (mApplicationClass.getActivitySize() > 0) {
                        final Activity activity = mApplicationClass.getOpenActivity(".Main2Activity");
                        if (activity != null) {
                            //이거 순서를 지켜주시길.. - 커플계좌 일경우만 처리된다.
                            ((Main2Activity)activity).forceReloadCoupleState(true);

                            if (param != null) {
                                intent = new Intent();
                                intent.putExtra(Const.INTENT_PARAM, param);
                                mContext.setResult(Activity.RESULT_OK);
                            }
                        } else {
                            intent = new Intent(mContext, Main2Activity.class);
                            mContext.startActivity(intent);
                        }
                    } else {
                        intent = new Intent(mContext, Main2Activity.class);
                        mContext.startActivity(intent);
                    }
                    progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
                    ((BaseActivity) mContext).dismissProgressDialog();
                    mContext.finish();
                }else if ((Activity) mContext instanceof Main2Activity) {
                    if("main".equals(screenName)){
                        mOnWebActionListener.closeWebView();
                    }else{
                        mOnWebActionListener.refreshWebView();
                    }
                }
                // 메인화면의 web fragment에서 호출일 떄는 종료하지 않음
                return;
            }


            if ("crt0000100".equals(screenName)) {
                intent = new Intent(mContext, ManageAuthWaysActivity.class);
                // 각 화면에서 전달받을 파라메터로 구분처리
                if (DataUtil.isNotNull(param)) {
                    intent.putExtra(Const.INTENT_PARAM, param);
                }
                mContext.startActivity(intent);
            }else if(screenName.startsWith("tra0090100")){
                //안심이체
                intent = new Intent(mContext, TransferSafeDealSelectActivity.class);
                if (!TextUtils.isEmpty(param)) {
                    JSONObject paramObj = new JSONObject(param);
                    String ACNO = paramObj.optString("ACNO");
                    Logs.e("ACNO : " + ACNO);
                    intent.putExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT, ACNO);
                }
                mContext.startActivity(intent);
            }else if(screenName.startsWith("tra0010100")){
                //통합이체
                final String webParam = param;
                TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
                    @Override
                    public void onCheckFinish() {

                        if(!TextUtils.isEmpty(webParam)){
                            try{
                                JSONObject paramObj = new JSONObject(webParam);
                                String BANKCD = paramObj.optString("BANKCD");
                                String ACNO = paramObj.optString("ACNO");

                                if(TextUtils.isEmpty(BANKCD) || TextUtils.isEmpty(ACNO)){
                                    MyAccountInfo accountInfo = TransferUtils.getSaidaMainAccount();
                                    if(accountInfo == null){
                                        DialogUtil.alert(mContext,"보유계좌가 없습니다.");
                                        return;
                                    }
                                    ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                                    ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.getACNO());
                                }else{
                                    //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                                    ITransferDataMgr.getInstance().setWTCH_BANK_CD(BANKCD);
                                    ITransferDataMgr.getInstance().setWTCH_ACNO(ACNO);
                                }
                            }catch (JSONException e){
                                Logs.printException(e);
                                return;
                            }
                        }else{
                            MyAccountInfo accountInfo = TransferUtils.getSaidaMainAccount();
                            if(accountInfo == null){
                                DialogUtil.alert(mContext,"보유계좌가 없습니다.");
                                return;
                            }
                            ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                            ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.getACNO());
                        }

                        Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                        mContext.startActivity(intent);
                    }
                });
            }else if(screenName.startsWith("tra0200100")) {
                //급여이체
                TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true, new TransferUtils.OnCheckFinishListener() {
                    @Override
                    public void onCheckFinish() {
                        ITransferSalayMgr.getInstance().clearSalaryInfo();
                        Intent intent = new Intent(mContext, ITransferSalaryActivity.class);
                        mContext.startActivity(intent);
                        if(mContext instanceof WebMainActivity) {
                            mContext.finish();
                        }
                    }
                });
            }
        }
    }

    /**
     * 새창으로 Web 호출
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_302_callWebView(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String link = body.getString("link");
            boolean gnb_flag = body.optBoolean("gnb_flag");

            if (DataUtil.isNull(link))
                return;
            if(gnb_flag && mContext instanceof Main2Activity){
                StringBuilder url = new StringBuilder();
                url.append(SaidaUrl.getBaseWebUrl());
                url.append(Const.SLASH);
                url.append(link);

                String params = "";
                if (body.has("param")) {
                    JSONObject param = body.optJSONObject("param");
                    StringBuilder strParam = new StringBuilder();
                    if (DataUtil.isNotNull(param)) {
                        for (int i = 0; i < param.length(); i++) {
                            if (i != 0)
                                strParam.append(Const.AND);
                            try {
                                strParam.append(param.names().getString(i) + Const.EQUAL + URLEncoder.encode(param.getString(param.names().getString(i)), Const.EUC_KR));
                            } catch (UnsupportedEncodingException e) {
                                MLog.e(e);
                            }
                        }
                        params =  strParam.toString();
                    }
                }

                mOnWebActionListener.loadUrl(url.toString(),params);
            }else{
                Intent intent = new Intent(mContext, WebMainActivity.class);
                intent.putExtra("url", SaidaUrl.getBaseWebUrl() + Const.SLASH + link);
                if (body.has("param")) {
                    JSONObject param = body.optJSONObject("param");
                    StringBuilder strParam = new StringBuilder();
                    if (DataUtil.isNotNull(param)) {
                        for (int i = 0; i < param.length(); i++) {
                            if (i != 0)
                                strParam.append(Const.AND);
                            try {
                                strParam.append(param.names().getString(i) + Const.EQUAL + URLEncoder.encode(param.getString(param.names().getString(i)), Const.EUC_KR));
                            } catch (UnsupportedEncodingException e) {
                                MLog.e(e);
                            }
                        }
                        intent.putExtra(Const.INTENT_PARAM, strParam.toString());
                    }
                }
                mContext.startActivityForResult(intent, JavaScriptApi.API_302);
            }
        }
    }

    /**
     * 푸시 앱 아이디
     *
     * @throws JSONException
     */
    private void api_304_requestPushAppID() throws JSONException {
        MLog.d();
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("app_id", FLKPushAgentSender.mAppName);
        jsonObj.put("push_id", Prefer.getPushRegID(mContext));
        jsonObj.put("gcm_id", Prefer.getPushGCMToken(mContext));
        jsonObj.put("os_type", "1");
        jsonObj.put("os_ver", Build.VERSION.RELEASE);
        jsonObj.put("app_ver", Utils.getVersionName(mContext));
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_304), jsonObj);
    }

    /**
     * 푸시 설정 상태
     *
     * @throws JSONException
     */
    private void api_305_requestPushNotiState() throws JSONException {
        MLog.d();
        String state = Utils.isNotificationsEnabled(mContext) ? "YES" : "NO";
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("push_state", state);
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_305), jsonObj);
    }

    /**
     * 푸시 설정으로 가기
     */
    private void api_306_openPushNotiSettingMenu() {
        MLog.d();
        if (DataUtil.isNotNull(mContext)) {
            Utils.openNotificationSettingMenu(mContext);
        }
    }

    //======================================================================
    // Preferance에 값저장및 조회
    //======================================================================
    private void api_350_savePreferance(JSONObject json) throws JSONException{
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String key = body.optString("key");
        if (DataUtil.isNull(key))
            return;
        String value = body.optString("value");

        Prefer.setStringPreference(mContext,key,value);

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("result", "TRUE");
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_350), jsonObj);

    }

    private void api_351_getPreferance(JSONObject json) throws JSONException{
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String key = body.optString("key");
        if (DataUtil.isNull(key))
            return;


        String value = Prefer.getStringPreference(mContext,key);

        JSONObject jsonObj = new JSONObject();
        jsonObj.put("result", "TRUE");
        jsonObj.put("value", value);
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_351), jsonObj);

    }



    /**
     * 챗봇 열기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_400_openChatBot(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String link = body.getString("link");
            if (DataUtil.isNull(link))
                return;
            Intent intent = new Intent(mContext, WebMainActivity.class);
            intent.putExtra("url", link);
            mContext.startActivityForResult(intent, JavaScriptApi.API_400);
        }
    }

    /**
     * 챗봇에서 신규 화면 열기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_401_openChatBotNewWebView(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String screenName = body.getString("screen_name");
            if (DataUtil.isNull(screenName))
                return;
            Intent intent = new Intent(mContext, WebMainActivity.class);
            intent.putExtra("url", SaidaUrl.getBaseWebUrl() + Const.SLASH + screenName);
            if (body.has("param")) {
                JSONObject param = body.optJSONObject("param");
                StringBuilder strParam = new StringBuilder();
                if (DataUtil.isNotNull(param)) {
                    for (int i = 0; i < param.length(); i++) {
                        if (i != 0)
                            strParam.append(Const.AND);
                        try {
                            strParam.append(param.names().getString(i) + Const.EQUAL + URLEncoder.encode(param.getString(param.names().getString(i)), Const.EUC_KR));
                        } catch (UnsupportedEncodingException e) {
                            MLog.e(e);
                        }
                    }
                    intent.putExtra(Const.INTENT_PARAM, strParam.toString());
                }
            }
            // 신규 화면 열기 전에 메인과 신규 사이에 떠있는 화면을 모두 닫는다.
            SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
            mApplicationClass.allActivityFinish(false);
            mContext.startActivityForResult(intent, JavaScriptApi.API_401);
        }
    }

    /**
     * 챗봇에서 고객번호 가져가기
     *
     * @throws JSONException
     */
    private void api_402_requestChatBotCustomNumber() throws JSONException {
        MLog.d();
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("mbrNo", LoginUserInfo.getInstance().getCUST_NO());
        jsonObj.put("cusNo", LoginUserInfo.getInstance().getMBR_NO());
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_402), jsonObj);
    }

    /**
     * 챗봇 닫기
     */
    private void api_403_closeChatBot() {
        MLog.d();
        // 현재 챗봇 화면만 닫는다.
        if ((Activity) mContext instanceof WebMainActivity) {
            progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
            ((BaseActivity) mContext).dismissProgressDialog();
            mContext.finish();
        }
    }

    /**
     * ============================================
     * 통합 이체관련
     */
    /**
     * 통합이체 실행
     */
    private void api_450_transferMain(JSONObject json) throws JSONException{

        JSONObject body = json.getJSONObject("body");

        final String BANKCD = body.optString("WTCH_BANK_CD");
        final String ACNO = body.optString("WTCH_ACNO");
        final String DTLBANKCD = body.optString("WTCH_DTLS_FNLT_CD");

        TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
            @Override
            public void onCheckFinish() {

                if(TextUtils.isEmpty(BANKCD) || TextUtils.isEmpty(ACNO)){
                    MyAccountInfo accountInfo = TransferUtils.getSaidaMainAccount();
                    if(accountInfo == null){
                        DialogUtil.alert(mContext,"보유계좌가 없습니다.");
                        return;
                    }
                    ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                    ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.getACNO());
                }else{
                    //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                    ITransferDataMgr.getInstance().setWTCH_BANK_CD(BANKCD);
                    ITransferDataMgr.getInstance().setWTCH_DTLS_BANK_CD(DTLBANKCD);
                    ITransferDataMgr.getInstance().setWTCH_ACNO(ACNO);
                }

                Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                mContext.startActivity(intent);
            }
        });
    }
    /**
     * 통합이체 내보내기
     */
    private void api_451_transferExport(JSONObject json) throws JSONException{

        final JSONObject body = json.getJSONObject("body");

        //출금계좌정보
        final String WTCH_BANK_CD = body.optString("WTCH_BANK_CD");
        final String WTCH_ACNO = body.optString("WTCH_ACNO");

        TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
            @Override
            public void onCheckFinish() {

                if(TextUtils.isEmpty(WTCH_BANK_CD) || TextUtils.isEmpty(WTCH_ACNO)){
                    MyAccountInfo accountInfo = TransferUtils.getSaidaMainAccount();
                    if(accountInfo == null){
                        DialogUtil.alert(mContext,"사이다뱅크 보유계좌가 없습니다.");
                        return;
                    }
                    ITransferDataMgr.getInstance().setWTCH_BANK_CD(WTCH_BANK_CD);
                    ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.getACNO());
                }else{
                    //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                    ITransferDataMgr.getInstance().setWTCH_BANK_CD(WTCH_BANK_CD);
                    ITransferDataMgr.getInstance().setWTCH_ACNO(WTCH_ACNO);
                }

                mOnWebActionListener.showProgress();

                //사이다 계좌가 아니면 오픈뱅킹 정보를 가져온다.
                if(!TextUtils.isEmpty(WTCH_BANK_CD) && !TransferUtils.isSaidaAccount(WTCH_BANK_CD)){

                    if(OpenBankDataMgr.getInstance().getAccountCnt() == 0){
                        Map param = new HashMap();
                        param.put("BLNC_INQ_YN", "N");

                        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
                            @Override
                            public void endHttpRequest(String ret) {

                                if(((BaseActivity)mContext).onCheckHttpError(ret,false)){
                                    mOnWebActionListener.dismissProgress();
                                    return;
                                }

                                try {
                                    JSONObject object = new JSONObject(ret);

                                    OpenBankDataMgr.getInstance().parsorAccountList(object);

                                    //여기서 디테일 코드를 가져온다.
                                    String detailBankCd = OpenBankDataMgr.getInstance().getDetailBankCd(WTCH_ACNO);
                                    ITransferDataMgr.getInstance().setWTCH_DTLS_BANK_CD(detailBankCd);

                                    procTransferExport(body);
                                } catch (JSONException e) {
                                    Logs.printException(e);
                                    mOnWebActionListener.dismissProgress();
                                }
                            }
                        });
                        return;
                    }else{
                        //여기서 디테일 코드를 가져온다.
                        String detailBankCd = OpenBankDataMgr.getInstance().getDetailBankCd(WTCH_ACNO);
                        ITransferDataMgr.getInstance().setWTCH_DTLS_BANK_CD(detailBankCd);
                    }
                }

                procTransferExport(body);
            }
        });
    }

    /**
     * 451 - 내보내기 데이타 만든 후 다음 화면으로 이동시킨다.
     * @param body
     */
    private void procTransferExport(JSONObject body){
        mOnWebActionListener.dismissProgress();
        try {
            ITransferRemitteeInfo remitteeInfo = new ITransferRemitteeInfo(body);
            remitteeInfo.setTRNF_DVCD("1");
            ITransferDataMgr.getInstance().setTRANSFER_ACCESS_TYPE(ITransferDataMgr.ACCESS_TYPE_EXPORT);
            ITransferDataMgr.getInstance().setMNRC_AMT(remitteeInfo.getTRN_AMT());

            //수취인정보를 추가해 놓는다.
            ITransferDataMgr.getInstance().setTRANSFER_TYPE(ITransferDataMgr.TR_TYPE_ACCOUNT);
            ITransferDataMgr.getInstance().getRemitteInfoArrayList().clear();
            ITransferDataMgr.getInstance().getRemitteInfoArrayList().add(remitteeInfo);

            //받는분이 있는지 체크
            if(TextUtils.isEmpty(remitteeInfo.getCNTP_BANK_ACNO())){
                //받는분 정보가 없으면
                Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                mContext.startActivity(intent);
            }else{
                Intent intent = new Intent(mContext, ITransferSendSingleActivity.class);
                mContext.startActivity(intent);
            }
            //다시 돌아왔을때 웹화면 리로드.
            mOnWebActionListener.setNeedResumeRefresh();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 통합이체 가져오기
     */
    private void api_452_transferImport(JSONObject json) throws JSONException{

        final JSONObject body = json.getJSONObject("body");

        //출금계좌정보
        final String WTCH_BANK_CD = body.optString("WTCH_BANK_CD");
        final String WTCH_ACNO = body.optString("WTCH_ACNO");

        TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
            @Override
            public void onCheckFinish() {

                if(!TextUtils.isEmpty(WTCH_BANK_CD) && !TextUtils.isEmpty(WTCH_ACNO)){
                    //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                    ITransferDataMgr.getInstance().setWTCH_BANK_CD(WTCH_BANK_CD);
                    ITransferDataMgr.getInstance().setWTCH_ACNO(WTCH_ACNO);
                }


                if(!TextUtils.isEmpty(WTCH_BANK_CD) && !TransferUtils.isSaidaAccount(WTCH_BANK_CD)){

                    mOnWebActionListener.showProgress();
                    if(OpenBankDataMgr.getInstance().getAccountCnt() == 0){
                        Map param = new HashMap();
                        param.put("BLNC_INQ_YN", "N");

                        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
                            @Override
                            public void endHttpRequest(String ret) {

                                if(((BaseActivity)mContext).onCheckHttpError(ret,false)){
                                    mOnWebActionListener.dismissProgress();
                                    return;
                                }


                                try {
                                    JSONObject object = new JSONObject(ret);

                                    OpenBankDataMgr.getInstance().parsorAccountList(object);

                                    requestOpenBankDataByAccount(WTCH_BANK_CD,WTCH_ACNO,body);

                                } catch (JSONException e) {
                                    Logs.printException(e);
                                    mOnWebActionListener.dismissProgress();
                                }
                            }
                        });
                    }else{
                        requestOpenBankDataByAccount(WTCH_BANK_CD,WTCH_ACNO,body);
                    }
                    return;
                }

                goITransferScreen(body);

            }
        });
    }
    /**
     * 452 통합이체 가져오기 - 오픈뱅킹 계좌별로 잔액,출금가능금액 조회
     */
    private void requestOpenBankDataByAccount(final String BANK_CD,String ACNO,final JSONObject body){

        Map param = new HashMap();


        param.put("RPRS_FNLT_CD", BANK_CD);
        param.put("ACNO", ACNO);
        param.put("BLNC_INQ_YN", "Y");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0140200A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(((BaseActivity)mContext).onCheckHttpError(ret,false)){
                    mOnWebActionListener.dismissProgress();
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    //계좌번호
                    String ACNO = object.optString("ACNO");
                    //계좌잔액
                    String ACCO_BLNC = object.optString("ACCO_BLNC");
                    //출금가능금액
                    String WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");

                    //오픈뱅킹 데이터 모델에도 업데이트 해준다.
                    OpenBankDataMgr.getInstance().updateAccountAmount(BANK_CD,ACNO,ACCO_BLNC,WTCH_POSB_AMT);

                    goITransferScreen(body);
                } catch (JSONException e) {
                    Logs.printException(e);
                    mOnWebActionListener.dismissProgress();
                }

            }
        });
    }

    /**
     * 452 통합이체 가져오기 - 이체화면으로 이동.
     * @param object
     */
    private void goITransferScreen(JSONObject object){
        mOnWebActionListener.dismissProgress();
        try {
            ITransferRemitteeInfo remitteeInfo = new ITransferRemitteeInfo(object);
            remitteeInfo.setTRNF_DVCD("1");
            ITransferDataMgr.getInstance().setTRANSFER_ACCESS_TYPE(ITransferDataMgr.ACCESS_TYPE_IMPORT);
            ITransferDataMgr.getInstance().setMNRC_AMT(remitteeInfo.getTRN_AMT());

            //수취인정보를 추가해 놓는다.
            ITransferDataMgr.getInstance().setTRANSFER_TYPE(ITransferDataMgr.TR_TYPE_ACCOUNT);
            ITransferDataMgr.getInstance().getRemitteInfoArrayList().clear();
            ITransferDataMgr.getInstance().getRemitteInfoArrayList().add(remitteeInfo);

            //받는분이 있는지 체크
            if(TextUtils.isEmpty(remitteeInfo.getCNTP_BANK_ACNO())){
                //받는분 정보가 없으면
                Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                mContext.startActivity(intent);
            }else{
                Intent intent = new Intent(mContext, ITransferSendSingleActivity.class);
                mContext.startActivity(intent);
            }
            //다시 돌아왔을때 웹화면 리로드.
            mOnWebActionListener.setNeedResumeRefresh();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 계좌관리에서 계좌 별명 변경시 네이티브에 바로 적용되도록 하기 위해 추가
     *
     * @param json
     * @throws JSONException
     */
    public void api_453_changeAccountAlas(JSONObject json) throws JSONException {
        JSONObject body = json.getJSONObject("body");
        String ACNO = body.optString("ACNO");
        String ACCO_ALS = body.optString("ACCO_ALS");
        String DSCT_CD = body.optString("DSCT_CD");

        if(DSCT_CD.equalsIgnoreCase("1")){
            for(int i=0;i<LoginUserInfo.getInstance().getMyAccountInfoArrayList().size();i++){
                MyAccountInfo info =  LoginUserInfo.getInstance().getMyAccountInfoArrayList().get(i);
                if(info.getACNO().equalsIgnoreCase(ACNO)){
                    info.setACCO_ALS(ACCO_ALS);
                    break;
                }
            }
        }else{
            if(OpenBankDataMgr.getInstance().getAccountCnt() == 0) return;

            for(int i=0;i<OpenBankDataMgr.getInstance().getGroupCnt();i++){
                OpenBankDataMgr.OpenBankGroupInfo groupInfo = OpenBankDataMgr.getInstance().getOpenBankGroupList().get(i);
                for(int k=0;k<groupInfo.getAccountList().size();k++){
                    OpenBankAccountInfo accInfo = groupInfo.getAccountList().get(k);
                    if(accInfo.ACNO.equalsIgnoreCase(ACNO)){
                        accInfo.ACCO_ALS = ACCO_ALS;
                        break;
                    }
                }
            }
        }
    }

    /**
     * PIN 전자서명
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_500_pinSign(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String signDocText = body.optString("sign_doc_text");
            String signText = body.optString("sign_text");
            String svcId = body.optString("svc_id");
            String bizDvcd = body.optString("biz_dv_cd");
            String trnCd = body.optString("trn_cd");
            String op = Const.SSENSTONE_AUTHENTICATE;
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
            commonUserInfo.setOperation(op);
            commonUserInfo.setSignData(signText);
            commonUserInfo.setSignDocData(signDocText);
            Intent intent = new Intent(mContext, PincodeAuthActivity.class);
            intent.putExtra(Const.INTENT_SERVICE_ID, svcId);
            intent.putExtra(Const.INTENT_BIZ_DV_CD, bizDvcd);
            intent.putExtra(Const.INTENT_TRN_CD, trnCd);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_500);
            ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 지문 전자서명
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_501_fingerprintSign(JSONObject json) throws JSONException {
        MLog.d();
        final FidoDialog fidoDialog = DialogUtil.fido(mContext, true,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                },
                new Dialog.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                        StonePassManager.getInstance(mContext.getApplication()).cancelFingerPrint();
                    }
                });
        fidoDialog.setCanceledOnTouchOutside(false);

        StonePassManager.StonePassListener spListener = new StonePassManager.StonePassListener() {
            @Override
            public void stonePassResult(String op, int errorCode, String errorMsg) {
                if (DataUtil.isNotNull(fidoDialog))
                    fidoDialog.dismiss();
                fidoDialog.showFidoCautionMsg(false);
                String msg = Const.EMPTY;
                switch (errorCode) {
                    case 2:
                    case 4:
                    case 8:
                    case 9:
                    case 11:
                    case 1001:
                        //msg = "지문 인식을 하지 못했습니다. 다시 시도해주세요.";
                        fidoDialog.showFidoCautionMsg(true);
                        return;
                    case 1200:
                        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                            msg = "등록했습니다.";
                            Prefer.setFidoRegStatus(mContext, true);
                            Prefer.setFlagFingerPrintChange(mContext, false);
                        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                            msg = "인증했습니다.";
                        } else {
                            msg = "해제했습니다.";
                            Prefer.setFidoRegStatus(mContext, false);
                        }
                        break;

                    case 1404:
                        msg = "사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.";
                        break;

                    case 1491:
                        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                            msg = "다른 사용자 지문으로 이미 등록되어 있습니다.";
                        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                            if (Prefer.getFidoRegStatus(mContext)) {
                                msg = "인증을 실패했습니다.";
                            } else {
                                msg = "사용자 정보가 없습니다. 먼저 등록하시기 바랍니다.";
                            }
                        }
                        break;

                    case 0:
                        msg = "해제했습니다.";
                        break;

                    case 3:
                        msg = "취소하셨습니다.";
                        break;

                    case 10:
                        msg = errorMsg;
                        break;

                    case 9998:
                        msg = "서버로 부터 리턴 값이 널(Null)입니다..";
                        break;

                    case 9999:
                        msg = "작업중 json에러가 발생했습니다.";
                        break;

                    default:
                        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                            msg = "등록을 실패했습니다.";
                        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                            msg = "인증을 실패했습니다.";
                        } else {
                            msg = "해제를 실패했습니다.";
                        }
                        break;
                }
                msg = "code [" + errorCode + "]\n" + msg;
                Logs.showToast(mContext, msg);
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("signed_data", errorCode);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_501), jsonObj);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        };
        StonePassManager.FingerPrintDlgListener fpdListener = new StonePassManager.FingerPrintDlgListener() {
            @Override
            public void showFingerPrintDialog() {
                if (DataUtil.isNotNull(fidoDialog))
                    fidoDialog.show();
            }
        };
        StonePassManager.getInstance(mContext.getApplication()).ssenstoneFIDO(mContext, spListener, fpdListener, Const.SSENSTONE_AUTHENTICATE, null, "FINGERPRINT", "FINGERPRINT");
    }


    /**
     * 휴대폰 본인 인증
     */
    private void api_502_selfCertify(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            boolean isFullIdnumber = body.optBoolean("full_identify_no", false);
            boolean useSession = body.optBoolean("use_session", false);
            Intent intent;
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
            // 세션정보로 처리
            if (useSession) {
                LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
                // 세션정보 없을 때 직접 입력
                if (TextUtils.isEmpty(loginUserInfo.getMBR_NO())) {
                    commonUserInfo.setUseSession(false);
                    if (isFullIdnumber) {
                        intent = new Intent(mContext, PersonalInfoWithFullIdNumberActivity.class);
                    } else {
                        intent = new Intent(mContext, CertifyPhonePersonalInfoActivity.class);
                    }
                } else {
                    commonUserInfo.setUseSession(true);
                    String userName = loginUserInfo.getCUST_NM();
                    if (!isFullIdnumber) {
                        String userBirth = loginUserInfo.getBRDD();
                        String userSex = loginUserInfo.getNRID_SEX_CD();
                        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(userBirth) || TextUtils.isEmpty(userSex)) {
                            Logs.showToast(mContext, "로그인 정보 오류");
                            return;
                        }
                        commonUserInfo.setBirth(userBirth);
                        commonUserInfo.setSexCode(userSex);
                    } else {
                        String jumin = loginUserInfo.getNRID();
                        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(jumin)) {
                            Logs.showToast(mContext, "로그인 정보 오류");
                            return;
                        }
                        commonUserInfo.setIdnumber(jumin);
                    }
                    intent = new Intent(mContext, CertifyPhoneActivity.class);
                    commonUserInfo.setName(userName);
                }
            } else {
                // 직접 입력
                commonUserInfo.setUseSession(false);
                if (isFullIdnumber) {
                    intent = new Intent(mContext, PersonalInfoWithFullIdNumberActivity.class);
                } else {
                    intent = new Intent(mContext, CertifyPhonePersonalInfoActivity.class);
                }
            }
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            mContext.startActivityForResult(intent, JavaScriptApi.API_502);
        }
    }

    /**
     * 타행계좌인증
     */
    private void api_503_certifyAnotherBank(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String bzwrDvcd = body.optString("nn_meet_bzwr_dvcd");
        String propNo = body.optString("prop_no");
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String userName = loginUserInfo.getCUST_NM();
        if (DataUtil.isNull(userName)) {
            Logs.showToast(mContext, "로그인 정보 오류");
            return;
        }
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
        commonUserInfo.setName(userName);
        Intent intent = new Intent(mContext, AnotherBankAccountVerifyActivity.class);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD, bzwrDvcd);
        intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO, propNo);
        mContext.startActivityForResult(intent, JavaScriptApi.API_503);
    }

    /**
     * 스크래핑
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_600_scraping(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String identify_no = body.optString("identify_no");
            String userName = body.optString("user_name");
            boolean isCertOnly = body.optBoolean("cert_only");
            Intent intent = new Intent(mContext, CertListActivity.class);
            intent.putExtra(Const.INTENT_CERT_PURPOSE, Const.CertJobType.SCRAPING);
            intent.putExtra(Const.INTENT_CERT_IDDATA, identify_no);
            intent.putExtra(Const.INTENT_CERT_SERVICEDATA, body.toString());
            intent.putExtra(Const.INTENT_CERT_USERNAME, userName);
            intent.putExtra(Const.INTENT_CERT_NO_SCRAPING, isCertOnly);
            mContext.startActivityForResult(intent, JavaScriptApi.API_600);
        }
    }

    /**
     * 스크래핑
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    public void api_601_scraping(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String identify_no = body.optString("identify_no");
            String userName = body.optString("user_name");
            boolean isCertOnly = body.optBoolean("cert_only");
            Intent intent = new Intent(mContext, CertListActivity.class);
            intent.putExtra(Const.INTENT_CERT_PURPOSE, Const.CertJobType.SCRAPING);
            intent.putExtra(Const.INTENT_CERT_IDDATA, identify_no);
            intent.putExtra(Const.INTENT_CERT_SERVICEDATA, body.toString());
            intent.putExtra(Const.INTENT_CERT_USERNAME, userName);
            intent.putExtra(Const.INTENT_CERT_NO_SCRAPING, isCertOnly);
            mContext.startActivityForResult(intent, JavaScriptApi.API_601);
        }
    }

    /**
     * MOTP 시리얼 넘버 요청
     */
    private void api_700_reqMOTPTAVersion() {
        MLog.d();
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        mOnWebActionListener.showProgress();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.getTaVersion(new MOTPManager.MOTPEventListener() {
            @Override
            public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                //((WebMainActivity)mContext).dismissProgressDialog();
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    jsonObj.put("ta_version", reqData1);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_700), jsonObj);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * MOTP 발급정보 저장
     */
    private void api_701_saveMOTPIssueInfo(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String otpKey1 = body.optString("otp_key1");
        String otpKey2 = body.optString("otp_key2");
        String issueDate = body.optString("issue_date");
        final String serialNum = body.optString("serial_num");
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        mOnWebActionListener.showProgress();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.saveIssueInfo(new MOTPManager.MOTPEventListener() {
            @Override
            public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    if ("0000".equals(resultCode))
                        LoginUserInfo.getInstance().setMOTPSerialNumber(serialNum);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_701), jsonObj);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        }, otpKey1, otpKey2, serialNum, issueDate);
    }

    /**
     * MOTP 일련번호 요청
     */
    private void api_702_reqMOTPSerialNum() {
        MLog.d();
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        mOnWebActionListener.showProgress();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.getSerialNum(new MOTPManager.MOTPEventListener() {
            @Override
            public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    jsonObj.put("serial_num", reqData1);
                    jsonObj.put("vender_code", reqData2);
                    jsonObj.put("issue_date", reqData3);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_702), jsonObj);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * MOTP 번호요청
     */
    private void api_703_reqMOTPCode(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String otpTime = body.optString("otp_time");
        String serrialNum = body.optString("serial_num");
        String hti = body.optString("hti");
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        mOnWebActionListener.showProgress();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.reqOTPCode(new MOTPManager.MOTPEventListener() {
            @Override
            public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    jsonObj.put("otp_code", reqData1);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_703), jsonObj);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        }, otpTime, serrialNum, hti);
    }

    /**
     * MOTP 폐기요청
     */
    private void api_704_reqMOTPDissue(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String serrialNum = body.optString("serial_num");
        progressDismissDelayHandler.removeCallbacks(progressDismissRunnable);
        mOnWebActionListener.showProgress();
        MOTPManager motpManager = MOTPManager.getInstance(mContext.getApplication());
        motpManager.reqDissue(new MOTPManager.MOTPEventListener() {
            @Override
            public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                progressDismissDelayHandler.postDelayed(progressDismissRunnable, MIN_PROGRESS_DISMISS_INTERVAL);
                try {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result_code", resultCode);
                    jsonObj.put("result_msg", resultMsg);
                    if ("0000".equals(resultCode))
                        LoginUserInfo.getInstance().setMOTPSerialNumber(Const.EMPTY);
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_704), jsonObj);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        }, serrialNum);
    }

    /**
     * OCR 촬영
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_800_runOcrCamera(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent() && DataUtil.isNotNull(mContext)) {
            String[] perList = new String[]{Manifest.permission.CAMERA};
            if (PermissionUtils.checkPermission(mContext, perList, JavaScriptApi.API_800)) {
                JSONObject body = json.getJSONObject("body");
                String docType = body.optString("doc_type");
                Intent intent = new Intent(mContext, CameraActivity.class);
                if (Const.IDENTIFICATION_TYPE_ID.equalsIgnoreCase(docType)) {
                    intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
                    intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
                } else if (Const.IDENTIFICATION_TYPE_DOC.equalsIgnoreCase(docType)) {
                    intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_DOCUMENT_A4);
                    intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_PORTRAIT);
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [문서]를 맞추면 자동촬영 됩니다");
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [문서]를 맞추고 촬영해 주세요");
                }
                intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, Const.ENCRYPT_KEY);
                mContext.startActivityForResult(intent, JavaScriptApi.API_800);
                mContext.overridePendingTransition(0, 0);
            } else {
                if (DataUtil.isNotNull(json)) {
                    setJsonObjecParam(JavaScriptApi.API_800, json.toString());
                }
            }
        }
    }

    /**
     * FDS 정보조회
     *
     * @param json JSON 데이터
     */
    private void api_801_searchFDSInfo(JSONObject json) {
        MLog.d();
        FDSManager.getInstance().getFDSInfo(mContext, (WebMainActivity) mContext);
    }

    /**
     * 단말기 기기 정보
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_802_DeviceInfo(JSONObject json) throws JSONException {
        MLog.d();
        if (DataUtil.isNotNull(mContext)) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("os", "android");
            jsonObj.put("device_info1", FDSManager.getInstance().getEncIMEI(mContext));
            jsonObj.put("device_info2", FDSManager.getInstance().getEncUUID(mContext));
            callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_802), jsonObj);
        }
    }

    /**
     * 화면 캡쳐해서 파일에 저장
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_900_screenCaptureSave(JSONObject json) throws JSONException {
        MLog.d();
        String[] perList = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        JSONObject body = json.getJSONObject("body");
        String fileName = body.optString("file_name");
        boolean fileSave = body.optBoolean("file_save", true);
        if (fileSave) {
            fileName = DataUtil.isNull(fileName) ? Utils.getCurrentTime(Const.EMPTY) : (fileName + Const.UNDER + Utils.getCurrentTime(Const.EMPTY));
            fileName = fileName + ".jpg";
            if (PermissionUtils.checkPermission(mContext, perList, JavaScriptApi.API_900)) {
                BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
                boolean ret = ImgUtils.captureSave((Activity) mContext, webView, fileName);
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("result", ret);
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_900), jsonObj);
            } else {
                setJsonObjecParam(JavaScriptApi.API_900, fileName);
            }
        } else {
            if (PermissionUtils.checkPermission(mContext, perList, JavaScriptApi.API_900)) {
                BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
                byte[] imgData = ImgUtils.captureSave(webView);
                JSONObject jsonObj = new JSONObject();
                if (imgData != null && imgData.length > 0) {
                    jsonObj.put("result", true);
                    jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                } else {
                    jsonObj.put("result", false);
                }
                callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_900), jsonObj);
            } else {
                setJsonObjecParam(JavaScriptApi.API_900, "webreturn");
            }
        }
    }

    private void api_901_jexEnc(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String plain_text = body.optString("plain_text");
        String enc_text = Const.EMPTY;
        JSONObject jsonObj = new JSONObject();
        if (DataUtil.isNotNull(plain_text)) {
            try {
                enc_text = JexAESEncrypt.encrypt(plain_text);
            } catch (Exception e) {
                MLog.e(e);
            }
            jsonObj.put("enc_text", enc_text);
        }
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_901), jsonObj);
    }

    private void api_902_jexDec(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String enc_text = body.optString("enc_text");
        String plain_text = Const.EMPTY;
        JSONObject jsonObj = new JSONObject();
        if (DataUtil.isNotNull(enc_text)) {
            try {
                plain_text = JexAESEncrypt.decrypt(enc_text);
            } catch (Exception e) {
                MLog.e(e);
            }
            jsonObj.put("plain_text", plain_text);
        }
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_902), jsonObj);
    }

    /**
     * PDF뷰어
     *
     * @param json
     * @throws JSONException
     */
    private void api_903_pdfVIew(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String title = body.optString("title");
            String url = body.optString("url");
            Intent intent = new Intent(mContext, WebTermsActivity.class);
            intent.putExtra("title", title);
            intent.putExtra("url", SaidaUrl.getBaseWebUrl() + Const.SLASH + url);
            mContext.startActivityForResult(intent, JavaScriptApi.API_903);
        }
    }

    /**
     * 전화 걸기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_904_makeCall(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        final String telNo = body.getString("tel_no");
        if (DataUtil.isNull(telNo)) {
            Utils.showToast(mContext, R.string.common_msg_empty_phone_nmumber);
            return;
        }
        String msg = mContext.getString(R.string.common_msg_ask_call_now, telNo);
        DialogUtil.alert(
                mContext,
                msg,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.makeCall(mContext, telNo);
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MLog.d();
                    }
                }
        );
    }

    /**
     * 앱 버전정보 확인
     *
     * @throws JSONException
     */
    private void api_905_appVersionInfo() throws JSONException {
        MLog.d();
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("current_version", Utils.getVersionName(mContext));
        jsonObj.put("device_os_version", Build.VERSION.RELEASE);
        jsonObj.put("device_model_name", Build.MODEL);
        callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_905), jsonObj);
    }

    /**
     * 세션동기화
     */
    private void api_906_sessionSync() {
        MLog.d();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                try {
                    if (DataUtil.isNotNull(ret)) {
                        JSONObject object = new JSONObject(ret);
                        if (DataUtil.isNull(object)) {
                            return;
                        }
                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            return;
                        }
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().syncLoginSession(ret);
                        LoginUserInfo.getInstance().setFakeFinderAllow(true);
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }

            }
        });
    }

    /**
     * 프로필 사진 가져오기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_907_getProfileImage(JSONObject json) throws JSONException {
        MLog.d();
        //if (Utils.isEnableClickEvent() && DataUtil.isNotNull(mContext)) {
            JSONObject body = json.getJSONObject("body");
            String workType = body.optString("work_type");
            if (DataUtil.isNull(workType))
                return;

            mOnWebActionListener.getProfileImage(json,workType);
        //}
    }

    /**
     * 외부 브라우저로 url 열기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_908_openUrl(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String url = body.getString("url");
            if (DataUtil.isNull(url))
                return;
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }
    }

    /**
     * SNS 공유하기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_910_shareSNS(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String strMsg = body.optString("str_msg");
            String strImgData = body.optString("str_imgdata");
            if (TextUtils.isEmpty(strMsg))
                return;
            Intent share = new Intent(Intent.ACTION_SEND);
            if (DataUtil.isNotNull(strImgData)) {
                byte[] imgdata = Base64.decode(strImgData.getBytes(), Base64.DEFAULT);
                Bitmap bitmap = ImgUtils.byteArrayToBitmap(imgdata);
                if (DataUtil.isNotNull(bitmap)) {
                    File storageDir = mContext.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
                    if (!storageDir.exists()) {
                        storageDir.mkdir();
                    }
                    File imageFile = new File(storageDir, "shareimg.jpg");
                    FileOutputStream outputStream = null;
                    try {
                        outputStream = new FileOutputStream(imageFile);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                        outputStream.flush();
                        outputStream.close();
                    } catch (FileNotFoundException e) {
                        MLog.e(e);
                        return;
                    } catch (IOException e) {
                        MLog.e(e);
                        return;
                    }
                    Uri uri;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        uri = FileProvider.getUriForFile(mContext, BuildConfig.APPLICATION_ID + ".shareimagefileprovider", imageFile);
                        share.setDataAndType(Uri.fromFile(imageFile), "application/vnd.android.package-archive");
                    } else {
                        uri = Uri.fromFile(imageFile);
                    }
                    share.setType("image/jpg");
                    share.putExtra(Intent.EXTRA_STREAM, uri);
                    mContext.startActivity(Intent.createChooser(share, "공유하기"));
                }
            } else {
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, strMsg);
                mContext.startActivity(Intent.createChooser(share, "공유하기"));
            }
        }
    }

    /**
     * 클립보드 복사
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_911_copyClipborad(JSONObject json) throws JSONException {
        MLog.d();
        JSONObject body = json.getJSONObject("body");
        String copyText = body.optString("copy_text");
        if (DataUtil.isNull(copyText))
            return;
        ClipboardManager clipboardManager = (ClipboardManager) mContext.getSystemService(CLIPBOARD_SERVICE);
        ClipData clipData = ClipData.newPlainText("label", copyText);
        clipboardManager.setPrimaryClip(clipData);
    }

    /**
     * 카카오톡 보내기
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_912_sendKakaoTalk(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent() && DataUtil.isNotNull(mContext)) {
            if (!Utils.isInstallApp(mContext, "com.kakao.talk")) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://search?q=pname:com.kakao.talk"));
                mContext.startActivity(intent);
                return;
            }

            JSONObject body = json.getJSONObject("body");
            String str_link = body.optString("str_link");
            String img_link = body.optString("img_link");
            String str_msg = body.optString("str_msg");
            String str_btn = body.optString("str_link_btn_name");
            if (DataUtil.isNull(str_btn))
                str_btn = "확인하기";

            Map<String, String> callbackParameters = new HashMap<>();
            if (DataUtil.isNull(img_link)) {
                TextTemplate txtTemplateparams = TextTemplate.newBuilder(
                        str_msg,
                        LinkObject.newBuilder()
                                .setWebUrl(str_link)
                                .setMobileWebUrl(str_link)
                                .build())
                        .setButtonTitle(str_btn)
                        .build();
                KakaoLinkService.getInstance().sendDefault(mContext, txtTemplateparams, callbackParameters, new ResponseCallback<KakaoLinkResponse>() {
                    @Override
                    public void onFailure(ErrorResult errorResult) {
                        MLog.i("errorResult >> " + errorResult.toString());
                    }

                    @Override
                    public void onSuccess(KakaoLinkResponse result) {
                        MLog.d();
                    }
                });
            } else {
                FeedTemplate feedTemplateparams = FeedTemplate.newBuilder(
                        ContentObject.newBuilder(
                                str_msg,
                                img_link,
                                LinkObject.newBuilder().setWebUrl(str_link).setMobileWebUrl(str_link).build()
                        ).build()
                ).addButton(new ButtonObject(str_btn, LinkObject.newBuilder().setWebUrl(str_link).setMobileWebUrl(str_link).build())).build();

                KakaoLinkService.getInstance().sendDefault(mContext, feedTemplateparams, callbackParameters, new ResponseCallback<KakaoLinkResponse>() {
                    @Override
                    public void onFailure(ErrorResult errorResult) {
                        MLog.i("errorResult >> " + errorResult.toString());
                    }

                    @Override
                    public void onSuccess(KakaoLinkResponse result) {
                        MLog.d();
                    }
                });
            }
        }
    }

    /**
     * Appsflyer
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_913_sendAppsflyerEvent(JSONObject json) throws JSONException {
        // MLog.d();
        JSONObject body = json.getJSONObject("body");
        String event_name = body.optString("event_name");
        if (DataUtil.isNull(event_name))
            return;
        AppsFlyerManager.getInstance(mContext).sendAppsFlyerTrackEvent(event_name, Const.EMPTY);
    }

    /**
     * 주소록 검색 결과값을 리턴한다.
     * 안전거래에서만 호출한다.
     *
     * @param json
     * @throws JSONException
     */
    private void api_915_returnSearchAddress(JSONObject json) throws JSONException {
        // MLog.d();

        Logs.e("api_915_returnSearchAddress : " + json.toString());
        //{"header":{"api":"915","callback":null},"body":{"addressDetail":"101동 1402호","addressType":"1","zonecode":"05045","buildingCode":"1121510300102440005000001","jibun":{"address":"서울 광진구 구의동 667","engAddr":"667, Guui-dong, Gwangjin-gu, Seoul, Korea"},"address":"서울 광진구 아차산로 431 (강변에스케이뷰)","engAddr":"431, Achasan-ro, Gwangjin-gu, Seoul, Korea"}}
        JSONObject body = json.getJSONObject("body");
        if(body.toString().length() > 0){
            Intent intent = new Intent();
            intent.putExtra(Const.INTENT_RESULT_ZIPCODE,body.optString("zonecode"));
            intent.putExtra(Const.INTENT_RESULT_BLDB_CODE,body.optString("buildingCode"));
            intent.putExtra(Const.INTENT_RESULT_ADDRESS,body.optString("address"));
            intent.putExtra(Const.INTENT_RESULT_ADDRESS_DETAIL,body.optString("addressDetail"));
            mContext.setResult(Activity.RESULT_OK,intent);
            mContext.finish();
        }

    }

    /**
     * 탭바 이동
     *
     * @param json JSON 데이터
     * @throws JSONException
     */
    private void api_920_selectTab(JSONObject json) throws JSONException {
        MLog.d();
        if (Utils.isEnableClickEvent()) {
            JSONObject body = json.getJSONObject("body");
            String str_index = body.optString("str_index");
            if (DataUtil.isNull(str_index))
                return;

            if ((Activity) mContext instanceof Main2Activity) {
                ((Main2Activity) mContext).selectTab(Integer.parseInt(str_index));
            }
        }
    }

    /**
     * 휴대폰 주소록 추출
     */
    private void api_930_requestContacts() {
        MLog.d();
        String[] perList = new String[]{Manifest.permission.READ_CONTACTS};
        if (DataUtil.isNotNull(mContext)) {
            if (PermissionUtils.checkPermission(mContext, perList, JavaScriptApi.API_930)) {
                JSONObject jsonObj = new JSONObject();
                ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();
                ContactsInfo contactsInfo;
                Cursor contactCursor = null;
                String name;
                String phonenumber;
                JSONArray jsonArray = new JSONArray();
                try {
                    Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                    String[] projection = new String[]{
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                            ContactsContract.CommonDataKinds.Phone.NUMBER,
                            ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.Contacts.PHOTO_ID
                    };

                    String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
                    contactCursor = mContext.getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
                    if (contactCursor.moveToFirst()) {
                        do {
                            phonenumber = contactCursor.getString(1);
                            if (phonenumber.length() <= 0)
                                continue;
                            name = contactCursor.getString(2);
                            phonenumber = phonenumber.replaceAll("[^0-9]", Const.EMPTY);
                            if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                                continue;
                            }
                            // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 핸드폰 번호인 경우
                            if (!TextUtils.isEmpty(name) && listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                                contactsInfo = new ContactsInfo();
                                contactsInfo.setTLNO(phonenumber);
                                contactsInfo.setFLNM(name);
                                listContactsInPhonebook.add(contactsInfo);
                                JSONObject itemObject = new JSONObject();
                                itemObject.put("TLNO", phonenumber);
                                itemObject.put("FLNM", name);
                                jsonArray.put(itemObject);
                            }
                        } while (contactCursor.moveToNext());
                    }
                } catch (Exception e) {
                    MLog.e(e);
                } finally {
                    if (DataUtil.isNotNull(contactCursor)) {
                        contactCursor.close();
                    }
                    try {
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                        jsonObj.put("addr_data", jsonArray);
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    callJavascriptFunc(getCallBackFunc(JavaScriptApi.API_930), jsonObj);
                }
            }
        }
    }

    /**
     * 커플 멤버 선택을 위한 휴대폰 주소록 추출
     */
    private void api_931_requestCoupleContacts() {
        String[] perList = new String[]{Manifest.permission.READ_CONTACTS};
        if (DataUtil.isNotNull(mContext)) {
            if (PermissionUtils.checkPermission(mContext, perList, JavaScriptApi.API_931)) {
                Intent intent = new Intent(mContext, CoupleContactsPickActivity.class);
                mContext.startActivityForResult(intent, JavaScriptApi.API_931);
                mContext.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
            }
        }
    }

    /**
     * 문자메세지 보내기
     */
    private void api_932_sendSMS(JSONObject json) throws JSONException {

        JSONObject body = json.getJSONObject("body");
        String PHONE_NO = body.optString("TLNO");
        String MSG = body.optString("MSG_CNTN");

        if(TextUtils.isEmpty(PHONE_NO)){
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.putExtra("sms_body", MSG);

            intent.setType("vnd.android-dir/mms-sms");
            mContext.startActivity(intent);
        }else{
            Uri uri = Uri.parse("smsto:" + PHONE_NO);

            Intent intent = new Intent(Intent.ACTION_SENDTO, uri);

            intent.putExtra("sms_body", MSG);
            mContext.startActivity(intent);
        }

    }
}
