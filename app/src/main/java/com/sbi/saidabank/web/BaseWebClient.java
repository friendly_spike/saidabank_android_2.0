package com.sbi.saidabank.web;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;

/**
 * BaseWebClient
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class BaseWebClient extends WebViewClient {

    private Activity mActivity;
    //private int mWebViewFragmentType;
    private boolean mConnectAbort;
    private OnWebActionListener onWebActionListener;

    private String[] captureEnableList = {"crt0010101","crt0030100","unt0030100","unt0030101","cus0010100","lgn0020100"};

    public BaseWebClient(Activity activity,OnWebActionListener listener) {
        this.mActivity = activity;
        this.onWebActionListener = listener;
    }


    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);

        MLog.d("onPageStarted : " + url);
        if (DataUtil.isNotNull(mActivity)) {
            mConnectAbort = false;
            onWebActionListener.showProgress();
        }

        boolean iFind = false;
        for(int i=0;i<captureEnableList.length;i++){
            if(url.contains(captureEnableList[i])){
                iFind = true;
                break;
            }
        }
        Logs.e("onPageStarted -iFind : " + iFind);
        //일부 웹페이지 캡쳐 못하도록 추가.
        if(iFind){
            mActivity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        MLog.d("onPageFinished : " + url);
        MLog.d("mConnectAbort : " + mConnectAbort);
        if (DataUtil.isNotNull(mActivity)) {
            onWebActionListener.dismissProgress();
            if (mConnectAbort) {
                onWebActionListener.setCurrentPage("");
                onWebActionListener.setFinishedPageLoading(false);
            }else {
                if ("about:blank".equals(url)) {
                    onWebActionListener.setCurrentPage("");
                    onWebActionListener.setFinishedPageLoading(false);
                }else {
                    onWebActionListener.setCurrentPage(url);
                    onWebActionListener.setFinishedPageLoading(true);
                }
            }
        }
        super.onPageFinished(view, url);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
        String url = (DataUtil.isNotNull(request.getUrl()) ? request.getUrl().toString() : Const.EMPTY);
        MLog.i("shouldOverrideUrlLoading >> " + url);
        return intentCheckUrl(url, view);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        MLog.i("shouldOverrideUrlLoading >> " + url);
        return intentCheckUrl(url ,view);
    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        if (DataUtil.isNotNull(mActivity)) {
            onWebActionListener.dismissProgress();
            onWebActionListener.setFinishedPageLoading(false);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            onReceivedError(view, error.getErrorCode(), error.getDescription().toString(), request.getUrl().toString());
            MLog.i("url : " + request.getUrl().toString() + " | errorCode : " + error.getErrorCode() + " | description : " + error.getDescription().toString());
        }
    }

    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        // CONNECTION 관련에러 - 서버에 접속을 못한것임.
        if (description.contains("ERR_CONNECTION_REFUSED")
                //|| description.contains("ERR_NAME_NOT_RESOLVED") -- CSS파일이 않올라 갔을때 발생하는 에러인데.. 화면은 그려진다. 그래서 제외!
                || description.contains("ERR_CONNECTION_TIMED_OUT")
                || description.contains("ERR_INTERNET_DISCONNECTED")
                || description.contains("ERR_ADDRESS_UNREACHABLE")
                || description.contains("ERR_CONNECTION_ABORTED")
                || description.contains("ERR_TIMED_OUT")
                || description.contains("ERR_CONNECTION_RESET")) {
            mConnectAbort = true;
        }
    }

    @Override
    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
        MLog.d();
        super.onReceivedHttpError(view, request, errorResponse);
        if (DataUtil.isNotNull(mActivity)) {
            onWebActionListener.dismissProgress();
            onWebActionListener.setFinishedPageLoading(false);
        }
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        if (DataUtil.isNotNull(mActivity)) {
            onWebActionListener.dismissProgress();
            onWebActionListener.setFinishedPageLoading(false);
        }

        //super.onReceivedSslError(view, handler, error);
        //20200812-구글 권고사항으로 변경함.
        onWebActionListener.onWebViewSSLError(error.getUrl(), String.valueOf(error.getPrimaryError()),handler);

        MLog.i("onReceivedSslError >> " + error.toString());
    }

    /**
     * Intent URL 체크
     * @param url
     * @param webView
     * @return
     */
    private boolean intentCheckUrl(String url, WebView webView) {
        MLog.i("checkUrlRunStoreApp >> " + url);
        if (url != null && url.startsWith("intent:")) {
            if(url.contains("saidabank") && url.contains("webclose")){
                mActivity.finish();
                return true;
            } else if(url.contains("saidabank") && url.contains("ExtWebclose")) {
                mActivity.finish();
                return true;
            } else if(url.contains("saidabank") && url.contains("niceWebclose")){
                mActivity.setResult(Const.NICE_PREV);
                mActivity.finish();
                return true;
            } else if(url.contains("saidabank") && url.contains("niceEntr")){
                //NICE 신용정보조회 개편으로 추가 niceEntr:가입
                mActivity.setResult(Const.NICE_NEXT);
                mActivity.finish();
                return true;
            }else if(url.contains("saidabank") && url.contains("niceTrmt")){
                //NICE 신용정보조회 개편으로 추가 niceTrmt:해지
                String niceUrl = WasServiceUrl.CRD0140100.getServiceUrl();
                webView.loadUrl(niceUrl);
                return true;
            }
        }else if (url != null && url.startsWith(Const.MARKET)) {
            MLog.i("Market Url >> " + url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mActivity.getBaseContext().startActivity(intent);
            return true;
        }
        return false;
    }
}
