package com.sbi.saidabank.web;

import android.app.Activity;
import android.os.Message;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.BaseWebView;

/**
 * BaseWebChromeClient
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class BaseWebChromeClient extends WebChromeClient {

    private Activity mContext;
    private FrameLayout mWebViewContainer;
    private JavaScriptBridge mJsBridge;

    public BaseWebChromeClient(Activity activity, FrameLayout container, JavaScriptBridge bridge) {
        this.mContext = activity;
        this.mWebViewContainer = container;
        this.mJsBridge = bridge;
    }

    @Override
    public boolean onConsoleMessage(ConsoleMessage cm) {
        /**
         if (BuildConfig.DEBUG) {
         StringBuilder message;
         message = new StringBuilder("Console: ")
         .append(cm.message())
         .append(" / SourceId: ")
         .append(cm.sourceId())
         .append(" / LineNumber: ")
         .append(cm.lineNumber());
         MLog.i("onConsoleMessage >> " + message.toString());
         }
         */
        return true;
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {
        MLog.i("onJsAlert >> " + message);

//        if (BuildConfig.DEBUG && DataUtil.isNotNull(mContext)) {

            DialogUtil.alert(mContext, "jsAlert", message, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result.confirm();
                }
            });

//        }

        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {
        MLog.i("onJsConfirm >> " + message);

//        if (BuildConfig.DEBUG && DataUtil.isNotNull(mContext)) {

            DialogUtil.alert(mContext, message, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result.confirm();
                }
            }, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    result.cancel();
                }
            });

//        }

        return true;
    }

    @Override
    public void onReceivedTitle(WebView view, String title) {
        // MLog.i("onReceivedTitle >> " + title);
        super.onReceivedTitle(view, title);
    }

    @Override
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
        MLog.d();

        BaseWebView newWebView = new BaseWebView(mContext);
        int webViewCnt = mWebViewContainer.getChildCount();
        newWebView.setTag(webViewCnt + 1);

        BaseWebClient client = new BaseWebClient(mContext,mJsBridge.getWebActionListener());
        newWebView.setWebViewClient(client);

        BaseWebChromeClient chromeClient = new BaseWebChromeClient(mContext, mWebViewContainer, mJsBridge);
        newWebView.setWebChromeClient(chromeClient);
        newWebView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);

        mWebViewContainer.addView(newWebView);

        WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
        transport.setWebView(newWebView);
        resultMsg.sendToTarget();

        return true;
    }

    @Override
    public void onCloseWindow(WebView window) {
        super.onCloseWindow(window);
        MLog.i("onCloseWindow >> " + window.getTag());
        mWebViewContainer.removeView(window);    // 화면에서 제거
    }
}
