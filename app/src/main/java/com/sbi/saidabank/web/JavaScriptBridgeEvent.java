package com.sbi.saidabank.web;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.widget.FrameLayout;

import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.SaidaUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * 메인 - 내계좌의 롤링 이벤트에서 이벤트 다이얼로그에서 사용하는 브릿지이다.
 */
public class JavaScriptBridgeEvent {

    public static final String CALL_NAME = "Android";
    private static final long MIN_CLICK_INTERVAL = 600;    // ms
    private Context mContext;
    private Dialog mDialog;
    private Handler mUiHandler;
    /**
     * 20181112 - API호출이 연속으로 발생하여 앞의 업무를 처리전에 다시 호출될 경우
     * CallBackFuncName이 변경되는 문제가 발생할수 있다.
     */
    private HashMap<Integer, String> mCallbackFuncName;
    private HashMap<Integer, String> mJsonObjectParam;
    private FrameLayout mWebViewContainer;
    private long mLastClickTime;

    public JavaScriptBridgeEvent(Dialog dialog, Context context, FrameLayout webViewContainer) {
        mDialog = dialog;
        mContext = context;
        mWebViewContainer = webViewContainer;
        mUiHandler = new Handler();
        mCallbackFuncName = new HashMap<Integer, String>();
        mJsonObjectParam = new HashMap<Integer, String>();
    }

    /**
     * 콜백함수를 스택으로 구성하여 여러번 호출되어도 됨.
     */
    private void setCallbackFunc(int apiNum, String callBackName) {
        mCallbackFuncName.put(apiNum, callBackName);
        MLog.i("mCallbackFuncName size() : " + mCallbackFuncName.size());
    }

    public String getCallBackFunc(int apiNum) {
        String callBackName = mCallbackFuncName.get(apiNum);
        mCallbackFuncName.remove(apiNum);

        MLog.i("mCallbackFuncName size() : " + mCallbackFuncName.size());
        return callBackName;
    }

    public String getCallBackFunc(int apiNum, boolean clear) {
        String callBackName = mCallbackFuncName.get(apiNum);
        if (clear)
            mCallbackFuncName.remove(apiNum);

        MLog.i("mCallbackFuncName size() : " + mCallbackFuncName.size());
        return callBackName;
    }

    /*
     * Json 파라미더를 를 스택으로 구성하여 여러번 호출되어도 값을 저장하여 꺼내올수 있도록 구성.
     */
    public void setJsonObjecParam(int apiNum, String param) {
        mJsonObjectParam.put(apiNum, param);
    }

    public String getJsonObjecParam(int apiNum) {

        String param = mJsonObjectParam.get(apiNum);
        mJsonObjectParam.remove(apiNum);

        MLog.i("getJsonObjecParam size() : " + mCallbackFuncName.size());
        return param;
    }

    /**
     * 자바스크립를 실행한다.
     *
     * @param javascript 자바스크립트 코드
     */
    public void callJavascript(String javascript) {
        MLog.e("callJavascript - loadUrl : " + javascript);
        BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
        webView.loadUrl("javascript:" + javascript);
    }

    /**
     * 자바스크립트 함수를 호출한다.
     *
     * @param funcName 자바스크립트 함수명
     * @param jsonObj  함수 파라미터 목록
     */
    public void callJavascriptFunc(String funcName, JSONObject jsonObj) {

        if (TextUtils.isEmpty(funcName)) return;

        String func = "";
        if (jsonObj == null) {
            func = String.format("%s()", funcName);
        } else {
            func = String.format("%s(%s)", funcName, jsonObj);
        }

        callJavascript(func);
        MLog.e("func : " + func);
    }

    /**
     * JSON 요청 문자열에서 API 구분값을 얻어온다.
     *
     * @param json JSON 요청 문자열
     * @return API 구분
     * }
     */
    private int getApiNum(JSONObject json) {

        int api = JavaScriptApi.API_UNKNOWN;
        try {
            JSONObject header = json.getJSONObject("header");
            String apiStr = header.getString("api");
            if (TextUtils.isEmpty(apiStr)) {
                Utils.showToast(mContext, "안드로이드 호출 함수 번호가 없습니다.");
                return -1;
            } else {
                api = Integer.parseInt(apiStr);
                String callbackFuncName = header.optString("callback", "");
                setCallbackFunc(api, callbackFuncName);
            }
        } catch (NumberFormatException | JSONException e) {
            MLog.e(e);
        }

        return api;

    }

    /**
     * 네이티브 기능을 수행하기 위한 자바스크립트 인터페이스 메소드
     *
     * @param jsonEncodeString json 문자열
     *                         {"header":{
     *                         "api":"100",
     *                         "call_back":""
     *                         },
     *                         "body":{
     *                         //각 호출 함수에 맞게 파라미터값
     *                         }
     */
    @JavascriptInterface
    public void callApi(String jsonEncodeString) {

        MLog.e("callApi - jsonEncodeString : " + jsonEncodeString);

        String jsonString = JavaScriptApi.makeNewJsonString(jsonEncodeString);

        MLog.e("callApi - jsonString : " + jsonString);

        try {
            final JSONObject json = new JSONObject(jsonString);

            mUiHandler.post(new Runnable() {
                public void run() {

                    try {
                        int apiNum = getApiNum(json);
                        MLog.e("callApi - apiCode : " + apiNum);
                        switch (apiNum) {
                            //======================================================================
                            // 다이얼로그
                            //======================================================================
                            case JavaScriptApi.API_301:
                                api_301_callNativeScreen(json);
                                break;
                            case JavaScriptApi.API_914:
                                api_914_go_event(json);
                                break;
                            default:
                                break;
                        }
                    } catch (JSONException e) {
                        MLog.e(e);
                    }

                }
            });
        } catch (JSONException e) {
            MLog.e(e);
        }
    }

    /*
     * ==============================================================================================
     * 아래서부터 인터페이스 함수들...
     * ==============================================================================================
     * */

    /**
     * Alert 다이얼로그 호출 요청을 처리한다.
     *
     * @param json JSON 데이터
     * @throws JSONException }
     */
    private void api_301_callNativeScreen(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String screenName = body.getString("screen_name");

        if (TextUtils.isEmpty(screenName))
            return;

        if ("main".equals(screenName)) {

        }

        mDialog.dismiss();
    }

    private void api_914_go_event(JSONObject json) throws JSONException {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            return;
        }

        JSONObject body = json.getJSONObject("body");
        String link = body.getString("link");

        if (TextUtils.isEmpty(link))
            return;

        String url = SaidaUrl.getBaseWebUrl();
        url = url + "/" + link;
        Intent intent = new Intent(mContext, WebMainActivity.class);
        intent.putExtra("url", url);

        /*JSONObject param = null;

        if (body.has(Const.INTENT_PARAM)) {
            param = body.optJSONObject(Const.INTENT_PARAM);
        }*/

        mContext.startActivity(intent);
        mDialog.dismiss();
    }
}
