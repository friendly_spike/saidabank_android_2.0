package com.sbi.saidabank.web;

import android.webkit.SslErrorHandler;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * 웹에서 각 Activity와 Fragment와의 통신을 원활하게 하기 위해 추가
 */
public interface OnWebActionListener {

    void showProgress();
    void dismissProgress();

    void openCamera();
    void loadUrl(String url,String params);
    void refreshWebView();
    void setNeedResumeRefresh();
    void closeWebView();
    void setCurrentPage(String url);
    void setFinishedPageLoading(boolean flag);
    void onWebViewSSLError(String url, String errorCd, SslErrorHandler handler);

    void getProfileImage(JSONObject json, String workType) throws JSONException;

}
