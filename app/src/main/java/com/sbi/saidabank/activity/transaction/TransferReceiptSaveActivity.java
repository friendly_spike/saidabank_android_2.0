package com.sbi.saidabank.activity.transaction;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Saidabank_android
 * Class: TransferReceiptSaveActivity
 * Created by 950546
 * Date: 2019-01-30
 * Time: 오후 2:41
 * Description: 이체확인증 저장 화면
 */
public class TransferReceiptSaveActivity extends BaseActivity implements View.OnClickListener {
    private TransferReceiptInfo mTransferReceiptInfo;
    private String mName;
    private String mBankName;
    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;

    private String[] perList = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_receipt_image);

        Intent intent = getIntent();
        mTransferReceiptInfo = (TransferReceiptInfo) intent.getSerializableExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO);
        mName = intent.getStringExtra("name");
        mBankName = intent.getStringExtra(Const.INTENT_BANK_NM);
        initUX();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        boolean isPerDeny = false;
        if (requestCode == Const.REQUEST_PERMISSION_EXTERNAL_STORAGE) {
            for (int i = 0; i < permissions.length; i++) {
                Logs.i("permissions." + i + "." + permissions[i] + " : " + grantResults[i]);
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    String msg;

                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]);

                    if (state) {
                        msg = "해당 접근권한을 허용하셔야 이미지를 저장할 수 있습니다.";
                    } else {
                        msg = "앱 설정에서 저장소 접근권한을 허용하셔야 이미지를 저장할 수 있습니다.";
                    }

                    DialogUtil.alert(this, msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //setResult(RESULT_CANCELED, null);
                            //finish();
                        }
                    });
                    isPerDeny = true;
                    break;
                }
            }

            if (!isPerDeny) {
                saveReceiptImage();
            }
        }
    }

    @Override
    public void onClick(View view) {
        // 스샷으로 저장
        if (PermissionUtils.checkPermission(this, perList, Const.REQUEST_PERMISSION_EXTERNAL_STORAGE)) {
            saveReceiptImage();
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        TextView tvTransferAmount = findViewById(R.id.tv_transferamount);
        TextView tvFee = findViewById(R.id.tv_fee);
        TextView tvTransferDate = findViewById(R.id.tv_transferdate);
        TextView tvWithdrawAccountName = findViewById(R.id.tv_withdrawaccountname);
        TextView tvWithdrawAccount = findViewById(R.id.tv_withdrawaccount);
        TextView tvWithdrawName = findViewById(R.id.tv_withdrawname);
        TextView tvDepositAccountName = findViewById(R.id.tv_depositccountname);
        TextView tvDepositAccount = findViewById(R.id.tv_depositccount);
        TextView tvDepositName = findViewById(R.id.tv_depositname);
        TextView tvDisplayRecipient = findViewById(R.id.tv_displayrecipient);
        TextView tvDisplaySending = findViewById(R.id.tv_displaysending);

        //TODO:transferReceiptInfos로 받은 이체 자료 셋팅
        if (mTransferReceiptInfo == null) {
            Logs.e("mTransferReceiptInfo is null");
            return;
        }
		
        String TRN_AMT = mTransferReceiptInfo.getTRN_AMT();
        if (!TextUtils.isEmpty(TRN_AMT)) {
            TRN_AMT = Utils.moneyFormatToWon(Double.valueOf(TRN_AMT));
            tvTransferAmount.setText(TRN_AMT);
        }

        String TRN_FEE = mTransferReceiptInfo.getTRN_FEE();
        if (!TextUtils.isEmpty(TRN_FEE)) {
            TRN_FEE = Utils.moneyFormatToWon(Double.valueOf(TRN_FEE));
            tvFee.setText(TRN_FEE + " " + getString(R.string.won));
        }

        String TRNF_DT = mTransferReceiptInfo.getTRNF_DT();
        String TRNF_MT = mTransferReceiptInfo.getTRNF_TM();
        if (!TextUtils.isEmpty(TRNF_DT) && !TextUtils.isEmpty(TRNF_MT) ) {
            String year = TRNF_DT.substring(0, 4);
            String month = TRNF_DT.substring(4, 6);
            String day = TRNF_DT.substring(6, 8);
            String time = TRNF_MT.substring(0, 2);
            String min = TRNF_MT.substring(2, 4);
            String sec = TRNF_MT.substring(4, 6);

            String date = year + "." + month + "." + day + " " + time + ":" + min + ":" + sec;
            tvTransferDate.setText(date);
        }

        tvWithdrawAccountName.setText(mBankName);

        String WTCH_ACNO = mTransferReceiptInfo.getWTCH_ACNO();
        if (!TextUtils.isEmpty(WTCH_ACNO)) {
            String account = WTCH_ACNO.substring(0, 5) + "-" + WTCH_ACNO.substring(5, 7) + "-" + WTCH_ACNO.substring(7, WTCH_ACNO.length());
            tvWithdrawAccount.setText(account);
        }

        if (!TextUtils.isEmpty(mName))
            tvWithdrawName.setText(mName);

        String bankname = mTransferReceiptInfo.getMNRC_BANK_NM();
        if (!TextUtils.isEmpty(bankname)) {
            tvDepositAccountName.setText(bankname);
        }

        String account = mTransferReceiptInfo.getCNTP_BANK_ACNO();
        if (!TextUtils.isEmpty(account)) {
            String CNTP_FIN_INST_CD = mTransferReceiptInfo.getCNTP_FIN_INST_CD();
            if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD) || "028".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                account = account.substring(0, 5) + "-" + account.substring(5, 7) + "-" + account.substring(7, account.length());
            }
            tvDepositAccount.setText(account);
        }

        String CNTP_ACCO_DEPR_NM = mTransferReceiptInfo.getCNTP_ACCO_DEPR_NM();
        if (!TextUtils.isEmpty(CNTP_ACCO_DEPR_NM))
            tvDepositName.setText(CNTP_ACCO_DEPR_NM);

        String WTCH_ACCO_MRK_CNTN = mTransferReceiptInfo.getWTCH_ACCO_MRK_CNTN();
        if (!TextUtils.isEmpty(WTCH_ACCO_MRK_CNTN))
            tvDisplayRecipient.setText(WTCH_ACCO_MRK_CNTN);

        String MNRC_ACCO_MRK_CNTN = mTransferReceiptInfo.getMNRC_ACCO_MRK_CNTN();
        if (!TextUtils.isEmpty(MNRC_ACCO_MRK_CNTN))
            tvDisplaySending.setText(MNRC_ACCO_MRK_CNTN);

        findViewById(R.id.btn_download).setOnClickListener(this);

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void saveReceiptImage() {
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!storageDir.exists()) {
            storageDir.mkdir();
        }
        String mPath = Utils.getCurrentDate("") + Utils.getCurrentTime("") + "_이체확인증" + ".jpg";
        View screenshotView = findViewById(R.id.ll_root);
        screenshotView.buildDrawingCache();
        Bitmap dialogBmp = screenshotView.getDrawingCache();

        File imageFile = new File(storageDir, mPath);

        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(imageFile);

            dialogBmp.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
            outputStream.flush();
            outputStream.close();
            Logs.showToast(this, "이미지가 저장되었습니다.");

            Intent share = new Intent(Intent.ACTION_SEND);
            Uri uri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                uri = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".imagefileprovider", imageFile);
                share.setDataAndType(Uri.fromFile(imageFile), "application/vnd.android.package-archive");
            } else {
                uri = Uri.fromFile(imageFile);
            }
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
            share.setType("image/jpg");
            share.putExtra(Intent.EXTRA_STREAM, uri);
            startActivity(Intent.createChooser(share, "이체확인증 공유"));
        } catch (FileNotFoundException e) {
            Logs.printException(e);
            Logs.showToast(this, "이미지 저장이 실패했습니다.");
        } catch (IOException e) {
            Logs.printException(e);
            Logs.showToast(this, "이미지 저장이 실패했습니다.");
        }
    }
}
