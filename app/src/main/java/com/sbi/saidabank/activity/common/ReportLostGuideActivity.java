package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.fds.FDSManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * siadabank_android
 * Class: ReportLostGuideActivity
 * Created by 950546
 * Date: 2018-12-20
 * Time: 오후 5:41
 * Description:ReportLostGuideActivity
 */
public class ReportLostGuideActivity extends BaseActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_lost_guide);
        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_confirm) {
            requestLostRelease();
        }
    }

    private void requestLostRelease() {

        Map param = new HashMap();
        param.put("DEVICE_OS", "android");
        param.put("DEVICE_INFO1", FDSManager.getInstance().getEncIMEI(ReportLostGuideActivity.this));
        param.put("DEVICE_INFO2", FDSManager.getInstance().getEncUUID(ReportLostGuideActivity.this));
        param.put("DEVICE_INFO3", FDSManager.getInstance().getEncIMEI121(ReportLostGuideActivity.this));
        param.put("TRMN_APSF_VRSN", Utils.getVersionName(ReportLostGuideActivity.this));
        param.put("TRMN_OPSY_VRSN", Build.VERSION.RELEASE);
        param.put("CMCM_NM", Utils.getOperator(ReportLostGuideActivity.this));

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A07.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String EROR_TCNT_EXCS_YN = object.optString("EROR_TCNT_EXCS_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(EROR_TCNT_EXCS_YN)) {
                        DialogUtil.alert(ReportLostGuideActivity.this,
                                getResources().getString(R.string.txt_reportlost_callcenter),
                                getResources().getString(R.string.common_cancel),
                                getResources().getString(R.string.msg_reportlost_callcenter),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Utils.makeCall(ReportLostGuideActivity.this, "16700042");
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                    } else {
                        Intent intent = new Intent(ReportLostGuideActivity.this, OtpPwAuthActivity.class);
                        CommonUserInfo commonUserInfo = new CommonUserInfo();
                        commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                        commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_LOSTPIN);
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}
