package com.sbi.saidabank.activity.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.orhanobut.dialogplus.DialogPlus;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.login.IdentificationPrepareActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: AnotherBankAccountVerifyActivity
 * Created by 950485 on 2018. 11. 13..
 * <p>
 * Description:타행이체 확인을 위한 화면
 */

public class AnotherBankAccountVerifyActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged {
    private static final int REQUEST_CONFIRM_WEB = 20000;
    private static final int SCROLL_EDITTEXT        = 0;
    private static final int SCROLL_CHECK_KEYBOARD  = 1;

    private RelativeLayout mLayoutScreen;
    private TextView       mTextName;
    private TextView       mTextBankName;
    private EditText       mEditAccountNum;
    private Button         mBtnConfirm;
    private ScrollView     mSvScview;

    private CommonUserInfo mCommonUserInfo;

//->20210113-951118-은행/증권사 코드조회 방식 변경
//    private ArrayList<RequestCodeInfo>       mListBank = new ArrayList<>();
//    private ArrayList<RequestCodeInfo>       mListStock = new ArrayList<>();

    private String     mBankStockCode;
    private String     mAccountNum;
    private String     mCustName;
    private String     mBzwrDvcd;
    private String     mPropNo;

    private DialogPlus dialogBankPlus;
    private SlidingExpandBankStockDialog dialogBankStock;
    private InputMethodManager imm = null;

    private WeakHandler mScrollHandler;
    private int mScrollOffset;
    private boolean mIsShownKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_another_bank_account_verify);

        getExtra();
        initUX();

//->20210113-951118-은행/증권사 코드조회 방식 변경
//        requestBankList(false);
//        requestStockList(false);
    }

    @Override
    public void onBackPressed() {
        if (dialogBankPlus!= null && dialogBankPlus.isShowing()) {
            dialogBankPlus.dismiss();
            dialogBankPlus = null;
            return;
        }

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            Intent intent = new Intent();
            intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
            setResult(RESULT_OK, intent);
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
            showCancelMessage(getResources().getString(R.string.msg_cancel_pin_rereg));
            return;
        } else {
            Intent intent = new Intent(AnotherBankAccountVerifyActivity.this, IdentificationPrepareActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        }
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CONFIRM_WEB :
                    Intent intent = new Intent();
                    intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_TRUE);
                    intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_NUMBER, mAccountNum);
                    intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_BANK_CODE, mBankStockCode);
                    intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_CUST_NAME, mCustName);
                    setResult(RESULT_OK, intent);
                    finish();
                break;
                default:
                    break;
            }
        } else if (requestCode == RESULT_CANCELED) {
            switch (requestCode) {
                case REQUEST_CONFIRM_WEB :
                    Intent intent = new Intent();
                    intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    setResult(RESULT_OK, intent);
                    finish();
                    break;
                default:
                    break;
            }
        } else if (resultCode == Const.VERIFY_ANOTHER_ACCOUNT) {
            mEditAccountNum.clearFocus();
            mTextBankName.setText("");
            mEditAccountNum.setText("");
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        Intent intent = getIntent();
        mCommonUserInfo = intent.getParcelableExtra(Const.INTENT_COMMON_INFO);

        if (intent.hasExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD))
            mBzwrDvcd = intent.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD);

        if (intent.hasExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO))
            mPropNo = intent.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mLayoutScreen = (RelativeLayout) findViewById(R.id.ll_root);
        mLayoutScreen.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                imm.hideSoftInputFromWindow(mEditAccountNum.getWindowToken(), 0);
            }
        });
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mScrollHandler = new WeakHandler(this);
        mScrollOffset = (int)Utils.dpToPixel(this, (float)30f);
        mTextName = (TextView) findViewById(R.id.edittext_name_verify_account);
        mTextBankName = (TextView) findViewById(R.id.textview_bank_name_verify_account);
        mBtnConfirm = (Button) findViewById(R.id.btn_ok_verify_account);
        mEditAccountNum = (EditText) findViewById(R.id.edittext_account_verify_account);
        mEditAccountNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(mTextBankName.getText().toString()) &&
                     editable.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                    mBtnConfirm.setEnabled(true);
                } else {
                    mBtnConfirm.setEnabled(false);
                }
            }
        });
        mEditAccountNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if  (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });
        mEditAccountNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });
        mBtnConfirm.setEnabled(false);

        RelativeLayout btnBank = (RelativeLayout) findViewById(R.id.layout_select_bank_verify_account);
        btnBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//->20210113-951118-은행/증권사 코드조회 방식 변경
//                if (mListBank.size() <= 0) {
//                    requestBankList(true);
//                    return;
//                }
//
//                if (mListStock.size() <= 0) {
//                    requestStockList(true);
//                    return;
//                }

                //showBankStockList();
                showBankStockDialog();
            }
        });

        if (mCommonUserInfo != null && !TextUtils.isEmpty(mCommonUserInfo.getName()))
            mTextName.setText(mCommonUserInfo.getName());

        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_verify_account);
        if (mCommonUserInfo != null && mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL)
            btnCancel.setText(getResources().getString(R.string.common_close));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //20200805 - back key와 같은 동작하도록 일원화한다.
                onBackPressed();
                /*
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
                    Intent intent = new Intent();
                    intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    setResult(RESULT_OK, intent);
                } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
                    showCancelMessage(getResources().getString(R.string.msg_cancel_pin_rereg));
                    return;
                } else {
                    Intent intent = new Intent(AnotherBankAccountVerifyActivity.this, IdentificationPrepareActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                }
                finish();
                 */
            }
        });

        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                //상호저축은행 선택 후 특정 계좌번호 입력 시 자행계좌 입력 불가 처리
                boolean isSBIAccount = false;
                if ("050".equals(mBankStockCode)) {
                    String accountNum = mEditAccountNum.getText().toString();
                    if (TextUtils.isEmpty(accountNum) || accountNum.length() < 3) {
                        isSBIAccount = false;
                    } else if (accountNum.startsWith("028") || accountNum.startsWith("023") || accountNum.startsWith("406") || accountNum.startsWith("621")) {
                       isSBIAccount = true;
                    }
                }
                if (isSBIAccount) {
                    DialogUtil.alert(AnotherBankAccountVerifyActivity.this,  "SBI저축은행 계좌인증은 불가합니다. 본인명의의 타행계좌번호를 입력해주세요.",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                } else {
                    requestTransferAccount();
                }
            }
        });

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(this);
    }

//->20210113-951118-은행/증권사 코드조회 방식 변경
//    /**
//     * 은행 조회
//     * @param isShowProgress 진행바 노출여부 (최초 진입시 선행으로 실행 위해 포함)
//     */
//    private void requestBankList(final boolean isShowProgress) {
//        Map param = new HashMap();
//        param.put("LCCD", "BANK_CD");
//        if (isShowProgress)
//            showProgressDialog();
//
//        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
//            @Override
//            public void endHttpRequest(String ret) {
//                if (isShowProgress)
//                    dismissProgressDialog();
//                Logs.i("LCCD : " + ret);
//                if (TextUtils.isEmpty(ret)) {
//                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
//                    return;
//                }
//
//                try {
//                    JSONObject object = new JSONObject(ret);
//                    if (object == null) {
//                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
//                        Logs.e(ret);
//                        return;
//                    }
//
//                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
//                    if (objectHead == null) {
//                        return;
//                    }
//
//                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
//                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
//                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
//                        if (TextUtils.isEmpty(msg))
//                            msg = getString(R.string.common_msg_no_reponse_value_was);
//
//                        Logs.e("error msg : " + msg + ", ret : " + ret);
//                        return;
//                    }
//
//                    Prefer.setBankList(AnotherBankAccountVerifyActivity.this, ret);
//
//                    JSONArray array = object.optJSONArray("REC");
//                    if (array == null) return;
//
//                    for (int index = 0; index < array.length(); index++) {
//                        JSONObject jsonObject = array.getJSONObject(index);
//                        if (jsonObject == null)
//                            continue;
//
//                        RequestCodeInfo item = new RequestCodeInfo(jsonObject);
//                        //타행이체인 경우 SBI은행은 리스트에서 제외
//                        if ("028".equals(item.getSCCD()) || "000".equals(item.getSCCD()))
//						    continue;
//
//                        mListBank.add(item);
//                    }
//                } catch (JSONException e) {
//                    Logs.printException(e);
//                }
//            }
//        });
//    }
//
//    /**
//     * 증권사 조회
//     * @param isShowProgress 진행바 노출여부 (최초 진입시 선행으로 실행 위해 포함)
//     */
//    private void requestStockList(final boolean isShowProgress) {
//        Map param = new HashMap();
//        param.put("LCCD", "SCCM_CD");
//        if (isShowProgress)
//            showProgressDialog();
//        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
//            @Override
//            public void endHttpRequest(String ret) {
//                if (isShowProgress)
//                    dismissProgressDialog();
//
//                Logs.i("LCCD : " + ret);
//                if (TextUtils.isEmpty(ret)) {
//                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
//                    return;
//                }
//
//                try {
//                    JSONObject object = new JSONObject(ret);
//                    if (object == null) {
//                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
//                        Logs.e(ret);
//                        return;
//                    }
//
//                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
//                    if (objectHead == null) {
//                        return;
//                    }
//
//                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
//                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
//                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
//                        if (TextUtils.isEmpty(msg))
//                            msg = getString(R.string.common_msg_no_reponse_value_was);
//
//                        Logs.e("error msg : " + msg + ", ret : " + ret);
//                        return;
//                    }
//
//                    Prefer.setStockList(AnotherBankAccountVerifyActivity.this, ret);
//
//                    JSONArray array = object.optJSONArray("REC");
//                    if (array == null) return;
//
//                    for (int index = 0; index < array.length(); index++) {
//                        JSONObject jsonObject = array.getJSONObject(index);
//                        if (jsonObject == null)
//                            continue;
//
//                        RequestCodeInfo item = new RequestCodeInfo(jsonObject);
//                        mListStock.add(item);
//                    }
//                } catch (JSONException e) {
//                    Logs.printException(e);
//                }
//            }
//        });
//    }

    private void showBankStockDialog(){
        if (this.isFinishing())
            return;

        if(dialogBankStock != null && dialogBankStock.isShowing())
            return;

        if (mEditAccountNum.isFocused()) {
            mEditAccountNum.clearFocus();
            Utils.hideKeyboard(AnotherBankAccountVerifyActivity.this, mEditAccountNum);
        }

        dialogBankStock = DialogUtil.bankStock(AnotherBankAccountVerifyActivity.this,true,new SlidingExpandBankStockDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bank, String code) {
                Logs.e("onSelectListener");
                mTextBankName.setText(bank);
                mBankStockCode = code;

                mEditAccountNum.setText("");
                mEditAccountNum.requestFocus();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Utils.showKeyboard(AnotherBankAccountVerifyActivity.this, mEditAccountNum);
                    }
                }, 500);
            }
        });
        if(DataUtil.isNotNull(dialogBankStock)){
            dialogBankStock.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    dialogBankStock = null;
                }
            });
            dialogBankStock.show();
        }
    }

//->20210113-951118-은행/증권사 코드조회 방식 변경
//    /**
//     * 은행/증권사 리스트 표시
//     */
//    private void showBankStockList() {
//        if (this.isFinishing())
//            return;
//
//        if (mEditAccountNum.isFocused()) {
//            mEditAccountNum.clearFocus();
//            Utils.hideKeyboard(AnotherBankAccountVerifyActivity.this, mEditAccountNum);
//        }
//
//        final BankStockAdapter adapter = new BankStockAdapter(this, mListBank, mListStock);
//
//        Holder holder = new ListHolder();
//
//        View viewHeader = getLayoutInflater().inflate(R.layout.dialog_backstock_header, null);
//        TextView textTitle = viewHeader.findViewById(R.id.textview_dialog_slide_header);
//        textTitle.setText(getString(R.string.title_bank_stock));
//
//        OnItemClickListener itemClickListener = new OnItemClickListener() {
//            @Override
//            public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
//                if (position < 1) return;
//
//                RequestCodeInfo codeInfo = null;
//                if (adapter.getTabState() == Const.BANK_STOCK_MODE.BANK_MODE)
//                    codeInfo = (RequestCodeInfo) mListBank.get(position - 1);
//                else if (adapter.getTabState() == Const.BANK_STOCK_MODE.STOCK_MODE)
//                    codeInfo = (RequestCodeInfo) mListStock.get(position - 1);
//
//                if (codeInfo == null)
//                    return;
//
//                String bankStockName = codeInfo.getCD_NM();
//                mTextBankName.setText(bankStockName);
//                mBankStockCode = codeInfo.getSCCD();
//
//                String accountNum = mEditAccountNum.getText().toString();
//                if (!TextUtils.isEmpty(accountNum) && accountNum.length() >= 5)
//                    mBtnConfirm.setEnabled(true);
//                else
//                    mBtnConfirm.setEnabled(false);
//
//                mEditAccountNum.setText("");
//                mEditAccountNum.requestFocus();
//
//                dialog.dismiss();
//            }
//        };
//
//        OnDismissListener dismissListener = new OnDismissListener() {
//            @Override
//            public void onDismiss(DialogPlus dialog) {
//                if (mEditAccountNum.isFocused()) {
//                    Utils.showKeyboard(AnotherBankAccountVerifyActivity.this, mEditAccountNum);
//                }
//                dialogBankPlus = null;
//            }
//        };
//
//        float pixel = Utils.dpToPixel(this, 245 + 68 + 45 + 14);
//
//        dialogBankPlus = DialogPlus.newDialog(this)
//                                        .setContentHolder(holder)
//                                        .setAdapter(adapter)
//                                        .setCancelable(true)
//                                        .setExpanded(true, (int) pixel)
//                                        .setHeader(viewHeader)
//                                        .setOnItemClickListener(itemClickListener)
//                                        .setOnDismissListener(dismissListener)
//                                        .create();
//        dialogBankPlus.show();
//    }

    /**
     * 실명인증 이체 요청
     */
    private void requestTransferAccount() {
        String memberNum;
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            memberNum = LoginUserInfo.getInstance().getMBR_NO();
        } else {
            memberNum = mCommonUserInfo.getMBRnumber();
        }

        if (TextUtils.isEmpty(memberNum)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mEditAccountNum.requestFocus();
                }
            };
            showErrorMessage(getString(R.string.msg_no_member_input), okClick);
            mBtnConfirm.setEnabled(false);
            return;
        }

        String accountNum = mEditAccountNum.getText().toString();
        mAccountNum = accountNum;
        if (TextUtils.isEmpty(accountNum)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mEditAccountNum.requestFocus();
                }
            };
            showErrorMessage(getString(R.string.msg_no_account_input), okClick);
            mBtnConfirm.setEnabled(false);
            return;
        }

        if (TextUtils.isEmpty(mBankStockCode)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mEditAccountNum.requestFocus();
                }
            };
            showErrorMessage(getString(R.string.msg_no_bankstock_input), okClick);
            mBtnConfirm.setEnabled(false);
            return;
        }

        Map param = new HashMap();
        param.put("MBR_NO", memberNum);
        param.put("MNRC_BANK_CD", mBankStockCode);
        param.put("MNRC_ACNO", accountNum);

        String userName = "";
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            userName = LoginUserInfo.getInstance().getCUST_NM();
        } else {
             userName = mCommonUserInfo.getName();
        }
        mCustName = userName;

        if (TextUtils.isEmpty(userName))
            return;

        param.put("RECV_NM", userName);
        param.put("NN_MEET_BZWR_DVCD", mBzwrDvcd);
        param.put("PROP_NO", mPropNo);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010500A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("LCCD : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        String msg = getString(R.string.common_msg_no_reponse_value_was);
                        showErrorMessage(msg);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                mEditAccountNum.requestFocus();
                            }
                        };
                        showCommonErrorDialog(msg, errCode, "",  objectHead, true, okClick);
                        Logs.e(ret);
                        return;
                    }

                    String athnSqno = object.optString("ATHN_SQNO");
                    Logs.e("ret : " + ret);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(athnSqno) || TextUtils.isEmpty(athnSqno)) {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mEditAccountNum.requestFocus();
                            }
                        };
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), okClick);
                        return;
                    }

                    showCompleteMessage(athnSqno);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 정상 완료 메세지 출력
     */
    private void showCompleteMessage(final String athnSqno) {
        String bankName = mTextBankName.getText().toString();
        String accountNumber = mEditAccountNum.getText().toString();

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert_verify_account);
        View view = dialog.getWindow().getDecorView();
        view.setBackgroundResource(android.R.color.transparent);

        Button btnOK = dialog.findViewById(R.id.btn_ok_alert_verify_account);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(AnotherBankAccountVerifyActivity.this, AnotherBankAccountConfirmActivity.class);
                mCommonUserInfo.setBankName(mTextBankName.getText().toString());
                mCommonUserInfo.setBankAccount(mEditAccountNum.getText().toString());
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_SQNO, athnSqno);
                intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD, mBzwrDvcd);
                intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO, mPropNo);

                if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
                    mTextBankName.setText("");
                    mEditAccountNum.setText("");
                    startActivityForResult(intent, REQUEST_CONFIRM_WEB);
                    overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                    return;
                }

                startActivity(intent);
                overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                finish();
            }
        });

        TextView textMsg01 = dialog.findViewById(R.id.textview_msg_verify_msg_01);
        String msg01 = bankName + " " + accountNumber;
        textMsg01.setText(msg01);

        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.width = size.x;
        dialog.getWindow().setAttributes(lp);

        dialog.setCancelable(false);
        dialog.show();
    }

    @Override
    public void onKeyboardShown() {
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 500);
        mBtnConfirm.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        mIsShownKeyboard = false;
        mBtnConfirm.setVisibility(View.VISIBLE);
    }

    private class WeakHandler extends Handler {
        private WeakReference<AnotherBankAccountVerifyActivity> mWeakActivity;

        WeakHandler(AnotherBankAccountVerifyActivity weakactivity) {
            mWeakActivity = new WeakReference<AnotherBankAccountVerifyActivity>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            AnotherBankAccountVerifyActivity weakactivity = mWeakActivity.get();
            if (weakactivity != null) {
                switch (msg.what) {
                    case SCROLL_EDITTEXT: {
                        if (!mIsShownKeyboard)
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollOffset);
                        break;
                    }
                    case SCROLL_CHECK_KEYBOARD: {
                        mIsShownKeyboard = true;
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}