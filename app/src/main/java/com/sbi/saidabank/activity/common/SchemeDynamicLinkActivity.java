package com.sbi.saidabank.activity.common;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

public class SchemeDynamicLinkActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        MLog.d();
        super.onStart();
        Logs.e("SchemeDynamicLinkActivity");

        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        Uri deepLink = null;
                        if (pendingDynamicLinkData != null) {
                            deepLink = pendingDynamicLinkData.getLink();
                        }

                        Logs.e("getDynamicLink:onSuccess : " + deepLink.getPath());

                        String urlStr = deepLink.getPath();

                        if(!TextUtils.isEmpty(urlStr)){
                            SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
                            Activity main2Activity = mApplicationClass.getOpenActivity("Main2Activity");

                            Logs.e("getDynamicLink:LoginUserInfo.getInstance().isLogin() : " + LoginUserInfo.getInstance().isLogin());
                            if(main2Activity != null || LoginUserInfo.getInstance().isLogin()){
                                if(urlStr.contains("main")){
                                    //메인만 남기고 모두 종료한다.
                                    mApplicationClass.allActivityFinish(false);
                                }else{
                                    Intent intent = new Intent(SchemeDynamicLinkActivity.this, WebMainActivity.class);
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, urlStr);
                                    startActivity(intent);
                                }
                            }else{
                                mApplicationClass.allActivityFinish(true);
                                Intent intent = new Intent(SchemeDynamicLinkActivity.this, IntroActivity.class);
                                if(!urlStr.contains("main"))
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, urlStr);
                                startActivity(intent);
                            }
                        }
                        finish();

                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Logs.e("getDynamicLink:onFailure : " + e);
                        finish();
                    }
                });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
