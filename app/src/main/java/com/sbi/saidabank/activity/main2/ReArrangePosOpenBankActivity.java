package com.sbi.saidabank.activity.main2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.main2.rearrange.OnOpenBankItemDragListener;
import com.sbi.saidabank.activity.main2.rearrange.ReArrangeOpenBankAdapter;
import com.sbi.saidabank.activity.main2.rearrange.SimpleItemTouchHelperCallback;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarException;


public class ReArrangePosOpenBankActivity extends BaseActivity implements View.OnClickListener, OnOpenBankItemDragListener {

    private ArrayList<OpenBankDataMgr.OpenBankGroupInfo> mListGroup;
    private ArrayList<ItemTouchHelper> mListItemTouchHelper;
    private ArrayList<ReArrangeOpenBankAdapter> mListOpenBankAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2_openbank_rearrange);

        //종료 X버튼
        findViewById(R.id.btn_close).setOnClickListener(this);
        findViewById(R.id.btn_save).setOnClickListener(this);

        //초기화버튼
        LinearLayout layoutReset = findViewById(R.id.layout_reset);
        layoutReset.setOnClickListener(this);

        int radius = (int) Utils.dpToPixel(this,12);
        layoutReset.setBackground(GraphicUtils.getRoundCornerDrawable("#00000000",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));


        //그룹헤더와 그룹별 계좌리스트를 추가하기 위한 레이아웃.
        LinearLayout mLayoutScrollViewItem = findViewById(R.id.layout_scrollview_item);

        mListGroup = new ArrayList<OpenBankDataMgr.OpenBankGroupInfo>();
        mListGroup.addAll(OpenBankDataMgr.getInstance().getCopyGroupList());

        mListItemTouchHelper = new ArrayList<ItemTouchHelper>();
        mListOpenBankAdapter = new ArrayList<ReArrangeOpenBankAdapter>();
        for(int i = 0; i< mListGroup.size(); i++){

            View groupView = getLayoutInflater().inflate(R.layout.layout_main2_openbank_rearrange_scrollview_item, null);

            //그룹헤더-그룹이름 그룹갯수
            ((TextView)groupView.findViewById(R.id.tv_group_name)).setText(mListGroup.get(i).getGroupName());
            ((TextView)groupView.findViewById(R.id.tv_group_cnt)).setText(String.valueOf(mListGroup.get(i).getGroupAccCnt()));

            //계좌리스트를 작성한다.
            ReArrangeOpenBankAdapter adapter = new ReArrangeOpenBankAdapter(this,i,mListGroup.get(i).getAccountList(),this);
            RecyclerView listView = groupView.findViewById(R.id.recyclerview);
            listView.setAdapter(adapter);
            listView.setLayoutManager(new LinearLayoutManager(this));

            //스크롤뷰 내부 레이아웃에 뷰 추가.
            mLayoutScrollViewItem.addView(groupView);

            //추후 사용을 위해 어텝터를 리스트에 넣어둔다.
            mListOpenBankAdapter.add(adapter);

            //드래그앤 드랍의 콜백을 등록하여 가지고 있는다.
            ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapter);
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(callback);
            itemTouchHelper.attachToRecyclerView(listView);
            mListItemTouchHelper.add(itemTouchHelper);
        }

    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_close:
                finish();
                break;
            case R.id.btn_save:
                try {
                    reqeustUploadReArrange();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.layout_reset:
                mListGroup.clear();
                mListGroup.addAll(OpenBankDataMgr.getInstance().getCopyGroupList());

                for(int i=0;i<mListGroup.size();i++){
                    mListOpenBankAdapter.get(i).setAccountList(mListGroup.get(i).getAccountList());
                }
                break;
        }
    }

    private void reqeustUploadReArrange() throws JSONException {
        int index=0;
        JSONArray jsonArray = new JSONArray();
        for(int i=0;i<mListGroup.size();i++){
            ArrayList<OpenBankAccountInfo> accountInfoArrayList = mListGroup.get(i).getAccountList();
            for(int k=0;k<accountInfoArrayList.size();k++){
                index++;
                OpenBankAccountInfo accountInfo = accountInfoArrayList.get(k);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ACNO",accountInfo.ACNO);
                jsonObject.put("ACNO_SQNO",index);

                Logs.e("acc name : " + accountInfo.BANK_NM);
                jsonArray.put(jsonObject);
            }
        }

        Map param = new HashMap();
        param.put("REC", jsonArray);
        param.put("REP_CNT", index);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    return;
                }

                DialogUtil.alert(ReArrangePosOpenBankActivity.this, "설정한 순서로 저장되었습니다.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });

            }
        });

    }

    @Override
    public void onStartDrag(int groupPos, ReArrangeOpenBankAdapter.OpenBankAccountHolder viewHolder) {
        mListItemTouchHelper.get(groupPos).startDrag(viewHolder);
    }

    @Override
    public void onEndDrag(int groupPos, ArrayList<?> list) {
        mListGroup.get(groupPos).getAccountList().clear();
        mListGroup.get(groupPos).getAccountList().addAll((ArrayList<OpenBankAccountInfo>)list);

    }
}