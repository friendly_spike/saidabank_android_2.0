package com.sbi.saidabank.activity.main2.common.tab;

import android.app.Activity;
import android.text.TextUtils;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.activity.main2.Main2CoupleFragment;
import com.sbi.saidabank.activity.main2.Main2MyAccountEmptyFragment;
import com.sbi.saidabank.activity.main2.Main2MyAccountFragment;
import com.sbi.saidabank.activity.main2.Main2CoupleWebFragment;
import com.sbi.saidabank.activity.main2.Main2OpenBankFragment;
import com.sbi.saidabank.activity.main2.Main2OpenBankWebFragment;
import com.sbi.saidabank.activity.main2.Main2TabWebFragment;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import java.io.File;


public class TabsPagerMainAdapter extends FragmentStatePagerAdapter {
	static final String TAG = "TabsPagerAdapter";
	Activity mActivity;

	private CustomTabLayout mCustomTabLayout;

	private Fragment mFragmentMyAccountEmpty;
	private Fragment mFragmentMyAccount;
	private Fragment mFragmentCouple;
	private Fragment mFragmentCoupleWeb;
	private Fragment mFragmentOpenBank;
	private Fragment mFragmentOpenBankWeb;
	private Fragment mFragmentGoods;
	private Fragment mFragmentMy;
	private Fragment mFragmentNoti;
	private Fragment mFragmentMenu;



	public TabsPagerMainAdapter(Main2Activity activity, FragmentManager fm) {
		super(fm,BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
		mActivity = activity;
	}
	
	@Override
    public Fragment getItem(int index) {

		Logs.e("TabsPagerMainAdapter - getItem index :" + index);
		switch (index){
			case Const.MAIN2_INDEX_HOME: {
				Logs.e("TabsPagerMainAdapter - mCustomTabLayout.getSelectHomeMenu() :" + mCustomTabLayout.getSelectHomeMenuIdx());
				switch (mCustomTabLayout.getSelectHomeMenuIdx()){
					case 0://내계좌
						return getFragmentHomeMyAccountTab();
					case 1://커플통장
						return getFragmentHomeCoupleTab();
					case 2://오픈뱅킹
						return getFragmentHomeOpenBankTab();
				}
				break;
			}
			case Const.MAIN2_INDEX_GOODS: {
				String url = WasServiceUrl.MAI0030200.getServiceUrl();
				Logs.e("TabsPagerMainAdapter - getItem - url : " + url);
				if (mFragmentGoods == null) {
					mFragmentGoods = Main2TabWebFragment.newInstance(url, index);
				}
				return mFragmentGoods;
			}
			case Const.MAIN2_INDEX_MY: {
				String url = WasServiceUrl.MAI0041100.getServiceUrl();
				Logs.e("TabsPagerMainAdapter - getItem - url : " + url);
				if (mFragmentMy == null) {
					mFragmentMy = Main2TabWebFragment.newInstance(url, index);
				}
				return mFragmentMy;
			}
			case Const.MAIN2_INDEX_NOTI: {
				String url = WasServiceUrl.MAI0060200.getServiceUrl();
				Logs.e("TabsPagerMainAdapter - getItem - url : " + url);
				if (mFragmentNoti == null) {
					mFragmentNoti = Main2TabWebFragment.newInstance(url, index);
				}
				return mFragmentNoti;
			}
			case Const.MAIN2_INDEX_MENU: {
				String url = WasServiceUrl.MAI0020200.getServiceUrl();
				Logs.e("TabsPagerMainAdapter - getItem - url : " + url);
				if (mFragmentMenu == null) {
					mFragmentMenu = Main2TabWebFragment.newInstance(url, index);
				}
				return mFragmentMenu;
			}
		}

        return null;
    }
 
    @Override
    public int getCount() {
        return Const.MAIN2_INDEX_MAX;
    }
    
    @Override
    public int getItemPosition(Object object) {
		if(object instanceof Main2TabWebFragment){
			return POSITION_UNCHANGED;
		}
        return POSITION_NONE;
    }

	public void setCustomTabLayout(CustomTabLayout tab){
		mCustomTabLayout = tab;
	}

	private Fragment getFragmentHomeMyAccountTab(){

		if((LoginUserInfo.getInstance().getMyAccountInfoArrayList().size()==0) && Main2DataInfo.getInstance().getMain2Main2AccountInfo().size() == 0){
			if (mFragmentMyAccountEmpty == null) {
				mFragmentMyAccountEmpty = Main2MyAccountEmptyFragment.newInstance();
			}
			return mFragmentMyAccountEmpty;
		}else{
			if (mFragmentMyAccount == null) {
				mFragmentMyAccount = Main2MyAccountFragment.newInstance();
			}
			return mFragmentMyAccount;
		}
	}

	private Fragment getFragmentHomeCoupleTab(){
		//내가 공유자로 커플통장이 연결되어 있는 상태
		String SHRN_PRGS_STCD = Main2DataInfo.getInstance().getSHRN_PRGS_STCD();
		if(DataUtil.isNotNull(SHRN_PRGS_STCD) && SHRN_PRGS_STCD.equals("03")){
			if (mFragmentCouple == null) {
				mFragmentCouple = Main2CoupleFragment.newInstance();
			}
			return mFragmentCouple;
		}

		//내가 계좌주로 커플통장이 연결되어 있는 상태
		if(LoginUserInfo.getInstance().getMyAccountInfoArrayList().size()>0){
			if(!TextUtils.isEmpty(Main2DataInfo.getInstance().getSHRP_NM())){//공유자 이름이 있다면 커플통장이 연결된 상태인것이다.
				if (mFragmentCouple == null) {
					mFragmentCouple = Main2CoupleFragment.newInstance();
				}
				return mFragmentCouple;
			}
		}

		//그외 경우는 웹으로 이동한다. 각 웹페이지는 안에서 이동하도록.
		if (mFragmentCoupleWeb == null) {
			mFragmentCoupleWeb = Main2CoupleWebFragment.newInstance();
		}

		//웹화면일때 기존에 설정된 이미지가 있으면 지워준다.
		if(mActivity != null){
			String bgImgPath = Prefer.getCoupleBackgroundImagePath(mActivity);
			if(!TextUtils.isEmpty(bgImgPath)){
				try {
					File file = new File(bgImgPath);
					if (file.exists()) {
						file.delete();
					}
				} catch (Exception e) {
					MLog.e(e);
				}
				Prefer.setCoupleBackgroundImagePath(mActivity,"");
			}
		}

		return mFragmentCoupleWeb;
	}

	private Fragment getFragmentHomeOpenBankTab(){
		//오픈뱅킹 가입여부 체크
		if(LoginUserInfo.getInstance().getOBA_SVC_ENTR_YN().equalsIgnoreCase("N")){
			if (mFragmentOpenBankWeb == null) {
				mFragmentOpenBankWeb = Main2OpenBankWebFragment.newInstance();
			}
			return mFragmentOpenBankWeb;

		}else{
			if (mFragmentOpenBank == null) {
				mFragmentOpenBank = Main2OpenBankFragment.newInstance();
			}
			return mFragmentOpenBank;
		}
	}

}
