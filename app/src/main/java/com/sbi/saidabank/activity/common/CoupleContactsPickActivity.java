package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.contacts.ContactsUtil;
import com.sbi.saidabank.common.contacts.CoupleContactsInfoAdapter;
import com.sbi.saidabank.common.contacts.GetContactListAsyncTask;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.CoupleContactInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: PincodeRegActivity
 * Created by 950485 on 2018. 12. 5..
 * <p>
 * Description: 주소록 정보 출력 화면
 */

public class CoupleContactsPickActivity extends BaseActivity {

	private EditText         mEditSearch;
	private TextView         mTextSearchCount;
	private RelativeLayout   mLayoutEmpty;

	private LinearLayout     mLayoutSidaMember;
	private LinearLayout     mLayoutSidaNoMember;

	private TextView         mTvSaidaMemberCnt;
	private TextView         mTvSaidaNoMemberCnt;

	private ListView         mListViewMember;
	private ListView         mListViewNoMember;

	private ArrayList<CoupleContactInfo> mMemberContactArray;
	private ArrayList<CoupleContactInfo> mNoMemberContactArray;

	private CoupleContactsInfoAdapter mMemberListAdapter;
	private CoupleContactsInfoAdapter mNoMemberListAdapter;

	private Handler mHandler;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_couple_contacts_pick);

		mMemberContactArray = new ArrayList<CoupleContactInfo>();
		mNoMemberContactArray = new ArrayList<CoupleContactInfo>();

		initUX();
		mHandler = new Handler();
		syncListContacts();
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {

		mTextSearchCount = findViewById(R.id.tv_result_count_search_contact);
		mLayoutEmpty =  findViewById(R.id.layout_empty_contacts);

		TextView btnClose = findViewById(R.id.tv_close_contacts_pick);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				setResult(RESULT_CANCELED);
				finish();
				overridePendingTransition(R.anim.stay,R.anim.sliding_dialog_down);
			}
		});

        LinearLayout btnRefresh = (LinearLayout) findViewById(R.id.ll_research);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditSearch.setText("");
				syncListContacts();
            }
        });

		mEditSearch = (EditText) findViewById(R.id.et_search_contacts_pick);
		mEditSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				Logs.e("onTextChanged : " + s);
				boolean digitsOnly = TextUtils.isDigitsOnly(s.toString());
				if (digitsOnly) {
					mMemberListAdapter.getPhoneFilter().filter(s.toString());
					mNoMemberListAdapter.getPhoneFilter().filter(s.toString());
				} else{
					mMemberListAdapter.getNameFilter().filter(s.toString());
					mNoMemberListAdapter.getNameFilter().filter(s.toString());
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		mLayoutSidaMember = findViewById(R.id.layout_saida_member);
		mLayoutSidaNoMember = findViewById(R.id.layout_saida_no_member);

		mTvSaidaMemberCnt = findViewById(R.id.tv_saida_menber_cnt);
		mTvSaidaNoMemberCnt = findViewById(R.id.tv_saida_no_menber_cnt);

		CoupleContactsInfoAdapter.OnFilterPublishListener mOnFilterPublishListener =new CoupleContactsInfoAdapter.OnFilterPublishListener() {
			@Override
			public void onFilterPublishEnd() {
				adjustListViewHeight();
			}
		};

		mMemberListAdapter = new CoupleContactsInfoAdapter(this, mOnFilterPublishListener);
		mListViewMember = findViewById(R.id.listview_member);
		mListViewMember.setAdapter(mMemberListAdapter);


		mNoMemberListAdapter = new CoupleContactsInfoAdapter(this, mOnFilterPublishListener);
		mListViewNoMember = findViewById(R.id.listview_member_no);
		mListViewNoMember.setAdapter(mNoMemberListAdapter);

		mListViewMember.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				final CoupleContactInfo contactsInfo = mMemberListAdapter.getFilterList().get(position);
				if (contactsInfo == null)
					return;

				if(contactsInfo.getSHRN_SVC_ENTR_YN().equals("Y")){
					DialogUtil.alert(CoupleContactsPickActivity.this,"선택하신 사용자는\n현재 커플통장 서비스를 이용할 수 없습니다.");
					return;
				}

				Intent intent = new Intent();
				intent.putExtra("TLNO", contactsInfo.getTLNO());
				intent.putExtra("MBR_NO", contactsInfo.getMBR_NO());
				intent.putExtra("FLNM", contactsInfo.getFLNM());
				setResult(RESULT_OK, intent);
				finish();
				overridePendingTransition(R.anim.stay,R.anim.sliding_dialog_down);
			}
		});

		mListViewNoMember.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
				DialogUtil.alert(CoupleContactsPickActivity.this,"상대방이 사이다뱅크에 가입 해야\n커플통장 초대가 가능합니다.");
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public void onBackPressed() {
		//super.onBackPressed();
		finish();
		overridePendingTransition(R.anim.stay,R.anim.sliding_dialog_down);
	}

	private void syncListContacts(){
		showProgressDialog();
		GetContactListAsyncTask gpbat = new GetContactListAsyncTask(this,
				GetContactListAsyncTask.GET_TYPE_CONTACT_APP,
				new GetContactListAsyncTask.OnFinishReadPhoneBook() {
					@Override
					public void onFinishReadPhoneBook(ArrayList<ContactsInfo> array) {
						if(array != null && array.size() > 0){
							requestCheckContacts(array);
							mLayoutEmpty.setVisibility(View.GONE);
						}else{
							dismissProgressDialog();
							mLayoutEmpty.setVisibility(View.VISIBLE);
						}
					}
		});
		gpbat.execute();
	}

	/**
	 * 휴대전화번호로 고객정보조회
	 * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
	 */
	void requestCheckContacts(final ArrayList<ContactsInfo> listContactsInPhonebook) {

		Map param = new HashMap();
		JSONArray jsonArray = ContactsUtil.convertToJsonArray(listContactsInPhonebook);

		//폰에 저장된 연락처가 없으면 동기화 하지 않는다.
		if(jsonArray.length() == 0 ){
			dismissProgressDialog();
			mLayoutEmpty.setVisibility(View.GONE);
			return;
		}

		param.put("REP_CNT", jsonArray.length());
		param.put("REC_TLNO", jsonArray);

		HttpUtils.sendHttpTask(WasServiceUrl.UNT0470101A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {


				if (TextUtils.isEmpty(ret)) {
					dismissProgressDialog();
					Logs.e(getString(R.string.msg_debug_no_response));
					showErrorMessage(getString(R.string.msg_debug_no_response));
					return;
				}

				try {
					JSONObject object = new JSONObject(ret);

					JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
					if (objectHead == null) {
						dismissProgressDialog();
						showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
						return;
					}

					String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
					if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
						dismissProgressDialog();
						String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
						if (TextUtils.isEmpty(msg))
							msg = getString(R.string.common_msg_no_reponse_value_was);

						Logs.e("error msg : " + msg + ", ret : " + ret);
						//mContactsList = listContactsInPhonebook;

						String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
						showCommonErrorDialog(msg, errCode, "", objectHead, true);
						return;
					}

					mMemberContactArray.clear();
					JSONArray member_array = object.optJSONArray("REC_MBR");
					if (member_array != null) {
						Logs.e("member_array : " + member_array.toString());
						for (int index = 0; index < member_array.length(); index++) {
							JSONObject jsonObject = member_array.getJSONObject(index);
							if (jsonObject == null)
								continue;

							CoupleContactInfo mainPopupInfo = new CoupleContactInfo(jsonObject,true);
							mMemberContactArray.add(mainPopupInfo);
						}
					}
					mMemberListAdapter.setArrayList(mMemberContactArray);



					mNoMemberContactArray.clear();
					JSONArray no_member_array = object.optJSONArray("REC_NNMB");
					if (no_member_array != null) {
						Logs.e("no_member_array : " + no_member_array.toString());
						for (int index = 0; index < no_member_array.length(); index++) {
							JSONObject jsonObject = no_member_array.getJSONObject(index);
							if (jsonObject == null)
								continue;

							CoupleContactInfo mainPopupInfo = new CoupleContactInfo(jsonObject,false);
							mNoMemberContactArray.add(mainPopupInfo);
						}
					}
					mNoMemberListAdapter.setArrayList(mNoMemberContactArray);

					adjustListViewHeight();
					//프로그레스바가 약간 일찍 닫히는 문제가 있어..추가.
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {
							dismissProgressDialog();
						}
					},500);

				} catch (JSONException e) {
					Logs.printException(e);
				}
			}
		});
	}

	private void adjustListViewHeight(){
		Logs.e("adjustListViewHeight - mMemberListAdapter.getCount() : " + mMemberListAdapter.getCount());
		if(mMemberListAdapter.getCount() > 0){
			mLayoutSidaMember.setVisibility(View.VISIBLE);
			int listItemHeight = (int)getResources().getDimension(R.dimen.contact_list_row_height);

			ViewGroup.LayoutParams params = mListViewMember.getLayoutParams();
			params.height = (mMemberListAdapter.getCount()) * listItemHeight;
			mListViewMember.setLayoutParams(params);
			mListViewMember.invalidate();

			String msg = "사이다회원 " + mMemberListAdapter.getCount() + "명";
			Utils.setTextWithSpan(mTvSaidaMemberCnt,msg,String.valueOf(mMemberListAdapter.getCount()), Typeface.BOLD,"#009beb");
		}else{
			mLayoutSidaMember.setVisibility(View.GONE);
		}

		Logs.e("adjustListViewHeight - mNoMemberListAdapter.getCount() : " + mNoMemberListAdapter.getCount());

		if(mNoMemberListAdapter.getCount() > 0){
			mLayoutSidaNoMember.setVisibility(View.VISIBLE);
			int listItemHeight = (int)getResources().getDimension(R.dimen.contact_list_row_height);

			ViewGroup.LayoutParams params = mListViewNoMember.getLayoutParams();
			params.height = mNoMemberListAdapter.getCount() * listItemHeight;
			mListViewNoMember.setLayoutParams(params);

			String msg = "일반연락처 " + mNoMemberListAdapter.getCount() + "명";
			Utils.setTextWithSpan(mTvSaidaNoMemberCnt,msg,String.valueOf(mNoMemberListAdapter.getCount()), Typeface.BOLD,"#009beb");
		}else{
			mLayoutSidaNoMember.setVisibility(View.GONE);
		}

		if(mMemberListAdapter.getCount() == 0 && mNoMemberListAdapter.getCount() == 0){
			mTextSearchCount.setVisibility(View.VISIBLE);
			mTextSearchCount.setText("연락처 검색결과가 없습니다.");
		}else{
			if(mEditSearch.getText().toString().length() > 0){
				mTextSearchCount.setVisibility(View.VISIBLE);
				int totalCnt = mMemberListAdapter.getCount() + mNoMemberListAdapter.getCount();
				String msg = "연락처 " + totalCnt + "명";
				Utils.setTextWithSpan(mTextSearchCount,msg,String.valueOf(totalCnt), Typeface.BOLD,"#000000");
			}else{
				mTextSearchCount.setVisibility(View.GONE);
			}
		}
	}
}