package com.sbi.saidabank.activity.crop;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;

/**
 * Saidabanking_android
 * <p>
 * Class: CropImageResultActivity
 * Created by 950485 on 2018. 10. 15..
 * <p>
 * Description:화면 Crop한 결과를 보이기 위한 화면
 */
public final class CropImageResultActivity extends BaseActivity {

    public static Bitmap mCropImage;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_crop_result);
        initUX();
        if (DataUtil.isNotNull(mCropImage)) {
            mImageView.setImageBitmap(mCropImage);
        } else {
            Uri imageUri = getIntent().getParcelableExtra("URI");
            if (DataUtil.isNotNull(imageUri)) {
                mImageView.setImageURI(imageUri);
            } else {
                Toast.makeText(CropImageResultActivity.this, "No Image is set to show", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        releaseBitmap();
        super.onBackPressed();
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mImageView = ((ImageView) findViewById(R.id.imageview_crop_result));
        Button btnCancel = (Button) findViewById(R.id.btn_crop_result_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED, null);
                finish();
            }
        });
        Button btnOK = (Button) findViewById(R.id.btn_crop_result_ok);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_OK, null);
                finish();
            }
        });
    }

    /**
     * 표시 비트맵 릴리즈
     */
    private void releaseBitmap() {
        if (DataUtil.isNotNull(mCropImage) && !mCropImage.isRecycled()) {
            mCropImage.recycle();
            mCropImage = null;
        }
    }
}
