package com.sbi.saidabank.activity.transaction.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.login.ReregLostPincodeActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.ITransferFailActivity;
import com.sbi.saidabank.activity.transaction.TransferManager;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertLinkDialog;
import com.sbi.saidabank.common.dialog.AlertRemitteeDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.TransferSafeDealSelectActivity;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;
import com.sbi.saidabank.define.datatype.transfer.TransferVerifyInfo;
import com.sbi.saidabank.solution.motp.MOTPManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TransferUtils {

    public interface OnCheckFinishListener{
        void onCheckFinish();
    }

    /**
     * 통합이체 화면 호출
     * 출금계좌를 물고 들어올경우 출금계좌를 넣어준다.
     *
     * @param activity
     * @param outComAccount  출금계좌
     */
    public static void startTransferActivity(Activity activity,String outComAccount){
        Intent intent = new Intent(activity, ITransferSelectReceiverActivity.class);
        if(!TextUtils.isEmpty(outComAccount))
            intent.putExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT, outComAccount);

        activity.startActivity(intent);
    }
    /**
     * 통합이체 화면 호출
     * 받는사람정보를 물고 들어올경우 받는분 은행코드와 계좌번호를 넣어준다.
     *
     *
     * @param activity
     * @param rcvBankCd
     * @param rcvAccountNo
     */
    public static void startTransferActivity(Activity activity, String rcvBankCd, String rcvAccountNo){
        Intent intent = new Intent(activity, ITransferSelectReceiverActivity.class);
        if(!TextUtils.isEmpty(rcvBankCd))
            intent.putExtra(Const.INTENT_RCV_BANK_CODE, rcvBankCd);
        if(!TextUtils.isEmpty(rcvAccountNo))
            intent.putExtra(Const.INTENT_RCV_ACCOUNT, rcvAccountNo);

        activity.startActivity(intent);
    }

    /**
     * 이체버튼 클릭시 세션을 동기화 하고 이체 세션 초기화 하고 인증수단의 상태여부를 체크한다.
     */
    public static void initAndSyncSessionAndCheckAuthMethod(final Activity activity,boolean initial,final TransferUtils.OnCheckFinishListener listener){
        //이체기본 설정. - 종류: 일반이체/가져오기/내보내기
        ITransferDataMgr.getInstance().setTRANSFER_ACCESS_TYPE(ITransferDataMgr.ACCESS_TYPE_NORMAL);

        if(LoginUserInfo.getInstance().getOBA_RESP_CD().equalsIgnoreCase("2")){
            //오픈뱅킹 동의가 1년이 지나 만료됨**************
            Intent intent = new Intent(activity, WebMainActivity.class);
            intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0110101.getServiceUrl());
            activity.startActivity(intent);
            return;
        }

        //웹의 이체 세션을 지워준다.
        TransferRequestUtils.requestClearTransferSession(activity);

        //여기서 모델링 초기화 하자...
        if(initial){
            ITransferDataMgr.clear();
        }


        //로그인 세션 동기화
        TransferRequestUtils.requestSessionSync(activity, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)activity).dismissProgressDialog();
                //이체시 인증수단의 상태를 체크한다.
                TransferUtils.checkErrorCntAndVaildAuthMethod(activity, listener);
            }
        });
    }



    /**
     * 이체시 인증수단의 상태를 체크한다.
     * 타행OTP,모바일OTP,핀등
     *
     * @param activity
     */
    public static void checkErrorCntAndVaildAuthMethod(final Activity activity,final OnCheckFinishListener listener) {
        MLog.d();

        //OTP값 초기화
        if(activity instanceof TransferSafeDealSelectActivity){
            TransferSafeDealDataMgr.getInstance().setOTP_SERIAL_NO("");
            TransferSafeDealDataMgr.getInstance().setOTP_TA_VERSION("");
        }else{
            ITransferDataMgr.getInstance().setOTP_SERIAL_NO("");
            ITransferDataMgr.getInstance().setOTP_TA_VERSION("");
        }


        if (activity.isFinishing()) {
            return;
        }

        // 핀코드 오류 횟수 체크
        if (Prefer.getPinErrorCount(activity) >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
            AlertDialog alertDialog = new AlertDialog(activity);
            alertDialog.mNListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //activity.finish();
                }
            };
            alertDialog.mPListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.PINCODE_FORGET);
                    commonUserInfo.setMBRnumber(DataUtil.isNotNull(LoginUserInfo.getInstance().getMBR_NO()) ? LoginUserInfo.getInstance().getMBR_NO() : Const.EMPTY);
                    String accn = LoginUserInfo.getInstance().getODDP_ACCN();
                    if((DataUtil.isNull(accn) || Integer.parseInt(accn) < 1)){
                        commonUserInfo.setHasAccount(false);
                    }else{
                        commonUserInfo.setHasAccount(true);
                    }

                    Intent intent = new Intent(activity, ReregLostPincodeActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    activity.startActivity(intent);
                    //activity.finish();
                }
            };
            alertDialog.msg = String.format(activity.getString(R.string.msg_err_over_match_pin), Const.SSENSTONE_AUTH_COUNT_FAIL);
            alertDialog.mPBtText = activity.getString(R.string.rereg);
            alertDialog.show();

            return;
        }

        final LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (DataUtil.isNull(SECU_MEDI_DVCD)) {
            listener.onCheckFinish();
            return;
        }

        if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
            String OTP_STCD = loginUserInfo.getOTP_STCD();
            final String MOTP_EROR_TCNT = loginUserInfo.getMOTP_EROR_TCNT();
            if ("120".equalsIgnoreCase(OTP_STCD)) { // 사고 분실


                AlertLinkDialog alertLinkDialog = new AlertLinkDialog(activity);
                alertLinkDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity.finish();
                    }
                };
                alertLinkDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        String url = WasServiceUrl.CUS0030101.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertLinkDialog.mLinkListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0040101.getServiceUrl();
                        String param = null;
                        if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                            param = "TRTM_DVCD=ACDTLOCK";
                        } else {
                            param = "TRTM_DVCD=ACDT";
                        }
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        intent.putExtra(Const.INTENT_PARAM, param);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertLinkDialog.mTopText = activity.getString(R.string.msg_otp_accident_topmsg);
                alertLinkDialog.mSubText = activity.getString(R.string.msg_otp_accdent_contents);
                alertLinkDialog.mPBtText = activity.getString(R.string.msg_report_otp);
                alertLinkDialog.mLinkText = activity.getString(R.string.msg_motp_issue);
                alertLinkDialog.show();

            } else if ("130".equalsIgnoreCase(OTP_STCD)) { // 폐기된 인증서


                AlertDialog alertDialog = new AlertDialog(activity);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity.finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0220101.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertDialog.mTopText = activity.getString(R.string.msg_otp_disposal_topmsg);
                alertDialog.msg = activity.getString(R.string.msg_otp_disposal_contsnts);
                alertDialog.mPBtText = activity.getString(R.string.msg_otp_other_disposal_termination);
                alertDialog.show();

            } else if ("140".equalsIgnoreCase(OTP_STCD)) { // 오류 횟수 초과


                AlertDialog alertDialog = new AlertDialog(activity);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity.finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertDialog.msg = activity.getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = activity.getString(R.string.msg_init_otp);
                alertDialog.show();

            } else if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {


                AlertDialog alertDialog = new AlertDialog(activity);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity.finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertDialog.msg = activity.getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = activity.getString(R.string.msg_init_otp);
                alertDialog.show();
                ((BaseActivity)activity).dismissProgressDialog();
            }else {
                listener.onCheckFinish();
            }
        } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {

            if (Const.BRIDGE_RESULT_YES.equals(loginUserInfo.getMOTP_END_YN())) {


                AlertDialog alertDialog = new AlertDialog(activity);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity.finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
                        String param = "TRTM_DVCD=" + 1;
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        intent.putExtra(Const.INTENT_PARAM, param);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertDialog.msg = activity.getString(R.string.msg_motp_expired);
                alertDialog.mPBtText = activity.getString(R.string.common_reissue);
                alertDialog.show();

                return;
            }

            if (DataUtil.isNotNull(loginUserInfo.getMOTP_EROR_TCNT()) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {


                AlertDialog alertDialog = new AlertDialog(activity);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //activity.finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
                        String param = "TRTM_DVCD=" + 1;
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        intent.putExtra(Const.INTENT_PARAM, param);
                        activity.startActivity(intent);
                        //activity.finish();
                    }
                };
                alertDialog.msg = activity.getString(R.string.msg_motp_lost);
                alertDialog.mPBtText = activity.getString(R.string.common_reissue);
                alertDialog.show();

                return;
            }

            MOTPManager.getInstance(activity).getSerialNum(new MOTPManager.MOTPEventListener() {
                @Override
                public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {

                    if (!"0000".equals(resultCode)) {
                        AlertDialog alertDialog = new AlertDialog(activity);
                        alertDialog.mNListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //activity.finish();
                            }
                        };
                        String deviceId = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
                        String motpDeviceId = LoginUserInfo.getInstance().getMOTP_SMPH_DEVICE_ID();
                        if (TextUtils.isEmpty(deviceId) || TextUtils.isEmpty(motpDeviceId)) {
                            alertDialog.mPListener = new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(activity, WebMainActivity.class);
                                    String url = WasServiceUrl.CRT0040401.getServiceUrl();
                                    String param = "TRTM_DVCD=" + 1;
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                                    intent.putExtra(Const.INTENT_PARAM, param);
                                    activity.startActivity(intent);
                                    //activity.finish();
                                }
                            };
                        } else {
                            if (deviceId.equals(motpDeviceId)) {
                                alertDialog.mPListener = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(activity, WebMainActivity.class);
                                        String url = WasServiceUrl.CRT0040501.getServiceUrl();
                                        intent.putExtra("url", url);
                                        activity.startActivity(intent);
                                        //activity.finish();
                                    }
                                };
                            } else {
                                alertDialog.mPListener = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(activity, WebMainActivity.class);
                                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
                                        String param = "TRTM_DVCD=" + 1;
                                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                                        intent.putExtra(Const.INTENT_PARAM, param);
                                        activity.startActivity(intent);
                                        //activity.finish();
                                    }
                                };
                            }
                        }
                        alertDialog.msg = activity.getString(R.string.msg_motp_change_device);
                        alertDialog.mPBtText = activity.getString(R.string.common_reissue);
                        alertDialog.show();
                        ((BaseActivity)activity).dismissProgressDialog();
                    } else {
                        Logs.e(activity,"OTP_SERIAL_NO : " + reqData1 + "\nOTP_TA_VERSION : " + reqData4);

                        if(activity instanceof TransferSafeDealSelectActivity){
                            TransferSafeDealDataMgr.getInstance().setOTP_SERIAL_NO(reqData1);
                            TransferSafeDealDataMgr.getInstance().setOTP_TA_VERSION(reqData4);
                        }else{
                            ITransferDataMgr.getInstance().setOTP_SERIAL_NO(reqData1);
                            ITransferDataMgr.getInstance().setOTP_TA_VERSION(reqData4);
                        }

                        listener.onCheckFinish();
                    }
                }
            });
        }else {
            listener.onCheckFinish();
        }
    }

    /**
     * 단건이체 수취인 조회결과 화면을 보여준다.

     * @param object          수취인 조회 결과값 (JSON object)
     */
    public static void showDialogRemitteeAccount(final Activity activity,
                                                 final boolean isAddTransfer,
                                                 final JSONObject object,final HttpSenderTask.HttpRequestListener2 listener) {
        MLog.d();

        AlertRemitteeDialog alertDialog = new AlertRemitteeDialog(activity);
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ITransferRemitteeInfo remitteeInfo = new ITransferRemitteeInfo();
                remitteeInfo.setCNTP_FIN_INST_CD(object.optString("CNTP_FIN_INST_CD"));
                remitteeInfo.setDTLS_FNLT_CD(object.optString("DTLS_FNLT_CD"));
                remitteeInfo.setCNTP_BANK_ACNO(object.optString("CNTP_BANK_ACNO"));

                remitteeInfo.setRECV_NM(object.optString("RECV_NM"));
                remitteeInfo.setCUST_NM(LoginUserInfo.getInstance().getCUST_NM());

                //은행명,은행별명
                remitteeInfo.setMNRC_BANK_NM(object.optString("MNRC_BANK_NM"));
                remitteeInfo.setMNRC_BANK_ALS(object.optString("MNRC_ACCO_ALS")); //은행 단축명이다..오락가락하지 말도록.


                if(ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() == ITransferDataMgr.ACCESS_TYPE_NORMAL){
                    //받는분통장표기/내통장표기 넣어준다.
                    remitteeInfo.setDEPO_BNKB_MRK_NM(LoginUserInfo.getInstance().getCUST_NM());
                    if(ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
                        remitteeInfo.setTRAN_BNKB_MRK_NM(object.optString("RECV_NM"));
                    }else{
                        remitteeInfo.setTRAN_BNKB_MRK_NM("사뱅오픈"+object.optString("RECV_NM"));
                    }
                }else {
                    if (ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 0) {
                        ITransferRemitteeInfo info = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                        remitteeInfo.setDEPO_BNKB_MRK_NM(info.getDEPO_BNKB_MRK_NM());
                    }

                    if (ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")) {
                        remitteeInfo.setTRAN_BNKB_MRK_NM(object.optString("RECV_NM"));
                    } else {
                        remitteeInfo.setTRAN_BNKB_MRK_NM("사뱅오픈" + object.optString("RECV_NM"));
                    }
                    remitteeInfo.setTRN_AMT(ITransferDataMgr.getInstance().getMNRC_AMT());
                    //ACCESS_TYPE_NORMAL로 변경해서 다시 여기에 들어오지 않도록 처리한다.
                    ITransferDataMgr.getInstance().setTRANSFER_ACCESS_TYPE(ITransferDataMgr.ACCESS_TYPE_NORMAL);
                }

                //즉시이체 기본설정.
                remitteeInfo.setTRNF_DVCD("1");
                //가상계좌여부
                remitteeInfo.setVERTUAL_ACCOUNT_YN("N");

                //수취인조회가 끝난것은 기존 데이타 지우고 리스트에 담아둔다.
                if(!isAddTransfer) {
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().clear();
                }

                //다건 리스트에 금액이 없는 경우 포커스 주기 위해.
                if(ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 0 && TextUtils.isEmpty(remitteeInfo.getTRN_AMT())){
                    remitteeInfo.setIS_NEED_FOCUS(true);
                }

                ITransferDataMgr.getInstance().addRemitteInfoIntoArrayList(remitteeInfo);

                Logs.e(activity,"수취인 이체항목 추가건수 : " +  ITransferDataMgr.getInstance().getRemitteInfoArraySize());

                listener.endHttpRequest(true,object.toString());
            }
        };
        alertDialog.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MLog.d();
                listener.endHttpRequest(false,"");
            }
        };
        alertDialog.msg = String.format(activity.getString(R.string.msg_confirm_received_account_01), object.optString("RECV_NM"));
        alertDialog.show();
    }

    /**
     * 사이다뱅크 계좌여부를 리턴한다.
     * @return
     */
    public static boolean isSaidaAccount(String bankCd){
        if(!TextUtils.isEmpty(bankCd) && bankCd.equalsIgnoreCase("028")){
            return true;
        }
        return false;
    }
    public static boolean isSaidaAccount(){
        String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        return isSaidaAccount(WTCH_BANK_CD);
    }

    //사이다계좌중 메인계좌를 리턴한다.
    public static MyAccountInfo getSaidaMainAccount(){
        ArrayList<MyAccountInfo> accountInfoArrayList =  LoginUserInfo.getInstance().getMyAccountInfoArrayList();

        if(accountInfoArrayList.size() == 0) return null;

        for(int i=0;i<accountInfoArrayList.size();i++){
            MyAccountInfo myAccountInfo = accountInfoArrayList.get(i);
            if(myAccountInfo.getRPRS_ACCO_YN().equalsIgnoreCase("Y")){
                return myAccountInfo;
            }
        }

        return accountInfoArrayList.get(0);
    }

    public static boolean checkAbleServiceCondition(final Activity activity,String typeText){

        //지연이체 서비스 가입했을때
        if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
            TransferUtils.showAlertLinkDialog(activity,typeText,"지연이체", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return false;
        }
        //입금지정 서비스 가입했을때
        if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
            TransferUtils.showAlertLinkDialog(activity,typeText,"입금계좌지정", new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            return false;
        }

        return true;
    }

    /**
     * 이동 링크가 달린 알럿다이얼로그를 보여준다.
     *
     * @param activity
     * @param mainText  지연이체,입금계좌지정 텍스트 직접 입력
     */
    public static void showAlertLinkDialog(final Activity activity,final String typeStr,final String mainText,View.OnClickListener listener){
        AlertLinkDialog alertLinkDialog = new AlertLinkDialog(activity);
        alertLinkDialog.mPListener = listener;
        alertLinkDialog.mLinkListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, WebMainActivity.class);
                String url="";
                if(mainText.contains("지연이체")){
                    url = WasServiceUrl.CRT0160101.getServiceUrl();

                }else if(mainText.contains("입금계좌지정")){
                    url = WasServiceUrl.CRT0180101.getServiceUrl();
                }
                if(!TextUtils.isEmpty(url)){
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    activity.startActivity(intent);
                    //activity.finish();
                }
            }
        };
        alertLinkDialog.mTopText = mainText + "서비스 이용중입니다.";
        alertLinkDialog.mSubText = "서비스 해지 후, "+ typeStr +"가 가능합니다.\n" + typeStr + "를 하시려면, 서비스를 해지해주세요.";
        alertLinkDialog.mLinkText = mainText + "서비스 해지";
        alertLinkDialog.show();

    }


    /**
     * 계좌이체시에 사용하는 mOTP, 타행 OTP, 핀코드 입력창 표시
     *
     * @param signData 전자서명된 이체정보
     */
    public static void showOTPActivity(final Activity activity,String signData) {
        MLog.d();
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (DataUtil.isNull(SECU_MEDI_DVCD)) {
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setSignData(signData);
            commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
            Intent intent = new Intent(activity, PincodeAuthActivity.class);
            intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
            intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
            intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            activity.startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
            activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        } else {
            Intent intent;
            if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                intent = new Intent(activity, OtherOtpAuthActivity.class);
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = ITransferDataMgr.getInstance().getTotalTranSumAmount();

                    if (totalAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(activity, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //TODO:sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        activity.startActivityForResult(intentPin, Const.REQUEST_SSENSTONE_AUTH);
                        activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = ITransferDataMgr.getInstance().getTotalTranSumAmount();
                    if (totalAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(activity, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //TODO:sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        activity.startActivityForResult(intentPin, Const.REQUEST_SSENSTONE_AUTH);
                        activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }

                intent = new Intent(activity, OtpPwAuthActivity.class);
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP);

                String mMOTPSerialNumber = ITransferDataMgr.getInstance().getOTP_SERIAL_NO();
                String mMOTPTAVersion = ITransferDataMgr.getInstance().getOTP_TA_VERSION();

                intent.putExtra(Const.INTENT_OTP_SERIAL_NUMBER, mMOTPSerialNumber);
                intent.putExtra(Const.INTENT_OTP_TA_VERSION, mMOTPTAVersion);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else {
                intent = new Intent(activity, PincodeAuthActivity.class);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                //TODO:sign data, service id 추가
                commonUserInfo.setSignData(signData);
                intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                activity.startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
                activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                return;
            }
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            activity.startActivityForResult(intent, Const.REQUEST_OTP_AUTH);
            activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }


    /**
     * 이체실패에 따른 메세지 화면 표시
     *
     * @param type 오류 타입
     * @param ret 오류 메시지
     */
    public static void showFailTransfer(Activity activity, int type, String ret) {
        MLog.d();
        Intent intent = new Intent(activity, ITransferFailActivity.class);
        intent.putExtra(Const.INTENT_TRANSFER_ENTRY_TYPE, 0);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_TYPE, type);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_RET, ret);
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);
        activity.finish();
    }

    public static ArrayList<OpenBankAccountInfo> getOpenBankDepositPosibleAccountList(JSONObject object) throws JSONException {
        JSONArray jsonArray = object.optJSONArray("REC");

        ArrayList<OpenBankAccountInfo> realAccountInfoArrayList = new ArrayList<OpenBankAccountInfo>();

        if (jsonArray != null && jsonArray.length() > 0) {
            ArrayList<OpenBankAccountInfo> tempAccountInfoArrayList = new ArrayList<OpenBankAccountInfo>();
            for (int i = 0; i < jsonArray.length(); i++) {

                JSONObject obj = jsonArray.getJSONObject(i);
                OpenBankAccountInfo obAccInfo = new OpenBankAccountInfo(obj);
                Logs.e(i + ".accountInfo :" + obj.toString());

                if(!obAccInfo.OBA_ACCO_KNCD.equalsIgnoreCase("6") ){
                    //입금이 가능한 모든 계좌를 보여주도록 한다.
                    tempAccountInfoArrayList.add(obAccInfo);
                }
            }

            //정상계좌만 처리하기 위해 한번더...

            for(int i=0;i<tempAccountInfoArrayList.size();i++){
                OpenBankAccountInfo info = tempAccountInfoArrayList.get(i);
                if(info.ACNO_REG_STCD.equalsIgnoreCase("1")){
                    realAccountInfoArrayList.add(info);
                }
            }
        }

        return realAccountInfoArrayList;
    }

    public static void showDialogSuspantionAccount(final Activity activity, final String acno){
        AlertLinkDialog alertLinkDialog = new AlertLinkDialog(activity);
//        alertLinkDialog.mNListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                //activity.finish();
//            }
//        };
        alertLinkDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
        alertLinkDialog.mLinkListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, WebMainActivity.class);
                String url = WasServiceUrl.UNT0090100.getServiceUrl();
                String param = "ACNO="+acno;
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                intent.putExtra(Const.INTENT_PARAM, param);
                activity.startActivity(intent);
            }
        };
        alertLinkDialog.mTopText = "거래정지 계좌입니다.";
        alertLinkDialog.mSubText = "거래중지 제한해제 후, 이체가 가능합니다.\n이체를 하시려면, 제한해제를 해주세요.";
        alertLinkDialog.mPBtText = "확인";
        alertLinkDialog.mLinkText = "거래중지 제한해제";
        alertLinkDialog.show();
    }

    /**
     * 오픈뱅킹 장애은행 다이얼로그 출력
     *
     * @param activity
     */
    public static void showDialogProblemBank(final Activity activity){
        DialogUtil.alert(activity,"확인","장애은행확인","일시적으로 계좌의 정보를\n확인할 수 없습니다.\n잠시 후 다시 이용해주시기 바랍니다.",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.TRA0190600.getServiceUrl());
                        activity.startActivity(intent);
                    }
                });
    }

    /**
     * 오픈뱅킹 참가기관 다이얼로그 출력
     *
     * @param activity
     */
    public static void showDialogEnterOpenBank(final Activity activity){
        DialogUtil.alert(activity,"확인","가능기관확인","오픈뱅킹으로 이체가 불가한 기관입니다.\n출금계좌를 당행으로 변경하시기 바랍니다.",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.TRA0190500.getServiceUrl());
                        activity.startActivity(intent);
                    }
                });
    }


}
