package com.sbi.saidabank.activity.test;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ahnlab.v3mobileplus.interfaces.V3MobilePlusCtl;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.LinkObject;
import com.kakao.message.template.TextTemplate;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.util.helper.log.Logger;
import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.certification.CertAuthPWActivity;
import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.activity.common.CoupleContactsPickActivity;
import com.sbi.saidabank.activity.login.IdentificationPrepareActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.login.SplashActivity;
import com.sbi.saidabank.activity.mtranskey.TransKeySafeCardActivity;
import com.sbi.saidabank.activity.setting.ManageAuthWaysActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeRegActivity;
import com.sbi.saidabank.activity.transaction.ITransferSalaryActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.TransferPhoneActivity;
import com.sbi.saidabank.activity.transaction.TransferPhonePolicyActivity;
import com.sbi.saidabank.activity.transaction.adater.ITransferSalaryAdapter;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.activity.common.ContactsPickActivity;
import com.sbi.saidabank.common.dialog.SlidingAccountInputDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.solution.jex.JexAESEncrypt;
import com.sbi.saidabank.solution.ocr.OcrImgMaskTask;
import com.sbi.saidabank.solution.v3.V3Manager;
import com.selvasai.selvyocr.idcard.recognizer.SelvyIDCardRecognizer;
import com.softsecurity.transkey.TransKeyActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 * Class: SolutionTestActivity
 * Created by 950469 on 2018. 8. 17..
 * <p>
 * Description:솔루션들을 적용하고 테스트 하기위한 화면.
 */
public class SolutionTestActivity extends BaseActivity implements View.OnClickListener, OcrImgMaskTask.OcrImgTaskListener,
        V3Manager.V3CompleteListener, SlidingDateTimerPickerDialog.OnConfirmListener {
    private static final int REQ_OCR_ID             = 100;
    private static final int REQUEST_PINAUTH        = 400;
    private static final int REQUEST_SEARCH_ADDRESS = 1000;

    private EditText mWebAddr;

    private ImageView mIDCardImg;
    private TextView mMBRNO;

    private V3Manager mV3Manager = null;

    // kakaolink
    private Map<String, String> serverCallbackArgs = getServerCallbackArgs();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_solution);

        findViewById(R.id.btn_logout).setOnClickListener(this);

        findViewById(R.id.idcard).setOnClickListener(this);
        findViewById(R.id.auto_logout_timer).setOnClickListener(this);
        findViewById(R.id.web_view).setOnClickListener(this);

        findViewById(R.id.alert_test).setOnClickListener(this);
        findViewById(R.id.http_test).setOnClickListener(this);
        findViewById(R.id.screen_capture).setOnClickListener(this);

        findViewById(R.id.v3_test).setOnClickListener(this);
        findViewById(R.id.mtranskey).setOnClickListener(this);

        findViewById(R.id.scrapping).setOnClickListener(this);
        findViewById(R.id.ssenstone).setOnClickListener(this);
        findViewById(R.id.safe_card).setOnClickListener(this);

        findViewById(R.id.aes_encrypt).setOnClickListener(this);
        findViewById(R.id.appsflyer_test).setOnClickListener(this);
        findViewById(R.id.graph_test).setOnClickListener(this);

        findViewById(R.id.contacts_test).setOnClickListener(this);
        findViewById(R.id.manage_auth_test).setOnClickListener(this);
        findViewById(R.id.kakaolink_test).setOnClickListener(this);

        findViewById(R.id.memberjoin_test).setOnClickListener(this);

        findViewById(R.id.account_test).setOnClickListener(this);
        findViewById(R.id.join_test).setOnClickListener(this);
        findViewById(R.id.transfer_test).setOnClickListener(this);
        findViewById(R.id.transferphone_test).setOnClickListener(this);

        //주소검색
        findViewById(R.id.search_address).setOnClickListener(this);

        findViewById(R.id.btn_bank_stock_dialog).setOnClickListener(this);


        mMBRNO = findViewById(R.id.tv_mbrno);
        mWebAddr = findViewById(R.id.web_address);
        String testUrl = checkTestUrl(Prefer.getSolutionWebAddress(this));
        mWebAddr.setText(testUrl);
        mIDCardImg = findViewById(R.id.idcard_img);


    }

    @Override
    protected void onResume() {
        super.onResume();
        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
        if (!TextUtils.isEmpty(mbrNo)) {
            mMBRNO.setText(mbrNo);
        }
    }

    @Override
    public void onBackPressed() {
        if (V3Manager.getInstance(getApplicationContext()).isRunV3MobilePlus()) {
            V3Manager.getInstance(getApplicationContext()).stopV3MobilePlus();
        }
        //LoginUserInfo.clearInstance();
        //LogoutTimeChecker.getInstance(SolutionTestActivity.this).autoLogoutStop();
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_logout: {
                DialogUtil.alert(SolutionTestActivity.this,
                        "로그아웃",
                        "취소",
                        "로그아웃 하시겠습니까?",

                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                LoginUserInfo.clearInstance();
                                LoginUserInfo.getInstance().setLogin(false);
                                LogoutTimeChecker.getInstance(SolutionTestActivity.this).autoLogoutStop();
                                SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
                                if (mApplicationClass != null)
                                    mApplicationClass.allActivityFinish(true);
                                else
                                    finish();
                                Intent intent = new Intent(SolutionTestActivity.this, LogoutActivity.class);
                                intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
                                startActivity(intent);
                                return;
                            }
                        },

                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        });
                break;
            }
            case R.id.idcard: {
//                ArrayList<String> permissions = new ArrayList<>();
//                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
//                    permissions.add(Manifest.permission.CAMERA);
//                }
//
//                if (permissions.size() > 0) {
//                    String[] temp = new String[permissions.size()];
//                    permissions.toArray(temp);
//                    ActivityCompat.requestPermissions(this, temp, Const.REQUEST_PERMISSION_CAMERA);
//                } else {
//                    startOCRActivity();
//                }

                CommonUserInfo mCommonUserInfo = new CommonUserInfo();


                Intent intent = new Intent(this, IdentificationPrepareActivity.class);
                mCommonUserInfo.setHasAccount(true);
                mCommonUserInfo.setNewUser(false);
                //mCommonUserInfo.setMBRnumber(mbrNo);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                finish();
                break;
            }
            case R.id.auto_logout_timer: {
                LogoutTimeChecker.getInstance(this).autoLogoutStart();
                Toast.makeText(this, "Start LogoutTime", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.web_view: {
                Prefer.setSolutionWebAddress(this, mWebAddr.getText().toString());
                Intent intent = new Intent(this, WebMainActivity.class);
                intent.putExtra("url", mWebAddr.getText().toString());
                startActivity(intent);
                break;
            }
            case R.id.alert_test: {
                DialogUtil.alert(this,
                        "다음",
                        "이전",
                        "네트워크를 사용용할 수 없습니다.\n네트워크 연결 상태를 확인후 다시 시도해주세요.",

                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        },

                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        });
                break;
            }
            case R.id.http_test: {
                showProgressDialog();
                Map param = new HashMap();
                param.put("MENU_VRSN", "0");
                HttpUtils.sendHttpTask(SaidaUrl.getBaseWebUrl() + "/cmm0010100A03.jct", param, new HttpSenderTask.HttpRequestListener() {
                    @Override
                    public void endHttpRequest(String ret) {
                        dismissProgressDialog();
                        DialogUtil.alert(SolutionTestActivity.this, ret);
                        Logs.e("ret : " + ret);
                    }
                });
                break;
            }
            case R.id.screen_capture: {
                DialogUtil.error(this, "ERROR", "CODE : 9999\n 작업중 에러가 발생했습니다.\n잠시후 다시 시도해 주세요.", "확인", null);
                break;
            }
            case R.id.v3_test: {
                checkV3MobilePlus();
                break;
            }
            case R.id.mtranskey: {
                int keyType = TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;
                //keyType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;

                Intent intentKey = new Intent(this, CertAuthPWActivity.class);
                intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
                intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, "보안키패드입니다.");
                intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, "보안키패드 입력 테스트");
                intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 10);
                intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, 2);
                intentKey.putExtra(Const.INTENT_TRANSKEY_INPUTID1, 0);

                this.startActivityForResult(intentKey, 300);
                break;
            }
            case R.id.scrapping: {
                Intent iScraping = new Intent(this, ESpiderTestActivity.class);
                startActivity(iScraping);
                break;
            }
            case R.id.ssenstone: {
                Intent iSsenston = new Intent(this, SsenstoneTestActivity.class);
                startActivity(iSsenston);
                break;
            }
            case R.id.safe_card: {
                Intent safecardIntent = new Intent(this, TransKeySafeCardActivity.class);
                safecardIntent.putExtra(Const.SAFE_CARD_FIRST_NUM, "01");
                safecardIntent.putExtra(Const.SAFE_CARD_SECOND_NUM, "21");
                safecardIntent.putExtra(Const.INTENT_TRANSKEY_PADTYPE, TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER);
                safecardIntent.putExtra(Const.INTENT_TRANSKEY_TITLE, "보안키패드입니다.");
                safecardIntent.putExtra(Const.INTENT_TRANSKEY_LABEL1, "보안키패드 입력 테스트");
                safecardIntent.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 2);
                safecardIntent.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, 2);
                startActivity(safecardIntent);
                break;
            }
            case R.id.aes_encrypt: {
                String encStr = null;
                try {
                    encStr = JexAESEncrypt.encrypt("0123456789");
                } catch (Exception e) {
                    Logs.printException(e);
                }
                Logs.e("aes enc string : " + encStr);
                break;
            }
            case R.id.appsflyer_test: {
                //Appsflyer 이벤트 전달 방식 예제 코드
                Map<String, Object> eventValue = new HashMap<String, Object>();
                eventValue.put(AFInAppEventParameterName.REVENUE, 200);
                eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "category_a");
                eventValue.put(AFInAppEventParameterName.CONTENT_ID, "1234567");
                eventValue.put(AFInAppEventParameterName.CURRENCY, "USD");
                AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.PURCHASE, eventValue);
                break;
            }
            case R.id.graph_test: {
                Intent cintent = new Intent(this, CustomGraphTestActivity.class);
                startActivity(cintent);
                break;
            }
            case R.id.contacts_test: {
                Intent contactsintent = new Intent(this, ContactsPickActivity.class);
                startActivity(contactsintent);
                break;
            }
            case R.id.manage_auth_test: {
                Intent authintent = new Intent(this, ManageAuthWaysActivity.class);
                startActivity(authintent);
                break;
            }
            case R.id.kakaolink_test: {
                //Logs.i(Utils.getKeyHash(this));
                TextTemplate params = TextTemplate.newBuilder(
                        "SBI 사이다뱅크 이체 완료되었습니다",
                        LinkObject.newBuilder()
                                .setWebUrl("https://www.sbisb.co.kr")
                                .setMobileWebUrl("https://www.sbisb.co.kr")
                                .build()
                )
                        .setButtonTitle("확인하기")
                        .build();

                KakaoLinkService.getInstance().sendDefault(this, params, serverCallbackArgs, new ResponseCallback<KakaoLinkResponse>() {
                    @Override
                    public void onFailure(ErrorResult errorResult) {
                        Logger.e(errorResult.toString());
                    }

                    @Override
                    public void onSuccess(KakaoLinkResponse result) {
                    }
                });
                break;
            }
            case R.id.memberjoin_test: {
                Intent sintent = new Intent(this, SplashActivity.class);
                CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.SERVICE_JOIN, EntryPoint.SERVICE_JOIN);
                sintent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(sintent);
                finish();
                break;
            }

            /*
            case R.id.for_test: {

                File saveFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/encdata");
                if(!saveFile.exists()){ // 폴더 없을 경우
                    saveFile.mkdir(); // 폴더 생성
                }
                BufferedWriter buf = null;
                try {
                    buf = new BufferedWriter(new FileWriter(saveFile+"/AES256encodedata.txt", true));
                    for (int i = 0;  i < 1000000 ; i++) {
                        String testStr = String.format("%06d", i);
                        buf.append(AES256Utils.encodeAES(testStr));
                        buf.newLine(); // 개행
                    }
                    buf.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                break;
            }
            */

            case R.id.account_test: {
                Intent intent = new Intent(this, WebMainActivity.class);
                intent.putExtra("url", SaidaUrl.getBaseWebUrl() + "/unt0030100.act");
                startActivity(intent);
                break;
            }
            case R.id.join_test: {
                Intent intent = new Intent(this, WebMainActivity.class);
                intent.putExtra("url", SaidaUrl.getBaseWebUrl() + "/crt0010101.act");
                startActivity(intent);
                break;
            }
            case R.id.transfer_test: {
                /*
                try {
                    JSONObject jsonObj0 = new JSONObject();
                    JSONObject jsonObj1 = new JSONObject();
                    jsonObj1.put("accountNum", "123456789012345");    // 통장번호
                    jsonObj1.put("alias", "내통장");                  // 통장별칭
                    jsonObj1.put("balance", 1234560);                 // 통장잔액
                    jsonObj1.put("transferOneTime", 1000);            // 1회 이체한도
                    jsonObj1.put("transferOneDay", 2000);             // 1일 이체한도
                    jsonObj0.put("body", jsonObj1);

                    Intent transferIntent = new Intent(this, TransferAccountActivity.class);
                    //transferIntent.putExtra(Const.INTENT_TRANSFER_ACCOUNT_INFO, jsonObj0.toString());
                    startActivity(transferIntent);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
                */
                Intent intent = new Intent(this, CoupleContactsPickActivity.class);
                startActivity(intent);
                break;
            }
            case R.id.transferphone_test: {
                requestIsAgreeTransferPhone();
                break;
            }
            case R.id.search_address:{
                Intent intent = new Intent(this, WebMainActivity.class);
                intent.putExtra("url", WasServiceUrl.CMM0040100.getServiceUrl());
                startActivityForResult(intent,REQUEST_SEARCH_ADDRESS);
                break;
            }
            case R.id.btn_bank_stock_dialog:{
//                DialogUtil.bankStock(SolutionTestActivity.this,new SlidingBankStockDialog.OnSelectBankStockListener() {
//                    @Override
//                    public void onSelectListener(String bank, String code) {
//                        SlidingAccountInputDialog dialog = new SlidingAccountInputDialog(SolutionTestActivity.this,
//                                "","",null);
//                        dialog.show();
//                    }
//                });

                Intent intent = new Intent(this, ITransferSalaryActivity.class);
                startActivity(intent);
                //bankStockAccountDialog();
                break;
            }

            default:
                break;
        }
    }

    private void bankStockAccountDialog(){
        MLog.d();


        String bankListStr = Prefer.getBankList(this);
        String stockListStr = Prefer.getStockList(this);

        ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
        ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);


        final SlidingAccountInputDialog.OnFinishAccountInputListener accountInputListener = new SlidingAccountInputDialog.OnFinishAccountInputListener() {
            @Override
            public void onClickBack() {

                bankStockAccountDialog();

            }

            @Override
            public void onEditFinishListener(String bankCode, String bankName, String accountNum) {
                String msg = "bankCdoe : " + bankCode + " , bankName : " + bankName + " , accountNum " + accountNum;
                Logs.showToast(SolutionTestActivity.this,msg);
            }
        };

        final SlidingExpandBankStockDialog.OnSelectBankStockListener bankStockListener = new SlidingExpandBankStockDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bank, String code) {
                SlidingAccountInputDialog accountInputDialog = new SlidingAccountInputDialog(SolutionTestActivity.this, code, bank, accountInputListener);
                accountInputDialog.show();
            };
        };


        SlidingExpandBankStockDialog bankStockDialog = new SlidingExpandBankStockDialog(SolutionTestActivity.this, bankList, stockList, bankStockListener);
        bankStockDialog.show();

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_CAMERA: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startOCRActivity();
                }
                break;
            }
            /*
            case Const.REQUEST_PERMISSION_EXTERNAL_STORAGE: {
                boolean isPerDeny = false;
                for (int i = 0; i < permissions.length; i++) {
                    Logs.i("permissions." + i + "." + permissions[i] + " : " + grantResults[i]);
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        String msg;

                        boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]);

                        if (state) {
                            msg = "해당 접근권한을 허용하셔야 APK파일을 저장할 수 있습니다.";
                        } else {
                            msg = "앱 설정에서 저장소 접근권한을 허용하셔야 APK파일을 저장할 수 있습니다.";
                        }

                        DialogUtil.alert(this, msg, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //setResult(RESULT_CANCELED, null);
                                //finish();
                            }
                        });
                        isPerDeny = true;
                        break;
                    }
                }

                if (!isPerDeny) {
                    downloadAPK();
                }
                break;
            }
            */

            case Const.REQUEST_PERMISSION_SYNC_CONTACTS: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    syncListContacts();
                } else {
                    showErrorMessage(getString(R.string.common_msg_sync_permission));
                }
                break;
            }

            default:
                break;
        }
    }

    /*
    private void downloadAPK() {
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.SMP0030101A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if (TextUtils.isEmpty(ret)) {
                    Logs.showToast(SolutionTestActivity.this, "서버응답이 없습니다.");
                    showErrorMessage("서버응답이 없습니다.");
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.showToast(SolutionTestActivity.this, "앱버전체크 에러");
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage("앱버전체크 에러");
                        return;
                    }

                    Logs.e("ret : " + ret);

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        Logs.showToast(SolutionTestActivity.this, msg);
                        showErrorMessage(msg);
                        return;
                    }

                    String serverVersion = object.optString("TRMN_APSF_VRSN");
                    String appVersion = Utils.getVersionName(SolutionTestActivity.this);
                    Logs.e("serverVersion : " + serverVersion);
                    Logs.e("appVersion : " + appVersion);
                    if (!TextUtils.isEmpty(serverVersion) && !appVersion.equals(serverVersion)) {
                        DialogUtil.alert(SolutionTestActivity.this,
                                "업데이트",
                                "취소",
                                "새로운 버전이 업데이트되었습니다. 다운로드하시겠습니까?\n(" + appVersion + " --> " + serverVersion + ")",

                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Intent intent = new Intent(SolutionTestActivity.this, NewApkDownloadActivity.class);
                                        startActivity(intent);
                                    }
                                },

                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                    } else {
                        DialogUtil.alert(SolutionTestActivity.this,
                                "최신 버전이 발견되지 않았습니다.\n(현재 버전 : " + appVersion + ")",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                    }
                                });
                    }

                } catch (JSONException e) {
                    Logs.printException(e);
                }

            }
        });
    }
*/


    private void startOCRActivity() {
        Intent intent = new Intent(SolutionTestActivity.this, CameraActivity.class);
        intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
        intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
        intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
        intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
        intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, Const.ENCRYPT_KEY);
        startActivityForResult(intent, REQ_OCR_ID);
        overridePendingTransition(0, 0);
    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQ_OCR_ID:
                if (resultCode == CameraActivity.RETURN_OK && data != null) {

                    /*
                    * 20180906 - SBI는 신분증 데이타를 뽑지 않고 BMP이미지만 서버로 전달한다.
                    *
                    ArrayList<String> resultText = data.getStringArrayListExtra(CameraActivity.DATA_RESULT_TEXT);

                    //암호화키 입력시 : 암호화값이 리턴되어 복호화 필요함
                    if( ENCRYPT_KEY.length() >= 1) {
                        for (int i = 0; i < resultText.size(); i++) {
                            resultText.set(i, Utils.decrypt(resultText.get(i), ENCRYPT_KEY));
                        }
                    }


                    Logs.e("mResultText_return : " + OcrUtils.parsorIdcardData(resultText));


                    Bitmap bmp = OcrUtils.makeMaskImg(data);
                    mIDCardImg.setImageBitmap(bmp);
                    //마스킹한 이미지를 가져온다.
                    //new OcrImgMaskTask(this).execute(data);
                    */
                    Bitmap idCardBitmap = null;
                    // 신분증 이미지 영역
                    byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
                    imageOCR =  SelvyIDCardRecognizer.decrypt(imageOCR, Const.ENCRYPT_KEY); // 이미지 복호화 추가

                    idCardBitmap = ImgUtils.byteArrayToBitmap(imageOCR);
                    mIDCardImg.setImageBitmap(idCardBitmap);
                }
                break;

            case REQUEST_PINAUTH: {
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        Logs.e(commonUserInfo.getPlainData());
                        Logs.showToast(this, "비밀번호 : " + commonUserInfo.getPlainData());
                    }
                }
                break;
            }
            case REQUEST_SEARCH_ADDRESS: {
                if (resultCode == RESULT_OK) {
                    if (data != null) {
                        String address = data.getStringExtra(Const.INTENT_RESULT_ADDRESS);
                        String addressDetail = data.getStringExtra(Const.INTENT_RESULT_ADDRESS_DETAIL);

                        DialogUtil.alert(this,address + " " + addressDetail);

                    }
                }
                break;
            }
            default:
                break;
        }
    }

    @Override
    public void onEndOcrMasking(boolean result, Bitmap bmp) {
        if (result) {
            mIDCardImg.setImageBitmap(bmp);
            //byte[] jpegbyte = OcrUtils.bitmapToByteArray(bmp);//jpeg byte로 변환해서 넘져준다.
            //String str1 = Base64.encodeToString(jpegbyte, 0);//웹에 넘겨줄때는 Base64로 인코딩해서 넘겨준다.
        }
    }

    private void checkV3MobilePlus() {
        mV3Manager = V3Manager.getInstance(getApplicationContext());
        mV3Manager.setV3CompleteListener(this);

        int ret = mV3Manager.startV3MobilePlus(LicenseKey.V3_LICENSE_KEY);
        if (ret == V3MobilePlusCtl.ERR_FAILURE) {
            finish();
            return;
        }
    }

    @Override
    public void onCompleteV3() {
        Logs.i("solution", "----------- onCompleteV3()");
    }

    /**
     * datepicker dialog에서 선태한 값을 받는 리스너
     *
     * @param year           선택한 년도
     * @param month          선택한 달
     * @param day            선택한 일
     * @param time           선택한 시간
     * @param needTimeSelect time select로 넘가가야하면 true
     * @param isToday        당일이면 true
     */
    @Override
    public void onConfirmPress(int year, int month, int day, int time, boolean needTimeSelect, boolean isToday, boolean isDelayService) {
        if (needTimeSelect) {
            // 이체날짜 선택 후 시간을 선택하기 위해 다시 다이얼로그 호출
            Logs.e("year : " + year + ", month : " + month + ", day : " + day);
            SlidingDateTimerPickerDialog timepickerslidingDialog = new SlidingDateTimerPickerDialog(SolutionTestActivity.this, Const.PICKER_TYPE_TIME, isToday, isDelayService);
            timepickerslidingDialog.setOnConfirmListener(this);
            timepickerslidingDialog.show();
        } else {
            Logs.e("time : " + time);
        }
    }

    private Map<String, String> getServerCallbackArgs() {
        Map<String, String> callbackParameters = new HashMap<>();
        callbackParameters.put("user_id", "1234");
        callbackParameters.put("title", "프로방스 자동차 여행 !@#$%");
        return callbackParameters;
    }

    /**
     * 간편이체 (휴대폰번호로 이체 기능)
     */

    /**
     * 간편이체약관동의여부관리
     */
    private void requestIsAgreeTransferPhone() {
        Map param = new HashMap();
        param.put("TRN_DVCD", "1");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                Logs.i("TRA0020100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        showErrorMessage(msg);
                        return;
                    }

                    String SMPL_TRNF_STPL_AGR_YN = object.optString("SMPL_TRNF_STPL_AGR_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SMPL_TRNF_STPL_AGR_YN)) {
                        checkContactsPermission();
                    } else {
                        Intent transferIntent = new Intent(SolutionTestActivity.this, TransferPhonePolicyActivity.class);
                        startActivity(transferIntent);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 휴대폰 주소록 동기화를 위한 퍼시션 획득
     */
    private void checkContactsPermission() {
        String[] perList = new String[]{
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if (PermissionUtils.checkPermission(SolutionTestActivity.this, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), Const.CONTACTS_INFO_PATH);
            if (file.exists()) {
                getListContacts();
            } else {
                syncListContacts();
            }
        }
    }

    /**
     * 저장된 주소록 파일에서 등록된 휴대폰 리스트 획득
     */
    private void getListContacts() {
        StringBuffer sb = new StringBuffer("");
        try {
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            String path = storageDir + "/" + Const.CONTACTS_INFO_PATH;
            FileInputStream fIn = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fIn);
            BufferedReader buffreader = new BufferedReader(isr);
            String readString = buffreader.readLine();
            while (readString != null) {
                sb.append(readString);
                readString = buffreader.readLine();
            }
            isr.close();

            if (!TextUtils.isEmpty(sb)) {
                try {
                    JSONObject object = new JSONObject(sb.toString());
                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null)
                        return;

                    ArrayList<ContactsInfo> listContacts = new ArrayList<>();
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);
                        if (contactsInfo == null)
                            continue;

                        listContacts.add(contactsInfo);
                    }
                    showTransferPhone(listContacts);
                } catch (JSONException e) {

                }
            }

        } catch (IOException ioe) {
        }
    }

    /**
     * 폰에 등록된 리스트 획득 후 동기화 요청
     */
    private void syncListContacts() {
        ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();

        ContactsInfo contactsInfo;
        Cursor contactCursor = null;

        String name;
        String phonenumber;

        try {
            Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_ID};

            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

            contactCursor = getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
            if (contactCursor.moveToFirst()) {
                do {
                    phonenumber = contactCursor.getString(1);
                    if (phonenumber.length() <= 0)
                        continue;

                    name = contactCursor.getString(2);
                    phonenumber = phonenumber.replaceAll("-", "");
                    Logs.e("phonenumber : " + phonenumber);
                    if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                        continue;
                    }

                    // 리스트에 추가할 조건 : 리스트 내 동일 이름항목이 없고 핸드폰 번호인 경우
                    if (listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                        contactsInfo = new ContactsInfo();
                        contactsInfo.setTLNO(phonenumber);
                        contactsInfo.setFLNM(name);
                        listContactsInPhonebook.add(contactsInfo);
                    }
                } while (contactCursor.moveToNext());
            }
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            if (contactCursor != null) {
                contactCursor.close();
            }

            requestCheckContacts(listContactsInPhonebook);
        }
    }

    /**
     * 휴대전화번호로 고객정보조회
     *
     * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
     */
    void requestCheckContacts(final ArrayList<ContactsInfo> listContactsInPhonebook) {
        Map param = new HashMap();
        JSONArray jsonArray = new JSONArray();
        for (int index = 0; index < listContactsInPhonebook.size(); index++) {
            ContactsInfo contactsInfo = listContactsInPhonebook.get(index);
            String TLNO = contactsInfo.getTLNO();
            if (TextUtils.isEmpty(TLNO))
                continue;

            try {
                JSONObject itemObject = new JSONObject();
                itemObject.put("TLNO", TLNO);

                String FLNM = contactsInfo.getFLNM();
                if (TextUtils.isEmpty(FLNM)) FLNM = "";
                itemObject.put("FLNM", FLNM);

                jsonArray.put(itemObject);
            } catch (JSONException e) {
            }
        }
        param.put("REC_IN_TLNO", jsonArray);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0020200A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showTransferPhone(listContactsInPhonebook);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null) {
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                    if (!storageDir.exists())
                        storageDir.mkdir();

                    String path = storageDir + "/" + Const.CONTACTS_INFO_PATH;
                    try {
                        FileWriter fw = new FileWriter(path, false);
                        fw.write(ret);
                        fw.flush();
                        fw.close();
                    } catch (Exception e) {
                        //e.printStackTrace();
                    }

                    ArrayList<ContactsInfo> listContacts = new ArrayList<>();
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);
                        if (contactsInfo == null)
                            continue;

                        listContacts.add(contactsInfo);
                    }

                    showTransferPhone(listContacts);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 휴대전화번호 이체화면으로 이동
     */
    private void showTransferPhone(ArrayList<ContactsInfo> listContacts) {
        Intent intent = new Intent(SolutionTestActivity.this, TransferPhoneActivity.class);
        intent.putParcelableArrayListExtra(Const.INTENT_TRANSFER_PHONE_INFO, listContacts);
        startActivity(intent);
    }

    String checkTestUrl(String url) {
        if (TextUtils.isEmpty(url))
            return "";

        int curServerIndex = SaidaUrl.serverIndex;
        if (url.startsWith("https://devapp.")) {
            if (curServerIndex == Const.DEBUGING_SERVER_TEST) {
                String newUrl = url.replace("https://devapp.", "https://testapp.");
                return newUrl;
            }
        } else if (url.startsWith("https://testapp.")) {
            if (curServerIndex == Const.DEBUGING_SERVER_DEV) {
                String newUrl = url.replace("https://testapp.", "https://devapp.");
                return newUrl;
            }
        }

        return url;
    }
}
