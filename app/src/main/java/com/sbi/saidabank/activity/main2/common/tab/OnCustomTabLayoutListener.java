package com.sbi.saidabank.activity.main2.common.tab;

public interface OnCustomTabLayoutListener {
    void onClickHomeMenu(int position);
    void onClickTab(int position);
}
