package com.sbi.saidabank.activity.transaction.itransfer;

import android.widget.EditText;

public interface OnITransferMultiActionListener {

    void makeMultiTransInfoList();
    void notifyDataChange();
    void notifyItemChange(int position);
    boolean changeStateBtnTransfer();
    void setCurrentFocusItem(int position);
    /**
     * 보낼일시 선택할때
     */
    void showDatePicker(int position);
    void showSelectSendAccountDialog();
}
