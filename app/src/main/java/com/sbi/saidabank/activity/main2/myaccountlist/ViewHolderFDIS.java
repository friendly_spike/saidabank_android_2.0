package com.sbi.saidabank.activity.main2.myaccountlist;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

/**
 * 정기예금 과 적금을 표시하는 뷰
 *
 * FixedDeposit : 정기예금
 * InstallmentSavings : 적금
 *
 */
public class ViewHolderFDIS extends BaseViewHolder<Main2AccountInfo> {
    private String[] mCodeWeek = {"일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"};

    private View mItemView;
    private LinearLayout mLayoutBody;   //전체몸통
    private ImageView mIvShadow;
    private TextView mTvAccName;    //계좌명
    private TextView mTvAccType;    //만기여부표시
    private TextView mTvAmount;     //계좌금액
    private TextView mTvAccNum;     //계좌번호
    private TextView mTvPaymentDay; //월납입일
    private RelativeLayout mLayoutCouple;



    public static ViewHolderFDIS newInstance(ViewGroup parent,int dispType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_main2_home_myaccount_list_item_fdis, parent, false);
        return new ViewHolderFDIS(parent.getContext(),itemView,dispType);
    }

    public ViewHolderFDIS(Context context, @NonNull View itemView,int dispType) {
        super(context,itemView,dispType);

        mItemView = itemView;

        int radius = (int)Utils.dpToPixel(mContext,20);

        mLayoutBody     = itemView.findViewById(R.id.layout_body);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));

        mIvShadow     = itemView.findViewById(R.id.iv_shadow);

        mTvAccName      = itemView.findViewById(R.id.tv_account_name);
        mTvAccNum       = itemView.findViewById(R.id.tv_account_num);
        mTvAccType      = itemView.findViewById(R.id.tv_account_type);
        mTvAmount       = itemView.findViewById(R.id.tv_amount);
        mTvPaymentDay   = itemView.findViewById(R.id.tv_payment_day);
        mLayoutCouple = itemView.findViewById(R.id.layout_couple);
        mDispType = dispType;

        mItemView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        //레이아웃의 높이를 재조정해둔다.
                        ViewGroup.LayoutParams params = mIvShadow.getLayoutParams();
                        params.width = mItemView.getWidth();
                        params.height = mItemView.getHeight();
                        mIvShadow.setLayoutParams(params);

                        mItemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    @Override
    public void onBindView(Main2AccountInfo mainAccountInfo) {

        mLayoutBody.setTag(mainAccountInfo);
        mLayoutBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                startWebAction(mainAccountInfo, new Main2AccountAdapter.OnProcWebActionListener() {
                    @Override
                    public void onOKWebAction(Object accountInfo) {
                        Main2AccountInfo mainAccountInfo = (Main2AccountInfo)accountInfo;

                        if(mainAccountInfo.getDSCT_CD().equals("400")){
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.UNT0360100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                            String ACNO = mainAccountInfo.getACNO();
                            String param =  "ACNO=" + ACNO;
                            intent.putExtra(Const.INTENT_PARAM, param);
                            mContext.startActivity(intent);

                        }else{
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.UNT0370100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                            String ACNO = mainAccountInfo.getACNO();
                            String param =  "ACNO=" + ACNO;
                            intent.putExtra(Const.INTENT_PARAM, param);
                            mContext.startActivity(intent);
                        }
                    }

                    @Override
                    public void onErrorWebAction(String code, Object accountInfo) {
                        if(code.equals("99")){
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.MAI0070100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                            mContext.startActivity(intent);
                        }
                    }
                });

            }
        });

        mTvAccName.setText(mainAccountInfo.getTRNF_DEPR_NM());

        String accountNum = mainAccountInfo.getACNO();
        if (!TextUtils.isEmpty(accountNum)) {
            String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
            mTvAccNum.setText(account);
        }

        //만기여부 출력
        if(mainAccountInfo.getEXPT_YN().equals("Y")){
            mTvAccType.setVisibility(View.VISIBLE);
            mTvAccType.setText("만기");
        }else{
            mTvAccType.setVisibility(View.GONE);
        }
        //커플계좌 탭이고 공유자이면 계좌의 상태(거래정지,지급정지,출금정지,한도제한)을 보이면 않된다.
        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
            mTvAccType.setVisibility(View.INVISIBLE);
        }


        String BLNC = mainAccountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            mTvAmount.setText(BLNC);
        }

        if(mainAccountInfo.getDSCT_CD().equals("400")){
            mTvPaymentDay.setVisibility(View.INVISIBLE);
        }else{
            mTvPaymentDay.setVisibility(View.VISIBLE);
            getPaymentDay(mainAccountInfo);
        }

        Logs.e("getSHRN_ACCO_YN : " + mainAccountInfo.getSHRN_ACCO_YN());
        if(mainAccountInfo.getSHRN_ACCO_YN().equals("Y")){

            if(mDispType==Main2AccountAdapter.DISP_TYPE_MY){
                ImageView imgView = new ImageView(mContext);
                imgView.setImageResource(R.drawable.ico_account_couple);
                mLayoutCouple.addView(imgView);
            }
        }else{

            if(mLayoutCouple.getChildCount() > 0)
                mLayoutCouple.removeAllViews();
        }

    }

    private void getPaymentDay(Main2AccountInfo mainAccountInfo){
        String CANO = mainAccountInfo.getCANO();//CANO 주기값이 들어온다.
        if (!TextUtils.isEmpty(CANO)) {
            String MM_PI_DD = mainAccountInfo.getMM_PI_DD();
            if ("01".equalsIgnoreCase(CANO)) {
                if (!TextUtils.isEmpty(MM_PI_DD))
                    mTvPaymentDay.setText("매월 " + MM_PI_DD + "일");
                else
                    mTvPaymentDay.setText("");

            } else if ("07".equalsIgnoreCase(CANO)) {
                if (!TextUtils.isEmpty(MM_PI_DD)) {
                    int code = Integer.valueOf(MM_PI_DD);
                    String strCode = mCodeWeek[code];
                    if (!TextUtils.isEmpty(strCode))
                        mTvPaymentDay.setText("매주 "  + strCode);
                    else
                        mTvPaymentDay.setText("");
                }
            } else if ("99".equalsIgnoreCase(CANO)) {
                mTvPaymentDay.setText("매일");
            }
        }else{
            mTvPaymentDay.setText("");
            mTvPaymentDay.setVisibility(View.INVISIBLE);
        }
    }

}
