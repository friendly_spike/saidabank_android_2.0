package com.sbi.saidabank.activity.main2.common.clipboard;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FastTransferLayout extends RelativeLayout {

    private Context mContext;
    private String mBankStockName;
    private String mAccountNum;
    private RequestCodeInfo mCodeInfo;
    private String mClipText;

    private TextView mTvBankAccount;
    private ImageView mIvClear;

    private class ParserStringType{
        public String mText;
        public boolean isIncludeNumber;
    }

    public FastTransferLayout(Context context) {
        super(context);
        init(context);
    }

    public FastTransferLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context){
        mContext = context;

        View layout = View.inflate(context, R.layout.layout_main2_fast_transfer, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));


        int radius = (int)Utils.dpToPixel(getContext(),29);
        findViewById(R.id.layout_body).setBackground(GraphicUtils.getRoundCornerDrawable("#F23e3e3e",new int[]{radius,radius,radius,radius}));

        layout.findViewById(R.id.layout_body).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
                    @Override
                    public void onCheckFinish() {

                        //메인계좌 내용을 넣어준다.
                        MyAccountInfo accountInfo = TransferUtils.getSaidaMainAccount();
                        if(accountInfo == null){
                            DialogUtil.alert(mContext,"보유계좌가 없습니다.");
                            return;
                        }
                        ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                        ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.getACNO());

                        //받는분 선택으로 이동
                        Intent intent = new Intent(getContext(), ITransferSelectReceiverActivity.class);
                        intent.putExtra(Const.INTENT_RCV_BANK_CODE, mCodeInfo.getSCCD());
                        String restStr = mAccountNum.replaceAll("[^0-9]","");
                        if(mAccountNum.startsWith("028")){
                            intent.putExtra(Const.INTENT_RCV_BANK_CODE, "028");
                        }else if(mAccountNum.startsWith("000")){
                            intent.putExtra(Const.INTENT_RCV_BANK_CODE, "000");
                        }

                        intent.putExtra(Const.INTENT_RCV_ACCOUNT, restStr);
                        getContext().startActivity(intent);

                        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                        if(DataUtil.isNotNull(clipboard.getPrimaryClipDescription())) {
                            Prefer.setLastCheckClipboardDesc(getContext(), clipboard.getPrimaryClipDescription().toString());
                        }
                    }
                });

                //바로 사라지면 이상해 보여서 약간 딜레이주고 사라지도록...
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setVisibility(View.GONE);
                    }
                },500);

            }
        });

        mIvClear = layout.findViewById(R.id.iv_clear);
        mIvClear.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //비교할 문자열을 저장해 둔다. 다음에 재 진입시 비교하지 않기 위해.
                ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                if(DataUtil.isNotNull(clipboard.getPrimaryClipDescription()))
                    Prefer.setLastCheckClipboardDesc(getContext(), clipboard.getPrimaryClipDescription().toString());

                setVisibility(View.GONE);
            }
        });


        mTvBankAccount = layout.findViewById(R.id.tv_bank_account);

        setVisibility(View.GONE);
    }

    public void checkCopyClipboardText(){
        mBankStockName = "";
        mAccountNum = "";

        //Firebase방어코드 추가
        if(((Activity)mContext).isFinishing()) return;

        mClipText = getClipboardData();
        if(!TextUtils.isEmpty(mClipText)) {

            parserClipboardText(mClipText);

            Logs.e("FastTransferLayout - mBankStockName : " + mBankStockName);
            Logs.e("FastTransferLayout - mAccountNum : " + mAccountNum);

            if(!TextUtils.isEmpty(mBankStockName)){
                mCodeInfo = PreloadBankStockCodeMgr.getInstance().getBankStockCodeInfo(getContext(),mBankStockName);
                if(mCodeInfo != null && !TextUtils.isEmpty(mAccountNum)){
                    mTvBankAccount.setText(mCodeInfo.getCD_NM() + " "+ mAccountNum);
                    setVisibility(VISIBLE);
                }
            }
        }
    }

    private String getClipboardData(){
        ClipboardManager clipboard = (ClipboardManager) getContext().getSystemService(Context.CLIPBOARD_SERVICE);

        ClipDescription clipDescription = clipboard.getPrimaryClipDescription();

        String clipDesc = "";
        if(clipDescription != null)
            clipDesc = clipDescription.toString();

        String lastCheckClipDesc = Prefer.getLastCheckClipboardDesc(getContext());
        if(lastCheckClipDesc.equals(clipDesc)) return "";

        ClipData clipData = clipboard.getPrimaryClip();
        if(DataUtil.isNotNull(clipData)){
            ClipData.Item item = clipData.getItemAt(0);

            if(DataUtil.isNotNull(item.getText())){
                String pasteData =  item.getText().toString();
                pasteData = pasteData.replace(" ","");

                return pasteData;
            }
        }

        return null;
    }

    private void parserClipboardText(String clipText){
        Logs.e("parserClipboardText - clipText : " + clipText);
        ArrayList<ParserStringType> strArray = new ArrayList<ParserStringType>();

        StringBuilder newStrBuilder = new StringBuilder();
        boolean isNumber=false;
        for(int i=0;i<clipText.length();i++) {

            char check = clipText.charAt(i);


            if (check == '-' || check == '_' || check == ',' || check == '.') {
                if (i > 0) {
                    newStrBuilder.append(check);
                }
                continue;
            }

            if (i == 0) {
                if (check >= 48 && check <= 58) {
                    isNumber = true;
                } else {
                    isNumber = false;
                }
            } else {
                //아스키코드값 0~9
                if (check >= 48 && check <= 57) {
                    if (!isNumber) {
                        ParserStringType stringType = new ParserStringType();
                        stringType.mText = newStrBuilder.toString();
                        stringType.isIncludeNumber = false;
                        strArray.add(stringType);

                        newStrBuilder.delete(0,newStrBuilder.length());
                        //newStrBuilder.append(" ");
                    }
                    isNumber = true;
                } else {
                    if (isNumber) {
                        ParserStringType stringType = new ParserStringType();
                        stringType.mText = newStrBuilder.toString();
                        stringType.isIncludeNumber = true;
                        strArray.add(stringType);

                        newStrBuilder.delete(0,newStrBuilder.length());
                        //newStrBuilder.append(" ");
                    }
                    isNumber = false;
                }
            }
            newStrBuilder.append(check);

            if(i+1 == clipText.length()){
                ParserStringType stringType = new ParserStringType();
                stringType.mText = newStrBuilder.toString();
                stringType.isIncludeNumber = isNumber;
                strArray.add(stringType);
            }
        }

        if(strArray.size() == 0) return;

        //Logs.e("FastTransferLayout - strArray.size() : " + strArray.size());


        for(int i=0;i < strArray.size();i++){
            ParserStringType info = strArray.get(i);
            //Logs.e("FastTransferLayout - info.mText : " + info.mText);
            //Logs.e("FastTransferLayout - info.isIncludeNumber : " + info.isIncludeNumber);
            if(info.isIncludeNumber){
                String restStr = info.mText.replaceAll("[^0-9]","");
                if(!TextUtils.isEmpty(restStr) && restStr.length() >= 7){
                    mAccountNum = info.mText;
                }
            }else{
                if(info.mText.length()>=2){
                    mBankStockName = info.mText;
                }
            }
        }
    }

    public void moveUpDown(boolean isDown){
        if(getVisibility() != VISIBLE) return;

        int moveY = (int) Utils.dpToPixel(getContext(),59);
        if(isDown){
            animate()
                    .translationY(moveY)
                    .setDuration(250)
                    .start();
        }else{
            animate()
                    .translationY(0)
                    .setDuration(250)
                    .start();
        }
    }
}
