package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: TransferAccountAdapter
 * Created by 950485 on 2019. 01. 02..
 * <p>
 * Description:아래에서 위로 올라오는 아래에서 위로 올라오는 최근/자주 이체한 계좌번호 다이얼로그 adapter
 */
public class SafeDealMyAccountAdapter extends BaseAdapter {

    private Context context;
    private MyAccountInfo mSelectMyAccountInfo;
    private ArrayList<MyAccountInfo> dataList;
    private int mSelectIdx;
    public SafeDealMyAccountAdapter(Context context, ArrayList<MyAccountInfo> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.mSelectIdx = 0;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        SafeDealMyAccountAdapter.AccountViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_transfer_safe_deal_my_account_list_item, null);
            viewHolder = new AccountViewHolder();
            viewHolder.imageRepresentation = (ImageView) convertView.findViewById(R.id.imageview_representation);
            //viewHolder.layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_my_account_list_item);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textview_name_my_account);
            viewHolder.imageShare = (ImageView) convertView.findViewById(R.id.imageview_share);
            viewHolder.textWithdrawableAmt = (TextView) convertView.findViewById(R.id.textview_withdrawable_my_account);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SafeDealMyAccountAdapter.AccountViewHolder) convertView.getTag();
        }

        MyAccountInfo accountInfo = (MyAccountInfo) dataList.get(position);

        if (DataUtil.isNull(accountInfo)) return  convertView;

        //대표계좌여부
        //if(accountInfo.getRPRS_ACCO_YN().equals("Y")){
        if(mSelectIdx == position){
            viewHolder.imageRepresentation.setVisibility(View.VISIBLE);
        }else{
            viewHolder.imageRepresentation.setVisibility(View.INVISIBLE);
        }

        if(mSelectMyAccountInfo.getACNO().equals(accountInfo.getACNO())){
            viewHolder.textName.setTextColor(ContextCompat.getColor(context, R.color.color888888));
            viewHolder.textWithdrawableAmt.setTextColor(ContextCompat.getColor(context, R.color.color888888));
        }else{
            viewHolder.textName.setTextColor(ContextCompat.getColor(context, R.color.black));
            viewHolder.textWithdrawableAmt.setTextColor(ContextCompat.getColor(context, R.color.black));
        }

        String ACCO_ALS = accountInfo.getACCO_ALS();
        if (TextUtils.isEmpty(ACCO_ALS)) {
            ACCO_ALS = accountInfo.getPROD_NM();
        }

        String ACNO = accountInfo.getACNO();
        if (!TextUtils.isEmpty(ACNO)) {
            int lenACNO = ACNO.length();
            if (lenACNO > 4)
                ACNO = ACNO.substring(lenACNO - 4, lenACNO);
        }

        String name = ACCO_ALS + " [" + ACNO + "]";
        viewHolder.textName.setText(name);

        String ACCO_BLNC = accountInfo.getACCO_BLNC();
        if (!TextUtils.isEmpty(ACCO_BLNC)) {
            String amount = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
            viewHolder.textWithdrawableAmt.setText(amount + context.getString(R.string.won));
        }


        if (!TextUtils.isEmpty(accountInfo.getSHRN_ACCO_YN()) && accountInfo.getSHRN_ACCO_YN().equals("Y")) {
            viewHolder.imageShare.setVisibility(View.VISIBLE);
        }else{
            viewHolder.imageShare.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    public void setSelectAccountInfo(MyAccountInfo accountInfo){
        mSelectMyAccountInfo = accountInfo;
    }

    public void setSelectIndex(int idx){
        mSelectIdx = idx;
        notifyDataSetChanged();
    }

    static class AccountViewHolder {
        //LinearLayout layoutItem;
        ImageView imageRepresentation;
        TextView textName;
        TextView textWithdrawableAmt;
        ImageView imageShare;
    }
}
