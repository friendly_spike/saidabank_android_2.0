package com.sbi.saidabank.activity.main2.myaccountlist;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.dialog.SlidingMain2TransferDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CoupleIconSmallView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DivideAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import java.util.ArrayList;

/**
 * 계좌분리 아이템 표시
 * 중요!!! 마이너스통장은 계좌쪼개기를 할수 없다.
 */
public class ViewHolderDevide extends BaseViewHolder<Main2AccountInfo>{
    private View mItemView;
    private LinearLayout mLayoutBody;   //전체몸통
    private ImageView mIvShadow;

    private RelativeLayout mLayoutTotalAmount; //총금액 레이아웃
    private TextView mTvTotalAmount;    //총금액

    private LinearLayout mLayoutItemMain; //첫 아이템 표시 레이아웃

    //아래변수는 일반 요구불 계좌 화면과 같이 가도록 한다.
    private TextView mTvAccName;        //계좌명
    private ImageView mIvCopyAccNum;    //계좌번호 복사버튼
    private TextView mTvAccType;        //계좌종류
    private TextView mTvAccNum;         //계좌번호
    private TextView mTvAmount;         //계좌금액
    private TextView mTvPaymentDay;     //월납입일

    //대출 여신거래 상태 레이아웃
    //private RelativeLayout mLayoutBtnAndDocRelay;
    private LinearLayout   mLayoutDocRelay;
    private TextView       mTvDocRelayMsg;
    private TextView       mTvDocJudgeMsg;


    //이체 충전 버튼 레이아웃
    private LinearLayout mLayoutBtn;    //버튼 전체 레이아웃
    private LinearLayout mLayoutBtnTransfer;   //이체버튼레이아웃
    //private LinearLayout mLayoutBtnCharge;     //충전버튼레이아웃
    private LinearLayout mLayoutBtnMoveAmount; //금액이동레이아웃

    private TextView     mTvBtnTransfer;  //이체버튼
    private TextView     mTvBtnCharge;    //충전버튼
    private TextView     mTvMoveAmount;   //금액이동

    private LinearLayout mLayoutDivideAccount;
    private RelativeLayout mLayoutCouple;

    public static ViewHolderDevide newInstance(ViewGroup parent,int dispType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_main2_home_myaccount_list_item_divide, parent, false);
        return new ViewHolderDevide(parent.getContext(),itemView, dispType);
    }

    public ViewHolderDevide(Context context, @NonNull View itemView,int dispType) {
        super(context,itemView,dispType);

        mItemView = itemView;

        mDispType = dispType;

        int radius = (int)Utils.dpToPixel(mContext,20);

        mLayoutBody     = itemView.findViewById(R.id.layout_body);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));

        mIvShadow     = itemView.findViewById(R.id.iv_shadow);

        mLayoutTotalAmount = itemView.findViewById(R.id.layout_total);
        mTvTotalAmount = itemView.findViewById(R.id.tv_total_amount);

        mLayoutItemMain = itemView.findViewById(R.id.layout_item_main);

        mTvAccName      = itemView.findViewById(R.id.tv_account_name);
        mIvCopyAccNum   = itemView.findViewById(R.id.iv_copy_account_num);
        mIvCopyAccNum.setVisibility(View.VISIBLE);

        mTvAccType      = itemView.findViewById(R.id.tv_account_type);
        mTvAccNum       = itemView.findViewById(R.id.tv_account_num);
        mTvAmount       = itemView.findViewById(R.id.tv_amount);
        mTvPaymentDay   = itemView.findViewById(R.id.tv_payment_day);

        //마이너스통장 여신 상태메세지
        //mLayoutBtnAndDocRelay  = itemView.findViewById(R.id.layout_doc_relay_judge_msg);
        //mLayoutBtnAndDocRelay.setVisibility(View.GONE);
        mLayoutDocRelay        = itemView.findViewById(R.id.layout_doc_relay);
        mLayoutDocRelay.setVisibility(View.GONE);
        mTvDocRelayMsg         = itemView.findViewById(R.id.tv_doc_relay_msg);
        mTvDocJudgeMsg         = itemView.findViewById(R.id.tv_doc_judge_msg);
        mTvDocJudgeMsg.setVisibility(View.GONE);


        //이체,충전 버튼 영역
        mLayoutBtn  = itemView.findViewById(R.id.layout_btn);

        if(mDispType == Main2AccountAdapter.DISP_TYPE_MY){
            mLayoutBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#27aef4",new int[]{0,0,radius,radius}));
        }else{
            mLayoutBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#ff7e6d",new int[]{0,0,radius,radius}));
        }


        mLayoutBtnTransfer = itemView.findViewById(R.id.layout_btn_1);
        mLayoutBtnTransfer.setVisibility(View.GONE);
//        mLayoutBtnCharge   = itemView.findViewById(R.id.layout_btn_2);
//        mLayoutBtnCharge.setVisibility(View.GONE);
        mTvBtnTransfer  = itemView.findViewById(R.id.tv_btn_1);
//        mTvBtnCharge  = itemView.findViewById(R.id.tv_btn_2);
        mLayoutBtnMoveAmount  = itemView.findViewById(R.id.layout_btn_3);
        mTvMoveAmount = itemView.findViewById(R.id.tv_btn_3);

        mLayoutDivideAccount = itemView.findViewById(R.id.layout_divide_account);
        mLayoutCouple = itemView.findViewById(R.id.layout_couple);

        mItemView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        //레이아웃의 높이를 재조정해둔다.
                        ViewGroup.LayoutParams params = mIvShadow.getLayoutParams();
                        params.width = mItemView.getWidth();
                        params.height = mItemView.getHeight();
                        mIvShadow.setLayoutParams(params);

                        mItemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    @Override
    public void onBindView(Main2AccountInfo mainAccountInfo) {
        //총 금액 누르면 계좌의 거래내역으로 이동.
//20201008 - 권성훈과장 요청으로 변경. - 셀 누르면 전체 계좌 내역으로 이동.
//        mLayoutTotalAmount.setTag(mainAccountInfo);
//        mLayoutTotalAmount.setOnClickListener(new View.OnClickListener() {
        mLayoutBody.setTag(mainAccountInfo);
        mLayoutBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                startWebAction(mainAccountInfo, new Main2AccountAdapter.OnProcWebActionListener() {
                    @Override
                    public void onOKWebAction(Object accountInfo) {
                        Main2AccountInfo mainAccountInfo = (Main2AccountInfo) accountInfo;
                        String ACNO = mainAccountInfo.getACNO();
                        String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
                        if (TextUtils.isEmpty(ACNO)  || TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                            return;

                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        String url = WasServiceUrl.INQ0010100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                        String param =  "ACNO=" + ACNO;
                        intent.putExtra(Const.INTENT_PARAM, param);
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onErrorWebAction(String code, Object accountInfo) {
                        if(code.equals("99")){
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.MAI0070100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                            mContext.startActivity(intent);
                        }
                    }
                });
            }
        });

        //총금액
        String BLNC = mainAccountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            mTvTotalAmount.setText(BLNC+" 원");
        }

        //계좌이름 표시
        mTvAccName.setText(mainAccountInfo.getTRNF_DEPR_NM());
        String accountNum = mainAccountInfo.getACNO();
        if (!TextUtils.isEmpty(accountNum)) {
            String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
            mTvAccNum.setText(account);
        }

        //계좌번호 복사
        mIvCopyAccNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO - 클립보드에 번호 카피해둔다.
                //형태 : 저축 계좌번호
                String saveStr = "저축 " + mTvAccNum.getText().toString();
                ClipboardManager clipboard = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("simple text", saveStr);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mContext,"계좌번호가 복사되었습니다.",Toast.LENGTH_LONG).show();
            }
        });

        //커플통장 : 마이너스 통장 불가.
        //계좌 쪼개기는 마이너스 통장은 불가능하여 아래 코드는 필요가 없는듯 하나
        //요구불 계좌 처리와 비슷하게 유지시키기 위해 그냥 둔다.
        //일반계좌 : 거래정지 > 출금정지 > 한도제한
        //마이너스계좌 : 기한이익상실 > 거래정지 > 출금정지 >연체
        if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){
            //마이너스계좌
            if(mainAccountInfo.getOVRD_DVCD().equals("2")){
                mTvAccType.setText("기한이익상실");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));//빨강색
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("1")){
                mTvAccType.setText("거래정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("2")){
                mTvAccType.setText("지급정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("3")){
                mTvAccType.setText("출금정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getOVRD_DVCD().equals("1")){
                mTvAccType.setText("연체");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else{
                mTvAccType.setText("");
            }
        }else{
            //요구불계좌
            if(mainAccountInfo.getACDT_DCL_CD().equals("1")){
                mTvAccType.setText("거래정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("2")){
                mTvAccType.setText("지급정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("3")){
                mTvAccType.setText("출금정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getLMIT_LMT_ACCO_YN().equals("Y")){
                mTvAccType.setText("한도제한");
                mTvAccType.setTextColor(Color.parseColor("#009beb"));
                if(!TextUtils.isEmpty(mainAccountInfo.getFAX_SEND_PRGS_CNTN())){
                    mTvDocJudgeMsg.setVisibility(View.VISIBLE);
                    mTvDocJudgeMsg.setText(mainAccountInfo.getFAX_SEND_PRGS_CNTN());
                }
            }else{
                mTvAccType.setText("");
            }

            //커플계좌 탭이고 공유자이면 계좌의 상태(거래정지,지급정지,출금정지,한도제한)을 보이면 않된다.
            if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
                mTvAccType.setVisibility(View.INVISIBLE);
            }
        }



        //충전하기,금액이동 버튼 처리
//        displayChargeButton(mainAccountInfo);
        //이체하기 버튼 처리
        displayTransferButton(mainAccountInfo);
        //한도제한계좌 증빙서류 심사중 - 상태 메세지 출력
        displaylimitDocJudgeState(mainAccountInfo);

        //커플계좌의 공유자는 금액이동 버튼이 보이면 않된다.
        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
            mLayoutBtnMoveAmount.setVisibility(View.GONE);
        }else{
            mLayoutBtnMoveAmount.setVisibility(View.VISIBLE);
            //계좌분리 금액 이동 버튼
            mLayoutBtnMoveAmount.setTag(mainAccountInfo);
            mLayoutBtnMoveAmount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
                    startWebAction(mainAccountInfo, new Main2AccountAdapter.OnProcWebActionListener() {
                        @Override
                        public void onOKWebAction(Object accountInfo) {
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.UNT0171400.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                            mContext.startActivity(intent);
                        }

                        @Override
                        public void onErrorWebAction(String code, Object accountInfo) {

                        }
                    });
                }
            });
        }

        //첫번째 아이템 화면 처리
        displayItemZero();

        //나머지 화면 처리
        displayItemOther();

        //커플계좌이면 커플 아이콘 출력한다.
        Logs.e("getSHRN_ACCO_YN : " + mainAccountInfo.getSHRN_ACCO_YN());
        if(mainAccountInfo.getSHRN_ACCO_YN().equals("Y")){
            if(mDispType==Main2AccountAdapter.DISP_TYPE_MY){
                String ower_img_data = Main2DataInfo.getInstance().getUSER_PROFL_IMG_CNTN();
                String ower_name = Main2DataInfo.getInstance().getUSER_NM();
                String share_img_data = Main2DataInfo.getInstance().getSHRP_PROFL_IMG_CNTN();
                String share_name = Main2DataInfo.getInstance().getSHRP_NM();
                if(!TextUtils.isEmpty(share_name)){
                    CoupleIconSmallView coupleIconSmallView = new CoupleIconSmallView(mContext);
                    coupleIconSmallView.setData(ower_img_data,ower_name,share_img_data,share_name);
                    mLayoutCouple.addView(coupleIconSmallView);
                }else{
                    if(mLayoutCouple.getChildCount() > 0)
                        mLayoutCouple.removeAllViews();
                }
            }
            //계좌분리에서는 커플이체일을 표시하지 않는다.
        }else{
            if(mLayoutCouple.getChildCount() > 0)
                mLayoutCouple.removeAllViews();
        }


        //계좌분리 금액 이동버튼 위치 지정
//        mLayoutTotalAmount.measure(View.MeasureSpec.UNSPECIFIED,View.MeasureSpec.UNSPECIFIED);
//        int totalHeight = mLayoutTotalAmount.getMeasuredHeight();
//        mLayoutItemMain.measure(View.MeasureSpec.UNSPECIFIED,View.MeasureSpec.UNSPECIFIED);
//        int itemMainHeight = mLayoutItemMain.getMeasuredHeight();
//
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mIvMoveAmount.getLayoutParams();
//        params.topMargin = totalHeight + itemMainHeight - (int)Utils.dpToPixel(mContext,39f);
//        mIvMoveAmount.setLayoutParams(params);


    }

    /**
     * 계좌 쪼개기 아이템의 화면의 가장 큰 영역 아이템
     */
    private void displayItemZero(){
        ArrayList<Main2DivideAccountInfo> divideArrayList =  Main2DataInfo.getInstance().getMain2DivideAccountArray();

        if(divideArrayList.size() == 0) return;

        Main2DivideAccountInfo info = divideArrayList.get(0);

        mTvAccName.setText(info.getUSE_CLS_ACCO_ALS());

        String ACCO_BLNC = info.getACCO_BLNC();
        if (!TextUtils.isEmpty(ACCO_BLNC)) {
            ACCO_BLNC = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
            mTvAmount.setText(ACCO_BLNC);
        }

        String ATMT_SCHD_AMT = info.getATMT_SCHD_AMT();
        if (!TextUtils.isEmpty(ATMT_SCHD_AMT)) {
            ATMT_SCHD_AMT = Utils.moneyFormatToWon(Double.valueOf(ATMT_SCHD_AMT));
            mTvPaymentDay.setText("자동이체예정 : "+ ATMT_SCHD_AMT + "원");
        }


//20201008 - 권성훈 과장 요청으로 변경 - 개별거래내역으로 이동하지 않음. 그냥 전체 거래내역으로 이동.
//        mLayoutItemMain.setTag(info);
//        mLayoutItemMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Main2DivideAccountInfo info = (Main2DivideAccountInfo)v.getTag();
//                startWebAction(info, new Main2AccountAdapter.OnProcWebActionListener() {
//                    @Override
//                    public void onOKWebAction(Object accountInfo) {
//                        moveItemWebPage((Main2DivideAccountInfo)accountInfo);
//                    }
//
//                    @Override
//                    public void onErrorWebAction(String code, Object accountInfo) {
//                        if(code.equals("99")){
//                            Intent intent = new Intent(mContext, WebMainActivity.class);
//                            String url = WasServiceUrl.MAI0070100.getServiceUrl();
//                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                            mContext.startActivity(intent);
//                        }
//                    }
//                });
//
//            }
//        });

    }

    /**
     * 계좌를 쪼갠 아랫부분 아이템
     */
    private void displayItemOther(){

        mLayoutDivideAccount.removeAllViews();

        ArrayList<Main2DivideAccountInfo> divideArrayList =  Main2DataInfo.getInstance().getMain2DivideAccountArray();

        for(int i=1;i< divideArrayList.size();i++){
            Main2DivideAccountInfo info = divideArrayList.get(i);
            RelativeLayout layout = (RelativeLayout) View.inflate(mContext, R.layout.layout_main2_home_myaccount_list_divide_item, null);

            TextView  accName = layout.findViewById(R.id.tv_account_name);
            ImageView accdot  = layout.findViewById(R.id.iv_dot);
            TextView amount  = layout.findViewById(R.id.tv_amount);

            accName.setText(info.getUSE_CLS_ACCO_ALS());

            switch (i){
                case 1:
                    accdot.setImageResource(R.drawable.ico_rsquare_gr);
                    break;
                case 2:
                    accdot.setImageResource(R.drawable.ico_rsquare_rpu);
                    break;
                case 3:
                    accdot.setImageResource(R.drawable.ico_rsquare_pu);
                    break;
            }


            //금액
            String ACCO_BLNC = info.getACCO_BLNC();
            if (!TextUtils.isEmpty(ACCO_BLNC)) {
                ACCO_BLNC = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
                amount.setText(ACCO_BLNC+" 원");
            }

//20201008 - 권성훈 과장 요청으로 변경 - 개별거래내역으로 이동하지 않음. 그냥 전체 거래내역으로 이동.
//            layout.setTag(info);
//            layout.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Main2DivideAccountInfo info = (Main2DivideAccountInfo)v.getTag();
//
//                    startWebAction(info, new Main2AccountAdapter.OnProcWebActionListener() {
//                        @Override
//                        public void onOKWebAction(Object accountInfo) {
//                            moveItemWebPage((Main2DivideAccountInfo)accountInfo);
//                        }
//
//                        @Override
//                        public void onErrorWebAction(String code, Object accountInfo) {
//                            if(code.equals("99")){
//                                Intent intent = new Intent(mContext, WebMainActivity.class);
//                                String url = WasServiceUrl.MAI0070100.getServiceUrl();
//                                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                                mContext.startActivity(intent);
//                            }
//                        }
//                    });
//                }
//            });


            if(i +1 == divideArrayList.size()){
                layout.findViewById(R.id.v_line).setVisibility(View.GONE);
            }

            mLayoutDivideAccount.addView(layout);
        }

//        int radius = (int)Utils.dpToPixel(mContext,12);
//        mLayoutDivideAccount.setBackground(GraphicUtils.getRoundCornerDrawable("#f5ffff",new int[]{0,0,radius,radius},1,"#1A000000"));
    }

    /**
     * 쪼갠 아이템 들의 거래 내역으로 이동한다.
      * @param info
     */
    private void moveItemWebPage(Main2DivideAccountInfo info){
        Intent intent = new Intent(mContext, WebMainActivity.class);
        String url = WasServiceUrl.UNT0170600.getServiceUrl();
        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
        String param =  "ACDIV_USG_CD=" + info.getACDIV_USG_CD();

        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
            param = param + "&SHRP_TRN_YN=Y";
        }else{
            param = param + "&SHRP_TRN_YN=N";
        }

        Logs.e("moveItemWebPage - param : " +param);
        intent.putExtra(Const.INTENT_PARAM, param);
        mContext.startActivity(intent);
    }

    //충전하기,금액이동 버튼 처리
//    private void displayChargeButton(Main2AccountInfo mainAccountInfo){
//        //20201017 - 권성훈 - 사고계좌여도 충전버튼은 보여야 한다.
//        //커플계좌 탭으로 진입했고 커플 타입이 공유자일때
//        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE&& Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
//            mLayoutBtnCharge.setVisibility(View.GONE);
//        }else{
//            //충전버튼 출력 - 마이너스 통장은 계좌쪼개기를 하지 않기 때문에 마이너스에 대한 처리는 않해도 된다.
//            //if(mainAccountInfo.getIMMD_WTCH_ACCO_YN().equals("Y") && mainAccountInfo.getOVRD_DVCD().equals("0")){
//            if(mainAccountInfo.getIMMD_WTCH_ACCO_YN().equals("Y")){
//                mLayoutBtnCharge.setVisibility(View.VISIBLE);
//            }else{
//                //충전버튼 - 위 조건에서 충전버튼이 모두 Gone일경우 주계좌에만 충전버튼을 표시해 준다.
//                if(mainAccountInfo.getDSCT_CD().equals("300") && Main2DataInfo.getInstance().getACCOUNT_CHARGE_SET_YN().equals("N")){
//                    mLayoutBtnCharge.setVisibility(View.VISIBLE);
//                }else{
//                    mLayoutBtnCharge.setVisibility(View.GONE);
//                }
//            }
//        }
//
//        //충전하기 버튼 클릭!!!
//        mLayoutBtnCharge.setTag(mainAccountInfo);
//        mLayoutBtnCharge.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
//                String ACNO = mainAccountInfo.getACNO();
//
//                Intent intent = new Intent(mContext, WebMainActivity.class);
//                String url = WasServiceUrl.UNT0110100.getServiceUrl();
//                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//
//                String param = "DSCT_CD=0";
//                if(Main2DataInfo.getInstance().getACCOUNT_CHARGE_SET_YN().equals("Y")){
//                    param =  "ACNO=" + ACNO + "&" + "DSCT_CD=0";
//                }
//
//                intent.putExtra(Const.INTENT_PARAM, param);
//                mContext.startActivity(intent);
//            }
//        });
//    }

    //이체하기 버튼 처리
    private void displayTransferButton(Main2AccountInfo mainAccountInfo){
        //이체/상환 버튼 출력
        //사고신고코드로 구분. - 사고신고가 접수되면 버튼 모두 사라진다.
        if(TextUtils.isEmpty(mainAccountInfo.getACDT_DCL_CD())){
            mLayoutBtnTransfer.setVisibility(View.VISIBLE);
            //마이너스통장이면
            //마이너스 통장이면 커플통장을 신청할수 없다.
            if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){
                if(!mainAccountInfo.getOVRD_DVCD().equals("0")){
                    mTvBtnTransfer.setText("이체");
                    procBtnTransfer(mainAccountInfo);
                }else{
                    mTvBtnTransfer.setText("상환");
                    procBtnPayBack(mainAccountInfo);
                }
            }else{
                //커플계좌 탭으로 진입했고 커플 타입이 공유자일때
                if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE&& Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
                    //이체요청일때는 충전,이동버튼 사라지도록 한다.
//                    mLayoutBtnCharge.setVisibility(View.GONE);
                    mLayoutBtnMoveAmount.setVisibility(View.GONE);

                    mTvBtnTransfer.setText("이체요청");
                    procBtnRequestTransfer(mainAccountInfo);
                }else{
                    mTvBtnTransfer.setText("이체");
                    procBtnTransfer(mainAccountInfo);
                }
            }

        }else{
            mLayoutBtnTransfer.setVisibility(View.GONE);

        }
    }

    /**
     * 한도제한계좌 증빙서류 심사중   상태 메세지 출력
     *
     * @param mainAccountInfo
     */
    private void displaylimitDocJudgeState(Main2AccountInfo mainAccountInfo){
        //공유자이면서 커플계좌탭에서는 서류 심사중 메세지 출력하지 않는다.
        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
            return;
        }

        if(DataUtil.isNotNull(mainAccountInfo.getFAX_SEND_PRGS_CNTN())) {//접수내용 체크
            mTvDocJudgeMsg.setText(mainAccountInfo.getFAX_SEND_PRGS_CNTN());
            mTvDocJudgeMsg.setVisibility(View.VISIBLE);
        }else{
            mTvDocJudgeMsg.setVisibility(View.GONE);
        }
    }

    //버튼이 이체로 표기될때 처리
    private void procBtnTransfer(Main2AccountInfo mainAccountInfo){
        //이체버튼
        mLayoutBtnTransfer.setTag(mainAccountInfo.getACNO());
        mLayoutBtnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String ACNO = (String)v.getTag();
                SlidingMain2TransferDialog dialog = new SlidingMain2TransferDialog(mContext,new SlidingMain2TransferDialog.OnConfirmListener(){
                    @Override
                    public void onConfirmPress(final int selectIndex) {

                        TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
                            @Override
                            public void onCheckFinish() {
                                switch (selectIndex){
                                    case 1: {//통합이체
                                        //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                                        ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                                        ITransferDataMgr.getInstance().setWTCH_ACNO(ACNO);

                                        Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                                        mContext.startActivity(intent);
                                        break;
                                    }
                                    case 2: {//안심이체
                                        Intent intent = new Intent(mContext, WebMainActivity.class);
                                        intent.putExtra("url", WasServiceUrl.TRA0090100.getServiceUrl());
                                        if (!TextUtils.isEmpty(ACNO)){
                                            String param = "ACNO="+ACNO;
                                            intent.putExtra(Const.INTENT_PARAM, param);
                                        }
                                        mContext.startActivity(intent);
                                        break;
                                    }
                                }
                            }
                        });

                    }
                });
                dialog.show();


//                SlidingMain2TransferDialog dialog = new SlidingMain2TransferDialog(mContext,new SlidingMain2TransferDialog.OnConfirmListener(){
//                    @Override
//                    public void onConfirmPress(int selectIndex) {
//                        switch (selectIndex){
//
//                            case 1: {//계좌이체
//                                Intent intent = new Intent(mContext, TransferAccountActivity.class);
//                                if (!TextUtils.isEmpty(ACNO))
//                                    intent.putExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT, ACNO);
//                                mContext.startActivity(intent);
//                                break;
//                            }
//                            case 2: {//안심이체
//                                Intent intent = new Intent(mContext, WebMainActivity.class);
//                                intent.putExtra("url", WasServiceUrl.TRA0090100.getServiceUrl());
//                                if (!TextUtils.isEmpty(ACNO)){
//                                    String param = "ACNO="+ACNO;
//                                    intent.putExtra(Const.INTENT_PARAM, param);
//                                }
//                                mContext.startActivity(intent);
//                                break;
//                            }
//                        }
//                    }
//                });
//                dialog.show();
            }

        });
    }

    //버튼이 이체요청로 표기될때 처리 - 커플통장의 계좌주가 아닌 고유자 일때 아래 처리
    private void procBtnRequestTransfer(Main2AccountInfo mainAccountInfo){

        mLayoutBtnTransfer.setTag(mainAccountInfo);
        //이체요청버튼
        mLayoutBtnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
                startWebAction(mainAccountInfo, new Main2AccountAdapter.OnProcWebActionListener() {
                    @Override
                    public void onOKWebAction(Object accountInfo) {
                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        String url = WasServiceUrl.TRA0180100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onErrorWebAction(String code, Object accountInfo) {
                        if(code.equals("99")){
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.MAI0070100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                            mContext.startActivity(intent);
                        }
                    }

                });

            }

        });
    }


    //마이너스 통장은 계좌 쪼개기 할수 없어 해당 함수는 호출되지 않는다.
    //버튼이 상환으로 표시될때 처리
    private void procBtnPayBack(Main2AccountInfo mainAccountInfo){

    }

}
