package com.sbi.saidabank.activity.crop;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.crop.CropImagePreset;
import com.sbi.saidabank.common.crop.CropImageViewOptions;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.net.URI;

/**
 * Saidabanking_android
 * <p>
 * Class: CropImageFragment
 * Created by 950485 on 2018. 10. 15..
 * <p>
 * Description:화면 Crop을 위한 화면
 */
public final class CropImageFragment extends Fragment implements CropImageView.OnSetImageUriCompleteListener, CropImageView.OnCropImageCompleteListener {
    private Context mContext = getActivity();
    private CropImagePreset mDemoPreset;
    public CropImageView mCropImageView;

    public static CropImageFragment newInstance(CropImagePreset demoPreset) {
        CropImageFragment fragment = new CropImageFragment();
        Bundle args = new Bundle();
        args.putString("DEMO_PRESET", demoPreset.name());
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        switch (mDemoPreset) {
            case CIRCULAR:
                return inflater.inflate(R.layout.fragment_crop_oval, container, false);
            case CUSTOMIZED_OVERLAY:
                return inflater.inflate(R.layout.fragment_crop_customized, container, false);
            case MIN_MAX_OVERRIDE:
                return inflater.inflate(R.layout.fragment_crop_min_max, container, false);
            case SCALE_CENTER_INSIDE:
                return inflater.inflate(R.layout.fragment_main_scale_center, container, false);
            case RECT:
            case CUSTOM:
            default:
                return inflater.inflate(R.layout.fragment_crop_rect, container, false);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        MLog.d();
        super.onViewCreated(view, savedInstanceState);

        mCropImageView = view.findViewById(R.id.cropImageView);
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnCropImageCompleteListener(this);
        mCropImageView.setAutoZoomEnabled(false);

        updateCurrentCropViewOptions();
        if (DataUtil.isNull(savedInstanceState) && DataUtil.isNotNull(((CropImageActivity) getActivity()).getTargetImageUri())) {
            setImageUri(((CropImageActivity) getActivity()).getTargetImageUri());
        }

        Logs.e("onViewCreated - mDemoPreset : " + mDemoPreset);

        if(mDemoPreset == CropImagePreset.RECT){

            mCropImageView.getViewTreeObserver()
                    .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            setInitialCropRect();
                            mCropImageView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    });
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mDemoPreset = CropImagePreset.valueOf(getArguments().getString("DEMO_PRESET"));
        ((CropImageActivity) activity).setCurrentFragment(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (DataUtil.isNotNull(mCropImageView)) {
            mCropImageView.setOnCropImageCompleteListener(null);
            mCropImageView.setOnSetImageUriCompleteListener(null);
        }
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {
        MLog.d();
        if (DataUtil.isNotNull(error)) {
            Toast.makeText(getActivity(), "Image Load Failed: " + error.getMessage(), Toast.LENGTH_LONG).show();
            MLog.i("Error Msg >> " + error.getMessage());
        }
    }

    @Override
    public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
        MLog.d();
        handleCropResult(result);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        MLog.d();
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            handleCropResult(result);
        }
    }

    /**
     * crop 옵션 업데이트
     */
    public void updateCurrentCropViewOptions() {
        CropImageViewOptions options = new CropImageViewOptions();
        options.scaleType = mCropImageView.getScaleType();
        options.cropShape = mCropImageView.getCropShape();
        options.guidelines = mCropImageView.getGuidelines();
        options.aspectRatio = mCropImageView.getAspectRatio();
        options.fixAspectRatio = mCropImageView.isFixAspectRatio();
        options.showCropOverlay = mCropImageView.isShowCropOverlay();
        options.showProgressBar = mCropImageView.isShowProgressBar();
        options.autoZoomEnabled = mCropImageView.isAutoZoomEnabled();
        options.maxZoomLevel = mCropImageView.getMaxZoom();
        options.flipHorizontally = mCropImageView.isFlippedHorizontally();
        options.flipVertically = mCropImageView.isFlippedVertically();
        ((CropImageActivity) getActivity()).setCurrentOptions(options);
    }

    /**
     * crop된 이미지뷰에 uri 설정
     *
     * @param imageUri 이미지 uri
     */
    public void setImageUri(Uri imageUri) {
        mCropImageView.setImageUriAsync(imageUri);
    }

    private void handleCropResult(CropImageView.CropResult result) {
        MLog.d();
        if (DataUtil.isNull(result.getError())) {
            Intent intent = new Intent(getActivity(), CropImageResultActivity.class);
            if (DataUtil.isNotNull(result.getUri())) {
                intent.putExtra("URI", result.getUri());
            } else {
                CropImageResultActivity.mCropImage = mCropImageView.getCropShape() == CropImageView.CropShape.OVAL ? CropImage.toOvalBitmap(result.getBitmap()) : result.getBitmap();
            }
            getActivity().startActivityForResult(intent, Const.REQUEST_CROP_IMAGE);
        } else {
            Toast.makeText(getActivity(), "Image Crop Failed: " + result.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * crop image view 초기값 설정
     *
     * @param options crop image view 옵션
     */
    public void setCropImageViewOptions(CropImageViewOptions options) {
        mCropImageView.setScaleType(options.scaleType);
        mCropImageView.setCropShape(options.cropShape);
        mCropImageView.setGuidelines(options.guidelines);
        mCropImageView.setAspectRatio(options.aspectRatio.first, options.aspectRatio.second);
        mCropImageView.setFixedAspectRatio(options.fixAspectRatio);
        mCropImageView.setMultiTouchEnabled(options.multitouch);
        mCropImageView.setShowCropOverlay(options.showCropOverlay);
        mCropImageView.setShowProgressBar(options.showProgressBar);
        mCropImageView.setAutoZoomEnabled(options.autoZoomEnabled);
        mCropImageView.setMaxZoom(options.maxZoomLevel);
        mCropImageView.setFlippedHorizontally(options.flipHorizontally);
        mCropImageView.setFlippedVertically(options.flipVertically);
    }

    /**
     * crop 사각형 화면 크기 설정
     * 아래 함수를 사용하는 경우는 홈 커플의 타이틀 배경화면을 자를때 이다..
     * 그래서 가로세로 값을 고정했음. 추후 필요하면 넘겨 받도록 수정요망.
     */
    public void setInitialCropRect() {

        //폰의 LCD 넓이를 기준으로 비율을 구한다.
        int coupleTitleWidth = (int) GraphicUtils.getActivityWidth(getActivity());//폰의 넓이는 가변
        int coupleTitleHeight = (int) getActivity().getResources().getDimension(R.dimen.main2_home_couple_title_height);//타이틀의 높이는 200dp로 고정
        float coupleTitleRatio = (float) coupleTitleHeight / (float) coupleTitleWidth;
        Logs.e("setInitialCropRect - coupleTitleWidth : " + coupleTitleWidth);
        Logs.e("setInitialCropRect - coupleTitleHeight : " + coupleTitleHeight);
        Logs.e("setInitialCropRect - coupleTitleRatio : " + coupleTitleRatio);

        //우선 Imageview의 넓이 높이를 구한다.
        int viewWidth = mCropImageView.getWidth();
        int viewHeight = mCropImageView.getHeight();
        Logs.e("setInitialCropRect - viewWidth : " + viewWidth);
        Logs.e("setInitialCropRect - viewHeight : " + viewHeight);

        String fileUrl = ((CropImageActivity) getActivity()).getTargetImageUri().getPath();
        Logs.e("setInitialCropRect - fileUrl : " + fileUrl);
        if (TextUtils.isEmpty(fileUrl)) return;

        ImageView imageView = new ImageView(getActivity());
        imageView.setImageURI(((CropImageActivity) getActivity()).getTargetImageUri());
        if(imageView.getDrawable() != null){
            Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
            if (bitmap != null) {
                viewWidth = bitmap.getWidth();
                viewHeight = bitmap.getHeight();
                Logs.e("setInitialCropRect - bmp width : " + viewWidth);
                Logs.e("setInitialCropRect - bmp height : " + viewHeight);
                if (viewWidth > viewHeight) {
                    //화웨이 폰에서 넓이와 높이가 바뀌어 들어오는 경우가 있다. 그래서 바꿔주도록...
                    viewWidth = bitmap.getHeight();
                    viewHeight = bitmap.getWidth();
                }
                if(!bitmap.isRecycled())
                    bitmap.recycle();
            }
        }

        Logs.e("setInitialCropRect - viewWidth : " + viewWidth);
        Logs.e("setInitialCropRect - viewHeight : " + viewHeight);

        int rectHeight = (int) (viewWidth * coupleTitleRatio);
        Logs.e("setInitialCropRect - rectHeight : " + rectHeight);

        int topPos = viewHeight / 2 - rectHeight / 2;
        int bottomPos = viewHeight / 2 + rectHeight / 2;
        mCropImageView.setCropRect(new Rect(0, topPos, viewWidth, bottomPos));

    }

    /**
     * crop 사각형 화면 크기 초기화
     */
    public void resetCropRect() {
        mCropImageView.resetCropRect();
    }
}
