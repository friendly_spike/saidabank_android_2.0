package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;

/**
 * Saidabanking_android
 * <p>
 * Class: CustomerAccountActivity
 * Created by 950485 on 2018. 11. 08..
 * <p>
 * Description:기기변경 고객 등록여부확인화면 (계좌보유)
 */

public class CustomerAccountGuideActivity extends BaseActivity {

    CommonUserInfo mCommonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_customer_account);

        LinearLayout layout_bio = findViewById(R.id.ll_bio);

        if (StonePassUtils.isUsableFingerprint(this) != 0) {
            layout_bio.setVisibility(View.GONE);
            //Logs.showToast(this, "지문인증 불가능 단말입니다.\n팝업 처리 예정");
        }
        else{
            layout_bio.setVisibility(View.VISIBLE);
        }

        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);

        if (!TextUtils.isEmpty(mCommonUserInfo.getName())) {
            TextView textDesc = (TextView) findViewById(R.id.textview_account_customer_desc_01);
            String desc01 = String.format(getString(R.string.account_customer_desc_01), mCommonUserInfo.getName());
            textDesc.setText(desc01);
        }

        Button btnOK = (Button) findViewById(R.id.btn_confirm_customer_account);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showPrepareIdentification();
            }
        });
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * 신분증 확인 설명 화면으로 이동
     */
    private void showPrepareIdentification() {
        Intent intent = new Intent(CustomerAccountGuideActivity.this, IdentificationPrepareActivity.class);
        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
        startActivity(intent);
        finish();
    }
}
