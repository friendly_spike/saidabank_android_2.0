package com.sbi.saidabank.activity.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomEditText;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Saidabanking_android
 * Class: PersonalInfoActivity
 * Created by 950546
 * Date: 2018-10-18
 * Time: 오전 10:40
 * Description: 개인정보 입력 화면 (이름, 생년월일, 성별코드, 이메일 주소)
 */

public class PersonalInfoActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged, View.OnClickListener, TextView.OnEditorActionListener, View.OnFocusChangeListener, TextWatcher, AdapterView.OnTouchListener, AdapterView.OnItemClickListener, AdapterView.OnItemSelectedListener, CustomEditText.OnBackPressListener {
    private static final int SCROLL_EDITTEXT        = 0;
    private static final int SCROLL_CHECK_KEYBOARD  = 1;
    private static final int SCROLL_EMAILLIST       = 2;
    private static final int SCROLL_EDITTEXT_NAME   = 3;
    private static final int SCROLL_EDITTEXT_EMAIL  = 4;

    private Context mContext;

    private RelativeLayout mLayoutScreen;
    private LinearLayout mLayoutName;
    private TextView mTvNameError01;
    private TextView mTvNameError02;
    private View mVNamePadding;
    private TextView mTvBirthError;
    private TextView mTvEmailError;
    private LinearLayout mLayoutIdNumber;
    private LinearLayout mLayoutBirth;
    private LinearLayout mLayoutEmailAddress;
    private LinearLayout mLayoutEmailList;
    private CustomEditText mEtName;
    private CustomEditText mEtIdnumber01;
    private CustomEditText mEtIdnumber02;
    private CustomEditText mEtEmailAddress;
    private TextView mTvNameCaption;
    private TextView mTvIdCaption;
    private TextView mTvEmailCaption;
    private Button mBtnPlaceholderDot;
    private Button mBtnConfirm;
    private View mVLine1;
    private View mVLine2;
    private ScrollView mSvScview;
    private int mCurFocusedViewId;
    private int mCurFocesedViewState;

    private int mScrollOffset;
    private int mScrollNameOffset;
    private boolean mIsShownKeyboard;
    private InputMethodManager imm = null;
    private ArrayList<String> mEmailList;
    private ArrayList<String> mEmailAddressList;
    private EmailListAdapter mEmailListApdater;
    private WeakHandler mScrollHandler;
    private boolean bIsfirstrun;
    private int mEmailListHeight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_personalinfo);

        initView();
        requestEmailDomain(true);

//        CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
//        if (commonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
//            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"3", "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logs.e("onResume");
        setEditTextBackground(mCurFocusedViewId, mCurFocesedViewState);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //++ activity stop될 때 마지막 포커스 뷰와 상태 저장
        if (getCurrentFocus() != null) {
            mCurFocusedViewId = getCurrentFocus().getId();
        } else {
            mCurFocusedViewId = mEtName.getId();
        }

        mCurFocesedViewState = Const.STATE_EDITBOX_FOCUSED;
        switch (mCurFocusedViewId) {
            case R.id.et_name:
                mCurFocusedViewId = Const.INDEX_EDITBOX_NAME;
                if (Utils.isKorean(mEtName.getText().toString()) != Const.STATE_VALID_STR_NORMAL
                        && (mTvNameError01.getVisibility() != View.GONE || mTvNameError02.getVisibility() != View.GONE))
                    mCurFocesedViewState = Const.STATE_EDITBOX_ERROR;
                break;
            case R.id.et_idnumber1:
            case R.id.et_idnumber2:
                mCurFocusedViewId = Const.INDEX_EDITBOX_BIRTH;
                boolean bisvalidBirth = isValidBirth(mEtIdnumber01.getText().toString());
                boolean bisvalidIdnumber = isValidIdNumber(mEtIdnumber02.getText().toString());
                if ((!bisvalidBirth || !bisvalidIdnumber)
                        && mTvBirthError.getVisibility() != View.GONE)
                    mCurFocesedViewState = Const.STATE_EDITBOX_ERROR;
                break;
            case R.id.et_emailaddress_01:
                mCurFocusedViewId = Const.INDEX_EDITBOX_EMAIL;
                if (isValidEmail(mEtEmailAddress.getText().toString()) != Const.STATE_VALID_STR_NORMAL
                        && mTvEmailError.getVisibility() != View.GONE)
                    mCurFocesedViewState = Const.STATE_EDITBOX_ERROR;
                break;
            default:
                break;
        }
        //--
    }

    /**
     * UI 초기화
     */
    private void initView() {
        mContext = this;
        bIsfirstrun = false;
        mEmailListHeight = 0;
        mCurFocesedViewState = Const.STATE_EDITBOX_NORMAL;
        mLayoutName = (LinearLayout) findViewById(R.id.ll_name);
        mLayoutScreen = (RelativeLayout) findViewById(R.id.ll_root);
        mTvNameError01 = (TextView) findViewById(R.id.tv_nameerrmsg01);
        mTvNameError02 = (TextView) findViewById(R.id.tv_nameerrmsg02);
        mVNamePadding = (View) findViewById(R.id.v_nameboxpadding);
        mTvBirthError = (TextView) findViewById(R.id.tv_birtherrmsg);
        mTvEmailError = (TextView) findViewById(R.id.tv_emailerrmsg);
        mLayoutIdNumber = (LinearLayout) findViewById(R.id.ll_idnumber);
        mLayoutBirth = (LinearLayout) findViewById(R.id.ll_birth);
        mLayoutEmailAddress = (LinearLayout) findViewById(R.id.ll_emainaddress);
        mLayoutEmailList = (LinearLayout) findViewById(R.id.ll_emaillist);
        mEtName = (CustomEditText) findViewById(R.id.et_name);
        mEtIdnumber01 = (CustomEditText) findViewById(R.id.et_idnumber1);
        mEtIdnumber02 = (CustomEditText) findViewById(R.id.et_idnumber2);
        mEtEmailAddress = (CustomEditText) findViewById(R.id.et_emailaddress_01);
        mVLine1 = (View) findViewById(R.id.v_line_01);
        mVLine2 = (View) findViewById(R.id.v_line_02);
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mTvNameCaption = (TextView) findViewById(R.id.tv_namecaption);
        mTvIdCaption = (TextView) findViewById(R.id.tv_idcaption);
        mTvEmailCaption = (TextView) findViewById(R.id.tv_emailcaption);
        mBtnPlaceholderDot = (Button) findViewById(R.id.btn_placeohlder_dot);

        mScrollOffset =  (int)Utils.dpToPixel(this, (float)40f);
        mScrollNameOffset = (int)Utils.dpToPixel(this, (float)20f);
        mCurFocusedViewId = Const.INDEX_EDITBOX_NAME;

        mEmailListApdater = new EmailListAdapter();
        mEmailList = new ArrayList<String>();
        mEmailAddressList = new ArrayList<String>();
        /*
        mEmailList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.email)));
        mEmailAddressList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.email)));
        */
        ListView lvemail = findViewById(R.id.lv_email);
        lvemail.setAdapter(mEmailListApdater);
        lvemail.setOnItemClickListener(this);
        lvemail.setOnTouchListener(this);
        mScrollHandler = new WeakHandler(this);

        mLayoutName.setOnClickListener(this);
        findViewById(R.id.ll_idbirth).setOnClickListener(this);
        mLayoutScreen.setOnClickListener(this);
        mLayoutIdNumber.setOnClickListener(this);
        mLayoutEmailAddress.setOnClickListener(this);
        mEtName.setOnEditorActionListener(this);
        mEtName.setOnFocusChangeListener(this);
        mEtName.setFilters(new InputFilter[]{specialCharacterFilter, new InputFilter.LengthFilter(14)});
        mEtName.setOnBackPressListener(this);
        mEtName.addTextChangedListener(this);
        mEtName.setOnClickListener(this);
        mEtIdnumber01.setOnEditorActionListener(this);
        mEtIdnumber01.setOnFocusChangeListener(this);
        mEtIdnumber01.setOnClickListener(this);
        mEtIdnumber01.addTextChangedListener(this);
        mEtIdnumber01.setOnBackPressListener(this);
        mEtIdnumber02.setOnEditorActionListener(this);
        mEtIdnumber02.addTextChangedListener(this);
        mEtIdnumber02.setOnFocusChangeListener(this);
        mEtIdnumber02.setOnBackPressListener(this);
        mEtEmailAddress.setOnFocusChangeListener(this);
        mEtEmailAddress.setOnEditorActionListener(this);
        mEtEmailAddress.setOnClickListener(this);
        mEtEmailAddress.setOnBackPressListener(this);
        mEtEmailAddress.addTextChangedListener(this);
        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (checkDataValidation()) {
                    // 만나이 체크
                    if(!Utils.isBirthFormat(mEtIdnumber01.getText().toString())){
                        showErrorMessage(getString(R.string.msg_invalid_birth_format));
                        return;
                    }
                    if(Utils.isAgeCheck(mEtIdnumber01.getText().toString(),mEtIdnumber02.getText().toString())){
                        // 휴대폰 본인인증 화면 이동 시 생년월일 정보 전달
                        Intent authphoneintent = new Intent(PersonalInfoActivity.this, CertifyPhoneActivity.class);
                        CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
                        commonUserInfo.setBirth(mEtIdnumber01.getText().toString());
                        commonUserInfo.setSexCode(mEtIdnumber02.getText().toString());
                        commonUserInfo.setName(mEtName.getText().toString());
                        commonUserInfo.setEmailAddress(mEtEmailAddress.getText().toString());
                        commonUserInfo.setFullIdnumber(false);
                        authphoneintent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivity(authphoneintent);

                        //20200623 - 여기서 종료하지 않는다. 이상하게 짜놨다!!!
                        //finish();
                    }else{
                        showErrorMessage(getString(R.string.msg_age_restriction_msg));
                    }

                } else {
                    mBtnConfirm.setEnabled(false);
                }
            }
        });

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(this);
        // emaillistbox 높이 구하기
        final RelativeLayout mRelativeRwapper = (RelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeRwapper.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int mRootViewHeight = mRelativeRwapper.getRootView().getHeight();   // 화면 전체 높이
                int mRelativeWrapperHeight = mRelativeRwapper.getHeight();          // 액티비티 높이
                int mkeyboardHeight = mRootViewHeight - mRelativeWrapperHeight;

                Logs.e("mkeyboardHeight : " + mkeyboardHeight);
                if (mkeyboardHeight < 300)
                    return;

                int mConfirmBtnHeight = mBtnConfirm.getHeight();                    // 확인버튼 높이
                int mEmailBoxHeight = mLayoutEmailAddress.getHeight();              // 이메일 입력 레이아웃 높이
                int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android"); // 인디케이터바 높이
                int statusBarHeight = 0;
                if (resourceId > 0) {
                    statusBarHeight = getResources().getDimensionPixelSize(resourceId);
                }
                int softKeyHeight = 0;
                resourceId = getResources().getIdentifier("config_showNavigationBar", "bool", "android");   // softkey 표시 여부
                if (resourceId > 0 && getResources().getBoolean(resourceId)) {
                    resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android"); // softkey 높이
                    if (resourceId > 0) {
                        softKeyHeight = getResources().getDimensionPixelSize(resourceId);
                    }
                }
                mEmailListHeight = mRootViewHeight - (mkeyboardHeight + mConfirmBtnHeight + mEmailBoxHeight * 2 + statusBarHeight + softKeyHeight);  // emaillistbox 높이
                if (Utils.pixelToDp(PersonalInfoActivity.this, mEmailListHeight) < 50) {
                    mEmailListHeight = (int) Utils.dpToPixel(PersonalInfoActivity.this, 90);
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 이메일 자동완성 리스트 show / hide
     *
     * @param isShow true면 show 아니면 hide
     */
    private int setEmailList(boolean isShow) {
        if (isShow) {

            String emailstr = mEtEmailAddress.getText().toString();
            String emailaddrsss = "";

            if (TextUtils.isEmpty(emailstr)) {
                if (mLayoutEmailList.getVisibility() != View.GONE)
                    mLayoutEmailList.setVisibility(View.GONE);
                return -1;
            }

            if (emailstr.contains("@")) {
                if (emailstr.substring(emailstr.indexOf("@")).length() > 0)
                    emailaddrsss = emailstr.substring(emailstr.indexOf("@"));
                emailstr = emailstr.substring(0, emailstr.indexOf("@"));
                Logs.i("emailaddrsss : " + emailaddrsss);
            }
/*
            // 완성된 email인지 체크
            boolean isCompleteEmail = false;
            if (mEmailList.size() > 0 && mEmailList.contains(mEtEmailAddress.getText().toString()))
                isCompleteEmail = true;
*/
            mEmailList.clear();

            // @뒤에 입력된 문자가 없거나 완성된 email형식이면 전체 Email List 출력
            if (/*isCompleteEmail ||*/ ("".equals(emailaddrsss))) {
                for (int i = 0; i < mEmailAddressList.size(); i++) {
                    mEmailList.add(emailstr + mEmailAddressList.get(i));
                }
            } else {    // @뒤에 입력된 문자가 있으면 비교하여 출력
                for (int i = 0; i < mEmailAddressList.size(); i++) {
                    if (mEmailAddressList.get(i).toLowerCase().startsWith(emailaddrsss.toLowerCase()))
                        mEmailList.add(emailstr + mEmailAddressList.get(i));
                }
            }

            // 사용자가 입력한 @뒷부분 주소가 이메일 리스트에 없는 경우 이메일 리스트 hide
            if (mEmailList.size() == 0) {
                //setEmailList(false);
                //return -1;
                mEmailList.add(mEtEmailAddress.getText().toString());
            }

            // 이메일 리스트 갱신
            mEmailListApdater.notifyDataSetChanged();
            mLayoutEmailList.setVisibility(View.VISIBLE);
            ListView lvemail = findViewById(R.id.lv_email);
            ViewGroup.LayoutParams params = lvemail.getLayoutParams();
            params.height = mEmailListHeight;
            lvemail.setLayoutParams(params);
            lvemail.requestLayout();
            mScrollHandler.sendEmptyMessageDelayed(SCROLL_EMAILLIST, 500);
        } else {
            if (mLayoutEmailList.getVisibility() != View.GONE)
                mLayoutEmailList.setVisibility(View.GONE);
        }
        return 0;
    }

    @Override
    public void onClick(View v) {
        Logs.i("onclick");
        switch (v.getId()) {
            case R.id.ll_name: {
                setEmailList(false);
                boolean hasFocuesd = mEtName.hasFocus();
                mEtName.requestFocus();
                imm.showSoftInput(mEtName, 0);
                checkVaildUserName(mEtName.getText().toString(), true);
                if (hasFocuesd)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_NAME, 300);
                break;
            }
            case R.id.et_name: {
                setEmailList(false);
                checkVaildUserName(mEtName.getText().toString(), true);
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_NAME, 300);
                break;
            }
            case R.id.ll_idbirth: {
                setEmailList(false);
                boolean hasFocuesd = mEtIdnumber01.hasFocus();
                mEtIdnumber01.requestFocus();
                imm.showSoftInput(mEtIdnumber01, 0);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                if (hasFocuesd)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_idnumber1: {
                setEmailList(false);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_idnumber2: {
                setEmailList(false);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.ll_emainaddress: {
                requestEmailDomain(false);
                boolean hasFocuesd = mEtEmailAddress.hasFocus();
                mEtEmailAddress.requestFocus();
                imm.showSoftInput(mEtEmailAddress, 0);
                String emailstr = mEtEmailAddress.getText().toString();
                checkVaildEmail(emailstr, true);
                //if (emailstr.contains("@"))
                if (setEmailList(true) != 0 && hasFocuesd) {
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                }
                break;
            }
            case R.id.et_emailaddress_01: {
                requestEmailDomain(false);
                String emailstr = mEtEmailAddress.getText().toString();
                checkVaildEmail(emailstr, true);
                //if (emailstr.contains("@"))
                if (setEmailList(true) != 0) {
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                }
                break;
            }
            case R.id.ll_root: {
                Logs.i("screen touch");
                imm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEtIdnumber01.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEtIdnumber02.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEtEmailAddress.getWindowToken(), 0);
                setEmailList(false);
                trimEditTextString(mEtName);
                trimEditTextString(mEtEmailAddress);

                View curview = getCurrentFocus();
                if (curview != null) {
                    switch (curview.getId()) {
                        case R.id.et_name: {
                            checkVaildUserName(mEtName.getText().toString(), true);
                            break;
                        }
                        case R.id.et_idnumber1:
                        case R.id.et_idnumber2: {
                            checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                            break;
                        }
                        case R.id.et_emailaddress_01: {
                            checkVaildEmail(mEtEmailAddress.getText().toString(), true);
                        }
                        default:
                            break;
                    }
                } else {
                    checkVaildUserName(mEtName.getText().toString(), false);
                    checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), false);
                    checkVaildEmail(mEtEmailAddress.getText().toString(), false);
                }

                mBtnConfirm.setEnabled(checkDataValidation());
                break;
            }
            case R.id.ll_idnumber: {
                Logs.i("touch ll_idnumber");
                setEmailList(false);
                boolean hasFocuesd = mEtIdnumber02.hasFocus();
                mEtIdnumber02.requestFocus();
                imm.showSoftInput(mEtIdnumber02, 0);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                if (hasFocuesd)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            default:
                break;
        }
    }

    /**
     * 최종 확인 버튼 활성화 체크
     */
    private boolean checkDataValidation() {
        String namestr = mEtName.getText().toString();
        String birthstr1 = mEtIdnumber01.getText().toString();
        String birthstr2 = mEtIdnumber02.getText().toString();
        String emailstr1 = mEtEmailAddress.getText().toString();

        // 입력 데이터가 없을 때
        if (namestr.length() == 0 || birthstr1.length() == 0 || birthstr2.length() == 0
                || emailstr1.length() == 0) {
            Logs.e("no data");
            return false;
        }

        // 데이터 유효성 체크
        if (Utils.isKorean(namestr) != Const.STATE_VALID_STR_NORMAL || isValidIdNumber(birthstr2) == false
                || isValidBirth(birthstr1) == false || isValidEmail(emailstr1) != Const.STATE_VALID_STR_NORMAL) {
            Logs.e("invalid data");
            return false;
        }

        return true;
    }

    /**
     * 키보드 하단 back key 버튼 입력 처리
     *
     * @param vid 선택된 view id
     * @return vid가 edittext면 true
     */
    private boolean keyboardBackKeyPressed(int vid) {
        Logs.i("keyboardBackKeyPressed");
        if (vid == mEtName.getId()) {
            setEmailList(false);
            trimEditTextString(mEtName);
            checkVaildUserName(mEtName.getText().toString(), true);
            return true;
        } else if (vid == mEtIdnumber02.getId()) {
            Logs.i("idnumber2 back");
            setEmailList(false);
            checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
            return true;
        } else if (vid == mEtIdnumber01.getId()) {
            setEmailList(false);
            checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
            return true;
        } else if (vid == mEtEmailAddress.getId()) {
            setEmailList(false);
            trimEditTextString(mEtEmailAddress);
            checkVaildEmail(mEtEmailAddress.getText().toString(), true);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

/*
    // 이름 유효성 체크(키보드입력 중 모음자음 포함)
    private int isKorean_whileInput(String str) {
        Logs.e("isKorean_whileInput");
        if (str.length() > 0) {
            if (str.matches("^[ㄱ-ㅎㅏ-ㅣ가-힣]*$")) {  // 한글 입력 ok
                return 0;
            } else {
                if (hasSpecialCharacter(str) || str.matches("^[0-9]*$")) { // 특수문자 포함
                    return 1;
                } else {    // 기타문자 포함
                    return 2;
                }

            }
        } else {
            return 0;
        }
    }
*/

    /**
     * 주민번호 뒷자리 첫번째 숫자 유효성 체크
     *
     * @param strnumber : 주민번호 뒷자리 첫번째 숫자
     * @return 1-4사이면 true
     */
    private boolean isValidIdNumber(String strnumber) {
        if (strnumber.length() > 0) {
            if (strnumber.matches("^[1-4]*$")) {  // 1-4
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * 생년월일 유효성 체크
     *
     * @param strbirth 생년월일
     * @return 6자리면 true
     */
    private boolean isValidBirth(String strbirth) {
        if (strbirth.length() > 0) {
            if (strbirth.length() == 6) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * 이메일 유효성 체크
     *
     * @param stremail 이메일 주소
     * @return STATE_VALID_STR_NORMAL : 정상, STATE_VALID_STR_LENGTH_ERR : 길이 오류
     */
    private int isValidEmail(String stremail) {
        Logs.i("email : " + stremail);
        if (stremail.length() > 0) {
            if (stremail.length() > 30) {
                return Const.STATE_VALID_STR_LENGTH_ERR;
            }
            else {
                if (Patterns.EMAIL_ADDRESS.matcher(stremail).matches()) {
                        return Const.STATE_VALID_STR_NORMAL;
                } else {
                    return Const.STATE_VALID_STR_CHAR_ERR;
                }
            }
        } else {
            /*
            String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(stremail);

            if (m.matches()) {
                return Const.STATE_VALID_STR_NORMAL;
            } else {
                return Const.STATE_VALID_STR_CHAR_ERR;
            }
            */
            return Const.STATE_VALID_STR_NORMAL;
        }
    }


    /**
     * 특수문자 체크
     *
     * @param str 체크할 문자열
     * @return 특수문자가 포함되어 있으면 true
     */
    public static boolean hasSpecialCharacter(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }

        for (int i = 0; i < str.length(); i++) {
            if (!Character.isLetter(str.charAt(i))) {
                return true;
            }
        }

        return false;
    }

    /**
     * 키보드 이모티콘 입력 금지
     */
    private InputFilter specialCharacterFilter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                // 이모티콘 패턴
                Pattern unicodeOutliers = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");
                if (unicodeOutliers.matcher(source).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    /**
     * 이름 유효성 체크
     *
     * @param strname  이름
     * @param hasfocus 현재 포커스된 상태인지
     */
    private void checkVaildUserName(String strname, boolean hasfocus) {

        int derrcode = Utils.isKorean(strname);

        if (derrcode == Const.STATE_VALID_STR_NORMAL) {
            mTvNameError01.setVisibility(View.GONE);
            mTvNameError02.setVisibility(View.GONE);
            mVNamePadding.setVisibility(View.GONE);
            mEtName.setTextColor(getResources().getColor(R.color.black));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_FOCUSED);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_NORMAL);
        } else {
            mEtName.setTextColor(getResources().getColor(R.color.colorRed));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_ERROR);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_NORMAL);

            if (derrcode == Const.STATE_VALID_STR_LENGTH_ERR) {
                mTvNameError01.setVisibility(View.GONE);
                mTvNameError02.setVisibility(View.VISIBLE);
                mVNamePadding.setVisibility(View.VISIBLE);
            }
            if (derrcode == Const.STATE_VALID_STR_CHAR_ERR) {
                mTvNameError01.setVisibility(View.VISIBLE);
                mVNamePadding.setVisibility(View.VISIBLE);
                if (strname.length() < 2 || strname.length() >= 15) {
                    mTvNameError02.setVisibility(View.VISIBLE);
                } else {
                    mTvNameError02.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * 생년월일, 주민번호 뒷자리 첫번째 숫자 유효성 체크
     *
     * @param strbirth  생년월일
     * @param strnumber 주민번호 뒷자리 첫번째 숫자
     * @param hasfocus  현재 포커스된 상태인지
     */
    private void checkVaildBrith(String strbirth, String strnumber, boolean hasfocus) {
        boolean bisvalidBirth = isValidBirth(strbirth);
        boolean bisvalidIdnumber = isValidIdNumber(strnumber);

        if (bisvalidBirth && bisvalidIdnumber) {
            mTvBirthError.setVisibility(View.GONE);
            mEtIdnumber01.setTextColor(getResources().getColor(R.color.black));
            mEtIdnumber02.setTextColor(getResources().getColor(R.color.black));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_FOCUSED);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_NORMAL);
        } else {
            mTvBirthError.setVisibility(View.VISIBLE);
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_ERROR);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_NORMAL);

            if (!bisvalidBirth) {
                mTvBirthError.setText(R.string.msg_invalid_birth);
                mEtIdnumber01.setTextColor(getResources().getColor(R.color.colorRed));
                if (!bisvalidIdnumber)
                    mEtIdnumber02.setTextColor(getResources().getColor(R.color.colorRed));
                else
                    mEtIdnumber02.setTextColor(getResources().getColor(R.color.black));
            } else {
                mTvBirthError.setText(R.string.msg_invalid_idnumber);
                mEtIdnumber02.setTextColor(getResources().getColor(R.color.colorRed));
                if (!bisvalidBirth)
                    mEtIdnumber01.setTextColor(getResources().getColor(R.color.black));
                else
                    mEtIdnumber01.setTextColor(getResources().getColor(R.color.colorRed));
            }
        }
    }

    /**
     * 이메일주소 유효성 체크
     *
     * @param stremail 이메일 주소
     * @param hasfocus 현재 포커스된 상태인지
     */
    private void checkVaildEmail(String stremail, boolean hasfocus) {
        int derrcode = isValidEmail(stremail);

        if (derrcode == Const.STATE_VALID_STR_NORMAL) {
            mTvEmailError.setVisibility(View.GONE);
            mEtEmailAddress.setTextColor(getResources().getColor(R.color.black));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_EMAIL, Const.STATE_EDITBOX_FOCUSED);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_EMAIL, Const.STATE_EDITBOX_NORMAL);
        } else {
            mTvEmailError.setVisibility(View.VISIBLE);
            mEtEmailAddress.setTextColor(getResources().getColor(R.color.colorRed));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_EMAIL, Const.STATE_EDITBOX_ERROR);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_EMAIL, Const.STATE_EDITBOX_NORMAL);

            if (derrcode == Const.STATE_VALID_STR_LENGTH_ERR) {
                mTvEmailError.setText(R.string.msg_invalid_email_length);
            } else {
                mTvEmailError.setText(R.string.msg_invalid_email);
            }
        }
    }


    /**
     * 텍스트 입력 박스 background 처리
     *
     * @param boxdindex editbox의 index(name, birth, email)
     * @param state     editbox의 상태(normal, focus, error)
     */
    private void setEditTextBackground(int boxdindex, int state) {
        switch (boxdindex) {
            case Const.INDEX_EDITBOX_NAME: {
                switch (state) {
                    case Const.STATE_EDITBOX_NORMAL: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top);
                        mVLine1.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        mTvNameCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    case Const.STATE_EDITBOX_FOCUSED: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top_focused);
                        mVLine1.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        mTvNameCaption.setTextColor(getResources().getColor(R.color.color00A2B3));
                        break;
                    }
                    case Const.STATE_EDITBOX_ERROR: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top_error);
                        mVLine1.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        mTvNameCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            case Const.INDEX_EDITBOX_BIRTH: {
                switch (state) {
                    case Const.STATE_EDITBOX_NORMAL: {
                        mLayoutBirth.setBackgroundResource(R.drawable.background_box_mid);
                        mVLine1.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        mVLine2.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        mTvIdCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    case Const.STATE_EDITBOX_FOCUSED: {
                        mLayoutBirth.setBackgroundResource(R.drawable.background_box_mid_focused);
                        mVLine1.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        mVLine2.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        mTvIdCaption.setTextColor(getResources().getColor(R.color.color00A2B3));
                        break;
                    }
                    case Const.STATE_EDITBOX_ERROR: {
                        mLayoutBirth.setBackgroundResource(R.drawable.background_box_mid_error);
                        mVLine1.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        mVLine2.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        mTvIdCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            case Const.INDEX_EDITBOX_EMAIL: {
                switch (state) {
                    case Const.STATE_EDITBOX_NORMAL: {
                        mLayoutEmailAddress.setBackgroundResource(R.drawable.background_box_bottom);
                        mVLine2.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        mTvEmailCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    case Const.STATE_EDITBOX_FOCUSED: {
                        mLayoutEmailAddress.setBackgroundResource(R.drawable.background_box_bottom_focused);
                        mVLine2.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        mTvEmailCaption.setTextColor(getResources().getColor(R.color.color00A2B3));
                        break;
                    }
                    case Const.STATE_EDITBOX_ERROR: {
                        mLayoutEmailAddress.setBackgroundResource(R.drawable.background_box_bottom_error);
                        mVLine2.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        mTvEmailCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }
    }

    /**
     * 이메일 주소 입력 후 완료 버튼 누를 때 처리
     */
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Logs.i("oneditoraction");
        setEmailList(false);
        trimEditTextString(mEtEmailAddress);
        checkVaildEmail(mEtEmailAddress.getText().toString(), false);
        mBtnConfirm.setEnabled(checkDataValidation());
        return false;
    }

    /**
     * edittext 간 포커스 전환 처리
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Logs.i("focus change" + v.getId() + ", hasfocus : " + hasFocus);
        if (hasFocus) {
            if (!bIsfirstrun)
                bIsfirstrun = true;
        }

        switch (v.getId()) {
            case R.id.et_name: {
                Logs.i("name focus : " + hasFocus);
                setEmailList(false);
                if (!hasFocus) {
                    trimEditTextString(mEtName);
                }
                checkVaildUserName(mEtName.getText().toString(), hasFocus);
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_NAME, 300);
                break;
            }
            case R.id.et_idnumber2: {
                Logs.i("idnumber2 focus : " + hasFocus);
                setEmailList(false);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), hasFocus);
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_idnumber1: {
                Logs.i("idnumber1 focus : " + hasFocus);
                setEmailList(false);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), hasFocus);
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_emailaddress_01: {
                Logs.i("email focus : " + hasFocus);
                if (!hasFocus) {
                    trimEditTextString(mEtEmailAddress);
                } else {
                    requestEmailDomain(false);
                }
                String emailsr = mEtEmailAddress.getText().toString();
                checkVaildEmail(emailsr, hasFocus);

                //if (emailsr.contains("@"))
                if (setEmailList(true) != 0 && hasFocus) {
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_EMAIL, 300);
                }
                break;
            }
            default:
                break;
        }
        mBtnConfirm.setEnabled(checkDataValidation());
    }

    /**
     * edittext에 입력된 문자열을 trim처리
     *
     * @param curet trim처리할 edittext
     */
    private void trimEditTextString(CustomEditText curet) {
        String strname = curet.getText().toString().trim();

        if (curet.getId() == R.id.et_name || curet.getId() == R.id.et_emailaddress_01) {
            // textchangeedlistener를 가진 eidttext인 경우 settext 전후로 remove listener, add listener를 해야함
            curet.removeTextChangedListener(this);
            curet.setText(strname);
            curet.setSelection(strname.length());
            curet.addTextChangedListener(this);
        } else {
            curet.setText(strname);
            curet.setSelection(strname.length());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Logs.d("beforeTextChanged : " + s + ";");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Logs.d("onTextChanged : " + s + ";");
    }

    /**
     * edittext 문자 입력 시 마다 처리(생년월일, 주민번호 앞자리, 이메일 주소만 처리)
     */
    @Override
    public void afterTextChanged(Editable s) {
        Logs.i("afterTextChanged : " + s + ";");

        View curFocusView = getCurrentFocus();

        if (curFocusView == null) {
            return;
        }

        switch (curFocusView.getId()) {
            case R.id.et_name: {
                //이름 입력 시 한글 이외 문자 필터링
                if (!s.toString().matches("^[ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]*$")) {
                    mEtName.removeTextChangedListener(this);
                    mEtName.setText(s.toString().replaceAll("[^ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]", ""));
                    mEtName.setSelection(mEtName.getText().length());
                    mEtName.addTextChangedListener(this);
                }
                break;
            }

            case R.id.et_idnumber1: {
                if (s.toString().length() == 6) {
                    mEtIdnumber02.requestFocus();
                }
                break;
            }

            case R.id.et_idnumber2: {
                checkVaildBrith(mEtIdnumber01.getText().toString(), s.toString(), true);
                if (s.toString().length() == 1 && mTvBirthError.getVisibility() == View.GONE) {
                    mEtEmailAddress.requestFocus();
                    mBtnPlaceholderDot.setVisibility(View.GONE);
                } else if (s.toString().length() == 0) {
                    mBtnPlaceholderDot.setVisibility(View.VISIBLE);
                } else {
                    mBtnPlaceholderDot.setVisibility(View.GONE);
                }
                break;
            }

            case R.id.et_emailaddress_01: {
                //이메일 주소 입력시 필터링
                if (!s.toString().matches("^[0-9a-zA-Z!#$%&'*+-/=?^_`{|}~.\"(),:;<>\\@]*$")) {
                    mEtEmailAddress.removeTextChangedListener(this);
                    mEtEmailAddress.setText(s.toString().replaceAll("[^0-9a-zA-Z!#$%&'*+-/=?^_`{|}~.\"(),:;<>\\@]", ""));
                    mEtEmailAddress.setSelection(mEtEmailAddress.getText().length());
                    mEtEmailAddress.addTextChangedListener(this);
                }
                setEmailList(true);
                /*
                if (mEtEmailAddress.getText().toString().contains("@")) {
                    setEmailList(true);
                } else {
                    setEmailList(false);
                }
                */
                break;
            }
            default:
                break;
        }
    }

    /**
     * 이메일 리스트를 스피너로 구현할 때 적용 예정
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Logs.i("onItemSelected");
        mBtnConfirm.setEnabled(checkDataValidation());
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        Logs.i("onNothingSelected");
    }

    /**
     * 키보드에서 back key 입력 시 처리
     */
    @Override
    public void onKeyboardBackPress() {
        if (keyboardBackKeyPressed(getCurrentFocus().getId())) {
            mBtnConfirm.setEnabled(checkDataValidation());
            return;
        }
    }

    /**
     * 이메일 리스트에서 아이템 선택 시 처리
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Logs.i("onItemClick : position : " + position);

        mEtEmailAddress.removeTextChangedListener(this);
        if (mEmailList.get(position).length() > 30) {
            mEtEmailAddress.setText(mEmailList.get(position).substring(0, 30));
            mEtEmailAddress.setSelection(30);
        }
        else {
            mEtEmailAddress.setText(mEmailList.get(position));
            mEtEmailAddress.setSelection(mEmailList.get(position).length());
        }

        imm.hideSoftInputFromWindow(mEtEmailAddress.getWindowToken(), 0);
        mEtEmailAddress.addTextChangedListener(this);
        setEmailList(false);
        checkVaildEmail(mEtEmailAddress.getText().toString(), true);
        mBtnConfirm.setEnabled(checkDataValidation());
    }

    /**
     * 이메일 리스트뷰 스크롤 시 외부 스크롤뷰 스크롤 방지
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        mSvScview.requestDisallowInterceptTouchEvent(true);
        return false;
    }

    @Override
    public void onKeyboardShown() {
        Logs.e("onKeyboardShown");
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 300);
        mBtnConfirm.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        Logs.e("onKeyboardHidden");
        mIsShownKeyboard = false;
        mBtnConfirm.setVisibility(View.VISIBLE);
    }

    /**
     * 이메일 리스트 어댑터
     */
    class EmailListAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mEmailList.size();
        }

        @Override
        public Object getItem(int position) {
            return mEmailList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (view == null) {
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.activity_personaliinfo_email_list_item, null);
            }

            TextView tvEmailItem = (TextView) view.findViewById(R.id.tv_emailitem);

            tvEmailItem.setText(mEmailList.get(position));

            return view;
        }
    }

    /**
     * 이메일 리스트 팝업 표시 딜레이를 주기 위한 핸들러
     */
    private class WeakHandler extends Handler {
        private WeakReference<PersonalInfoActivity> mWeakActivity;

        WeakHandler(PersonalInfoActivity weakactivity) {
            mWeakActivity = new WeakReference<PersonalInfoActivity>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            PersonalInfoActivity weakactivity = mWeakActivity.get();
            if (weakactivity != null) {
                switch (msg.what) {
                    case SCROLL_EDITTEXT: {
                        if (!mIsShownKeyboard)
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollOffset);
                        break;
                    }
                    case SCROLL_EDITTEXT_NAME: {
                        if (!mIsShownKeyboard)
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() +mScrollNameOffset);
                        break;
                    }
                    case SCROLL_EDITTEXT_EMAIL: {
                        mSvScview.smoothScrollTo(0, mSvScview.getScrollY() +mScrollOffset);
                        break;
                    }
                    case SCROLL_CHECK_KEYBOARD: {
                        mIsShownKeyboard = true;
                        break;
                    }
                    case SCROLL_EMAILLIST: {
                        mSvScview.smoothScrollTo(0, mLayoutEmailAddress.getTop());
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }

    /**
     * 이메일 도메인 조회
     */
    private void requestEmailDomain(boolean isStarted) {
        Map param = new HashMap();
        param.put("LCCD", "EML_DMN");

        if (mEmailAddressList.size() > 0)
            return;

        if (isStarted)
            showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("LCCD : " + ret);
                dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        return;
                    }

                    JSONArray array = object.getJSONArray("REC");
                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        mEmailList.add(obj.optString("CD_NM"));
                        mEmailAddressList.add(obj.optString("CD_NM"));
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}
