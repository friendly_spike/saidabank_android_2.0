package com.sbi.saidabank.activity.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebTermsActivity;
import com.sbi.saidabank.activity.login.AuthRegisterGuideActivity;
import com.sbi.saidabank.activity.login.CertifyPhonePersonalInfoActivity;
import com.sbi.saidabank.activity.login.CustomerAccountGuideActivity;
import com.sbi.saidabank.activity.login.CustomerNoAccountActivity;
import com.sbi.saidabank.activity.login.IdentificationPrepareActivity;
import com.sbi.saidabank.activity.login.PersonalInfoActivity;
import com.sbi.saidabank.activity.login.PersonalInfoWithFullIdNumberActivity;
import com.sbi.saidabank.activity.ssenstone.PatternRegActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeRegActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.SlidingMobileCarrierDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomButtomInput;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Saidabanking_android
 * <p>
 * Class: CertifyPhoneActivity
 * Created by 950485 on 2018. 10. 17..
 * <p>
 * Description:폰본인인증을 위한 화면
 */

public class CertifyPhoneActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged {
//    private static final int FINISH_TIME            = 5 * 1;
    private static final int FINISH_TIME            = 5 * 60;
    private static final int SCROLL_EDITTEXT        = 0;
    private static final int SCROLL_CHECK_KEYBOARD  = 1;

    private ScrollView        mSvScview;
    private LinearLayout      mLayoutPolicyCheck;
    private CheckBox          mCheckCertifyPhone;
    private CustomButtomInput mBtnRequestCertify;
    private TextView          mTextCarrier;
    private EditText          mEditPhoneNum;
    private EditText          mEditCertifyNum;
    private TextView          mTextRemainTime;
    private Button            mBtnOkCertifyPhone;

    private WeakHandler mScrollHandler;
    private int mScrollOffset;
    private boolean mIsShownKeyboard;
    private Timer mAutoTimeoutTimer = null;
    private long mInteractionTime = 0;
    private boolean mIsRequestCertifyPhone;
    private InputMethodManager imm = null;

    private ArrayList<RequestCodeInfo> mCarrierItems;

    //필요권한 - SMS 받아서 인증번호 자동입력
    //private String[] perList = new String[]{Manifest.permission.RECEIVE_SMS};

    private CommonUserInfo mCommonUserInfo;
    private String    mMobileCarrierCode;
    private String    mReceiveSrno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certify_phone);

        getExtra();
        initUX();
        setHideSoftKeyboard(findViewById(R.id.layout_certify_phone));

        mEditPhoneNum.setText(Utils.getPhoneNumber(this));
        if (!BuildConfig.DEBUG) {
            mEditPhoneNum.setEnabled(false);
            mEditPhoneNum.setTextColor(getResources().getColor(R.color.color9A9A9A));
        }

        //PermissionUtils.checkPermission(this, perList, Const.REQUEST_PERMISSION_SMS);
        requestMobileCarrier();

//        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
//            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"4", "");
    }
/*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logs.e("onRequestPermissionsResult");
    }
*/
    @Override
    public void onBackPressed() {

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            countCertifyStop();
            mReceiveSrno = "";

            Intent intent = new Intent(CertifyPhoneActivity.this, CertifyPhonePersonalInfoActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_left, R.anim.stay);
            finish();
            return;
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) {
            String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) ?
                    getResources().getString(R.string.msg_cancel_pattern_rereg) : getResources().getString(R.string.msg_cancel_pin_rereg);
            showCancelMessage(msg);
            return;
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
            countCertifyStop();
            mReceiveSrno = "";
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            countCertifyStop();
            Intent intent;
            if (!mCommonUserInfo.isUseSession()) {
                if (mCommonUserInfo.isFullIdnumber())
                    intent = new Intent(CertifyPhoneActivity.this, PersonalInfoWithFullIdNumberActivity.class);
                else
                    intent = new Intent(CertifyPhoneActivity.this, CertifyPhonePersonalInfoActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_left, R.anim.stay);
                mReceiveSrno = "";
            } else {
                intent = new Intent();
                intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                setResult(RESULT_CANCELED, intent);
                mReceiveSrno = "";
            }
        }
        finish();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        countCertifyStop();
        //사용한 주민번호 초기화.
        mCommonUserInfo.setIdnumber("");
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mCarrierItems = new ArrayList<RequestCodeInfo>();

        ((KeyboardDetectorRelativeLayout) findViewById(R.id.layout_certify_phone)).addKeyboardStateChangedListener(this);
        findViewById(R.id.ll_root).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(mEditPhoneNum.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEditCertifyNum.getWindowToken(), 0);
            }
        });
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mTextCarrier = (TextView) findViewById(R.id.textview_mobile_carrier);
        mEditPhoneNum = (EditText) findViewById(R.id.edittext_request_certify_phone);
        mEditCertifyNum = (EditText) findViewById(R.id.edittext_request_certify_num);
        mTextRemainTime = (TextView) findViewById(R.id.textview_certify_remain_time);

        mLayoutPolicyCheck = (LinearLayout) findViewById(R.id.ll_policy);
        mCheckCertifyPhone = (CheckBox) findViewById(R.id.checkbox_certify_phone);
        mBtnRequestCertify = (CustomButtomInput) findViewById(R.id.btn_request_certify_phone);
        mBtnOkCertifyPhone = (Button) findViewById(R.id.btn_ok_certify_phone);

        mScrollHandler = new WeakHandler(this);
        mScrollOffset = (int)Utils.dpToPixel(CertifyPhoneActivity.this, (float)30f);

        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_request_certify_phone);

        if (mCommonUserInfo.getEntryPoint() != EntryPoint.WEB_CALL && mCommonUserInfo.getEntryPoint() != EntryPoint.SERVICE_JOIN
                && mCommonUserInfo.getEntryPoint() != EntryPoint.DEVICE_CHANGE) {
            btnCancel.setText(getResources().getString(R.string.common_cancel));
        }

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        final CheckBox checkBoxItem01 = (CheckBox) findViewById(R.id.checkbox_certify_phone_01);
        final CheckBox checkBoxItem02 = (CheckBox) findViewById(R.id.checkbox_certify_phone_02);
        final CheckBox checkBoxItem03 = (CheckBox) findViewById(R.id.checkbox_certify_phone_03);
        final CheckBox checkBoxItem04 = (CheckBox) findViewById(R.id.checkbox_certify_phone_04);

        mLayoutPolicyCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCheckCertifyPhone.setChecked(!mCheckCertifyPhone.isChecked());
            }
        });
        mCheckCertifyPhone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    mBtnRequestCertify.setEnabled(true);
                    checkBoxItem01.setChecked(isChecked);
                    checkBoxItem02.setChecked(isChecked);
                    checkBoxItem03.setChecked(isChecked);
                    checkBoxItem04.setChecked(isChecked);

                    Intent intent = new Intent(CertifyPhoneActivity.this,WebTermsActivity.class);
                    intent.putExtra("title", "약관");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=A002");
                    startActivity(intent);
                } else {
                    checkBoxItem01.setChecked(isChecked);
                    checkBoxItem02.setChecked(isChecked);
                    checkBoxItem03.setChecked(isChecked);
                    checkBoxItem04.setChecked(isChecked);
                    mBtnRequestCertify.setEnabled(false);
                }
            }
        });

        mTextCarrier.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditPhoneNum.isFocused())
                    mEditPhoneNum.clearFocus();

                if (mEditCertifyNum.isFocused())
                    mEditCertifyNum.clearFocus();

                if (mCarrierItems == null || mCarrierItems.size() <= 0)
                    requestMobileCarrier();
                else
                    showMobileCarrier();
            }
        });

        RelativeLayout btnMabileCarrier = (RelativeLayout) findViewById(R.id.imageview_dropdown_mobile_carrier);
        btnMabileCarrier.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideAllKeyboard();

                if (mCarrierItems == null || mCarrierItems.size() <= 0)
                    requestMobileCarrier();
                else
                    showMobileCarrier();
            }
        });

        mBtnRequestCertify.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                /*
                if (!PermissionUtils.checkPermission(CertifyPhoneActivity.this, perList_SMS, Const.REQUEST_PERMISSION)) {
                    return;
                }
                */
                requestSendSms();
            }
        });

        LinearLayout btnCertifyPhone01 = (LinearLayout) findViewById(R.id.layout_certify_phone_01);
        btnCertifyPhone01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CertifyPhoneActivity.this,WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1027");
                startActivity(intent);
            }
        });

        LinearLayout btnCertifyPhone02 = (LinearLayout) findViewById(R.id.layout_certify_phone_02);
        btnCertifyPhone02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CertifyPhoneActivity.this,WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1028");
                startActivity(intent);
            }
        });

        LinearLayout btnCertifyPhone03 = (LinearLayout) findViewById(R.id.layout_certify_phone_03);
        btnCertifyPhone03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CertifyPhoneActivity.this,WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1025");
                startActivity(intent);
            }
        });

        LinearLayout btnCertifyPhone04 = (LinearLayout) findViewById(R.id.layout_certify_phone_04);
        btnCertifyPhone04.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CertifyPhoneActivity.this,WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1026");
                startActivity(intent);
            }
        });

        mBtnOkCertifyPhone.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (!mCheckCertifyPhone.isChecked()) {
                    Toast.makeText(CertifyPhoneActivity.this, R.string.msg_check_certify_phone, Toast.LENGTH_SHORT).show();
                    return;
                }

                String certifyNum = mEditCertifyNum.getText().toString();
                if (TextUtils.isEmpty(certifyNum)) {
                    Toast.makeText(CertifyPhoneActivity.this, R.string.msg_no_certify_number, Toast.LENGTH_SHORT).show();
                    return;
                }

                requestCertifySMS(certifyNum);
            }
        });
        mBtnOkCertifyPhone.setEnabled(false);

       /* mEditCertifyNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(mTextCompany.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });*/

        mEditPhoneNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
                }
            }
        });

        mEditPhoneNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });

        mEditCertifyNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    String certifyNum = mEditPhoneNum.getText().toString();
                    String carrier = mTextCarrier.getText().toString();
                    if (TextUtils.isEmpty(carrier)) {
                        Toast.makeText(CertifyPhoneActivity.this, R.string.msg_no_mobile_carrier, Toast.LENGTH_SHORT).show();
                        mEditCertifyNum.clearFocus();

                        if (mEditPhoneNum.isFocused())
                            mEditPhoneNum.clearFocus();

                        mBtnOkCertifyPhone.performClick();
                        return;
                    } else if (TextUtils.isEmpty(certifyNum) || certifyNum.length() < 10) {
                        Toast.makeText(CertifyPhoneActivity.this, R.string.msg_check_phone_number, Toast.LENGTH_SHORT).show();
                        mEditCertifyNum.clearFocus();
                        return;
                    }
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
                }
            }
        });

        mEditCertifyNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 500);
            }
        });

        mEditPhoneNum.addTextChangedListener(getTextWatcher(mEditPhoneNum));
        mEditCertifyNum.addTextChangedListener(getTextWatcher(mEditCertifyNum));
    }

    private void showMobileCarrier() {
        if(!this.isFinishing()){
            SlidingMobileCarrierDialog dialog = new SlidingMobileCarrierDialog(this, mCarrierItems, new SlidingMobileCarrierDialog.SelectItemListener() {
                @Override
                public void OnSelectItemListener(int position) {
                    mTextCarrier.setText(mCarrierItems.get(position).getCD_NM());
                    mMobileCarrierCode = mCarrierItems.get(position).getSCCD();
                }
            });
            dialog.show();
        }
    }

    /**
     * 생년월일 SMS 인증번호요청
     */
    private void requestSendSms() {
        if (!mCheckCertifyPhone.isChecked()) {
            Toast.makeText(CertifyPhoneActivity.this, R.string.msg_check_certify_phone, Toast.LENGTH_SHORT).show();
            return;
        }

        String certifyNum = mTextCarrier.getText().toString();
        if (TextUtils.isEmpty(certifyNum)) {
            Toast.makeText(CertifyPhoneActivity.this, R.string.msg_no_mobile_carrier, Toast.LENGTH_SHORT).show();
            mBtnOkCertifyPhone.performClick();
            return;
        }

        String phoneNum = mEditPhoneNum.getText().toString();
        if (TextUtils.isEmpty(phoneNum)) {
            Toast.makeText(CertifyPhoneActivity.this, R.string.msg_check_phone_number, Toast.LENGTH_SHORT).show();
            return;
        } else if (phoneNum.length() < 10) {
            Toast.makeText(CertifyPhoneActivity.this, R.string.msg_check_phone_number, Toast.LENGTH_SHORT).show();
            return;
        }

        hideAllKeyboard();
        Map param = new HashMap();
        String serviceUrl;
        if (mCommonUserInfo.isFullIdnumber()) {
            serviceUrl = WasServiceUrl.CMM0010400A03.getServiceUrl();
            param.put("BRDD", mCommonUserInfo.getBirth());
            param.put("NRID__E2E__", mCommonUserInfo.getIdnumber());
            param.put("CUST_NM", mCommonUserInfo.getName());
            param.put("SELF_ATHN_DMND_RSCD", "99");
            param.put("CMCM_DVCD", mMobileCarrierCode);
            phoneNum = phoneNum.trim();
            mCommonUserInfo.setPhoneNumber(phoneNum);
            param.put("CPNO", phoneNum);
            param.put("SMS_SVC_DVCD", "10");
        } else {
            serviceUrl = WasServiceUrl.CMM0010401A01.getServiceUrl();
            String userBirth = mCommonUserInfo.getBirth();
            String userAllBirth = "";
            String sGender = mCommonUserInfo.getSexCode();
            if (userBirth.length() == 6) {
                if ("1".equals(sGender) || "2".equals(sGender))
                    userAllBirth = "19" + userBirth;
                if ("3".equals(sGender) || "4".equals(sGender))
                    userAllBirth = "20" + userBirth;
            } else {
                userAllBirth = userBirth;
            }

            phoneNum = phoneNum.trim();
            mCommonUserInfo.setPhoneNumber(phoneNum);
            param.put("BRDD", userAllBirth);
            param.put("SEX_CD", sGender);
            param.put("CUST_NM", mCommonUserInfo.getName());
            param.put("SMS_SVC_UZ_STPL_AGR_YN", Const.BRIDGE_RESULT_YES);
            param.put("SMS_MVMN_CMCM_UZ_STPL_AGR_YN", Const.BRIDGE_RESULT_YES);
            param.put("SMS_INDV_INFO_UZ_AGR_YN", Const.BRIDGE_RESULT_YES);
            param.put("SMS_CLT_UZ_HNDL_ENTS_AGR_YN", Const.BRIDGE_RESULT_YES);
            param.put("SELF_ATHN_DMND_RSCD", "99");
            param.put("CMCM_DVCD", mMobileCarrierCode);
            param.put("CPNO", phoneNum);
            param.put("SMS_SVC_DVCD", "10");
        }
        mReceiveSrno = "";
        showProgressDialog();
        HttpUtils.sendHttpTask(serviceUrl, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }
                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
                            final String finalMsg = msg;


                            View.OnClickListener okClick = new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    if (!TextUtils.isEmpty(finalMsg) && finalMsg.contains("본인 인증에 실패")) {
                                        Intent intent = new Intent(CertifyPhoneActivity.this, PersonalInfoActivity.class);
                                        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            };
                            showErrorMessage(msg, okClick);
                            //showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        } else {
                            showErrorMessage(msg);
                            //showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        Logs.e(ret);
                        return;
                    }

                    mReceiveSrno = object.optString("SMS_TRN_SRNO");
                    if (TextUtils.isEmpty(mReceiveSrno)) {
                        showErrorMessage("SMS거래일련번호가 없습니다.");
                        mBtnOkCertifyPhone.setEnabled(false);
                        return;
                    } else {
                        //SMS 거래일련번호가 정상적으로 수신된 상태라도 SMS 인증번호가 없으면 확인버튼 비활성화
                        if ((!TextUtils.isEmpty(mEditCertifyNum.getText().toString()) && mEditCertifyNum.getText().toString().length() == 6)
                                && (mIsRequestCertifyPhone || mCheckCertifyPhone.isChecked())) {
                            mBtnOkCertifyPhone.setEnabled(true);
                        } else {
                            mBtnOkCertifyPhone.setEnabled(false);
                        }
                    }

                    final AlertDialog alertDialog = new AlertDialog(CertifyPhoneActivity.this);
                    String msg = getString(R.string.msg_send_certify_number);
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (alertDialog != null && alertDialog.isShowing())
                                alertDialog.dismiss();

                            countCertifyStop();
                            countCertifyStart();
                            mIsRequestCertifyPhone = true;

                            //인증번호 초기화한다.
                            mEditCertifyNum.setText("");
                            if (!mEditCertifyNum.isFocused()) {
                                mEditCertifyNum.requestFocus();

                                new Handler().postDelayed(new Runnable() {
                                    public void run() {
                                        Utils.showKeyboard(CertifyPhoneActivity.this, mEditCertifyNum);
                                    }
                                }, 300);
                            }
                        }
                    };

                    alertDialog.mPListener = okClick;
                    alertDialog.msg = getString(R.string.msg_send_certify_number);
                    alertDialog.show();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 인증 요청 완료 후 입력 대기 카운트 시작
     */
    public void countCertifyStart() {
        mInteractionTime = System.currentTimeMillis();
        Logs.i("interactionTime : " + mInteractionTime);

        if (mAutoTimeoutTimer == null) {
            mAutoTimeoutTimer = new Timer();
            mAutoTimeoutTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    long duration = (System.currentTimeMillis() - mInteractionTime) / 1000;
                    Logs.i("autoLogout Timer : " + duration);

                    if (duration >= FINISH_TIME) {
                        countCertifyStop();
                        updateRemainTime(0);
                        mIsRequestCertifyPhone = false;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mBtnOkCertifyPhone.setEnabled(false);
                            }
                        });
                        dismissAllPopup();
                        showOverTimeMessage();
                        mReceiveSrno = "";
                    } else {
                        long remainTimeSec = FINISH_TIME - duration;
                        updateRemainTime(remainTimeSec);
                    }
                }
            }, 0, 1000);
        }
    }

    /**
     * 인증 요청 완료 후 입력 대기 카운트 종료
     */
    public void countCertifyStop() {
        try {
            mInteractionTime = 0;
            if (mAutoTimeoutTimer != null) {
                mAutoTimeoutTimer.cancel();
                mAutoTimeoutTimer = null;
            }
        } catch (Exception e) {
            Logs.printException(e.getMessage());
        }
    }

    /**
     * 인증 요청 완료 후 입력 대기 카운트 시간 표시
     */
    public void updateRemainTime(final long remainTime) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Logs.i("Display time : " + remainTime);

                String min = String.format("%02d", remainTime / 60);
                String sec = String.format("%02d", remainTime % 60);

                StringBuilder stRemainTime = new StringBuilder();
                stRemainTime.append(min);
                stRemainTime.append(":");
                stRemainTime.append(sec);

                mTextRemainTime.setText(stRemainTime);
            }
        });
    }

    /**
     * view 클릭 시, 키패드 닫기
     * @param view
     */
    public void setHideSoftKeyboard(View view) {
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard();
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setHideSoftKeyboard(innerView);
            }
        }
    }

    /**
     * 포커스된 뷰 키패드 닫기
     */
    public void hideSoftKeyboard() {
        View view = getCurrentFocus();
        if (view == null) return;

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * SMS 자동 입력
     * @param authCode 인증값
     */
    public void autoInsertAuthCode(String authCode) {
        //mEditCertifyNum.setText(authCode);
    }

    /**
     * 에디드창 입력값 자리수 유효성 확인
     * @param editText 확인할 에디트창
     * @return
     */
    private TextWatcher getTextWatcher(final EditText editText) {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String ret = editable.toString();
                if (editText == mEditCertifyNum) {
                    if (ret.length() == 6) {
                        //SMS가 정상 수신되서 인증번호가 입력된 상태라도 거래일련번호가 없으면 확인버튼 비활성화
                        if ((mIsRequestCertifyPhone || mCheckCertifyPhone.isChecked()) && !TextUtils.isEmpty(mReceiveSrno))
                            mBtnOkCertifyPhone.setEnabled(true);
                        else
                            mBtnOkCertifyPhone.setEnabled(false);
                    } else {
                        mBtnOkCertifyPhone.setEnabled(false);
                    }
                } else if (editText == mEditPhoneNum && (ret.length() == 11)) {
                    mEditPhoneNum.clearFocus();
                    hideSoftKeyboard();
                }
            }
        };
    }

    /**
     * 통신사 조회
     */
    private void requestMobileCarrier() {
        Map param = new HashMap();
        param.put("LCCD","CLPH_CMCM_DVCD");
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("LCCD : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for(int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        RequestCodeInfo item = new RequestCodeInfo(obj);
                        mCarrierItems.add(item);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 생년월일 SMS 인증번호요청확인
     */
    private void requestCertifySMS(String certifyNum) {
        if (TextUtils.isEmpty(mReceiveSrno)) {
            Toast.makeText(this, getString(R.string.msg_resend_over_time_certify_number), Toast.LENGTH_SHORT).show();
            return;
        }

        Map param = new HashMap();
        String serviceUrl;
        if (mCommonUserInfo.isFullIdnumber()) {
            serviceUrl = WasServiceUrl.CMM0010400A04.getServiceUrl();
            param.put("BRDD", mCommonUserInfo.getBirth());
            param.put("NRID__E2E__", mCommonUserInfo.getIdnumber());
            param.put("CUST_NM", mCommonUserInfo.getName());
            param.put("SMS_TRN_SRNO", mReceiveSrno);
            param.put("SMS_ATHN_NO", certifyNum);
        } else {
            serviceUrl = WasServiceUrl.CMM0010401A02.getServiceUrl();

            String userBirth = mCommonUserInfo.getBirth();
            String userAllBirth = "";
            String sGender = mCommonUserInfo.getSexCode();
            if (userBirth.length() == 6) {
                if ("1".equals(sGender) || "2".equals(sGender))
                    userAllBirth = "19" + userBirth;
                if ("3".equals(sGender) || "4".equals(sGender))
                    userAllBirth = "20" + userBirth;
            } else {
                userAllBirth = userBirth;
            }

            param.put("BRDD", userAllBirth);
            param.put("SEX_CD", sGender);
            param.put("SMS_TRN_SRNO", mReceiveSrno);
            param.put("SMS_ATHN_NO", certifyNum);
        }

        showProgressDialog();
        HttpUtils.sendHttpTask(serviceUrl, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                Logs.e("requestCertifySMS - ret : " + ret);

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showErrorMessage(msg);
                        //showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        Logs.e(ret);
                        return;
                    }

                    String diNo = object.optString("DI_NO");
                    String ciNo = object.optString("CI_NO");

                    if(!TextUtils.isEmpty(diNo) && !TextUtils.isEmpty(ciNo)){
                        requestMemberState(diNo, ciNo);
                    }else{
                        showErrorMessage(getResources().getString(R.string.common_msg_no_reponse_value_phonecertify));
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void requestMemberState(String diNo, final String ci) {


        if(mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            Intent intent = new Intent();
            intent.putExtra("DI_NO", diNo);
            intent.putExtra("CI_NO", ci);
            intent.putExtra("CMCM_DVCD", mMobileCarrierCode);
            intent.putExtra("CPNO", mCommonUserInfo.getPhoneNumber());
            setResult(RESULT_OK, intent);
            finish();
            return;
        }

        Map param = new HashMap();
        param.put("DI_NO", diNo);
        param.put("CI_NO", ci);

        mCommonUserInfo.setDInumber(diNo);
        mCommonUserInfo.setCInumber(ci);

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showErrorMessage(msg);
                        //showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        Logs.e(ret);
                        return;
                    }

                    countCertifyStop();
                    mTextRemainTime.setText("00:00");
                    DialogUtil.alert(CertifyPhoneActivity.this,
                            "휴대폰 본인인증이\n완료되었습니다.",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                   completeCertify(object, ci);
                                }
                            });


                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void completeCertify(final JSONObject object, String ci) {
        String userName = LoginUserInfo.getInstance().getCUST_NM();
        if (TextUtils.isEmpty(userName))
            LoginUserInfo.getInstance().setCUST_NM(mCommonUserInfo.getName());
        switch (mCommonUserInfo.getEntryPoint()) {
            // 로그인 신규/기존
            case SERVICE_JOIN:
            case DEVICE_CHANGE:{
                final String entrYn = object.optString("SBK_ENTR_YN");
                // 가입 신규/재설치 완료 시 CI Number Preference로 저장.
                Prefer.setAuthPhoneCI(CertifyPhoneActivity.this, mCommonUserInfo.getCInumber());
                StonePassManager.clearInstance();
                StonePassManager stonePassManager = StonePassManager.getInstance(getApplication());
                StonePassManager.StonePassInitListener stonePassInitListener = new StonePassManager.StonePassInitListener() {
                    @Override
                    public void stonePassInitResult() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                               dismissProgressDialog();
                                // 신규 가입
                                if (TextUtils.isEmpty(entrYn) || "N".equalsIgnoreCase(entrYn)) {
                                    Intent intent = new Intent(CertifyPhoneActivity.this, AuthRegisterGuideActivity.class);
                                    mCommonUserInfo.setNewUser(true);
                                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                    startActivity(intent);
                                    finish();
                                }
                                // 기존 고객
                                else if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(entrYn)) {
                                    String accn = object.optString("ODDP_ACCN");
                                    String mbrNo = object.optString("MBR_NO");
                                    String CustNo = object.optString("CUST_NO");
                                    mCommonUserInfo.setNewUser(false);
                                    mCommonUserInfo.setMBRnumber(mbrNo);
                                    LoginUserInfo.getInstance().setCUST_NO(CustNo);

                                    // 계좌미보유 고객
                                    if (TextUtils.isEmpty(accn) || Integer.valueOf(accn) < 1) {
                                        Intent intent = new Intent(CertifyPhoneActivity.this, CustomerNoAccountActivity.class);
                                        mCommonUserInfo.setHasAccount(false);
                                        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                        startActivity(intent);
                                        finish();
                                    }
                                    // 계좌보유 고객
                                    else {
                                        Intent intent = new Intent(CertifyPhoneActivity.this, CustomerAccountGuideActivity.class);
                                        mCommonUserInfo.setHasAccount(true);
                                        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            }
                        });
                    }
                };
                showProgressDialog();
                stonePassManager.stonePassInit(stonePassInitListener);
                break;
            }

            // 로그인, 인증수단관리 패턴재등록
            case PATTERN_FORGET: {
                // 패턴 분실재등록을 본인이 아닌 사람이 한 경우 예외처리
                if (!ci.equals(Prefer.getAuthPhoneCI(CertifyPhoneActivity.this))) {
                    DialogUtil.alert(CertifyPhoneActivity.this,
                            "고객정보가 일치하지 않습니다.",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                    return;
                }
                Intent intent = new Intent(CertifyPhoneActivity.this, PatternRegActivity.class);
                String accn = object.optString("ODDP_ACCN");
                String mbrNo = object.optString("MBR_NO", "");
                Logs.e("accn: " + accn);
                if (TextUtils.isEmpty(accn) || Integer.valueOf(accn) < 1) {
                    mCommonUserInfo.setHasAccount(false);
                } else {
                    mCommonUserInfo.setHasAccount(true);
                }
                mCommonUserInfo.setMBRnumber(mbrNo);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                finish();
                break;
            }

            // 로그인 핀재등록
            case PINCODE_FORGET: {
                Intent intent;
                // 계좌보유여부 체크
                String accn = object.optString("ODDP_ACCN");
                String mbrNo = object.optString("MBR_NO", "");
                if (TextUtils.isEmpty(accn) || Integer.valueOf(accn) < 1) {
                    intent = new Intent(CertifyPhoneActivity.this, PincodeRegActivity.class);
                    mCommonUserInfo.setHasAccount(false);
                } else {
                    intent = new Intent(CertifyPhoneActivity.this, IdentificationPrepareActivity.class);
                    mCommonUserInfo.setHasAccount(true);
                }
                mCommonUserInfo.setNewUser(false);
                mCommonUserInfo.setMBRnumber(mbrNo);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                finish();
                break;
            }

            default: {
                finish();
                break;
            }
        }
    }

    /**
     * 인증번호 유효시간 완료 메세지 출력
     */
    private void showOverTimeMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideAllKeyboard();

                View.OnClickListener okClick = new OnSingleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        requestSendSms();
                    }
                };

                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                };

                final AlertDialog alertDialog = new AlertDialog(CertifyPhoneActivity.this);
                alertDialog.mPListener = okClick;
                alertDialog.mPBtText = getString(R.string.resend_certify_number);
                alertDialog.mNListener = cancelClick;
                alertDialog.mNBtText = getString(R.string.common_cancel);
                alertDialog.msg = getString(R.string.msg_over_time_certify_number);
                alertDialog.show();
            }
        });
    }

    private void hideAllKeyboard() {
        if (mEditPhoneNum.isFocused()) {
            mEditPhoneNum.clearFocus();
            Utils.hideKeyboard(CertifyPhoneActivity.this, mEditPhoneNum);
        }

        if (mEditCertifyNum.isFocused()) {
            mEditCertifyNum.clearFocus();
            Utils.hideKeyboard(CertifyPhoneActivity.this, mEditCertifyNum);
        }
    }

    private void checkAllNecessaryPolicy() {
        if (((CheckBox)findViewById(R.id.checkbox_certify_phone_01)).isChecked()
                && ((CheckBox) findViewById(R.id.checkbox_certify_phone_02)).isChecked()
                && ((CheckBox) findViewById(R.id.checkbox_certify_phone_03)).isChecked()
                && ((CheckBox) findViewById(R.id.checkbox_certify_phone_04)).isChecked())
            mCheckCertifyPhone.setChecked(true);
        else
            mCheckCertifyPhone.setChecked(false);
    }

    @Override
    public void onKeyboardShown() {
        Logs.e("onKeyboardShown");
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 500);
        mBtnOkCertifyPhone.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        Logs.e("onKeyboardHidden");
        mIsShownKeyboard = false;
        mBtnOkCertifyPhone.setVisibility(View.VISIBLE);
    }

    private class WeakHandler extends Handler {
        private WeakReference<CertifyPhoneActivity> mWeakActivity;

        WeakHandler(CertifyPhoneActivity weakactivity) {
            mWeakActivity = new WeakReference<CertifyPhoneActivity>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            CertifyPhoneActivity weakactivity = mWeakActivity.get();
            if (weakactivity != null) {
                switch (msg.what) {
                    case SCROLL_EDITTEXT: {
                        Logs.e("mIsShownKeyboard : " + mIsShownKeyboard);
                        if (!mIsShownKeyboard)
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollOffset);
                        break;
                    }
                    case SCROLL_CHECK_KEYBOARD: {
                        mIsShownKeyboard = true;
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}