package com.sbi.saidabank.activity.transaction.itransfer.single;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.TransferPhoneCompleteActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSingleActionListener;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmPhoneTransferDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.TransferPhoneResultInfo;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ITransferInputDetailPhoneLayout extends ITransferBaseLayout implements View.OnClickListener {


    private ScrollView mScrollView;

    //한글머니
//    private TextView mTvHangleMoney;
//    private TextView mTvAmount;


    //상세정보
    private RelativeLayout mLayoutSendMsg;

    private EditText        mEtDispSendMsg;
    private ImageView       mIvSendMsgErase;

    private TextView        mTvTransferBtn;

    private OnITransferSingleActionListener mActionListener;

    public ITransferInputDetailPhoneLayout(Context context) {
        super(context);
        initUX(context);
    }

    public ITransferInputDetailPhoneLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_itransfer_input_detail_phone_info, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mScrollView = layout.findViewById(R.id.scrollview);

        //한글머니
//        mTvHangleMoney = layout.findViewById(R.id.tv_hangle_money);
//
//        //보낼금액
//        mTvAmount = layout.findViewById(R.id.tv_amount);
//        mTvAmount.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if(mActionListener != null) {
//                    mTvAmount.setEnabled(false);
//                    if(mEtDispSendMsg.hasFocus()){
//                        mEtDispSendMsg.clearFocus();
//                        Utils.hideKeyboard(getContext(),mEtDispSendMsg);
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                mActionListener.openInputMoneyView();
//                            }
//                        },2500);
//                    }else{
//                        mActionListener.openInputMoneyView();
//                    }
//
//
//
//
//                }else
//                    Logs.showToast(getContext(),"ActionListener를 등록해주세요.");
//
//            }
//        });

        //상세정보 입력화면
        mLayoutSendMsg = layout.findViewById(R.id.layout_send_msg);


        int radius = (int) Utils.dpToPixel(getContext(),6);
        mLayoutSendMsg.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));

        mEtDispSendMsg = layout.findViewById(R.id.et_send_msg);
        mEtDispSendMsg.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!v.isEnabled()) return;

                int radius = (int) Utils.dpToPixel(getContext(),6);

                Drawable background;
                if(hasFocus){
                    background = GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#01a2b4");
                }else{
                    background = GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5");
                }

                mLayoutSendMsg.setBackground(background);

                if(hasFocus){
                    if(!TextUtils.isEmpty(mEtDispSendMsg.getText().toString())){
                        mIvSendMsgErase.setVisibility(VISIBLE);
                    }
                }else{
                    mIvSendMsgErase.setVisibility(GONE);
                }
            }
        });

        mEtDispSendMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(mEtDispSendMsg.getText().toString())){
                    mIvSendMsgErase.setVisibility(GONE);
                }else{
                    mIvSendMsgErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEtDispSendMsg.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(getContext(),mEtDispSendMsg);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        mIvSendMsgErase = layout.findViewById(R.id.iv_send_msg_erase);
        mIvSendMsgErase.setOnClickListener(this);

        mTvTransferBtn = layout.findViewById(R.id.tv_tranfer_btn);
        mTvTransferBtn.setOnClickListener(this);

    }

    public void setActionListener(OnITransferSingleActionListener listener){
        mActionListener = listener;
    }

    public void setInputMoney(String inputMoney){
        if(TextUtils.isEmpty(inputMoney)) return;

        //이체정보에 보낼금액을 넣어준다.
        ITransferDataMgr.getInstance().getTransferContactsInfo().setTRN_AMT(inputMoney);


//        String dispMoney = Utils.moneyFormatToWon(Double.valueOf(inputMoney));
//        String msg = dispMoney + " 원";
//        Utils.setTextWithSpan(mTvAmount,msg,"원", Typeface.NORMAL,"#000000");
//
//
//        if(Double.parseDouble(inputMoney) >= 10000){
//            String hangul = Utils.convertHangul(inputMoney);
//            mTvHangleMoney.setText(hangul+"원");
//            if(mTvHangleMoney.getVisibility() != VISIBLE){
//                mTvHangleMoney.setVisibility(VISIBLE);
//            }
//        }else{
//            mTvHangleMoney.setVisibility(INVISIBLE);
//        }
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_tranfer_btn:
                mTvTransferBtn.setEnabled(false);
                String sendMsg = mEtDispSendMsg.getText().toString();
                ITransferDataMgr.getInstance().getTransferContactsInfo().setDEPO_BNKB_MRK_NM(sendMsg);
                requestVerifyTransferPhone();
                break;
            case R.id.iv_send_msg_erase:
                mEtDispSendMsg.setText("");
                break;
        }
    }


    public void hideView(){
        if(mLayoutSendMsg.hasFocus()){
            Utils.hideKeyboard(getContext(),mLayoutSendMsg);
            mLayoutSendMsg.clearFocus();
        }

        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_out_transfer_view);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //mTvAmount.setEnabled(true);
                setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(ani);
    }

    public void showView(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLayoutSendMsg.clearFocus();
            }
        },100);
        setVisibility(VISIBLE);

        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_in_transfer_view);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(ani);
    }

    public void clearInputText(){
        mEtDispSendMsg.setText("");
    }

    /**
     * 오늘 동일금액 이체 확인요청
     */
    private void requestVerifyTransferPhone() {


        String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String TLNO = ITransferDataMgr.getInstance().getTransferContactsInfo().getTLNO();
        String TRN_AMT = ITransferDataMgr.getInstance().getTransferContactsInfo().getTRN_AMT();

        if (TextUtils.isEmpty(TLNO))
            TLNO = "";

        Map param = new HashMap();
        param.put("TRTM_DVCD", "2");
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("MNRC_CPNO", TLNO);
        param.put("TRN_AMT", TRN_AMT);

        ((BaseActivity)getContext()).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190200A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0010400A01 : " + ret);

                if(((BaseActivity)getContext()).onCheckHttpError(ret,false)){
                    ((BaseActivity)getContext()).dismissProgressDialog();
                    mTvTransferBtn.setEnabled(true);
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);

                    String SAME_AMT_TRNF_YN = object.optString("SAME_AMT_TRNF_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SAME_AMT_TRNF_YN)) {
                        ((BaseActivity)getContext()).dismissProgressDialog();
                        View.OnClickListener cancelClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        };

                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestTransFeeferPhone();
                            }
                        };

                        final AlertDialog alertDialog = new AlertDialog(getContext());
                        alertDialog.mPListener = okClick;
                        alertDialog.mNListener = cancelClick;
                        alertDialog.msg = "오늘 같은 분에게 동일한 금액을\n이체하셨습니다.\n정말 이체하시겠습니까?";
                        alertDialog.show();
                        return;
                    }

                    requestTransFeeferPhone();

                } catch (JSONException e) {
                    Logs.printException(e);
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 휴대폰이체 수수료조회
     *
     * 20210221
     * 초기에 한도조회한다고 만들어 두었던거 같은데..
     * 단지 수수료만 리턴해준다.
     * 웹에서 수수료도 무조건 0으로 하드코딩 되어 있는데..
     * 추후 수수료가 변경될 경우 웹에서 수정하도록 한다.
     *
     */
    private void requestTransFeeferPhone() {

        String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String TRN_AMT = ITransferDataMgr.getInstance().getTransferContactsInfo().getTRN_AMT();

        Map param = new HashMap();
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("TRN_AMT", TRN_AMT);
        param.put("TRNF_DVCD", "4");

        ((BaseActivity)getContext()).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)getContext()).dismissProgressDialog();
                Logs.i("TRA0020300A01 : " + ret);
                if(((BaseActivity)getContext()).onCheckHttpError(ret,false)){
                    mTvTransferBtn.setEnabled(true);
                    return;
                }


                try {
                    JSONObject object = new JSONObject(ret);

                    String TRNF_FEE = object.optString("TRNF_FEE");
                    if (TextUtils.isEmpty(TRNF_FEE))
                        ITransferDataMgr.getInstance().getTransferContactsInfo().setTRNF_FEE("0");
                    else
                        ITransferDataMgr.getInstance().getTransferContactsInfo().setTRNF_FEE(TRNF_FEE);

                    showConfirmDialog();
                } catch (JSONException e) {
                    Logs.printException(e);
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 휴대폰이체 확인창 표시
     */
    private void showConfirmDialog() {

        String withdrawAccount = "";

                View view = getSlideView(ITransferSelectAccountLayout.class);
        if(view != null){
            String accountName = ITransferDataMgr.getInstance().getWTCH_ACNO_NM();
            String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();
            String shortAccNo = accountNo.substring(accountNo.length()-4,accountNo.length());


            withdrawAccount = accountName + " [" +shortAccNo + "]";
        }

        ContactsInfo contactsInfo = ITransferDataMgr.getInstance().getTransferContactsInfo();

        SlidingConfirmPhoneTransferDialog singleTransferDialog = new SlidingConfirmPhoneTransferDialog(getContext(), withdrawAccount, contactsInfo,
                new SlidingConfirmPhoneTransferDialog.FinishListener() {
                    @Override
                    public void OnCancelListener() {
                        mTvTransferBtn.setEnabled(true);
                    }

                    @Override
                    public void OnOKListener() {
                        requestSignData();
                    }
                });
        singleTransferDialog.setCancelable(false);
        singleTransferDialog.show();
    }

    /**
     * 이체정보 전자 서명값 요청
     *
     */
    private void requestSignData() {
        Map param = new HashMap();

        ContactsInfo contactsInfo = ITransferDataMgr.getInstance().getTransferContactsInfo();

        String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String FLNM = contactsInfo.getFLNM();
        String TLNO = contactsInfo.getTLNO();
        String TRN_AMT = contactsInfo.getTRN_AMT();
        String DEPO_BNKB_MRK_NM = contactsInfo.getDEPO_BNKB_MRK_NM();

        if (TextUtils.isEmpty(WTCH_ACNO) || TextUtils.isEmpty(FLNM) ||
                TextUtils.isEmpty(FLNM) || TextUtils.isEmpty(TLNO))
            return;

        param.put("TRN_TYCD", "2");
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("MNRC_TRGT_DEPR_NM", FLNM);
        param.put("MNRC_CPNO", TLNO);
        param.put("TRAM", TRN_AMT);

        if (TextUtils.isEmpty(DEPO_BNKB_MRK_NM))
            DEPO_BNKB_MRK_NM = "";

        param.put("MNRC_ACCO_MRK_CNTN", DEPO_BNKB_MRK_NM);
        ((BaseActivity)getContext()).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)getContext()).dismissProgressDialog();
                Logs.i("TRA0019900A01 : " + ret);
                if(((BaseActivity)getContext()).onCheckHttpError(ret,false)){
                    mTvTransferBtn.setEnabled(true);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    String ELEC_SGNR_VAL_CNTN = object.optString("ELEC_SGNR_VAL_CNTN");
                    if (!TextUtils.isEmpty(ELEC_SGNR_VAL_CNTN)) {
                        showOTPActivity(ELEC_SGNR_VAL_CNTN);
                    }
                } catch (JSONException e) {
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }


    /**
     * 핀코드 입력창 표시
     * @param signData 전자서명된 이체정보
     */
    private void showOTPActivity(String signData) {
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);

        Intent intent = new Intent(getContext(), PincodeAuthActivity.class);
        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        commonUserInfo.setSignData(signData);
        intent.putExtra(Const.INTENT_SERVICE_ID, "");
        intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
        intent.putExtra(Const.INTENT_TRN_CD, "EIF50001");
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        ((BaseActivity)getContext()).startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
        ((BaseActivity)getContext()).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 인증 메소드가 취소되었을때 호출
     */
    public void cancelAuthMethod(){
        mTvTransferBtn.setEnabled(true);
    }

    public void showHideSoftKeyboard(boolean keyboardShowState){
        if(keyboardShowState){
            mTvTransferBtn.setVisibility(GONE);
        }else{
            mTvTransferBtn.setVisibility(VISIBLE);
            mEtDispSendMsg.clearFocus();
        }
    }



    /**
     * 이체 요청
     * @param elecSrno 전자서명된 이체정보
     */
    public void requestTransfer(String elecSrno) {
        Map param = new HashMap();
        param.put("ELEC_SGNR_SRNO", elecSrno);


        ContactsInfo contactsInfo = ITransferDataMgr.getInstance().getTransferContactsInfo();

        String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String TLNO = contactsInfo.getTLNO();
        String FLNM = contactsInfo.getFLNM();
        String TRN_AMT = contactsInfo.getTRN_AMT();
        String DEPO_BNKB_MRK_NM = contactsInfo.getDEPO_BNKB_MRK_NM();
        if (TextUtils.isEmpty(DEPO_BNKB_MRK_NM))
            DEPO_BNKB_MRK_NM = "";

        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("MNRC_TRGT_DEPR_NM", FLNM);
        param.put("MNRC_CPNO", TLNO);
        param.put("MNRC_ACNO", "");
        param.put("TRAM", TRN_AMT);
        param.put("MNRC_ACCO_MRK_CNTN", DEPO_BNKB_MRK_NM);
        ((BaseActivity)getContext()).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)getContext()).dismissProgressDialog();
                Logs.i("TRA0020400A01 : " + ret);
                if(((BaseActivity)getContext()).onCheckHttpError(ret,false)){
                    mTvTransferBtn.setEnabled(true);
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);

                    String TRNF_KEY_VAL = object.optString("TRNF_KEY_VAL");
                    if (TextUtils.isEmpty(TRNF_KEY_VAL)) {


                        ((BaseActivity)getContext()).showErrorMessage(getContext().getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String MSG_CNTN = object.optString("MSG_CNTN");

                    TransferPhoneResultInfo phoneResultInfo = new TransferPhoneResultInfo(object);
                    Intent intent = new Intent(getContext(), TransferPhoneCompleteActivity.class);
                    //intent.putParcelableArrayListExtra(Const.INTENT_TRANSFER_PHONE_INFO, mListContacts);
                    intent.putExtra(Const.INTENT_TRANSFER_PHONE_RET, phoneResultInfo);
                    intent.putExtra(Const.INTENT_TRANSFER_PHONE_KAKAO_MSG, MSG_CNTN);
                    ((BaseActivity)getContext()).startActivity(intent);
                    //((BaseActivity)getContext()).overridePendingTransition(0, 0);
                    ((BaseActivity)getContext()).finish();
                } catch (JSONException e) {
                    Logs.printException(e);
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }

}
