package com.sbi.saidabank.activity.main2.common.clipboard;

import android.content.Context;
import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PreloadBankStockCodeMgr {

    private static PreloadBankStockCodeMgr instance = null;

    private String[][] mExeptionString = {
            {"카뱅","카카오뱅크"},
            {"BOA","뱅크오브아메리카"}
    };

    /**
     * Singleton instance 생성
     *
     * @return instance
     */
    public static PreloadBankStockCodeMgr getInstance() {
        if (instance == null) {
            synchronized (PreloadBankStockCodeMgr.class) {
                if (instance == null) {
                    instance = new PreloadBankStockCodeMgr();
                }
            }
        }
        return instance;
    }

    public static void clearInstance(){
        instance = null;
    }

    private String checkExptionString(String searchText){
        for(int i=0;i<mExeptionString.length;i++){
            if(mExeptionString[i][0].equals(searchText)){
                return mExeptionString[i][1];
            }
        }
        return null;
    }

    public RequestCodeInfo getBankStockCodeInfo(Context context,String searchText){
        String newSearchText = searchText;
        if(searchText.contains("저축")){
            newSearchText = "저축";
        }

        String exepStr = checkExptionString(searchText);
        if(!TextUtils.isEmpty(exepStr)){
            newSearchText = exepStr;
        }

        if(!TextUtils.isEmpty(newSearchText)){
            newSearchText = newSearchText.toUpperCase();
        }


        //먼저 은행부터 검색해본다.
        String bankListString = Prefer.getBankList(context);
        Logs.e("getBankStockCodeInfo - bankListString : " + bankListString);
        if(!TextUtils.isEmpty(bankListString)){
            bankListString = bankListString.toUpperCase();
            try {
                JSONObject object = new JSONObject(bankListString);

                JSONArray array = object.optJSONArray("REC");
                if (array == null)
                    return null;

                for (int index = 0; index < array.length(); index++) {
                    JSONObject jsonObject = array.getJSONObject(index);
                    if (jsonObject == null)
                        continue;

                    String codeName = jsonObject.optString("CD_NM");

                    //Logs.e("getBankStockCodeInfo - codeName : " + codeName);
                    //Logs.e("getBankStockCodeInfo - searchText : " + searchText);
                    if(codeName.contains(newSearchText)){
                        return new RequestCodeInfo(jsonObject);
                    }

                }
            } catch (JSONException e) {
                MLog.e(e);
            }
        }


        //은행이 없으면 증권을 검색한다.
        String stockListString = Prefer.getStockList(context);
        Logs.e("getBankStockCodeInfo - stockListString : " + stockListString);
        if(!TextUtils.isEmpty(stockListString)){
            stockListString = stockListString.toUpperCase();

            try {
                JSONObject object = new JSONObject(stockListString);

                JSONArray array = object.optJSONArray("REC");
                if (array == null)
                    return null;

                for (int index = 0; index < array.length(); index++) {
                    JSONObject jsonObject = array.getJSONObject(index);
                    if (jsonObject == null)
                        continue;

                    String codeName = jsonObject.optString("CD_NM");
                    if(codeName.equals(searchText)){
                        return new RequestCodeInfo(jsonObject);
                    }
                }
            } catch (JSONException e) {
                MLog.e(e);
            }
        }


        return null;
    }
}