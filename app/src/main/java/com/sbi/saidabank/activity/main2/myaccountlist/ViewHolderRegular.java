package com.sbi.saidabank.activity.main2.myaccountlist;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.common.dialog.SlidingMain2TransferDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CoupleIconSmallView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 보통예금을 표시하는 뷰(마이너스통장 포함)
 * 코드값 300,330
 */
public class ViewHolderRegular extends BaseViewHolder<Main2AccountInfo> {
    private View mItemView;
    private LinearLayout mLayoutBody;   //전체몸통
    private RelativeLayout mLayoutSubBody;   //전체몸통
    private ImageView    mIvShadow;
    private LinearLayout mLayoutItemMain; //아이템 표시 레이아웃

    private TextView mTvAccName;        //계좌명
    private ImageView mIvCopyAccNum;    //계좌번호 복사버튼
    private TextView mTvAccType;        //계좌종류
    private TextView mTvAccNum;         //계좌번호
    private TextView mTvAmount;         //계좌금액
    private TextView mTvPaymentDay;     //월납입일

    //대출 여신거래 상태 레이아웃
    //private RelativeLayout mLayoutBtnAndLoanRelay;
    private LinearLayout mLayoutLoanRelay;
    private TextView       mTvLoanRelayMsg;
    private TextView       mTvLoanJudgeMsg;


    //이체 충전 버튼 레이아웃
    private LinearLayout mLayoutBtn;    //버튼 전체 레이아웃
    private LinearLayout mLayoutBtnTransfer;   //이체버튼레이아웃
    //private LinearLayout mLayoutBtnCharge;   //충전버튼레이아웃
    private TextView     mTvBtnTransfer;  //이체버튼
    //private TextView     mTvBtnCharge;    //충전버튼

    //커플 아이콘
    private RelativeLayout mLayoutCouple;


    public static ViewHolderRegular newInstance(ViewGroup parent,int dispType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_main2_home_myaccount_list_item_regular, parent, false);
        return new ViewHolderRegular(parent.getContext(), itemView,dispType);
    }

    public ViewHolderRegular(Context context, @NonNull View itemView,int dispType) {
        super(context,itemView,dispType);

        mItemView = itemView;
        mDispType = dispType;

        int radius = (int)Utils.dpToPixel(mContext,20);
        mLayoutBody     = itemView.findViewById(R.id.layout_body);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mLayoutSubBody  = itemView.findViewById(R.id.layout_subbody);
        mIvShadow     = itemView.findViewById(R.id.iv_shadow);

        mLayoutItemMain = itemView.findViewById(R.id.layout_item_main);

        mTvAccName      = itemView.findViewById(R.id.tv_account_name);
        mIvCopyAccNum   = itemView.findViewById(R.id.iv_copy_account_num);
        mIvCopyAccNum.setVisibility(View.VISIBLE);

        mTvAccType      = itemView.findViewById(R.id.tv_account_type);
        mTvAccType.setTextColor(Color.parseColor("#27aef4"));//파란색

        mTvAccNum       = itemView.findViewById(R.id.tv_account_num);
        mTvAmount       = itemView.findViewById(R.id.tv_amount);
        mTvPaymentDay   = itemView.findViewById(R.id.tv_payment_day);

        //마이너스통장 여신 상태메세지
        //mLayoutBtnAndLoanRelay  = itemView.findViewById(R.id.layout_loan_relay_judge_msg);
        mLayoutLoanRelay        = itemView.findViewById(R.id.layout_loan_relay);
        mLayoutLoanRelay.setVisibility(View.GONE);
        mTvLoanRelayMsg         = itemView.findViewById(R.id.tv_loan_relay_msg);
        mTvLoanRelayMsg.setTextColor(Color.parseColor("#27aef4"));//파란색

        //화살표를 푸른색으로 변경한다.
        ((ImageView)itemView.findViewById(R.id.iv_loan_relay_msg_arrow)).setImageResource(R.drawable.btn_arr_bl);

        mTvLoanJudgeMsg         = itemView.findViewById(R.id.tv_loan_judge_msg);
        mTvLoanJudgeMsg.setTextColor(Color.parseColor("#27aef4"));//파란색
        mTvLoanJudgeMsg.setVisibility(View.GONE);

        //이체,충전 버튼 영역
        mLayoutBtn  = itemView.findViewById(R.id.layout_btn);

        if(mDispType == Main2AccountAdapter.DISP_TYPE_MY){
            mLayoutBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#27aef4",new int[]{0,0,radius,radius}));//파란색
        }else{
            mLayoutBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#ff7e6d",new int[]{0,0,radius,radius}));//빨강색
        }

        mLayoutBtnTransfer = itemView.findViewById(R.id.layout_btn_1);
        mLayoutBtnTransfer.setVisibility(View.GONE);
//        mLayoutBtnCharge   = itemView.findViewById(R.id.layout_btn_2);
//        mLayoutBtnCharge.setVisibility(View.GONE);
        mTvBtnTransfer  = itemView.findViewById(R.id.tv_btn_1);
//        mTvBtnCharge  = itemView.findViewById(R.id.tv_btn_2);

        mLayoutCouple = itemView.findViewById(R.id.layout_couple);


        mItemView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        //레이아웃의 높이를 재조정해둔다.
                        ViewGroup.LayoutParams params = mIvShadow.getLayoutParams();
                        params.width = mItemView.getWidth();
                        params.height = mItemView.getHeight();
                        mIvShadow.setLayoutParams(params);

                        mItemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

    }

    @Override
    public void onBindView(Main2AccountInfo mainAccountInfo) {
        Logs.e("ViewHolderNomal - onBindView");
        mLayoutItemMain.setClickable(true);
        mLayoutItemMain.setTag(mainAccountInfo);
        mLayoutItemMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutItemMain.setClickable(false);
                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                startWebAction(mainAccountInfo, new Main2AccountAdapter.OnProcWebActionListener() {
                    @Override
                    public void onOKWebAction(Object accountInfo) {
                        Main2AccountInfo mainAccountInfo = (Main2AccountInfo)accountInfo;
                        String ACNO = mainAccountInfo.getACNO();
                        String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
                        if (TextUtils.isEmpty(ACNO)  || TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                            return;

                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        String url = WasServiceUrl.INQ0010100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                        String param =  "ACNO=" + ACNO;
                        intent.putExtra(Const.INTENT_PARAM, param);
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onErrorWebAction(String code, Object accountInfo) {
                        if(code.equals("99")){
                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            String url = WasServiceUrl.MAI0070100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                            mContext.startActivity(intent);
                        }
                    }

                });
            }
        });

        String accountName = mainAccountInfo.getTRNF_DEPR_NM();
        if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){//마이너스통장일때
            if(accountName.equals("입출금통장")){
                accountName = "마이너스통장";
            }
        }
        mTvAccName.setText(accountName);

        String accountNum = mainAccountInfo.getACNO();
        if (!TextUtils.isEmpty(accountNum)) {
            String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
            mTvAccNum.setText(account);
        }

        mIvCopyAccNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO - 클립보드에 번호 카피해둔다.
                //형태 : 저축 계좌번호
                String saveStr = "저축 " + mTvAccNum.getText().toString();
                ClipboardManager clipboard = (ClipboardManager)mContext.getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("simple text", saveStr);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(mContext,"계좌번호가 복사되었습니다.",Toast.LENGTH_LONG).show();
            }
        });

        //커플통장 : 마이너스 통장 불가.
        //일반계좌 : 거래정지 > 출금정지 > 한도제한
        //마이너스계좌 : 기한이익상실 > 거래정지 > 출금정지 >연체
        if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){
            //마이너스계좌
            if(mainAccountInfo.getOVRD_DVCD().equals("2")){
                mTvAccType.setText("기한이익상실");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("1")){
                mTvAccType.setText("거래정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("2")){
                mTvAccType.setText("지급정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("3")){
                mTvAccType.setText("출금정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getOVRD_DVCD().equals("1")){
                mTvAccType.setText("연체");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else{
                mTvAccType.setText("");
            }
        }else{
            //요구불계좌
           if(mainAccountInfo.getACDT_DCL_CD().equals("1")){
                mTvAccType.setText("거래정지");
                mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("2")){
                mTvAccType.setText("지급정지");
               mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getACDT_DCL_CD().equals("3")){
                mTvAccType.setText("출금정지");
               mTvAccType.setTextColor(Color.parseColor("#e0250c"));
            }else if(mainAccountInfo.getLMIT_LMT_ACCO_YN().equals("Y")){
                mTvAccType.setText("한도제한");
               mTvAccType.setTextColor(Color.parseColor("#009beb"));

                if(!TextUtils.isEmpty(mainAccountInfo.getFAX_SEND_PRGS_CNTN())){
                    mTvLoanJudgeMsg.setVisibility(View.VISIBLE);
                    mTvLoanJudgeMsg.setText(mainAccountInfo.getFAX_SEND_PRGS_CNTN());
                }
            }else{
               mTvAccType.setText("");
           }

            //커플계좌 탭이고 공유자이면 계좌의 상태(거래정지,지급정지,출금정지,한도제한)을 보이면 않된다.
            if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
                mTvAccType.setVisibility(View.INVISIBLE);
            }
        }

        //충전하기 버튼 처리
//        displayChargeButton(mainAccountInfo);
        //이체하기 버튼 처리
        displayTransferButton(mainAccountInfo);


        //금액출력
        String BLNC = mainAccountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            mTvAmount.setText(BLNC);
        }

        //커플계좌이면 커플 아이콘 출력한다.
        Logs.e("getSHRN_ACCO_YN : " + mainAccountInfo.getSHRN_ACCO_YN());
        if(mainAccountInfo.getSHRN_ACCO_YN().equals("Y")){
            if(mDispType==Main2AccountAdapter.DISP_TYPE_MY){
                //TODO - 커플일경우 거틀이체일이 나와야 한다.
                String ower_img_data = Main2DataInfo.getInstance().getUSER_PROFL_IMG_CNTN();
                String ower_name = Main2DataInfo.getInstance().getUSER_NM();
                String share_img_data = Main2DataInfo.getInstance().getSHRP_PROFL_IMG_CNTN();
                String share_name = Main2DataInfo.getInstance().getSHRP_NM();
                if(!TextUtils.isEmpty(share_name)){
                    CoupleIconSmallView coupleIconSmallView = new CoupleIconSmallView(mContext);
                    coupleIconSmallView.setData(ower_img_data,ower_name,share_img_data,share_name);
                    mLayoutCouple.addView(coupleIconSmallView);
                }else{
                    if(mLayoutCouple.getChildCount() > 0)
                        mLayoutCouple.removeAllViews();
                }
            }

            if(TextUtils.isEmpty(mainAccountInfo.getSHRN_TRNF_DD())){
                mTvPaymentDay.setVisibility(View.GONE);
            }else{
                String SHRN_TRNF_DD = mainAccountInfo.getSHRN_TRNF_DD();
                if(TextUtils.isEmpty(SHRN_TRNF_DD)){
                    mTvPaymentDay.setVisibility(View.GONE);
                }else{
                    mTvPaymentDay.setText("알림일: 매월 " + SHRN_TRNF_DD + "일");
                }
            }
        }else{
            if(mLayoutCouple.getChildCount() > 0)
                mLayoutCouple.removeAllViews();

            if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){//마이너스통장여부
                String MM_PI_DD = mainAccountInfo.getMM_PI_DD();
                if (!TextUtils.isEmpty(MM_PI_DD))
                    mTvPaymentDay.setText("매월 " + MM_PI_DD + "일");
                else
                    mTvPaymentDay.setVisibility(View.GONE);
            }else{
                mTvPaymentDay.setVisibility(View.GONE);
            }
        }

        //마이너스 대출 상태표시
        displayMinusLoanState(mainAccountInfo);


        //한도계좌 심사중 문구상태표시
        displaylimitDocDudgeState(mainAccountInfo);
    }

    /**
     * 충전하기 버튼 처리
     * 내계좌탭에서 나타남.
     *
     * @param mainAccountInfo
     */
//    private void displayChargeButton(Main2AccountInfo mainAccountInfo){
//        //이체/상환 버튼 출력
//        //사고신고코드로 구분. - 사고신고가 접수되면 버튼 모두 사라진다.
//        if(TextUtils.isEmpty(mainAccountInfo.getACDT_DCL_CD())){
//            //커플계좌 탭으로 진입했고 커플 타입이 공유자일때
//            if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE&& Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
//                mLayoutBtnCharge.setVisibility(View.GONE);
//            }else{
//                //충전버튼 출력
//                if(mainAccountInfo.getIMMD_WTCH_ACCO_YN().equals("Y")){//해당 계좌에 충전이 설정되어 있는상태에서
//                    if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){//마이너스통장이면
//                        if(mainAccountInfo.getOVRD_DVCD().equals("2")) {//기한이익상실이면 충전버튼 사라지고
//                            mLayoutBtnCharge.setVisibility(View.GONE);
//                        }else{//0.정상이거나 1.연체일때는 충전버튼 보여야 한다.
//                            mLayoutBtnCharge.setVisibility(View.VISIBLE);
//                        }
//                    }else{//마이너스 통장이 아니면 충전 버튼 보이고.
//                        mLayoutBtnCharge.setVisibility(View.VISIBLE);
//                    }
//                }else{
//                    //충전버튼 설정이 없을때 - 주계좌에만 충전버튼을 표시해 준다.
//                    if(mainAccountInfo.getDSCT_CD().equals("300") && Main2DataInfo.getInstance().getACCOUNT_CHARGE_SET_YN().equals("N")){
//                        mLayoutBtnCharge.setVisibility(View.VISIBLE);
//                        mLayoutBtn.setVisibility(View.VISIBLE);
//                    }else{
//                        mLayoutBtnCharge.setVisibility(View.GONE);
//                    }
//                }
//            }
//        }else{
//            mLayoutBtnCharge.setVisibility(View.GONE);
//        }
//
//        //충전하기 버튼 클릭!!!
//        mLayoutBtnCharge.setTag(mainAccountInfo);
//        mLayoutBtnCharge.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
//                String ACNO = mainAccountInfo.getACNO();
//
//                Intent intent = new Intent(mContext, WebMainActivity.class);
//                String url = WasServiceUrl.UNT0110100.getServiceUrl();
//                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//
//                String param = "DSCT_CD=0";
//                if(Main2DataInfo.getInstance().getACCOUNT_CHARGE_SET_YN().equals("Y")){
//                    param =  "ACNO=" + ACNO + "&" + "DSCT_CD=0";
//                }
//
//                intent.putExtra(Const.INTENT_PARAM, param);
//                mContext.startActivity(intent);
//            }
//        });
//    }

    /**
     * 좌측 버튼을 각 상태로 표시
     * 이체 - 내계좌 탭, 커플계좌탭에서 계좌주일때
     * 이체요청 - 커플계좌탭에서 공유자일때
     * 상환 - 마이너스통장으로 기한 도래했을때
     *
     * @param mainAccountInfo
     */
    private void displayTransferButton(Main2AccountInfo mainAccountInfo){
        //이체/상환 버튼 출력
        //사고신고코드로 구분. - 사고신고가 접수되면 버튼 모두 사라진다.
        if(TextUtils.isEmpty(mainAccountInfo.getACDT_DCL_CD())){
            mLayoutBtnTransfer.setVisibility(View.VISIBLE);
            mLayoutBtn.setVisibility(View.VISIBLE);
            //마이너스통장이면 - 마이너스 통장이면 커플통장을 신청할수 없다.
            if(mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")){
                if(mainAccountInfo.getOVRD_DVCD().equals("2")){//기한이익상실
                    mTvBtnTransfer.setText("상환");
                    procBtnPayBack(mainAccountInfo);
                }else{
                    mTvBtnTransfer.setText("이체");
                    procBtnTransfer(mainAccountInfo);
                }
            }else{
                //커플계좌 탭으로 진입했고 커플 타입이 공유자일때
                if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE&& Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
                    //이체요청일경우엔 충전버튼 사라지도록 한다.
//                    mTvBtnCharge.setVisibility(View.GONE);
                    mTvBtnTransfer.setText("이체요청");
                    procBtnRequestTransfer(mainAccountInfo);
                }else{
                    mTvBtnTransfer.setText("이체");
                    procBtnTransfer(mainAccountInfo);
                }
            }

        }else{
            mLayoutBtnTransfer.setVisibility(View.GONE);
        }
    }

    /**
     * 버튼이 이체로 표기될때 처리
     * 내계좌탭의 계좌이거나 커플계좌 탭에서 계좌주일경우 이체로 표시
     *
     * @param mainAccountInfo
     */
    private void procBtnTransfer(Main2AccountInfo mainAccountInfo){
        //이체버튼
        mLayoutBtnTransfer.setTag(mainAccountInfo.getACNO());
        mLayoutBtnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String ACNO = (String)v.getTag();

                SlidingMain2TransferDialog dialog = new SlidingMain2TransferDialog(mContext,new SlidingMain2TransferDialog.OnConfirmListener(){
                    @Override
                    public void onConfirmPress(final int selectIndex) {

                        TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true, new TransferUtils.OnCheckFinishListener() {
                            @Override
                            public void onCheckFinish() {
                                switch (selectIndex){
                                    case 1: {//통합이체
                                        //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                                        ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                                        ITransferDataMgr.getInstance().setWTCH_ACNO(ACNO);

                                        Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                                        mContext.startActivity(intent);
                                        break;
                                    }
                                    case 2: {//안심이체
                                        Intent intent = new Intent(mContext, WebMainActivity.class);
                                        intent.putExtra("url", WasServiceUrl.TRA0090100.getServiceUrl());
                                        if (!TextUtils.isEmpty(ACNO)){
                                            String param = "ACNO="+ACNO;
                                            intent.putExtra(Const.INTENT_PARAM, param);
                                        }
                                        mContext.startActivity(intent);
                                        break;
                                    }
                                }
                            }
                        });

                    }
                });
                dialog.show();
            }

        });
    }

    /**
     * 버튼이 이체요청로 표기될때 처리
     * 커플계좌 탭에서 계좌주가 아닌 공유자 일때 아래 처리
     *
     * @param mainAccountInfo
     */
    private void procBtnRequestTransfer(Main2AccountInfo mainAccountInfo){
        //이체요청버튼
        mLayoutBtnTransfer.setTag(mainAccountInfo);
        mLayoutBtnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
                startWebAction(mainAccountInfo, new Main2AccountAdapter.OnProcWebActionListener() {
                    @Override
                    public void onOKWebAction(Object accountInfo) {
                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        String url = WasServiceUrl.TRA0180100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        mContext.startActivity(intent);
                    }

                    @Override
                    public void onErrorWebAction(String code, Object accountInfo) {

                    }
                });

            }

        });
    }

    /**
     * 마이너스 통장에서 버튼이 상환으로 표시될때 처리
     */
    private void procBtnPayBack(Main2AccountInfo mainAccountInfo){

        mLayoutBtnTransfer.setTag(mainAccountInfo);
        mLayoutBtnTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                String ACNO = mainAccountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
                if (TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                    return;

                String SUBJ_CD =  mainAccountInfo.getSUBJ_CD();
                if(TextUtils.isEmpty(SUBJ_CD)){
                    SUBJ_CD = ACCO_IDNO.substring(0, 2);
                }
                requestLoanRepay(ACNO, ACCO_IDNO, SUBJ_CD);
            }
        });
    }

    /**
     * 상환 요청
     */
    private void requestLoanRepay(String ACNO, String ACCO_IDNO, String SUBJ_CD) {
        Map param = new HashMap();
        param.put("ACNO", ACNO);
        param.put("ACCO_IDNO", ACCO_IDNO);
        param.put("SUBJ_CD", SUBJ_CD);

        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.LON0050100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if(((BaseActivity) mContext).onCheckHttpError(ret, false)) return;

                Intent intent = new Intent(mContext, WebMainActivity.class);
                String url = WasServiceUrl.LON0050900.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                mContext.startActivity(intent);
            }
        });
    }

    /**
     * 마이너스 통장에서 여신거래 조건변경을 한 경우 상태 메세지 출력
     *
     * @param mainAccountInfo
     */
    private void displayMinusLoanState(Main2AccountInfo mainAccountInfo){
        //마이너스 통장 체크 - 아니면 실행하지 않는다.
        if(!mainAccountInfo.getCMPH_BNKB_LOAN_YN().equals("Y")) return;

        if(DataUtil.isNotNull(mainAccountInfo.getACPT_CNTN())){//접수내용 체크
            //먼저 메세지 출력 전체 레이아웃을 보여준다.
            //mLayoutBtnAndLoanRelay.setVisibility(View.VISIBLE);
            if(DataUtil.isNotNull(mainAccountInfo.getLOAN_PROP_NO())){//대출신청번호체크
                mTvLoanRelayMsg.setText(mainAccountInfo.getACPT_CNTN());
                mLayoutLoanRelay.setVisibility(View.VISIBLE);

                mTvLoanJudgeMsg.setVisibility(View.GONE);

                mLayoutLoanRelay.setTag(mainAccountInfo);
                mLayoutLoanRelay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
                        requestMinusLoanRelayGoOn(mainAccountInfo);
                    }
                });

            }else{
                mTvLoanJudgeMsg.setText(mainAccountInfo.getACPT_CNTN());
                mTvLoanJudgeMsg.setVisibility(View.VISIBLE);
            }

            //여신거래 상태메세지가 출력되게 되면 Top Margin을 5 -> 15로 변경한다.
            //디자인팀 요청..
//            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) mLayoutBtnAndLoanRelay.getLayoutParams();
//            params.topMargin = (int)Utils.dpToPixel(mContext,15);
//            mLayoutBtnAndLoanRelay.setLayoutParams(params);

        }else{
            mLayoutLoanRelay.setVisibility(View.GONE);
        }
    }

    /**
     * 한도제한계좌 증빙서류 심사중   상태 메세지 출력
     *
     * @param mainAccountInfo
     */
    private void displaylimitDocDudgeState(Main2AccountInfo mainAccountInfo){
        //공유자이면서 커플계좌탭에서는 서류 심사중 메세지 출력하지 않는다.
        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
            return;
        }

        MLog.d();
        if(DataUtil.isNotNull(mainAccountInfo.getFAX_SEND_PRGS_CNTN())) {//접수내용 체크
            mTvLoanJudgeMsg.setText(mainAccountInfo.getFAX_SEND_PRGS_CNTN());
            mTvLoanJudgeMsg.setVisibility(View.VISIBLE);
        }else{
            mTvLoanJudgeMsg.setVisibility(View.GONE);
        }
    }

    /**
     * 마이너스 대출 상태 이어하기
     */
    private void requestMinusLoanRelayGoOn(Main2AccountInfo mainAccountInfo) {
        String DSCT_CD = mainAccountInfo.getDSCT_CD();
        String ACNO = mainAccountInfo.getACNO();
        String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
        String SUBJ_CD = mainAccountInfo.getSUBJ_CD();
        final String PROP_STEP_CD = mainAccountInfo.getPROP_STEP_CD();
        final String LOAN_PROP_NO = mainAccountInfo.getLOAN_PROP_NO();

        Logs.d("DSCT_CD : " + DSCT_CD);
        Logs.d("ACNO : " + ACNO);
        Logs.d("ACCO_IDNO : " +  ACCO_IDNO);
        Logs.d("SUBJ_CD : " +  SUBJ_CD);
        Logs.d("PROP_STEP_CD : " + PROP_STEP_CD);
        Logs.d("LOAN_PROP_NO : " + LOAN_PROP_NO);

        if (TextUtils.isEmpty(DSCT_CD) || TextUtils.isEmpty(ACNO) || TextUtils.isEmpty(ACCO_IDNO) ||
                TextUtils.isEmpty(SUBJ_CD) || TextUtils.isEmpty(PROP_STEP_CD) || TextUtils.isEmpty(LOAN_PROP_NO))
            return;

        Map param = new HashMap();
        param.put("ACNO", ACNO);
        param.put("ACCO_IDNO", ACCO_IDNO);
        param.put("SUBJ_CD", SUBJ_CD);
        param.put("LOAN_LNKN_PRGS_STEP_DVCD", PROP_STEP_CD);
        param.put("PROP_NO", LOAN_PROP_NO);

        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if(((BaseActivity) mContext).onCheckHttpError(ret, false)) return;

                try {
                    final JSONObject object = new JSONObject(ret);

                    String SCRN_ID = object.optString("SCRN_ID");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = SaidaUrl.getBaseWebUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = "PROP_NO=" + LOAN_PROP_NO + "&INTR_RDCN_DEMD_PROP_NO=" + LOAN_PROP_NO + "&LOAN_LNKN_PRGS_STEP_DVCD=" + PROP_STEP_CD;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    mContext.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}
