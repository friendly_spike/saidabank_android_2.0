package com.sbi.saidabank.activity.login;

import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

/**
 * Saidabank_android
 * Class: BlockIPActivity
 * Created by 950546
 * Date: 2018-10-25
 * Time: 오전 9:50
 * Description: BlockIP 안내 화면
 */
public class BlockIPActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blockip);
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
