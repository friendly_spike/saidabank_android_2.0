package com.sbi.saidabank.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.login.SplashActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.MainNoticeDialog;
import com.sbi.saidabank.common.dialog.ProgressExDialog;
import com.sbi.saidabank.common.dialog.ReadyLogoutDialog;
import com.sbi.saidabank.common.dialog.SlidingFakeAppsListDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PicassoTransformations;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;
import com.sbi.saidabank.solution.v3.V3Manager;
import com.secuchart.android.sdk.base.FakeFinderReadyCallback;
import com.secuchart.android.sdk.base.listener.FakeAppDetectListener;
import com.secuchart.android.sdk.base.listener.RemoteAppDetectListener;
import com.secuchart.android.sdk.base.model.FakeAppResultStatus;
import com.secuchart.android.sdk.base.model.fake_app.FakeAppResult;
import com.secuchart.android.sdk.base.model.remote_app.RemoteAppResult;
import com.secuchart.android.sdk.internal.FakeFinder;
import com.sullegro.typekit.TypekitContextWrapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * BaseActivity : 공통 액티비티
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity implements LogoutTimeChecker.LogoutTimeCheckerListener, FakeFinderReadyCallback {

    static final String INSTANCE_STATE = "save_instances_state";
    private ProgressExDialog mProgressExDialog;
    private ReadyLogoutDialog mReadyLogoutDialog;
    private CommonErrorDialog mCommonErrorDialog;
    private AlertDialog mAlertDialog;
    private MainNoticeDialog mNoticeDialog;
    public SlidingFakeAppsListDialog mFakeAppsListDialog = null;
    private FakeFinder mFakeFinder = null;
    private boolean mIsForground = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        // 재시작되었을 경우 기존 앱을 종료하고 인트로부터 다시 시작
        if ((DataUtil.isNotNull(savedInstanceState)) && (savedInstanceState.getBoolean(INSTANCE_STATE))) {
            initActivity();
        }
        disableScreenCapture();
        if (isDev())
            return;
        initFakeFinder();
    }

    @Override
    protected void onResume() {
        MLog.d();
        super.onResume();
        SaidaApplication.getInstance().setActivity(this);
        if (LogoutTimeChecker.getInstance(this).isBackground()) {
            LogoutTimeChecker.getInstance(this).autoLogoutStop();
            LogoutTimeChecker.getInstance(this).setBackground(false);
            LoginUserInfo.clearInstance();
            LoginUserInfo.getInstance().setLogin(false);
            SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
            if (DataUtil.isNotNull(mApplicationClass)) {
                mApplicationClass.allActivityFinish(true);
            } else {
                finish();
            }
            Intent intent = new Intent(this, LogoutActivity.class);
            intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_TIMEOUT);
            startActivity(intent);
            return;
        } else {
            LogoutTimeChecker.getInstance(this).autoLogoutUserInteraction(DataUtil.isNotNull(mReadyLogoutDialog));
        }
        if (isDev())
            return;
        if (DataUtil.isNull(mFakeFinder)) {
            initFakeFinder();
        }
        onStartFakeFinder();
    }

    @Override
    protected void onPause() {
        MLog.d();
        super.onPause();
        dismissFakeAppDialog();
        onClearFakeFinder();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgressExDialog != null) {
            mProgressExDialog.dismiss();
            mProgressExDialog = null;
        }

    }

    @Override
    protected void onDestroy() {
        MLog.d();
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(INSTANCE_STATE, true);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    @Override
    public void onReadyLogoutTime(long remainTime) {
        updateReadyLogoutDialog(remainTime);
    }

    @Override
    public void onEndLogoutTime(boolean isRestart) {
        MLog.d();
        dismissReadyLogoutDialog();
        if (isRestart) {
            mReadyLogoutDialog = null;
        } else {
            if (Utils.isAppOnForeground(this)) {
                LogoutTimeChecker.getInstance(this).setBackground(false);
                LoginUserInfo.clearInstance();
                LoginUserInfo.getInstance().setLogin(false);

                SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
                if (mApplicationClass != null)
                    mApplicationClass.allActivityFinish(true);
                else
                    finish();

                Intent intent = new Intent(this, LogoutActivity.class);
                intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_TIMEOUT);
                startActivity(intent);
            } else {
                LogoutTimeChecker.getInstance(this).setBackground(true);
                LoginUserInfo.getInstance().setLogin(false);
            }
        }

    }

    @Override
    public void onFakeFinderReady() {
        onCheckFakeFinder();
    }

    @Override
    public void onFakeFinderReadyFail(int errorCode) {
        // TODO Auto-generated method stub
        MLog.i("FakeFinder Ready Fail Error Code >> " + errorCode);
    }

    /**
     * 기존 앱을 종료하고 인트로부터 다시 시작
     */
    public void initActivity() {
        LoginUserInfo.clearInstance();
        LoginUserInfo.getInstance().setLogin(false);
        Intent intent = new Intent(this, IntroActivity.class);
        startActivity(intent);
        ActivityCompat.finishAffinity(this);
    }

    /**
     * FakeFinder 설정
     */
    public void initFakeFinder() {
        if (DataUtil.isNull(mFakeFinder)) {
            mFakeFinder = FakeFinder.getInstance();
            mFakeFinder.setSiteId(LicenseKey.FAKE_FINDER_ID);
            mFakeFinder.setLicenseKey(LicenseKey.FAKE_FINDER_LICENSE_KEY);
            mFakeFinder.registerFakeAppDetectListener(new FakeAppDetectListener() {
                @Override
                public void onResultSuccess(String requestId, List<? extends FakeAppResult> resultList) {
                    for (FakeAppResult item : resultList) {
                        // 악성앱이 감지되었을 경우
                        if (item.getResult() == FakeAppResultStatus.DANGER) {
                            boolean isLogin = LoginUserInfo.getInstance().isLogin();
                            if (isLogin) {
                                if (mIsForground) {
                                    showFakeAppDialog(resultList);
                                    mIsForground = false;
                                } else {
                                    boolean isFakeFinderAllow = LoginUserInfo.getInstance().isFakeFinderAllow();
                                    // 앱 최초 실행시 or 로그인 상태 && 계속하기 버튼을 누르지 않았을 경우
                                    if (!isFakeFinderAllow) {
                                        showFakeAppDialog(resultList);
                                    }
                                }
                            } else if (SaidaApplication.getInstance().getActivity() instanceof SplashActivity) {
                                showFakeAppDialog(resultList);
                            }
                            break;
                        }
                    }
                }

                @Override
                public void onResultFail(String requestId, int errorCode) {
                    // TODO Auto-generated method stub
                }

            });
            mFakeFinder.registerRemoteAppDetectListener(new RemoteAppDetectListener() {
                @Override
                public void onResultSuccess(@Nullable String requestId, @Nullable List<? extends RemoteAppResult> resultList) {
                    for (RemoteAppResult item : resultList) {
                        if (DataUtil.isNotNull(item.getPackageId())) {
                            // 원격지원 탐지 안내 팝업을 띄운다.
                            showRemoteDialog();
                            break;
                        }
                    }
                }

                @Override
                public void onResultFail(@Nullable String requestId, int errorCode) {
                    // TODO Auto-generated method stub
                }
            });
        }
    }

    /**
     * 악성 앱 및 원격탐지 시작
     */
    public void onStartFakeFinder() {
        if (DataUtil.isNotNull(mFakeFinder)) {
            // FakeFinder Start
            mFakeFinder.startFakeFinder(BaseActivity.this);
        }
    }

    /**
     * 악성 앱 및 원격탐지 종료
     */
    public void onStopFakeFinder() {
        if (DataUtil.isNotNull(mFakeFinder)) {
            // FakeFinder Stop
            mFakeFinder.stopFakeFinder();
        }
    }

    /**
     * 악성 앱 및 원격탐지 리스너 초기화
     */
    public void onClearFakeFinder() {
        if (DataUtil.isNotNull(mFakeFinder)) {
            MLog.d();
            // 악성 앱 탐지 결과 리스너 초기화
            mFakeFinder.clearFakeAppDetectListener();
            // 원격 앱 탐지 결과 리스너 초기화
            mFakeFinder.clearRemoteAppDetectListener();
            // FakeFinder 초기화
            mFakeFinder = null;
        }
    }

    /**
     * 악성 앱 및 원격탐지 실행
     */
    private void onCheckFakeFinder() {
        if (DataUtil.isNotNull(mFakeFinder)) {
            MLog.d();
            // 악성 앱 탐지 결과 받기
            mFakeFinder.fetchFakeAppResult();
            // 원격 앱 탐지 결과 받기
            mFakeFinder.fetchRemoteAppResult();
        }
    }

    /**
     * 포그라운드 상태를 체크
     *
     * @param forground
     */
    public void setForground(boolean forground) {
        this.mIsForground = forground;
    }

    /**
     * 악성액 목록 팝업을 띄운다.
     *
     * @param resultList : 악성앱 목록
     */
    private void showFakeAppDialog(List<? extends FakeAppResult> resultList) {
        if (SaidaApplication.getInstance().getActivity() instanceof Main2Activity) {
            Main2Activity.mMainActivity.dismissMainPopupDialog();
        }
        dismissFakeAppDialog();
        mFakeAppsListDialog = new SlidingFakeAppsListDialog(BaseActivity.this, (List<FakeAppResult>) resultList);
        mFakeAppsListDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mFakeAppsListDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        mFakeAppsListDialog.setCancelable(false);
        mFakeAppsListDialog.setOnItemListener(new SlidingFakeAppsListDialog.OnItemListener() {
            @Override
            public void onContinue() {
                mFakeAppsListDialog = null;
                if (SaidaApplication.getInstance().getActivity() instanceof Main2Activity) {
                    LoginUserInfo.getInstance().setFakeFinderAllow(true);
                    Main2Activity.mMainActivity.showMainPopupDialog();
                } else if (SaidaApplication.getInstance().getActivity() instanceof SplashActivity) {

                } else {
                    LoginUserInfo.getInstance().setFakeFinderAllow(true);
                }
            }

            @Override
            public void onExitApp() {
                doExitApp();
            }
        });
        mFakeAppsListDialog.show();
    }

    /**
     * 악성액 목록 팝업을 종료
     */
    public void dismissFakeAppDialog() {
        if (isFinishing()) return;
        if (DataUtil.isNotNull(mFakeAppsListDialog) && mFakeAppsListDialog.isShowing()) {
            mFakeAppsListDialog.dismiss();
            mFakeAppsListDialog = null;
        }
    }

    /**
     * 원격지원 탐지 안내 팝업을 띄운다.
     */
    private void showRemoteDialog() {
        if (isFinishing()) return;
        final Dialog dialog = new Dialog(BaseActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_alert_remote);
        Button btnOK = dialog.findViewById(R.id.btn_ok_alert_remote_exit);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                doExitApp();
            }
        });
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.width = size.x;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().getDecorView().setBackgroundResource(android.R.color.transparent);
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * 프로그레스바 출력 - 딜레이 기능 추가.
     */
    public void showProgressDialog() {
        Logs.e("showProgressDialog - 0");
        if (isFinishing()) return;
        Logs.e("showProgressDialog - 1");
        LogoutTimeChecker.getInstance(this).autoLogoutUserInteraction(false);

        if(DataUtil.isNotNull(mProgressExDialog) && mProgressExDialog.isShowing()) return;
        Logs.e("showProgressDialog - 2");
        dismissReadyLogoutDialog();

        mProgressExDialog = new ProgressExDialog(BaseActivity.this);
        mProgressExDialog.show();

    }

    /**
     * 프로그레스바 종료
     */
    public void dismissProgressDialog() {
        Logs.e("dismissProgressDialog - 0");
        if (isFinishing()) return;
        Logs.e("dismissProgressDialog - 1");
        if (DataUtil.isNotNull(mProgressExDialog) && mProgressExDialog.isShowing()) {
            Logs.e("dismissProgressDialog - 2");
            mProgressExDialog.dismiss();
            mProgressExDialog = null;
        }
    }

    /**
     * 로그아웃 연장 알림창 노출 및 업데이트
     */
    private void updateReadyLogoutDialog(long remainTime) {
        if (isFinishing()) return;
        if (DataUtil.isNull(mReadyLogoutDialog)) {
            MLog.d();
            dismissReadyLogoutDialog();
            mReadyLogoutDialog = new ReadyLogoutDialog(BaseActivity.this);
            mReadyLogoutDialog.setCancelable(false);
            mReadyLogoutDialog.show();
            mReadyLogoutDialog.updateTime(remainTime);
        } else {
            mReadyLogoutDialog.updateTime(remainTime);
        }
    }

    /**
     * 로그아웃 연장 알림창 종료
     */
    public void dismissReadyLogoutDialog() {
        if (isFinishing()) return;
        if (DataUtil.isNotNull(mReadyLogoutDialog) && mReadyLogoutDialog.isShowing()) {
            mReadyLogoutDialog.dismiss();
            mReadyLogoutDialog = null;
        }
    }

    /**
     * 로그아웃 안내 다이얼로그를 띄운다.
     */
    public void showLogoutDialog() {
        if (isFinishing()) return;
        DialogUtil.alert(BaseActivity.this,
                "로그아웃",
                "취소",
                "로그아웃 하시겠습니까?",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (LoginUserInfo.getInstance().isFinishedLogin()) {
                            V3Manager.getInstance(BaseActivity.this).stopV3MobilePlus();
                        } else {
                            LoginUserInfo.getInstance().setFinishedLogin(true);
                        }

                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().setLogin(false);
                        LogoutTimeChecker.getInstance(BaseActivity.this).autoLogoutStop();
                        SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
                        if (mApplicationClass != null)
                            mApplicationClass.allActivityFinish(true);
                        else
                            finish();
                        Intent intent = new Intent(BaseActivity.this, LogoutActivity.class);
                        intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
                        startActivity(intent);
                    }
                },

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
    }

    /**
     * 전문 오류 메세지 표시
     *
     * @param msg              오류 메세지
     * @param codeRet          오류 코드
     * @param scrnId           스크린 아이디
     * @param object           전문 오류 헤더 jsonobject
     * @param isShownRetryText "잠시후 다시시도해주세요" 문구 표시 여부
     */
    public void showCommonErrorDialog(String msg, String codeRet, String scrnId, JSONObject object, boolean isShownRetryText) {
        showCommonErrorDialog(msg, codeRet, scrnId, object, isShownRetryText, null);
    }

    /**
     * 전문 오류 메세지 표시
     *
     * @param msg     오류 메세지
     * @param codeRet 오류 코드
     */
    public void showCommonErrorDialog(String msg, String codeRet, String scrnId, JSONObject object, boolean isShownRetryText, CommonErrorDialog.OnConfirmListener okClick) {
        if (isFinishing()) return;

        msg = msg.replaceAll("\\\\n", "\n");
        if (DataUtil.isNotNull(mCommonErrorDialog)) {
            mCommonErrorDialog.dismiss();
            mCommonErrorDialog = null;
        }

        mCommonErrorDialog = new CommonErrorDialog(BaseActivity.this, msg, codeRet, scrnId, object, isShownRetryText);
        mCommonErrorDialog.setOnConfirmListener(okClick);
        mCommonErrorDialog.setOnDismissListener(new CommonErrorDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mCommonErrorDialog = null;
            }
        });
        mCommonErrorDialog.show();
    }

    /**
     * 오류 메세지 표시
     *
     * @param msg 오류 메세지
     */
    public void showErrorMessage(String msg) {
        if (isFinishing()) return;

        if (DataUtil.isNotNull(mAlertDialog)) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };
        msg = msg.replaceAll("\\\\n", "\n");
        mAlertDialog = new AlertDialog(BaseActivity.this);
        mAlertDialog.mPListener = okClick;
        mAlertDialog.msg = msg;
        mAlertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mAlertDialog = null;
            }
        });
        mAlertDialog.show();
    }

    /**
     * 오류 메세지 표시
     *
     * @param msg 오류 메세지
     */
    public void showErrorMessage(String msg, View.OnClickListener okClick) {
        if (isFinishing()) return;

        if (DataUtil.isNotNull(mAlertDialog)) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }
        mAlertDialog = new AlertDialog(BaseActivity.this);
        msg = msg.replaceAll("\\\\n", "\n");
        mAlertDialog.mPListener = okClick;
        mAlertDialog.msg = msg;
        mAlertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mAlertDialog = null;
            }
        });
        mAlertDialog.show();
    }

    /**
     * 취소 메세지 표시
     *
     * @param msg 취소 메세지
     */
    public void showCancelMessage(String msg) {
        if (isFinishing()) return;

        if (DataUtil.isNotNull(mAlertDialog)) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }

        mAlertDialog = new AlertDialog(BaseActivity.this);
        msg = msg.replaceAll("\\\\n", "\n");
        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        };
        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        };
        mAlertDialog.mPListener = okClick;
        mAlertDialog.mNListener = cancelClick;
        mAlertDialog.msg = msg;
        mAlertDialog.mPBtText = "나가기";
        mAlertDialog.mNBtText = getResources().getString(R.string.common_cancel);
        mAlertDialog.setOnDismissListener(new AlertDialog.OnDismissListener() {
            @Override
            public void onDismiss() {
                mAlertDialog = null;
            }
        });
        mAlertDialog.show();
    }

    /**
     * Http request시 에러 체크사항 공통으로 만듬.
     *
     * @param ret
     * @param isFinish
     * @return
     */
    public boolean onCheckHttpError(String ret, final boolean isFinish) {
        return onCheckHttpError(ret,"",isFinish);
    }
    public boolean onCheckHttpError(String ret,String msgStr, final boolean isFinish) {
        if (DataUtil.isNull(ret)) {
            dismissProgressDialog();
            String msg = getString(R.string.msg_debug_no_response);
            if(!TextUtils.isEmpty(msgStr)){
                msg = msgStr;
            }
            showErrorMessage(msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (isFinish) finish();
                }
            });
            return true;
        }

        try {
            JSONObject object = new JSONObject(ret);
            if (DataUtil.isNull(object)) {
                dismissProgressDialog();
                String msg = getString(R.string.msg_debug_err_response);
                if(!TextUtils.isEmpty(msgStr)){
                    msg = msgStr;
                }
                showErrorMessage(msg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (isFinish) finish();
                    }
                });
                return true;
            }

            JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
            String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
            if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                dismissProgressDialog();
                String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                if (TextUtils.isEmpty(msg))
                    msg = getString(R.string.common_msg_no_reponse_value_was);
                String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);


                if(msg.contains("오픈뱅킹으로 이체가 불가한 기관")){
                    TransferUtils.showDialogEnterOpenBank(this);
                    return true;
                }else if(msg.contains("일시적으로 계좌의 정보")){
                    TransferUtils.showDialogProblemBank(this);
                    return true;
                }

                //이상금융거래 에러 차단.
                if ("EEFN0306".equalsIgnoreCase(errCode) ||
                        "EEFN0307".equalsIgnoreCase(errCode) ||
                        "EEIF0516".equalsIgnoreCase(errCode)) {
                    Intent intent = new Intent(BaseActivity.this, WebMainActivity.class);
                    String url = WasServiceUrl.ERR0030100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode +
                            "&" + Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                    intent.putExtra(Const.INTENT_PARAM, param);

                    startActivity(intent);
                    finish();
                    return true;
                }

                CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                    @Override
                    public void onConfirmPress() {
                        if (isFinish) finish();
                    }
                };
                showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, okClick);
                return true;
            }

        } catch (JSONException e) {
            MLog.e(e);
        }

        return false;
    }

    /**
     * 이미지 공지 팝업을 노출
     *
     * @param item 공지 팝업 정보
     */
    public void showNoticeDialog(final MainPopupInfo item) {
        if (DataUtil.isNotNull(mNoticeDialog)) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }
        if (isFinishing()) return;
        mNoticeDialog = new MainNoticeDialog(BaseActivity.this, item, new MainNoticeDialog.OnListener() {
            @Override
            public void onImageClick() {
                MLog.d();
                doExitApp();
            }

            @Override
            public void onCloseClick() {
                MLog.d();
                doExitApp();
            }

            @Override
            public void onNoShowClick() {
                MLog.d();
                doExitApp();
            }
        });
        mNoticeDialog.mRightText = getResources().getString(R.string.common_confirm);
        mNoticeDialog.setCancelable(false);
        mNoticeDialog.setTransformation(PicassoTransformations.resizeTransformation);
        mNoticeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                MLog.d();
                mNoticeDialog = null;
                doExitApp();
            }
        });
        mNoticeDialog.show();
    }

    /**
     * 앱을 종료
     */
    private void doExitApp() {
        V3Manager.getInstance(getApplicationContext()).stopV3MobilePlus();
        ActivityCompat.finishAffinity(BaseActivity.this);
        System.exit(0);
    }

    /**
     * 현재 떠 있는 CommonError/Error/Cancel message 팝업 모두 닫음
     */
    public void dismissAllPopup() {
        if (DataUtil.isNotNull(mCommonErrorDialog)) {
            mCommonErrorDialog.dismiss();
            mCommonErrorDialog = null;
        }
        if (DataUtil.isNotNull(mAlertDialog)) {
            mAlertDialog.dismiss();
            mAlertDialog = null;
        }
        if (DataUtil.isNotNull(mNoticeDialog)) {
            mNoticeDialog.dismiss();
            mNoticeDialog = null;
        }
    }

    /**
     * 화면캡쳐 방지
     */
    public void disableScreenCapture() {
        // 릴리즈 버전일 때만 캡쳐 방지
        if (!BuildConfig.DEBUG) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    /**
     * 화면캡쳐 방지해지
     */
    public void enableScreenCapture() {
        if (!BuildConfig.DEBUG) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    /**
     * 개발계 접속여부
     *
     * @return
     */
    private boolean isDev() {
        return (BuildConfig.DEBUG && SaidaUrl.getBaseWebUrl().equals(SaidaUrl.DEV_SERVER));
    }
}
