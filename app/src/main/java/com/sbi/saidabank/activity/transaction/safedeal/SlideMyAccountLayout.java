package com.sbi.saidabank.activity.transaction.safedeal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.adater.SafeDealMyAccountAdapter;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 내 계좌 리스트 슬라이드 화면
 */
public class SlideMyAccountLayout extends SlideBaseLayout implements View.OnClickListener{


    private ArrayList<MyAccountInfo> mArrayMyAccount = new ArrayList<>();  // 나의 계좌 리스트값
    private SafeDealMyAccountAdapter mMyAccountAdapter;                    // 나의 계좌 리스트 어댑터

    private ListView mAccountListView;
    private MyAccountInfo mSelectedAccountInfo;

    private RelativeLayout mLayoutScaleTitle;
    private TextView mScaleTitleFirst;
    private TextView mScaleTitleSecond;

    private Double mBalanceAmount = 0.0;                // 계좌 잔액
    private Double mTransferOneTime = 0.0;              // 1회 이체한도
    private Double mTransferOneDay = 0.0;               // 일일 이체한도

    private int mSafeDealKind;

    public SlideMyAccountLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideMyAccountLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        setTag("SlideMyAccountLayout");
        View layout = View.inflate(context, R.layout.layout_safedeal_slide_my_account, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);
        mLayoutBody.setVisibility(INVISIBLE);

        mLayoutScaleTitle = layout.findViewById(R.id.layout_scale_title);
        mLayoutScaleTitle.setOnClickListener(this);
        mScaleTitleFirst = layout.findViewById(R.id.tv_scale_title_first);
        mScaleTitleSecond = layout.findViewById(R.id.tv_scale_title_second);

        mAccountListView = layout.findViewById(R.id.listview);
        initLayoutSetBottom(true,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_scale_title:
                View viewResaon = getSlideView(SlideDealReasonLayout.class);
                if(viewResaon != null && viewResaon.getVisibility() == VISIBLE){
                    ((SlideDealReasonLayout)viewResaon).slidingDown(250,0);
                }

                View viewMoney = getSlideView(SlideInputMoneyLayout.class);
                if(viewMoney != null && viewMoney.getVisibility() == VISIBLE){
                    //((SlideInputMoneyLayout)viewMoney).scaleOrigin(0,0);
                    ((SlideInputMoneyLayout)viewMoney).slidingDown(250,150);
                }
                scaleOrigin(500,500);
                break;
        }
    }

    @Override
    public void scaleSmall(final int aniTime, int delayTime) {
        //if(isScale) return;

        mLayoutScaleTitle.setAlpha(0f);
        mLayoutScaleTitle.setVisibility(VISIBLE);
        mLayoutScaleTitle.animate()
                .alpha(1f)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }
                } )
                .start();

        super.scaleSmall(aniTime, delayTime);

        //금액입력화면 올린다.
        View view = getSlideView(SlideInputMoneyLayout.class);
        if(view != null){
            ((SlideInputMoneyLayout)view).slidingUp(250,aniTime - 300);
        }


        //계좌 리스트가 이미 보이지 않으면 리턴한다.
        if(mLayoutBody.getVisibility() != VISIBLE) return;

        //계좌 리스트는 사라지는 에니메이션.
        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_out_transfer_view);
        ani.setDuration(aniTime);
        mLayoutBody.clearAnimation();
        mLayoutBody.startAnimation(ani);
        mLayoutBody.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mLayoutBody.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //mLayoutMoney.invalidate();
                //displayComma();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void scaleOrigin(int aniTime, int delayTime) {
        //if(!isScale) return;

        mLayoutScaleTitle.animate()
                .alpha(0f)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutScaleTitle.setVisibility(INVISIBLE);
                        mLayoutScaleTitle.setAlpha(1f);
                    }
                } )
                .start();

        //계좌 리스트는 사라지는 에니메이션.
        mLayoutBody.setAlpha(1f);
        mLayoutBody.setVisibility(VISIBLE);
        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_in_transfer_view);
        ani.setDuration(aniTime);
        mLayoutBody.clearAnimation();
        mLayoutBody.startAnimation(ani);

        super.scaleOrigin(aniTime, delayTime);
    }

    @Override
    public void slidingUp(final int aniTime, int delayTime) {
        setVisibility(VISIBLE);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mActivity.isFinishing()) return;

                mSlideLayout.animate()
                        .translationY(0)
                        .setDuration(aniTime)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mActionlistener.onAnimationEnd(mSlideLayout, AnimationKind.ANI_SLIDE_UP);
                                scaleSmall(500,0);
                            }
                        } );
            }
        }, delayTime);
    }

    /**
     * 전체 계좌 리스트 생성
     */
    public void makeMyAccountList(int safeDealKind,String mJsonAccount) {

        MLog.d();
        mSafeDealKind = safeDealKind;

        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mArrayMyAccount = userInfo.getMyAccountInfoArrayList();

        if(mArrayMyAccount.size() == 0) return;

        mMyAccountAdapter = new SafeDealMyAccountAdapter(getContext(), mArrayMyAccount);
        mAccountListView.setAdapter(mMyAccountAdapter);
        mAccountListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mMyAccountAdapter.setSelectIndex(position);

                MyAccountInfo account = mArrayMyAccount.get(position);
                if(!account.getACNO().equals(mSelectedAccountInfo.getACNO())){
                    mSelectedAccountInfo = account;
                    requestTransLimit(mSafeDealKind,account.getACNO(),true);
                } else{
                    setDefaultScaleTitle(false);
                    scaleSmall(500,0);
                }
            }
        });


        if (DataUtil.isNull(mJsonAccount)) {
            mSelectedAccountInfo = mArrayMyAccount.get(0);
        } else {

            // 로그인 세션정보의 계좌번호와 넘어온 계좌번호와 일치할경우 계좌정보를 세팅한다.
            for (MyAccountInfo account : mArrayMyAccount) {
                if (account == null)
                    continue;

                if (mJsonAccount.equalsIgnoreCase(account.getACNO())) {
                    mSelectedAccountInfo = account;
                    break;
                }
            }
        }

        if (DataUtil.isNull(mSelectedAccountInfo)) {
            mSelectedAccountInfo = mArrayMyAccount.get(0);
        }
        mMyAccountAdapter.setSelectAccountInfo(mSelectedAccountInfo);

        //대표계좌가 선택되었으면 모델에 넣어준다.
        TransferSafeDealDataMgr.getInstance().setWTCH_ACNO(mSelectedAccountInfo.getACNO());
        TransferSafeDealDataMgr.getInstance().setACCO_ALS(mSelectedAccountInfo.getACCO_ALS());

        //setDefaultScaleTitle(false);
        requestTransLimit(mSafeDealKind,mSelectedAccountInfo.getACNO(),false);

    }

    //다른 슬라이드 레이아웃에서 계좌정보를 사용할때를 위해.
    public MyAccountInfo getCurrentAccountInfo(){
        return mSelectedAccountInfo;
    }


    /**
     * 이체 한도 조회
     *
     * @param account  출금계좌
     */
    private void requestTransLimit(int safeDealKind,final String account, final boolean isCallList) {
        MLog.d();

        Map param = new HashMap();
        param.put("TRN_DVCD", "0");
        param.put("CUST_NO", LoginUserInfo.getInstance().getCUST_NO());
        param.put("ACNO", account);
        if(safeDealKind == Const.SAFEDEAL_TYPE_PROPERTY)
            param.put("TRN_TYCD", "9");
        else
            param.put("TRN_TYCD", "1");

        if(isCallList) mActionlistener.showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0090100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if(isCallList) mActionlistener.dismissProgressDialog();

                if(mActionlistener.onCheckHttpError(ret,true))   return;

                try {
                    JSONObject object = new JSONObject(ret);

                    //저장값을 여기에 넣어줘야 아래에서 처리시 에러가 발생하지 않는다.
                    //변경된 계좌번호를 저장해 둔다.
                    TransferSafeDealDataMgr.getInstance().setWTCH_ACNO(account);
                    //한도계좌여부 저장해둔다.

                    TransferSafeDealDataMgr.getInstance().setLMIT_LMT_ACCO_YN(mSelectedAccountInfo.getLMIT_LMT_ACCO_YN());

                    String TI1_TRNF_LMIT_AMT = object.optString("TI1_TRNF_LMIT_AMT");
                    if (!TextUtils.isEmpty(TI1_TRNF_LMIT_AMT))
                        mTransferOneTime = Double.valueOf(TI1_TRNF_LMIT_AMT);

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                    } else {
                        String D1_TRNF_LMIT_AMT = object.optString("D1_TRNF_LMIT_AMT");
                        if (!TextUtils.isEmpty(D1_TRNF_LMIT_AMT))
                            mTransferOneDay = Double.valueOf(D1_TRNF_LMIT_AMT);
                    }

                    //이체가능금액
                    mBalanceAmount = Double.valueOf(object.optString("WTCH_TRN_POSB_AMT"));
                    //String amount = Utils.moneyFormatToWon(mBalanceAmount);


                    View view = getSlideView(SlideInputMoneyLayout.class);
                    if(view != null){
                        ((SlideInputMoneyLayout)view).setOneTimeOneDayLimit(mTransferOneTime,mTransferOneDay);
                        ((SlideInputMoneyLayout)view).setBalanceAmount(mBalanceAmount);
                    }
                    setDefaultScaleTitle(false);
                    if(isCallList){
                        scaleSmall(500,0);
                    }
                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 스케일할때 처음 나오는 타이틀
     */
    public void setDefaultScaleTitle(boolean ani){
        if(ani){
            int centerPos = mLayoutScaleTitle.getHeight();
            ObjectAnimator animator = ObjectAnimator.ofFloat(mLayoutScaleTitle, "rotationX", centerPos, 360);
            animator.setDuration(500);
            animator.start();
        }

        MyAccountInfo accountInfo = mSelectedAccountInfo;
        String ACCO_ALS = accountInfo.getACCO_ALS();
        if (TextUtils.isEmpty(ACCO_ALS)) {
            ACCO_ALS = accountInfo.getPROD_NM();
        }

        String ACNO = accountInfo.getACNO();
        if (!TextUtils.isEmpty(ACNO)) {
            int lenACNO = ACNO.length();
            if (lenACNO > 4)
                ACNO = ACNO.substring(lenACNO - 4, lenACNO);
        }
        String name = ACCO_ALS + " [" + ACNO + "]";
        mScaleTitleFirst.setText(name);


        //이체가능금액을 보여준다.
        String amount = Utils.moneyFormatToWon(Double.valueOf(mBalanceAmount));
        mScaleTitleSecond.setText(amount + getContext().getString(R.string.won));

/*
        String ACCO_BLNC = accountInfo.getACCO_BLNC();
        if (!TextUtils.isEmpty(ACCO_BLNC)) {
            String amount = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
            mScaleTitleSecond.setText(amount + getContext().getString(R.string.won));
        }
 */
    }

    /**
     * 보낼금액 입력에서 확인 눌러 거래사유가 나올때 표시되어야할 스케일 타이틀
     */
    public void changeScaleTitle(){
        int centerPos = mLayoutScaleTitle.getHeight();
        ObjectAnimator animator = ObjectAnimator.ofFloat(mLayoutScaleTitle, "rotationX", centerPos, 360);
        animator.setDuration(500);
        animator.start();

        mScaleTitleFirst.setText("출금계좌");

        MyAccountInfo accountInfo = mSelectedAccountInfo;
        String ACCO_ALS = accountInfo.getACCO_ALS();
        if (TextUtils.isEmpty(ACCO_ALS)) {
            ACCO_ALS = accountInfo.getPROD_NM();
        }

        String ACNO = accountInfo.getACNO();
        if (!TextUtils.isEmpty(ACNO)) {
            int lenACNO = ACNO.length();
            if (lenACNO > 4)
                ACNO = ACNO.substring(lenACNO - 4, lenACNO);
        }

        String name = ACCO_ALS + " [" + ACNO + "]";

        mScaleTitleSecond.setText(name);


    }

    /**
     * 계좌변경시 해당계좌의 상태및 출금가능 금액 재조회한다.
     * 이체한도는 계좌가 아닌 사람에게 걸리는 것이므로 여기서 한도는 조회하지 않는다.
     *
     * @param accountInfo  선택된 계좌정보
     */
    private void requestBallanceAmount(final MyAccountInfo accountInfo) {
        MLog.d();

        Map param = new HashMap();
        param.put("ACNO", accountInfo.getACNO());
        mActionlistener.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.UNT0060100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                mActionlistener.dismissProgressDialog();

                if(mActionlistener.onCheckHttpError(ret,false))   return;

                try {
                    JSONObject object = new JSONObject(ret);

                    mBalanceAmount = Double.valueOf(object.optString("WTCH_POSB_AMT"));

                    View view = getSlideView(SlideInputMoneyLayout.class);
                    if(view != null){
                        ((SlideInputMoneyLayout)view).setBalanceAmount(mBalanceAmount);
                    }

                    mSelectedAccountInfo = accountInfo;
                    mMyAccountAdapter.setSelectAccountInfo(mSelectedAccountInfo);

                    //안심이체정보에 변경되 계좌번호를 넣어준다.
                    TransferSafeDealDataMgr.getInstance().setWTCH_ACNO(mSelectedAccountInfo.getACNO());
                    TransferSafeDealDataMgr.getInstance().setACCO_ALS(mSelectedAccountInfo.getACCO_ALS());

                    setDefaultScaleTitle(false);
                    scaleSmall(500,0);
                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

}
