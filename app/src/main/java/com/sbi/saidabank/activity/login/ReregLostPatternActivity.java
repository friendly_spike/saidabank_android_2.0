package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;

/**
 * siadabank_android
 * Class: ReregLostPatternActivity
 * Created by 950546
 * Date: 2018-12-18
 * Time: 오전 10:33
 * Description: ReregLostPatternActivity
 */
public class ReregLostPatternActivity extends BaseActivity {

    CommonUserInfo mCommonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rereg_lost_pattern);
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReregLostPatternActivity.this, CertifyPhonePersonalInfoActivity.class);
                mCommonUserInfo.setEntryPoint(EntryPoint.PATTERN_FORGET);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                finish();
            }
        });
        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
                    Intent intent = new Intent(ReregLostPatternActivity.this, PatternAuthActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                }
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            Intent intent = new Intent(this, PatternAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
        }
        finish();
    }
}
