package com.sbi.saidabank.activity.main2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.main2.common.banner.BannerLayout;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.ParseUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomScrollView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;
import com.sbi.saidabank.define.datatype.main.MainAdsItemInfo;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 처음 가입해서 입출금 계좌가 전혀 없는 사람인 경우 나타나는 화면.
 */
public class Main2MyAccountEmptyFragment extends BaseFragment{
    Context mContext;

    private ProgressBarLayout mProgressLayout;


    private BannerLayout mLayoutBanner;
    private CustomScrollView mScrollView;

    //버튼
    private LinearLayout mLayoutBtn;
    private LinearLayout mLayoutBtn1;
    private LinearLayout mLayoutBtn2;
    private TextView     mTvBtn1;
    private TextView     mTvBtn2;

    private TextView     mTvLabel1;
    private TextView     mTvLabel2;
    private TextView     mTvLabel3;

    private ImageView    mImgNewPerson;
    private RelativeLayout mLayoutDateCircle;
    private TextView       mTvRemainDate;

    private Main2RelayInfo m310RelayInfo; //입출금 계좌 이어하기 정보
    private Main2RelayInfo m320RelayInfo; //대출 계좌 이어하기 정보


    public static final Main2MyAccountEmptyFragment newInstance(){
        Main2MyAccountEmptyFragment f = new Main2MyAccountEmptyFragment();
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main2_home_myaccount_empty, container, false);

        mLayoutBtn = rootView.findViewById(R.id.layout_btn);

        mLayoutBtn1 = rootView.findViewById(R.id.layout_btn1);
        mLayoutBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DataUtil.isNotNull(m310RelayInfo)){
                    String msg = "계좌개설 이어하기를\n취소하시겠습니까?\n취소된 경우\n처음부터 다시 진행됩니다.";
                    DialogUtil.alert(getActivity(), "이어하기 취소", "닫기", msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestDelRelay(m310RelayInfo);
                        }
                    }, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    });
                }else{
                    Intent intent = new Intent(getContext(), WebMainActivity.class);
                    String url = WasServiceUrl.UNT0030100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    startActivity(intent);

                }
            }
        });
        mLayoutBtn2 = rootView.findViewById(R.id.layout_btn2);
        mLayoutBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goRelay_310(m310RelayInfo.getACCO_IDNO());
            }
        });

        mTvBtn1 = rootView.findViewById(R.id.tv_btn1);
        mTvBtn2 = rootView.findViewById(R.id.tv_btn2);

        mTvLabel1 = rootView.findViewById(R.id.tv_label_1);
        mTvLabel2 = rootView.findViewById(R.id.tv_label_2);
        mTvLabel3 = rootView.findViewById(R.id.tv_label_3);

        mImgNewPerson = rootView.findViewById(R.id.iv_new_person);
        mLayoutDateCircle = rootView.findViewById(R.id.layout_date_circle);
        mTvRemainDate = rootView.findViewById(R.id.tv_remain_date);

        //환영 인사부터 올립시다.
        String userName = LoginUserInfo.getInstance().getCUST_NM();
        String msg = "안녕하세요. " + userName + "님";
        Utils.setTextWithSpan(mTvLabel1,msg,userName, Typeface.BOLD,"#000000");

        mProgressLayout = rootView.findViewById(R.id.ll_progress);

        mLayoutBanner = rootView.findViewById(R.id.layout_banner);

        mScrollView = rootView.findViewById(R.id.scrollview);
        mScrollView.setOnScrollActionListener(new CustomScrollView.OnScrollActionListener() {
            @Override
            public void onScrollStart(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Logs.e("setOnScrollActionListener - onScrollStart");

                ((Main2Activity)mContext).changeHomeFragmentScrollState(true);
            }

            @Override
            public void onScrollStop(boolean isBottom) {
                //Logs.e("setOnScrollActionListener - onScrollStop");

                ((Main2Activity)mContext).changeHomeFragmentScrollState(false);
            }

            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Logs.e("onScrollChange - onScrollChange - scrollY : " + scrollY);
            }
        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        MLog.d();

        mLayoutBtn.setVisibility(View.GONE);
        mTvLabel2.setVisibility(View.GONE);
        mTvLabel3.setVisibility(View.GONE);


        if(mLayoutBanner != null)
            mLayoutBanner.onResumeLayout();

        //Fragment가 시작할대는 홈메뉴는 항상보여야 한다.
        ((Main2Activity)mContext).changeHomeFragmentScrollState(false);

        requestMainInfo();

    }

    @Override
    public void onPause() {
        super.onPause();

        MLog.d();
        if(mLayoutBanner != null)
            mLayoutBanner.onPauseLayout();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
                //AniUtils.changeViewSizeAnimation(mRelayTopMaginView,0);
            }
        }, 500);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mLayoutBanner != null)
            mLayoutBanner.onDestroyLayout();
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        //mProgressLayout.show();
        if(((BaseActivity)getActivity()) != null && !((BaseActivity)getActivity()).isFinishing())
            ((BaseActivity)getActivity()).showProgressDialog();
    }

    @Override
    public void dismissProgress() {
        //mProgressLayout.hide();
        if(((BaseActivity)getActivity()) != null && !((BaseActivity)getActivity()).isFinishing())
            ((BaseActivity)getActivity()).dismissProgressDialog();
    }


    /**
     * 메인정보조회
     */
    private void requestMainInfo() {

        if (getActivity() == null || getActivity().isFinishing()) return;
        // 혹시 로그아웃되어있으면...
        if (!LoginUserInfo.getInstance().isLogin()) return;

        showProgress();
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    ((Main2Activity)getActivity()).goLogoutPage();
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        if ("CMM0037".equals(errCode)) {
                            //로그인 중 분실신고기기 체크
                            if (Utils.isAppOnForeground(mContext)) {
                                LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
                                SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
                                mApplicationClass.allActivityFinish(true);
                                LoginUserInfo.clearInstance();
                                LoginUserInfo.getInstance().setLogin(false);
                                Intent intent = new Intent(mContext, LogoutActivity.class);
                                intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
                                mContext.startActivity(intent);
                            } else {
                                LoginUserInfo.getInstance().setLogin(false);
                                LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
                                LogoutTimeChecker.getInstance(mContext).setBackground(true);
                            }
                        } else {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                            ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    //팝업정보 조회
                    ArrayList<MainPopupInfo> listPopupInfo = new ArrayList<>();
                    JSONArray popup_array = object.optJSONArray("REC_POPUP");
                    if (popup_array != null) {
                        Logs.e("popup_array : " + popup_array.toString());
                        for (int index = 0; index < popup_array.length(); index++) {
                            JSONObject jsonObject = popup_array.getJSONObject(index);
                            if (jsonObject == null)
                                continue;

                            MainPopupInfo mainPopupInfo = new MainPopupInfo(jsonObject);
                            listPopupInfo.add(mainPopupInfo);
                        }
                        Logs.e("listPopupInfo.size() : " + listPopupInfo.size());
                        //팝업 오픈
                        if(listPopupInfo.size() > 0){
                            if(getActivity() != null && !getActivity().isFinishing()) {
                                ((Main2Activity) getActivity()).showPopupDialog(listPopupInfo);

                                final JSONObject jsonObject = object;
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        try {
                                            parsorRequestMainInfo(jsonObject);
                                        } catch (JSONException e) {
                                            Logs.printException(e);
                                        }
                                    }
                                }, 1000);
                                return;
                            }
                        }
                    }

                    parsorRequestMainInfo(object);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
                Logs.d("Main 정보 조회 종료");
            }
        });
    }

    private void parsorRequestMainInfo(JSONObject object) throws JSONException{
        //메인 레코드가 아닌 기본값 조회 - 계좌분리 리스트도 여기서 한다.
        Main2DataInfo.getInstance().setMain2DataInfoFromMyAccount(object);

        //계좌정보조회
        ArrayList<Main2AccountInfo> listAccountInfo = new ArrayList<>();
        JSONArray account_array = object.optJSONArray("REC");
        if (account_array != null){

            for (int index = 0; index < account_array.length(); index++) {
                JSONObject jsonObject = account_array.getJSONObject(index);
                if (jsonObject == null)
                    continue;

                try {
                    ParseUtil.parseJSONObject(jsonObject);
                    MLog.i("--------------------------------------------");
                } catch (Exception e) {
                    MLog.e(e);
                }

                Main2AccountInfo accountInfo = new Main2AccountInfo(jsonObject);
                String DSCT_CD = accountInfo.getDSCT_CD();
                if (TextUtils.isEmpty(DSCT_CD))
                    continue;

                listAccountInfo.add(accountInfo);
            }
            Main2DataInfo.getInstance().setMain2Main2AccountInfo(listAccountInfo);
            Logs.e("listAccountInfo.size() : " + listAccountInfo.size());
            if(listAccountInfo.size() > 0){
                //여기는 계좌가 없을때 들어와야 하는데... 계좌가 없다가 계좌를 생성한 후 돌아온 후 라면
                //Fragment를 변경해줘야 한다.
                if(getActivity() != null && !getActivity().isFinishing())
                    ((Main2Activity)getActivity()).viewPagerUpdate();

                return;
            }
        }


        //배너정보 조회
        ArrayList<MainAdsItemInfo> listBannerInfo = new ArrayList<>();
        JSONArray banner_array = object.optJSONArray("REC_BNR");
        if (banner_array != null) {

            for (int index = 0; index < banner_array.length(); index++) {
                JSONObject jsonObject = banner_array.getJSONObject(index);
                if (jsonObject == null)
                    continue;

                MainAdsItemInfo adsItemInfo = new MainAdsItemInfo(jsonObject);
                listBannerInfo.add(adsItemInfo);
            }
            Logs.e("listBannerInfo.size() : " + listBannerInfo.size());
            if(listBannerInfo.size() > 0){
                mLayoutBanner.setBannerInfo(listBannerInfo);
            }else{
                mLayoutBanner.setVisibility(View.GONE);
            }
        }

        //이어하기 정보조회
        ArrayList<Main2RelayInfo> listRelayInfo = new ArrayList<>();
        JSONArray relay_array = object.optJSONArray("REC_WAIT");
        if (relay_array != null){
            Logs.e("relay_array : " + relay_array.toString());
            for (int index = 0; index < relay_array.length(); index++) {
                JSONObject jsonObject = relay_array.getJSONObject(index);
                if (jsonObject == null)
                    continue;

                Main2RelayInfo accountInfo = new Main2RelayInfo(jsonObject);
                String DSCT_CD = accountInfo.getDSCT_CD();
                if (TextUtils.isEmpty(DSCT_CD))
                    continue;

                listRelayInfo.add(accountInfo);
            }
            Logs.e("listRelayInfo.size() : " + listRelayInfo.size());
            //이어하기 오픈
            if(listRelayInfo.size() > 0){
                for(int i=0;i<listRelayInfo.size();i++){
                    Main2RelayInfo info = listRelayInfo.get(i);
                    if(info.getDSCT_CD().equals("310")){//일반예금 이어하기
                        m310RelayInfo = info;
                        listRelayInfo.remove(i);
                        break;
                    }if(info.getDSCT_CD().equals("320")||
                            info.getDSCT_CD().equals("321")||
                            info.getDSCT_CD().equals("322")||
                            info.getDSCT_CD().equals("323")||
                            info.getDSCT_CD().equals("326")||
                            info.getDSCT_CD().equals("328")||
                            info.getDSCT_CD().equals("324")||
                            info.getDSCT_CD().equals("325")||
                            info.getDSCT_CD().equals("327")
                    ){ //대출이어하기
                        m320RelayInfo = info;
                        listRelayInfo.remove(i);
                        break;
                    }
                }
            }
        }
        //보통예금,대출 이어하기 외에 이어하기가 존재하면 위에서 나타나도록...없으면 사라지도록..
        if(getActivity() != null && !getActivity().isFinishing())
            ((Main2Activity) getActivity()).showRelayLayout(listRelayInfo);

        mLayoutBtn.setVisibility(View.VISIBLE);
        //사람/날짜 이미지 보여주기
        if(DataUtil.isNotNull(m310RelayInfo)||DataUtil.isNotNull(m320RelayInfo)) {

            mLayoutDateCircle.setVisibility(View.VISIBLE);
            mImgNewPerson.setVisibility(View.GONE);

            if(DataUtil.isNotNull(m310RelayInfo)){
                mTvRemainDate.setText("D-" + m310RelayInfo.getREMN_DCNT());
                mTvLabel3.setText("입출금통장 개설을 완료하세요!");

                mTvBtn1.setText("취소하기");
                mTvBtn2.setText("이어하기");
                mTvBtn1.setTextColor(Color.parseColor("#FFFFFF"));
                mTvBtn2.setTextColor(Color.parseColor("#FFFFFF"));
                int radius = (int) Utils.dpToPixel(mContext,16);
                mLayoutBtn1.setBackground(GraphicUtils.getRoundCornerDrawable("#8993aa",new int[]{radius,radius,radius,radius}));
                mLayoutBtn2.setBackground(GraphicUtils.getRoundCornerDrawable("#34446b",new int[]{radius,radius,radius,radius}));
                mLayoutBtn2.setVisibility(View.VISIBLE);

            }else{
                mTvRemainDate.setText("D-" + m320RelayInfo.getREMN_DCNT());
                mTvLabel2.setText("대출신청 진행 중입니다.");
                mTvLabel3.setText("입출금통장 계좌개설 후 이어하기 가능합니다.");

                mImgNewPerson.setVisibility(View.VISIBLE);
                mTvBtn1.setText("계좌개설하기");
                int radius = (int) Utils.dpToPixel(mContext,16);
                mLayoutBtn1.setBackground(GraphicUtils.getRoundCornerDrawable("#00ebff",new int[]{radius,radius,radius,radius}));
                mLayoutBtn2.setVisibility(View.GONE);
            }
        }else{
            mImgNewPerson.setVisibility(View.VISIBLE);
            mTvBtn1.setText("계좌개설하기");
            int radius = (int) Utils.dpToPixel(mContext,16);
            mLayoutBtn1.setBackground(GraphicUtils.getRoundCornerDrawable("#00ebff",new int[]{radius,radius,radius,radius}));
            mLayoutBtn2.setVisibility(View.GONE);
        }
        //라벨 텍스트 보여주기
        mTvLabel2.setVisibility(View.VISIBLE);
        mTvLabel3.setVisibility(View.VISIBLE);

    }

    /**
     * 보통예금이어하기
     */
    private void goRelay_310(final String PROP_NO) {

        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A04.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();

                if(((BaseActivity)mContext).onCheckHttpError(ret,false)) return;

                try {
                    final JSONObject object = new JSONObject(ret);

                    String SCRN_ID = object.optString("SCRN_ID");
                    String SBK_PROP_STEP_CD = object.optString("SBK_PROP_STEP_CD");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = SaidaUrl.getBaseWebUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = "";
                    if (TextUtils.isEmpty(SBK_PROP_STEP_CD) && !TextUtils.isEmpty(PROP_NO)) {
                        param = "PROP_NO=" + PROP_NO;
                    } else if (!TextUtils.isEmpty(SBK_PROP_STEP_CD) && !TextUtils.isEmpty(PROP_NO)) {
                        param = "SBK_PROP_STEP_CD=" + SBK_PROP_STEP_CD + "&PROP_NO=" + PROP_NO;
                    }
                    intent.putExtra(Const.INTENT_PARAM, param);
                    mContext.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이어하기 삭제
     */
    private void requestDelRelay(Main2RelayInfo relayInfo) {
        MLog.d();

        Map param = new HashMap();

        String DSCT_CD = relayInfo.getDSCT_CD();
        Logs.e("DSCT_CD : " + DSCT_CD);
        if (TextUtils.isEmpty(DSCT_CD) || !DSCT_CD.equals("310"))
            return;

        String url = WasServiceUrl.MAI0010400A01.getServiceUrl();
        String ACCO_IDNO = relayInfo.getACCO_IDNO();
        if (TextUtils.isEmpty(ACCO_IDNO))
            return;

        param.put("PROP_NO", ACCO_IDNO);


        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(url, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if(((BaseActivity)mContext).onCheckHttpError(ret,false))   return;

                m310RelayInfo=null;

                mImgNewPerson.setVisibility(View.VISIBLE);
                mLayoutDateCircle.setVisibility(View.GONE);

                mTvBtn1.setText("계좌개설하기");
                int radius = (int) Utils.dpToPixel(mContext,16);
                mLayoutBtn1.setBackground(GraphicUtils.getRoundCornerDrawable("#00ebff",new int[]{radius,radius,radius,radius}));
                mLayoutBtn2.setVisibility(View.GONE);

            }
        });
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);
    }

}
