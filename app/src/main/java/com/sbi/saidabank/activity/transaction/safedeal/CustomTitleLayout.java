package com.sbi.saidabank.activity.transaction.safedeal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;


public class CustomTitleLayout extends RelativeLayout {
    private static final String TAG = CustomTitleLayout.class.getSimpleName();

    private TextView mTvTitleKind;
    private RelativeLayout mLayoutBody;

    private LinearLayout  mLayoutFirstTitle;
    private LinearLayout  mLayoutSecondTitle;
    private LinearLayout  mLayoutClose;

    private RelativeLayout mDescLayout;
    private ImageView mIvIcon;
    private OnSafeDealActionListener mActionListener;

    private TextView mSecondTitle;
    private TextView mPhoneNum;

    private int mSafeDealKind;

    public CustomTitleLayout(Context context) {
        super(context);
        initUX(context);
    }

    public CustomTitleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        mActionListener = (OnSafeDealActionListener)context;

        View layout = View.inflate(context, R.layout.layout_safe_deal_title, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mLayoutBody = layout.findViewById(R.id.layout_body);
        mLayoutFirstTitle = layout.findViewById(R.id.layout_first_title);

        mLayoutSecondTitle = layout.findViewById(R.id.layout_second_title);
        mLayoutSecondTitle.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                //두번째 타이틀을 내리고.
                startAnimateTitleFadeInOut(mLayoutSecondTitle,false,250,0);
                //떠있는 슬라이드를 모두 내리고
                goToFirstStep();

                //타이틀의 좌표를 다시 설정하고
                changeTitlePosition(mLayoutBody.getHeight());

                //첫번째 타이틀로 변경한다.
                startAnimateTitleFadeInOut(mLayoutFirstTitle,true,250,250);
            }
        });


        //First Title Layout 의 Item들.
        mTvTitleKind = layout.findViewById(R.id.tv_kind);
        mDescLayout = layout.findViewById(R.id.layout_desc);
        mIvIcon = layout.findViewById(R.id.iv_icon);

        mLayoutClose = layout.findViewById(R.id.layout_close);
        mLayoutClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionListener.onActionClose();
            }
        });

        //Second Title Layout의 Item들.
        mSecondTitle = layout.findViewById(R.id.tv_second_title);
        mPhoneNum    = layout.findViewById(R.id.tv_phone_num);

        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Logs.e("CustomTitleLayout - onGlobalLayout - height : " + getHeight());

                        int titleHeight = 0;

                        //첫번재 슬라이드로 타이틀 높이를 고정하자.
//                        View view = getSlideView(SlideReceiverAccountNumLayout.class);
//
//
//                        if(view != null){
//                            titleHeight = (int)view.getY();
//                        }else{
//                            int activityHeight = GraphicUtils.getActivityHeight(getContext());
//                            int firstSlideHeight = (int)getContext().getResources().getDimension(R.dimen.safe_deal_receiver_accountnum_layout_height);
//                            titleHeight = activityHeight - firstSlideHeight;
//                        }

                        int activityHeight = GraphicUtils.getActivityHeight(getContext());
                        int firstSlideHeight = (int)getContext().getResources().getDimension(R.dimen.safe_deal_receiver_accountnum_layout_height);
                        titleHeight = activityHeight - firstSlideHeight;

                        //레이아웃의 높이를 재조정해둔다.
                        ViewGroup.LayoutParams params = mLayoutBody.getLayoutParams();
                        params.height = titleHeight;
                        mLayoutBody.setLayoutParams(params);

                        if(mLayoutFirstTitle.getHeight() > titleHeight){
                            mDescLayout.setVisibility(GONE);
                            mLayoutClose.setY(0f);
                        }

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    /**
     * 슬라이드 레이아웃이 변경될때 마다 타이틀의 위치를 변경해 준다.
     */
    public void changeTitlePosition(){
        changeTitlePosition(0);
    }
    public void changeTitlePosition(int minY){
        int slideMinY = minY;
        if(minY == 0)
            slideMinY = getSlideLayoutMinYPos();

        Logs.e(" slideMinY : " + slideMinY);

        if(mLayoutFirstTitle != null){

            int firstTitleHeight = mLayoutFirstTitle.getHeight();
            if(mDescLayout.getVisibility() != VISIBLE){
                firstTitleHeight = mTvTitleKind.getHeight();
            }
            Logs.e(" mLayoutFirstTitle.getHeight() : " +firstTitleHeight);
            //First title margin 은 27이다.
            int topMargin = (int)Utils.dpToPixel(getContext(),27f);
            if(slideMinY - firstTitleHeight < topMargin){
                int moveY = topMargin;
                Logs.e("0.moveY : " + moveY);
                mLayoutFirstTitle.animate()
                        .translationY(moveY)
                        .setDuration(0)
                        .start();
            }else{
                int moveY = slideMinY - firstTitleHeight;
                Logs.e("1.moveY : " + moveY);
                mLayoutFirstTitle.animate()
                        .translationY(moveY)
                        .setDuration(0)
                        .start();
            }
        }

        if(mLayoutSecondTitle != null){
            int secondTitleHeight = mLayoutSecondTitle.getHeight();
            int topMargin = (int)Utils.dpToPixel(getContext(),1f);
            if(slideMinY - secondTitleHeight < topMargin){
                mLayoutSecondTitle.setY(topMargin);
            }else{
                mLayoutSecondTitle.setY(slideMinY - secondTitleHeight);
            }
        }
    }

    //슬라이드뷰를 가져온다.
    private View getSlideView(Class slideClass){
        RelativeLayout rootLayout = (RelativeLayout) (this.getParent());

        for(int i=0;i<rootLayout.getChildCount();i++){
            View view = rootLayout.getChildAt(i);
            if(slideClass.isInstance(view) ){
                return view;
            }
        }

        return null;
    }

    /**
     * 열려있는 레이아웃중 최소 위치에 위치한 값을 갸져온다.
     * @return 슬라이드 뷰중 제일 작은 Y값
     */
    private int getSlideLayoutMinYPos(){
        int minY=100000;
        RelativeLayout rootLayout = (RelativeLayout) (this.getParent());

        for(int i=0;i<rootLayout.getChildCount();i++){
            View view = rootLayout.getChildAt(i);

            if(view instanceof SlideBaseLayout){
                int posY = (int)view.getY();
                Logs.e(view.getTag() + " , YPos : " + view.getY());
                if(view.getVisibility() == View.VISIBLE && posY > 0){
                    if(posY < minY){
                        minY = posY;
                    }
                }
            }
        }

        return minY;
    }

    /**
     * 두번째 타이틀을 클릭하여 다시 받는이 계좌로 이동하고자 할때 호출
     */

    private void goToFirstStep(){
        RelativeLayout rootLayout = (RelativeLayout) (this.getParent());

        for(int i=0;i<rootLayout.getChildCount();i++){
            View view = rootLayout.getChildAt(i);

            if(view instanceof SlideBaseLayout){
                if(view.getVisibility() == View.VISIBLE){
                    ((SlideBaseLayout)view).slidingDown(250,0);
                }
                //혹시 데이타 초기화 요구하면 아래 주석 풀어준다.
                //((SlideBaseLayout)view).clearData();
            }
        }

        for(int i=0;i<rootLayout.getChildCount();i++){
            View view = rootLayout.getChildAt(i);

            if(view instanceof SlideReceiverAccountNumLayout){
                ((SlideReceiverAccountNumLayout)view).slidingUp(250,250);
            }
        }

    }

    public void setSafeDealKind(int kind){
        mSafeDealKind = kind;
        switch (kind){
            case Const.SAFEDEAL_TYPE_MOENY:
                mTvTitleKind.setText("금전거래");
                mIvIcon.setImageResource(R.drawable.ico_coin);
                break;
            case Const.SAFEDEAL_TYPE_PROPERTY:
                mTvTitleKind.setText("부동산거래");
                mIvIcon.setImageResource(R.drawable.ico_land);
                break;
            case Const.SAFEDEAL_TYPE_GOODS:
                mTvTitleKind.setText("물품거래");
                mIvIcon.setImageResource(R.drawable.ico_product);
                break;
        }
    }

    public int getSafeDealKind(){
        return mSafeDealKind;
    }

    public void setReceiverInfo(String name,String phoneNum){
        mSecondTitle.setText(name+"님과 " + mTvTitleKind.getText());

        String telNum = Utils.changeFormatTelephoneNumber(phoneNum,".");

        mPhoneNum.setText(telNum);

        startAnimateTitleFadeInOut(mLayoutFirstTitle,false,250,0);
        startAnimateTitleFadeInOut(mLayoutSecondTitle,true,250,250);
    }

    private void startAnimateTitleFadeInOut(final View view, final boolean isShow, final int duration, int delayTime){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isShow) {
                    view.setVisibility(VISIBLE);
                }

                Animation anim;
                if(isShow){
                    anim = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_in_transfer_view);
                }else{
                    anim = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_out_transfer_view);
                }
                anim.setDuration(duration);
                view.clearAnimation();
                view.startAnimation(anim);
                view.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        if(!isShow) {
                            view.setVisibility(INVISIBLE);
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }, delayTime);

    }


}
