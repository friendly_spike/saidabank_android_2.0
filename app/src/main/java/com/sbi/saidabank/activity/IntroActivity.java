package com.sbi.saidabank.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.ahnlab.v3mobileplus.interfaces.V3MobilePlusCtl;
import com.netfunnel.api.ContinueData;
import com.netfunnel.api.Netfunnel;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.common.ReportLostGuideActivity;
import com.sbi.saidabank.activity.login.BlockIPActivity;
import com.sbi.saidabank.activity.login.SplashActivity;
import com.sbi.saidabank.activity.login.TutorialActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.activity.test.NewApkDownloadActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.NetfunnelDialog;
import com.sbi.saidabank.common.dialog.PermissionDialog;
import com.sbi.saidabank.common.dialog.SelectDevDialog;
import com.sbi.saidabank.common.dialog.SlidingDevModeCheckDialog;
import com.sbi.saidabank.common.download.DownloadRSCUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;
import com.sbi.saidabank.push.FLKPushAgentSender;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.solution.v3.V3Manager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;

/**
 * IntroActivity : 인트로 화면
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class IntroActivity extends BaseActivity implements V3Manager.V3CompleteListener {

    private String[] mPermissionList = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    private static final int DEVICE_STATE_NORMAL = 0;
    private static final int DEVICE_STATE_NEWUSER = 1;
    private static final int DEVICE_STATE_CHANGE = 2;
    private static final int REQUEST_DEBUGINGMODE = 1000;

    private V3Manager mV3Manager = null;
    private int mDeviceState = DEVICE_STATE_NORMAL;
    private boolean mNeedV3Install = false;
    private boolean iamDeveloper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);

        // 로그인 정보 Clear
        LoginUserInfo.clearInstance();

        //갤럭시에서는 서비스가 정상 시작및 종료된다. 하지만 샤오미폰에서는 종료가 되지 않는것을 확인했다.
        //또한 일부폰에서는 Foreground로 시작하지 않을경우 죽는 현상도 발견되었다.
//        try{
//            // 안드로이드 8 이상에서는 foreground 서비스로 시작한다.
//            startService(new Intent(IntroActivity.this, TaskService.class));
//        }catch (IllegalStateException e){
//            Logs.printException(e);
//        }

        setContentView(R.layout.activity_intro);
        if (!isTaskRoot()
                && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
                && getIntent().getAction() != null
                && getIntent().getAction().equals(Intent.ACTION_MAIN)) {
            finish();
            return;
        }
        // 퍼미션 가이드 노출여부 확인
        if (!Prefer.getPermissionGuideShowStatus(IntroActivity.this)) {
            if (isFinishing())
                return;
            PermissionDialog dialog = new PermissionDialog(IntroActivity.this);
            dialog.mPListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Prefer.setPermissionGuideShowStatus(IntroActivity.this, true);
                    checkPermisstionState();
                }
            };
            dialog.mNListener = mOnOkClickListener;
            dialog.show();
        } else {
            checkPermisstionState();
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        if (mNeedV3Install && DataUtil.isNotNull(mV3Manager) && !mV3Manager.isRunV3MobilePlus()) {
            finish();
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_DEBUGINGMODE) {
            goNextActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        MLog.d();
        if (requestCode == Const.REQUEST_PERMISSION_READ_PHONE_STATE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startIntro();
            } else {
                //Firebase index error수정.
                final String[] reqPermissions = permissions;

                DialogUtil.alert(
                        IntroActivity.this,
                        "권한설정",
                        "종료",
                        getString(R.string.msg_permission_phone_state_allow),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean showFlag = false;
                                if(reqPermissions.length > 0)
                                    showFlag = ActivityCompat.shouldShowRequestPermissionRationale(IntroActivity.this, reqPermissions[0]);

                                if (showFlag)
                                    PermissionUtils.checkPermission(IntroActivity.this, mPermissionList, Const.REQUEST_PERMISSION_READ_PHONE_STATE);
                                else {
                                    PermissionUtils.goAppSettingsActivity(IntroActivity.this);
                                    finish();
                                }
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Utils.finishAffinity(IntroActivity.this);
                            }
                        }
                );
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onCompleteV3() {
        MLog.d();
    }

// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    /**
     * 권한설명 다이얼로그와는 관계없이 기본 권한이 설정되어 있는지 체크한다.
     * */
    private void checkPermisstionState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!PermissionUtils.checkPermission(IntroActivity.this, mPermissionList, Const.REQUEST_PERMISSION_READ_PHONE_STATE)) {
                return;
            }
        }
        startIntro();
    }

    /**
     * DEBUG 여부에 따라 Server 선택 다이얼로그를 띄운다.
     */
    private void startIntro() {
        MLog.d();
        if (isFinishing()) return;
        if (BuildConfig.DEBUG) {
            MLog.line();
            MLog.i("Application HashKey >> " + getApplicationHash());
            MLog.line();
            showDebugServerSelectDialog();
        } else {
            //startPlayIntro();
            checkNetStatus();
        }
    }

    /**
     * 디버그모드일 경우 서버 선택 다이얼로그를 띄운다.
     * */
    private void showDebugServerSelectDialog() {
        SelectDevDialog selectDevDialog = new SelectDevDialog(IntroActivity.this);
        selectDevDialog.setOnCofirmListener(new SelectDevDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress(int selectIndex) {
                if (selectIndex == Const.DEBUGING_SERVER_NONE) {
                    finish();
                } else {
                    //SaidaUrl.serverIndex = selectIndex;
                    //startPlayIntro();
                    checkNetStatus();
                }
            }

            @Override
            public void onIamDeveloper() {
                if (!iamDeveloper) {
                    iamDeveloper = true;
                }
            }
        });
        selectDevDialog.show();
    }

    /**
     * Intro 화면 Logo 애니메이션 호출
     */
//    public void startPlayIntro() {
//        MLog.d();
//        RequestOptions reqOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.RESOURCE).skipMemoryCache(true);
//        Glide.with(IntroActivity.this)
//                .load(R.drawable.img_intro)
//                .apply(reqOptions)
//                .listener(new RequestListener<Drawable>() {
//                    @Override
//                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
//                        MLog.e(e);
//                        return false;
//                    }
//
//                    @Override
//                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
//                        final GifDrawable gifDrawable = (GifDrawable) resource;
//                        // 로고 gif 1회만 재생
//                        gifDrawable.setLoopCount(1);
//                        //이광호 과장님요청 - 사이다 시작 애니메이션이 완료된 후에 다음작업 진행하도록.
//                        new Handler().postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        checkNetStatus();
//                                    }
//                                });
//                            }
//                        }, 2500);
//                        return false;
//                    }
//                })
//                .into((ImageView) findViewById(R.id.iv_logo))
//                .clearOnDetach();
//    }

    /**
     * 네트워크 상태를 체크한다.
     */
    private void checkNetStatus() {
        MLog.d();
        if (isFinishing())
            return;
        showProgress(true);
        HttpUtils.NetState state = HttpUtils.checkNetworkState(IntroActivity.this);
        if (state == HttpUtils.NetState.NET_STATE_OFFLINE || state == HttpUtils.NetState.NET_STATE_NOT_SUPPORT) {
            showProgress(false);
            DialogUtil.alert(
                    IntroActivity.this,
                    "종료",
                    "재시도",
                    getResources().getString(R.string.msg_error_network_status),
                    mOnOkClickListener,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            checkNetStatus();
                        }
                    });
        } else {
            // USIM 장착여부 확인
            if (!BuildConfig.DEBUG && !Utils.isExistSimCard(IntroActivity.this)) {
                showProgress(false);
                DialogUtil.alert(
                        IntroActivity.this,
                        getResources().getString(R.string.common_exit),
                        getResources().getString(R.string.msg_error_no_usim),
                        mOnOkClickListener
                );
                return;
            }
            checkV3MobilePlus();
        }
    }

    /**
     * V3 매니져를 실행한다.
     */
    private void checkV3MobilePlus() {
        MLog.d();
        mV3Manager = V3Manager.getInstance(getApplicationContext());
        mV3Manager.setV3CompleteListener(this);
        // V3가 실행중일 경우
        if (mV3Manager.isRunV3MobilePlus()) {
            checkNetfunnel();
            return;
        }
        int ret = mV3Manager.startV3MobilePlus(LicenseKey.V3_LICENSE_KEY);
        switch (ret) {
            case V3MobilePlusCtl.ERR_FAILURE:
            case V3MobilePlusCtl.ERR_NEW_VERSION:
            case V3MobilePlusCtl.ERR_INVALIED_LICENSEKEY:
            case V3MobilePlusCtl.ERR_ASSET_APK:
            case V3MobilePlusCtl.ERR_ALREADY_NEW_INSTALLED:
            case V3MobilePlusCtl.ERR_V3M_NOT_FOUND:
            case V3MobilePlusCtl.ERR_V3M_NOT_CHECK:
            case V3MobilePlusCtl.ERR_V3M_NOT_SIGNING:
                DialogUtil.alert(
                        IntroActivity.this,
                        R.string.msg_error_run_v3,
                        mOnOkClickListener
                );
                break;
            case V3MobilePlusCtl.ERR_NOT_INSTALLED:
                // 개발 서버에서는 V3설치 않해도 넘어가도록 수정.
                if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV) {
                    checkNetfunnel();
                } else {
                    DialogUtil.alert(
                            IntroActivity.this,
                            getString(R.string.common_market),
                            getString(R.string.common_close),
                            getString(R.string.msg_not_install_v3),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mNeedV3Install = true;
                                    V3Manager.getInstance(getApplicationContext()).goProductMarket();
                                    finish();
                                }
                            },
                            mOnOkClickListener
                    );
                }

                break;
            case V3MobilePlusCtl.ERR_NOT_CONNECTABLE:
                if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV) {
                    checkNetfunnel();
                } else {
                    DialogUtil.alert(
                            IntroActivity.this,
                            R.string.msg_not_setting_v3,
                            mOnOkClickListener
                    );
                }
                break;
            case V3MobilePlusCtl.ERR_SUCCESS:
            default:
                checkNetfunnel();
                break;
        }
    }

    /**
     * Netfunnel 시작
     */
    private void checkNetfunnel() {
        MLog.d();

        if (isFinishing())
            return;

        if(BuildConfig.DEBUG && SaidaUrl.getBaseWebUrl().equals(SaidaUrl.DEV_SERVER)) {
            checkTestVersion();
            return;
        }

        Netfunnel.BEGIN(
                "service_1",
                "intro_app",
                NetfunnelDialog.Show(IntroActivity.this),
                new Netfunnel.Listener() {
                    @Override
                    public void netfunnelMessage(Netfunnel netfunnel, Netfunnel.EvnetCode code) {
                        try {
                            //넷퍼넬 관련 코드 수정
                            MLog.i("code >> " + netfunnel.getResponse().getCode());
                            if (code.isContinue()) {
                                ContinueData continue_data = netfunnel.getContinueData();
                                if (code == Netfunnel.EvnetCode.ContinueInterval) {
                                } else {  // Netfunnel.EvnetCode.Continue
                                    String continue_msg = "    예상대기시간:" + continue_data.getCurrentWaitTimeSecond() + "초\n"
                                            + "    진   행   율:" + continue_data.getCurrentWaitPercent() + "%\n"
                                            + "    대기자수(앞):" + continue_data.getCurrentWaitCount() + "명\n"
                                            + "    대기자수(뒤):" + continue_data.getCurrentNextCount() + "명\n"
                                            + " 초당처리량(TPS):" + netfunnel.getResponse().getTPS() + "\n"
                                            + "   확인주기(TTL):" + netfunnel.getResponse().getTTL() + "초\n"
                                            + "   notice acount:" + continue_data.getAcountNotice() + "\n"
                                            + "   update acount:" + continue_data.getUpdateAcount() + "\n"
                                            + "UI     표시대기시간:" + netfunnel.getProperty().getUiWaitTimeLimit() + "초\n"
                                            + "UI 표시대기자수(앞):" + netfunnel.getProperty().getUiWaitCountLimit() + "명\n"
                                            + "UI 표시대기자수(뒤):" + netfunnel.getProperty().getUiNextCountLimit() + "명";




                                    Logs.d("netfunnel", "-------------------------\n" + continue_msg);
                                }
                                return;
                            }

                            NetfunnelDialog.Close();
                            if (code.isSuccess()) {
                                checkTestVersion();
//                              if(code == Netfunnel.EvnetCode.Success) {
//                                  // 주의) 여기에만 AliveNotice()을 사용할수 있는 곳
//                              }else if(code == Netfunnel.EvnetCode.NotUsed){
//                              }else if(code == Netfunnel.EvnetCode.Bypass){
//                              }else if(code == Netfunnel.EvnetCode.ErrorBypass){
//                              }else if(code == Netfunnel.EvnetCode.ExpressNumber){
//                              }
                            } else if (code.isBlocking()) {
                                dismissProgressDialog();
                                DialogUtil.alert(IntroActivity.this, R.string.netfunnel_blocking_error_msg, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });
//                              if(code == Netfunnel.EvnetCode.Block) {
//                                  // 서비스 차단
//                              }else if(code == Netfunnel.EvnetCode.IpBlock){
//                                  // 사용자 사용패턴에 의한 차단
//                              }
                            } else if (code.isStop()) {
                                // TODO: 자체처리영역
                                dismissProgressDialog();
                                DialogUtil.alert(IntroActivity.this, R.string.netfunnel_many_people_error_msg, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });
                            } else { // }else if(code.isError()){
                                //20201230 - 개발서버에서는 넷퍼넬 에러나도 그냥 통과
                                if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV) {
                                    checkTestVersion();
                                }else{
                                    dismissProgressDialog();
                                    DialogUtil.alert(IntroActivity.this, R.string.netfunnel_error_msg, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            finish();
                                        }
                                    });
                                }
                            }
                            Netfunnel.END();
                        } catch (Exception e) {
                            MLog.e(e);
                            checkTestVersion();
                        }
                    }
                }
        );
    }

    /**
     * 개발/테스트 서버의 버전을 체크한다.
     */
    @SuppressWarnings("unchecked")
    private void checkTestVersion() {
        MLog.d();
        if (!BuildConfig.DEBUG) {
            sendCheckDeviceInfo();
            return;
        }

        if (SaidaUrl.serverIndex != Const.DEBUGING_SERVER_OPERATION) {
            String url = SaidaUrl.getBaseMWebUrl() + "/app2/aos/aos_version.txt";

            HttpUtils.sendHttpTask(url, new HashMap(), new HttpSenderTask.HttpRequestListener() {
                @Override
                public void endHttpRequest(String ret) {

                    if (DataUtil.isNull(ret) || ret.contains("페이지를 표시할 수 없습니다")) {
                        sendCheckDeviceInfo();
                        return;
                    }
                    int serverVersion = Integer.parseInt(ret);
                    int appVersion = Utils.getVersionCode(IntroActivity.this);
                    MLog.i("Server Version : " + serverVersion + " | App Version : " + appVersion);
                    if (serverVersion > appVersion) {
                        showProgress(false);

                        String msg = "현재버전 : " + appVersion + "\n신규버전 : " + serverVersion + "\n\n신규버전이 생성되었습니다.\n다운로드 하시겠습니까?";
                        DialogUtil.alert(IntroActivity.this, "예", "아니요", msg, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://devm.saidabank.co.kr/test0000200.act"));
                                        Intent intent = new Intent(IntroActivity.this, NewApkDownloadActivity.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        // 네트워크 연결 재시도
                                        showProgress(true);
                                        sendCheckDeviceInfo();
                                    }
                                }
                        );
                        return;
                    } else {
                        sendCheckDeviceInfo();
                    }
                }
            });
        } else {
            sendCheckDeviceInfo();
        }
    }

    /**
     * 디바이스 정보를 체크한다.
     */
    @SuppressWarnings("s")
    private void sendCheckDeviceInfo() {
        MLog.d();
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (DataUtil.isNotNull(FDSManager.getInstance())) {
                            String encIMEI = FDSManager.getInstance().getEncIMEI(IntroActivity.this);
                            String encUUID = FDSManager.getInstance().getEncUUID(IntroActivity.this);
                            String encUUID121 = FDSManager.getInstance().getEncIMEI121(IntroActivity.this);
                            checkDeviceInfo(encIMEI, encUUID, encUUID121);
                        }
                    }
                });
            }
        }).start();
    }

    /**
     * 설정의 개발자 모드 상태를 체크하고 팝업을 띄어준다. (true : 팝업 띄우고 해제할 것인지 선택 / false : 다음 intro 순서 진행)
     */
    private void checkDeveloperMode() {
        if (isFinishing())
            return;

        if (Utils.isUsbDebuggingEnable(IntroActivity.this) && !Prefer.getFlagDontShowDevModeCheckDialog(IntroActivity.this)) {

            SlidingDevModeCheckDialog dialog = new SlidingDevModeCheckDialog(IntroActivity.this);
            dialog.mPListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DEVELOPMENT_SETTINGS);
                        startActivityForResult(intent, REQUEST_DEBUGINGMODE);
                    }catch (ActivityNotFoundException e){
                        Intent intent = new Intent(Settings.ACTION_SETTINGS);
                        startActivityForResult(intent, REQUEST_DEBUGINGMODE);
                    }
                }
            };
            dialog.mNListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goNextActivity();
                }
            };
            dialog.setCancelable(false);
            dialog.show();

        } else {
            goNextActivity();
        }
    }


    /**
     * 다음 화면으로 랜딩처리한다.
     */
    private void goNextActivity() {
        MLog.d();

        // Push Agent Sender
        FLKPushAgentSender.sendToStart(IntroActivity.this, false);
        String regId = Prefer.getPushRegID(this);
        Logs.e("### IntroActivity - goNextActivity() - Prefer.getPushRegID : " + regId);

        // 로고 애니메이션 종료
//        Drawable drawable = ((ImageView) findViewById(R.id.iv_logo)).getDrawable();
//        if (drawable instanceof Animatable) {
//            ((Animatable) drawable).stop();
//        }



        // 튜토리얼 노출여부 확인
        if (!Prefer.getTutorialStatus(IntroActivity.this)) {
            Intent intent = new Intent(IntroActivity.this, TutorialActivity.class);
            intent.putExtra("device_state", mDeviceState);
            startActivity(intent);
            finish();
        }
        // 디바이스 인증여부 확인
        else if (mDeviceState != DEVICE_STATE_NORMAL) {
            if (BuildConfig.DEBUG) {
                if (SaidaUrl.serverIndex != Prefer.getJoinDebugServerIndex(this)) {
                    Prefer.setDebugBackupAuthStatus(this);
                }
            }
            Prefer.setPatternRegStatus(IntroActivity.this, false);
            Prefer.setPincodeRegStatus(IntroActivity.this, false);
            Prefer.setFidoRegStatus(IntroActivity.this, false);
            Prefer.setFlagFingerPrintChange(IntroActivity.this, false);

            EntryPoint curEntryPoint = (mDeviceState == DEVICE_STATE_NEWUSER) ? EntryPoint.SERVICE_JOIN : EntryPoint.DEVICE_CHANGE;
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.DEVICE_CHANGE, curEntryPoint);
            Intent intent = new Intent(IntroActivity.this, SplashActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivity(intent);
            finish();
        } else {
            boolean status = Prefer.getFidoRegStatus(IntroActivity.this);
            Logs.e("getFidoRegStatus : " + status);


            CommonUserInfo commonUserInfo = new CommonUserInfo();
            commonUserInfo.setEntryStart(status ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
            commonUserInfo.setEntryPoint(status ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
            Intent intent = new Intent(IntroActivity.this, PatternAuthActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL)) {
                Logs.e("getIntent().getStringExtra(Const.INTENT_MAINWEB_URL) : " + getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
                intent.putExtra(Const.INTENT_MAINWEB_URL, getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
            }
            startActivity(intent);
            finish();
        }
    }

    /**
     * 프로그래스바를 띄어준다.
     *
     * @param isShow : 노출여부
     */
    private void showProgress(final boolean isShow) {
        //Firebase에러 - 인트로에서 올라오는 다이얼로그 에러 관련 방어코드 추가
        if(isFinishing()) return;

        IntroActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isShow) {
                    showProgressDialog();
                } else {
                    dismissProgressDialog();
                }
            }
        });
    }

    /**
     * 해시키 정보를 얻는다.
     *
     * @return 해시키 정보
     */
    private String getApplicationHash() {
        try {
            PackageManager manager = IntroActivity.this.getPackageManager();
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(
                    manager.getPackageInfo(
                            IntroActivity.this.getPackageName(), PackageManager.GET_SIGNATURES
                    ).signatures[0].toByteArray()
            );
            Certificate certificate = CertificateFactory.getInstance("X509").generateCertificate(byteArrayInputStream);
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            return Base64.encodeToString(messageDigest.digest(certificate.getEncoded()), 3);
        } catch (PackageManager.NameNotFoundException | CertificateException | NoSuchAlgorithmException e) {
            MLog.e(e);
            return "";
        }
    }
// ==============================================================================================================
// LISTENER
// ==============================================================================================================
    /**
     * 확인버튼을 눌렀을때 클릭 이벤트 리스너 (공통)
     */
    private View.OnClickListener mOnOkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            finish();
        }
    };
// ==============================================================================================================
// API
// ==============================================================================================================
    /**
     * 기기정보를 확인한다.
     *
     * @param item1 : 디바이스정보 01
     * @param item2 : 디바이스정보 02
     * @param item3 : 디바이스정보 03
     */
    @SuppressWarnings("unchecked")
    private void checkDeviceInfo(String item1, String item2, String item3) {
        MLog.d();

        //세션및 쿠기 Clear
        HttpUtils.removeCookie();

        // 로그인 정보 Clear
        LoginUserInfo.clearInstance();

        HashMap param = new HashMap();
        param.put("DEVICE_OS", "android");
        param.put("DEVICE_INFO1", item1);
        param.put("DEVICE_INFO2", item2);
        param.put("DEVICE_INFO3", item3);
        param.put("TRMN_APSF_VRSN", Utils.getVersionName(IntroActivity.this));
        param.put("TRMN_OPSY_VRSN", Build.VERSION.RELEASE);
        param.put("CMCM_NM", Utils.getOperator(IntroActivity.this));
        param.put("TRMN_MODL_NM", Build.MODEL);

        // 디바이스 정보 확인(cmm0010300A01)
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Netfunnel.END();
                // 응답 데이터가 없을 경우 종료 처리
                if (DataUtil.isNull(ret)) {
                    // 응답데이터가 없을 경우 기존대로 종료처리
                    showProgress(false);
                    String msg = getResources().getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg, mOnOkClickListener);
                } else {
                    try {
                        // 응닶 데이터가 HTML 문자열일 경우 웹뷰로 호출
                        if (Utils.isHtmlFormat(ret)) {
                            showProgress(false);

                            //응답데이터가 HTML 문자열일 경우 해당 도메인을 loadURL
                            Intent intent = new Intent(IntroActivity.this, WebMainActivity.class);
                            intent.putExtra(Const.INTENT_MAINWEB_URL, SaidaUrl.getBaseWebUrl());
                            startActivity(intent);
                            finish();
                            return;
                        }
                        final JSONObject object = new JSONObject(ret);
                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            showProgress(false);
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            MLog.e("error msg : " + msg + ", ret : " + ret);
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    finish();
                                }
                            };
                            showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                            return;
                        }
                        // 스톤패스 초기화
                        StonePassManager stonePassManager = StonePassManager.getInstance(getApplication());
                        StonePassManager.StonePassInitListener stonePassInitListener = new StonePassManager.StonePassInitListener() {
                            @Override
                            public void stonePassInitResult() {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        showProgress(false);

                                        //================================================================================================
                                        // 긴급공지 팝업
                                        try {
                                            JSONArray recPopup = object.optJSONArray("REC_POPUP");
                                            // 노출할 팝업정보가 있을 경우
                                            if (DataUtil.isNotNull(recPopup)) {
                                                JSONObject item = recPopup.getJSONObject(0);
                                                // 이미지 팝업을 노출
                                                if (item.has("PUP_CNTN_DVCD")
                                                        && DataUtil.isNotNull(item.optString("PUP_CNTN_DVCD"))
                                                        && item.optString("PUP_CNTN_DVCD").equals("I")) {
                                                    showNoticeDialog(new MainPopupInfo(item));
                                                    return;
                                                }
                                                // 텍스트 팝업을 노출
                                                String popupMsg = item.optString("PUP_CNTN");
                                                showErrorMessage(popupMsg, mOnOkClickListener);
                                                return;
                                            }
                                        } catch (JSONException e) {
                                            MLog.e(e);
                                        }
                                        //================================================================================================
                                        // 회원번호
                                        LoginUserInfo.getInstance().setMBR_NO(object.optString("MBR_NO", Const.EMPTY));
                                        //================================================================================================
                                        // 고객번호
                                        LoginUserInfo.getInstance().setCUST_NO(object.optString("CUST_NO", Const.EMPTY));
                                        //================================================================================================
                                        // Device ID
                                        LoginUserInfo.getInstance().setSMPH_DEVICE_ID(object.optString("SMPH_DEVICE_ID", Const.EMPTY));
                                        //================================================================================================
                                        // App 버전 체크
                                        String appVersionName = Utils.getVersionName(IntroActivity.this).replaceAll("[^0-9]", Const.EMPTY);
                                        int appVersion = Integer.parseInt(appVersionName);
                                        String serverVersionName = object.optString("APSF_VRSN", "").replaceAll("[^0-9]", Const.EMPTY);
                                        int serverVersion = Integer.parseInt(serverVersionName);
                                        // App Store : 서버버전이 앱버전보다 클 때만 업데이트 수행
                                        if (!BuildConfig.DEBUG && (serverVersion > appVersion)) {
                                            DialogUtil.alert(
                                                    IntroActivity.this,
                                                    "업데이트",
                                                    getResources().getString(R.string.msg_app_update),
                                                    "업데이트",
                                                    new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            Intent intent = new Intent(Intent.ACTION_VIEW);
                                                            intent.setData(Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName()));
                                                            intent.setPackage("com.android.vending");
                                                            startActivity(intent);
                                                            finish();
                                                        }
                                                    }
                                            );
                                            return;
                                        }


                                        //은행/증권사 리소스 버전을 체크.
                                        String localRscVersion = Prefer.getResourceIconVersion(IntroActivity.this);
                                        String serverRscVersion = object.optString("FIN_INST_IMG_VRSN_SRNO", localRscVersion);
                                        Logs.e("RSC Image Version - local : " + localRscVersion + " , server : " + serverRscVersion);

                                        if(!localRscVersion.equals(serverRscVersion)){
                                            String rscUrl = object.optString("FIN_INST_IMG_DWLD_URL", Const.EMPTY);
                                            if(!TextUtils.isEmpty(rscUrl)){
                                                rscUrl = SaidaUrl.getBaseMWebUrl() + rscUrl;
                                                DownloadRSCUtil.DownloadResource(IntroActivity.this,rscUrl,serverRscVersion);
                                            }
                                        }else{
                                            //혹시 은행/증권사 코드가 지워졌거나 받지 않은경우를 위해 받는다.
                                            String bankArrayStr = Prefer.getBankList(IntroActivity.this);
                                            String stockArrayStr = Prefer.getStockList(IntroActivity.this);

                                            if (TextUtils.isEmpty(bankArrayStr) || TextUtils.isEmpty(stockArrayStr)){
                                                SaidaCodeUtil.requestBankList(IntroActivity.this);
                                                SaidaCodeUtil.requestStockList(IntroActivity.this);
                                            }
                                        }

                                        /*
                                        cmm0010300A01
                                        AB_IP_YN        :  해외IP여부(현재 접속 IP 기준)

                                        CUS39001 사이다뱅킹기기상태조회 응답
                                        AB_IP_BLCK_YN   :  해외IP차단서비스신청여부
                                        EQMT_LOS_DCL_YN : 기기분실신고여부
                                        EQMT_NEW_YN     : 기기신규여부
                                        EQMT_CHNG_YN    : 기기변경여부
                                        */

                                        //================================================================================================
                                        // 해외 IP 차단 여부
                                        String blackIpYn = object.optString("AB_IP_BLCK_YN", Const.EMPTY);
                                        if (DataUtil.isNull(blackIpYn)) {
                                            showErrorMessage("기기정보 확인(해외IP차단여부) 오류입니다.", mOnOkClickListener);
                                            return;
                                        }
                                        String strIPBlockValue = object.optString("AB_IP_YN", Const.EMPTY);
                                        if (DataUtil.isNull(strIPBlockValue)) {
                                            showErrorMessage("기기정보 확인(해외IP차단) 오류입니다.", mOnOkClickListener);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(blackIpYn) && Const.BRIDGE_RESULT_YES.equalsIgnoreCase(strIPBlockValue)) {
                                            Intent intent = new Intent(IntroActivity.this, BlockIPActivity.class);
                                            startActivity(intent);
                                            finish();
                                            return;
                                        }
                                        //================================================================================================
                                        // 분실신고 기기여부
                                        String losDclYn = object.optString("EQMT_LOS_DCL_YN", Const.EMPTY);
                                        if (DataUtil.isNull(losDclYn)) {
                                            showErrorMessage("기기정보 확인(분실기기확인) 오류입니다.", mOnOkClickListener);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(losDclYn)) {
                                            Intent intent = new Intent(IntroActivity.this, ReportLostGuideActivity.class);
                                            startActivity(intent);
                                            finish();
                                            return;
                                        }
                                        //================================================================================================
                                        // 신규가입 여부
                                        String eqmtNewYn = object.optString("EQMT_NEW_YN", Const.EMPTY);
                                        if (DataUtil.isNull(eqmtNewYn)) {
                                            showErrorMessage("기기정보 확인(신규가입여부) 오류입니다.", mOnOkClickListener);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(eqmtNewYn)) {
                                            // 회원가입 진행
                                            mDeviceState = DEVICE_STATE_NEWUSER;
                                            checkDeveloperMode();
                                            return;
                                        }
                                        //================================================================================================
                                        // 타기기 등록 여부
                                        String eqmtChngYn = object.optString("EQMT_CHNG_YN", Const.EMPTY);
                                        if (DataUtil.isNull(eqmtChngYn)) {
                                            showErrorMessage("기기정보 확인(기기변경확인) 오류입니다.", mOnOkClickListener);
                                            return;
                                        }
                                        if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(eqmtChngYn)) {
                                            mDeviceState = DEVICE_STATE_CHANGE;
                                            // 기기변경이라도 앱 삭제 후 재설치된 상태면 기기변견 알림 팝업 띄우지 않음
                                            if (!Prefer.getPincodeRegStatus(IntroActivity.this)) {
                                                checkDeveloperMode();
                                                return;
                                            }
                                            DialogUtil.alert(
                                                    IntroActivity.this,
                                                    "기기변경",
                                                    "앱종료",
                                                    getResources().getString(R.string.msg_error_change_device),
                                                    new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            // 기기변경 진행
                                                            checkDeveloperMode();
                                                        }
                                                    },
                                                    mOnOkClickListener
                                            );
                                            return;
                                        }

                                        if (BuildConfig.DEBUG && !iamDeveloper) {
                                            //============================================================================
                                            //디버깅 모드에서 서버 선택할때 개발자가 아닌사람은 서버 변경하면 기기변경하도록 한다.
                                            //개발자는 귀찮으니까.. 기변 하지 않고 개발/테스트 모두 가입되어 있으면 바로 로그인 가능하도록 한다.
                                            //기변하지 않고 바로 사용할경우 각 서버별 Prefer,MOTP사용시 값들이 달라 에러가 발생할수 있다.
                                            //============================================================================
                                            int joinServer = Prefer.getJoinDebugServerIndex(IntroActivity.this);
                                            if (joinServer != SaidaUrl.serverIndex) {
                                                mDeviceState = DEVICE_STATE_CHANGE;
                                            }
                                        }
                                        checkDeveloperMode();
                                    }
                                });
                            }
                        };
                        stonePassManager.stonePassInit(stonePassInitListener);
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                }
            }
        });
    }
}
