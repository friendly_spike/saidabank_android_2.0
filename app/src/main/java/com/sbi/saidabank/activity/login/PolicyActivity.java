package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebTermsActivity;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;

/**
 * Saidabank_android
 * Class: PolicyActivity
 * Created by 950546
 * Date: 2018-10-17
 * Time: 오후 2:41
 * Description: 이용약관 화면
 */
public class PolicyActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    private CheckBox mChboxNecessary;
    private CheckBox mChboxOption;
    private CheckBox mChboxApppush;
    private CheckBox mChboxSMS;
    private CheckBox mChboxSecurityApppush;
    private CheckBox mChboxSecuritySMS;
    private CheckBox mChboxSecurityNotAccept;

    private Button mBtnMarkettingAgreement;
    private Button mBtnSaidaPolicy;
    private Button mBtnEasyTrabsferPolicy;
    private Button mBtnPrivateInfo;
    private Button mBtnBasicPolicy;
    private Button mBtnNotiPolicy;
    private Button mBtnOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_policy);
        initView();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * UI 초기화
     */
    private void initView() {

        LinearLayout necessaryLayout = findViewById(R.id.ll_necessarypolicy);
        necessaryLayout.setOnClickListener(this);

        LinearLayout optionLayout = findViewById(R.id.ll_optionpolicy);
        optionLayout.setOnClickListener(this);

        LinearLayout apppushLayout = findViewById(R.id.ll_apppush);
        apppushLayout.setOnClickListener(this);

        LinearLayout smsLayout = findViewById(R.id.ll_sms);
        smsLayout.setOnClickListener(this);

        LinearLayout appSecurityPushLayout = findViewById(R.id.ll_security_apppush);
        appSecurityPushLayout.setOnClickListener(this);

        LinearLayout securitySMSLayout = findViewById(R.id.ll_security_sms);
        securitySMSLayout.setOnClickListener(this);

        LinearLayout securityNoAcceptLayout = findViewById(R.id.ll_security_noaccept);
        securityNoAcceptLayout.setOnClickListener(this);

        mChboxNecessary = findViewById(R.id.chbox_necessary);
        mChboxNecessary.setOnCheckedChangeListener(this);

        mChboxOption = findViewById(R.id.chbox_option);
        mChboxOption.setOnCheckedChangeListener(this);

        mBtnMarkettingAgreement = findViewById(R.id.btn_markettingagreement);
        mBtnMarkettingAgreement.setOnClickListener(this);

        mBtnSaidaPolicy = findViewById(R.id.btn_saidapolicy);
        mBtnEasyTrabsferPolicy = findViewById(R.id.btn_easytransferpolicy);
        mBtnPrivateInfo = findViewById(R.id.btn_privateinfo);
        mBtnBasicPolicy = findViewById(R.id.btn_basicpolicy);
        mBtnNotiPolicy = findViewById(R.id.btn_notipolicy);

        mChboxApppush = findViewById(R.id.chbox_apppush);
        mChboxApppush.setOnCheckedChangeListener(this);

        mChboxSMS = findViewById(R.id.chbox_SMS);
        mChboxSMS.setOnCheckedChangeListener(this);

        mChboxSecurityApppush = findViewById(R.id.chbox_security_apppush);
        mChboxSecurityApppush.setOnClickListener(this);

        mChboxSecuritySMS = findViewById(R.id.chbox_security_SMS);
        mChboxSecuritySMS.setOnClickListener(this);

        mChboxSecurityNotAccept = findViewById(R.id.chbox_security_noaccept);
        mChboxSecurityNotAccept.setOnClickListener(this);

        mBtnOK = findViewById(R.id.btn_confirm);
        mBtnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
                commonUserInfo.setOptionalPolicyPush(mChboxApppush.isChecked());
                commonUserInfo.setOptionalPolicySMS(mChboxSMS.isChecked());                
                commonUserInfo.setSecurityPolicyPush(mChboxSecurityApppush.isChecked());
                commonUserInfo.setSecurityPolicySMS(mChboxSecuritySMS.isChecked());
                Intent intent = new Intent(PolicyActivity.this, PersonalInfoActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
            }
        });

        findViewById(R.id.layout_basicpolicy).setOnClickListener(this);
        findViewById(R.id.layout_notipolicy).setOnClickListener(this);
        findViewById(R.id.layout_necessary).setOnClickListener(this);
        findViewById(R.id.layout_easytransferpolicy).setOnClickListener(this);
        findViewById(R.id.layout_privateinfo).setOnClickListener(this);
        findViewById(R.id.layout_marketting).setOnClickListener(this);
//        findViewById(R.id.ll_chbox_markettingagreement).setOnClickListener(this);

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.chbox_necessary:
                if (isChecked) {
                    if (isEnabledOKBtn()) {
                        mBtnOK.setEnabled(true);
                    }
                    mBtnSaidaPolicy.setEnabled(true);
                    mBtnEasyTrabsferPolicy.setEnabled(true);
                    mBtnPrivateInfo.setEnabled(true);
                    mBtnBasicPolicy.setEnabled(true);
                    mBtnNotiPolicy.setEnabled(true);
                    Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "약관");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=A001");
                    startActivity(intent);
                } else {
                    mBtnSaidaPolicy.setEnabled(false);
                    mBtnEasyTrabsferPolicy.setEnabled(false);
                    mBtnPrivateInfo.setEnabled(false);
                    mBtnBasicPolicy.setEnabled(false);
                    mBtnNotiPolicy.setEnabled(false);
                    mBtnOK.setEnabled(false);
                }
                break;

            case R.id.chbox_option:
                if (isChecked) {
                    if (mBtnMarkettingAgreement.isEnabled()) {
                        break;
                    }
                    mBtnMarkettingAgreement.setEnabled(true);
                    mChboxApppush.setChecked(true);
                    mChboxSMS.setChecked(true);
                    Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "동의서");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1001");
                    startActivity(intent);
                } else {
                    mBtnMarkettingAgreement.setEnabled(false);
                    mChboxApppush.setChecked(false);
                    mChboxSMS.setChecked(false);
                }
                break;

            case R.id.chbox_apppush:
                if (isChecked) {
                    if (!mChboxSMS.isChecked()) {
                        mBtnMarkettingAgreement.setEnabled(true);
                        mChboxOption.setChecked(true);
                    }
                } else {
                    if (!mChboxSMS.isChecked()) {
                        mChboxOption.setChecked(false);
                    }
                }
                break;

            case R.id.chbox_SMS:
                if (isChecked) {
                    if (!mChboxApppush.isChecked()) {
                        mBtnMarkettingAgreement.setEnabled(true);
                        mChboxOption.setChecked(true);
                    }
                } else {
                    if (!mChboxApppush.isChecked()) {
                        mChboxOption.setChecked(false);
                    }
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_necessarypolicy:
                mChboxNecessary.setChecked(!mChboxNecessary.isChecked());
                break;

            case R.id.ll_optionpolicy:
                mChboxOption.setChecked(!mChboxOption.isChecked());
                break;

            case R.id.ll_apppush:
                mChboxApppush.setChecked(!mChboxApppush.isChecked());
                break;

            case R.id.ll_sms:
                mChboxSMS.setChecked(!mChboxSMS.isChecked());
                break;

            case R.id.layout_basicpolicy:
                Intent intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1008");
                startActivity(intent);
                break;

            case R.id.layout_necessary:
                intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1009");
                startActivity(intent);
                break;
            case R.id.layout_easytransferpolicy:
                intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1030");
                startActivity(intent);
                break;
            case R.id.layout_notipolicy:
                intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1024");
                startActivity(intent);
                break;

            case R.id.layout_privateinfo:
                intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1000");
                startActivity(intent);
                break;

            case R.id.btn_markettingagreement:
                mBtnMarkettingAgreement.setEnabled(!mBtnMarkettingAgreement.isEnabled());
                if (mBtnMarkettingAgreement.isEnabled()) {
                    mChboxApppush.setChecked(true);
                    mChboxSMS.setChecked(true);
                    intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "동의서");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1001");
                    startActivity(intent);
                } else {
                    mChboxApppush.setChecked(false);
                    mChboxSMS.setChecked(false);
                }
                break;

            case R.id.layout_marketting:
                if (!mBtnMarkettingAgreement.isEnabled()) {
                    mBtnMarkettingAgreement.setEnabled(true);
                    mChboxApppush.setChecked(true);
                    mChboxSMS.setChecked(true);
                }
                intent = new Intent(PolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "동의서");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1001");
                startActivity(intent);
                break;

            case R.id.ll_security_apppush:
                if (!mChboxSecurityApppush.isChecked()) {
                    mChboxSecurityApppush.setChecked(true);
                    mChboxSecuritySMS.setChecked(false);
                    mChboxSecurityNotAccept.setChecked(false);
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                }
                break;

            case R.id.chbox_security_apppush:
                mChboxSecurityApppush.setChecked(true);
                mChboxSecuritySMS.setChecked(false);
                mChboxSecurityNotAccept.setChecked(false);
                if (isEnabledOKBtn())
                    mBtnOK.setEnabled(true);
                break;

            case R.id.ll_security_sms:
                if (!mChboxSecuritySMS.isChecked()) {
                    mChboxSecuritySMS.setChecked(true);
                    mChboxSecurityApppush.setChecked(false);
                    mChboxSecurityNotAccept.setChecked(false);
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                }
                break;

            case R.id.chbox_security_SMS:
                mChboxSecuritySMS.setChecked(true);
                mChboxSecurityApppush.setChecked(false);
                mChboxSecurityNotAccept.setChecked(false);
                if (isEnabledOKBtn())
                    mBtnOK.setEnabled(true);
                break;

            case R.id.ll_security_noaccept:
                if (!mChboxSecurityNotAccept.isChecked()) {
                    mChboxSecurityNotAccept.setChecked(true);
                    mChboxSecurityApppush.setChecked(false);
                    mChboxSecuritySMS.setChecked(false);
                    if (isEnabledOKBtn())
                        mBtnOK.setEnabled(true);
                }
                break;

            case R.id.chbox_security_noaccept:
                mChboxSecurityNotAccept.setChecked(true);
                mChboxSecurityApppush.setChecked(false);
                mChboxSecuritySMS.setChecked(false);
                if (isEnabledOKBtn())
                    mBtnOK.setEnabled(true);
                break;

            default:
                break;
        }
    }

    private boolean isEnabledOKBtn() {
        return mChboxNecessary.isChecked() && (mChboxSecurityApppush.isChecked() || mChboxSecuritySMS.isChecked() || mChboxSecurityNotAccept.isChecked());
    }
}
