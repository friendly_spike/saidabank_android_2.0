package com.sbi.saidabank.activity.common;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternRegActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeRegActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;

import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;


import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: AnotherBankAccountConfirmActivity
 * Created by 950485 on 2018. 11. 14..
 * <p>
 * Description:타행이체 확인을 위한 화면
 */

public class AnotherBankAccountConfirmActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged, TextWatcher {
    private static final int SCROLL_EDITTEXT        = 0;
    private static final int SCROLL_CHECK_KEYBOARD  = 1;

    private RelativeLayout mLayoutScreen;
    private TextView       mTextDesc;
    private LinearLayout   mLayoutName;
    private EditText       mEditName;
    private TextView       mTextNameMsg;
    private Button         mBtnConfirm;
    private ScrollView     mSvScview;

    private CommonUserInfo mCommonUserInfo;
    private String         mAthnSqno;
    private String         mBzwrDvcd;
    private String         mPropNo;

    private InputMethodManager imm = null;

    private WeakHandler mScrollHandler;
    private int mScrollOffset;
    private boolean mIsShownKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_another_bank_account_confirm);

        getExtra();
        initUX();
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
            DialogUtil.alert(this,
                    "나가기",
                    "취소",
                    getResources().getString(R.string.msg_cancel_pin_rereg),

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                            overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
                        }
                    },

                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    });
            return;
        } else {
            goBackVerifyAnotherAccount();
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        mAthnSqno = getIntent().getStringExtra(Const.INTENT_VERIFY_ACCOUNT_SQNO);
        mBzwrDvcd = getIntent().getStringExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD);
        mPropNo = getIntent().getStringExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mLayoutScreen = (RelativeLayout) findViewById(R.id.ll_root);
        mLayoutScreen.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                imm.hideSoftInputFromWindow(mEditName.getWindowToken(), 0);
            }
        });
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mScrollHandler = new WeakHandler(this);
        mScrollOffset = (int)Utils.dpToPixel(this, (float)30f);

        mTextDesc= (TextView) findViewById(R.id.textview_comfirm_account_desc);
        mLayoutName = (LinearLayout) findViewById(R.id.layout_deposit_name);
        mEditName = (EditText) findViewById(R.id.edittext_name_comfirm_account);
        mTextNameMsg = (TextView) findViewById(R.id.textview_msg_deposit_name);
        mBtnConfirm = (Button) findViewById(R.id.btn_ok_confirm_account);

        String desc = mCommonUserInfo.getBankName() + " " + mCommonUserInfo.getBankAccount() ;
        mTextDesc.setText(desc);
        mEditName.addTextChangedListener(this);
        mEditName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if  (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
            }
        });
        mEditName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
            }
        });
        mBtnConfirm.setEnabled(false);

        if (SaidaUrl.serverIndex != Const.DEBUGING_SERVER_DEV){
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(5);
            mEditName.setFilters(FilterArray);
        }


        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_confirm_account);
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL)
            btnCancel.setText(getResources().getString(R.string.common_close));
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //20200805 - back key와 같은 동작하도록 일원화한다.
                onBackPressed();
                /*
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
                    DialogUtil.alert(AnotherBankAccountConfirmActivity.this,
                            "나가기",
                            "취소",
                            getResources().getString(R.string.msg_cancel_pin_rereg),

                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                    overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
                                }
                            },

                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                    return;
                }else {
                    goBackVerifyAnotherAccount();
                }
                */
            }
        });

        LinearLayout btnAnotherAccount =  (LinearLayout) findViewById(R.id.layout_input_another_account);
        btnAnotherAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBackVerifyAnotherAccount();
            }
        });

        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditName.isFocused()) {
                    mEditName.clearFocus();
                    Utils.hideKeyboard(AnotherBankAccountConfirmActivity.this, mEditName);
                }

                String name = mEditName.getText().toString();
                if (TextUtils.isEmpty(name))
                    return;

                name = name.trim();
                if (!checkVaildUserName(name)) {
                    mLayoutName.setBackgroundResource(R.drawable.background_error_box);
                    return;
                }

                requestConfirmAccount();
            }
        });

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(this);

        //개발서버는 무조건 [가나다라]여서 넣어준다.
        if(SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV){
            mEditName.setText("가나다라");
        }
    }

    /**
     * 실명인증 검증 요청
     */
    private void requestConfirmAccount() {
        String memberNum;
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            memberNum = LoginUserInfo.getInstance().getMBR_NO();
        } else {
            memberNum = mCommonUserInfo.getMBRnumber();
        }
        if (TextUtils.isEmpty(memberNum)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mEditName.requestFocus();
                }
            };
            showErrorMessage(getString(R.string.msg_no_member_input), okClick);
            mBtnConfirm.setEnabled(false);
            return;
        }

        String name = mEditName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mEditName.requestFocus();
                }
            };
            showErrorMessage(getString(R.string.msg_no_transfer_name_input), okClick);
            mBtnConfirm.setEnabled(false);
            return;
        }

        Map param = new HashMap();
        param.put("MBR_NO", memberNum);
        param.put("ATHN_SQNO", mAthnSqno);
        param.put("ATHN_LTRS_VAL", name);
        param.put("NN_MEET_BZWR_DVCD", mBzwrDvcd);
        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010500A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("LCCD : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                mEditName.requestFocus();
                            }
                        };
                        showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        Logs.e(ret);
                        return;
                    }

                    String yesNo = object.optString("ATHN_YN");
                    if (TextUtils.isEmpty(yesNo)) {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mEditName.requestFocus();
                            }
                        };
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), okClick);
                        return;
                    }

                    if (Const.BRIDGE_RESULT_YES.equalsIgnoreCase(yesNo)) {
                        completeConfirmAccount();
                    } else {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mEditName.requestFocus();
                            }
                        };
                        showErrorMessage(getString(R.string.msg_fail_verify_another_account), okClick);
                    }

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 정상 완료
     */
    private void completeConfirmAccount() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
            Intent intent = new Intent(this, PatternRegActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            setResult(RESULT_OK);
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
            Intent intent = new Intent(this, PincodeRegActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL) {
            Intent intent = new Intent(Const.ACTION_SAFE_DEAL_NOCONTACT_COMPLETE);
            sendBroadcast(intent);
            finish();
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
            return;
        }
        finish();
        overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
    }

    /**
     * 타행 계좌 입력 화면으로 이동
     * 타행계좌 부정사용으로 행계좌입력 이동후 데이터 초기화
     */
    private void goBackVerifyAnotherAccount() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            setResult(Const.VERIFY_ANOTHER_ACCOUNT);
        } else {
            Intent intent = new Intent(AnotherBankAccountConfirmActivity.this, AnotherBankAccountVerifyActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_DVCD, mBzwrDvcd);
            intent.putExtra(Const.INTENT_VERIFY_ACCOUNT_PROPNO, mPropNo);
            startActivity(intent);

        }
        overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
        finish();
    }

    /**
     * 이름 유효성 체크
     *
     * @param str      체크 이름값
     * @return
     */
    private boolean checkVaildUserName(String str) {
        int derrcode = Utils.isKorean(str);

        if (derrcode == Const.STATE_VALID_STR_NORMAL) {
            mTextNameMsg.setVisibility(View.GONE);
            mLayoutName.setBackgroundResource(R.drawable.background_a5af_box);
        } else {
            mTextNameMsg.setVisibility(View.VISIBLE);
            mLayoutName.setBackgroundResource(R.drawable.background_error_box);

            return false;
        }
        return true;
    }

    @Override
    public void onKeyboardShown() {
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 300);
        mBtnConfirm.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        mIsShownKeyboard = false;
        mBtnConfirm.setVisibility(View.VISIBLE);
        String name = mEditName.getText().toString();
        if (SaidaUrl.serverIndex != Const.DEBUGING_SERVER_DEV){
            if (!TextUtils.isEmpty(name) && name.length() > 4) {
                name = name.substring(0, 4);
                mEditName.removeTextChangedListener(this);
                mEditName.setText(name);
            }
        }
        mEditName.setSelection(name.length(  ));
        mEditName.addTextChangedListener(this);
        mBtnConfirm.setEnabled(true);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String name = s.toString();
        if (!TextUtils.isEmpty(name) && name.contains(" ")) {
            name = name.trim();
            mEditName.removeTextChangedListener(this);
            mEditName.setText(name);
            mEditName.setSelection(name.length());
            mEditName.addTextChangedListener(this);
        }

        if (name.length() == 4) {
            mBtnConfirm.setEnabled(true);
        } else {
            mBtnConfirm.setEnabled(false);
        }
    }

    private class WeakHandler extends Handler {
        private WeakReference<AnotherBankAccountConfirmActivity> mWeakActivity;

        WeakHandler(AnotherBankAccountConfirmActivity weakactivity) {
            mWeakActivity = new WeakReference<AnotherBankAccountConfirmActivity>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            AnotherBankAccountConfirmActivity weakactivity = mWeakActivity.get();
            if (weakactivity != null) {
                switch (msg.what) {
                    case SCROLL_EDITTEXT: {
                        if (!mIsShownKeyboard)
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollOffset);
                        break;
                    }
                    case SCROLL_CHECK_KEYBOARD: {
                        mIsShownKeyboard = true;
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}