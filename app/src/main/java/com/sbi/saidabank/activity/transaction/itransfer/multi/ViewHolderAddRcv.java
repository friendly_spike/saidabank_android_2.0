package com.sbi.saidabank.activity.transaction.itransfer.multi;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferMultiActionListener;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.SlidingAccountInputDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectRcverAccountDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import java.util.ArrayList;

public class ViewHolderAddRcv extends RecyclerView.ViewHolder{


    private Context mContext;
    private TextView mTvAdd;
    private OnITransferMultiActionListener mListener;

    public static ViewHolderAddRcv newInstance(ViewGroup parent,OnITransferMultiActionListener listener) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_itransfer_multi_add_transfer, parent, false);
        return new ViewHolderAddRcv(parent.getContext(),itemView,listener);
    }

    public ViewHolderAddRcv(Context context, @NonNull View itemView,OnITransferMultiActionListener listener) {
        super(itemView);
        mContext = context;
        mListener = listener;
        mTvAdd = itemView.findViewById(R.id.tv_add_transfer);
    }

    public void onBind() {
        mTvAdd.setPaintFlags(mTvAdd.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSelectRcvAccountDialog();
            }
        });
    }


    //받는분 선택 다이얼로그를 보여준다.
    public void showSelectRcvAccountDialog() {

        //아이템을 선택했을때 처리방법
        SlidingExpandSelectRcverAccountDialog.OnSelectItemListener selectItemListener = new SlidingExpandSelectRcverAccountDialog.OnSelectItemListener() {
            @Override
            public void onSelectAccountItem(String bankCd, String detailBankCd, String accountNo, String recvNm) {
                //그룹 등록 가능인원 체크
                if (ITransferDataMgr.getInstance().getRemitteInfoArraySize() >= 5) {
                    DialogUtil.alert(mContext, "최대 5건까지\n다건 이체 등록이 가능합니다.");
                    return;
                }

                String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
                String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
                //if(WTCH_BANK_CD.equalsIgnoreCase(bankCd) && WTCH_ACNO.equalsIgnoreCase(accountNo)){
                if(WTCH_ACNO.equalsIgnoreCase(accountNo)){
                    DialogUtil.alert(mContext,"출금계좌와 입금계좌가 동일합니다.");
                    return;
                }

                //수치인조회 *****
                checkRemitteeAccount(bankCd, detailBankCd, accountNo,recvNm);
            }

            @Override
            public void onSelectPhoneItem(String name, String phoneNo) {

                DialogUtil.alert(mContext, mContext.getString(R.string.itransfer_msg_able_add_transfer), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSelectRcvAccountDialog();
                    }
                });

            }

            @Override
            public void onSelectGroupItem(String groupId,String favorite, String groupCntText) {
                if (!TransferUtils.isSaidaAccount()) {
                    DialogUtil.alert(mContext, mContext.getString(R.string.itransfer_msg_group_only_able_saida));
                    return;
                }

                //지연이체, 입금지정계좌서비스 체크
                if (!TransferUtils.checkAbleServiceCondition((BaseActivity) mContext, "다건이체")) {
                    return;
                }

                //그룹 등록 가능인원 체크
                if (!TextUtils.isEmpty(groupCntText)) {
                    int goupCnt = Integer.parseInt(groupCntText);
                    if (goupCnt + ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 5) {
                        DialogUtil.alert(mContext, "최대 5건까지\n다건 이체 등록이 가능합니다.");
                        return;
                    }
                }

                TransferRequestUtils.requestRemitteeAccountByMulti((BaseActivity) mContext,
                        groupId,
                        favorite,
                        false,
                        new HttpSenderTask.HttpRequestListener2() {
                            @Override
                            public void endHttpRequest(boolean result, String ret) {
                                if (result) {
                                    //다건은 수취조회가 끝나면 리스트를 새로 생성해 줘야 한다.
                                    mListener.makeMultiTransInfoList();
                                }
                            }
                        });

            }

            @Override
            public void onClickDirectInput() {
                //그룹 등록 가능인원 체크
                if (ITransferDataMgr.getInstance().getRemitteInfoArraySize() >= 5) {
                    DialogUtil.alert(mContext, "최대 5건까지\n다건 이체 등록이 가능합니다.");
                    return;
                }

                showBankStockAccountDialog();
            }

            @Override
            public void onShowErrorAlertMsg() {
                DialogUtil.alert(mContext, mContext.getString(R.string.itransfer_msg_able_add_transfer), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showSelectRcvAccountDialog();
                    }
                });



                //아래조건은 다건에서는 체크하지 않아도 되는 조건들이다.
//                //출금계좌가 오픈뱅킹 계좌이면 보이지 않도록 한다.
//                if (!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")) {
//                    DialogUtil.alert(mContext, mContext.getString(R.string.itransfer_msg_phone_only_able_saida), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            showSelectRcvAccountDialog();
//                        }
//                    });
//                    return;
//                }
//
//                //지연이체, 입금지정계좌서비스 체크
//                if (!TransferUtils.checkAbleServiceCondition((BaseActivity) mContext, "휴대폰이체")) {
//                    return;
//                }

            }

            @Override
            public void onAskPhonePermission() {
                //다건이체에서 여기들어 올 일은 없을듯하다.
            }

            @Override
            public void onDismissDialog() {

            }
        };

        //받는분 선택 다이얼로그 출력
        SlidingExpandSelectRcverAccountDialog dialog = new SlidingExpandSelectRcverAccountDialog((Activity) mContext,selectItemListener);
        dialog.show();
    }

    /**
     * 은행 직접 선택 다이얼로그
     */
    private void showBankStockAccountDialog(){
        MLog.d();

        String bankListStr = Prefer.getBankList(mContext);
        String stockListStr = Prefer.getStockList(mContext);

        ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
        ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);

        final SlidingExpandBankStockDialog.OnSelectBankStockListener bankStockListener = new SlidingExpandBankStockDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bank, String code) {
                showAccountInputDialog(code,bank,"");
            };
        };


        SlidingExpandBankStockDialog bankStockDialog = new SlidingExpandBankStockDialog((Activity) mContext, bankList, stockList, bankStockListener);
        bankStockDialog.show();
    }

    /**
     * 계좌번호 직접입력 다이얼로그
     *
     * @param bankCode
     * @param bankName
     */
    private void showAccountInputDialog(String bankCode, String bankName,String accountNo){
        MLog.d();

        String newBankName = bankName;

        //메인 클립보드 이체에서 받은사람 은행코드만 물고들어오기에 여기서 은행 명을 찾아낸다.
        if(TextUtils.isEmpty(newBankName)){
            String bankListStr = Prefer.getBankList(mContext);
            String stockListStr = Prefer.getStockList(mContext);

            ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
            ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);

            ArrayList<RequestCodeInfo> totalList = new ArrayList<RequestCodeInfo>();
            if (bankList != null) {
                totalList.addAll(bankList);
            }

            if (stockList != null) {
                totalList.addAll(stockList);
            }

            for(int i=0;i<totalList.size();i++){
                RequestCodeInfo info = totalList.get(i);
                if(info.getSCCD().equalsIgnoreCase(bankCode)){
                    newBankName = info.getCD_NM();
                    break;
                }
            }
        }

        final SlidingAccountInputDialog.OnFinishAccountInputListener accountInputListener = new SlidingAccountInputDialog.OnFinishAccountInputListener() {
            @Override
            public void onClickBack() {
                showBankStockAccountDialog();
            }

            @Override
            public void onEditFinishListener(String bankCode, String bankName, String accountNum) {
                String msg = "bankCdoe : " + bankCode + " , bankName : " + bankName + " , accountNum " + accountNum;
                if(BuildConfig.DEBUG)
                    Logs.showToast((Activity) mContext,msg);

                //수치인조회 *****
                checkRemitteeAccount(bankCode,"",accountNum,"");
            }
        };

        SlidingAccountInputDialog accountInputDialog = new SlidingAccountInputDialog((Activity) mContext, bankCode, newBankName,false,accountNo,accountInputListener);
        accountInputDialog.show();

    }


    //수취인을 확인한다.
    private void checkRemitteeAccount(String bankCode,String detailBankCd, String accountNo,String recvNm){

        TransferRequestUtils.requestRemitteeAccountBySingle((Activity) mContext,
                bankCode,
                detailBankCd,
                accountNo,
                recvNm,
                true,
                new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result,String ret) {
                        if(result){
                            //다건은 수취조회가 끝나면 리스트를 새로 생성해 줘야 한다.
                            mListener.makeMultiTransInfoList();

                        }
                    }
                });
    }

}
