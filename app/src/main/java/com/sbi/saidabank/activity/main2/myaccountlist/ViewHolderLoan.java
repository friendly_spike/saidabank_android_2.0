package com.sbi.saidabank.activity.main2.myaccountlist;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 대출계좌를 표시하는 뷰
 *
 */
public class ViewHolderLoan extends BaseViewHolder<Main2AccountInfo>{
    private View mItemView;
    private LinearLayout mLayoutBody;   //전체몸통
    private ImageView mIvShadow;
    private LinearLayout mLayoutItemMain; //아이템 표시 레이아웃
    private TextView mTvAccName;        //계좌명
    private TextView mTvAccType;        //연체종류
    private LinearLayout mLayoutAccNum;  //계좌번호 레이아웃
    private TextView mTvAccNum;         //계좌번호
    private TextView mTvAmount;         //계좌금액
    private TextView mTvPaymentDay;     //월납입일

    //대출 여신거래 상태 레이아웃
    //private RelativeLayout mLayoutBtnAndLoanRelay;
    private LinearLayout mLayoutLoanRelay;
    private TextView       mTvLoanRelayMsg;
    private TextView       mTvLoanJudgeMsg;

    //상환 버튼 레이아웃
    private LinearLayout mLayoutBtn;   //버튼 전체 레이아웃
    private LinearLayout mLayoutBtn1;  //상환버튼 전체 레이아웃
    private TextView     mTvBtn1;      //상환버튼
    private LinearLayout mLayoutBtn2;  //관리버튼 전체 레이아웃
    private TextView     mTvBtn2;      //관리버튼

    public static ViewHolderLoan newInstance(ViewGroup parent,int dispType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_main2_home_myaccount_list_item_loan, parent, false);
        return new ViewHolderLoan(parent.getContext(),itemView,dispType);
    }

    public ViewHolderLoan(Context context, @NonNull View itemView,int dispType) {
        super(context,itemView,dispType);

        mItemView = itemView;

        int radius = (int)Utils.dpToPixel(mContext,20);

        mLayoutBody     = itemView.findViewById(R.id.layout_body);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));

        mIvShadow     = itemView.findViewById(R.id.iv_shadow);

        mLayoutItemMain = itemView.findViewById(R.id.layout_item_main);
        mTvAccName      = itemView.findViewById(R.id.tv_account_name);
        mTvAccType      = itemView.findViewById(R.id.tv_account_type);
        mLayoutAccNum   = itemView.findViewById(R.id.layout_account_num);
        mTvAccNum       = itemView.findViewById(R.id.tv_account_num);
        mTvAmount       = itemView.findViewById(R.id.tv_amount);
        mTvPaymentDay   = itemView.findViewById(R.id.tv_payment_day);

        //마이너스통장 여신 상태메세지 레이아웃
        //mLayoutBtnAndLoanRelay  = itemView.findViewById(R.id.layout_loan_relay_judge_msg);
        //mLayoutBtnAndLoanRelay.setVisibility(View.GONE);
        mLayoutLoanRelay        = itemView.findViewById(R.id.layout_loan_relay);
        mLayoutLoanRelay.setVisibility(View.GONE);
        mTvLoanRelayMsg         = itemView.findViewById(R.id.tv_loan_relay_msg);
        mTvLoanRelayMsg.setTextColor(Color.parseColor("#00b5b8"));

        mTvLoanJudgeMsg         = itemView.findViewById(R.id.tv_loan_judge_msg);
        mTvLoanJudgeMsg.setTextColor(Color.parseColor("#00b5b8"));
        mTvLoanJudgeMsg.setVisibility(View.GONE);


        //상환버튼 레이아웃
        mLayoutBtn  = itemView.findViewById(R.id.layout_btn);
        mLayoutBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#00b5b8",new int[]{0,0,radius,radius}));

        mLayoutBtn1 = itemView.findViewById(R.id.layout_btn_1);
        mTvBtn1  = itemView.findViewById(R.id.tv_btn_1);
        mTvBtn1.setText("상환");

        mLayoutBtn2 = itemView.findViewById(R.id.layout_btn_2);
        mTvBtn2  = itemView.findViewById(R.id.tv_btn_2);
        mTvBtn2.setText("관리");

        mItemView.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        //레이아웃의 높이를 재조정해둔다.
                        ViewGroup.LayoutParams params = mIvShadow.getLayoutParams();
                        params.width = mItemView.getWidth();
                        params.height = mItemView.getHeight();
                        mIvShadow.setLayoutParams(params);

                        mItemView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    @Override
    public void onBindView(Main2AccountInfo mainAccountInfo) {
        mLayoutItemMain.setClickable(true);
        mLayoutItemMain.setTag(mainAccountInfo);
        mLayoutItemMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutItemMain.setClickable(false);
                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                String ACNO = mainAccountInfo.getACNO();
                String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
                if (TextUtils.isEmpty(ACNO)  || TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                    return;

                Intent intent = new Intent(mContext, WebMainActivity.class);
                String url = WasServiceUrl.LON0050200.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                String SUBJ_CD = ACCO_IDNO.substring(0, 2);
                String param =  "ACNO=" + ACNO + "&ACCO_IDNO=" + ACCO_IDNO + "&SUBJ_CD=" + SUBJ_CD;
                intent.putExtra(Const.INTENT_PARAM, param);
                mContext.startActivity(intent);
            }
        });


        mTvAccName.setText(mainAccountInfo.getTRNF_DEPR_NM());

        String accountNum = mainAccountInfo.getACNO();
        if (!TextUtils.isEmpty(accountNum)) {
            String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
            mTvAccNum.setText(account);
        }

        String OVRD_DVCD = mainAccountInfo.getOVRD_DVCD();
        if(TextUtils.isEmpty(OVRD_DVCD)) OVRD_DVCD = "0";
        if(OVRD_DVCD.equals("0")){
            mTvAccType.setText("");
        }else{
            if(OVRD_DVCD.equals("1")){
                mTvAccType.setText("연체");
            }else{
                mTvAccType.setText("기한이익상실");
            }
            mTvAccType.setTextColor(Color.parseColor("#e0250c"));
        }

        String BLNC = mainAccountInfo.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            mTvAmount.setText(BLNC);
        }

        String MM_PI_DD = mainAccountInfo.getMM_PI_DD();
        if (!TextUtils.isEmpty(MM_PI_DD))
            mTvPaymentDay.setText("매월 " + MM_PI_DD + "일");

        //상환은 무조건 출력해준다.
        mTvBtn1.setTag(mainAccountInfo);
        mTvBtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                String ACNO = mainAccountInfo.getACNO();
                if (TextUtils.isEmpty(ACNO))
                    return;

                String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
                if (TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                    return;

                String SUBJ_CD =  mainAccountInfo.getSUBJ_CD();
                if(TextUtils.isEmpty(SUBJ_CD)){
                    SUBJ_CD = ACCO_IDNO.substring(0, 2);
                }
                requestLoanRepay(ACNO, ACCO_IDNO, SUBJ_CD);
            }
        });

        //관리버튼 출력
        mTvBtn2.setClickable(true);
        if(OVRD_DVCD.equals("0")){
            mLayoutBtn2.setVisibility(View.VISIBLE);
            mTvBtn2.setTag(mainAccountInfo);
            mTvBtn2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTvBtn2.setClickable(false);
                    Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();

                    if (mainAccountInfo == null)
                        return;

                    String ACNO = mainAccountInfo.getACNO();
                    if (TextUtils.isEmpty(ACNO))
                        return;

                    String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
                    if (TextUtils.isEmpty(ACCO_IDNO) || ACCO_IDNO.length() <= 2)
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = WasServiceUrl.LON0050100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String SUBJ_CD = ACCO_IDNO.substring(0, 2);
                    String param =  "ACNO=" + ACNO + "&ACCO_IDNO=" + ACCO_IDNO + "&SUBJ_CD=" + SUBJ_CD;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    mContext.startActivity(intent);
                }
            });
        }else{
            mLayoutBtn2.setVisibility(View.GONE);
        }

        //대출 여신거래 상태 레이아웃 표시
        displayLoanState(mainAccountInfo);
    }

    /**
     * 상환 요청
     */
    private void requestLoanRepay(String ACNO, String ACCO_IDNO, String SUBJ_CD) {
        Map param = new HashMap();
        param.put("ACNO", ACNO);
        param.put("ACCO_IDNO", ACCO_IDNO);
        param.put("SUBJ_CD", SUBJ_CD);

        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.LON0050100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if(((BaseActivity) mContext).onCheckHttpError(ret, false)) return;

                Intent intent = new Intent(mContext, WebMainActivity.class);
                String url = WasServiceUrl.LON0050900.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                mContext.startActivity(intent);
            }
        });
    }

    /**
     * 여신거래 조건변경을 한 경우 상태 메세지 출력
     *
     * @param mainAccountInfo
     */
    private void displayLoanState(Main2AccountInfo mainAccountInfo){
        MLog.d();
        if(DataUtil.isNotNull(mainAccountInfo.getACPT_CNTN())){//접수내용 체크
            //mLayoutBtnAndLoanRelay.setVisibility(View.VISIBLE);
            if(DataUtil.isNotNull(mainAccountInfo.getLOAN_PROP_NO())){//대출신청번호체크
                mTvLoanRelayMsg.setText(mainAccountInfo.getACPT_CNTN());
                mLayoutLoanRelay.setVisibility(View.VISIBLE);
                mTvLoanJudgeMsg.setVisibility(View.GONE);

                mLayoutLoanRelay.setTag(mainAccountInfo);
                mLayoutLoanRelay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Main2AccountInfo mainAccountInfo = (Main2AccountInfo)v.getTag();
                        requestLoanRelayGoOn(mainAccountInfo);
                    }
                });
            }else{
                mTvLoanJudgeMsg.setText(mainAccountInfo.getACPT_CNTN());
                mTvLoanJudgeMsg.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * 여신거래 상태 이어하기
     */
    private void requestLoanRelayGoOn(Main2AccountInfo mainAccountInfo) {
        MLog.d();

        String DSCT_CD = mainAccountInfo.getDSCT_CD();
        String ACNO = mainAccountInfo.getACNO();
        String ACCO_IDNO = mainAccountInfo.getACCO_IDNO();
        String SUBJ_CD = mainAccountInfo.getSUBJ_CD();
        final String PROP_STEP_CD = mainAccountInfo.getPROP_STEP_CD();
        final String LOAN_PROP_NO = mainAccountInfo.getLOAN_PROP_NO();

        Logs.d("DSCT_CD : " + DSCT_CD);
        Logs.d("ACNO : " + ACNO);
        Logs.d("ACCO_IDNO : " +  ACCO_IDNO);
        Logs.d("SUBJ_CD : " +  SUBJ_CD);
        Logs.d("PROP_STEP_CD : " + PROP_STEP_CD);
        Logs.d("LOAN_PROP_NO : " + LOAN_PROP_NO);

        if (TextUtils.isEmpty(DSCT_CD) || TextUtils.isEmpty(ACNO) || TextUtils.isEmpty(ACCO_IDNO) ||
                TextUtils.isEmpty(SUBJ_CD) || TextUtils.isEmpty(PROP_STEP_CD) || TextUtils.isEmpty(LOAN_PROP_NO))
            return;

        Map param = new HashMap();
        param.put("ACNO", ACNO);
        param.put("ACCO_IDNO", ACCO_IDNO);
        param.put("SUBJ_CD", SUBJ_CD);
        param.put("LOAN_LNKN_PRGS_STEP_DVCD", PROP_STEP_CD);
        param.put("PROP_NO", LOAN_PROP_NO);

        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if(((BaseActivity) mContext).onCheckHttpError(ret, false)) return;

                try {
                    final JSONObject object = new JSONObject(ret);

                    String SCRN_ID = object.optString("SCRN_ID");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = SaidaUrl.getBaseWebUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = "PROP_NO=" + LOAN_PROP_NO + "&INTR_RDCN_DEMD_PROP_NO=" + LOAN_PROP_NO + "&LOAN_LNKN_PRGS_STEP_DVCD=" + PROP_STEP_CD;
                    intent.putExtra(Const.INTENT_PARAM, param);
                    mContext.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}
