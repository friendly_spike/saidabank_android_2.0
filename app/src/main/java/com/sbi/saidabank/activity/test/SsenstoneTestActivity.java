package com.sbi.saidabank.activity.test;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.widget.RadioButton;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.login.AuthRegisterGuideActivity;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.FingerprintActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.activity.ssenstone.PatternRegActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeRegActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;
import com.sbi.saidabank.web.JavaScriptApi;
import com.sbi.saidabank.web.JavaScriptBridge;
import com.sbi.saidabank.web.OnWebActionListener;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Saidabank_android
 * Class: ESpiderTestActivity
 * Created by 950469 on 2018. 9. 4..
 * <p>
 * Description:
 */
public class SsenstoneTestActivity extends BaseActivity implements View.OnClickListener, OnWebActionListener {
    private static final int REQ_REG_PATTERN = 10000;
    private static final int REQ_LOGIN_PATTERN = 10001;
    private static final int REQ_FIDO_FINGERPRINT = 10002;
    private static final int REQ_REG_PIN = 10003;
    private static final int REQ_LOGIN_PIN = 10004;
    private static final int REQ_CHANGE_PIN_AUTH = 10005;

    private static final int ACCESS_PATTERN = 1;
    private static final int ACCESS_PIN = 2;
    private static final int ACCESS_FINGERPRINT = 3;

    private RadioButton         mBtnRegRadio;
    private RadioButton         mBtnAuthRadio;
    private RadioButton         mBtnChangePinRadio;

    private String              mOperation;
    private int                 mAccessType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_ssenstone);

        initUX();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sstone_test_01 :
                mAccessType = ACCESS_PATTERN;
                //checkPermission(ACCESS_PATTERN);
                showPatternActivity();
                break;

            case R.id.sstone_test_02 :
                mAccessType = ACCESS_PATTERN;
                //checkPermission(ACCESS_PATTERN);
                showFingerprintActivity();
                break;

            case R.id.sstone_test_03 :
                mAccessType = ACCESS_PIN;
                //checkPermission(ACCESS_PIN);
                showPincodeActivity();
                break;

            case R.id.sstone_test_04 :
                showSignAuthFingerprint();
                break;

            case R.id.sstone_test_05 :
                showSignAuthPin();
                break;

            case R.id.sstone_test_06 :
                showFirstReg();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case JavaScriptApi.API_500 :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String result = commonUserInfo.getResultMsg();
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put("signed_data", result);
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                        Logs.showToast(this, jsonObj.toString());
                    }
                }
                break;

                case JavaScriptApi.API_501 :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String result = commonUserInfo.getResultMsg();
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put("signed_data", result);
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                        Logs.showToast(this, jsonObj.toString());
                    }
                }
                break;

                case REQ_REG_PATTERN :
                {
                    String msg = "success REQ_REG_PATTERN";
                    Logs.showToast(this, msg);
                }
                break;

                case REQ_LOGIN_PATTERN :
                {
                    String msg = "success REQ_LOGIN_PATTERN";
                    Logs.showToast(this, msg);
                }
                break;

                case REQ_LOGIN_PIN :
                {
                    String msg = "success REQ_LOGIN_PIN";
                    Logs.showToast(this, msg);
                }
                break;

                case REQ_CHANGE_PIN_AUTH :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String code = commonUserInfo.getSignData();
                        showChangePinPW(code);
                    }
                }
                break;

                default:
                    break;
            }
        } else {
            switch (requestCode) {
                case JavaScriptApi.API_501 :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String msg = commonUserInfo.getResultMsg();
                        Logs.showToast(this, msg);
                    }
                }
                break;

                case REQ_LOGIN_PATTERN :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String code = commonUserInfo.getResultMsg();
                        String msg = "Operation : " + mOperation + ", Code : " + code;
                        Logs.showToast(this, msg);
                    }
                }
                break;

                case REQ_REG_PATTERN :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String code = commonUserInfo.getResultMsg();
                        String msg = "Operation : " + mOperation + ", Code : " + code;
                        Logs.showToast(this, msg);
                    }
                }
                break;

                case REQ_REG_PIN :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String msg = commonUserInfo.getResultMsg();
                        Logs.showToast(this, msg);
                    }
                }
                break;

                case REQ_LOGIN_PIN :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String msg = commonUserInfo.getResultMsg();
                        Logs.showToast(this, msg);
                    }
                }
                break;

                case REQ_CHANGE_PIN_AUTH :
                {
                    if (data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String msg = commonUserInfo.getResultMsg();
                        Logs.showToast(this, msg);
                    }
                }
                break;

                default:
                    break;
            }
        }

        mOperation = "";
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_READ_PHONE_STATE:
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mAccessType == ACCESS_PATTERN)
                        showPatternActivity();
                    else if (mAccessType == ACCESS_PIN)
                        showPincodeActivity();
                    else if (mAccessType == ACCESS_FINGERPRINT)
                        showFingerprintActivity();
                } else {
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
/*
                    DialogUtil.alert(SsenstoneTestActivity.this, getString(R.string.common_notice),
                            getString(R.string.msg_permission_exit_no_permission), getString(R.string.common_confirm), okClick);
*/
                }
            }
            break;

            default:
                break;
        }
    }

    private void initUX() {
        mBtnRegRadio = findViewById(R.id.btn_ss_reg);
        mBtnAuthRadio = findViewById(R.id.btn_ss_auth);
        mBtnChangePinRadio = findViewById(R.id.btn_ss_change_pin);

        findViewById(R.id.sstone_test_01).setOnClickListener(this);
        findViewById(R.id.sstone_test_02).setOnClickListener(this);
        findViewById(R.id.sstone_test_03).setOnClickListener(this);
        findViewById(R.id.sstone_test_04).setOnClickListener(this);
        findViewById(R.id.sstone_test_05).setOnClickListener(this);
        findViewById(R.id.sstone_test_06).setOnClickListener(this);
    }

    private void showPatternActivity() {
        String op = getOperation();
        if (TextUtils.isEmpty(op)) {
            String  msg = "기능을 선택해 주세요.";
            Logs.showToast(this, msg);
            return;
        }

        mOperation = op;
        CommonUserInfo commonUserInfo;
        if (mOperation.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
            Intent intent = new Intent(this, PatternRegActivity.class);
            commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PATTERN, EntryPoint.AUTH_TOOL_MANAGE_PATTERN);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQ_REG_PATTERN);

        } else if (mOperation.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
            Intent intent = new Intent(this, PatternAuthActivity.class);
            commonUserInfo = new CommonUserInfo(EntryPoint.LOGIN_PATTERN, EntryPoint.LOGIN_PATTERN);
            commonUserInfo.setOperation(op);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQ_LOGIN_PATTERN);

        } else if (mOperation.equalsIgnoreCase(Const.SSENSTONE_CHANGE_PIN)) {
            /*
            Intent intent = new Intent(this, PatternAuthActivity.class);
            commonUserInfo.setEntryPoint(Const.SSENSTONE_ACCESS_CHANGE);
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivity(intent);
            */
        }
    }

    private void showFingerprintActivity() {
        String op = getOperation();
        if (TextUtils.isEmpty(op)) {
            String  msg = "기능을 선택해 주세요.";
            Logs.showToast(this, msg);
            return;
        }

        if (StonePassUtils.isUsableFingerprint(this) != 0) {
            String  msg = "해당 기능을 사용 할 수 없습니다.";
            Logs.showToast(this, msg);
            return;
        }

        mOperation = op;

        Intent intent = new Intent(this, FingerprintActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_BIO, EntryPoint.AUTH_TOOL_MANAGE_BIO);
        commonUserInfo.setOperation(op);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivityForResult(intent, REQ_FIDO_FINGERPRINT);
    }

    private void showPincodeActivity() {
        String op = getOperation();
        if (TextUtils.isEmpty(op)) {
            String  msg = "기능을 선택해 주세요.";
            Logs.showToast(this, msg);
            return;
        }
/*
        SSUserManager ssUserManager = mStonePassManager.getSSUserManager();
        if (ssUserManager == null) {
            return;
        }

        String ret = mStonePassManager.checkRegistPinUser(userName, systemId);
        if ("-1".equalsIgnoreCase(ret)) {
            Toast.makeText(this, "사용자 정보를 가져오지 못했습니다.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (Const.SSENSTONE_REGISTER.equalsIgnoreCase(op)) {
            if ("00".equalsIgnoreCase(ret) ||
                    "01".equalsIgnoreCase(ret) ) {
                Toast.makeText(this, "등록된 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("02".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "차단된 사용자입니다.\n차단 해제 후 사용 가능합니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else if (Const.SSENSTONE_AUTHENTICATE.equalsIgnoreCase(op)) {
            if ("99".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "삭제된 사용자입니다.\n등록 후 사용 가능합니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("G104".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "등록되지 않은 사용자입니다.\n등록 후 사용 가능합니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else if (Const.SSENSTONE_DEREGISTER.equalsIgnoreCase(op)) {
            if ("99".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "삭제된 사용자입니다.\n등록 후 사용 가능합니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("02".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "차단된 사용자입니다.\n차단 해제 후 사용 가능합니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("G104".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "등록되지 않은 사용자입니다.\n등록 후 사용 가능합니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else if (Const.SSENSTONE_CHANGE_PIN.equalsIgnoreCase(op)) {
            if ("99".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "삭제된 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("02".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "차단된 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("G104".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "등록되지 않은 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else if (Const.SSENSTONE_BLOCK.equalsIgnoreCase(op)) {
            if ("99".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "삭제된 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("02".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "차단된 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            } else if ("G104".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "등록되지 않은 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        } else if (Const.SSENSTONE_UNBLOCK.equalsIgnoreCase(op)) {
            if ("99".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "삭제된 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            } else  if ("G104".equalsIgnoreCase(ret)) {
                Toast.makeText(this, "등록되지 않은 사용자입니다.", Toast.LENGTH_SHORT).show();
                return;
            }
        }
*/
        mOperation = op;

        if (mOperation.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
            Intent intent = new Intent(this, PincodeRegActivity.class);
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PINCODE, EntryPoint.AUTH_TOOL_MANAGE_PINCODE);
            commonUserInfo.setOperation(op);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQ_REG_PIN);

        } else if (mOperation.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
            Intent intent = new Intent(this, PincodeAuthActivity.class);
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PINCODE, EntryPoint.AUTH_TOOL_MANAGE_PINCODE);
            commonUserInfo.setOperation(op);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQ_LOGIN_PIN);

        }  else if (mOperation.equalsIgnoreCase(Const.SSENSTONE_CHANGE_PIN)) {
            Intent intent = new Intent(this, PincodeAuthActivity.class);
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PINCODE, EntryPoint.AUTH_TOOL_MANAGE_PINCODE);
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQ_CHANGE_PIN_AUTH);
        }
    }

    private void showSignAuthFingerprint() {
        JavaScriptBridge javaScriptBridge = new JavaScriptBridge(this, null,this);

        try {
            JSONObject jsonObj = new JSONObject();
            JSONObject jsonObj1 = new JSONObject();
            jsonObj.put("body", jsonObj1);
            javaScriptBridge.api_501_fingerprintSign(jsonObj);
        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    private void showSignAuthPin() {
        JavaScriptBridge javaScriptBridge = new JavaScriptBridge(this, null,this);

        try {
            JSONObject jsonObj = new JSONObject();
            JSONObject jsonObj1 = new JSONObject();
            jsonObj1.put("sign_text", "1234567890");
            jsonObj1.put("length", 6);
            jsonObj.put("body", jsonObj1);
            javaScriptBridge.api_500_pinSign(jsonObj);
        } catch (Exception e) {
            Logs.printException(e);
        }
    }

    private void showFirstReg() {
        Intent intent = new Intent(this, AuthRegisterGuideActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.SERVICE_JOIN, EntryPoint.SERVICE_JOIN);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivity(intent);
    }

    private void showChangePatternPW(String data) {
        String op = getOperation();
        if (TextUtils.isEmpty(op)) {
            String  msg = "기능을 선택해 주세요.";
            Logs.showToast(this, msg);
            return;
        }

        Intent intent = new Intent(this, PatternRegActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.LOGIN_PATTERN, EntryPoint.LOGIN_PATTERN);
        if (!TextUtils.isEmpty(data))
            commonUserInfo.setSignData(data);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivity(intent);
    }

    private void showChangePinPW(String data) {
        String op = getOperation();
        if (TextUtils.isEmpty(op)) {
            String  msg = "기능을 선택해 주세요.";
            Logs.showToast(this, msg);
            return;
        }

        Intent intent = new Intent(this, PincodeRegActivity.class);
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PINCODE, EntryPoint.AUTH_TOOL_MANAGE_PINCODE);
        if (!TextUtils.isEmpty(data))
            commonUserInfo.setSignData(data);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivity(intent);
    }

    public String getOperation() {
        if (mBtnRegRadio.isChecked())
            return Const.SSENSTONE_REGISTER;

        if (mBtnAuthRadio.isChecked())
            return Const.SSENSTONE_AUTHENTICATE;

        if (mBtnChangePinRadio.isChecked())
            return Const.SSENSTONE_CHANGE_PIN;

        return null;
    }

    @Override
    public void showProgress() {

    }

    @Override
    public void dismissProgress() {

    }

    @Override
    public void openCamera() {

    }

    @Override
    public void loadUrl(String url, String params) {

    }

    @Override
    public void refreshWebView() {

    }

    @Override
    public void setNeedResumeRefresh() {

    }

    @Override
    public void closeWebView() {

    }

    @Override
    public void setCurrentPage(String url) {

    }

    @Override
    public void setFinishedPageLoading(boolean flag) {

    }

    @Override
    public void onWebViewSSLError(String url, String errorCd, SslErrorHandler handler) {

    }

    @Override
    public void getProfileImage(JSONObject json, String workType) {

    }
}
