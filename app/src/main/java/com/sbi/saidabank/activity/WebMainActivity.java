package com.sbi.saidabank.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.info.DeviceResult;
import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.common.CoupleContactsPickActivity;
import com.sbi.saidabank.activity.crop.CropImageActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;

import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.WasServiceUrl;

import com.sbi.saidabank.solution.espider.EspiderManager;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptApi;
import com.sbi.saidabank.web.JavaScriptBridge;
import com.selvasai.selvyocr.idcard.recognizer.SelvyIDCardRecognizer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * WebMainActivity : 웹뷰 메인 화면
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class WebMainActivity extends BaseWebActivity implements I3GAsyncResponse, EspiderManager.EspiderEventListener, DatePickerCallback {

    private JavaScriptBridge mJsBridge;
    private FrameLayout mWebViewContainer;
    private String mCurrentWebPage;
    private Uri mTargetPhotoURI;

    private String mUrl;
    private String mParam;
    private boolean isNeedReload;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        enableScreenCapture();
        setContentView(R.layout.activity_web_main);

        if (DataUtil.isNull(getIntent()))
            return;

        mWebViewContainer = (FrameLayout) findViewById(R.id.webview_container);
        BaseWebView webView = getCurrentWebView();
        webView.setTag(1);
        webView.setWebViewClient(new BaseWebClient(WebMainActivity.this, this));
        webView.setWebChromeClient(new BaseWebChromeClient(WebMainActivity.this, mWebViewContainer, mJsBridge));
        mJsBridge = new JavaScriptBridge(WebMainActivity.this, mWebViewContainer,this);
        webView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);

        Button mBtnClose = (Button) findViewById(R.id.btn_close);
        mBtnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        String url = getIntent().getStringExtra(Const.INTENT_MAINWEB_URL);
        if(TextUtils.isEmpty(url)){
            finish();
            return;
        }

        //scheme가 없다는 말은 이동 페이지 값만 들어왔다는 말이다.
        if(!url.contains("http")){
            if(url.startsWith("/")){
                url = SaidaUrl.getBaseWebUrl() + url;
            }else{
                url = SaidaUrl.getBaseWebUrl() + "/" + url;
            }
        }

        MLog.i("WebMainActivity - url : " + url);
        String param = getIntent().hasExtra(Const.INTENT_PARAM) ? getIntent().getStringExtra(Const.INTENT_PARAM) : Const.EMPTY;
        mUrl = url;
        mParam = param;
        if (DataUtil.isNull(param)) {
            webView.loadUrl(url);
            mBtnClose.setVisibility(SaidaUrl.getBaseWebUrl().equals(url) ? View.VISIBLE : View.GONE);
        } else {
            MLog.i("WebMainActivity - param : " + param);
            webView.postUrl(url, param.getBytes());
            mBtnClose.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        MLog.d();
        super.onResume();
        if(isNeedReload){
            isNeedReload = false;
            if (DataUtil.isNull(mParam)) {
                getCurrentWebView().loadUrl(mUrl);
            } else {
                getCurrentWebView().postUrl(mUrl, mParam.getBytes());
            }
        }
    }

    @Override
    protected void onPause() {
        MLog.d();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        MLog.d();
        if (DataUtil.isNotNull(mJsBridge)) {
            // 인증서 내보내기의 Pending상태이면 취소 시키고 빠져 나간다.
            mJsBridge.destroyJavaScriptBrigdge();
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        String url = getCurrentWebView().getUrl();
        if (url.contains("mdjoin")){
            if(url.contains("MAIN1001")){
                Intent intent = new Intent(this, Main2Activity.class);
                intent.putExtra("tabIndex",4);
                startActivity(intent);
            }else{
                getCurrentWebView().goBack();
            }
        }else{
            if (DataUtil.isNotNull(mJsBridge)) {
                Logs.e("onBackPressed - url : " + url);
                if(url.contains(SaidaUrl.getBaseWebUrl())){
                    // 백키 이벤트를 알림
                    mJsBridge.callJavascriptFunc("co.app.from.back", null);
                }else{
                    finish();
                }
            }else{
                if (DataUtil.isNotNull(mCurrentWebPage)) {
                    int webViewCnt = mWebViewContainer.getChildCount();
                    if (webViewCnt > 1) {
                        mWebViewContainer.removeViewAt(webViewCnt - 1);
                    }
                } else {
                    finish();
                }
            }
        }
    }

    private BaseWebView getCurrentWebView(){

        int webViewCnt = mWebViewContainer.getChildCount();

        if(webViewCnt < 1){
            BaseWebView webView = new BaseWebView(this);
            mWebViewContainer.addView(webView);
            return webView;
        }

        return (BaseWebView)mWebViewContainer.getChildAt(webViewCnt-1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        MLog.d();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isPerDeny = false;

        MLog.i("requestCode >> " + requestCode);

        switch (requestCode) {
            case Const.REQUEST_PERMISSION_CALL:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }
                if (isPerDeny) {
                    DialogUtil.alert(WebMainActivity.this, R.string.common_msg_call_permission);
                } else {
                    String telNo = ((SaidaApplication) getApplication()).getTempTelNo();
                    if (!TextUtils.isEmpty(telNo)) {
                        if (ActivityCompat.checkSelfPermission(WebMainActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telNo));
                        startActivity(intent);
                    }
                }
                break;

            case JavaScriptApi.API_800:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED)
                        isPerDeny = true;
                }
                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800, true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(WebMainActivity.this, permissions[0]);
                    Logs.i("state : " + state);
                    if (!state) {
                        DialogUtil.alert(
                                WebMainActivity.this,
                                "권한설정",
                                "닫기",
                                getString(R.string.msg_permission_camera_allow),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ;
                                    }
                                }
                        );
                    }
                } else {
                    JSONObject jsonObject = null;
                    try {
                        String param = mJsBridge.getJsonObjecParam(JavaScriptApi.API_800);
                        if (!TextUtils.isEmpty(param)) {
                            jsonObject = new JSONObject(param);
                        }
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    Intent intent = new Intent(WebMainActivity.this, CameraActivity.class);
                    if (jsonObject != null) {
                        try {
                            JSONObject body = jsonObject.getJSONObject("body");
                            String docType = body.optString("doc_type");
                            if (Const.IDENTIFICATION_TYPE_ID.equalsIgnoreCase(docType)) {
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
                            } else if (Const.IDENTIFICATION_TYPE_DOC.equalsIgnoreCase(docType)) {
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_DOCUMENT_A4);
                                intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_PORTRAIT);
                            }
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                        //파라미터를 꺼내오면 자동으로 삭제한다.
                        //mJsBridge.setJsonObjecParam(null);
                    } else {
                        intent.putExtra(CameraActivity.DATA_DOCUMENT_TYPE, CameraActivity.TYPE_ID_CARD);
                        intent.putExtra(CameraActivity.DATA_DOCUMENT_ORIENTATION, CameraActivity.ORIENTATION_LANDSCAPE);
                    }
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_AUTO, "카메라 영역에 [신분증]을 맞추면 자동촬영 됩니다");
                    intent.putExtra(CameraActivity.DATA_TITLE_MESSAGE_MANUAL, "카메라 영역에 [신분증]을 맞추고 촬영해 주세요");
                    intent.putExtra(CameraActivity.DATA_ENCRYPT_KEY, Const.ENCRYPT_KEY);
                    startActivityForResult(intent, JavaScriptApi.API_800);
                    overridePendingTransition(0, 0);
                }
                break;

            case JavaScriptApi.API_900:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }
                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_900, true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(WebMainActivity.this, permissions[0]);
                    if (!state) {
                        DialogUtil.alert(
                                WebMainActivity.this,
                                "권한설정",
                                "닫기",
                                getString(R.string.msg_permission_storage_allow),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                }
                        );
                    }
                } else {
                    JSONObject jsonObj = new JSONObject();
                    String fileName = mJsBridge.getJsonObjecParam(JavaScriptApi.API_900);
                    if (!"webreturn".equals(fileName)) {
                        try {
                            BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
                            if (TextUtils.isEmpty(fileName))
                                fileName = Utils.getCurrentDate("") + "_" + Utils.getCurrentTime("") + ".jpg";
                            boolean ret = ImgUtils.captureSave(WebMainActivity.this, webView, fileName);
                            jsonObj.put("result", ret);
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                    } else {
                        try {
                            BaseWebView webView = (BaseWebView) mWebViewContainer.getChildAt(mWebViewContainer.getChildCount() - 1);
                            byte[] imgData = ImgUtils.captureSave(webView);
                            if (imgData != null && imgData.length > 0) {
                                jsonObj.put("result", true);
                                jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                            } else {
                                jsonObj.put("result", false);
                            }
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_900, true), jsonObj);
                }
                break;

            case Const.REQUEST_PROFILE_ALBUM:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }
                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907, true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(WebMainActivity.this, permissions[0]);
                    if (!state) {
                        DialogUtil.alert(
                                WebMainActivity.this,
                                "권한설정",
                                "닫기",
                                "저장소 권한동의를 하셔야 앨범에서 사진을 가져올 수 있습니다.",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) { }
                                }
                        );
                    }
                } else {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                    startActivityForResult(intent, Const.REQUEST_PROFILE_ALBUM);
                }
                break;

            case Const.REQUEST_PROFILE_IMAGE:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }
                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907, true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(WebMainActivity.this, permissions[0]);
                    if (!state) {
                        DialogUtil.alert(
                                WebMainActivity.this,
                                "권한설정",
                                "닫기",
                                "저장소 권한동의를 하셔야 프로필 사진을 가져올 수 있습니다.",
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) { }
                                }
                        );
                    }
                } else {
                    sendProfileImage("");
                }
                break;

            case Const.REQUEST_PROFILE_CAMERA:
                int i = 0;
                for (i = 0; i < permissions.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }
                if (isPerDeny) {
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907, true), null);
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(WebMainActivity.this, permissions[i]);
                    String msg = "";
                    if (!state) {
                        msg = "앱 설정에서 카메라 및 저장소 권한을 모두 허용하셔야 프로필 사진을 촬영하실 수 있습니다.";
                    } else {
                        if (Manifest.permission.CAMERA.equals(permissions[i])) {
                            msg = getString(R.string.msg_permission_camera_allow);
                        } else {
                            msg = getString(R.string.msg_permission_storage_allow);
                        }
                    }
                    DialogUtil.alert(
                            WebMainActivity.this,
                            "권한설정",
                            "닫기",
                            msg,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    PermissionUtils.goAppSettingsActivity(WebMainActivity.this);
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            }
                    );
                } else {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            String imageFileName = "sbi_" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                            File storageDir = getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
                            photoFile = File.createTempFile(imageFileName, ".jpg", storageDir);
                        } catch (IOException e) {
                            MLog.e(e);
                        }
                        // Continue only if the File was successfully created
                        if (DataUtil.isNotNull(photoFile)) {
                            Uri mTargetPhotoURI;
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                                mTargetPhotoURI = Uri.fromFile(photoFile);
                            } else {
                                mTargetPhotoURI = FileProvider.getUriForFile(WebMainActivity.this, getPackageName() + ".fileprovider", photoFile);
                            }
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                            startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
                        }
                    }
                }
                break;

            case JavaScriptApi.API_930:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }
                if (isPerDeny) {
                    final JSONObject jsonObj = new JSONObject();
                    DialogUtil.alert(
                            WebMainActivity.this,
                            getString(R.string.msg_permission_read_contacts),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        jsonObj.put("result", Const.BRIDGE_RESULT_FALSE);
                                    } catch (JSONException e) {
                                        //e.printStackTrace();
                                    }
                                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_930), jsonObj);
                                }
                            }
                    );
                } else {
                    JSONObject jsonObj = new JSONObject();
                    ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();
                    ContactsInfo contactsInfo;
                    Cursor contactCursor = null;
                    String name;
                    String phonenumber;
                    JSONArray jsonArray = new JSONArray();
                    try {
                        Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
                        String[] projection = new String[]{
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                                ContactsContract.CommonDataKinds.Phone.NUMBER,
                                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                                ContactsContract.Contacts.PHOTO_ID
                        };

                        String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";
                        contactCursor = getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
                        if (contactCursor.moveToFirst()) {
                            do {
                                phonenumber = contactCursor.getString(1);
                                if (phonenumber.length() <= 0)
                                    continue;

                                name = contactCursor.getString(2);
                                phonenumber = phonenumber.replaceAll("-", "");
                                if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                                    continue;
                                }

                                // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 핸드폰 번호인 경우
                                if (!TextUtils.isEmpty(name) && listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                                    contactsInfo = new ContactsInfo();
                                    contactsInfo.setTLNO(phonenumber);
                                    contactsInfo.setFLNM(name);
                                    listContactsInPhonebook.add(contactsInfo);
                                    JSONObject itemObject = new JSONObject();
                                    itemObject.put("TLNO", phonenumber);
                                    itemObject.put("FLNM", name);
                                    jsonArray.put(itemObject);
                                }
                            } while (contactCursor.moveToNext());
                        }
                    } catch (Exception e) {
                        MLog.e(e);
                    } finally {
                        if (contactCursor != null) {
                            contactCursor.close();
                        }
                        try {
                            jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                            jsonObj.put("addr_data", jsonArray);
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_930), jsonObj);
                    }
                }
                break;
            case JavaScriptApi.API_931:
                for (int grant : grantResults) {
                    if (grant == PackageManager.PERMISSION_DENIED) {
                        isPerDeny = true;
                        break;
                    }
                }
                if (isPerDeny) {
                    final JSONObject jsonObj = new JSONObject();
                    DialogUtil.alert(
                            WebMainActivity.this,
                            getString(R.string.msg_permission_read_contacts),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try {
                                        jsonObj.put("result", Const.BRIDGE_RESULT_FALSE);
                                    } catch (JSONException e) {
                                        //e.printStackTrace();
                                    }
                                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_931), jsonObj);
                                }
                            }
                    );
                } else {
                    Intent intent = new Intent(this, CoupleContactsPickActivity.class);
                    startActivityForResult(intent, JavaScriptApi.API_931);
                    overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                }
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        MLog.d();
        super.onActivityResult(requestCode, resultCode, data);

        // 로시스 라이브러리 업체의 카메라 화면의 리턴값이 1로 되어 있다.
        if (requestCode == JavaScriptApi.API_800) {
            if (resultCode == 1) {
                // 신분증 이미지 영역
                byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
                imageOCR =  SelvyIDCardRecognizer.decrypt(imageOCR, Const.ENCRYPT_KEY); // 이미지 복호화 추가
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("encode_image", Base64.encodeToString(imageOCR, Base64.DEFAULT));
                } catch (JSONException e) {
                    MLog.e(e);
                }
                Arrays.fill(imageOCR,(byte) 0);
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800, true), jsonObj);
            } else {
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800, true), null);
            }
        } else if (requestCode == JavaScriptApi.API_301 || requestCode == JavaScriptApi.API_302 || requestCode == JavaScriptApi.API_400) {
            mJsBridge.callJavascriptFunc("co.app.from.webviewClose", null);
        }

        if (resultCode == RESULT_OK) {

            MLog.i("requestCode >> " + requestCode);

            switch (requestCode) {

                // 1줄 보안키패드
                case JavaScriptApi.API_200:
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("input_id", data.getStringExtra(Const.INTENT_TRANSKEY_INPUTID1));
                        jsonObj.put("secure_data", data.getStringExtra(Const.INTENT_TRANSKEY_SECURE_DATA1));
                        jsonObj.put("input_length", data.getStringExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1).length());
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;

                case JavaScriptApi.API_201:  // Dot 타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:  // Dot 타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:  // 타기관 OTP 비밀번호 등록
                case JavaScriptApi.API_204:  // 타기관 OTP 비밀번호 인증
                case JavaScriptApi.API_205:  // 모바일 OTP 비밀번호 등록
                case JavaScriptApi.API_206:  // 모바일 OTP 비밀번호 인증
                    jsonObj = new JSONObject();
                    try {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                        jsonObj.put("secure_data", commonUserInfo.getSecureData());
                        if (requestCode == JavaScriptApi.API_202 || requestCode == JavaScriptApi.API_206)
                            jsonObj.put("forget_auth_no", commonUserInfo.isOtpForgetAuthNo());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;

                case JavaScriptApi.API_207:  //타기관OTP 비밀번호 인증 - 결과값 리턴
                case JavaScriptApi.API_208:  //모바일OTP 비밀번호 인증 - 결과값 리턴
                    jsonObj = new JSONObject();
                    try {
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                // PIN 전자서명 (500)
                case JavaScriptApi.API_500:
                    if (DataUtil.isNotNull(data)) {
                        jsonObj = new JSONObject();
                        try {
                            CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                            jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                            jsonObj.put("elec_sgnr_srno", commonUserInfo.getSignData());
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    }
                    break;

                 // 지문 전자서명 (501)
                case JavaScriptApi.API_501:
                    break;

                // 휴대폰 본인인증 (502)
                case JavaScriptApi.API_502:
                    try {
                        jsonObj = new JSONObject();
                        jsonObj.put("ci", "");
                        jsonObj.put("di", "");
                        jsonObj.put("cmcm_dvcd", "");
                        jsonObj.put("cpno", "");
                        if (DataUtil.isNotNull(data)) {
                            String diNo;
                            String ciNo;
                            if (data.hasExtra("DI_NO")) {
                                diNo = data.getStringExtra("DI_NO");
                                jsonObj.put("di", diNo);
                            }
                            if (data.hasExtra("CI_NO")) {
                                ciNo = data.getStringExtra("CI_NO");
                                jsonObj.put("ci", ciNo);
                            }
                            if (data.hasExtra("CMCM_DVCD")) {
                                ciNo = data.getStringExtra("CMCM_DVCD");
                                jsonObj.put("cmcm_dvcd", ciNo);
                            }
                            if (data.hasExtra("CPNO")) {
                                ciNo = data.getStringExtra("CPNO");
                                jsonObj.put("cpno", ciNo);
                            }
                        } else {
                            Utils.showToast(WebMainActivity.this, "본인인증 실패. data null");
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    break;

                // 타행계좌 인증 (503)
                case JavaScriptApi.API_503:
                    jsonObj = new JSONObject();
                    if (DataUtil.isNotNull(data)) {
                        try {
                            jsonObj.put(Const.BRIDGE_RESULT_KEY, data.getStringExtra(Const.BRIDGE_RESULT_KEY));
                            jsonObj.put("account_num", data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_NUMBER));
                            jsonObj.put("bank_cd", data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_BANK_CODE));
                            jsonObj.put("deposit_cust_name", data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_CUST_NAME));
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                break;

                // 스크래핑 : 건강보험납부내역, 건강보험 자격득실
                case JavaScriptApi.API_600:
                case JavaScriptApi.API_601:
                    boolean isCertOnly = data.getBooleanExtra(Const.INTENT_CERT_NO_SCRAPING, false);
                    if (isCertOnly) {
                        jsonObj = new JSONObject();
                        try {
                            jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_COMPLETE);
                            jsonObj.put("cn", data.getStringExtra(Const.INTENT_CERT_ISSUER_DN));
                            jsonObj.put("dn", data.getStringExtra(Const.INTENT_CERT_SUBJECT_DN));
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    } else {
                        jsonObj = new JSONObject();
                        String callBackName = mJsBridge.getCallBackFunc(requestCode, true);
                        EspiderManager spiderManager = EspiderManager.getInstance(this);
                        spiderManager.setEspiderEventListener(WebMainActivity.this);
                        if (DataUtil.isNotNull(data) &&
                                spiderManager.startEspider(
                                requestCode,
                                callBackName,
                                data.getStringExtra(Const.INTENT_CERT_PASSWORD),
                                data.getStringExtra(Const.INTENT_CERT_PATH),
                                data.getStringExtra(Const.INTENT_CERT_IDDATA),
                                data.getStringExtra(Const.INTENT_CERT_SERVICEDATA),
                                data.getStringExtra(Const.INTENT_CERT_ISSUER_DN),
                                data.getStringExtra(Const.INTENT_CERT_SUBJECT_DN)) == Const.ESPIDER_TYPE_RETURN_SUCCESS) {
                            // Scraping Start
                            try {
                                jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_START);
                            } catch (JSONException e) {
                                MLog.e(e);
                            }

                        } else {
                            // Scraping Fail
                            try {
                                jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                            } catch (JSONException e) {
                                MLog.e(e);
                            }

                        }
                        mJsBridge.callJavascriptFunc(callBackName, jsonObj);
                    }
                    break;

                // PDF 약관보기 (903)
                case JavaScriptApi.API_903:
                    jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_TRUE);
                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;

                // 앨범에서 가져오기 (2000)
                case Const.REQUEST_PROFILE_ALBUM:
                    if (DataUtil.isNotNull(data)) {
                        Uri photoUri = data.getData();
                        if (DataUtil.isNull(photoUri))
                            return;
                        MLog.i("Album Uri >> " + photoUri.toString());
                        Intent intent = new Intent(WebMainActivity.this, CropImageActivity.class);
                        intent.putExtra(Const.CROP_IMAGE_URI, photoUri.toString());
                        startActivityForResult(intent, Const.REQUEST_CROP_TO_IMAGE);
                    }
                    break;

                 // 카메라 촬영 (2002)
                case Const.REQUEST_PROFILE_CAMERA:
                    if (DataUtil.isNull(mTargetPhotoURI))
                        return;
                    MLog.i("Camera Uri >> " + mTargetPhotoURI.toString());
                    Intent intent = new Intent(WebMainActivity.this, CropImageActivity.class);
                    intent.putExtra(Const.CROP_IMAGE_URI, mTargetPhotoURI.toString());
                    startActivityForResult(intent, Const.REQUEST_CROP_TO_IMAGE);
                    break;

                // 이미지 크롭 (2003)
                case Const.REQUEST_CROP_TO_IMAGE:
                    if (DataUtil.isNotNull(data)) {
                        Bundle bundle = data.getExtras();
                        if (DataUtil.isNotNull(bundle.getString(Const.CROP_IMAGE_URI)) && DataUtil.isNotNull(Uri.parse(bundle.getString(Const.CROP_IMAGE_URI)))) {
                            MLog.i("Uri >> " + bundle.getString(Const.CROP_IMAGE_URI));
                            Uri cropUri = Uri.parse(bundle.getString(Const.CROP_IMAGE_URI));
                            if(DataUtil.isNotNull(cropUri)) {
                                //패스와 파일명이 모두 넘어간다.
                                sendProfileImage(cropUri.getPath());
                            }
                        }
                    }
                    break;
                case JavaScriptApi.API_931:
                    jsonObj = new JSONObject();
                    if (DataUtil.isNotNull(data)) {
                        try {
                            jsonObj.put("TLNO", data.getStringExtra("TLNO"));
                            jsonObj.put("MBR_NO", data.getStringExtra("MBR_NO"));
                            jsonObj.put("FLNM", data.getStringExtra("FLNM"));
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                default:
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case JavaScriptApi.API_201:          // dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:          // dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:          // 타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:          // 타기관OTP 비밀번호 인증 - 비밀번호 리턴
                case JavaScriptApi.API_205:          // 모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:          // 모바일OTP 비밀번호 인증 - 비밀번호 리턴
                case JavaScriptApi.API_207:          // 타기관OTP 비밀번호 인증 - 결과값 리턴
                case JavaScriptApi.API_208:          // 모바일OTP 비밀번호 인증 - 결과값 리턴
                case JavaScriptApi.API_500:          // Pincode 인증
                case JavaScriptApi.API_501:          // 지문 인증
                case JavaScriptApi.API_502:          // 휴대폰 본인인증
                case JavaScriptApi.API_503:          // 타행계좌인증
                case JavaScriptApi.API_903:          // PDF약관보기
                case Const.REQUEST_PROFILE_ALBUM:    // API_907 프로필 사진 가져오기
                case Const.REQUEST_PROFILE_IMAGE:    // API_907 프로필 사진 가져오기
                case Const.REQUEST_CROP_TO_IMAGE:    // API_907 프로필 사진 가져오기
                case Const.REQUEST_PROFILE_CAMERA: { // API_907 프로필 사진 가져오기
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    if (requestCode == Const.REQUEST_PROFILE_CAMERA) {
                        ImgUtils.deleteImageFile(this,mTargetPhotoURI);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_600:
                case JavaScriptApi.API_601: { // Javainterface 에서 600, 601번 호출하여 인증서 리스트 취소 시 콜백
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                }

                default:
                    break;
            }
        } else if(resultCode == Const.NICE_PREV){//나이스 이전키
            mJsBridge.callJavascriptFunc("co.app.from.back", null);
        } else if(resultCode == Const.NICE_NEXT){//나이스 가입
            WebView webview = getCurrentWebView();
            String niceUrl = WasServiceUrl.CRD0140100.getServiceUrl();
            webview.loadUrl(niceUrl);
        }
    }

    /*
     * Javainterface 에서 801번 호출되면 작업 후 값을 만들어 넘겨준다.
     * */
    @Override
    public void i3GProcessFinish(DeviceResult deviceResult) {
        MLog.d();
        if (DataUtil.isNotNull(deviceResult)) {
            try {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("natip", deviceResult.getNatip());
                jsonObj.put("wdata", deviceResult.getResultStr());
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_801, true), jsonObj);
            } catch (JSONException e) {
                MLog.e(e);
            }
        }
    }

    @Override
    public void onStatusEspider(String callBackName, int status, String msg) {
        MLog.i("onStatusEspider status[" + status + "] msg[" + msg + "]");
    }

    @Override
    public void onFinishEspider(String callBackName, JSONObject returnjsonobj) {
        MLog.d();
        // Javainterface 에서 600, 601번 호출되면 작업 후 값을 만들어 넘겨준다.
        if (EspiderManager.isEngineEnabled() && DataUtil.isNotNull(mJsBridge)) {
            mJsBridge.callJavascriptFunc(callBackName, returnjsonobj);
        }
    }

    @Override
    public void onDateSet(long l) {
        MLog.d();
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(l);
            int inputYear = calendar.get(Calendar.YEAR);
            int inputMonth = calendar.get(Calendar.MONTH) + 1;
            int inputDay = calendar.get(Calendar.DAY_OF_MONTH);
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("date", String.format("%04d", inputYear) + "" + String.format("%02d", inputMonth) + "" + String.format("%02d", inputDay));
            mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_250), jsonObj);
        } catch (JSONException e) {
            MLog.e(e);
        }
    }

    @Override
    public void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {

            File photoFile = ImgUtils.createNewImageFilePathName(this,"profile");
            if (DataUtil.isNotNull(photoFile)) {

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                    mTargetPhotoURI = Uri.fromFile(photoFile);
                } else {
                    mTargetPhotoURI = FileProvider.getUriForFile(this, getPackageName() + ".fileprovider", photoFile);
                }
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
            }
        }
    }

    @Override
    public void setNeedResumeRefresh() {
        isNeedReload = true;
    }

    @Override
    public void setCurrentPage(String page) {
        mCurrentWebPage = page;
    }

    @Override
    public void onWebViewSSLError(String url, String errorCd,final SslErrorHandler handler) {
        DialogUtil.alert(this, R.string.common_msg_error_ssl_cert, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.proceed();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
            }
        });
    }

    @Override
    public void getProfileImage(JSONObject json,String workType) throws JSONException{
        MLog.d();

        if(isFinishing()) return;

        MLog.i("workType >> " + workType);

        String[] perList01 = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        String[] perList02 = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (workType.equals("album")) {
            if (PermissionUtils.checkPermission(WebMainActivity.this, perList01, Const.REQUEST_PROFILE_ALBUM)) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                startActivityForResult(intent, Const.REQUEST_PROFILE_ALBUM);
            } else {
                if (DataUtil.isNotNull(json))
                    mJsBridge.setJsonObjecParam(JavaScriptApi.API_907, json.toString());
            }
        } else if (workType.equals("camera")) {
            if (PermissionUtils.checkPermission(WebMainActivity.this, perList02, Const.REQUEST_PROFILE_CAMERA)) {
                openCamera();
            } else {
                if (DataUtil.isNotNull(json))
                    mJsBridge.setJsonObjecParam(JavaScriptApi.API_907, json.toString());
            }
        } else if (workType.equals("image")) {
            if (PermissionUtils.checkPermission(WebMainActivity.this, perList01, Const.REQUEST_PROFILE_IMAGE)) {
                File file = new File(Prefer.getProfileImagePath(WebMainActivity.this));
                if (file.exists()) {
                    int size = (int) file.length();
                    byte[] imgData = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(imgData, 0, imgData.length);
                        buf.close();
                    } catch (IOException e) {
                        MLog.e(e);
                    }
                    JSONObject jsonObj = new JSONObject();
                    if (DataUtil.isNotNull(imgData)) {
                        jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                    } else {
                        jsonObj.put("result", false);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
                } else {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result", false);
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
                }
            } else {
                if (DataUtil.isNotNull(json))
                    mJsBridge.setJsonObjecParam(JavaScriptApi.API_907, json.toString());
            }
        }
    }


    /**
     * 웹에 이미지 데이타를 전송한다.
     * @param imagePath  파일패스와 파일이름 모두 포함되어 있다.
     */
    private void sendProfileImage(String imagePath) {
        MLog.d();
        try {
//            File storageDir = getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
//            if (!storageDir.exists()) {
//                storageDir.mkdir();
//            }

            //카메라 촬영한 이미지 삭제
            ImgUtils.deleteImageFile(this,mTargetPhotoURI);

            File file = new File(imagePath);
            if (file.exists()) {
                //이전 프로파일 이미지를 지원준다.
                String oldFilePath = Prefer.getProfileImagePath(this);
                if(!TextUtils.isEmpty(oldFilePath)) {
                    ImgUtils.deleteImageFile(this, Uri.parse(oldFilePath));
                }
                Prefer.setProfileImagePath(this,imagePath);

                int size = (int) file.length();
                byte[] imgData = new byte[size];
                try {
                    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                    buf.read(imgData, 0, imgData.length);
                    buf.close();
                } catch (IOException e) {
                    MLog.e(e);
                }
                JSONObject jsonObj = new JSONObject();
                try {
                    if (DataUtil.isNotNull(imgData)) {
                        jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                    } else {
                        jsonObj.put("result", false);
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
            } else {
                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("result", false);
                } catch (JSONException e) {
                    MLog.e(e);
                }
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }


}
