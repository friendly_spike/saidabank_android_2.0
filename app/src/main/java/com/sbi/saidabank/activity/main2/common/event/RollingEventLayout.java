package com.sbi.saidabank.activity.main2.common.event;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import com.sbi.saidabank.common.dialog.MainEventDialog;
import com.sbi.saidabank.customview.DrawEvent;

public class RollingEventLayout extends RelativeLayout {
    private Context mContext;
    private View mEventView;
    private MainEventDialog mEventDialog;

    private String mEventID;

    public RollingEventLayout(Context context) {
        super(context);
        initUX(context);
    }

    public RollingEventLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        mContext = context;
    }

    public void setEventID(String id){
        mEventID = id;
    }

    public void showEventIcon(){
        mEventView = new DrawEvent(mContext, new DrawEvent.TouchViewListener() {
            @Override
            public void onTouchView() {
                if (mContext instanceof Activity && ((Activity) mContext).isFinishing())
                    return;

                if (mEventDialog != null) {
                    mEventDialog.dismiss();
                    mEventDialog = null;
                }

                if(!TextUtils.isEmpty(mEventID))
                    showMainEventDialog();

                removeView(mEventView);
                mEventView = null;
            }
        });
        addView(mEventView);
    }

    private void showMainEventDialog() {
        if (mEventDialog != null && mEventDialog.isShowing()) {
            return;
        }

        mEventDialog = new MainEventDialog(mContext, mEventID, new MainEventDialog.OnListener() {
            @Override
            public void onFinishEvent(String moveUrl) {
                if (mContext instanceof Activity && ((Activity) mContext).isFinishing())
                    return;

                mEventDialog.dismiss();
                mEventDialog = null;
            }
        });
        mEventDialog.show();
    }
}
