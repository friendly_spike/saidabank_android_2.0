package com.sbi.saidabank.activity.transaction.safedeal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.TransferSafeDealActivity;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;

public class SlideBaseLayout extends RelativeLayout {

    /** 안심이체 애니메이션 타입 */
    public enum AnimationKind{
        ANI_SCALE_SMALL,
        ANI_SCALE_ORIGIN,
        ANI_SLIDE_DOWN,
        ANI_SLIDE_UP
    }

    protected TransferSafeDealActivity mActivity;
    protected SlideBaseLayout mSlideLayout;
    protected RelativeLayout mLayoutBody;

    protected OnSafeDealActionListener mActionlistener;
    protected int mOriginViewHeight;
    protected int mScreenHeight;     //좌표 계산을 위한 실제적인 높이.getY()로 계산할때 Activity의 높이가된다.
    protected int mOriginViewYPos;
    //public boolean isScale;

    public SlideBaseLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideBaseLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    /**
     * 초기화 함수
     * Override해서 사용하도록 한다.
     */
    public void clearData(){

    }

    private void initUX(Context context){
        mActivity = (TransferSafeDealActivity)context;
        mSlideLayout = this;
        //isScale = false;
        mActionlistener = (OnSafeDealActionListener) context;
        mScreenHeight = GraphicUtils.getActivityHeight(getContext());


        int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);
        setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,0,0}));
    }

    /**
     * 뷰를 작은 사이즈로 스케일함
     * @param aniTime 밀리세컨드 초
     * @param delayTime 밀리세컨드 초
     */
    public void scaleSmall(final int aniTime,int delayTime){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mActivity.isFinishing()) return;

                final long aniKey= System.currentTimeMillis();
                mActivity.showAniProtectView(true,aniKey,"scaleSmall");

                float offset = (getHeight() * 0.083f)/2;

                mSlideLayout.animate()
                        .scaleX(0.917f)
                        .scaleY(0.917f)
                        .translationY(offset * -1)
                        .setDuration(aniTime)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mActivity.showAniProtectView(false,aniKey,"scaleSmall");

                                //isScale = true;
                                mActionlistener.onAnimationEnd(mSlideLayout, AnimationKind.ANI_SCALE_SMALL);

                                int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);

                                if(mSlideLayout instanceof SlideMyAccountLayout)
                                    setBackground(GraphicUtils.getRoundCornerDrawable("#B3FFFFFF",new int[]{radius,radius,0,0}));
                                else
                                    setBackground(GraphicUtils.getRoundCornerDrawable("#80FFFFFF",new int[]{radius,radius,0,0}));

                            }
                        } )
                        .start();
            }
        }, delayTime);
    }

    /**
     * 뷰를 원래 사이즈로 스케일함
     * @param aniTime 밀리세컨드 초
     * @param delayTime 밀리세컨드 초
     */
    public void scaleOrigin(final int aniTime,int delayTime){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(mActivity.isFinishing()) return;

                final long aniKey= System.currentTimeMillis();
                mActivity.showAniProtectView(true,aniKey,"scaleOrigin");

                ViewPropertyAnimator animator = mSlideLayout.animate();
                if(getVisibility() == VISIBLE){
                    animator.translationY(0);
                }else{
                    animator.translationY(mOriginViewHeight);
                }
                animator.scaleX(1f)
                        .scaleY(1f)
                        .setDuration(aniTime)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                                int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);
                                setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,0,0}));
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mActivity.showAniProtectView(false,aniKey,"scaleOrigin");

                                //isScale = false;
                                mActionlistener.onAnimationEnd(mSlideLayout, AnimationKind.ANI_SCALE_ORIGIN);
                            }
                        } )
                        .start();
            }
        }, delayTime);
    }

    /**
     * 뷰를 바닥으로 내림
     * @param aniTime 밀리세컨드 초
     * @param delayTime 밀리세컨드 초
     */

    public void slidingDown(final int aniTime,int delayTime){


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mActivity.isFinishing()) return;

                final long aniKey= System.currentTimeMillis();
                mActivity.showAniProtectView(true,aniKey,"slidingDown");

                mSlideLayout.animate()
                        .translationY(mOriginViewHeight)
                        .setDuration(aniTime)
                        .setListener(new AnimatorListenerAdapter() {

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mActivity.showAniProtectView(false,aniKey,"slidingDown");

                                setVisibility(INVISIBLE);
                                mActionlistener.onAnimationEnd(mSlideLayout, AnimationKind.ANI_SLIDE_DOWN);
                                //if(isScale){
                                    scaleOrigin(0,0);
                                //}

                                //Logs.e("mSlideLayout.getHeight() : " + mSlideLayout.getHeight());
                                //Logs.e("mOriginViewHeight : " + mOriginViewHeight);
                                if(mLayoutBody != null && mLayoutBody.getHeight() < mOriginViewHeight){
                                    //Logs.e("mLayoutBody.getHeight() : " + mLayoutBody.getHeight());
                                    ViewGroup.LayoutParams paramsBody = mLayoutBody.getLayoutParams();
                                    paramsBody.height = mOriginViewHeight;
                                    mLayoutBody.setLayoutParams(paramsBody);
                                    mLayoutBody.requestLayout();

                                    //Logs.e("mSlideLayout Y : " + getY());
                                }

                            }
                        } );
            }
        }, delayTime);
    }

    /**
     * 뷰를 위로 올림
     * @param aniTime 밀리세컨드 초
     * @param delayTime 밀리세컨드 초
     */
    public void slidingUp(final int aniTime,int delayTime){

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mActivity.isFinishing()) return;

                setVisibility(VISIBLE);

                final long aniKey= System.currentTimeMillis();
                mActivity.showAniProtectView(true,aniKey,"slidingUp");

                mSlideLayout.animate()
                        .translationY(0)
                        .setDuration(aniTime)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                mActivity.showAniProtectView(false,aniKey,"slidingUp");
                                mActionlistener.onAnimationEnd(mSlideLayout, AnimationKind.ANI_SLIDE_UP);
                            }
                        } );
            }
        }, delayTime);

    }

    /**
     * 슬라이드 내부에서 각각 뷰들의 애니메이션을 주기위한 함수
     * @param view 애니메이션할 뷰
     * @param isShow true:슬라이드 내부로 나타나는 애니 , false : 슬라이드에서 사라지는 애니
     *
     */
    protected void startAnimateFadeInOut(final View view, final boolean isShow, final int duration, int delayTime, final Animation.AnimationListener listener) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (isShow) {
                    view.setVisibility(VISIBLE);
                }

                Animation anim;
                if (isShow) {
                    anim = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_in_transfer_view);
                } else {
                    anim = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_out_transfer_view);
                }
                anim.setDuration(duration);
                view.clearAnimation();
                view.startAnimation(anim);
                if(listener != null) {
                    view.getAnimation().setAnimationListener(listener);
                }else{
                    view.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            if (!isShow) {
                                view.setVisibility(INVISIBLE);
                            }
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                }

            }
        }, delayTime);
    }


//  private static final int MIN_SCREEN_DIP_HEIGHT = 600;
    /**
     * 현재 뷰의 시작을 바닥에 위치 시켜 놓아야할 필요가 있을 경우
     */
    public void initLayoutSetBottom(final boolean isBottom,final boolean isAdjustTitlePos){
        if(isBottom) setVisibility(INVISIBLE);
        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        int activityPixelHeight = GraphicUtils.getActivityHeight(getContext());
                        int activityDipHeight = (int)Utils.pixelToDp(getContext(),activityPixelHeight);
                        Logs.e("activityDipHeight : " + activityDipHeight);

                        mOriginViewHeight = getHeight();
                        mOriginViewYPos = (int)getY();

//                        if(activityDipHeight <= MIN_SCREEN_DIP_HEIGHT){//디자인 기본폰일경우
//                            mOriginViewHeight = getHeight();
//                            mOriginViewYPos = (int)getY();
//                        }else{
//                            //디자인 기본 사이즈인 360 * 480 dimen보다 큰 폰은 슬라이드뷰의 사이즈를 늘려준다.
//                            int difDipHeight = activityDipHeight - MIN_SCREEN_DIP_HEIGHT;
//                            Logs.e("onGlobalLayout - difDipHeight : " + difDipHeight);
//                            if(difDipHeight > 0){
//                                difDipHeight = difDipHeight/3;
//                            }
//                            Logs.e("onGlobalLayout - difDipHeight/3 : " + difDipHeight);
//
//
//                            int offsetPixel = (int)Utils.dpToPixel(getContext(),difDipHeight);
//
//                            Logs.e("onGlobalLayout - original height : " + getHeight());
//                            Logs.e("onGlobalLayout - offsetPixel : " + offsetPixel);
//
//                            mOriginViewHeight = getHeight() + offsetPixel;
//
//                            Logs.e("onGlobalLayout - new height : " + mOriginViewHeight);
//
//                            ViewGroup.LayoutParams params = mLayoutBody.getLayoutParams();
//                            params.height = mOriginViewHeight;
//                            mLayoutBody.setLayoutParams(params);
//                            mLayoutBody.invalidate();
//
//                            //늘어난 만큰 Y 포지션을 줄여준다.
//                            mOriginViewYPos = (int)getY()-offsetPixel;
//                        }
//
                        if(isAdjustTitlePos){
                            View view = getSlideView(CustomTitleLayout.class);
                            if(view != null){
                                ((CustomTitleLayout)view).changeTitlePosition(mOriginViewYPos);
                            }
                        }

                        if(isBottom) slidingDown(0,0);

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

    }



    public void onCancel(){
        //if(isScale) return;
        //TODO - 상속 받아서 각 레이아웃 별로 동작 추가
    }

    public View getSlideView(Class slideClass){
        RelativeLayout rootLayout = (RelativeLayout) (this.getParent());

        for(int i=0;i<rootLayout.getChildCount();i++){
            View view = rootLayout.getChildAt(i);
            if(slideClass.isInstance(view) ){
                return view;
            }
        }

        return null;
    }
}
