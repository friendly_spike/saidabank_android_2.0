package com.sbi.saidabank.activity.transaction.itransfer;

public interface OnITransferSearchActionListener {

    void checkRemitteeAccount(String bankCd,String detailBankCd, String accountNo,String recvNm);

    void checkGroupAccount(String groupIdx,String favorite,String groupCntText);

    void showPhoneRealNameDialog(String name,String telNo);
}
