package com.sbi.saidabank.activity.main2.rearrange;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.ItemTouchHelperViewHolder;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;

import java.util.ArrayList;
import java.util.Collections;


public class ReArrangeOpenBankAdapter extends RecyclerView.Adapter<ReArrangeOpenBankAdapter.OpenBankAccountHolder> implements ItemTouchHelperAdapter{
    private final Context mContext;
    private final int mGroupPos;
    private ArrayList<OpenBankAccountInfo> mAccountList;
    private final OnOpenBankItemDragListener mDragStartListener;


    //생성자
    public ReArrangeOpenBankAdapter(Context context, int groupPos, ArrayList<OpenBankAccountInfo> accountList, OnOpenBankItemDragListener dragStartListener){
        mContext = context;
        mGroupPos = groupPos;

        mAccountList = new ArrayList<OpenBankAccountInfo>();
        mAccountList.addAll(accountList);

        mDragStartListener = dragStartListener;
    }

    @NonNull
    @Override
    public OpenBankAccountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main2_openbank_rearrange_recyclerview_item, parent, false);

        int radius = (int) Utils.dpToPixel(mContext,12);
        itemView.setBackground(GraphicUtils.getRoundCornerDrawable("#eef7fc",new int[]{radius,radius,radius,radius},1,"#dfeef3"));

        return new OpenBankAccountHolder(mContext,itemView,this);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull OpenBankAccountHolder holder, int position) {
        OpenBankAccountInfo accountInfo = mAccountList.get(position);

        String shortAccNo = "";
        if(!TextUtils.isEmpty(accountInfo.ACNO) && accountInfo.ACNO.length() > 4){
            shortAccNo = "[" + accountInfo.ACNO.substring(accountInfo.ACNO.length()-4,accountInfo.ACNO.length()) + "]";
        }

        String prodName = accountInfo.PROD_NM;
        if(!TextUtils.isEmpty(prodName) && prodName.length() > 7){
            prodName = prodName.substring(0,7) + Const.ELLIPSIS;
        }
        holder.mTvAccountName.setText(prodName + shortAccNo);

        String BLNC = Utils.moneyFormatToWon(Double.valueOf(accountInfo.BLNC_AMT));
        holder.mTvAmount.setText(BLNC);

        holder.mIvholdbar.setTag(holder);
        holder.mIvholdbar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Logs.e("MotionEvent : " + event);
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    Logs.e("ACTION_DOWN ");
                    OpenBankAccountHolder holder = (OpenBankAccountHolder)v.getTag();
                    mDragStartListener.onStartDrag(mGroupPos,holder);
                }
                return false;
            }
        });

        //은행 아이콘 출력
        Uri iconUri = ImgUtils.getIconUri(mContext,accountInfo.RPRS_FNLT_CD,accountInfo.DTLS_FNLT_CD);
        if (iconUri != null)
            holder.mIvIcon.setImageURI(iconUri);
        else
            holder.mIvIcon.setImageResource(R.drawable.img_logo_xxx);
    }

    @Override
    public int getItemCount() {
        return mAccountList.size();
    }

    public void setAccountList(ArrayList<OpenBankAccountInfo> accountList){
        mAccountList.clear();
        mAccountList.addAll(accountList);
        notifyDataSetChanged();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mAccountList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        Logs.e("onItemMove : " + fromPosition + "->" + toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        Logs.e("onItemDismiss ");
    }

    @Override
    public void onItemMoveEnd() {
        Logs.e("onItemMoveEnd ");
        mDragStartListener.onEndDrag(mGroupPos,mAccountList);
    }

    //ViewHolder정의
    public static class OpenBankAccountHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        private Context mContext;
        private RelativeLayout mLayoutBody;
        private ImageView mIvIcon;
        private TextView  mTvAccountName;
        private TextView  mTvAmount;
        private ImageView mIvholdbar;

        ItemTouchHelperAdapter mHelperAdapter;

        public OpenBankAccountHolder(Context context, @NonNull View itemView, ItemTouchHelperAdapter adapter) {
            super(itemView);
            mContext = context;

            mLayoutBody = itemView.findViewById(R.id.layout_body);
            mIvIcon = itemView.findViewById(R.id.iv_icon);
            mTvAccountName = itemView.findViewById(R.id.tv_account_name);
            mTvAmount = itemView.findViewById(R.id.tv_amount);
            mIvholdbar = itemView.findViewById(R.id.iv_hold_bar);
            mHelperAdapter = adapter;
        }


        @Override
        public void onItemSelected() {
            Logs.e("OpenBankAccountHolder - onItemSelected");
            int radius = (int)Utils.dpToPixel(mContext,12f);
            mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#d0e0e9",new int[]{radius,radius,radius,radius},1,"#dfeef3"));
        }

        @Override
        public void onItemClear() {
            int radius = (int)Utils.dpToPixel(mContext,12f);
            mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#eef7fc",new int[]{radius,radius,radius,radius},1,"#dfeef3"));
            Logs.e("OpenBankAccountHolder - onItemClear");
            //드래그가 끝나면 여기로 온다.
            mHelperAdapter.onItemMoveEnd();
        }


    }


}