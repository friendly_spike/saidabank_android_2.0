package com.sbi.saidabank.activity;

import android.webkit.SslErrorHandler;

import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.web.OnWebActionListener;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseWebActivity extends BaseActivity implements OnWebActionListener {
    @Override
    public void showProgress() {
        showProgressDialog();
    }

    @Override
    public void dismissProgress() {
        dismissProgressDialog();
    }

    @Override
    public void openCamera() {

    }

    @Override
    public void loadUrl(String url, String params) {

    }

    @Override
    public void refreshWebView() {

    }

    @Override
    public void setNeedResumeRefresh() {

    }

    @Override
    public void closeWebView() {

    }


    @Override
    public void setCurrentPage(String url) {

    }

    @Override
    public void setFinishedPageLoading(boolean flag) {

    }

    @Override
    public void onWebViewSSLError(String url, String errorCd, SslErrorHandler handler) {

    }

    @Override
    public void getProfileImage(JSONObject json, String workType) throws JSONException {

    }
}
