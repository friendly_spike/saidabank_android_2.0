package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.appsflyer.AppsFlyerManager;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Saidabanking_android
 * <p>
 * Class: CompleteMemberActivity
 * Created by 950485 on 2018. 10. 25..
 * <p>
 * Description:신규재등록 완료를 위한 화면
 */
public class CompleteMemberActivity extends BaseActivity {

    private LinearLayout layoutBanner;
    private CommonUserInfo commonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_member);

        //현재 화면을 제외한 이전까지 진행했던 가입 화면은 모두 제거한다.
        ((SaidaApplication)getApplication()).allActivityFinish(this.getLocalClassName());

        initUX();
        requestBannerInfo();

        //다보깅-인트로의 서버선택을 위해 가입한 서버의 인덱스를 저장해둔다.
        if(BuildConfig.DEBUG){
            Prefer.setJoinDebugServerIndex(this, SaidaUrl.serverIndex);
            //백업해둔 패턴,지문,핀 값을 지워둔다.
            Prefer.setDebugBackupAuthStatus(this,"");
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {

        if (DataUtil.isNull(getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO)))
            return;



        commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        if (DataUtil.isNotNull(commonUserInfo.getName())) {
            TextView textDesc = (TextView) findViewById(R.id.textview_complete_member_desc);
            String desc01 = String.format(getString(R.string.account_customer_desc_01), commonUserInfo.getName());
            textDesc.setText(desc01);
        }

        //가대출 여부 출력할지 고민
        String existLoanText = commonUserInfo.getCRDT_LOAN_YN();
        if(!TextUtils.isEmpty(existLoanText) && existLoanText.equals("Y")){
            findViewById(R.id.layout_exist_lone).setVisibility(View.VISIBLE);
        }




        layoutBanner = (LinearLayout) findViewById(R.id.layout_ads_area);

        LinearLayout layoutFingerprint = (LinearLayout) findViewById(R.id.layout_use_fingerprint_complete_member);
        Boolean isFingerpringt = Prefer.getFidoRegStatus(CompleteMemberActivity.this);
        if (isFingerpringt) {
            layoutFingerprint.setVisibility(View.VISIBLE);
        } else {
            if (StonePassUtils.hasFingerprintDevice(this) == 0) {
                ImageView imageView = findViewById(R.id.imageview_check_finger);
                imageView.setImageResource(R.drawable.btn_check_off);
                TextView textCheckFinger = findViewById(R.id.textview_check_finger);
                textCheckFinger.setTextColor(getResources().getColor(R.color.color888888));
                textCheckFinger.setText(R.string.msg_no_use_fingerprint);
                layoutFingerprint.setVisibility(View.VISIBLE);
            }
        }

        Button btnOK = (Button) findViewById(R.id.btn_ok_complete_member);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                commonUserInfo.setEntryPoint((Prefer.getFidoRegStatus(CompleteMemberActivity.this)) ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                Intent intent = new Intent(CompleteMemberActivity.this, PatternAuthActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
                finish();
            }
        });

        Button btnJoinAccount = (Button) findViewById(R.id.btn_join_account);
        btnJoinAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefer.setJoinAccount(CompleteMemberActivity.this, true);
                commonUserInfo.setEntryPoint((Prefer.getFidoRegStatus(CompleteMemberActivity.this)) ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                Intent intent = new Intent(CompleteMemberActivity.this, PatternAuthActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
                finish();
            }
        });

        if (commonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN) {
            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER + "fin", Const.EMPTY);
        }
    }

    /**
     * requestBannerInfo : 배너조회
     */
    private void requestBannerInfo() {

        HashMap param = new HashMap();
        param.put("BNR_LOC_DVCD", "cmm0010300A03");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                // 응답 데이터가 없을 경우 종료 처리
                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                } else {
                    try {
                        JSONObject object = new JSONObject(ret);
                        if (DataUtil.isNull(object)) {
                            showErrorMessage(getString(R.string.msg_debug_err_response));
                            return;
                        }

                        JSONArray array = object.optJSONArray("REC");
                        if (DataUtil.isNull(array)) return;

                        JSONObject banner = array.getJSONObject(0);
                        String imgUrl = banner.optString("IMG_URL_ADDR");
                        if (DataUtil.isNotNull(imgUrl)) {
                            final String linkUrl = banner.optString("LINK_URL_ADDR");
                            ImageView ivBanner = (ImageView) findViewById(R.id.iv_ads);
                            ivBanner.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (DataUtil.isNotNull(linkUrl)) {
                                        String url = SaidaUrl.getBaseWebUrl() + "/" + linkUrl + ".act";
                                        boolean status = Prefer.getFidoRegStatus(CompleteMemberActivity.this);
                                        commonUserInfo.setEntryStart(status ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
                                        commonUserInfo.setEntryPoint(status ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
                                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                                        Intent intent = new Intent(CompleteMemberActivity.this, PatternAuthActivity.class);
                                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                                        startActivity(intent);
                                    }
                                }
                            });
                            String baseUrl = SaidaUrl.getBaseWebUrl();
                            baseUrl += imgUrl;
                            Picasso.with(CompleteMemberActivity.this)
                                    .load(baseUrl)
                                    .fit()
                                    .memoryPolicy(MemoryPolicy.NO_STORE)
                                    .into(ivBanner);

                            layoutBanner.setVisibility(View.VISIBLE);
                        } else {
                            layoutBanner.setVisibility(View.GONE);
                        }

                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                }
            }
        });
    }
}
