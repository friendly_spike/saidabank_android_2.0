package com.sbi.saidabank.activity.transaction;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kakao.kakaolink.v2.KakaoLinkResponse;
import com.kakao.kakaolink.v2.KakaoLinkService;
import com.kakao.message.template.ButtonObject;
import com.kakao.message.template.ContentObject;
import com.kakao.message.template.FeedTemplate;
import com.kakao.message.template.LinkObject;
import com.kakao.network.ErrorResult;
import com.kakao.network.callback.ResponseCallback;
import com.kakao.util.helper.log.Logger;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.TransferPhoneResultInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: TransferPhoneCompleteActivity
 * Created by 950485 on 2019. 02. 21..
 * <p>
 * Description: 휴대폰번호로 이체 완료 화면
 */

public class TransferPhoneCompleteActivity extends BaseActivity {
    private TransferPhoneResultInfo mPhoneResultInfo;
    private String mKakaoMsg;

    private Dialog    dlgVerifyTransfer;
    private ImageView mImageFavorite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_phone_complete);

        getExtra();
        initUX();
        showVerifyTransferPhone();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        Intent intent = getIntent();
        mPhoneResultInfo = (TransferPhoneResultInfo) intent.getSerializableExtra(Const.INTENT_TRANSFER_PHONE_RET);
        mKakaoMsg = intent.getStringExtra(Const.INTENT_TRANSFER_PHONE_KAKAO_MSG);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        TextView textDesc01 = (TextView) findViewById(R.id.textview_phone_transfer_desc01);
        String desc01 = getString(R.string.msg_fail_phone_transfer_desc01);
        desc01 = desc01.replaceAll(" ", "\u00A0");
        textDesc01.setText(desc01);

        TextView textDesc02 = (TextView) findViewById(R.id.textview_phone_transfer_desc02);
        String desc02 = getString(R.string.msg_fail_phone_transfer_desc02);
        desc02 = desc02.replaceAll(" ", "\u00A0");
        textDesc02.setText(desc02);

        String FAVO_ACCO_YN = mPhoneResultInfo.getFAVO_ACCO_YN();
        RelativeLayout btnFavorite = (RelativeLayout) findViewById(R.id.layout_favorite_phone);
        mImageFavorite = (ImageView) findViewById(R.id.imageview_favorite_phone);
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN)) {
            mImageFavorite.setImageResource(R.drawable.btn_favorite_on);
        } else {
            mImageFavorite.setImageResource(R.drawable.btn_favorite_off);
        }

        btnFavorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestFavirtePhone();
            }
        });

        String TRAM = mPhoneResultInfo.getTRAM();
        TextView textAmount = (TextView) findViewById(R.id.textview_transfer_amount);
        if (!TextUtils.isEmpty(TRAM)) {
            TRAM = Utils.moneyFormatToWon(Double.parseDouble(TRAM));
            textAmount.setText(TRAM);
        }

        String TRNF_FEE = mPhoneResultInfo.getTRNF_FEE();
        TextView textFee = (TextView) findViewById(R.id.textview_transfer_fee);
        if (!TextUtils.isEmpty(TRNF_FEE)) {
            TRNF_FEE = Utils.moneyFormatToWon(Double.parseDouble(TRNF_FEE));
            textFee.setText(TRNF_FEE + " " + getString(R.string.won));
        }

        String recieveName = mPhoneResultInfo.getMNRC_TRGT_DEPR_NM();
        TextView textRecieveName = (TextView) findViewById(R.id.textview_receive_name);
        if (!TextUtils.isEmpty(recieveName)) {
            textRecieveName.setText(recieveName);
        }

        String receivePhoneNumber = mPhoneResultInfo.getMNRC_CPNO();
        TextView textReceivePhoneNuber = (TextView) findViewById(R.id.textview_receive_phonenumber);
        if (!TextUtils.isEmpty(receivePhoneNumber)) {
            receivePhoneNumber = PhoneNumberUtils.formatNumber(receivePhoneNumber, Locale.getDefault().getCountry());
            textReceivePhoneNuber.setText(receivePhoneNumber);
        }

        String WTCH_ACNO = mPhoneResultInfo.getWTCH_ACNO();
        if (!TextUtils.isEmpty(WTCH_ACNO) && WTCH_ACNO.length() > 4) {
            WTCH_ACNO = "[" + WTCH_ACNO.substring(WTCH_ACNO.length() - 4, WTCH_ACNO.length()) + "]";
        }
        String ACCO_ALS = mPhoneResultInfo.getACCO_ALS();
        String PROD_NM = mPhoneResultInfo.getPROD_NM();
        TextView textAccount = (TextView) findViewById(R.id.textview_withdraw_account);
        if (!TextUtils.isEmpty(ACCO_ALS)) {
            textAccount.setText(ACCO_ALS + WTCH_ACNO);
        } else {
            textAccount.setText(PROD_NM + WTCH_ACNO);
        }


        String MNRC_ACCO_MRK_CNTN = mPhoneResultInfo.getMNRC_ACCO_MRK_CNTN();
        TextView textMsg = (TextView) findViewById(R.id.textview_send_msg);
        if (!TextUtils.isEmpty(MNRC_ACCO_MRK_CNTN)) {
            textMsg.setText(MNRC_ACCO_MRK_CNTN);
        }

        LinearLayout layoutDesc = (LinearLayout) findViewById(R.id.layout_phone_complete_desc);
        String MNRC_ACNO = mPhoneResultInfo.getMNRC_ACNO();
        if (!TextUtils.isEmpty(MNRC_ACNO)) {
            layoutDesc.setVisibility(View.GONE);
        }

        Button btnContinue = (Button) findViewById(R.id.btn_continue);
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ITransferDataMgr.getInstance().clearTransferData();

                Intent intent = new Intent(TransferPhoneCompleteActivity.this, ITransferSelectReceiverActivity.class);
                startActivity(intent);
                finish();
            }
        });

        Button btnConfirm = (Button) findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    /**
     * 카카오톡, sms 보내기 화면 표시
     */
    void showVerifyTransferPhone() {
        hideVerifyTransferPhone();

        dlgVerifyTransfer = new Dialog(this);
        dlgVerifyTransfer.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dlgVerifyTransfer.setContentView(R.layout.dialog_transfer_phone_verify);
        View view = dlgVerifyTransfer.getWindow().getDecorView();
        view.setBackgroundResource(android.R.color.transparent);

        String MNRC_TRGT_DEPR_NM = mPhoneResultInfo.getMNRC_TRGT_DEPR_NM();
        MNRC_TRGT_DEPR_NM = String.format(getString(R.string.msg_complete_phone_transfer_desc_01), MNRC_TRGT_DEPR_NM);

        String sourceString = MNRC_TRGT_DEPR_NM + getString(R.string.msg_complete_phone_transfer_desc_02);
        //String sourceString = "<b>" + MNRC_TRGT_DEPR_NM + "</b>" + getString(R.string.msg_complete_phone_transfer_desc_02);
        TextView textDesc = (TextView) dlgVerifyTransfer.findViewById(R.id.textview_complete_phone_transfer_desc);
        textDesc.setText(Html.fromHtml(sourceString));

        RelativeLayout btnKakaotalk = (RelativeLayout) dlgVerifyTransfer.findViewById(R.id.layout_send_kakaotalk);
        btnKakaotalk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TransferPhoneCompleteActivity.this.isFinishing())
                    return;

                requestKakao();
            }
        });

        RelativeLayout btnSms = (RelativeLayout) dlgVerifyTransfer.findViewById(R.id.layout_send_sms);
        btnSms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TransferPhoneCompleteActivity.this.isFinishing())
                    return;

                requestSms();
            }
        });

        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        WindowManager.LayoutParams lp = dlgVerifyTransfer.getWindow().getAttributes();
        lp.width = size.x;
        dlgVerifyTransfer.getWindow().setAttributes(lp);
        dlgVerifyTransfer.setCancelable(false);
        dlgVerifyTransfer.show();
    }

    void hideVerifyTransferPhone() {
        if (dlgVerifyTransfer != null) {
            dlgVerifyTransfer.dismiss();
            dlgVerifyTransfer = null;
        }
    }

    private void requestKakao() {
        if (Utils.isInstallApp(TransferPhoneCompleteActivity.this, "com.kakao.talk") == false) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("market://search?q=pname:com.kakao.talk"));
            startActivity(intent);
            hideVerifyTransferPhone();
            return;
        }

        String TRNF_KEY_VAL = mPhoneResultInfo.getTRNF_KEY_VAL();
        if (TextUtils.isEmpty(TRNF_KEY_VAL)) {
            return;
        }

        String url = SaidaUrl.getBaseMWebUrl();
        String linkUrl = url + "/" + "tra0010100A01.act?TRNF_KEY_VAL=" + TRNF_KEY_VAL;
        /*
        TextTemplate params = TextTemplate.newBuilder(
            mKakaoMsg,
            LinkObject.newBuilder()
            .setWebUrl(linkUrl)
            .setMobileWebUrl(linkUrl)
            .build()
        )
        .setButtonTitle("입금받기")
        .build();

        KakaoLinkService.getInstance().sendDefault(this, params, serverCallbackArgs, new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Logger.e(errorResult.toString());
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {
                hideVerifyTransferPhone();
            }
        });
        */
        Map<String, String> callbackParameters = new HashMap<>();
        String imgUrl = url + "/_pub/img/share/share-01.png";
        FeedTemplate params1 = FeedTemplate
                .newBuilder(ContentObject.newBuilder(mKakaoMsg,
                        imgUrl,
                        LinkObject.newBuilder().setWebUrl(linkUrl)
                                .setMobileWebUrl(linkUrl).build())
                        .build())
                .addButton(new ButtonObject("입금받기", LinkObject.newBuilder().setWebUrl(linkUrl).setMobileWebUrl(linkUrl).build()))
                .build();

        KakaoLinkService.getInstance().sendDefault(this, params1, callbackParameters, new ResponseCallback<KakaoLinkResponse>() {
            @Override
            public void onFailure(ErrorResult errorResult) {
                Logger.e(errorResult.toString());
            }

            @Override
            public void onSuccess(KakaoLinkResponse result) {

            }
        });

        hideVerifyTransferPhone();
    }

    private void requestSms() {
        String WTCH_ACNO = mPhoneResultInfo.getWTCH_ACNO();
        String TRN_SRNO = mPhoneResultInfo.getTRN_SRNO();

        if (TextUtils.isEmpty(WTCH_ACNO) || TextUtils.isEmpty(TRN_SRNO)) {
            hideVerifyTransferPhone();
            return;
        }

        Map param = new HashMap();
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("TRN_SRNO", TRN_SRNO);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0020500A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }
                    hideVerifyTransferPhone();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private Map<String, String> getServerCallbackArgs() {
        Map<String, String> callbackParameters = new HashMap<>();
        callbackParameters.put("user_id", "1234");
        callbackParameters.put("title", "프로방스 자동차 여행 !@#$%");
        return callbackParameters;
    }

    /**
     * 즐겨찾기계좌관리 요청
     */
    private void requestFavirtePhone() {
        if (mPhoneResultInfo == null)
            return;

        final String FAVO_ACCO_YN = mPhoneResultInfo.getFAVO_ACCO_YN();
        String MNRC_TRGT_DEPR_NM = mPhoneResultInfo.getMNRC_TRGT_DEPR_NM();
        String MNRC_CPNO = mPhoneResultInfo.getMNRC_CPNO();

        if (TextUtils.isEmpty(MNRC_CPNO))
            return;

        Map param = new HashMap();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN))
            param.put("REG_STCD", "1");
        else
            param.put("REG_STCD", "0");

        param.put("CNTP_ACCO_DEPR_NM", MNRC_TRGT_DEPR_NM);
        param.put("MNRC_CPNO", MNRC_CPNO);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN)) {
                        mImageFavorite.setImageResource(R.drawable.btn_favorite_off);
                        mPhoneResultInfo.setFAVO_ACCO_YN(Const.REQUEST_WAS_NO);
                    } else {
                        mImageFavorite.setImageResource(R.drawable.btn_favorite_on);
                        mPhoneResultInfo.setFAVO_ACCO_YN(Const.REQUEST_WAS_YES);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}
