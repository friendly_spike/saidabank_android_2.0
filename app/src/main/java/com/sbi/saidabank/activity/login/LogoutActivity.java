package com.sbi.saidabank.activity.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.solution.v3.V3Manager;

import java.util.HashMap;

/**
 * LogoutActivity : 로그아웃
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class LogoutActivity extends BaseActivity {
    private Button btnConfirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout);

        int logoutType = getIntent().hasExtra(Const.INTENT_LOGOUT_TYPE) ? getIntent().getIntExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER) : Const.LOGOUT_TYPE_USER;

        TextView tvMsg = findViewById(R.id.tv_msg);
        if (logoutType == Const.LOGOUT_TYPE_TIMEOUT) {
            tvMsg.setText("장시간 미사용으로\n자동 로그아웃 되었습니다.");
        }

        btnConfirm = findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(LogoutActivity.this, IntroActivity.class);
//                startActivity(intent);
                V3Manager.getInstance(getApplicationContext()).stopV3MobilePlus();
                ActivityCompat.finishAffinity(LogoutActivity.this);
                finish();
            }
        });

        LogoutTimeChecker.clearInstance();

        if (logoutType != Const.LOGOUT_TYPE_FORCE) {
            showProgressDialog();
            HttpUtils.sendHttpTask(WasServiceUrl.CMM0010900A01.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
                @Override
                public void endHttpRequest(String ret) {
                    dismissProgressDialog();
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        btnConfirm.performClick();
    }
}
