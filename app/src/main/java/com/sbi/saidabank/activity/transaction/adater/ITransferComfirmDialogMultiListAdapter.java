package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.TransferManager;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferVerifyInfo;

public class ITransferComfirmDialogMultiListAdapter extends RecyclerView.Adapter<ITransferComfirmDialogMultiListAdapter.ReceiptInfoListViewHolder> {
    private Context    context;


    public ITransferComfirmDialogMultiListAdapter(Context context) {
        this.context = context;
    }

    public class ReceiptInfoListViewHolder extends RecyclerView.ViewHolder {

        public TextView        textName;
        public TextView        textAmount;
        public TextView        textFee;
        public TextView        textBank;
        public TextView        textAccount;
        public TextView        textDate;
        public View            viewLine;

        public ReceiptInfoListViewHolder(Context context, View view, int viewType) {
            super(view);
            textName = (TextView) view.findViewById(R.id.textview_name_transfer);
            textAmount = (TextView) view.findViewById(R.id.textview_amount_transfer);
            textFee = (TextView) view.findViewById(R.id.textview_free_transfer);
            textBank = (TextView) view.findViewById(R.id.texview_bank_transfer);
            textAccount = (TextView) view.findViewById(R.id.texview_account_transfer);
            textDate = (TextView) view.findViewById(R.id.textvew_send_date);
            viewLine = view.findViewById(R.id.v_line);
        }
    }

    @Override
    public ITransferComfirmDialogMultiListAdapter.ReceiptInfoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_confirm_multi_transfer_list_item, parent, false);
        ReceiptInfoListViewHolder viewHolder = new ReceiptInfoListViewHolder(context, itemView, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ReceiptInfoListViewHolder viewHolder, int position) {

        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position);

        String name = remitteeInfo.getRECV_NM();
        if (!TextUtils.isEmpty(name))
            viewHolder.textName.setText(name);

        String amount = remitteeInfo.getTRN_AMT();
        if (!TextUtils.isEmpty(amount)) {
            amount = Utils.moneyFormatToWon(Double.valueOf(amount));
            viewHolder.textAmount.setText(amount + " " + context.getString(R.string.won));
        }

        String free = remitteeInfo.getFEE();
        if (!TextUtils.isEmpty(free)) {
            free = Utils.moneyFormatToWon(Double.valueOf(free));
            viewHolder.textFee.setText(free + " " + context.getString(R.string.won));
        }

        String MNRC_BANK_NM = remitteeInfo.getMNRC_BANK_NM();
        if (!TextUtils.isEmpty(MNRC_BANK_NM))
            viewHolder.textBank.setText(MNRC_BANK_NM);

        String CNTP_FIN_INST_CD = remitteeInfo.getCNTP_FIN_INST_CD();
        String CNTP_BANK_ACNO = remitteeInfo.getCNTP_BANK_ACNO();
        String account;
        if (!TextUtils.isEmpty(CNTP_FIN_INST_CD) &&
            ("000".equalsIgnoreCase(CNTP_FIN_INST_CD) || "028".equalsIgnoreCase(CNTP_FIN_INST_CD)))
            account = CNTP_BANK_ACNO.substring(0, 5) + "-" + CNTP_BANK_ACNO.substring(5, 7) + "-" + CNTP_BANK_ACNO.substring(7, CNTP_BANK_ACNO.length());
        else
            account = CNTP_BANK_ACNO;

        if (!TextUtils.isEmpty(account))
            viewHolder.textAccount.setText(account);

        String TRNF_DVCD = remitteeInfo.getTRNF_DVCD();
        if ("1".equalsIgnoreCase(TRNF_DVCD)) {
            viewHolder.textDate.setText(R.string.Immediately);
        } else if ("3".equalsIgnoreCase(TRNF_DVCD)) {
            String TRNF_DMND_DT = remitteeInfo.getTRNF_DMND_DT();
            String TRNF_DMND_TM = remitteeInfo.getTRNF_DMND_TM();
            if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                String year = TRNF_DMND_DT.substring(0, 4);
                String month = TRNF_DMND_DT.substring(4, 6);
                String day = TRNF_DMND_DT.substring(6, 8);
                String hour = TRNF_DMND_TM.substring(0, 2);
                String date = year + "." + month + "." + day + " " + hour + ":00";
                viewHolder.textDate.setText(date);
            }
        } else if ("2".equalsIgnoreCase(TRNF_DVCD)) {
            String TRNF_DMND_DT = remitteeInfo.getTRNF_DMND_DT();
            String TRNF_DMND_TM = remitteeInfo.getTRNF_DMND_TM();
            if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                String year = TRNF_DMND_DT.substring(0, 4);
                String month = TRNF_DMND_DT.substring(4, 6);
                String day = TRNF_DMND_DT.substring(6, 8);
                String hour = TRNF_DMND_TM.substring(0, 2);
                String date = year + "-" + month + "-" + day + " " + hour + ":00";
                viewHolder.textDate.setText(date);
            } else {
                viewHolder.textDate.setText(context.getString(R.string.msg_after_3_hour));
//                String SVC_DVCD = remitteeInfo.getSVC_DVCD();
//                if ("1".equalsIgnoreCase(SVC_DVCD)) {
//                    viewHolder.textDate.setText(context.getString(R.string.Immediately));
//                } else {
//                    viewHolder.textDate.setText(context.getString(R.string.msg_after_3_hour));
//                }
            }
        } else if ("5".equalsIgnoreCase(TRNF_DVCD)) {
            String TRNF_DMND_DT = remitteeInfo.getTRNF_DMND_DT();
            String TRNF_DMND_TM = remitteeInfo.getTRNF_DMND_TM();
            if (!TextUtils.isEmpty(TRNF_DMND_DT) && !TextUtils.isEmpty(TRNF_DMND_TM)) {
                String year = TRNF_DMND_DT.substring(0, 4);
                String month = TRNF_DMND_DT.substring(4, 6);
                String day = TRNF_DMND_DT.substring(6, 8);
                String hour = TRNF_DMND_TM.substring(0, 2);
                String date = year + "-" + month + "-" + day + " " + hour + ":00";
                viewHolder.textDate.setText(date);
            }
        }

        if(position == ITransferDataMgr.getInstance().getRemitteInfoArraySize() -1){
            viewHolder.viewLine.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return ITransferDataMgr.getInstance().getRemitteInfoArraySize();
    }
}
