package com.sbi.saidabank.activity.transaction.safedeal;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.contacts.SafeDealContactsInfoAdapter;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 전화번호 선택 슬라이드 리스트 화면
 */
public class SlideListContactLayout extends SlideBaseLayout implements View.OnClickListener,View.OnTouchListener{


    private ArrayList<ContactsInfo> mContactsList;
    private SafeDealContactsInfoAdapter mListAdapter;

    private LinearLayout mContactListLayout;
    private LinearLayout mEmptyListLayout;

    private ListView mListContacts;
    private EditText mEditSearch;
    private TextView mTextSearchCount;
    private TextView mTvEmpty;

    public SlideListContactLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideListContactLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        setTag("SlideListContactLayout");
        //메모리 할당.
        mContactsList = new ArrayList<>();

        View layout = View.inflate(context, R.layout.layout_safedeal_slide_list_contact, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);

        layout.findViewById(R.id.layout_movebar).setOnTouchListener(this);

        LinearLayout edittextLayout = layout.findViewById(R.id.layout_edittext);
        int radius_et = (int) Utils.dpToPixel(getContext(),7f);
        Drawable et_drawable = GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.white),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5));
        edittextLayout.setBackground(et_drawable);

        mEditSearch = layout.findViewById(R.id.et_search);
        mEditSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean digitsOnly = TextUtils.isDigitsOnly(s.toString());
                if (digitsOnly) {
                    mListAdapter.getPhoneFilter().filter(s.toString());
                } else{
                    mListAdapter.getNameFilter().filter(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mTextSearchCount = layout.findViewById(R.id.tv_count);

        mContactListLayout = layout.findViewById(R.id.layout_contact_list);
        mEmptyListLayout = layout.findViewById(R.id.layout_empty_list);

        mTvEmpty = layout.findViewById(R.id.tv_empty);

        //주소록 동기화 버튼
        findViewById(R.id.layout_research).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                syncListContacts();
                mEditSearch.setText("");
            }
        });

        mListAdapter = new SafeDealContactsInfoAdapter(getContext(), mContactsList, mTextSearchCount);
        mListContacts = layout.findViewById(R.id.listview);
        mListContacts.setAdapter(mListAdapter);
        mListContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                final ContactsInfo contactsInfo = mListAdapter.getFilterList().get(position);
                if (contactsInfo == null)
                    return;

                View slideView = getSlideView(SlideReceiverPhoneNumLayout.class);
                if(slideView != null){
                    ((SlideReceiverPhoneNumLayout)slideView).setPhoneNum(contactsInfo.getTLNO());
                }

                slidingDown(250,0);
                hideKeypad();

            }
        });

        int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);
        setBackground(GraphicUtils.getRoundCornerDrawable("#F5F5F5",new int[]{radius,radius,0,0}));
        initLayoutSetBottom(true,false);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void slidingUp(int aniTime, int delayTime) {
        mListContacts.setSelection(0);
        mEditSearch.setText("");
        super.slidingUp(aniTime, delayTime);
    }

    @Override
    public void slidingDown(int aniTime, int delayTime) {
        super.slidingDown(aniTime, delayTime);
        //뷰가 내려가면 키보드도 내려간다.
        Utils.hideKeyboard(getContext(),mEditSearch);
    }

    @Override
    public void onCancel() {
        //super.onCancel();
        //if(isScale) return;

        slidingDown(250,0);

        View slideView = getSlideView(SlideReceiverPhoneNumLayout.class);
        if(slideView != null){
            ((SlideReceiverPhoneNumLayout)slideView).setIsOpenContactList(false);
        }
    }



    private float startdY;
    private float moveY;
    private int   startViewHeight;
    private boolean isMove;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() != R.id.layout_movebar){
            return false;
        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_DOWN - y : " + event.getRawY());
                startdY = event.getRawY();
                isMove = false;
                startViewHeight = mLayoutBody.getHeight();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY =  startdY - event.getRawY();
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - moveY : " + moveY);

                if(moveY == 0){
                    return false;
                }

                //확장하려고 하지만 이미 모두 확장했당.
                if(moveY > 0 && mScreenHeight == startViewHeight){
                    return false;
                }

//                if(moveY < 0  && (mOriginViewHeight > startViewHeight + moveY) ){
//                    return false;
//                }

                isMove = true;
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - isMove : " + isMove);
                ViewGroup.LayoutParams params = mLayoutBody.getLayoutParams();
                params.height = startViewHeight + (int)moveY;
                mLayoutBody.setLayoutParams(params);
                mLayoutBody.requestLayout();

                break;

            case MotionEvent.ACTION_UP:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_UP - y : " + event.getRawY());
                if(event.getRawY() > mOriginViewYPos){
                    slidingDown(250,0);
                    return false;
                }

                if(isMove && moveY != 0){
                    int toSize = 0;
                    if(moveY > 0){ //확장
                        if(moveY > 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }else if(moveY < 0){ //복귀
                        moveY = moveY * -1;
                        if(moveY < 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }

                    AniUtils.changeViewHeightSizeAnimation(mLayoutBody,toSize,250);
                }
                isMove = false;
                break;

            default:
                return false;
        }
        return false;
    }

    /**
     * 저장된 주소록 파일에서 등록된 휴대폰 리스트 획득
     */
    public void getListContacts() {
        if (mContactsList != null)
            mContactsList.clear();

        StringBuffer sb = new StringBuffer("");
        try {
            File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            String path = storageDir + "/" + Const.CONTACTS_INFO_PATH;
            FileInputStream fIn = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fIn);
            BufferedReader buffreader = new BufferedReader(isr);
            String readString = buffreader.readLine();
            while (readString != null) {
                sb.append(readString);
                readString = buffreader.readLine();
            }
            isr.close();

            if (!TextUtils.isEmpty(sb)) {
                try {
                    JSONObject object = new JSONObject(sb.toString());
                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null)
                        return;

                    //ArrayList<ContactsInfo> listContacts = new ArrayList<>();
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);

                        mContactsList.add(contactsInfo);
                    }
                    showContactsList();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }

        } catch (IOException e) {
            Logs.printException(e);
        }
    }

    public void showContactsList() {
        Logs.e("showContactsList - mContactsList.size() : " + mContactsList.size());
        if (mContactsList == null || mContactsList.size() < 1) {
            mContactListLayout.setVisibility(View.GONE);
            mEmptyListLayout.setVisibility(View.VISIBLE);
            mTvEmpty.setText("저장된 연락처가 없습니다.");

            String msg = getContext().getString(R.string.contacts_result_search_count, "0");
            Utils.setTextWithSpan(mTextSearchCount, msg, "0", Typeface.BOLD,"#000000");
        } else {
            mListAdapter.setArrayList(mContactsList);

            mContactListLayout.setVisibility(View.VISIBLE);
            mEmptyListLayout.setVisibility(View.GONE);

            String msg = getContext().getString(R.string.contacts_result_search_count, String.valueOf(mContactsList.size()));
            Utils.setTextWithSpan(mTextSearchCount, msg, String.valueOf(mContactsList.size()), Typeface.BOLD,"#000000");
        }

        slidingUp(250,0);
    }

    /**
     * 폰에 등록된 리스트 획득 후 동기화 요청
     */
    public void syncListContacts() {

        ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();

        ContactsInfo contactsInfo;
        Cursor contactCursor = null;

        String name;
        String phonenumber;

        try {
            Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            String[] projection = new String[] {
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_ID};

            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

            contactCursor = getContext().getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
            if (contactCursor.moveToFirst()) {
                do {
                    phonenumber = contactCursor.getString(1);
                    if (phonenumber.length() <= 0)
                        continue;

                    name = contactCursor.getString(2);
                    phonenumber = phonenumber.replaceAll("-", "");
                    Logs.e("phonenumber : " + phonenumber);
                    if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                        continue;
                    }

                    // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 핸드폰 번호인 경우
                    if (!TextUtils.isEmpty(name) && listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                        contactsInfo = new ContactsInfo();
                        contactsInfo.setTLNO(phonenumber);
                        contactsInfo.setFLNM(name);
                        listContactsInPhonebook.add(contactsInfo);
                    }
                } while (contactCursor.moveToNext());
            }
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            if (contactCursor != null) {
                contactCursor.close();
            }

            requestCheckContacts(listContactsInPhonebook);
        }
    }

    /**
     * 휴대전화번호로 고객정보조회
     * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
     */
    private void requestCheckContacts(final ArrayList<ContactsInfo> listContactsInPhonebook) {

        Map param = new HashMap();
        JSONArray jsonArray = new JSONArray();
        for (int index = 0; index < listContactsInPhonebook.size(); index++) {
            ContactsInfo contactsInfo = listContactsInPhonebook.get(index);
            String TLNO = contactsInfo.getTLNO();
            if (TextUtils.isEmpty(TLNO))
                continue;

            try {
                JSONObject itemObject = new JSONObject();
                itemObject.put("TLNO", TLNO);

                String FLNM = contactsInfo.getFLNM();
                if (TextUtils.isEmpty(FLNM)) FLNM = "";
                itemObject.put("FLNM", FLNM);

                jsonArray.put(itemObject);
            } catch (JSONException e) {
            }
        }
        //폰에 저장된 연락처가 없으면 동기화 하지 않는다.
        if(jsonArray.length() == 0 ){
            return;
        }

        param.put("REC_IN_TLNO", jsonArray);
        mActionlistener.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                mActionlistener.dismissProgressDialog();
                Logs.i("TRA0020200A01 : " + ret);
                if(mActionlistener.onCheckHttpError(ret,false))   return;


                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null) {
                        mContactsList = listContactsInPhonebook;
                        return;
                    }

                    File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);

                    if (storageDir != null && !storageDir.exists())
                        if(!storageDir.mkdir()) return;

                    String path =  storageDir + "/" +  Const.CONTACTS_INFO_PATH;
                    try {
                        FileWriter fw = new FileWriter(path, false);
                        fw.write(ret);
                        fw.flush();
                        fw.close();
                    } catch(Exception e){
                        //e.printStackTrace();
                    }

                    if (mContactsList != null)
                        mContactsList.clear();
                    else
                        mContactsList = new ArrayList<>();

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);

                        mContactsList.add(contactsInfo);
                    }
                    showContactsList();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    public void hideKeypad(){

        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEditSearch.getWindowToken(), 0);
        mEditSearch.clearFocus();

    }
}
