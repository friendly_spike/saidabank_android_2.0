package com.sbi.saidabank.activity.transaction.itransfer.multi;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferMultiActionListener;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ViewHolderRemitte extends RecyclerView.ViewHolder {
    private Context mContext;
    private OnITransferMultiActionListener mListener;


    private RelativeLayout mLayoutBody;
    private ImageView mIvIcon;
    private RelativeLayout mLayoutDelIcon;
    private TextView mTvRcvName;
    private TextView mTvAccountName;

    private LinearLayout mLayoutOpenMoreInfo;
    private ImageView mIvOpenMoreInfo;

    private RelativeLayout mLayoutHangleMoney;
    private TextView mTvHangleMoney;
    private LinearLayout mLayoutInputAmount;
    private EditText mEtInputAmount;
    private TextView mTvWon;
    private RelativeLayout mLayoutLimit;
    private TextView mTvLimit;
    private TextView mTvErrLimitText;

    //상세정보
    private LinearLayout   mLayoutDetailInfo;
    private RelativeLayout mLayoutLine1Info;
    private RelativeLayout mLayoutLine2Info;
    private RelativeLayout mLayoutLine3Info;
    private RelativeLayout mLayoutLine4Info;

    private EditText        mEtDispRcvAccText;
    private EditText        mEtDispMyAccText;
    private EditText        mEtMemoText;

    private ImageView        mIvDispRcvAccErase;
    private ImageView        mIvDispMyAccErase;
    private ImageView        mIvMemoErase;

    private LinearLayout     mLayoutDisplaySendDate;
    private TextView         mTvSendDateText;
    private ImageView        mIvSendDateDropDown;
    private ImageView        mIvSendDateErase;

    //보앨일시 툴팁관련
    private RelativeLayout   mLayoutTooltip;
    private ImageView        mIvOpenTooltip;
    private ImageView        mIvTooltipClose;

    //에러출력
    private LinearLayout    mLayoutRequestErrorArea;
    private TextView        mTvRequestErrorText;

    private String mInputAmount = "";
    private String mInputAmountNoComma = "";

    //나를 제외한 나머지 리스트들의 이체금액 합계
    private double mExeptMeUntilSum = 0d;
    private double mExeptMeAmountSum = 0d;

    public static ViewHolderRemitte newInstance(ViewGroup parent, OnITransferMultiActionListener listener) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_itransfer_multi_remitte_list_item, parent, false);
        return new ViewHolderRemitte(parent.getContext(),itemView,listener);
    }

    public ViewHolderRemitte(Context context, @NonNull View itemView, OnITransferMultiActionListener listener) {
        super(itemView);
        mContext = context;
        mListener = listener;

        mLayoutBody = itemView.findViewById(R.id.layout_body);
        int radius = (int) Utils.dpToPixel(context,20);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));

        //아이콘,계좌명.계좌번호 출력
        mIvIcon = itemView.findViewById(R.id.iv_bank_icon);
        mLayoutDelIcon = itemView.findViewById(R.id.layout_del_btn);
        mTvRcvName = itemView.findViewById(R.id.tv_rcv_name);
        mTvAccountName = itemView.findViewById(R.id.tv_account_name);

        mLayoutOpenMoreInfo = itemView.findViewById(R.id.layout_open_more_info);
        mIvOpenMoreInfo = itemView.findViewById(R.id.iv_open_more_info);

        mLayoutHangleMoney = itemView.findViewById(R.id.layout_hangle_money);
        mLayoutInputAmount = itemView.findViewById(R.id.layout_input_amount);
        mLayoutLimit = itemView.findViewById(R.id.layout_limit);

        mTvHangleMoney = itemView.findViewById(R.id.tv_hangle_money);
        mEtInputAmount = itemView.findViewById(R.id.et_input_amount);

        //금액입력 리스너 연결
        setAmountListener();

        mTvWon = itemView.findViewById(R.id.tv_won);
        mTvLimit = itemView.findViewById(R.id.tv_limit);
        mTvLimit.setVisibility(GONE);
        mTvErrLimitText =  itemView.findViewById(R.id.tv_error_limit);
        mTvErrLimitText.setVisibility(GONE);

        //상세정보
        mLayoutDetailInfo = itemView.findViewById(R.id.layout_detail_info);
        mLayoutDetailInfo.setVisibility(GONE);


        mLayoutLine1Info = itemView.findViewById(R.id.layout_detail_line_1);
        mLayoutLine2Info = itemView.findViewById(R.id.layout_detail_line_2);
        mLayoutLine3Info = itemView.findViewById(R.id.layout_detail_line_3);
        mLayoutLine4Info = itemView.findViewById(R.id.layout_detail_line_4);

        mEtDispRcvAccText = itemView.findViewById(R.id.et_line_1);
        mEtDispMyAccText = itemView.findViewById(R.id.et_line_2);
        mEtMemoText = itemView.findViewById(R.id.et_line_4);

        mIvDispRcvAccErase = itemView.findViewById(R.id.iv_line_1_erase);
        mIvDispMyAccErase = itemView.findViewById(R.id.iv_line_2_erase);
        mIvMemoErase = itemView.findViewById(R.id.iv_line_4_erase);

        //세부정보 리스너 연결
        setDetailInfoListener();

        mLayoutDisplaySendDate = itemView.findViewById(R.id.layout_send_date);
        mTvSendDateText = itemView.findViewById(R.id.tv_senddate_text);
        mIvSendDateDropDown = itemView.findViewById(R.id.iv_senddate_dropdown);
        mIvSendDateErase = itemView.findViewById(R.id.iv_senddate_erase);

        mIvOpenTooltip = itemView.findViewById(R.id.iv_open_tooltip);
        mLayoutTooltip = itemView.findViewById(R.id.layout_tooltip);
        mLayoutTooltip.setVisibility(GONE);
        radius = (int) Utils.dpToPixel(context,3);
        mLayoutTooltip.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#000000"));
        


        mIvTooltipClose = itemView.findViewById(R.id.iv_tooltip_close);

        //에러출력
        mLayoutRequestErrorArea = itemView.findViewById(R.id.layout_request_error_msg_area);
        mLayoutRequestErrorArea.setVisibility(GONE);
        mTvRequestErrorText = itemView.findViewById(R.id.tv_request_error_text);
    }

    public void onBind(int listPostion,int itemPosition){
        Logs.e("########################################");
        Logs.e("start - ViewHolderRemitte - onBind : " + itemPosition);
        Logs.e("########################################");


        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition);

        //아이콘
        Uri iconUri = ImgUtils.getIconUri(mContext,remitteeInfo.getCNTP_FIN_INST_CD(),remitteeInfo.getDTLS_FNLT_CD());
        if(iconUri != null)
            mIvIcon.setImageURI(iconUri);
        else
            mIvIcon.setImageResource(R.drawable.img_logo_xxx);

        //삭제버튼
        mLayoutDelIcon.setTag(R.id.ITEM_POS,itemPosition);
        mLayoutDelIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final int position = (int)v.getTag(R.id.ITEM_POS);
                if(ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 1){
                    ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position);

                    String msg = "<strong><font color=\"#000000\">[" + remitteeInfo.getRECV_NM()  + "]</font></strong>\n이체정보를\n삭제하시겠습니까?";
                    DialogUtil.alert(mContext, "예", "아니요", msg, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    ITransferDataMgr.getInstance().removeRemitteInfoFromArrayList(position);
                                    mListener.makeMultiTransInfoList();
                                    mListener.changeStateBtnTransfer();
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                }else{
                    DialogUtil.alert(mContext,"최소 1건은 삭제 불가능합니다.");
                }
            }
        });

        //받는사람 이름
        if(TextUtils.isEmpty(remitteeInfo.getRECV_NM())){
            mTvRcvName.setText(remitteeInfo.getMNRC_ACCO_DEPR_NM());
        }else{
            mTvRcvName.setText(remitteeInfo.getRECV_NM());
        }


        //은행+계좌번호
        String accountName = "";
        String acno = remitteeInfo.getCNTP_BANK_ACNO();
        String CNTP_FIN_INST_CD = remitteeInfo.getCNTP_FIN_INST_CD();
        if(!TextUtils.isEmpty(CNTP_FIN_INST_CD) && (CNTP_FIN_INST_CD.equalsIgnoreCase("000") || CNTP_FIN_INST_CD.equalsIgnoreCase("028"))){
            String account = acno.substring(0, 5) + "-" + acno.substring(5, 7) + "-" + acno.substring(7, acno.length());
            accountName = remitteeInfo.getMNRC_BANK_ALS()  + " " + account;
        }else{
            accountName = remitteeInfo.getMNRC_BANK_ALS() + " " + acno;
        }
        mTvAccountName.setText(accountName);

        //에러메세지가 존재하면 금액 및 여러 정보를 보이지 않도록 한다.
        if(!TextUtils.isEmpty(remitteeInfo.getEROR_MSG_CNTN())) {
            mTvHangleMoney.setVisibility(View.INVISIBLE);
            mLayoutInputAmount.setVisibility(GONE);
            mLayoutLimit.setVisibility(GONE);
            mLayoutOpenMoreInfo.setVisibility(GONE);

            mTvRequestErrorText.setText(remitteeInfo.getEROR_MSG_CNTN());
            mLayoutRequestErrorArea.setVisibility(VISIBLE);
            return;
        }else{
            mLayoutRequestErrorArea.setVisibility(GONE);

            mLayoutInputAmount.setVisibility(VISIBLE);
            mLayoutLimit.setVisibility(VISIBLE);
            mLayoutOpenMoreInfo.setVisibility(VISIBLE);
        }

        //추가정보
        mLayoutOpenMoreInfo.setTag(R.id.ITEM_POS,itemPosition);
        mLayoutOpenMoreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int)v.getTag(R.id.ITEM_POS);

                if(mLayoutDetailInfo.getVisibility() != VISIBLE){
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position).setIS_OPEN_MORE_INFO(true);
                }else{
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position).setIS_OPEN_MORE_INFO(false);
                }
                if(mEtInputAmount.hasFocus()){
                    mEtInputAmount.clearFocus();
                    Utils.hideKeyboard(mContext,mEtInputAmount);
                }

                mListener.notifyItemChange(position+1);
            }
        });
        if(remitteeInfo.isIS_OPEN_MORE_INFO()){
            mLayoutDetailInfo.setVisibility(VISIBLE);
            ObjectAnimator.ofFloat(mIvOpenMoreInfo, View.ROTATION, 0, 180f) .setDuration(10) .start();
        }else{
            mLayoutDetailInfo.setVisibility(GONE);
            ObjectAnimator.ofFloat(mIvOpenMoreInfo, View.ROTATION, 180f, 360f) .setDuration(10) .start();
        }

        //입금금액
        setTransAmount(listPostion,itemPosition,remitteeInfo);

        //상세정보입력
        setDetailInfoLayout(listPostion,itemPosition,remitteeInfo);



        //포커스를 가지고 있는 경우 아이템을 바인드 한다고 해서 포커스가 사라지지 않는다.
        //바인드시엔 무조건 포커스 사라지도록 수정.
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                clearFocus();
//            }
//        },100);


        //수취인 조회시 입력된 마지막 수취인에 포커스가 필요하면 포커스를 표시한다.
        Logs.e("remitteeInfo.isIS_NEED_FOCUS() : " + remitteeInfo.isIS_NEED_FOCUS());
        if(remitteeInfo.isIS_NEED_FOCUS()){
            remitteeInfo.setIS_NEED_FOCUS(false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mEtInputAmount.requestFocus();
                    Utils.showKeyboard(mContext,mEtInputAmount);
                }
            },100);
        }

        Logs.e("########################################");
        Logs.e("end - ViewHolderRemitte - onBind : " + itemPosition);
        Logs.e("########################################");
    }

    private void setAmountListener(){
        mEtInputAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Logs.e("onEditorAction : " + actionId);
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(mContext,mEtInputAmount);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        mEtInputAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                int itemPosition = (int)v.getTag(R.id.ITEM_POS);
                int listPosition = (int)v.getTag(R.id.LIST_POS);
                Logs.e("start - setOnFocusChangeListener - position : " + itemPosition + " , hasfous : " + hasFocus);
                if(hasFocus){
                    mListener.setCurrentFocusItem(listPosition);

                    //나를 제외하 나머지 리스트 아이템들의 이체금액 합계
                    mExeptMeUntilSum = ITransferDataMgr.getInstance().getUntilTranSumAmount(itemPosition);
                    mExeptMeAmountSum = ITransferDataMgr.getInstance().getExeptPositionTranSumAmount(itemPosition);

                    //포커스 받으면 커서의 위치를 뒤로 이동.
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            String inputAmount = mEtInputAmount.getText().toString();
                            if(!TextUtils.isEmpty(inputAmount)){
                                mEtInputAmount.setSelection(inputAmount.length());

                                String newBLNC = inputAmount.replace(",","");
                                if(Double.valueOf(newBLNC) >= 10000){
                                    mTvHangleMoney.setVisibility(VISIBLE);
                                }
                            }else{
                                mEtInputAmount.setSelection(0);
                            }
                        }
                    },100);

                    if (!checkVaildAmount(itemPosition)) {
                        mTvLimit.setVisibility(GONE);
                        mTvErrLimitText.setVisibility(VISIBLE);

                        mEtInputAmount.setTextColor(Color.parseColor("#e0250c"));
                        mTvWon.setTextColor(Color.parseColor("#e0250c"));
                    }else{
                        mTvLimit.setVisibility(VISIBLE);
                        mTvErrLimitText.setVisibility(GONE);

                        mEtInputAmount.setTextColor(Color.parseColor("#009beb"));
                        mTvWon.setTextColor(Color.parseColor("#009beb"));
                    }
                }else{
                    //모델에 입력 금액을 넣어준다.
                    String BLNC = mEtInputAmount.getText().toString();
                    if(!TextUtils.isEmpty(BLNC)){
                        String newBLNC = BLNC.replace(",","");
                        ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setTRN_AMT(newBLNC);
                    }else{
                        ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setTRN_AMT("");
                    }

                    //포커스가 사라지면 무조건 안보이게.
                    mTvHangleMoney.setVisibility(View.INVISIBLE);
                    //Utils.hideKeyboard(mContext,mEtInputAmount);
                    if(checkVaildAmount(itemPosition)){
                        mEtInputAmount.setTextColor(Color.parseColor("#000000"));
                        mTvWon.setTextColor(Color.parseColor("#000000"));
                    }

                    mTvErrLimitText.setVisibility(GONE);
                    mTvLimit.setVisibility(GONE);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mListener.notifyItemChange(0);
                            mListener.changeStateBtnTransfer();
                        }
                    },100);
                }
                //Logs.e("end - setOnFocusChangeListener - position : " + position + " , hasfous : " + hasFocus);
            }
        });

        mEtInputAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Logs.e("beforeTextChanged : " + s.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if(!mEtInputAmount.hasFocus()) return;
                if(TextUtils.isEmpty(charSequence.toString())){
                    mTvWon.setVisibility(GONE);
                    mTvHangleMoney.setText("0원");
                    return;
                }else if(charSequence.toString().equalsIgnoreCase("0")){
                    return;
                }

                Logs.e("start - onTextChanged : " + charSequence.toString());


                if (!TextUtils.isEmpty(charSequence.toString()) && !charSequence.toString().equals(mInputAmount)) {
                    //Firebase에러 - 계좌로 보내기에서 금액 클립보드 복사 텍스트 붙여넣기시 에러.
                    mInputAmountNoComma = charSequence.toString().replaceAll("[^0-9]", Const.EMPTY);
                    mInputAmount = Utils.moneyFormatToWon(Double.parseDouble(mInputAmountNoComma));
                    mEtInputAmount.setText(mInputAmount);
                    mEtInputAmount.setSelection(mInputAmount.length());

                }

                Logs.e("end - onTextChanged : " + charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(!mEtInputAmount.hasFocus()) return;
                int position = (int)mEtInputAmount.getTag(R.id.ITEM_POS);
                String amount = editable.toString();
                if(TextUtils.isEmpty(amount)) {
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position).setTRN_AMT("");
                    mEtInputAmount.setTypeface(Typeface.DEFAULT);
                    return;
                }else{
                    mEtInputAmount.setTypeface(Typeface.DEFAULT_BOLD);
                }

                if(mTvWon.getVisibility() != VISIBLE)
                    mTvWon.setVisibility(VISIBLE);

                Logs.e("0.afterTextChanged : " + editable.toString());

                //amount = amount.replaceAll("[^0-9]", Const.EMPTY);
                if(!editable.toString().equalsIgnoreCase(mInputAmount))
                    return;

                Logs.e("1.afterTextChanged : " + editable.toString());

                String newBLNC = editable.toString().replace(",","");
                String hangul = Utils.convertHangul(mInputAmountNoComma);
                if (!TextUtils.isEmpty(hangul)) {
                    hangul += mContext.getString(R.string.won);
                    mTvHangleMoney.setText(hangul);
                }
                if(Double.valueOf(newBLNC) >= 10000){
                    if(mTvHangleMoney.getVisibility() != VISIBLE){
                        mTvHangleMoney.setVisibility(VISIBLE);
                    }
                }else{
                    mTvHangleMoney.setVisibility(View.INVISIBLE);
                }

                //여기에서 변경된 금액을 넣어준다.
                ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position).setTRN_AMT(mInputAmountNoComma);

                if(checkVaildAmount(position)){
                    if(mTvLimit.getVisibility() != VISIBLE){
                        mEtInputAmount.setTextColor(Color.parseColor("#009beb"));
                        mTvWon.setTextColor(Color.parseColor("#009beb"));
                        mTvLimit.setVisibility(VISIBLE);
                        mTvErrLimitText.setVisibility(GONE);
                    }
                }else{
                    if(mTvErrLimitText.getVisibility() != VISIBLE){
                        mEtInputAmount.setTextColor(Color.parseColor("#e0250c"));
                        mTvWon.setTextColor(Color.parseColor("#e0250c"));
                        mTvErrLimitText.setVisibility(View.VISIBLE);
                        mTvLimit.setVisibility(GONE);
                    }
                }

                Logs.e("2.afterTextChanged : " + editable.toString());
            }
        });
    }

    //입금금액
    private void setTransAmount(final int listPosition,final int itemPosition, ITransferRemitteeInfo remitteeInfo){
        String BLNC = "0";
        double doubleBLNC = 0;
        //금액과 원을 표시한다.
        if(!TextUtils.isEmpty(remitteeInfo.getTRN_AMT())){
            doubleBLNC = Double.valueOf(remitteeInfo.getTRN_AMT());
            BLNC = Utils.moneyFormatToWon(doubleBLNC);
            mEtInputAmount.setText(BLNC);
            mTvWon.setVisibility(VISIBLE);
            mEtInputAmount.setTypeface(Typeface.DEFAULT_BOLD);
        }else{
            mEtInputAmount.setText("");
            mEtInputAmount.setTypeface(Typeface.DEFAULT);
            mTvWon.setVisibility(GONE);
        }

        //한글금액을 표시한다.
        if(!TextUtils.isEmpty(remitteeInfo.getTRN_AMT())){
            String hangul = Utils.convertHangul(BLNC);
            hangul += mContext.getString(R.string.won);
            mTvHangleMoney.setText(hangul);
        }else{
            mTvHangleMoney.setText("0원");
        }
        //처음 뷰를 그릴때는 한글금액 표시하지 않음.
        mTvHangleMoney.setVisibility(View.INVISIBLE);

        mEtInputAmount.setTag(R.id.ITEM_POS,itemPosition);
        mEtInputAmount.setTag(R.id.LIST_POS,listPosition);


        //한도문구를 넣어둔다.
        String limitOneTimeText = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getTransferOneTime());
        String limitOneDayText = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getTransferOneDay());
        String limitText = "1회 : " + limitOneTimeText + "원 / 1일 : " + limitOneDayText + "원";
        mTvLimit.setText(limitText);

        mTvErrLimitText.setVisibility(GONE);
        mTvLimit.setVisibility(GONE);

        //나를 제외하 나머지 리스트 아이템들의 이체금액 합계
        mExeptMeUntilSum = ITransferDataMgr.getInstance().getUntilTranSumAmount(itemPosition);
        mExeptMeAmountSum = ITransferDataMgr.getInstance().getExeptPositionTranSumAmount(itemPosition);

        if(checkVaildAmount(itemPosition)){
            mEtInputAmount.setTextColor(Color.parseColor("#000000"));
            mTvWon.setTextColor(Color.parseColor("#000000"));
        }else{
            mEtInputAmount.setTextColor(Color.parseColor("#e0250c"));
            mTvWon.setTextColor(Color.parseColor("#e0250c"));
        }
    }

    /**
     * 이체금액 유효성 체크
     *
     * @return boolean
     */
    private boolean checkVaildAmount(int position) {

        String amount = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position).getTRN_AMT();
        if(TextUtils.isEmpty(amount)) amount = "0";

        double untilTranSumAmount = mExeptMeUntilSum + Double.valueOf(amount);
        Logs.e("bindTransAmount - untilTranSumAmount : " + untilTranSumAmount);
        if(untilTranSumAmount > ITransferDataMgr.getInstance().getBalanceAmount()){
            //해당 인덱스까지의 이체금액 합이 이체가능금액보다 크면 색변경
            mEtInputAmount.setTextColor(Color.parseColor("#e0250c"));
            mTvWon.setTextColor(Color.parseColor("#e0250c"));
            String balanceAmountStr = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getBalanceAmount());
            mTvErrLimitText.setText(mContext.getString(R.string.msg_err_trnasfer_over_balance) + "출금가능:" + balanceAmountStr + "원");
            return false;
        }else if(ITransferDataMgr.getInstance().getBalanceAmount() <= 0){
            mEtInputAmount.setTextColor(Color.parseColor("#e0250c"));
            mTvWon.setTextColor(Color.parseColor("#e0250c"));
            String balanceAmountStr = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getBalanceAmount());
            mTvErrLimitText.setText(mContext.getString(R.string.msg_err_trnasfer_over_balance) + "출금가능:" + balanceAmountStr + "원");
            return false;
        }

        double dInputAmount = Double.valueOf(amount);

        if (dInputAmount > ITransferDataMgr.getInstance().getTransferOneTime()) {
            mTvErrLimitText.setText(mContext.getString(R.string.msg_err_trnasfer_over_one_time));
            return false;
        } else if (untilTranSumAmount > ITransferDataMgr.getInstance().getTransferOneDay()) {
            mTvErrLimitText.setText(mContext.getString(R.string.msg_err_trnasfer_over_one_day));
            return false;
        }

        return true;

    }

    private void setDetailInfoListener(){

        //내통장
        mEtDispRcvAccText.setOnFocusChangeListener(onFocusChangeListener);
        mEtDispRcvAccText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Logs.e("onEditorAction : " + actionId);
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(mContext,mEtDispRcvAccText);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
        mEtDispRcvAccText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(mEtDispRcvAccText.getText().toString())){
                    mIvDispRcvAccErase.setVisibility(GONE);
                }else{
                    mIvDispRcvAccErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int itemPosition = (int)mEtDispRcvAccText.getTag(R.id.ITEM_POS);
                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition);
                String inputText = mEtDispRcvAccText.getText().toString();
                Logs.e(itemPosition + "...mEtDispRcvAccText - afterTextChanged : " + inputText);

                if(TextUtils.isEmpty(inputText)){
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setDEPO_BNKB_MRK_NM(remitteeInfo.getCUST_NM());
                }else{
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setDEPO_BNKB_MRK_NM(inputText);
                }
            }
        });

        //받는분 통장
        mEtDispMyAccText.setOnFocusChangeListener(onFocusChangeListener);
        mEtDispMyAccText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Logs.e("onEditorAction : " + actionId);
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(mContext,mEtDispMyAccText);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
        mEtDispMyAccText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(mEtDispMyAccText.getText().toString())){
                    mIvDispMyAccErase.setVisibility(GONE);
                }else{
                    mIvDispMyAccErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int itemPosition = (int)mEtDispMyAccText.getTag(R.id.ITEM_POS);
                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition);
                String inputText = mEtDispMyAccText.getText().toString();
                Logs.e(itemPosition + "...mEtDispMyAccText - afterTextChanged : " + inputText);
                if(TextUtils.isEmpty(inputText)){
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setTRAN_BNKB_MRK_NM(remitteeInfo.getRECV_NM());
                }else{
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setTRAN_BNKB_MRK_NM(inputText);
                }
            }
        });

        //메모
        mEtMemoText.setOnFocusChangeListener(onFocusChangeListener);
        mEtMemoText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Logs.e("onEditorAction : " + actionId);
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(mContext,mEtMemoText);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });


        mEtMemoText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(mEtMemoText.getText().toString())){
                    mIvMemoErase.setVisibility(GONE);
                }else{
                    mIvMemoErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                int itemPosition = (int)mEtMemoText.getTag(R.id.ITEM_POS);
                String inputText = mEtMemoText.getText().toString();
                Logs.e(itemPosition + "...mEtMemoText - afterTextChanged : " + inputText);
                if(TextUtils.isEmpty(inputText)){
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setTRN_MEMO_CNTN("");
                }else{
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(itemPosition).setTRN_MEMO_CNTN(inputText);
                }
            }
        });
    }

    //상세정보입력
    private void setDetailInfoLayout(int listPosition,int itemPosition, ITransferRemitteeInfo remitteeInfo){

        int radius = (int) Utils.dpToPixel(mContext,6);
        mLayoutLine1Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));
        mLayoutLine2Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));
        mLayoutLine3Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));
        mLayoutLine4Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));


        //받는분 통장표기
        mEtDispRcvAccText.setTag(R.id.ITEM_POS,itemPosition);
        mEtDispRcvAccText.setText(remitteeInfo.getDEPO_BNKB_MRK_NM());


        //내통장 표기
        mEtDispMyAccText.setTag(R.id.ITEM_POS,itemPosition);
        mEtDispMyAccText.setText(remitteeInfo.getTRAN_BNKB_MRK_NM());


        //보내는 사람.
        mLayoutDisplaySendDate.setTag(R.id.ITEM_POS,itemPosition);
        mLayoutDisplaySendDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int position = (int) v.getTag(R.id.ITEM_POS);
                mListener.showDatePicker(position);
            }
        });
        if(remitteeInfo.getTRNF_DVCD().equalsIgnoreCase("3")){
            String sendDate = remitteeInfo.getTRNF_DMND_DT();
            Logs.e("sendDate : " + sendDate);
            if(!TextUtils.isEmpty(sendDate)){
                sendDate = sendDate.substring(0,4) + "." + sendDate.substring(4,6) + "." + sendDate.substring(6,8);
            }

            String sendTime = remitteeInfo.getTRNF_DMND_TM();
            Logs.e("sendTime : " + sendTime);
            if(!TextUtils.isEmpty(sendTime)){
                sendDate = sendDate + " " + sendTime.substring(0,2) + ":00";
            }

            mTvSendDateText.setText(sendDate);
            mIvSendDateDropDown.setVisibility(GONE);
            mIvSendDateErase.setVisibility(VISIBLE);
        }else{
            mTvSendDateText.setText(R.string.Immediately);
            mIvSendDateDropDown.setVisibility(VISIBLE);
            mIvSendDateErase.setVisibility(GONE);
        }


        mIvSendDateErase.setTag(R.id.ITEM_POS,itemPosition);
        mIvSendDateErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag(R.id.ITEM_POS);
                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position);
                remitteeInfo.setTRNF_DVCD("1");
                remitteeInfo.setTRNF_DMND_DT("");
                remitteeInfo.setTRNF_DMND_TM("");

                mTvSendDateText.setText(R.string.Immediately);
                mIvSendDateDropDown.setVisibility(VISIBLE);
                mIvSendDateErase.setVisibility(GONE);
            }
        });

        if(remitteeInfo.isIS_OPEN_TOOLTIP_SENDDATE()){
            mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_on);
            mLayoutTooltip.setVisibility(VISIBLE);
        }else{
            mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_off);
            mLayoutTooltip.setVisibility(GONE);
        }
        mIvOpenTooltip.setTag(R.id.ITEM_POS,itemPosition);
        mIvOpenTooltip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag(R.id.ITEM_POS);
                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position);
                if(mLayoutTooltip.getVisibility() == VISIBLE){
                    remitteeInfo.setIS_OPEN_TOOLTIP_SENDDATE(false);
                    mLayoutTooltip.setVisibility(GONE);
                    mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_off);
                }else{
                    remitteeInfo.setIS_OPEN_TOOLTIP_SENDDATE(true);
                    mLayoutTooltip.setVisibility(VISIBLE);
                    mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_on);
                }
            }
        });

        mIvTooltipClose.setTag(R.id.ITEM_POS,itemPosition);
        mIvTooltipClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = (int) v.getTag(R.id.ITEM_POS);
                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position);
                remitteeInfo.setIS_OPEN_TOOLTIP_SENDDATE(false);
                mLayoutTooltip.setVisibility(GONE);
                mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_off);
            }
        });



        //메모
        mEtMemoText.setTag(R.id.ITEM_POS,itemPosition);
        mEtMemoText.setText(remitteeInfo.getTRN_MEMO_CNTN());

        //삭제 이미지.
        mIvDispRcvAccErase.setVisibility(GONE);
        mIvDispMyAccErase.setVisibility(GONE);
        mIvMemoErase.setVisibility(GONE);
        mIvDispRcvAccErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEtDispRcvAccText.setText("");
            }
        });
        mIvDispMyAccErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEtDispMyAccText.setText("");
            }
        });
        mIvMemoErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEtMemoText.setText("");
            }
        });

    }

    View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            int itemPosition = (int)v.getTag(R.id.ITEM_POS);

            int radius = (int) Utils.dpToPixel(mContext,6);
            Drawable background;
            if(hasFocus){
                background = GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#01a2b4");
            }else{
                background = GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5");
            }

            switch (v.getId()){
                case R.id.et_line_1:
                    mLayoutLine1Info.setBackground(background);
                    if(hasFocus){
                        if(!TextUtils.isEmpty(mEtDispRcvAccText.getText().toString())){
                            mIvDispRcvAccErase.setVisibility(VISIBLE);
                        }
                    }else{
                        mIvDispRcvAccErase.setVisibility(GONE);
                    }
                    break;
                case R.id.et_line_2:
                    mLayoutLine2Info.setBackground(background);
                    if(hasFocus){
                        if(!TextUtils.isEmpty(mEtDispMyAccText.getText().toString())){
                            mIvDispMyAccErase.setVisibility(VISIBLE);
                        }
                    }else{
                        mIvDispMyAccErase.setVisibility(GONE);
                    }
                    break;
                case R.id.et_line_4:
                    mLayoutLine4Info.setBackground(background);
                    if(hasFocus){
                        if(!TextUtils.isEmpty(mEtMemoText.getText().toString())){
                            mIvMemoErase.setVisibility(VISIBLE);
                        }
                    }else{
                        mIvMemoErase.setVisibility(GONE);
                    }
                    break;
            }
        }
    };
}


