package com.sbi.saidabank.activity.login;

import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

/**
 * siadabank_android
 * Class: ReIssueLostmOTPActivity
 * Created by 950546
 * Date: 2018-12-21
 * Time: 오전 9:41
 * Description:ReIssueLostmOTPActivity
 */
public class ReIssueLostmOTPActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reissue_lost_motppw);
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
