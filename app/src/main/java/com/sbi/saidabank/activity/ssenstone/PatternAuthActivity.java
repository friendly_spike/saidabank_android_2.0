package com.sbi.saidabank.activity.ssenstone;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;

import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.info.DeviceResult;
import com.netfunnel.api.ContinueData;
import com.netfunnel.api.Netfunnel;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.login.ReregChangeFingerPrintActivity;
import com.sbi.saidabank.activity.login.ReregLostPatternActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.activity.test.SolutionTestActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.NetfunnelDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.patternlockview.PatternLockView;
import com.sbi.saidabank.customview.patternlockview.listener.PatternLockViewListener;
import com.sbi.saidabank.customview.patternlockview.utils.PatternLockUtils;
import com.sbi.saidabank.customview.patternlockview.utils.ResourceUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.push.FLKPushAgentSender;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.solution.v3.V3Manager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: PatternAuthActivity
 * Created by 950485 on 2018. 10. 18..
 * <p>
 * Description:패턴 인증을 위한 화면
 */
public class PatternAuthActivity extends BaseActivity implements StonePassManager.FingerPrintDlgListener, StonePassManager.StonePassListener, I3GAsyncResponse {

    private LinearLayout mLayoutMsg;
    private TextView mTextMsg;
    private PatternLockView mPatternLockView;
    private FidoDialog mFingerprintDlg;
    private CommonUserInfo mCommonUserInfo;
    private String mBioType = "PATTERN";
    private String mAuthPattern;
    private boolean mIsProgress = false;
    private boolean mIsFingerprintLogin;
    private int mCountFail = 0;
    // 지문인증 체크한후 에러발생 수.(노트10에서 사용자 취소시 에러코드가 잘못되어 넘어온다.그래서 추가)
    private int mFidoCheckErrorCnt;
    // 필요권한 - 전화접근권한
    // String[] perList = new String[] { Manifest.permission.READ_PHONE_STATE };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_auth);
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);


        initUX();
        // 진입점이 로그인이고 지문등록 상태일 때 체크
        mFidoCheckErrorCnt = 0;
        mIsFingerprintLogin = false;
        mCommonUserInfo.setChangedFinger(false);
//        if (!Prefer.getFlagFingerPrintChange(PatternAuthActivity.this)
//                && (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO)
//                && Prefer.getFidoRegStatus(PatternAuthActivity.this)) {
//            mIsFingerprintLogin = true;
//            showProgressDialog();
//            StonePassManager.getInstance(getApplication()).ssenstoneFIDO(PatternAuthActivity.this, mCommonUserInfo.getOperation(), mCommonUserInfo.getSignData(), mBioTypeF, mBioTypeF);
//        }
//        PermissionUtils.checkPermission(this,perList,Const.REQUEST_READ_PHONE_STATE_PERMISSIONS);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        mCountFail = Prefer.getPatternErrorCount(PatternAuthActivity.this);
        if ((DataUtil.isNull(mFingerprintDlg) || (DataUtil.isNotNull(mFingerprintDlg) && !mFingerprintDlg.isShowing()))
                && !Prefer.getFlagFingerPrintChange(PatternAuthActivity.this)
                && (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO)
                && Prefer.getFidoRegStatus(PatternAuthActivity.this)) {
            mIsFingerprintLogin = true;
            showProgressDialog();
            StonePassManager.getInstance(getApplication()).ssenstoneFIDO(PatternAuthActivity.this,
                    mCommonUserInfo.getOperation(), mCommonUserInfo.getSignData(), "FINGERPRINT", "FINGERPRINT");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            V3Manager.getInstance(getApplicationContext()).stopV3MobilePlus();
            ActivityCompat.finishAffinity(PatternAuthActivity.this);
            System.exit(0);
        } else {
            finish();
        }
    }

    @Override
    public void showFingerPrintDialog() {
        dismissProgressDialog();
        if (isFinishing()) return;
        if (DataUtil.isNull(mFingerprintDlg)) {
            mFingerprintDlg = DialogUtil.fido(
                    PatternAuthActivity.this,
                    true,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mIsFingerprintLogin = false;
                            StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                            mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                            mFingerprintDlg.dismiss();
                        }
                    },
                    null
            );
            mFingerprintDlg.mBackKeyListener = new Dialog.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    mIsFingerprintLogin = false;
                    StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                }
            };
            mFingerprintDlg.setCanceledOnTouchOutside(false);
        } else {
            mFingerprintDlg.showFidoCautionMsg(false);
            mFingerprintDlg.show();
        }
    }

    /**
     * 지문인식 화면 이벤트 리스너
     */
    @Override
    public void stonePassResult(String op, int errorCode, String errorMsg) {
        MLog.i("Pattern > stonePassResult : errorCode[" + errorCode + "], errorMsg[" + errorMsg + "]");
        // dismissProgressDialog();
        String msg = Const.EMPTY;

        // 화면이 종료되면 리턴시킨다.
        if (isFinishing()) return;

        if (mIsFingerprintLogin) {
            // 지문인증을 계속 진행해야하는 에러인경우.
            switch (errorCode) {
                case 1:
                case 2:
                case 4:     // 너무 짧게 눌렀을때
                case 8:
                case 1001:
                    dismissProgressDialog();
                    if (DataUtil.isNotNull(mFingerprintDlg)) {
                        mFingerprintDlg.showFidoCautionMsg(true);
                    }
                    return;
                case 11:    // 인증실패
                    dismissProgressDialog();
                    mFidoCheckErrorCnt++;
                    if (DataUtil.isNotNull(mFingerprintDlg)) {
                        mFingerprintDlg.showFidoCautionMsg(true);
                    }
                    return;
                default:
                    break;
            }

            // 여기서부터는 지문인증을 종료해야 하는 에러인경우 출력.
            if (DataUtil.isNotNull(mFingerprintDlg)) {
                mFingerprintDlg.dismiss();
            }

            switch (errorCode) {

                // 사용자 취소
                case 3:
                    dismissProgressDialog();
                    mIsFingerprintLogin = false;
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                    return;

                /**
                 * 20201101 - 노트10에서 에러코드가 이상하게 날아온다.
                 * 전면 지문인증 화면이 출력되는 폰에서 발생하는 문제점이다.
                 * 그래서 에러 카운트를 해서 시도횟수별 에러 메세지를 출력하도록 수정한다.
                 * */
                case 10:
                    if (mFidoCheckErrorCnt >= 4) {
                        msg = "지문인증 시도횟수가 너무 많습니다. 나중에 다시 시도하세요.";
                        showErrorMessage(msg);
                    }
                    return;

                // 지문 추가 시 재등록 처리
                case 200:
                    dismissProgressDialog();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (DataUtil.isNotNull(mLayoutMsg))
                                mLayoutMsg.setVisibility(View.INVISIBLE);
                        }
                    });
                    Prefer.setFlagFingerPrintChange(PatternAuthActivity.this, true);
                    Prefer.setFidoRegStatus(PatternAuthActivity.this, false);
                    mCommonUserInfo.setChangedFinger(true);
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                    mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                    Intent intent = new Intent(PatternAuthActivity.this, ReregChangeFingerPrintActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                    //finish();
                    return;

                case 1200:
                    break;

                default:
                    dismissProgressDialog();
                    mIsFingerprintLogin = false;
                    mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                    msg = (errorCode == 9) ? "시도 횟수가 너무 많습니다. 지문 센서가 사용 중지되었습니다." : "지문 인증 실패했습니다.";
                    showErrorMessage(msg);
                    return;

            }
        }

        switch (errorCode) {

            case 100:
                mCountFail++;
                if (mCountFail < Const.SSENSTONE_AUTH_COUNT_FAIL) {
                    msg = String.format(getString(R.string.msg_err_no_match_pattern), mCountFail, Const.SSENSTONE_AUTH_COUNT_FAIL);
                    Prefer.setPatternErrorCount(PatternAuthActivity.this, mCountFail);
                } else {
                    Prefer.setPatternErrorCount(PatternAuthActivity.this, Const.SSENSTONE_AUTH_COUNT_FAIL);
                    dismissProgressDialog();
                    if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN) {
                        msg = String.format(getString(R.string.msg_err_over_match_pattern_login), Const.SSENSTONE_AUTH_COUNT_FAIL);
                    } else {
                        msg = String.format(getString(R.string.msg_err_over_match_pattern), Const.SSENSTONE_AUTH_COUNT_FAIL);
                    }
                    DialogUtil.alert(
                            PatternAuthActivity.this,
                            getResources().getString(R.string.rereg),
                            getResources().getString(R.string.common_cancel),
                            msg,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mLayoutMsg.setVisibility(View.INVISIBLE);
                                    Intent intent = new Intent(PatternAuthActivity.this, ReregLostPatternActivity.class);
                                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                    startActivity(intent);
                                    if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN)
                                        finish();
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                    //setResult(RESULT_CANCELED);
                                }
                            });
                    return;
                }
                break;

            case 1200:
                if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    msg = "인증했습니다.";
                    if (mCommonUserInfo.getEntryPoint() != EntryPoint.LOGIN_BIO) {
                        Prefer.setPatternErrorCount(PatternAuthActivity.this, 0);
                    }
                } else {
                    msg = "해제했습니다.";
                    Prefer.setPatternRegStatus(PatternAuthActivity.this, false);
                }
                break;

            case 1404:
                // msg = "사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.";
                msg = "인증을 실패했습니다.";
                break;

            case 1491:
                if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    if (Prefer.getPatternRegStatus(PatternAuthActivity.this)) {
                        msg = "인증을 실패했습니다.";
                    } else {
                        msg = "사용자 정보가 없습니다. 먼저 등록하시기 바랍니다.";
                    }
                }
                break;

            case 0://패턴일때만 리턴되는 값인듯.
                // msg = "해제했습니다.";
                msg = "인증을 실패했습니다.";
                break;

            case 3:
                dismissProgressDialog();
                msg = "취소하셨습니다.";
                mIsFingerprintLogin = false;
                mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
                return;

            case 9998:
                // msg = "서버로 부터 리턴 값이 널(Null)입니다..";
                msg = "인증을 실패했습니다.";
                break;

            case 9999:
                // msg = "작업중 json에러가 발생했습니다.";
                msg = "인증을 실패했습니다.";
                break;

            default:
                if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    msg = "인증을 실패했습니다.";
                } else {
                    msg = "해제를 실패했습니다.";
                }
                break;
        }

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE) && errorCode != 1200) {
                dismissProgressDialog();
                List<PatternLockView.Dot> list = mPatternLockView.getPattern();
                mPatternLockView.setPattern(PatternLockView.PatternViewMode.WRONG, list);
                mTextMsg.setText(msg);
                mLayoutMsg.setVisibility(View.VISIBLE);

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mPatternLockView.clearPattern();
                        if (mCountFail >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
                            mPatternLockView.setEnabled(false);
                        }
                    }
                }, 500);
                AniUtils.shakeView(mLayoutMsg);
                return;
            }
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            // TODO Auto-generated method stub
        } else {
            mPatternLockView.clearPattern();
            if (errorCode != 1200) {
                dismissProgressDialog();
                mCommonUserInfo.setResultMsg(msg);
                Intent intent = new Intent();
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                //setResult(RESULT_CANCELED, intent);
                finish();
                return;
            }
        }

        mPatternLockView.clearPattern();
        mCountFail = 0;
        mPatternLockView.setEnabled(true);

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            dismissProgressDialog();
            mCommonUserInfo.setSignData(mAuthPattern);
            Intent intent = new Intent(this, PatternRegActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
            finish();
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            checkNetfunnel();
        } else {
            dismissProgressDialog();
            // setResult(RESULT_OK);
            finish();
        }
    }

    @Override
    public void i3GProcessFinish(final DeviceResult deviceResult) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // 로그인 요청
                        loginProcess(
                                FDSManager.getInstance().getEncIMEI(PatternAuthActivity.this),
                                FDSManager.getInstance().getEncUUID(PatternAuthActivity.this),
                                deviceResult.getNatip(),
                                deviceResult.getResultStr()
                        );
                    }
                });
            }
        }).start();
    }

    /*
     * 화면 초기화
     */
    private void initUX() {
        mPatternLockView = (PatternLockView) findViewById(R.id.pattern_lockview_auth);
        mPatternLockView.setDotCount(3);
        mPatternLockView.setDotNormalSize((int) ResourceUtils.getDimensionInPx(PatternAuthActivity.this, R.dimen.pattern_dot_size));
        mPatternLockView.setDotSelectedSize((int) ResourceUtils.getDimensionInPx(PatternAuthActivity.this, R.dimen.pattern_selected_dot_size));
        mPatternLockView.setPathWidth((int) ResourceUtils.getDimensionInPx(PatternAuthActivity.this, R.dimen.pattern_path_width));
        mPatternLockView.setAspectRatioEnabled(true);
        //mPatternLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS);
        mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
        mPatternLockView.setDotAnimationDuration(150);
        mPatternLockView.setPathEndAnimationDuration(100);
        mPatternLockView.setBackgroundColor(getResources().getColor(R.color.color3F444D));
        mPatternLockView.setNormalStateColor(Color.WHITE);
        mPatternLockView.setCorrectStateColor(Color.WHITE);
        mPatternLockView.setWrongStateColor(getResources().getColor(R.color.colorErr));
        mPatternLockView.setDrawStateColor(getResources().getColor(R.color.color00EBFF));
        mPatternLockView.setInStealthMode(Prefer.getUsePatternStealth(PatternAuthActivity.this));
        mPatternLockView.setTactileFeedbackEnabled(true);
        mPatternLockView.setInputEnabled(true);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);

        RelativeLayout layoutLostPattern = (RelativeLayout) findViewById(R.id.layout_lost_pattern);
        layoutLostPattern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MLog.d();
                mLayoutMsg.setVisibility(View.INVISIBLE);
                Intent intent = new Intent(PatternAuthActivity.this, ReregLostPatternActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
                    finish();
                }
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);
        float density = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        float rate = dpHeight / dpWidth;
        if (rate < 1.70) {
            ViewGroup.LayoutParams p = layoutLostPattern.getLayoutParams();
            float magin = Utils.dpToPixel(PatternAuthActivity.this, 10);
            if (p instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutLostPattern.setLayoutParams(lp);
            } else if (p instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutLostPattern.setLayoutParams(lp);
            } else if (p instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutLostPattern.requestLayout();
            }
        }

        TextView textDesc = (TextView) findViewById(R.id.textview_pattern_auth_desc);
        ImageView imageBG = (ImageView) findViewById(R.id.imageview_profile_bg);
        ImageView imageProfile = (ImageView) findViewById(R.id.imageview_profile_pattern_auth);
        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 테스트 코드. 로그인 불가 시 프로필이미지 클릭하여 테스트 메뉴 진입
                if (BuildConfig.DEBUG) {
                    Intent intent = new Intent(PatternAuthActivity.this, SolutionTestActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
        mLayoutMsg = (LinearLayout) findViewById(R.id.layout_pattern_msg_auth);
        mTextMsg = (TextView) findViewById(R.id.textview_pattern_msg_auth);

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            textDesc.setVisibility(View.VISIBLE);
            imageBG.setVisibility(View.GONE);
            imageProfile.setVisibility(View.GONE);
            TextView textClose = findViewById(R.id.tv_close);
            textClose.setVisibility(View.VISIBLE);
            textClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MLog.d();
                    finish();
                }
            });
        }

        File file = new File(Prefer.getProfileImagePath(PatternAuthActivity.this));
        if (file.exists()) {
            //========================================================================
            Uri uri;
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                uri = Uri.fromFile(file);
            } else {
                uri = FileProvider.getUriForFile(PatternAuthActivity.this, getPackageName() + ".fileprovider", file);
            }
            imageProfile.setImageDrawable(null);
            imageProfile.destroyDrawingCache();
            imageProfile.setImageURI(uri);
            //========================================================================
            // imageProfile.setImageURI(Uri.fromFile(file));
        } else {
            imageProfile.setImageResource(R.drawable.ico_profile);
        }

        LoginUserInfo.getInstance().setCompletedReg(false);
    }

    /**
     * 패턴 화면 이벤트 리스너
     */
    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
            if (mLayoutMsg.isShown()) {
                mLayoutMsg.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
            mIsProgress = true;
        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            mIsProgress = false;
            if (pattern.size() < Const.SSENSTONE_MIN_PATTERN) {
                mTextMsg.setText(R.string.msg_min_length_reg_pattern);
                mTextMsg.announceForAccessibility(getString(R.string.msg_min_length_reg_pattern));
                mLayoutMsg.setVisibility(View.VISIBLE);
                mPatternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!mIsProgress) {
                            mPatternLockView.clearPattern();
                        }
                    }
                }, 500);
                AniUtils.shakeView(mLayoutMsg);
                return;
            }

            MLog.i("mCountFail >> " + mCountFail);

            if (mCountFail >= Const.SSENSTONE_AUTH_COUNT_FAIL) {

                mPatternLockView.clearPattern();

                String msg;
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_PATTERN) {
                    msg = String.format(getString(R.string.msg_err_over_match_pattern_login), Const.SSENSTONE_AUTH_COUNT_FAIL);
                } else {
                    msg = String.format(getString(R.string.msg_err_over_match_pattern), Const.SSENSTONE_AUTH_COUNT_FAIL);
                }

                DialogUtil.alert(
                        PatternAuthActivity.this,
                        getResources().getString(R.string.rereg),
                        getResources().getString(R.string.common_cancel),
                        msg,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mLayoutMsg.setVisibility(View.INVISIBLE);
                                Intent intent = new Intent(PatternAuthActivity.this, ReregLostPatternActivity.class);
                                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                                startActivity(intent);
                                if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
                                    finish();
                                }
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                                //setResult(RESULT_CANCELED);
                            }
                        }
                );
                return;
            }

            showProgressDialog();

            mAuthPattern = PatternLockUtils.patternToString(mPatternLockView, pattern);
            StonePassManager.getInstance(
                    getApplication()).ssenstoneFIDO(
                    PatternAuthActivity.this,
                    mCommonUserInfo.getOperation(),
                    null,
                    mBioType,
                    PatternLockUtils.patternToString(mPatternLockView, pattern)
            );
            mPatternLockView.clearPattern();
        }

        @Override
        public void onCleared() {
            // TODO Auto-generated method stub
        }

        @Override
        public void onCheckPoint(PatternLockView.Dot dot) {
            MLog.i("Pattern CheckPoint >> " + dot.getId());
        }
    };

    private void checkNetfunnel() {
        MLog.d();

        mCommonUserInfo.setLoginPattern(!mIsFingerprintLogin);

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            showProgressDialog();
        }
        if(BuildConfig.DEBUG && SaidaUrl.getBaseWebUrl().equals(SaidaUrl.DEV_SERVER)) {
            FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this, PatternAuthActivity.this);
            return;
        }

        Netfunnel.BEGIN(
                "service_1",
                "login_app",
                NetfunnelDialog.Show(PatternAuthActivity.this),
                new Netfunnel.Listener() {
                    @Override
                    public void netfunnelMessage(Netfunnel netfunnel, Netfunnel.EvnetCode code) {
                        try {
                            MLog.i("netfunnelMessage -> code: " + code + " | " + netfunnel.getResponse().getCode() + " ttl=" + netfunnel.getResponse().getTTL());
                            if (code.isContinue()) {
                                ContinueData continue_data = netfunnel.getContinueData();
                                if (code == Netfunnel.EvnetCode.ContinueInterval) {

                                } else {  // Netfunnel.EvnetCode.Continue
                                    String continue_msg =
                                            "    예상대기시간:" + continue_data.getCurrentWaitTimeSecond() + "초\n"
                                                    + "    진   행   율:" + continue_data.getCurrentWaitPercent() + "%\n"
                                                    + "    대기자수(앞):" + continue_data.getCurrentWaitCount() + "명\n"
                                                    + "    대기자수(뒤):" + continue_data.getCurrentNextCount() + "명\n"
                                                    + " 초당처리량(TPS):" + netfunnel.getResponse().getTPS() + "\n"
                                                    + "   확인주기(TTL):" + netfunnel.getResponse().getTTL() + "초\n"
                                                    + "   notice acount:" + continue_data.getAcountNotice() + "\n"
                                                    + "   update acount:" + continue_data.getUpdateAcount() + "\n"
                                                    + "UI     표시대기시간:" + netfunnel.getProperty().getUiWaitTimeLimit() + "초\n"
                                                    + "UI 표시대기자수(앞):" + netfunnel.getProperty().getUiWaitCountLimit() + "명\n"
                                                    + "UI 표시대기자수(뒤):" + netfunnel.getProperty().getUiNextCountLimit() + "명";
                                    MLog.i("netfunnelMessage >> \n" + continue_msg);
                                }
                                return;
                            }

                            NetfunnelDialog.Close();

                            if (code.isSuccess()) {
                                FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this, PatternAuthActivity.this);
                            } else if (code.isBlocking()) {
                                dismissProgressDialog();
                                showNetfunnelErrorPopup(R.string.netfunnel_blocking_error_msg);
                            } else if (code.isStop()) {
                                dismissProgressDialog();
                                showNetfunnelErrorPopup(R.string.netfunnel_many_people_error_msg);
                            } else {
                                //20201230 - 개발서버에서는 넷퍼넬 에러 발생해도 그냥 통과.
                                if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_DEV) {
                                    FDSManager.getInstance().getFDSInfo(PatternAuthActivity.this, PatternAuthActivity.this);
                                }else{
                                    dismissProgressDialog();
                                    showNetfunnelErrorPopup(R.string.netfunnel_error_msg);
                                }
                            }

                            Netfunnel.END();

                        } catch (Exception e) {
                            MLog.e(e);
                            dismissProgressDialog();
                            showNetfunnelErrorPopup(R.string.netfunnel_error_msg);
                        }
                    }
                }
        );
    }

    /**
     * Netfunnel 에러 팝업을 띄어준다.
     *
     * @param msgId
     */
    private void showNetfunnelErrorPopup(int msgId) {
        DialogUtil.alert(
                PatternAuthActivity.this,
                msgId,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }
        );
    }

// ==============================================================================================================
// API
// ==============================================================================================================
    private void loginProcess(String imei, String uuid, String nData, String wData) {
        MLog.d();

        boolean isFingerPrint = (DataUtil.isNotNull(mCommonUserInfo) && !mCommonUserInfo.isLoginPattern());

        Map param = new HashMap();
        param.put("LOIN_DVCD", isFingerPrint ? "02" : "01");          // 02=지문, 01=패턴
        param.put("MBR_NO", LoginUserInfo.getInstance().getMBR_NO()); // 회원번호
        param.put("DEVICE_OS", "android");                            // 802 os
        param.put("DEVICE_INFO1", imei);                              // 802 IMEI
        param.put("DEVICE_INFO2", uuid);                              // 802 UUID
        param.put("DEVICE_INFO3", FDSManager.getInstance().getEncIMEI121(PatternAuthActivity.this));
        param.put("N_DATA", nData);                                   // 801 NDATA
        param.put("W_DATA", wData);                                   // 801 WDATA
        param.put("PUSH_APSF_ID", FLKPushAgentSender.mAppName);       // App id
        param.put("PUSH_REG_ID", Prefer.getPushRegID(PatternAuthActivity.this));        // Push id
        param.put("PUSH_GCM_TKN", Prefer.getPushGCMToken(PatternAuthActivity.this));    // Gcm id
        param.put("APSF_VRSN", Utils.getVersionName(PatternAuthActivity.this));         // App Version
        param.put("TRMN_APSF_VRSN", Utils.getVersionName(PatternAuthActivity.this));
        param.put("TRMN_OPSY_VRSN", Build.VERSION.RELEASE);
        param.put("CMCM_NM", Utils.getOperator(PatternAuthActivity.this));

        //오픈뱅킹 7일간 보지 않기 날짜..
        String OBA_RE_AGR_PUP_NT_EPOS_END_DT = Prefer.getStringPreference(this,"OBA_RE_AGR_PUP_NT_EPOS_END_DT");
        param.put("OBA_RE_AGR_PUP_NT_EPOS_END_DT", OBA_RE_AGR_PUP_NT_EPOS_END_DT);

        mIsFingerprintLogin = false;
        mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
        mCommonUserInfo.setLoginPattern(true);
        // showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A05.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                // 넷퍼넬을 종료한다.
                Netfunnel.END();

                if (DataUtil.isNull(ret)) {
                    dismissProgressDialog();
                    showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
                    return;
                } else {
                    try {
                        JSONObject object = new JSONObject(ret);
                        if (DataUtil.isNull(object)) {
                            dismissProgressDialog();
                            showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (DataUtil.isNull(msg)) {
                                msg = getString(R.string.common_msg_no_reponse_value_was);
                            }
                            dismissProgressDialog();
                            showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                            return;
                        }

                        // 로그인 세션 초기화
                        LoginUserInfo.clearInstance();

                        // 로그인 세션을 갱신처리
                        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
                        loginUserInfo.syncLoginSession(ret);
                        loginUserInfo.setLogin(true);

                        // 자동 로그아웃 시간 체크
                        LogoutTimeChecker.clearInstance();
                        LogoutTimeChecker.getInstance(PatternAuthActivity.this).autoLogoutStart();

                        // SSENSTONE 인증
                        MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                            @Override
                            public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                                if ("0000".equals(resultCode)) {
                                    LoginUserInfo.getInstance().setMOTPSerialNumber(reqData1);
                                }
                            }
                        });

                        /**
                         * 넷퍼넬 관련 코드 수정
                         * 앱 쉬트를 적용하게 되면 메인화면으로 이동시 약간의 딜레이가 발생하며 기다리는 현상이 발생한다.
                         * 그래서 화면이동 에니메이션과 동시에 사라지도록 코드 추가함.
                         * */
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                dismissProgressDialog();
                            }
                        }, 500);

                        // 신분증 보완 체크 (상태코드가 2 또는 3이면 해당 페이지 이동)
                        String strIdConfirm = loginUserInfo.getIDNF_CNFR_PRGS_STCD();
                        if (DataUtil.isNotNull(strIdConfirm)
                                && ("2".equals(strIdConfirm) || "3".equals(strIdConfirm))) {
                            Intent intent = new Intent(PatternAuthActivity.this, WebMainActivity.class);
                            intent.putExtra("url", WasServiceUrl.LGN0020100.getServiceUrl());
                            startActivity(intent);
                            finish();
                            return;
                        }

                        // cdd/edd 고객 재확인 평가일자 체크
                        String strCddElpsCount = loginUserInfo.getCDD_EDD_CUST_CNFR_RESS_ELPS_DCNT();
                        int cddElpsCount = DataUtil.isNull(strCddElpsCount) ? 99999 : Integer.parseInt(strCddElpsCount);
                        boolean needOpenCDD = false;
                        String curServerTime = loginUserInfo.getSYS_DTTM();
                        String savedServerTime = Prefer.getCDDEDDNotOpenFor1Week(PatternAuthActivity.this);
                        // 이전에 7일간 열지않기를 선택했을 때 저장된 서버 시간 체크
                        if (DataUtil.isNotNull(curServerTime) && DataUtil.isNotNull(savedServerTime)) {
                            // 현재 서버 시간과 이전에 앱에 저장된 서버 시간을 모두 가지고 있는 경우
                            Calendar calendar = Calendar.getInstance();
                            calendar.set(Integer.parseInt(curServerTime.substring(0, 4)), Integer.parseInt(curServerTime.substring(4, 6)) - 1, Integer.parseInt(curServerTime.substring(6, 8)));
                            Calendar savedCalendar = Calendar.getInstance();
                            savedCalendar.set(Integer.parseInt(savedServerTime.substring(0, 4)), Integer.parseInt(savedServerTime.substring(4, 6)) - 1, Integer.parseInt(savedServerTime.substring(6, 8)));
                            if (((long) (60 * 60 * 24 * 7 * 1000.0)) < (calendar.getTimeInMillis() - savedCalendar.getTimeInMillis())) {
                                needOpenCDD = true;
                            }
                        }
                        // 이전에 앱에 저장된 서버 시간이 없는 경우
                        else if (DataUtil.isNull(savedServerTime)) {
                            needOpenCDD = true;
                        }
                        // 현재 서버 시간이 없거나 못 가져온 경우 디바이스 시간으로 서버 시간 설정하고 비교
                        else {
                            Calendar calendar = Calendar.getInstance();
                            Calendar savedCalendar = Calendar.getInstance();
                            savedCalendar.set(Integer.parseInt(savedServerTime.substring(0, 4)), Integer.parseInt(savedServerTime.substring(4, 6)) - 1, Integer.parseInt(savedServerTime.substring(6, 8)));
                            if (((long) (60 * 60 * 24 * 7 * 1000.0)) < (calendar.getTimeInMillis() - savedCalendar.getTimeInMillis())) {
                                needOpenCDD = true;
                            }
                        }

                        /**
                         * cdd/edd 남은 기간이 0보다 같거나 작으면 무조건 안내 페이지 접속
                         * cdd/edd 남은 기간이 0-30일이면 일주일간 보지않기 선택했는지 여부에 따라 안내 페이지 접속
                         * */
                        MLog.i("needOpenCDD >> " + needOpenCDD);
                        // cdd/edd 로그인
                        if ((cddElpsCount <= 0) || ((cddElpsCount > 0 && cddElpsCount <= 30) && needOpenCDD)) {
                            Intent intent = new Intent(PatternAuthActivity.this, WebMainActivity.class);
                            intent.putExtra("url", WasServiceUrl.LGN0010100.getServiceUrl());
                            startActivity(intent);
                        }else if(!TextUtils.isEmpty(LoginUserInfo.getInstance().getALNC_INST_PROP_NO())){
                            //제휴사계좌개설
                            Intent intent = new Intent(PatternAuthActivity.this, WebMainActivity.class);
                            intent.putExtra("url", WasServiceUrl.LGN0030100.getServiceUrl());
                            intent.putExtra(Const.INTENT_PARAM, "ALNC_INST_PROP_NO="+LoginUserInfo.getInstance().getALNC_INST_PROP_NO());
                            startActivity(intent);
                        }else {
                            // 로그인 성공 후 메인 페이지 이동
                            Intent intent;
                            if (!mCommonUserInfo.isChangedFinger()) {
                                intent = new Intent(PatternAuthActivity.this, Main2Activity.class);
                                if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL)) {
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
                                }
                            } else {
                                mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                                mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                                intent = new Intent(PatternAuthActivity.this, FingerprintActivity.class);
                                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                            }
                            startActivity(intent);
                        }

                        loginUserInfo.setFinishedLogin(false);
                        finish();

                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                }
            }
        });
    }
}
