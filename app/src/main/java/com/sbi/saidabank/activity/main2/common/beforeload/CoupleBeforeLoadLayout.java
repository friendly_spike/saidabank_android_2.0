package com.sbi.saidabank.activity.main2.common.beforeload;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

public class CoupleBeforeLoadLayout extends RelativeLayout {

    private LinearLayout mLayoutSum;

    public CoupleBeforeLoadLayout(Context context) {
        super(context);
        initUX(context);
    }

    public CoupleBeforeLoadLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_before_loaded_couple, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mLayoutSum = layout.findViewById(R.id.layout_sum);

        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        int radius = (int) Utils.dpToPixel(getContext(), 12);
                        mLayoutSum.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF", new int[]{ radius, radius, radius, radius }));

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    public void hideLayout() {
        if (getVisibility() != VISIBLE) return;
        animate()
                .alpha(0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(GONE);
                    }
                })
                .start();
    }



}
