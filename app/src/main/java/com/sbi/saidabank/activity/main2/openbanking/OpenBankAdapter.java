package com.sbi.saidabank.activity.main2.openbanking;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.brandongogetap.stickyheaders.exposed.StickyHeaderHandler;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.openbank.CellType;
import com.sbi.saidabank.define.datatype.openbank.CellTypeHeader;
import com.sbi.saidabank.define.datatype.openbank.CellTypeItem;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;


import java.util.ArrayList;
import java.util.List;

/**
 * Create by 20210119
 * 오픈뱅킹 리스트 리스트 출력
 *
 * Fragment에 출력되는 가장 상위 Adapter이다.
 * 내부적으로 입출력,예금,적금,수입증권,카드 등의 내부 리스트가 또있다.
 * 입출력 - ItemInOutAdapter
 * 그밖에 - ItemNormalAdapter
 *
 * 위 아이템을 2가지로 나눈이유는 입출력일때와 그밖에의 출력 형식이 완전 다르기 때문에
 * 각각의 Adapter에서 그려주는 편이 소스 독립적인 부분에서 훨나아보이기 때문이다.
 *
 */

public class OpenBankAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements StickyHeaderHandler {

    private ArrayList<CellType> mListItem;
    private ArrayList<OpenBankDataMgr.OpenBankGroupInfo> mListGroupInfo;
    private OnOpenBankActionListener mListener;

    public OpenBankAdapter(OnOpenBankActionListener listener){
        mListItem = new ArrayList<>();
        mListGroupInfo = new ArrayList<>();
        mListener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case CellType.ITEM_TOTAL:
                return ViewHolderTotal.newInstance(parent);
            case CellType.HEADER:
                return ViewHolderHeader.newInstance(parent);
            case CellType.ITEM_BANNER:
                return ViewHolderBanner.newInstance(parent);
            case CellType.ITEM_ACC:
                return ViewHolderAcc.newInstance(parent);
            case CellType.ITEM_ETC:
                return ViewHolderEtc.newInstance(parent);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        int groupPos = mListItem.get(position).getGroupPosition();
        int itemPos = mListItem.get(position).getAccountPos();

        if (holder instanceof ViewHolderTotal) {
            int cnt = OpenBankDataMgr.getInstance().getAccountCnt();
            String amountStr = OpenBankDataMgr.getInstance().getTotalAmountString();
            ((ViewHolderTotal) holder).onBindView(cnt,amountStr);
        }else if (holder instanceof ViewHolderHeader) {
            ((ViewHolderHeader) holder).onBindView(mListGroupInfo.get(groupPos));
        }else if (holder instanceof ViewHolderAcc) {
            ((ViewHolderAcc) holder).onBindView(groupPos,mListGroupInfo.get(groupPos),itemPos,mListener);
        }else if (holder instanceof ViewHolderEtc) {
            ((ViewHolderEtc) holder).onBindView(mListener);
        }else if (holder instanceof ViewHolderBanner) {
            ((ViewHolderBanner) holder).onBindView();
        }
    }

    @Override
    public int getItemViewType(int position) {
        return mListItem.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return mListItem.size();
    }

    //StickyHeaderHandler
    @Override
    public List<CellType> getAdapterData() {
        return mListItem;
    }

    public OpenBankAccountInfo getAccountInfo(int grpIdx,int accIdx){
        if(mListGroupInfo.size() == 0) return null;
        if(mListGroupInfo.get(grpIdx).getAccountList().size() == 0) return null;
        return mListGroupInfo.get(grpIdx).getAccountList().get(accIdx);
    }

    public void makeOpenBankList(){

        if(mListGroupInfo.size() > 0){
            for(int i=0;i<mListGroupInfo.size();i++){
                mListGroupInfo.get(i).getAccountList().clear();
            }
            mListGroupInfo.clear();
        }

        mListGroupInfo.addAll(OpenBankDataMgr.getInstance().getCopyGroupList());

        mListItem.clear();
        //리스트의 아이템들은 순서대로 추가되어야 한다.
        mListItem.add(new CellTypeItem(CellType.ITEM_TOTAL,-1));

        boolean add_banner= true;
        //급여이체 배너 노출여부
        String SLRY_TRNF_YN = OpenBankDataMgr.getInstance().getSLRY_TRNF_YN();
        if(SLRY_TRNF_YN.equalsIgnoreCase("X")){
            add_banner= false;
        }

        for(int grpIdx=0;grpIdx<mListGroupInfo.size();grpIdx++){
            mListItem.add(new CellTypeHeader(grpIdx));
            if(grpIdx==0 && add_banner) {
                mListItem.add(new CellTypeItem(CellType.ITEM_BANNER, grpIdx, -1));
                add_banner = false;
            }
            for(int accIdx=0;accIdx<mListGroupInfo.get(grpIdx).getAccountList().size();accIdx++){
                mListItem.add(new CellTypeItem(CellType.ITEM_ACC,grpIdx,accIdx));
            }
        }

        mListItem.add(new CellTypeItem(CellType.ITEM_ETC,-1));

        notifyDataSetChanged();
    }
}
