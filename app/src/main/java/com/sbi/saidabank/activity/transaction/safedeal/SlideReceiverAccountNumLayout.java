package com.sbi.saidabank.activity.transaction.safedeal;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.sbi.saidabank.R;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.activity.transaction.adater.SafeDealRecentlyAdapter;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.AlertRemitteeDialog;
import com.sbi.saidabank.common.dialog.TransferEditAliasDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

//커스텀 뷰의 초기화 순서
//1.onAttachedToWindow : 0
//2.onMeasure : 0
//3.onLayout : 1294..
//4.onGlobalLayout - 우린 여기서 옵져버로 외부에서 셋해놓을수 있다.

/**
 * 받는분 계좌번호 입력 슬라이드 화면
 */

public class SlideReceiverAccountNumLayout extends SlideBaseLayout implements CustomNumPadLayout.OnNumPadClickListener,View.OnClickListener{
    private static final String TAG = SlideReceiverAccountNumLayout.class.getSimpleName();

    private ArrayList<ITransferRecentlyInfo> mArrayRecently;

    private LinearLayout mLayoutBankStock;
    private ImageView mIvBankIcon;
    private TextView  mTvBankName;

    private EditText mEtAccNum;
    private TextView mTvSelectBankStock;
    private ImageView mBtnFavorite;
    private Button mBtnOk;

    private RelativeLayout mLayoutNumPad;
    private RelativeLayout mLayoutRecently;

    private LinearLayout   mEmptyRecentlyLayout;
    private SafeDealRecentlyAdapter mRecentlyAdapter;
    private SwipeMenuListView mRecentlyListView;

    private int mSafeDealKind;
    private boolean mRequestRecentlyFinish;

    public SlideReceiverAccountNumLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideReceiverAccountNumLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @Override
    public void clearData(){
        mIvBankIcon.setVisibility(GONE);
        mTvBankName.setText("은행선택");
        mTvBankName.setTextColor(Color.parseColor("#888888"));
        mEtAccNum.setText("");
        mBtnFavorite.setVisibility(GONE);
        mBtnOk.setEnabled(false);
        mLayoutNumPad.setVisibility(INVISIBLE);
        mLayoutRecently.setVisibility(VISIBLE);
    }

    private void initUX(Context context){
        setTag("SlideReceiverAccountNumLayout");
        mArrayRecently = new ArrayList<>();

        View layout = View.inflate(context, R.layout.layout_safedeal_slide_receiver_account_num, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);

        mIvBankIcon = layout.findViewById(R.id.iv_bank_img);
        mTvBankName = layout.findViewById(R.id.tv_bank_name);

        mTvSelectBankStock = layout.findViewById(R.id.tv_select_bank_stock);
        mTvSelectBankStock.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionlistener.onActionOpenBank();
            }
        });

        mEtAccNum = layout.findViewById(R.id.et_account_num);
        mEtAccNum.setVisibility(GONE);
        mEtAccNum.setTextIsSelectable(true);
        mEtAccNum.setShowSoftInputOnFocus(false);

        mEtAccNum.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                showFavoriteBtnList(!hasFocus);
            }
        });

        mEtAccNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!TextUtils.isEmpty(mTvBankName.getText().toString()) && mIvBankIcon.getVisibility() == VISIBLE) {

                    if (editable.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        mBtnOk.setEnabled(true);
                    } else{
                        mBtnOk.setEnabled(false);
                    }
                }
            }
        });

        mEtAccNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    String bankName = mTvBankName.getText().toString();
                    String accountNum = mEtAccNum.getText().toString();
                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum))
                        return false;

                    if (mEtAccNum.isFocused()) {
                        Utils.hideKeyboard(getContext(), mEtAccNum);
                    }

                    if (accountNum.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        mBtnOk.performClick();
                    } else {
                        mActionlistener.showErrorMessage(String.format(getContext().getString(R.string.msg_input_limit_length_account), Const.MAX_TRANSFER_ACOOUNT_LENGTH));
                    }
                }
                return false;
            }
        });

        mLayoutNumPad = findViewById(R.id.layout_numpad);
        mLayoutNumPad.setVisibility(GONE);
        
        CustomNumPadLayout numPadLayout = layout.findViewById(R.id.numpad);
        numPadLayout.setNumPadClickListener(this);

        //다음 버튼
        mLayoutBankStock = layout.findViewById(R.id.layout_bank_stock);
        mLayoutBankStock.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mActionlistener.onActionOpenBank();
            }
        });
        mLayoutBankStock.setVisibility(INVISIBLE);

        //입력창 레이아웃
        RelativeLayout editTextLayout = layout.findViewById(R.id.layout_edittext);
        int radius_et = (int) Utils.dpToPixel(getContext(),7f);
        editTextLayout.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF5F5F5),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5)));

        mBtnFavorite = layout.findViewById(R.id.iv_favorite);
        mBtnFavorite.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mLayoutNumPad.getVisibility() == VISIBLE){
                    mEtAccNum.clearFocus();
                }else{
                    mEtAccNum.requestFocus();
                }

            }
        });
        mBtnFavorite.setVisibility(GONE);

        mBtnOk = layout.findViewById(R.id.btn_ok);
        mBtnOk.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
               String bankCD = (String)mTvBankName.getTag();
               String accNum = mEtAccNum.getText().toString();

                //수취인조회
                requestVerifyTransferAccountReceiver(bankCD,accNum);
            }
        });


        //최근/자주 리스트 설정
        mLayoutRecently = layout.findViewById(R.id.layout_recently);
        mEmptyRecentlyLayout = layout.findViewById(R.id.layout_empty_recently);
        mRecentlyAdapter = new SafeDealRecentlyAdapter(getContext(), 0, mArrayRecently, new SafeDealRecentlyAdapter.OnItemClickListener() {
            @Override
            public void OnFavoriteListener(int position) {
                requestUpdateFavoriteRecentlyList(position);
            }
        });
        mRecentlyListView = layout.findViewById(R.id.listview_recently);
        mRecentlyListView.setAdapter(mRecentlyAdapter);
        mRecentlyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ITransferRecentlyInfo info = mArrayRecently.get(position);

                Uri iconUri = ImgUtils.getIconUri(mActivity,info.getMNRC_BANK_CD());
                if(iconUri != null){
                    mIvBankIcon.setImageURI(iconUri);
                }else{
                    mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);
                }

                mTvBankName.setText(info.getMNRC_BANK_NM());
                mTvBankName.setTag(info.getMNRC_BANK_CD());

                if(mEtAccNum.getText().toString().length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH){
                    mBtnOk.setEnabled(true);
                }else{
                    mBtnOk.setEnabled(false);
                }

                mEtAccNum.setText(info.getMNRC_ACNO());

                //보여야할 뷰와 사라져야할 뷰.
                mTvSelectBankStock.setVisibility(GONE);
                mLayoutBankStock.setVisibility(VISIBLE);
                mEtAccNum.setVisibility(VISIBLE);
                mBtnFavorite.setVisibility(VISIBLE);
                if(mRecentlyListView.getVisibility() == VISIBLE){
                    mBtnFavorite.setImageResource(R.drawable.btn_favorite_input_on);
                }else{
                    mBtnFavorite.setImageResource(R.drawable.btn_favorite_input_off);
                }

                //수취인조회
                requestVerifyTransferAccountReceiver(info.getMNRC_BANK_CD(),info.getMNRC_ACNO());
            }
        });


        //최근/자주 리스트 슬라이드 메뉴 정의
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem editItem = new SwipeMenuItem(getContext());
                editItem.setBackground(new ColorDrawable(Color.rgb(0x8C, 0x97, 0xAC)));
                editItem.setWidth((int) Utils.dpToPixel(getContext(), 66));
                editItem.setIcon(R.drawable.btn_write_w);
                menu.addMenuItem(editItem);

                SwipeMenuItem deleteItem = new SwipeMenuItem(getContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xE0, 0x25, 0x0C)));
                deleteItem.setWidth((int) Utils.dpToPixel(getContext(), 66));
                deleteItem.setIcon(R.drawable.btn_delete_w);
                menu.addMenuItem(deleteItem);
            }
        };

        mRecentlyListView.setMenuCreator(creator);
        mRecentlyListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        mRecentlyListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        showEditAliasDialogRecently(position);
                        break;
                    case 1:
                        showRemoveRecentlyDialog(position);
                        break;
                    default:
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });


        initLayoutSetBottom(false,true);
    }

    @Override
    public void onCancel() {
        //super.onCancel();
        //if(isScale) return;

        mIvBankIcon.setVisibility(GONE);
        mTvBankName.setText("은행선택");
        mEtAccNum.setText("");
        mEtAccNum.clearFocus();

        mEtAccNum.clearFocus();
    }

    @Override
    public void onNumPadClick(String numStr) {


        switch (numStr){
            case "cancel": {
                mEtAccNum.setText("");
                break;
            }
            case "del": {
                deleteString();
                break;
            }
            default: {
                setNumberString(numStr);
                break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_ok:
                break;
        }
    }

    private void deleteString(){
        int cursor_start = mEtAccNum.getSelectionStart();
        int cursor_end = mEtAccNum.getSelectionEnd();

        String inputText = mEtAccNum.getText().toString();

        String frontStr = inputText.substring(0,cursor_start);
        String backStr = inputText.substring(cursor_end,inputText.length());

         if(cursor_start == cursor_end){
             if(cursor_start == 0) return;

             String newFrontStr = frontStr.substring(0,cursor_start-1);
             mEtAccNum.setText(newFrontStr+backStr);
             mEtAccNum.setSelection(newFrontStr.length());
         }else{
             mEtAccNum.setText(frontStr+backStr);
             mEtAccNum.setSelection(frontStr.length());
         }
    }

    private void setNumberString(String numStr){
        int cursor_start = mEtAccNum.getSelectionStart();
        int cursor_end = mEtAccNum.getSelectionEnd();

        String inputText = mEtAccNum.getText().toString();

        if(inputText.length() >= 20) return;

        String frontStr = inputText.substring(0,cursor_start);
        String backStr = inputText.substring(cursor_end,inputText.length());

        String newStr = frontStr + numStr + backStr;

        mEtAccNum.setText(newStr);
        mEtAccNum.setSelection(frontStr.length() + 1);
    }

    /**
     * 은행 선택 화면에서 값을 넣어준다.
     * @param bank_cd
     * @param bank_name
     */
    public void setBankInfo(String bank_cd,String bank_name){
        Uri iconUri = ImgUtils.getIconUri(mActivity,bank_cd);
        if(iconUri != null){
            mIvBankIcon.setImageURI(iconUri);
        }else{
            mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);
        }

        mTvBankName.setText(bank_name);
        mTvBankName.setTag(bank_cd);

        if(mEtAccNum.getText().toString().length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH){
            mBtnOk.setEnabled(true);
        }else{
            mBtnOk.setEnabled(false);
        }
        mEtAccNum.setVisibility(VISIBLE);
        mEtAccNum.requestFocus();

        mTvSelectBankStock.setVisibility(GONE);
        mLayoutBankStock.setVisibility(VISIBLE);

        showFavoriteBtnList(false);
    }


//    private int getImageBank(String code) {
//        int resourceID = -1;
//
//        String resourcename = "img_logo_" + code;
//        if ("028".equalsIgnoreCase(code)) {
//            resourcename = "img_logo_000";
//        } else if ("032".equalsIgnoreCase(code)) {
//            resourcename = "img_logo_224";
//        }
//
//        resourceID = getContext().getResources().getIdentifier(resourcename, "drawable", getContext().getPackageName());
//
//        return resourceID;
//    }




    @Override
    public void slidingUp(final int aniTime,int delayTime){
        mLayoutBankStock.setVisibility(INVISIBLE);
        mEtAccNum.setVisibility(INVISIBLE);
        mBtnFavorite.setVisibility(INVISIBLE);
        mTvSelectBankStock.setVisibility(VISIBLE);

        super.slidingUp(aniTime,delayTime);
    }


    /**
     * 최근/자주 이체 계좌 조회
     */
    public void requestRecentlyList() {
        MLog.d();

        Map param = new HashMap();
        param.put("INQ_DVCD", "1");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0140100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                mRequestRecentlyFinish = true;
                if(mActionlistener.onCheckHttpError(ret,false)){
                    mEmptyRecentlyLayout.setVisibility(VISIBLE);
                    mRecentlyListView.setVisibility(GONE);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) {
                        mEmptyRecentlyLayout.setVisibility(VISIBLE);
                        mRecentlyListView.setVisibility(GONE);
                        return;
                    }

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        mArrayRecently.add(new ITransferRecentlyInfo(obj));
                    }

                    if(mArrayRecently.size() > 0){
                        mEmptyRecentlyLayout.setVisibility(GONE);
                        mRecentlyListView.setVisibility(VISIBLE);
                    }else{
                        mEmptyRecentlyLayout.setVisibility(VISIBLE);
                        mRecentlyListView.setVisibility(GONE);
                    }

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 계좌 별칭 수정 화면 표시
     *
     * @param position 선택된 리스트 아이템 위치
     */
    void showEditAliasDialogRecently(final int position) {
        MLog.d();

        TransferEditAliasDialog dialog = new TransferEditAliasDialog(getContext(), new TransferEditAliasDialog.OnBtnClickListener() {
            @Override
            public void onBtnClick(View v, String alias) {
                requestRegisterAlias(position, alias);
            }
        });
        dialog.show();
    }

    /**
     * 계좌별칭 등록/변경 요청
     *
     * @param position 선택위치
     */
    private void requestRegisterAlias(final int position, final String alias) {
        MLog.d();

        ITransferRecentlyInfo ITransferRecentlyInfo = mArrayRecently.get(position);

        Map param = new HashMap();
        param.put("CHNG_DVCD", "1");
        param.put("FIN_INST_CD", ITransferRecentlyInfo.getMNRC_BANK_CD());
        param.put("BANK_ACNO", ITransferRecentlyInfo.getMNRC_ACNO());
        param.put("ACCO_ALS", alias);

        mActionlistener.showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                mActionlistener.dismissProgressDialog();

                if(mActionlistener.onCheckHttpError(ret,false)) return;

                try {

                    JSONObject object = new JSONObject(ret);

                    // 계좌변경 성공여부
                    if (!Const.REQUEST_WAS_YES.equalsIgnoreCase(object.optString("TRTM_RSLT_CD"))) {
                        mActionlistener.showErrorMessage(getContext().getString(R.string.msg_fail_edit_alias));
                        return;
                    }

                    ITransferRecentlyInfo accountInfo = mArrayRecently.get(position);
                    accountInfo.setMNRC_ACCO_ALS(alias);
                    mArrayRecently.set(position, accountInfo);
                    mRecentlyAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 최근/자주  삭제메세지 화면 표시
     *
     * @param position 선택된 리스트 아이템 위치
     */
    void showRemoveRecentlyDialog(final int position) {
        MLog.d();

        DialogUtil.alert(getContext(), getContext().getString(R.string.msg_ask_remove), new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestRemoveRecentlyListItem(position);
                    }
                },
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
    }

    /**
     * 선택된 최근/자주 계좌 삭제 처리 요청
     *
     * @param position 선택된 리스트 아이템 위치
     */
    private void requestRemoveRecentlyListItem(final int position) {
        MLog.d();


        ITransferRecentlyInfo ITransferRecentlyInfo = mArrayRecently.get(position);

        Map param = new HashMap();
        param.put("REG_STCD", "0");
        param.put("CNTP_FIN_INST_CD", ITransferRecentlyInfo.getMNRC_BANK_CD());
        param.put("CNTP_BANK_ACNO", ITransferRecentlyInfo.getMNRC_ACNO());

        mActionlistener.showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0050100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                mActionlistener.dismissProgressDialog();

                if(mActionlistener.onCheckHttpError(ret,false)) return;

                mArrayRecently.remove(position);
                mRecentlyAdapter.notifyDataSetChanged();

            }
        });
    }

    /**
     * 즐겨찾기 계좌관리 요청
     *
     * @param position 최근/자주  리스트에서 선택된 계좌 위치
     */
    private void requestUpdateFavoriteRecentlyList(final int position) {
        MLog.d();

        ITransferRecentlyInfo ITransferRecentlyInfo = mArrayRecently.get(position);

        if (DataUtil.isNull(ITransferRecentlyInfo.getFAVO_ACCO_YN())
                || DataUtil.isNull(ITransferRecentlyInfo.getMNRC_BANK_CD())
                || DataUtil.isNull(ITransferRecentlyInfo.getMNRC_ACNO())) {
            return;
        }

        Map param = new HashMap();
        param.put("REG_STCD", Const.REQUEST_WAS_YES.equalsIgnoreCase(ITransferRecentlyInfo.getFAVO_ACCO_YN()) ? "1" : "0");
        param.put("CNTP_FIN_INST_CD", ITransferRecentlyInfo.getMNRC_BANK_CD());
        param.put("CNTP_BANK_ACNO", ITransferRecentlyInfo.getMNRC_ACNO());
        param.put("CNTP_ACCO_DEPR_NM", ITransferRecentlyInfo.getMNRC_ACCO_DEPR_NM());
        if (DataUtil.isNotNull(ITransferRecentlyInfo.getMNRC_ACCO_ALS())) {
            param.put("CNTP_ACCO_ALS", ITransferRecentlyInfo.getMNRC_ACCO_ALS());
        }

        mActionlistener.showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                mActionlistener.dismissProgressDialog();

                if(mActionlistener.onCheckHttpError(ret,false)) return;

                if (mArrayRecently != null){
                    ITransferRecentlyInfo accountInfo = (ITransferRecentlyInfo) mArrayRecently.get(position);
                    if(accountInfo.getFAVO_ACCO_YN().equals("Y")){
                        accountInfo.setFAVO_ACCO_YN("N");
                    }else{
                        accountInfo.setFAVO_ACCO_YN("Y");
                    }
                    mRecentlyAdapter.updateDataList(mArrayRecently);
                }
            }
        });
    }

    /**
     * 수취인 조회
     * @param bankStockCode 이체계좌 은행코드
     * @param transferAccount 이체계좌
     *
     */
    public void requestVerifyTransferAccountReceiver(final String bankStockCode, final String transferAccount) {

        Map param = new HashMap();
        param.put("TRTM_DVCD", "3");
        param.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());
        param.put("CNTP_FIN_INST_CD", bankStockCode);
        param.put("CNTP_BANK_ACNO", transferAccount);
        param.put("MNRC_CPNO", "");
        param.put("TRN_AMT", "");
        mActionlistener.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                mActionlistener.dismissProgressDialog();
                Logs.i("TRA0010400A01 : " + ret);

                if(mActionlistener.onCheckHttpError(ret,false)) return;


                try {
                    final JSONObject object = new JSONObject(ret);

                    String SAME_AMT_TRNF_YN = object.optString("SAME_AMT_TRNF_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SAME_AMT_TRNF_YN)) {
                        View.OnClickListener cancelClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        };

                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDlgVerifyTransferAccount(bankStockCode, transferAccount, object);
                            }
                        };

                        final AlertDialog alertDialog = new AlertDialog(getContext());
                        alertDialog.mPListener = okClick;
                        alertDialog.mNListener = cancelClick;
                        alertDialog.msg = "오늘 같은 분에게 동일한 금액을\n이체하셨습니다.\n정말 이체하시겠습니까?";
                        alertDialog.show();
                        return;
                    }

                    showDlgVerifyTransferAccount(bankStockCode, transferAccount, object);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });

    }


    /**
     * 수취인 조회 결과 화면 다이얼로그 표시
     * @param bankStockCode 이체계좌 은행코드
     * @param transferAccount 이체계좌
     * @param object 수취인 조회 결과값 (JSON object)
     */
    private void showDlgVerifyTransferAccount(final String bankStockCode, final String transferAccount,final JSONObject object) {

        if (DataUtil.isNull(object))
            return;

        final String recv_name = object.optString("RECV_NM");

        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransferSafeDealDataMgr.getInstance().setMNRC_ACCO_DEPR_NM(recv_name);
                TransferSafeDealDataMgr.getInstance().setMNRC_BANK_CD(bankStockCode);
                TransferSafeDealDataMgr.getInstance().setMNRC_BANK_NAME(mTvBankName.getText().toString());
                TransferSafeDealDataMgr.getInstance().setMNRC_ACNO(transferAccount);

                TransferSafeDealDataMgr.getInstance().setWTCH_ACCO_MRK_CNTN(recv_name);
                TransferSafeDealDataMgr.getInstance().setMNRC_ACCO_MRK_CNTN(LoginUserInfo.getInstance().getCUST_NM());
                mActionlistener.onActionNext(SlideReceiverAccountNumLayout.this,recv_name);
                slidingDown(0,500);

            }
        };

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        final AlertRemitteeDialog alertDialog = new AlertRemitteeDialog(getContext(),true);
        alertDialog.mPListener = okClick;
        alertDialog.mNListener = cancelClick;
        alertDialog.msg = String.format(getContext().getString(R.string.msg_confirm_received_account_01), recv_name);
        alertDialog.show();
    }

    public void setSafeDealKind(int kind){
        mSafeDealKind = kind;
//        if(mSafeDealKind != Const.SAFEDEAL_TYPE_MOENY){
//            mBtnFavorite.setVisibility(GONE);
//            mLayoutRecently.setVisibility(GONE);
//            mLayoutNumPad.setVisibility(VISIBLE);
//        }
    }

    /**
     * 즐겨찾기및 즐겨찾기 버튼을 출력하거나 숨긴다.
     * @param isShow
     */
    private void showFavoriteBtnList(boolean isShow){
        //if(mSafeDealKind != Const.SAFEDEAL_TYPE_MOENY) return;

        mBtnFavorite.setVisibility(VISIBLE);

        if(!isShow){
            startAnimateFadeInOut(mLayoutRecently,false,250,0,null);
            startAnimateFadeInOut(mLayoutNumPad,true,250,250,null);
            mBtnFavorite.setImageResource(R.drawable.btn_favorite_input_off);
        }else{
            startAnimateFadeInOut(mLayoutNumPad,false,250,0,null);
            startAnimateFadeInOut(mLayoutRecently,true,250,250,null);
            mBtnFavorite.setImageResource(R.drawable.btn_favorite_input_on);
            mEtAccNum.setText("");
        }
    }

    public boolean getRequestRecentlyFinishState(){
        return mRequestRecentlyFinish;
    }
}
