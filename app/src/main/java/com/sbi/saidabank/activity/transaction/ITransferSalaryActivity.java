package com.sbi.saidabank.activity.transaction;

import android.animation.Animator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.adater.ITransferSalaryAdapter;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSalaryActionListener;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SalaryCompleteDialog;
import com.sbi.saidabank.common.dialog.SlidingRetrySalaryDialog;
import com.sbi.saidabank.common.dialog.SlidingStopSalaryDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 급여이체 화면
 */

public class ITransferSalaryActivity extends BaseActivity implements View.OnClickListener,KeyboardDetectorRelativeLayout.IKeyboardChanged, OnITransferSalaryActionListener {

    private RelativeLayout mLayoutClose;
    private TextView mTvTitle;
    private TextView mTvSubTitle;
    private RelativeLayout mLayoutProgress;
    private View     mViewProgress;
    //private ImageView mIvPlayBtn;

    private RelativeLayout mLayoutPlayBtn;
    private TextView  mTvPlayText;

    private LinearLayout mLayoutBtn;
    private TextView mTvHistoryBtn;
    private TextView mTvChangeBtn;

    private RecyclerView mRecyclerView;
    private ITransferSalaryAdapter mAdapter;

    private LinearLayout mLayoutNoti;

    private boolean isNowBinding;
    private boolean isForcePause;
    private boolean isNeedRequestSalary;
    private boolean isShowExpireOpenBankReAgree;
    private int mCntPlayRequest;
    private EditText mFocusedEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_itransfer_salary);
        ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_NONE);
        mCntPlayRequest = 0;

        initViews();

        isNeedRequestSalary=true;

    }

    private void initViews(){
        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.layout_main);
        mRelativeLayout.addKeyboardStateChangedListener(this);

        //종료버튼
        mLayoutClose = findViewById(R.id.layout_close);
        mLayoutClose.setOnClickListener(this);

        mTvTitle = findViewById(R.id.tv_title);
        mTvSubTitle = findViewById(R.id.tv_subtitle);

        mLayoutProgress = findViewById(R.id.layout_progress);
        mViewProgress = findViewById(R.id.v_progress);
        mViewProgress.setVisibility(View.GONE);

        mLayoutPlayBtn = findViewById(R.id.layout_play_btn);
        mTvPlayText = findViewById(R.id.tv_play_text);
        mLayoutPlayBtn.setOnClickListener(this);

        int radius = (int) Utils.dpToPixel(this,5);
        findViewById(R.id.layout_progress).setBackground(GraphicUtils.getRoundCornerDrawable("#33ffffff",new int[]{radius,radius,radius,radius}));
        radius = (int) Utils.dpToPixel(this,30);
        findViewById(R.id.layout_list_body).setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,0,0}));

        mRecyclerView = findViewById(R.id.recyclerview);
        mAdapter = new ITransferSalaryAdapter(this,this);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mLayoutBtn = findViewById(R.id.layout_btn);
        mLayoutBtn.setVisibility(View.GONE);
        mTvHistoryBtn = findViewById(R.id.tv_history_btn);
        mTvHistoryBtn.setOnClickListener(this);
        mTvChangeBtn = findViewById(R.id.tv_change_btn);
        mTvChangeBtn.setOnClickListener(this);

        mTvHistoryBtn.setPaintFlags(mTvHistoryBtn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvChangeBtn.setPaintFlags(mTvChangeBtn.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);


        mLayoutNoti = findViewById(R.id.layout_noti);
        mLayoutNoti.setVisibility(View.GONE);

        drawPlayButton(false);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(isForcePause){
            isForcePause = false;
        }

        if(isNeedRequestSalary) {
            isNeedRequestSalary = false;

            //오픈뱅킹 만료 1개월전.
            String OBA_RESP_CD = LoginUserInfo.getInstance().getOBA_RESP_CD();
            if(TextUtils.isEmpty(OBA_RESP_CD)) OBA_RESP_CD = "0";
            if(OBA_RESP_CD.equalsIgnoreCase("1")&&!isShowExpireOpenBankReAgree){
                Intent intent = new Intent(ITransferSalaryActivity.this, WebMainActivity.class);
                intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0110100.getServiceUrl());
                startActivity(intent);
                isNeedRequestSalary=true;
                isShowExpireOpenBankReAgree=true;
            }else{
                requestSalayTransferList();
            }

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isForcePause = true;
    }

    @Override
    protected void onDestroy() {
        MLog.d();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY
           //||ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_STOP
        )
            return;

        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_close: {
                clickCloseBtn();
                break;
            }
            case R.id.layout_play_btn: {

                if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY){
                    //마지막 이체중일때는 중지할수 없다.
                    if(ITransferSalayMgr.getInstance().getCurrentPlayIndex()+1 == ITransferSalayMgr.getInstance().getSalayArrayListCount()-1){
                        //Logs.showToast(ITransferSalaryActivity.this,"마지막 이체중으로 중지할수 없음.");
                        return;
                    }
                    showProgressDialog();
                    isForcePause = true;
                }else{
                    mLayoutPlayBtn.setEnabled(false);
                    if(ITransferSalayMgr.getInstance().checkTotalTransferSumAmountLimit()){

                        if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PAUSE
                           ||ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_STOP){

                            SlidingRetrySalaryDialog dialog = new SlidingRetrySalaryDialog(ITransferSalaryActivity.this
                                    ,new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            requestCheckAmountToday();
                                        }
                                    },new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            mLayoutPlayBtn.setEnabled(true);
                                        }
                                    });
                            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mLayoutPlayBtn.setEnabled(true);
                                }
                            });
                            dialog.show();

                        }else{
                            requestCheckAmountToday();
                        }
                    }else{
                        String WTCH_LMIT_REMN_AMT = ITransferSalayMgr.getInstance().getWTCH_LMIT_REMN_AMT();
                        if(TextUtils.isEmpty(WTCH_LMIT_REMN_AMT)) WTCH_LMIT_REMN_AMT = "0";

                        WTCH_LMIT_REMN_AMT = Utils.moneyFormatToWon(Double.parseDouble(WTCH_LMIT_REMN_AMT));
                        setTitle("이체한도 초과입니다","이체한도 : 1일 / " + WTCH_LMIT_REMN_AMT + " 원",true);


                        String totalSumAmount = Utils.moneyFormatToWon(ITransferSalayMgr.getInstance().getTotalTransferAmount());
                        String msg = "1일 이체한도 "+ WTCH_LMIT_REMN_AMT + "원을 초과하셨습니다. \n급여이체 금액 : " + totalSumAmount +"원";

                        DialogUtil.alert(ITransferSalaryActivity.this, msg, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                ITransferSalayMgr.getInstance().getSalayArrayList().get(0).setIS_NEED_FOCUS(true);
//                                notifyDataChanged();
                                mLayoutPlayBtn.setEnabled(true);
                                ITransferSalayMgr.getInstance().getSalayArrayList().get(0).setIS_NEED_FOCUS(true);
                                ITransferSalayMgr.getInstance().getSalayArrayList().get(0).setIS_TOTAL_LIMIT_ERROR(true);
                                mAdapter.notifyItemChanged(0);
                            }
                        });
                    }
                }

                break;
            }
            case R.id.tv_history_btn: {
                Intent intent = new Intent(ITransferSalaryActivity.this, WebMainActivity.class);
                String url = WasServiceUrl.INQ0100100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                startActivity(intent);
                isNeedRequestSalary = true;
                break;
            }
            case R.id.tv_change_btn:{
                Intent intent = new Intent(ITransferSalaryActivity.this, WebMainActivity.class);
                String url = WasServiceUrl.TRA0200200.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                intent.putExtra(Const.INTENT_PARAM, "TRTM_DVCD=2");
                startActivity(intent);
                isNeedRequestSalary = true;
                break;
            }
        }
    }

    private void clickCloseBtn(){
        if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_NONE){
            AlertDialog alertDialog = new AlertDialog(ITransferSalaryActivity.this);
            alertDialog.msg = getString(R.string.msg_cancel_transfer_salary);
            alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
            alertDialog.mPListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MLog.d();
                    finish();
                }
            };
            alertDialog.mNListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            };
            alertDialog.show();
        }else if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PAUSE
                ||ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_STOP){

            SlidingStopSalaryDialog dialog = new SlidingStopSalaryDialog(ITransferSalaryActivity.this, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_COMPLETE);
                    drawCompleteState();
                    notifyDataChanged();
                }
            });
            dialog.show();
        }else if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_COMPLETE){
            finish();
        }
    }

    @Override
    public void onKeyboardShown() {
        Logs.e("onKeyboardShown");
        drawPlayButton(true);
    }

    @Override
    public void onKeyboardHidden() {
        Logs.e("onKeyboardHidden");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataChanged();
            }
        },50);
    }

    @Override
    public void checkRowAndPlayBtnState(){

        //해당 함수는 금액 입력시에 타이틀을 변경하기 위한 함수로
        //이체가 시작하면 업데이트를 하지 않도록 한다.
        if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY
        ||ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_STOP
        ||ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_COMPLETE)
            return;

        Logs.e("checkRowAndPlayBtnState !!!!!!!!!!!!");
        //전체 금액을 한번 체크한다.
        boolean hasError=false;
        for (int i= 0;i < ITransferSalayMgr.getInstance().getSalayArrayListCount(); i++) {
            ITransferSalayMgr.ITransferSalayInfo info = ITransferSalayMgr.getInstance().getSalayArrayList().get(i);
            if(info.getIS_NOW_ERROR()){
                setTitle(info.getMSG_TITLE_ERROR(),info.getMSG_SUBTITLE_ERROR(),true);

                drawPlayButton(true);
//                mIvPlayBtn.setImageResource(R.drawable.btn_play_disabled);
//                mIvPlayBtn.setEnabled(false);
                hasError = true;
                break;
            }
        }

        if(!hasError){
            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PAUSE){
                setTitle("이체가 일시정지 되었습니다","금액 변경 및 삭제 가능",false);
            }else{
                setTitle("이체정보를 확인해주세요","금액 변경 및 삭제 가능",false);
            }


            drawPlayButton(false);
//            mIvPlayBtn.setImageResource(R.drawable.btn_play_play);
//            mIvPlayBtn.setEnabled(true);
        }
    }

    private void requestSalayTransferList(){

        Map param = new HashMap();
        param.put("TRTM_DVCD", "2");
        param.put("BLNC_INQ_YN", "Y");

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0210100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if(onCheckHttpError(ret,true)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    ITransferSalayMgr.getInstance().clearSalaryInfo();//초기화진행
                    ITransferSalayMgr.getInstance().setACCO_BLNC(object.optString("ACCO_BLNC"));
                    ITransferSalayMgr.getInstance().setWTCH_POSB_AMT(object.optString("WTCH_POSB_AMT"));
                    ITransferSalayMgr.getInstance().setWTCH_LMIT_REMN_AMT(object.optString("WTCH_LMIT_REMN_AMT"));

                    JSONArray array = object.optJSONArray("REC_OUT");
                    if (DataUtil.isNull(array))
                        return;

                    ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();
                    salayInfoArrayList.clear();

                    //리스트를 만들어 준다.
                    for(int i=0;i<array.length();i++){
                        JSONObject objectItem = array.getJSONObject(i);
                        ITransferSalayMgr.ITransferSalayInfo info = new  ITransferSalayMgr.ITransferSalayInfo(array.length(),i,objectItem);
                        salayInfoArrayList.add(info);
                    }

                    if(array.length() > 0){
                        /**
                         * 중요!!
                         * 이체 금액을 다시 계산해서 가지고 있도록 한다.
                         */
                        ITransferSalayMgr.getInstance().reCalTransAmount();
                        notifyDataChanged();
                    }

                    //급여이체내역 및 계좌변경 버튼을 보여준다.
                    mLayoutBtn.setVisibility(View.VISIBLE);


                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    @Override
    public void showBottomNoti(boolean flag) {
        if(flag){
            mLayoutNoti.setVisibility(View.VISIBLE);
        }else{
            mLayoutNoti.setVisibility(View.GONE);
        }
    }

    @Override
    public void setTitle(String mainText, String subText, boolean isError) {
        mTvTitle.setText(mainText);
        mTvSubTitle.setText(subText);

        if(isError){
            mTvTitle.setTextColor(Color.parseColor(ITransferSalaryAdapter.ERROR_AMOUNT_TEXT_COLOR));
            drawPlayButton(true);
//            mIvPlayBtn.setImageResource(R.drawable.btn_play_disabled);
//            mIvPlayBtn.setEnabled(false);
        }else{
            mTvTitle.setTextColor(Color.parseColor("#ffffff"));
        }
    }

    @Override
    public void setFocusedEditText(EditText editText) {
        mFocusedEditText = editText;
    }

    @Override
    public void notifyDataChanged() {
        Logs.e("notifyDataChanged !!!!!!!!!!!!");
        isNowBinding = true;
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void setListBindingFlag(boolean flag) {
        isNowBinding = flag;
    }

    @Override
    public boolean getListBindingFlag(){
        return isNowBinding;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            // Pincode Auth
            case Const.REQUEST_SSENSTONE_AUTH:
                if (resultCode == RESULT_OK) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    if (DataUtil.isNotNull(commonUserInfo)) {
                        if ("P000".equalsIgnoreCase(commonUserInfo.getResultMsg())) {
                            requestSaveInfoSalaryTransfer(commonUserInfo.getSignData());
                        } else {
                            Utils.showToast(ITransferSalaryActivity.this, "인증에 실패했습니다.");
                            mLayoutPlayBtn.setEnabled(true);
                        }
                    }
                } else {
                    mLayoutPlayBtn.setEnabled(true);
                }
                break;

            case Const.REQUEST_OTP_AUTH:
                if (resultCode == RESULT_OK) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String otpAuthTools = data.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
                    if (DataUtil.isNotNull(commonUserInfo)
                            && DataUtil.isNotNull(otpAuthTools)
                            && ("3".equalsIgnoreCase(otpAuthTools) || "2".equalsIgnoreCase(otpAuthTools))
                            && data.hasExtra(Const.INTENT_OTP_AUTH_SIGN)) {
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        commonUserInfo.setSignData(data.getStringExtra(Const.INTENT_OTP_AUTH_SIGN));
                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                        Intent intent = new Intent(ITransferSalaryActivity.this, PincodeAuthActivity.class);
                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                    }
                } else {
                    mLayoutPlayBtn.setEnabled(true);
                }
                break;

            default:
                break;
        }
    }

    /** ########################################
     *
     *  여기서 부터 급여이체 프로세스 시작
     *
     *##########################################
     */

    /**
     * 1.이체전 금액 체크
     */
    public void requestCheckAmountToday(){

        Map param = new HashMap();

        try{
            JSONArray jsonArray = new JSONArray();

            ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();

            for(int i=0;i<salayInfoArrayList.size();i++){
                ITransferSalayMgr.ITransferSalayInfo salayInfo = salayInfoArrayList.get(i);

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("SLRY_TRNF_SQNO",String.valueOf(i));         //급여이체순번
                jsonObject.put("RPRS_FNLT_CD",salayInfo.getRPRS_FNLT_CD()); //대표금융기관코드
                jsonObject.put("DTLS_FNLT_CD",salayInfo.getDTLS_FNLT_CD()); //세부금융기관코드
                jsonObject.put("ACNO",salayInfo.getACNO());                 //계좌번호

                if(i==0){
                    jsonObject.put("TRTM_AMT",salayInfo.getTRTM_AMT());         //표시금액
                }else if(i==salayInfoArrayList.size()-1){
                    jsonObject.put("TRTM_AMT",salayInfo.getTRTM_AMT());         //표시금액
                }else{
                    jsonObject.put("TRTM_AMT",salayInfo.getREMAIN_AMT());       //표시금액
                }

                jsonArray.put(jsonObject);
            }
            param.put("REC_IN", jsonArray);
        }catch (JSONException e){
            Logs.printException(e);
            mLayoutPlayBtn.setEnabled(true);
            return;
        }


        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0210100A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(onCheckHttpError(ret,false)){
                    dismissProgressDialog();
                    mLayoutPlayBtn.setEnabled(true);
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);

                    JSONArray jsonArray = object.getJSONArray("REC_OUT");
                    if(jsonArray.length() == 0){
                        dismissProgressDialog();
                        showErrorMessage(getString(R.string.msg_debug_err_response), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mLayoutPlayBtn.setEnabled(true);
                            }
                        });
                        return;
                    }

                    //전자서명 데이타 요청.
                    transferSignData();
                } catch (JSONException e) {
                    MLog.e(e);
                    mLayoutPlayBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 2.계좌이체 전자 서명값 요청
     */
    private void transferSignData() {
        MLog.d();

        Map param = new HashMap();
        param.put("TRN_TYCD", "4");

        try{
            JSONArray jsonArray = new JSONArray();

            ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();

            for(int i=0;i<salayInfoArrayList.size()-1;i++){
                ITransferSalayMgr.ITransferSalayInfo outSalayInfo = salayInfoArrayList.get(i);
                ITransferSalayMgr.ITransferSalayInfo inSalayInfo = salayInfoArrayList.get(i+1);

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("WTCH_ACCO_SQNO",String.valueOf(i));
                jsonObject.put("WTCH_BANK_CD",outSalayInfo.getRPRS_FNLT_CD());
                jsonObject.put("WTCH_ACNO",outSalayInfo.getACNO());
                jsonObject.put("TRAM",outSalayInfo.getTRTM_AMT());
                jsonObject.put("MNRC_ACCO_SQNO",String.valueOf(i+1));
                jsonObject.put("MNRC_BANK_CD",inSalayInfo.getRPRS_FNLT_CD());
                jsonObject.put("MNRC_ACNO",inSalayInfo.getACNO());

                jsonArray.put(jsonObject);
            }
            param.put("REC_IN", jsonArray);
        }catch (JSONException e){
            Logs.printException(e);
            mLayoutPlayBtn.setEnabled(true);
            return;
        }

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    mLayoutPlayBtn.setEnabled(true);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    String ELEC_SGNR_VAL_CNTN = Utils.getJsonString(object, "ELEC_SGNR_VAL_CNTN");

                    if (DataUtil.isNotNull(ELEC_SGNR_VAL_CNTN)) {
                        showOTPPinActivity(ELEC_SGNR_VAL_CNTN);
                    }
                } catch (Exception e) {
                    MLog.e(e);
                    mLayoutPlayBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 3.mOTP, 타행 OTP, 핀코드 입력창 표시
     *
     * @param signData 전자서명된 이체정보
     */
    private void showOTPPinActivity(String signData) {
        MLog.d();
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (DataUtil.isNull(SECU_MEDI_DVCD)||mCntPlayRequest > 0) {
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setSignData(signData);
            commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
            Intent intent = new Intent(this, PincodeAuthActivity.class);
            intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
            intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
            intent.putExtra(Const.INTENT_TRN_CD, "EIF56021");
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        } else {
            Intent intent;
            if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {

                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    //double totalAmount = ITransferSalayMgr.getInstance().getTotalTransferAmount();
                    //OTP출력 조건이 전체 급여이체 금액이 아닌 첫번째 이체 시작 금액을 기준으로 한다.
                    double transAmount = 0;
                    String TRTM_AMT = ITransferSalayMgr.getInstance().getSalayArrayList().get(0).getTRTM_AMT();
                    if(!TextUtils.isEmpty(TRTM_AMT))
                        transAmount = Double.parseDouble(TRTM_AMT);
                    if (transAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EIF56021");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, Const.REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }

                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);

                intent = new Intent(this, OtherOtpAuthActivity.class);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    //double totalAmount = ITransferSalayMgr.getInstance().getTotalTransferAmount();
                    //OTP출력 조건이 전체 급여이체 금액이 아닌 첫번째 이체 시작 금액을 기준으로 한다.
                    double transAmount = 0;
                    String TRTM_AMT = ITransferSalayMgr.getInstance().getSalayArrayList().get(0).getTRTM_AMT();
                    if(!TextUtils.isEmpty(TRTM_AMT))
                        transAmount = Double.parseDouble(TRTM_AMT);
                    if (transAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EIF56021");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, Const.REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }


                intent = new Intent(this, OtpPwAuthActivity.class);
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY);

                intent.putExtra(Const.INTENT_OTP_SERIAL_NUMBER, ITransferDataMgr.getInstance().getOTP_SERIAL_NO());
                intent.putExtra(Const.INTENT_OTP_TA_VERSION, ITransferDataMgr.getInstance().getOTP_TA_VERSION());
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else {
                intent = new Intent(this, PincodeAuthActivity.class);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                //sign data, service id 추가
                commonUserInfo.setSignData(signData);
                intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                intent.putExtra(Const.INTENT_TRN_CD, "EIF56021");
                commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
                overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                return;
            }

            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, Const.REQUEST_OTP_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 4.OTP,핀코드까지 인증이 끝난 후 급여이체 정보를 계정계에 저장한다.
     * ROW 하나하나 이체를 실행한다.
     */
    private void requestSaveInfoSalaryTransfer(final String signData){
        Map param = new HashMap();


        //재시도일때 3번.
        if(mCntPlayRequest > 0)
            param.put("TRTM_DVCD",3);
        else
            param.put("TRTM_DVCD",1);

        try{
            JSONArray jsonArray = new JSONArray();

            ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();

            for(int i=0;i<salayInfoArrayList.size();i++){
                ITransferSalayMgr.ITransferSalayInfo salayInfo = salayInfoArrayList.get(i);

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("SLRY_TRNF_SQNO",String.valueOf(i));         //급여이체순번
                jsonObject.put("RPRS_FNLT_CD",salayInfo.getRPRS_FNLT_CD()); //대표금융기관코드
                jsonObject.put("DTLS_FNLT_CD",salayInfo.getDTLS_FNLT_CD()); //세부금융기관코드
                jsonObject.put("ACNO",salayInfo.getACNO());                 //계좌번호

                if(i==0){
                    jsonObject.put("TRTM_AMT",salayInfo.getTRTM_AMT());         //표시금액
                }else if(i==salayInfoArrayList.size()-1){
                    jsonObject.put("TRTM_AMT",salayInfo.getTRTM_AMT());         //표시금액
                }else{
                    jsonObject.put("TRTM_AMT",salayInfo.getREMAIN_AMT());       //표시금액
                }

                if(salayInfo.getIS_TRANSFER_COMPLETE())
                    jsonObject.put("TRNF_RSLT_YN","Y");                 //이체결과여부
                else
                    jsonObject.put("TRNF_RSLT_YN","N");                 //이체결과여부

                jsonArray.put(jsonObject);
            }
            param.put("REC_IN", jsonArray);
        }catch (JSONException e){
            Logs.printException(e);
            mLayoutPlayBtn.setEnabled(true);
            return;
        }


        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0210100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if(onCheckHttpError(ret,false)){
                    mLayoutPlayBtn.setEnabled(true);
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);

                    JSONArray jsonArray = object.getJSONArray("REC_TRAN");
                    if(jsonArray.length() == 0){
                        dismissProgressDialog();
                        showErrorMessage(getString(R.string.msg_debug_err_response), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mLayoutPlayBtn.setEnabled(true);
                            }
                        });
                        return;
                    }

                    //각각 이체를 진행한다.
                    setTitle("순서대로 이체하고 있습니다","이체 진행 중",false);

                    //혹시 에러나서 멈췄다가 다시 시작할수도 있기에 기존 에러 사항은 클리어해준다.
                    for(int i=0;i<ITransferSalayMgr.getInstance().getSalayArrayListCount();i++){
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(i).setIS_NOW_ERROR(false);
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(i).setMSG_TITLE_ERROR("");
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(i).setMSG_SUBTITLE_ERROR("");
                    }

                    ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_PLAY);
                    mViewProgress.setVisibility(View.VISIBLE);
                    mLayoutClose.setVisibility(View.GONE);
                    mLayoutBtn.setVisibility(View.GONE);

                    drawPlayButton(false);
                    mViewProgress.setBackgroundResource(R.drawable.background_itransfer_salary_progress_success);


                    requestSalaryTransfer(ITransferSalayMgr.getInstance().getCurrentPlayIndex(),signData);
                } catch (JSONException e) {
                    MLog.e(e);
                    mLayoutPlayBtn.setEnabled(true);
                }
            }
        });
    }


    /**
     * 5.실제 이체 요청
     * ROW 하나하나 이체를 실행한다.
     * @param startIndex
     */
    private void requestSalaryTransfer(final int startIndex,final String signData){

        ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();
        if(startIndex+1 ==  salayInfoArrayList.size()){
            ITransferSalayMgr.getInstance().getSalayArrayList().get(startIndex).setIS_TRANSFER_COMPLETE(true);
            ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_COMPLETE);
            drawProgressBar(startIndex);
            drawCompleteState();
            notifyDataChanged();
            return;
        }
        notifyDataChanged();
        mCntPlayRequest++;

        ITransferSalayMgr.ITransferSalayInfo outSalayInfo = salayInfoArrayList.get(startIndex);
        ITransferSalayMgr.ITransferSalayInfo inSalayInfo = salayInfoArrayList.get(startIndex+1);

        Map param = new HashMap();
        param.put("WTCH_ACCO_SQNO",String.valueOf(startIndex));
        param.put("WTCH_BANK_CD",outSalayInfo.getRPRS_FNLT_CD());
        param.put("WTCH_ACNO",outSalayInfo.getACNO());
        param.put("TRAM",outSalayInfo.getTRTM_AMT());
        param.put("MNRC_ACCO_SQNO",String.valueOf(startIndex+1));
        param.put("MNRC_BANK_CD",inSalayInfo.getRPRS_FNLT_CD());
        param.put("MNRC_ACNO",inSalayInfo.getACNO());
        param.put("ELEC_SGNR_SRNO",signData);

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0210100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if(onCheckHttpSalaryError(startIndex,ret)){
                    //mIvPlayBtn.setEnabled(true);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    String WTCH_LMIT_REMN_AMT = object.optString("WTCH_LMIT_REMN_AMT");//출금한도잔여금액
                    String TRNF_TRTM_RSLT = object.optString("TRNF_TRTM_RSLT");//이체처리결과
                    String RESP_CD = object.optString("RESP_CD");//응답코드
                    String EROR_SVC_NM = object.optString("EROR_SVC_NM");//오류명
                    String EROR_MSG_CNTN = object.optString("EROR_MSG_CNTN");//오류메시지내용


                    //각각 이체를 진행한다.
                    if(TRNF_TRTM_RSLT.equalsIgnoreCase("0")){
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(startIndex).setIS_TRANSFER_COMPLETE(true);
                        drawProgressBar(startIndex);
                        int nextIndex = startIndex + 1;
                        ITransferSalayMgr.getInstance().setCurrentPlayIndex(nextIndex);
                        if(!isForcePause){
                            requestSalaryTransfer(nextIndex,signData);
                        }else{
                            isForcePause=false;
                            ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_PAUSE);
                            setTitle("이체가 일시정지 되었습니다","금액 변경 및 삭제 가능",false);
                            drawPlayButton(false);
                            mLayoutClose.setVisibility(View.VISIBLE);
                            notifyDataChanged();
                        }
                    }else{

                        setTitle(EROR_SVC_NM,EROR_MSG_CNTN,true);
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(startIndex).setIS_NOW_ERROR(true);
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(startIndex).setMSG_TITLE_ERROR(EROR_SVC_NM);
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(startIndex).setMSG_SUBTITLE_ERROR(EROR_MSG_CNTN);
                        ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_STOP);
                        drawPlayButton(false);
//                        mIvPlayBtn.setImageResource(R.drawable.btn_play_play);
//                        mIvPlayBtn.setEnabled(true);
                        mLayoutClose.setVisibility(View.VISIBLE);
                        mViewProgress.setBackgroundResource(R.drawable.background_itransfer_salary_progress_fail);

                        notifyDataChanged();
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                    mLayoutPlayBtn.setEnabled(true);
                }

            }
        });
    }

    /**
     * 급여이체 에러 케이스가 다양하고 에러를 출력하는 다이얼로그가 일반 알럿을 사용하도록 기획되어
     * 공통함수를 사용하지 않고 급여이체용으로 따로 만들도록 한다.
     * @param ret
     * @return
     */
    public boolean onCheckHttpSalaryError(final int position,String ret) {
        if (DataUtil.isNull(ret)) {
            dismissProgressDialog();
            String msg = getString(R.string.msg_debug_no_response);
            showErrorMessage(msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawErrorStopCompleteState(position);
                }
            });
            return true;
        }

        try {
            JSONObject object = new JSONObject(ret);
            if (DataUtil.isNull(object)) {
                dismissProgressDialog();
                String msg = getString(R.string.msg_debug_err_response);
                showErrorMessage(msg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        drawErrorStopCompleteState(position);
                    }
                });
                return true;
            }

            JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
            String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
            if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                dismissProgressDialog();
                String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                if (TextUtils.isEmpty(msg))
                    msg = getString(R.string.common_msg_no_reponse_value_was);
                String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);

                //이상금융거래 에러 차단.
                if ("EEFN0306".equalsIgnoreCase(errCode) ||
                        "EEFN0307".equalsIgnoreCase(errCode) ||
                        "EEIF0516".equalsIgnoreCase(errCode)) {
                    Intent intent = new Intent(ITransferSalaryActivity.this, WebMainActivity.class);
                    String url = WasServiceUrl.ERR0030100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode +
                            "&" + Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                    intent.putExtra(Const.INTENT_PARAM, param);

                    startActivity(intent);
                    finish();
                    return true;
                }

                if(msg.contains("잔액부족")){
                    msg = "<b>급여이체 중단</b><br><br>계좌의 잔액이 부족합니다.<br>확인 후 다시 시도해주세요.";
                    DialogUtil.alert(ITransferSalaryActivity.this, msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawErrorStopCompleteState(position);
                        }
                    });
                }else if(msg.contains("잔액증명서")){
                    msg = "<b>급여이체 중단</b><br><br>당일 발급된 증명서로 인하여<br>이체가 실패되었습니다.<br>내일 다시 거래해주세요.";
                    DialogUtil.alert(ITransferSalaryActivity.this, msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawErrorStopCompleteState(position);
                        }
                    });
                }else if(msg.contains("출금정지")){
                    msg = "<b>급여이체 중단</b><br><br>출금정지계좌로 이체되었습니다.<br>거래내역을 확인하세요.";
                    DialogUtil.alert(ITransferSalaryActivity.this, msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawErrorStopCompleteState(position);
                        }
                    });
                }else if(msg.contains("재입금실패")){
                    msg = "<b>급여이체 중단</b><br><br>이체가 실패되었습니다.<br>거래내역을 재확인해주세요.";
                    DialogUtil.alert(ITransferSalaryActivity.this, msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            drawErrorStopCompleteState(position);
                        }
                    });
                }else{
                    msg = "급여이체 중단\n \n"+ msg;

                    CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                        @Override
                        public void onConfirmPress() {
                            drawErrorStopCompleteState(position);
                        }
                    };
                    showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, okClick);
                }

                return true;
            }

        } catch (JSONException e) {
            MLog.e(e);
        }

        return false;
    }

    /**
     * 각 이체 상태에따라 플레이 버튼의 상태를 그려준다.
     * @param isDisable
     */
    private void drawPlayButton(boolean isDisable){
        String backGroundColor = "#00ebff";
        switch (ITransferSalayMgr.getInstance().getTransferPlayState()){
            case ITransferSalayMgr.TRANSFER_STATE_NONE:
                if(isDisable) {
                    backGroundColor = "#9aa3ab";
                    mTvPlayText.setTextColor(Color.parseColor("#666666"));
                }else{
                    mTvPlayText.setTextColor(Color.parseColor("#000000"));
                }
                mTvPlayText.setText("시작하기");

                break;
            case ITransferSalayMgr.TRANSFER_STATE_PLAY:
                backGroundColor = "#080a0d";
                mTvPlayText.setText("일시정지");
                mTvPlayText.setTextColor(Color.parseColor("#ffffff"));
                break;
            case ITransferSalayMgr.TRANSFER_STATE_PAUSE:
            case ITransferSalayMgr.TRANSFER_STATE_STOP:
                mTvPlayText.setText("재시도");
                mTvPlayText.setTextColor(Color.parseColor("#000000"));
                break;
            case ITransferSalayMgr.TRANSFER_STATE_COMPLETE:
                backGroundColor = "#9aa3ab";
                mTvPlayText.setText("이체완료");
                mTvPlayText.setTextColor(Color.parseColor("#666666"));
                break;

        }

        mLayoutPlayBtn.setEnabled(!isDisable);

        int radius = (int) Utils.dpToPixel(this,12);
        mLayoutPlayBtn.setBackground(GraphicUtils.getRoundCornerDrawable(backGroundColor,new int[]{radius,radius,radius,radius}));

    }

    private void drawErrorStopCompleteState(int postion){
        drawCompleteState();
        ITransferSalayMgr.getInstance().setTransferPlayState(ITransferSalayMgr.TRANSFER_STATE_COMPLETE);
        drawPlayButton(true);
//        mIvPlayBtn.setImageResource(R.drawable.btn_play_disabled);
//        mIvPlayBtn.setEnabled(false);
        mLayoutNoti.setVisibility(View.VISIBLE);
        mLayoutClose.setVisibility(View.VISIBLE);

        ITransferSalayMgr.getInstance().getSalayArrayList().get(postion).setIS_NOW_ERROR(true);

        notifyDataChanged();
    }

    private void drawCompleteState(){
        if(ITransferSalayMgr.getInstance().getCompleteCount() == ITransferSalayMgr.getInstance().getSalayArrayListCount()){
            setTitle("이체가 완료되었습니다","거래내역 확인",false);
        }else{
            setTitle("이체가 일부 성공했습니다","",false);

            int completeCnt = ITransferSalayMgr.getInstance().getCompleteCount();
            String subtitle = (ITransferSalayMgr.getInstance().getSalayArrayListCount()-1) + "건 중 " + (completeCnt) + "건 이체완료";
            Utils.setTextWithSpan(mTvSubTitle,subtitle,String.valueOf(completeCnt), Typeface.NORMAL,"#009beb");
        }

        drawPlayButton(true);
//        mIvPlayBtn.setImageResource(R.drawable.btn_play_disabled);
//        mIvPlayBtn.setEnabled(false);
        mLayoutNoti.setVisibility(View.VISIBLE);
        mLayoutClose.setVisibility(View.VISIBLE);

        //완료다이얼로그 출력.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SalaryCompleteDialog dialog = new SalaryCompleteDialog(ITransferSalaryActivity.this);
                dialog.show();
            }
        },250);

    }

    private void drawProgressBar(int position){
        int progLayoutWidth = mLayoutProgress.getWidth();

        int progCnt = ITransferSalayMgr.getInstance().getSalayArrayListCount() - 1;

        float percent = ((float) position + 1f)/(float) progCnt;

        float progViewWidth = progLayoutWidth * percent;

        AniUtils.changeViewWidthSizeAnimation(mViewProgress,(int)progViewWidth,500,new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if(mViewProgress.getVisibility() != View.VISIBLE)
                    mViewProgress.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


    }


}