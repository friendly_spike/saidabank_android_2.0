package com.sbi.saidabank.activity.main2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.login.LogoutActivity;

import com.sbi.saidabank.activity.main2.common.banner.BannerLayout;
import com.sbi.saidabank.activity.main2.common.beforeload.MyAccBeforeLoadLayout;
import com.sbi.saidabank.activity.main2.common.clipboard.FastTransferLayout;
import com.sbi.saidabank.activity.main2.common.event.RollingEventLayout;
import com.sbi.saidabank.activity.main2.myaccountlist.Main2AccountAdapter;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;

import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.CheatUseAccountDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomScrollView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;

import com.sbi.saidabank.define.datatype.common.CheatUseAccountInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main.MainAdsItemInfo;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main2MyAccountFragment extends BaseFragment{
    Context mContext;

    private ProgressBarLayout mProgressLayout;

    private View mRelayTopMaginView;
    private BannerLayout mLayoutBanner;
    private CustomScrollView mScrollView;
    private Main2AccountAdapter mMyAccountAdapter;
    private RecyclerView mListView;
    private ArrayList<Main2AccountInfo> mMyAccountArray;

    private RelativeLayout mLayoutBtnReArrange;
    private MyAccBeforeLoadLayout mBeforeLoadLayout;
    private FastTransferLayout mFastTransferLayout;

    private RollingEventLayout mRollingEventLayout;
    private boolean isFistEnter;

    private CheatUseAccountDialog mCheatUseAccountDialog;
    private ArrayList<MainPopupInfo> mListPopupInfo;

    public static final Main2MyAccountFragment newInstance(){
        Main2MyAccountFragment f = new Main2MyAccountFragment();
        return f;
    }

    public Main2MyAccountFragment(){
        isFistEnter = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = getContext();
    }


    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_main2_home_myaccount, container, false);

        //placehloder view
        mBeforeLoadLayout = rootView.findViewById(R.id.layout_before_loaded);

        mProgressLayout = rootView.findViewById(R.id.ll_progress);

        mRelayTopMaginView = rootView.findViewById(R.id.layout_relay_top_margin);

        mLayoutBanner = rootView.findViewById(R.id.layout_banner);

        mRollingEventLayout = rootView.findViewById(R.id.layout_rolling_event);
        mRollingEventLayout.setVisibility(View.GONE);

        mScrollView = rootView.findViewById(R.id.scrollview);
        mScrollView.setOnScrollActionListener(new CustomScrollView.OnScrollActionListener() {
            @Override
            public void onScrollStart(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Logs.e("onScrollStart - oldScrollY : " + oldScrollY);
                //Logs.e("onScrollStart - scrollY : " + scrollY);
                ((Main2Activity)mContext).changeHomeFragmentScrollState(true);
                mFastTransferLayout.moveUpDown(true);
            }

            @Override
            public void onScrollStop(boolean isBottom) {
                //Logs.e("setOnScrollActionListener - onScrollStop");

                ((Main2Activity)mContext).changeHomeFragmentScrollState(false);
                mFastTransferLayout.moveUpDown(false);
            }

            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
//                Logs.e("onScrollChange - onScrollChange - v.getHeight() : " + v.getHeight());
//                Logs.e("onScrollChange - onScrollChange - oldScrollY : " + oldScrollY);
//                Logs.e("onScrollChange - onScrollChange - scrollY : " + scrollY);
            }
        });

        mMyAccountArray = new ArrayList<Main2AccountInfo>();
        mMyAccountAdapter = new Main2AccountAdapter(getContext(),Main2AccountAdapter.DISP_TYPE_MY);
        mListView = rootView.findViewById(R.id.listview);
        mListView.setAdapter(mMyAccountAdapter);
        mListView.setLayoutManager(new LinearLayoutManager(mContext));


        mLayoutBtnReArrange = rootView.findViewById(R.id.layout_btn_rearrange);
        mLayoutBtnReArrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), ReArrangePosMyAccountActivity.class);
                if(getActivity() != null && !getActivity().isFinishing())
                    getActivity().startActivity(intent);
            }
        });
        int radius = (int)Utils.dpToPixel(mContext,18f);
        mLayoutBtnReArrange.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));



        mFastTransferLayout  = rootView.findViewById(R.id.layout_fast_transfer);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        MLog.d();

        //리스트를 처음으로 올린다.
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);

            }
        });

        requestMainFullInfo();

        if(mLayoutBanner != null)
            mLayoutBanner.onResumeLayout();

        //Fragment가 시작할대는 홈메뉴는 항상보여야 한다.
        ((Main2Activity)mContext).changeHomeFragmentScrollState(false);
    }

    @Override
    public void onPause() {
        super.onPause();
        MLog.d();
        if(mLayoutBanner != null)
            mLayoutBanner.onPauseLayout();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mLayoutBanner != null)
            mLayoutBanner.onDestroyLayout();
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        //mProgressLayout.show();
        //홈탭에서는 프로그레스바 돌때 다른탭 이동 못하게.
        if(((BaseActivity)getActivity()) != null && !((BaseActivity)getActivity()).isFinishing())
            ((BaseActivity)getActivity()).showProgressDialog();
    }

    @Override
    public void dismissProgress() {
        //mProgressLayout.hide();
        if(((BaseActivity)getActivity()) != null && !((BaseActivity)getActivity()).isFinishing())
            ((BaseActivity)getActivity()).dismissProgressDialog();
    }

    public void displayRelayTopMaringView(boolean isShow){
        if(isShow){
            mRelayTopMaginView.setVisibility(View.VISIBLE);
        }else{
            mRelayTopMaginView.setVisibility(View.GONE);
        }
    }

    /**
     * 메인정보조회 시작
     */
    private void requestMainFullInfo() {
        MLog.d();
        //20201102 추가.
        //백그라운드에서 10분 후에 다시 시작시 Logout cheker에서 로그아웃화면으로 보내버리지만...
        //메인에서는 Resume에서 request를 한번 보내기때문에 서버측면에서는 에러가 발생할수 있다.
        //현업이 에러로그 쌓이는 것이 싫다고 해서 방어코드 한줄 추가했다.
        //이건 추가 않해도 괜찮으것 같은데.. 해달라니.. 해준다.
        if (getActivity() == null || getActivity().isFinishing()) return;
        //혹시 로그아웃되어있으면...
        if (!LoginUserInfo.getInstance().isLogin()) return;

        showProgress();
        Map param = new HashMap();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(getActivity() == null || getActivity().isFinishing()) return;

                if (TextUtils.isEmpty(ret)) {
                    dismissProgress();
                    Logs.e(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        dismissProgress();
                        if ("CMM0037".equals(errCode)) {
                            //로그인 중 분실신고기기 체크
                            if (Utils.isAppOnForeground(mContext)) {
                                LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
                                SaidaApplication mApplicationClass = (SaidaApplication) mContext.getApplicationContext();
                                mApplicationClass.allActivityFinish(true);
                                LoginUserInfo.clearInstance();
                                LoginUserInfo.getInstance().setLogin(false);
                                Intent intent = new Intent(mContext, LogoutActivity.class);
                                intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
                                mContext.startActivity(intent);
                            } else {
                                LoginUserInfo.getInstance().setLogin(false);
                                LogoutTimeChecker.getInstance(mContext).autoLogoutStop();
                                LogoutTimeChecker.getInstance(mContext).setBackground(true);
                            }
                        } else {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                            ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    //사기이용계좌 명의인 등록고객 거래제한
                    CheatUseAccountInfo.getInstance().setFRD_ACCO_REG_YN(object.optString("FRD_ACCO_REG_YN", Const.EMPTY));
                    CheatUseAccountInfo.getInstance().setFRD_ACCO_REG_FIN_INST_NM(object.optString("FRD_ACCO_REG_FIN_INST_NM", Const.EMPTY));

                    //팝업정보 조회
                    //20200929 - 이광호과장님요청 - 팝업이 먼저뜬 후 메인 갱신되도록 요청.
                    mListPopupInfo = new ArrayList<>();
                    JSONArray popup_array = object.optJSONArray("REC_POPUP");
                    if (popup_array != null) {
                        Logs.e("popup_array : " + popup_array.toString());
                        for (int index = 0; index < popup_array.length(); index++) {
                            JSONObject jsonObject = popup_array.getJSONObject(index);
                            if (jsonObject == null)
                                continue;

                            MainPopupInfo mainPopupInfo = new MainPopupInfo(jsonObject);
                            mListPopupInfo.add(mainPopupInfo);
                        }
                        Logs.e("listPopupInfo.size() : " + mListPopupInfo.size());
                        //팝업 오픈
                        final JSONObject jsonObject = object;
                        if(mListPopupInfo.size() > 0){
                            if(getActivity() != null && !getActivity().isFinishing()) {
                                //공지사항이 있고 사기계좌가 등록된 경우
                                if (CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_YN().equals("Y")){
                                    showCheatUseAccountDialog();

                                    //사기계좌 확인버튼 클릭시 하루 안보이게되어 체크후 공지사항 보여줌
                                    if (mCheatUseAccountDialog.oneDayAppInfo() == false){
                                        ((Main2Activity) getActivity()).showPopupDialog(mListPopupInfo);
                                    }

                                    mCheatUseAccountDialog.setOnCofirmListener(new CheatUseAccountDialog.OnConfirmListener() {
                                        @Override
                                        public void onConfirmPress() {
                                            ((Main2Activity) getActivity()).showPopupDialog(mListPopupInfo);
                                        }
                                    });
                                }else{
                                    //공지사항이 있고 사기계좌가 등록안된 경우
                                    showCheatUseAccountDialog();
                                    ((Main2Activity) getActivity()).showPopupDialog(mListPopupInfo);
                                }

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        loadBannerImage(jsonObject,true);
                                    }
                                }, 1000);
                                return;
                            }
                        }
                    }

                    //공지사항이 없고 사기계좌가 등록된 경우 or 공지사항이 없고 사기계좌가 등록안된 경우
                    showCheatUseAccountDialog();

                    //주의! 위 딜레이 코드 주의깊게 보도록.
                    loadBannerImage(object,false);

                } catch (JSONException e) {
                    dismissProgress();
                    Logs.printException(e);
                }

            }
        });
    }

    /**
     * 먼저 팝업 처리가 끝나고 출력되면 다음으로 배너 이미지를 조회한다.
     * @param jsonObject
     * @param closeProgress
     */
    private void loadBannerImage(final JSONObject jsonObject,boolean closeProgress) {
        if(closeProgress)
            dismissProgress();

        try {
            //배너 이미지를 다운로드 받는데 시간이 걸리니까.. 먼저 시작한다.
            //배너정보 조회
            ArrayList<MainAdsItemInfo> listBannerInfo = new ArrayList<>();
            JSONArray banner_array = jsonObject.optJSONArray("REC_BNR");
            if (banner_array != null) {

                for (int index = 0; index < banner_array.length(); index++) {
                    JSONObject object = banner_array.getJSONObject(index);
                    if (object == null)
                        continue;

                    MainAdsItemInfo adsItemInfo = new MainAdsItemInfo(object);
                    listBannerInfo.add(adsItemInfo);
                }
//                Logs.e("listBannerInfo.size() : " + listBannerInfo.size());
                if(listBannerInfo.size() > 0){
                    //배너 이미지 로딩 작업이 끝나면 나머지 리스트작업을 진행한다.
                    //리스트 화면과 배너가 같이 뜨도록 하려고 추가한다.요청사항임.
                    mLayoutBanner.setLoadFinishListener(new BannerLayout.OnLoadFinishListener() {
                        @Override
                        public void onLoadFinish() {
                            try {
                                parsorRequestMainInfo(jsonObject);
                            } catch (JSONException e) {
                                Logs.printException(e);
                            }
                        }
                    });
                    mLayoutBanner.setBannerInfo(listBannerInfo);
                    return;
                }
            }

            parsorRequestMainInfo(jsonObject);
        } catch (JSONException e) {
            dismissProgress();
            Logs.printException(e);
        }
    }

    private void parsorRequestMainInfo(JSONObject object) throws JSONException{
        Logs.d();
        dismissProgress();

        //이어하기 정보조회
        ArrayList<Main2RelayInfo> listRelayInfo = new ArrayList<>();
        JSONArray relay_array = object.optJSONArray("REC_WAIT");
        if (relay_array != null){
            Logs.e("relay_array : " + relay_array.toString());
            for (int index = 0; index < relay_array.length(); index++) {
                JSONObject jsonObject = relay_array.getJSONObject(index);
                if (jsonObject == null)
                    continue;

                Main2RelayInfo accountInfo = new Main2RelayInfo(jsonObject);
                String DSCT_CD = accountInfo.getDSCT_CD();
                if (TextUtils.isEmpty(DSCT_CD))
                    continue;

                listRelayInfo.add(accountInfo);
            }
            Logs.e("listRelayInfo.size() : " + listRelayInfo.size());
        }
        Main2DataInfo.getInstance().setMain2RelayArray(listRelayInfo);
        if(getActivity() != null && !getActivity().isFinishing()){
            ((Main2Activity) getActivity()).showRelayLayout(listRelayInfo);
        }

        //이어하기가 있으면 탑 마진값을 넣어준다.
        if(listRelayInfo.size() > 0 && DataUtil.isNotNull(getActivity())) {
            int height = (int)(int)getContext().getResources().getDimension(R.dimen.relay_small_mode_height_margin);
            AniUtils.changeViewHeightSizeAnimation(mRelayTopMaginView,height,150);
        }else{
            AniUtils.changeViewHeightSizeAnimation(mRelayTopMaginView,0,150);
        }


        //메인 레코드가 아닌 기본값 조회 - 계좌분리 리스트도 여기서 한다.
        Main2DataInfo.getInstance().setMain2DataInfoFromMyAccount(object);

        //계좌정보조회 ***********
        ArrayList<Main2AccountInfo> listAccountInfo = new ArrayList<>();
        JSONArray account_array = object.optJSONArray("REC");
        if (account_array == null) return;

        boolean isHaveDDAcc=false;//계좌 리스트에 요구불계좌 존재여부 체크
        boolean isSetCharge=false;
//        MLog.i(" ############### Main2 Account Start ###############");
        for (int index = 0; index < account_array.length(); index++) {
            JSONObject jsonObject = account_array.getJSONObject(index);
            if (jsonObject == null)
                continue;

//            try {
//                ParseUtil.parseJSONObject(jsonObject);
//                MLog.i("--------------------------------------------");
//            } catch (Exception e) {
//                MLog.e(e);
//            }

            Main2AccountInfo accountInfo = new Main2AccountInfo(jsonObject);
            String DSCT_CD = accountInfo.getDSCT_CD();
            if (TextUtils.isEmpty(DSCT_CD))
                continue;

            if(DSCT_CD.equals("300")||DSCT_CD.equals("330")){
                isHaveDDAcc = true;
            }

            //충전하기 설정여부를 가지고 있는다.
            String IMMD_WTCH_ACCO_YN = accountInfo.getIMMD_WTCH_ACCO_YN();
            if(!isSetCharge && IMMD_WTCH_ACCO_YN.equals("Y")){
                isSetCharge = true;
                Main2DataInfo.getInstance().setACCOUNT_CHARGE_SET_YN("Y");
            }
            listAccountInfo.add(accountInfo);
        }
        //계좌는 존재하나 요구불계좌가 없는 경우 셀을 하나 넣어준다.
        if(listAccountInfo.size() >0 && !isHaveDDAcc){
            Main2AccountInfo accountInfo = new Main2AccountInfo();
            accountInfo.setDSCT_CD("999");
            listAccountInfo.add(0,accountInfo);
        }

        Logs.e("listAccountInfo.size() : " + listAccountInfo.size());

        mMyAccountArray.clear();
        if(listAccountInfo.size() > 0){
            mMyAccountArray.addAll(listAccountInfo);
        }
        mMyAccountAdapter.setMyAccountArray(mMyAccountArray);
        Main2DataInfo.getInstance().setMain2Main2AccountInfo(mMyAccountArray);

        //순서변경 표시
        if(listAccountInfo.size() > 1){
            mLayoutBtnReArrange.setVisibility(View.VISIBLE);
        }else{
            mLayoutBtnReArrange.setVisibility(View.GONE);
        }
        MLog.i(" ############### Main2 Account End  ###############");


        //클립보드에 복사한 은행/계좌 정보가 있는지 체크하여 띄워준다.
        mFastTransferLayout.checkCopyClipboardText();

        mBeforeLoadLayout.hideLayout();


        //움직이는 이벤트 아이콘
        String MAIN_EVNT_PRTP_POSB_YN = object.optString("MAIN_EVNT_PRTP_POSB_YN");
        String EVNT_ID = object.optString("EVNT_ID");
        if (isFistEnter) {
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(MAIN_EVNT_PRTP_POSB_YN)) {
                if (TextUtils.isEmpty(EVNT_ID))
                    return;

                mRollingEventLayout.setEventID(EVNT_ID);
                mRollingEventLayout.showEventIcon();
                mRollingEventLayout.setVisibility(View.VISIBLE);
            }
            isFistEnter = false;
        }else{
            if(mRollingEventLayout.getChildCount() > 0)
                mRollingEventLayout.removeAllViews();
        }
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);
    }

    /**
     * 사기이용계좌 금융거래제한 안내 팝업
     */
    private void showCheatUseAccountDialog(){
        //사기이용계좌 금융거래제한 안내 팝업
        if (DataUtil.isNotNull(mCheatUseAccountDialog) && mCheatUseAccountDialog.isShowing()) {
            mCheatUseAccountDialog.dismiss();
            mCheatUseAccountDialog = null;
        }

        mCheatUseAccountDialog = new CheatUseAccountDialog(mContext);
        if (mCheatUseAccountDialog.oneDayAppInfo() == true){
            mCheatUseAccountDialog.setCancelable(false);
            mCheatUseAccountDialog.show();
        }

        mCheatUseAccountDialog.setOnCofirmListener(new CheatUseAccountDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress() {
            }
        });
    }
}
