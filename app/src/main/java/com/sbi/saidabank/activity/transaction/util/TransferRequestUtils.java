package com.sbi.saidabank.activity.transaction.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;

import com.google.gson.JsonObject;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.TransferAccountActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.openbank.CellType;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.ahnlab.enginesdk.MetadataManager.getString;

public class TransferRequestUtils {

    /**
     * 이체 세션을 초기화 한다.
     *
     * @param activity
     *
     */
    public static void requestClearTransferSession(final Activity activity) {
        Map param = new HashMap();


        param.put("WTCH_ACNO", "");
        param.put("TRN_TYCD", "1");

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void endHttpRequest(String ret) {

            }
        });
    }

    /**
     * 이체 시작전 세션 동기화 작업,세션초기화도 같이 진행
     *
     * @param activity
     * @param listener
     */
    public static void requestSessionSync(final Activity activity, final HttpSenderTask.HttpRequestListener listener) {
        MLog.d();
        Map param = new HashMap();

        ((BaseActivity)activity).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(((BaseActivity)activity).onCheckHttpError(ret,true)){
                    ((BaseActivity)activity).dismissProgressDialog();
                    return;
                }

                LoginUserInfo.clearInstance();
                LoginUserInfo.getInstance().syncLoginSession(ret);
                LoginUserInfo.getInstance().setFakeFinderAllow(true);

                listener.endHttpRequest(ret);

            }
        });
    }

    /**
     * 즐겨찾기 설정해제
     * @param activity
     * @param recentlyInfo
     * @param listener
     */
    public static void requestChangeFavoriteState(final Activity activity, ITransferRecentlyInfo recentlyInfo, final HttpSenderTask.HttpRequestListener listener){

        int recentlyKind = Const.RECENTLY_KIND_ACCOUNT;

        if(!TextUtils.isEmpty(recentlyInfo.getMNRC_TLNO())){
            //휴대폰 즐겨찾기 변경
            recentlyKind = Const.RECENTLY_KIND_CONTACT;
        }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){
            //그룹 즐겨찾기 변경
            recentlyKind = Const.RECENTLY_KIND_GROUP;
        }


        Map param = new HashMap();
        param.put("TRTM_DVCD","1");//처리구분코드( 1:즐겨찾기, 2:계좌별칭, 3:최근이력삭제 )
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(recentlyInfo.getFAVO_ACCO_YN()))
            param.put("REG_STCD", "1");
        else
            param.put("REG_STCD", "0");

        switch (recentlyKind){
            case Const.RECENTLY_KIND_ACCOUNT:
                param.put("CNTP_FIN_INST_CD", recentlyInfo.getMNRC_BANK_CD());
                param.put("CNTP_BANK_ACNO", recentlyInfo.getMNRC_ACNO());
                param.put("CNTP_ACCO_DEPR_NM", recentlyInfo.getMNRC_ACCO_DEPR_NM());
                if (DataUtil.isNotNull(recentlyInfo.getMNRC_ACCO_ALS())) {
                    param.put("CNTP_ACCO_ALS", recentlyInfo.getMNRC_ACCO_ALS());
                }
                break;
            case Const.RECENTLY_KIND_CONTACT:
                param.put("CNTP_ACCO_DEPR_NM", recentlyInfo.getMNRC_ACCO_DEPR_NM());
                param.put("MNRC_CPNO", recentlyInfo.getMNRC_TLNO());
                break;
            case Const.RECENTLY_KIND_GROUP:
                String flag = "Y";
                if(!TextUtils.isEmpty(recentlyInfo.getFAVO_ACCO_YN()) && recentlyInfo.getFAVO_ACCO_YN().equalsIgnoreCase("Y")){
                    flag = "N";
                }
                param.put("FAVO_ACCO_YN", flag);
                param.put("GRP_SRNO", recentlyInfo.getGRP_SRNO());
                break;
        }

        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    ((BaseActivity)activity).dismissProgressDialog();
                    return;
                }

                listener.endHttpRequest(ret);
            }
        });
    }

    /**
     * 계좌별칭 등록/변경 요청
     *
     * @param activity
     * @param recentlyInfo
     * @param alias
     * @param listener
     */
    public static void requestRegisterAlias(final Activity activity, ITransferRecentlyInfo recentlyInfo, final String alias, final HttpSenderTask.HttpRequestListener listener) {
        MLog.d();

        int recentlyKind = Const.RECENTLY_KIND_ACCOUNT;

        if(!TextUtils.isEmpty(recentlyInfo.getMNRC_TLNO())){
            //휴대폰 즐겨찾기 변경
            recentlyKind = Const.RECENTLY_KIND_CONTACT;
        }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){
            //그룹 즐겨찾기 변경
            recentlyKind = Const.RECENTLY_KIND_GROUP;
        }

        Map param = new HashMap();
        param.put("TRTM_DVCD","2");//처리구분코드( 1:즐겨찾기, 2:계좌별칭, 3:최근이력삭제 )
        switch (recentlyKind){
            case Const.RECENTLY_KIND_ACCOUNT:
                //param.put("CHNG_DVCD", "1");
                param.put("FIN_INST_CD", recentlyInfo.getMNRC_BANK_CD());
                param.put("BANK_ACNO", recentlyInfo.getMNRC_ACNO());

                break;
            case Const.RECENTLY_KIND_CONTACT:
                param.put("MNRC_CPNO", recentlyInfo.getMNRC_TLNO());
                break;
            case Const.RECENTLY_KIND_GROUP:
                param.put("GRP_SRNO", recentlyInfo.getGRP_SRNO());
                break;
        }
        param.put("ACCO_ALS", alias);//계좌,휴대폰,그룹 모두 하나로 퉁!!

        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    return;
                }

                listener.endHttpRequest(ret);

            }
        });
    }

    /**
     * 즐겨찾기 선택된 이체계좌 삭제 처리 요청
     *
     * @param activity
     * @param recentlyInfo
     * @param listener
     */
    public static void requestRemoveRecentlyItem(final Activity activity, ITransferRecentlyInfo recentlyInfo, final HttpSenderTask.HttpRequestListener listener) {
        MLog.d();


        int recentlyKind = Const.RECENTLY_KIND_ACCOUNT;

        if(!TextUtils.isEmpty(recentlyInfo.getMNRC_TLNO())){
            //휴대폰 즐겨찾기 변경
            recentlyKind = Const.RECENTLY_KIND_CONTACT;
        }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){
            //그룹 즐겨찾기 변경
            recentlyKind = Const.RECENTLY_KIND_GROUP;
        }


        Map param = new HashMap();
        param.put("TRTM_DVCD","3");//처리구분코드( 1:즐겨찾기, 2:계좌별칭, 3:최근이력삭제 )
        param.put("REG_STCD", "0");//그냥 0 넣어야 에러 않남. 웹 개발자가 넣으라고 해서리..
        switch (recentlyKind){
            case Const.RECENTLY_KIND_ACCOUNT:
                param.put("CNTP_FIN_INST_CD", recentlyInfo.getMNRC_BANK_CD());
                param.put("CNTP_BANK_ACNO", recentlyInfo.getMNRC_ACNO());
                break;
            case Const.RECENTLY_KIND_CONTACT:
                param.put("MNRC_CPNO", recentlyInfo.getMNRC_TLNO());
                break;
            case Const.RECENTLY_KIND_GROUP:
                param.put("GRP_SRNO", recentlyInfo.getGRP_SRNO());
                break;
        }


        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    return;
                }

                listener.endHttpRequest(ret);

            }
        });
    }

    /**
     * 오픈뱅킹 리스트 조회,잔액은 포함하지 않음.
     * 잔액을 포함하고 있지 않기에 주의할것.
     * 잔액은 리스트 조회후 개별적으로 다시 조회한다.
     */
    public static void requestOpenBankDataNotIncludeAmount(final Activity activity,final HttpSenderTask.HttpRequestListener2 listener){
        Map param = new HashMap();
        param.put("BLNC_INQ_YN", "N");//우선 잔액조회를 제외한 리스트만 조회해온다.


        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    listener.endHttpRequest(false,ret);
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    int accCnt = OpenBankDataMgr.getInstance().parsorAccountList(object);

                    listener.endHttpRequest(true,ret);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }




    /**
     * 한건씩 수취인 조회
     * 그룹은 따로 있음.
     * @param bankCd
     * @param accountNo
     */
    public static void requestRemitteeAccountBySingle(final Activity activity,
                                                      String bankCd,
                                                      String detailBankCd,
                                                      String accountNo,
                                                      final boolean isAddTransfer,
                                                      final HttpSenderTask.HttpRequestListener2 listener){

        requestRemitteeAccountBySingle(activity,bankCd,detailBankCd,accountNo,"",isAddTransfer,listener);

    }
    public static void requestRemitteeAccountBySingle(final Activity activity,
                                                      String bankCd,
                                                      String detailBankCd,
                                                      String accountNo,
                                                      String recvNm,
                                                      final boolean isAddTransfer,
                                                      final HttpSenderTask.HttpRequestListener2 listener){
        requestRemitteeAccountBySingle(activity,bankCd,detailBankCd,accountNo,recvNm,"",isAddTransfer,listener);
    }
    public static void requestRemitteeAccountBySingle(final Activity activity,
                                                      String bankCd,
                                                      String detailBankCd,
                                                      String accountNo,
                                                      String recvNm,
                                                      String transAmount,
                                                      final boolean isAddTransfer,
                                                      final HttpSenderTask.HttpRequestListener2 listener){


        String bankStockCode = bankCd;
        if ("000".equalsIgnoreCase(bankStockCode))
            bankStockCode = "028";

        Logs.e("은행코드 : " + bankStockCode + "\n계좌번호 : " + accountNo);

        Map param = new HashMap();
        param.put("TRTM_DVCD", "1");
        //출금계좌정보
        param.put("WTCH_FNLT_CD", ITransferDataMgr.getInstance().getWTCH_BANK_CD());
        param.put("WTCH_ACNO", ITransferDataMgr.getInstance().getWTCH_ACNO());

        //받는사람 정보
        param.put("CNTP_FIN_INST_CD", bankStockCode);
        param.put("CNTP_BANK_ACNO", accountNo);
        if(!bankCd.equalsIgnoreCase("028")){
            param.put("DTLS_FNLT_CD", detailBankCd);
        }
        param.put("RECV_NM", recvNm);

        if(!TextUtils.isEmpty(transAmount)){
            param.put("TRN_AMT", transAmount);
        }

        ((BaseActivity)activity).showProgressDialog();


        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    String msg = getString(R.string.msg_debug_no_response);
                    ((BaseActivity)activity).showErrorMessage(msg, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listener.endHttpRequest(false,"");
                        }
                    });
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        String msg = getString(R.string.msg_debug_err_response);

                        ((BaseActivity)activity).showErrorMessage(msg, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.endHttpRequest(false,"");
                            }
                        });
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        //이상금융거래 에러 차단.
                        if ("EEFN0306".equalsIgnoreCase(errCode) ||
                                "EEFN0307".equalsIgnoreCase(errCode) ||
                                "EEIF0516".equalsIgnoreCase(errCode)) {
                            Intent intent = new Intent(activity, WebMainActivity.class);
                            String url = WasServiceUrl.ERR0030100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                            String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode +
                                    "&" + Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                            intent.putExtra(Const.INTENT_PARAM, param);

                            activity.startActivity(intent);
                            activity.finish();
                            return;
                        }

                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                listener.endHttpRequest(false,"");
                            }
                        };
                        ((BaseActivity)activity).showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, okClick);
                        return;
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }

                try {
                    final JSONObject object = new JSONObject(ret);

                    JSONArray jsonArray = object.getJSONArray("REC_RECV");

                    if(jsonArray.length() > 0){
                        JSONObject itemObject = jsonArray.getJSONObject(0);

                        //20210504-가상계좌 입금시 금액이 있어야 만 수취인조회가 가능하여 예외처리한다.
                        //가상계좌일때 여기에 에러코드를 넘겨준다. 그래서 그냥 단건이체 화면으로 이동한다.
                        String EROR_MSG_CNTN = itemObject.optString("EROR_MSG_CNTN");
                        if(!TextUtils.isEmpty(EROR_MSG_CNTN) && EROR_MSG_CNTN.startsWith("XEEL")){
                            procVirtualAccountXEEL(isAddTransfer,itemObject,listener);
                        }else{
                            TransferUtils.showDialogRemitteeAccount(activity,isAddTransfer,itemObject,listener);
                        }
                    }else{
                        listener.endHttpRequest(false,"");
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                    listener.endHttpRequest(false,"");
                }
            }
        });
    }

    private static void procVirtualAccountXEEL(boolean isAddTransfer,JSONObject object,HttpSenderTask.HttpRequestListener2 listener){
        ITransferRemitteeInfo remitteeInfo = new ITransferRemitteeInfo();
        remitteeInfo.setCNTP_FIN_INST_CD(object.optString("CNTP_FIN_INST_CD"));
        remitteeInfo.setDTLS_FNLT_CD(object.optString("DTLS_FNLT_CD"));
        remitteeInfo.setCNTP_BANK_ACNO(object.optString("CNTP_BANK_ACNO"));

        remitteeInfo.setRECV_NM(object.optString("RECV_NM"));
        remitteeInfo.setCUST_NM(LoginUserInfo.getInstance().getCUST_NM());

        //받는분통장표기/내통장표기 넣어준다.
        remitteeInfo.setDEPO_BNKB_MRK_NM(LoginUserInfo.getInstance().getCUST_NM());
        if(ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
            remitteeInfo.setTRAN_BNKB_MRK_NM(object.optString("RECV_NM"));
        }else{
            remitteeInfo.setTRAN_BNKB_MRK_NM("사뱅오픈"+object.optString("RECV_NM"));
        }


        remitteeInfo.setMNRC_BANK_NM(object.optString("MNRC_BANK_NM"));
        remitteeInfo.setMNRC_BANK_ALS(object.optString("MNRC_ACCO_ALS")); //은행 단축명이다..오락가락하지 말도록.

        //단건은 금액 입력전이기에 0을 넣어준다.
        //remitteeInfo.setTRN_AMT("0");
        //즉시이체 기본설정.
        remitteeInfo.setTRNF_DVCD("1");

        //가상계좌여부
        remitteeInfo.setVERTUAL_ACCOUNT_YN("Y");

        //가져오기, 내보내기시 물고 들어온 금액이 있으면 이체 금액에 넣어둔다.
        if(ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() != ITransferDataMgr.ACCESS_TYPE_NORMAL){
            remitteeInfo.setTRN_AMT(ITransferDataMgr.getInstance().getMNRC_AMT());
            //내보내기로 들어왔다가 수취조회를 한 경우엔 기본값으로 변경해준다.
            if(ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() == ITransferDataMgr.ACCESS_TYPE_EXPORT){
                ITransferDataMgr.getInstance().setTRANSFER_ACCESS_TYPE(ITransferDataMgr.ACCESS_TYPE_NORMAL);
            }
        }

        //수취인조회가 끝난것은 기존 데이타 지우고 리스트에 담아둔다.
        if(!isAddTransfer)
            ITransferDataMgr.getInstance().getRemitteInfoArrayList().clear();

        ITransferDataMgr.getInstance().addRemitteInfoIntoArrayList(remitteeInfo);

        listener.endHttpRequest(true,object.toString());
    }

    /**
     * 다건/그룹 수취인 조회
     *
     * @param groupIdx
     *
     */
    public static void requestRemitteeAccountByMulti(final Activity activity,
                                                      String groupIdx,
                                                      String favorite,
                                                      final boolean isRemoveSaveRemitte,//혹시 단건 수취조회한 데이타가 있으면 유지 할지?지울지?
                                                      final HttpSenderTask.HttpRequestListener2 listener){

        Map param = new HashMap();
        param.put("GRP_SRNO", groupIdx);
        param.put("FAVO_ACCO_YN", favorite);
        //출금계좌정보
        param.put("WTCH_FNLT_CD", ITransferDataMgr.getInstance().getWTCH_BANK_CD());
        param.put("WTCH_ACNO", ITransferDataMgr.getInstance().getWTCH_ACNO());

        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                String errorMsg = String.format(activity.getString(R.string.msg_fail_timeout_request), activity.getString(R.string.inquire_deposit_account));
                if(((BaseActivity)activity).onCheckHttpError(ret,errorMsg,false)){
                    listener.endHttpRequest(false,"");
                    return;
                }

                if(isRemoveSaveRemitte)
                    ITransferDataMgr.getInstance().getRemitteInfoArrayList().clear();

                try {
                    final JSONObject object = new JSONObject(ret);

                    JSONArray jsonArray = object.getJSONArray("REC_RECV");

                    boolean isSetNeedFocus=false;
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject itemObject = jsonArray.getJSONObject(i);

                        ITransferRemitteeInfo remitteeInfo = new ITransferRemitteeInfo();


                        remitteeInfo.setCNTP_FIN_INST_CD(itemObject.optString("CNTP_FIN_INST_CD"));
                        remitteeInfo.setDTLS_FNLT_CD(itemObject.optString("DTLS_FNLT_CD"));
                        remitteeInfo.setCNTP_BANK_ACNO(itemObject.optString("CNTP_BANK_ACNO"));

                        remitteeInfo.setTRN_AMT(itemObject.optString("MNRC_AMT"));
                        remitteeInfo.setDEPO_BNKB_MRK_NM(itemObject.optString("MNRC_ACCO_MRK_CNTN"));
                        remitteeInfo.setTRAN_BNKB_MRK_NM(itemObject.optString("WTCH_ACCO_MRK_CNTN"));
                        remitteeInfo.setMNRC_ACCO_DEPR_NM(itemObject.optString("MNRC_ACCO_DEPR_NM"));

                        remitteeInfo.setRECV_NM(itemObject.optString("RECV_NM"));
                        remitteeInfo.setCUST_NM(LoginUserInfo.getInstance().getCUST_NM());
                        //remitteeInfo.setSVC_DVCD(itemObject.optString("SVC_DVCD")); //통합이체에서는 필요없다.그룹은 기본설정 1로 한다.
                        remitteeInfo.setMNRC_BANK_NM(itemObject.optString("MNRC_BANK_NM"));
                        remitteeInfo.setMNRC_BANK_ALS(itemObject.optString("MNRC_ACCO_ALS")); //은행 단축명이다..오락가락하지 말도록.


                        remitteeInfo.setEROR_MSG_CNTN(itemObject.optString("EROR_MSG_CNTN"));

                        remitteeInfo.setGRP_SRNO(itemObject.optString("GRP_SRNO"));

                        //그룹은 기본설정1
                        remitteeInfo.setTRNF_DVCD("1");

                        if(TextUtils.isEmpty(remitteeInfo.getTRN_AMT())&& !isSetNeedFocus){
                            isSetNeedFocus = true;
                            remitteeInfo.setIS_NEED_FOCUS(true);
//                            if(i == jsonArray.length()-1){
//                                remitteeInfo.setIS_NEED_FOCUS(true);
//                            }
                        }

                        //리스트에 수취정보 추가.
                        ITransferDataMgr.getInstance().addRemitteInfoIntoArrayList(remitteeInfo);

                    }

                    Logs.e("수취인 이체항목 추가건수 : " +  ITransferDataMgr.getInstance().getRemitteInfoArraySize());
                    listener.endHttpRequest(true,object.toString());
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 단건 금액입력후 지연이체, 입금지정서비스 체크하도록 한다.
     *
     * @param activity
     * @param listener
     */
    public static void requestDlyDsgtServiceCheck(final Activity activity,String amount, final HttpSenderTask.HttpRequestListener2 listener) {
        MLog.d();
        Map param = new HashMap();
        param.put("WTCH_ACNO", ITransferDataMgr.getInstance().getWTCH_ACNO());

        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);

        param.put("CNTP_FIN_INST_CD", remitteeInfo.getCNTP_FIN_INST_CD());
        param.put("CNTP_BANK_ACNO", remitteeInfo.getCNTP_BANK_ACNO());
        param.put("TRN_AMT", amount);

        ((BaseActivity)activity).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190200A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)activity).dismissProgressDialog();
                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    listener.endHttpRequest(false,ret);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    //지연이체 서비스 가입상태면
                    ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                    if( LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                        remitteeInfo.setTRNF_DVCD(object.optString("SVC_DVCD"));
                    }else if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                        remitteeInfo.setTRNF_DVCD("5");
                    }

                    listener.endHttpRequest(true,ret);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }


    /**
     * 이체전 금액확인 조회
     * 단건,다건 모두 이체버튼 클릭시 호출해야 한다.
     */
    public static void requestCheckAmountToday(final Activity activity,final HttpSenderTask.HttpRequestListener2 listener){

        Map param = new HashMap();
        param.put("TRTM_DVCD", "1");

        try{
            JSONArray jsonArray = new JSONArray();
            ArrayList<ITransferRemitteeInfo> remitteeInfoArrayList =  ITransferDataMgr.getInstance().getRemitteInfoArrayList();
            for(int i=0;i<remitteeInfoArrayList.size();i++){
                ITransferRemitteeInfo remitteeInfo = remitteeInfoArrayList.get(i);

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("GRP_SRNO",remitteeInfo.getGRP_SRNO()); //출금금융기관코드
                jsonObject.put("DMND_SQNO",String.valueOf(i)); //레코드번호
                jsonObject.put("WTCH_FNLT_CD",ITransferDataMgr.getInstance().getWTCH_BANK_CD()); //출금금융기관코드
                jsonObject.put("WTCH_ACNO",ITransferDataMgr.getInstance().getWTCH_ACNO());//출금계좌번호
                jsonObject.put("CNTP_FIN_INST_CD",remitteeInfo.getCNTP_FIN_INST_CD());    //상대금융기관코드
                jsonObject.put("CNTP_BANK_ACNO",remitteeInfo.getCNTP_BANK_ACNO());        //상대은행계좌번호
                jsonObject.put("CNTP_ACCO_DEPR_NM",remitteeInfo.getRECV_NM());            //상대계좌예금주명

                if(TextUtils.isEmpty(remitteeInfo.getDEPO_BNKB_MRK_NM())){
                    jsonObject.put("MNRC_ACCO_MRK_CNTN",LoginUserInfo.getInstance().getCUST_NM());  //입금계좌표시내용
                }else{
                    jsonObject.put("MNRC_ACCO_MRK_CNTN",remitteeInfo.getDEPO_BNKB_MRK_NM());  //입금계좌표시내용
                }

                if(TextUtils.isEmpty(remitteeInfo.getTRAN_BNKB_MRK_NM())){
                    jsonObject.put("WTCH_ACCO_MRK_CNTN",remitteeInfo.getRECV_NM());  //출금계좌표시내용
                }else{
                    jsonObject.put("WTCH_ACCO_MRK_CNTN",remitteeInfo.getTRAN_BNKB_MRK_NM());  //출금계좌표시내용
                }

                jsonObject.put("TRN_AMT",remitteeInfo.getTRN_AMT());                      //거래금액
                jsonObject.put("TRNF_DMND_DT",remitteeInfo.getTRNF_DMND_DT());            //이체요청일자
                jsonObject.put("TRNF_DMND_TM",remitteeInfo.getTRNF_DMND_TM());            //이체요청시각

                //이체 중간에 입금지정서비스나 지연이체 서비스를 해지할수 있다.
                //이때 이체정보를 변경시켜주어야 한다.
                if(remitteeInfo.getTRNF_DVCD().equalsIgnoreCase("2")){
                    //입금지정일때 진짜 현재 세션 상태가 입금 지정인지 체크
                    if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("N")){
                        remitteeInfo.setTRNF_DVCD("1");
                    }
                }else if(remitteeInfo.getTRNF_DVCD().equalsIgnoreCase("5")){
                    //입금지정일때 진짜 현재 세션 상태가 입금 지정인지 체크
                    if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("N")){
                        remitteeInfo.setTRNF_DVCD("1");
                    }
                }
                jsonObject.put("TRNF_KNCD",remitteeInfo.getTRNF_DVCD());                  //이체종류코드
                jsonObject.put("TRN_MEMO_CNTN",remitteeInfo.getTRN_MEMO_CNTN());          //이체종류코드

                //20210423추가
                jsonObject.put("MNRC_DTLS_FNLT_CD",remitteeInfo.getDTLS_FNLT_CD());       //입금세부금융기관코드

                jsonArray.put(jsonObject);
            }
            param.put("REC_IN_TRAN", jsonArray);
        }catch (JSONException e){
            Logs.printException(e);
            return;
        }


        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190200A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    listener.endHttpRequest(false,"");
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);

                    JSONArray jsonArray = object.getJSONArray("REC_OUT_TRAN");
                    if(jsonArray.length() == 0){
                        ((BaseActivity)activity).showErrorMessage(getString(R.string.msg_debug_err_response), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.endHttpRequest(false,"");
                            }
                        });
                        return;
                    }

                    boolean isIncludeError = false;
                    for(int i=0;i<jsonArray.length();i++){
                        JSONObject jsonItemObj =  jsonArray.getJSONObject(i);

                        String DMND_SQNO = jsonItemObj.optString("DMND_SQNO");
                        if(!TextUtils.isEmpty(DMND_SQNO)){
                            int rowIndex = Integer.parseInt(DMND_SQNO);
                            ITransferRemitteeInfo remitteeInfo =  ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(rowIndex);

                            remitteeInfo.setFEE(jsonItemObj.optString("FEE"));
                            remitteeInfo.setTRN_UNQ_NO(jsonItemObj.optString("TRN_UNQ_NO"));


                            String EROR_MSG_CNTN = jsonItemObj.optString("EROR_MSG_CNTN");
                            remitteeInfo.setEROR_MSG_CNTN(EROR_MSG_CNTN);
                            if(!TextUtils.isEmpty(EROR_MSG_CNTN)){
                                isIncludeError = true;
                            }

                            remitteeInfo.setREQ_OBJ_DATA(jsonItemObj.toString());
                        }
                    }
                    if(isIncludeError){
                        listener.endHttpRequest(false,ret);
                        return;
                    }

                    //단건에서 에러메세지가 있으면 바로 출력해준다.
                    //리턴함수에서 처리하도록변경.
//                    if(ITransferDataMgr.getInstance().getRemitteInfoArrayList().size() == 1){
//                        String EROR_MSG_CNTN = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getEROR_MSG_CNTN();
//                        if(!TextUtils.isEmpty(EROR_MSG_CNTN)){
//                            DialogUtil.alert(activity, EROR_MSG_CNTN, new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    listener.endHttpRequest(false,"");
//                                }
//                            });
//                            return;
//                        }
//                    }

                    // 동일이체건이 존재할 경우
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(object.optString("SAME_AMT_TRNF_YN"))) {
                        AlertDialog alertDialog = new AlertDialog(activity);
                        alertDialog.mPListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.endHttpRequest(true,"");
                            }
                        };
                        alertDialog.mNListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                listener.endHttpRequest(false,"");
                            }
                        };
                        alertDialog.msg = "오늘 같은 분에게 동일한 금액을\n이체하셨습니다.\n정말 이체하시겠습니까?";
                        alertDialog.show();
                        return;
                    }
                    listener.endHttpRequest(true,ret);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }


    /**
     * 통장 한도 조회
     *
     */
    public static void requestAccountLimit(final Activity activity,final HttpSenderTask.HttpRequestListener listener) {
        String bankCd = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String kind="";

        //출금계좌 정보가 없으면 한도조회 진행하지 않는다.
        if(TextUtils.isEmpty(bankCd) || TextUtils.isEmpty(accountNo) ) return;


        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_ACCOUNT)
            kind = "1";
        else if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE)
            kind = "2";
        else{
            Logs.e(activity,"받는분을 선택하지 않았습니다.");
            return;
        }

        requestAccountLimit(activity,bankCd,accountNo,kind,listener);

    }

    /**
     * 계좌에 대한 한도를 조회한다.
     *
     * @param activity
     * @param bankCd
     * @param accountNo
     * @param kind       1:계좌이체, 2:휴대폰이체
     * @param listener
     */

    public static void requestAccountLimit(final Activity activity,final String bankCd,final String accountNo,String kind,final HttpSenderTask.HttpRequestListener listener) {
        Map param = new HashMap();
        param.put("TRN_DVCD", "0");//거래구분코드 조회
        param.put("WTCH_FNLT_CD", bankCd);
        param.put("ACNO", accountNo);
        param.put("TRN_TYCD", kind);

        if(activity instanceof ITransferSendSingleActivity
                && ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_ACCOUNT){
            param.put("CNTP_FIN_INST_CD", ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getCNTP_FIN_INST_CD());
            param.put("CNTP_BANK_ACNO", ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getCNTP_BANK_ACNO());
        }

        ((BaseActivity)activity).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)activity).dismissProgressDialog();
                Logs.i("TRA0190200A01 : " + ret);
                if(((BaseActivity)activity).onCheckHttpError(ret,true)){
                    ITransferDataMgr.getInstance().setTransferOneTime(0d);
                    ITransferDataMgr.getInstance().setTransferOneDay(0d);
                    ITransferDataMgr.getInstance().setBalanceAmount(0d);
                    listener.endHttpRequest("FALSE");
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    if(TransferUtils.isSaidaAccount(bankCd)){
                        //1회이체한도금액
                        String TI1_TRNF_LMIT_AMT = object.optString("TI1_TRNF_LMIT_AMT");
                        if (!TextUtils.isEmpty(TI1_TRNF_LMIT_AMT))
                            ITransferDataMgr.getInstance().setTransferOneTime(Double.valueOf(TI1_TRNF_LMIT_AMT));


                        //1일이체한도금액
                        String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                        if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                            ITransferDataMgr.getInstance().setTransferOneDay(Double.valueOf(D1_UZ_LMIT_AMT));
                        } else {
                            String D1_TRNF_LMIT_AMT = object.optString("D1_TRNF_LMIT_AMT");
                            if (!TextUtils.isEmpty(D1_TRNF_LMIT_AMT))
                                ITransferDataMgr.getInstance().setTransferOneDay(Double.valueOf(D1_TRNF_LMIT_AMT));
                        }

                        //출금가능금액
                        String WTCH_TRN_POSB_AMT = object.optString("WTCH_TRN_POSB_AMT");
                        if (!TextUtils.isEmpty(WTCH_TRN_POSB_AMT))
                            ITransferDataMgr.getInstance().setBalanceAmount(Double.valueOf(WTCH_TRN_POSB_AMT));

                        //지연이체에 가입된 사람이면 즉시입금 가능 금액을 조회해 온다.
                        if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                            ITransferDataMgr.getInstance().setDLY_POSB_LMIT_AMT(object.optString("POSB_LMIT_AMT"));
                        }


                    }else{
                        //1회이체한도금액
                        String TI1_TRNF_LMIT_AMT = object.optString("D1_WTCH_LMIT_AMT");
                        ITransferDataMgr.getInstance().setTransferOneTime(Double.valueOf(TI1_TRNF_LMIT_AMT));
                        //1일이체한도금액
                        String D1_UZ_LMIT_AMT = object.optString("D1_WTCH_LMIT_AMT");
                        ITransferDataMgr.getInstance().setTransferOneDay(Double.valueOf(D1_UZ_LMIT_AMT));

                        //출금가능금액
                        String  WTCH_POSB_AMT = OpenBankDataMgr.getInstance().getWTCH_POSB_AMT(bankCd,accountNo);
                        ITransferDataMgr.getInstance().setBalanceAmount(Double.valueOf(WTCH_POSB_AMT));
                    }

                    if(BuildConfig.DEBUG){
                        String msg = "1회이체한도금액 : " + Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getTransferOneTime()) + "\n" +
                                "1일이체한도금액 : " + Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getTransferOneDay()) + "\n" +
                                "출금가능금액 : " + Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getBalanceAmount()) + "\n";

                        Logs.e(activity,msg);
                    }

                    listener.endHttpRequest("TRUE");
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * STEP_03. 업무서버에 이체목록 등록
     *
     */
    public static void requestUploadTransferInfo(final Activity activity,final HttpSenderTask.HttpRequestListener2 listener) {
        MLog.d();

        Map param = new HashMap();

        JSONArray jsonArray = new JSONArray();
        for(int i=0;i<ITransferDataMgr.getInstance().getRemitteInfoArrayList().size();i++){
            String reqObjStr = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(i).getREQ_OBJ_DATA();
            if(!TextUtils.isEmpty(reqObjStr)){
                try {
                    JSONObject jsonObject = new JSONObject(reqObjStr);
                    jsonArray.put(jsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        param.put("REC_IN_TRAN", jsonArray);

        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190200A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    listener.endHttpRequest(false,ret);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray jsonArray = object.optJSONArray("REC");
                    if (DataUtil.isNull(jsonArray) || jsonArray.length() < 1) {
                        listener.endHttpRequest(false,ret);
                        return;
                    }

                    for (int index = 0; index < jsonArray.length(); index++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(index);

                        if (DataUtil.isNull(jsonObject))
                            continue;

                        String DMND_SQNO = jsonObject.optString("DMND_SQNO");
                        if(!TextUtils.isEmpty(DMND_SQNO)){
                            int position = Integer.parseInt(DMND_SQNO);

                            ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(position);
                            remitteeInfo.setUploadRemitteeInfo(jsonObject);
                        }
                    }

                    listener.endHttpRequest(true,ret);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }


    /**
     * 보이스피싱 문진 요청
     * */
    @SuppressLint("SimpleDateFormat")
    public static void requestVoicePhising(final Activity activity,final HttpSenderTask.HttpRequestListener2 listener) {
        MLog.d();

        Map param = new HashMap();
        param.put("TRNF_DVCD", 1);  // 1 = 일반이체, 2 = 안심이체
        param.put("DMND_CCNT", ITransferDataMgr.getInstance().getRemitteInfoArraySize());

        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    listener.endHttpRequest(false,ret);
                    return;
                }

                listener.endHttpRequest(true,ret);

            }
        });
    }



    /**
     * 계좌이체 전자 서명값 요청
     */
    public static void transferSignData(final Activity activity,final HttpSenderTask.HttpRequestListener2 listener) {
        MLog.d();

        Map param = new HashMap();
        param.put("TRN_TYCD", "1");



        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    listener.endHttpRequest(false,"");
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    String ELEC_SGNR_VAL_CNTN = Utils.getJsonString(object, "ELEC_SGNR_VAL_CNTN");

                    if (DataUtil.isNotNull(ELEC_SGNR_VAL_CNTN)) {
                        listener.endHttpRequest(true,ELEC_SGNR_VAL_CNTN);
                        return;
                    }
                } catch (Exception e) {
                    MLog.e(e);
                }
                listener.endHttpRequest(false,"");
            }
        });
    }

}
