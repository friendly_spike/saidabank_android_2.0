package com.sbi.saidabank.activity.transaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.Holder;
import com.orhanobut.dialogplus.ListHolder;
import com.orhanobut.dialogplus.OnDismissListener;
import com.orhanobut.dialogplus.OnItemClickListener;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.login.ReregLostPincodeActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.adapter.BankStockAdapter;
import com.sbi.saidabank.common.adapter.MyAccountAdapter;
import com.sbi.saidabank.activity.transaction.adater.ITransferRecentlyAdapter;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.AlertRemitteeDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmMultiTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmSingleTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.dialog.SlidingInputTextDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingPreventAskDialog;
import com.sbi.saidabank.common.dialog.TransferEditAliasDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;

import com.sbi.saidabank.define.datatype.common.CheatUseAccountInfo;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferVerifyInfo;

import com.sbi.saidabank.solution.motp.MOTPManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * TransferAccountActivity : 계좌번호 이체 화면
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TransferAccountActivity extends BaseActivity implements DatePickerCallback, KeyboardDetectorRelativeLayout.IKeyboardChanged {

    private static final float ANIMATION_SHAKE_OFFSET = 20f;
    private static final int REQUEST_SSENSTONE_AUTH = 20000;
    private static final int REQUEST_OTP_AUTH = 20001;

    private static final int SCROLL_EDITTEXT = 0;
    private static final int SCROLL_CHECK_KEYBOARD = 1;

    private enum TRANSFER_ACCOUNT_STEP {
        STEP_01,
        STEP_02,
        STEP_03
    }

    private WeakHandler mScrollHandler;
    private ScrollView mSvScview;
    private KeyboardDetectorRelativeLayout mRelativeLayout;
    private LinearLayout mLayoutTotalTransfer;          // 다건 바로이체
    private TextView mTextSendNum;                      // 다건 바로이체 건수
    private TextView mTextSendAmount;                   // 다건 바로이체 총금액
    private RelativeLayout mLayoutMyAccountList;
    private ListView mListViewMyAccount;                // 상단 계좌 리스트
    private RelativeLayout mBtnCloseMyAccountList;      // 상단 계좌 리스트 닫힘
    private RelativeLayout mBtnWithdrawAccount;         // 상단 출금계좌 표시 (출금 계좌 변경 기능)
    private TextView mTextAccountName;                  // 출금 계좌명 (별칭->상품명순)
    private TextView mTextAccount;                      // 계좌번호 (['계좌번호 뒤4자리'])
    private ImageView mImageShare;                      // 커플통장 가입여부
    private ImageView mImageClock;                      // 지연이체 서비스 가입여부 표시
    private TextView mTextAccountBalance;               // 계좌 출금 가능 금액
    private ImageView mBtnBalanceDropdown;              // 출금 계좌 변경 버튼
    private ImageView mBtnLockDropdown;                 // 계좌 리스트 사용불가 버튼 (다건)
    private LinearLayout mLayoutStep01;                 // 이체 금액 입력 단계
    private TextView mTextHangulAmount;                 // 이체 금액 한글 표시
    private LinearLayout mLayoutStep01Body;             // 실제 금액 입력란 표시 (interaction 위해 구분)
    private View mViewEditAmountLine;                   // 금액 입력란 하단 바
    private ProgressBar mProgressLine;                  // 금액 입력란 하단 바 (interaction 위해 추가)
    private RelativeLayout mLayoutStep01Bottom;         // 금액 입력란 하단 (interaction 위해 구분)
    private EditText mEditAmount;                       // 금액 입력란
    private RelativeLayout mBtnEraseAmount;             // 금액 입력 삭제 버튼
    private TextView mTextErrMsg;                       // 금액 입력 오류 메세지
    private LinearLayout mLayoutLimitTransfer;          // 한도
    private TextView mTextLimitOneTime;                 // 1회 이체한도
    private TextView mTextLimitOneDay;                  // 1일 이체한도
    private LinearLayout mLayoutStep02;                 // 이체 계좌 입력 단계
    private TextView mTextDisplayTransferAmount;        // 이체 금액 표시 (선택시, 1단계로 이동)
    private TextView mTextBankName;                     // 은행명 표시
    private LinearLayout mBtnSelectBank;                // 은행 선택창 표시 버튼
    private EditText mEditAccountNum;                   // 이체할 계좌번호
    private RelativeLayout mBtnEraseAccount;            // 이체할 계좌번호 삭제 버튼
    private View mViewStep02Bar;
    private LinearLayout mLayoutStep03;                 // 수취 조회 후 단계
    private ScrollView mScrollviewStep03;
    private TextView mTextDisplayRecipient;             // 보낸 계좌 표시 문장
    private TextView mTextDisplaySending;               // 받을 계좌 표시 문장
    private RelativeLayout mLayoutOpenCloseInput;
    private ToggleButton mBtnOpenCloseInput;            // 시간 및 메모 입력창 표시 버튼
    private RelativeLayout mLayoutAddInpuntItem;        // 시간 및 메모 입력창
    private TextView mTextInputDate;                    // 시간 표시
    private TextView mTextInputMemo;                    // 메모 표시
    private LinearLayout mLayoutSendDate;               // 시간 설정
    private RelativeLayout mBtnDropdownSendDate;        // 시간 설정창 표시 버튼
    private RelativeLayout mBtnEraseSendDate;           // 시간 삭제 버튼
    private LinearLayout mLayoutHelp;                   // 시간 설정 도움 버튼
    private LinearLayout mLayoutDelayDesc;              // 지연이체 설명창
    private Button mBtnConfirm;                         // 하단 확인 버튼
    private Button mBtnSendTransfer;                    // 하단 이체하기 버튼
    private LinearLayout mLayoutTransferRecentlyList;   // 최근 이체 내역 표시
    private LinearLayout mLayoutNoTransferRecently;     // 최근 이체 내역 없을 시 표시
    private SwipeMenuListView mListViewTransferRecently;  // 최근 이체 내역 리스트
    private LinearLayout mLayoutHelpDelayAccount;       // 지연이체 도움 설명창

    private String mIntentMyAccount = "";               // 타메뉴에서 전달된 계좌 값
    private String mTagBankCode = "";                   // 메인 최근 이체 태그 은행코드
    private String mTagAccount = "";                    // 메인 최근 이체 태그 계좌
    private String mInputAmount = "";                   // 이체 금액
    private int mInputYear = 0;                         // 날짜선택값
    private int mInputMonth = 0;                        // 날짜선택값
    private int mInputDay = 0;                          // 날짜선택값
    private int mInputHour = 0;                         // 날짜선택값
    private Double mBalanceAmount = 0.0;                // 계좌 잔액
    private Double mTransferOneTime = 0.0;              // 1회 이체한도
    private Double mTransferOneDay = 0.0;               // 일일 이체한도
    private int mCount = 0;                             // 계좌입력 라인 인터액션을 위한 카운트
    private int mSendAccountIndex = -1;                 // 나의 계좌 리스트에서 선택된 계좌의 인덱스
    private String mSendBankStockCode;                  // 은행선택창에서 선택된 은행 코드값
    private boolean mDlyTrnfSvcEntrYn;                  // 지연이체 서비스 가입 여부
    private boolean mDsgtMnrcAccoSvcEntrYn;             // 입금계좌지정 서비스 가입 여부
    private String mMOTPSerialNumber;
    private String mMOTPTAVersion;

    //++ DatePicker values
    private boolean mIstoday;
    private boolean mIsDelayService;
    private int mCurYear;
    private int mCurMon;
    private int mCurDay;

    private ArrayList<MyAccountInfo> mListMyAccount = new ArrayList<>();                    // 나의 계좌 리스트값
    private ArrayList<ITransferRecentlyInfo> mListTransferRecently = new ArrayList<>();       // 나의 최근 이체계좌 리스트값
    private ArrayList<RequestCodeInfo> mListBank = new ArrayList<>();                       // 은행 리스트값
    private ArrayList<RequestCodeInfo> mListStock = new ArrayList<>();                      // 증권사 리스트값
    private ArrayList<TransferReceiptInfo> mListTransferReceiptInfo = new ArrayList<>();    // 이체완료 후 결과 리스트값

    private TRANSFER_ACCOUNT_STEP mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_01;            // 이체 수행 단계 (1단계 - 금액입력, 2단계 - 이체계좌 입력, 3단계 - 수취조회후)

    private MyAccountAdapter mMyAccountAdapter;                         // 나의 계좌 리스트 어댑터
    private ITransferRecentlyAdapter mITransferRecentlyAdapter;           // 최근 이체 리스트 어댑터
    private TransferVerifyInfo mLastTransferInfo;                       // 수취조회 후 '아니요' 선택 처리를 위해 추가

    private DialogPlus dialogBankPlus;                                  // 은행 or 증권사 선택 다이얼로그
    private SlidingConfirmSingleTransferDialog singleTransferDialog;    // 단건 확인 다이얼로그
    private SlidingConfirmMultiTransferDialog multiTransferDialog;      // 다건 확인 다이얼로그

    private int mTransferCnt = 0;
    private boolean mIsShownKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_account);
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        //메인에서 내계좌 물고 들어올때
        mIntentMyAccount = getIntent().getStringExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT);
        //보낼계좌 물고 들어올때
        mTagAccount = getIntent().getStringExtra(Const.INTENT_RCV_ACCOUNT);
        mTagBankCode = getIntent().getStringExtra(Const.INTENT_RCV_BANK_CODE);
        if ("000".equalsIgnoreCase(mTagBankCode))
            mTagBankCode = "028";

        if (DataUtil.isNotNull(LoginUserInfo.getInstance())) {
            mDlyTrnfSvcEntrYn = Const.REQUEST_WAS_YES.equalsIgnoreCase(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN());
            mDsgtMnrcAccoSvcEntrYn = Const.REQUEST_WAS_YES.equalsIgnoreCase(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN());
        }

        initUX();
        initMyAccount();
        requestSessionSync();
        requestBankList();
        requestStockList();
    }

    @Override
    protected void onDestroy() {
        MLog.d();
        TransferManager.getInstance().clearAll();
        hideBankStockList();
        hideAllConfirmTransferDialog();
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MLog.d();
        if (resultCode == RESULT_OK && DataUtil.isNotNull(data)) {
            if (requestCode == REQUEST_SSENSTONE_AUTH) {
                CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                if (DataUtil.isNotNull(commonUserInfo)) {
                    if ("P000".equalsIgnoreCase(commonUserInfo.getResultMsg()) && DataUtil.isNotNull(commonUserInfo.getSignData()) && Utils.isEnableClickEvent()) {
                        // 핀코드 인증완료 후 이체요청
                        if (DataUtil.isNotNull(mListTransferReceiptInfo) && !mListTransferReceiptInfo.isEmpty()) {
                            mListTransferReceiptInfo.clear();
                        }

                        if (mTransferCnt > 0) {
                            MLog.i("mTransferCnt >> " + mTransferCnt);
                        }
                        String elecSrno = commonUserInfo.getSignData();

                        Map param = new HashMap();
                        param.put("ELEC_SGNR_SRNO", elecSrno);
                        param.put("DMND_CCNT", mTransferCnt);

                        showProgressDialog();

                        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
                            @Override
                            public void endHttpRequest(String ret) {

                                dismissProgressDialog();

                                if (DataUtil.isNull(ret)) {
                                    showFailTransfer(0, Const.EMPTY);
                                    return;
                                }

                                try {
                                    JSONObject object = new JSONObject(ret);
                                    if (DataUtil.isNull(object)) {
                                        showFailTransfer(0, Const.EMPTY);
                                        return;
                                    }

                                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                                        if (TextUtils.isEmpty(msg))
                                            msg = getString(R.string.common_msg_no_reponse_value_was);
                                        if (TransferManager.getInstance().size() > 1) {
                                            mLastTransferInfo = null;
                                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                                            showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                                        } else {
                                            showFailTransfer(0, ret);
                                        }
                                        return;
                                    }

                                    JSONArray array = object.optJSONArray("REC_OUT");

                                    if (DataUtil.isNull(array))
                                        return;

                                    for (int index = 0; index < array.length(); index++) {
                                        JSONObject jsonObject = array.getJSONObject(index);
                                        if (jsonObject == null)
                                            continue;
                                        mListTransferReceiptInfo.add(new TransferReceiptInfo(jsonObject));
                                    }

                                    String ACCO_ALS = object.optString("ACCO_ALS");
                                    String mWithdrawName = Const.EMPTY;
                                    if (DataUtil.isNotNull(mListMyAccount) && mListMyAccount.size() > mSendAccountIndex) {
                                        if (DataUtil.isNull(ACCO_ALS)) {
                                            ACCO_ALS = object.optString("PROD_NM");
                                        }
                                        String ACNO = mListMyAccount.get(mSendAccountIndex).getACNO();
                                        if (DataUtil.isNotNull(ACNO)) {
                                            int lenACNO = ACNO.length();
                                            if (lenACNO > 4) {
                                                ACNO = ACNO.substring(lenACNO - 4, lenACNO);
                                            }
                                            ACNO = " [" + ACNO + "]";
                                        }
                                        mWithdrawName = ACCO_ALS + ACNO;
                                    }

                                    if (DataUtil.isNotNull(mListTransferReceiptInfo) && mListTransferReceiptInfo.size() == 1) {
                                        String RESP_CD = mListTransferReceiptInfo.get(0).getRESP_CD();
                                        if ("XEEL0140".equalsIgnoreCase(RESP_CD) || "XEEL0141".equalsIgnoreCase(RESP_CD) ||
                                                "XEEL0142".equalsIgnoreCase(RESP_CD) || "XEEL0143".equalsIgnoreCase(RESP_CD) ||
                                                "XEKM0089".equalsIgnoreCase(RESP_CD) || "XEKM0091".equalsIgnoreCase(RESP_CD) ||
                                                "XEKM0096".equalsIgnoreCase(RESP_CD)) {
                                            showFailTransfer(0, Const.EMPTY);
                                            return;
                                        }
                                    }

                                    // 이체완료 페이지로 이동
                                    Intent intent = new Intent(TransferAccountActivity.this, TransferCompleteActivity.class);
                                    intent.putExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO, mListTransferReceiptInfo);
                                    intent.putExtra(Const.INTENT_LIST_TRANSFER_NAME, LoginUserInfo.getInstance().getCUST_NM());
                                    intent.putExtra(Const.INTENT_TRANSFER_BALANCE, object.optString("AFTR_BLNC"));
                                    intent.putExtra(Const.INTENT_PROD_NAME, object.optString("PROD_NM"));
                                    intent.putExtra(Const.INTENT_BANK_NM, DataUtil.isNotNull(object.optString("BANK_NM")) ? object.optString("BANK_NM") : "SBI저축(사이다뱅크)");
                                    intent.putExtra(Const.INTENT_WITHDRAWABLE_AMOUNT, object.optString("WTCH_TRN_POSB_AMT"));
                                    intent.putExtra(Const.INTENT_WITHDRAW_NAME, mWithdrawName);
                                    intent.putExtra(Const.INTENT_ACCO_ALS, ACCO_ALS);
                                    startActivity(intent);
                                    finish();

                                } catch (JSONException e) {
                                    MLog.e(e);
                                }
                            }
                        });
                    } else {
                        Utils.showToast(TransferAccountActivity.this, "인증에 실패했습니다.");
                        if (!TransferManager.getInstance().getIsNotRemove()) {
                            requestRemoveTransfer(true);
                        }
                    }
                }
            } else if (requestCode == REQUEST_OTP_AUTH) {
                CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                String otpAuthTools = data.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
                if (DataUtil.isNotNull(commonUserInfo)
                        && DataUtil.isNotNull(otpAuthTools)
                        && ("3".equalsIgnoreCase(otpAuthTools) || "2".equalsIgnoreCase(otpAuthTools))
                        && data.hasExtra(Const.INTENT_OTP_AUTH_SIGN)) {
                    commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                    commonUserInfo.setSignData(data.getStringExtra(Const.INTENT_OTP_AUTH_SIGN));
                    commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                    Intent intent = new Intent(TransferAccountActivity.this, PincodeAuthActivity.class);
                    intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                    intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                    intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
                    overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                }
            }
        } else {
            if (!TransferManager.getInstance().getIsNotRemove()) {
                requestRemoveTransfer(true);
            }
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                updateSendMultiTransfer(true);
                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mEditAmount.requestFocus();
                        Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                    }
                }, 50);
            }
            TransferManager.getInstance().setIsNotRemove(false);
        }
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        hideBankStockList();
        hideAllConfirmTransferDialog();
        if (mLayoutHelpDelayAccount.isShown()) {
            hideHelpDelay();
            return;
        }
        if (mLayoutHelp.isShown()) {
            mLayoutHelp.setVisibility(View.GONE);
            mLayoutSendDate.setVisibility(View.VISIBLE);
            return;
        }
        if (mLayoutMyAccountList.isShown()) {
            hideMyAccountList();
            return;
        }
        showCancelMessage();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        Rect rectHelpDelay = new Rect();
        mLayoutHelpDelayAccount.getGlobalVisibleRect(rectHelpDelay);
        if (mLayoutHelpDelayAccount.getVisibility() == View.VISIBLE && !rectHelpDelay.contains((int) event.getRawX(), (int) event.getRawY())) {
            hideHelpDelay();
            return true;
        } else if (mLayoutHelpDelayAccount.getVisibility() == View.INVISIBLE) {
            return true;
        }

        Rect rectMyAccountList = new Rect();
        mLayoutMyAccountList.getGlobalVisibleRect(rectMyAccountList);
        if (mLayoutMyAccountList.getVisibility() == View.VISIBLE && !rectMyAccountList.contains((int) event.getRawX(), (int) event.getRawY())) {
            hideMyAccountList();
            return true;
        } else if (mLayoutMyAccountList.getVisibility() == View.INVISIBLE) {
            return true;
        }

        Rect rectEditAccountNum = new Rect();
        mEditAccountNum.getGlobalVisibleRect(rectEditAccountNum);
        if (mEditAccountNum.getVisibility() == View.VISIBLE
                && !mLayoutMyAccountList.isShown()
                && rectEditAccountNum.contains((int) event.getRawX(), (int) event.getRawY())) {
            if (mLayoutHelpDelayAccount.isShown()) {
                hideHelpDelay();
            }
            if (mLayoutHelp.isShown()) {
                mLayoutHelp.setVisibility(View.GONE);
                mLayoutSendDate.setVisibility(View.VISIBLE);
            }
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02 && !mLayoutTransferRecentlyList.isShown()) {
                showTransferRecentlyList();
            }
        }
        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onDateSet(long date) {
        MLog.d();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        mInputYear = calendar.get(Calendar.YEAR);
        mInputMonth = calendar.get(Calendar.MONTH) + 1;
        mInputDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (mIstoday && (mCurYear != mInputYear || mCurDay != mInputDay)) {
            mIstoday = false;
        }

        SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(
                TransferAccountActivity.this,
                Const.PICKER_TYPE_TIME,
                mIstoday,
                mIsDelayService
        );
        slidingDateTimerPickerDialog.setOnConfirmListener(new SlidingDateTimerPickerDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress(int year, int month, int day, int time, boolean needtimeselect, boolean istoday, boolean isDelayService) {
                String inputDate = String.valueOf(mInputYear) + "년 " +
                        String.valueOf(mInputMonth) + "월 " +
                        String.valueOf(mInputDay) + "일 " +
                        String.format("%02d", time) + ":00";
                mTextInputDate.setText(inputDate);
                mInputHour = time;
                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                    mLastTransferInfo.setTRNF_DVCD("3");
                    mLastTransferInfo.setTRNF_DMND_DT(String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay));
                    mLastTransferInfo.setTRNF_DMND_TM(String.format("%02d0000", mInputHour));
                }
                mBtnDropdownSendDate.setVisibility(View.GONE);
                mBtnEraseSendDate.setVisibility(View.VISIBLE);
            }
        });
        slidingDateTimerPickerDialog.show();
    }

    @Override
    public void onKeyboardShown() {
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 300);
    }

    @Override
    public void onKeyboardHidden() {
        mIsShownKeyboard = false;
    }

// ==============================================================================================================
// LAYOUT
// ==============================================================================================================
    /**
     * 화면 초기화
     */
    private void initUX() {
        mScrollHandler = new WeakHandler(TransferAccountActivity.this);
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(TransferAccountActivity.this);
        mLayoutTotalTransfer = (LinearLayout) findViewById(R.id.layout_total_transfer_account);
        mTextSendNum = (TextView) findViewById(R.id.textview_send_num_multi_transfer);
        mTextSendAmount = (TextView) findViewById(R.id.textview_send_amount_multi_transfer);
        mLayoutMyAccountList = (RelativeLayout) findViewById(R.id.layout_my_account_list);
        mListViewMyAccount = (ListView) findViewById(R.id.listview_my_account_list);
        mBtnCloseMyAccountList = (RelativeLayout) findViewById(R.id.layout_close_my_account_list);
        mBtnWithdrawAccount = (RelativeLayout) findViewById(R.id.layout_account_transfer_account);
        mTextAccountName = (TextView) findViewById(R.id.textview_account_name_transfer_account);
        mTextAccount = (TextView) findViewById(R.id.textview_account_transfer_account);
        mImageShare = (ImageView) findViewById(R.id.imageview_share);
        mImageClock = (ImageView) findViewById(R.id.imageview_clock);
        mTextAccountBalance = (TextView) findViewById(R.id.textview_account_balance_transfer_account);
        mBtnBalanceDropdown = (ImageView) findViewById(R.id.imageview_account_balance_dropdown);
        mBtnLockDropdown = (ImageView) findViewById(R.id.imageview_lock_balance_dropdown);
        mLayoutStep01 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_01);
        mTextHangulAmount = (TextView) findViewById(R.id.textview_hangul_amount_transfer_account);
        mLayoutStep01Body = (LinearLayout) findViewById(R.id.layout_transfer_account_step_01_body);
        mViewEditAmountLine = (View) findViewById(R.id.view_edit_amount_bar);
        mProgressLine = (ProgressBar) findViewById(R.id.progress_edit_amount_bar);
        mLayoutStep01Bottom = (RelativeLayout) findViewById(R.id.layout_transfer_account_step_01_bottom);
        mEditAmount = (EditText) findViewById(R.id.edittext_amount_transfer_account);
        mBtnEraseAmount = (RelativeLayout) findViewById(R.id.imageview_erase_transfer_amount);
        mLayoutLimitTransfer = (LinearLayout) findViewById(R.id.layout_limit_transfer);
        mTextLimitOneTime = (TextView) findViewById(R.id.textview_limit_one_time);
        mTextLimitOneDay = (TextView) findViewById(R.id.textview_limit_one_day);
        mTextErrMsg = (TextView) findViewById(R.id.textview_err_msg_transfer_account);
        mLayoutStep02 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_02);
        mTextDisplayTransferAmount = (TextView) findViewById(R.id.textview_display_transfer_amount);
        mTextBankName = (TextView) findViewById(R.id.textview_bank_transfer_account);
        mBtnSelectBank = (LinearLayout) findViewById(R.id.layout_select_bank_transfer_account);
        mEditAccountNum = (EditText) findViewById(R.id.edittext_account_num_transfer_account);
        mBtnEraseAccount = (RelativeLayout) findViewById(R.id.imageview_erase_transfer_account);
        mViewStep02Bar = (View) findViewById(R.id.view_step02_bar);
        mLayoutStep03 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_03);
        mScrollviewStep03 = (ScrollView) findViewById(R.id.scrollview_step_03);
        mTextDisplayRecipient = (TextView) findViewById(R.id.textview_display_recipient);
        mTextDisplaySending = (TextView) findViewById(R.id.textview_display_sending);
        mLayoutOpenCloseInput = (RelativeLayout) findViewById(R.id.layout_open_close_input);
        mBtnOpenCloseInput = (ToggleButton) findViewById(R.id.btn_open_close_input);
        mLayoutAddInpuntItem = (RelativeLayout) findViewById(R.id.layout_add_input_item);
        mTextInputDate = (TextView) findViewById(R.id.textview_input_date);
        mTextInputMemo = (TextView) findViewById(R.id.textview_input_memo);
        mLayoutDelayDesc = (LinearLayout) findViewById(R.id.layout_delay_desc);
        mBtnConfirm = (Button) findViewById(R.id.btn_ok_transfer_account);
        mBtnSendTransfer = (Button) findViewById(R.id.btn_send_transfer_account);
        mLayoutTransferRecentlyList = (LinearLayout) findViewById(R.id.layout_transfer_recently_list);
        mLayoutNoTransferRecently = (LinearLayout) findViewById(R.id.layout_no_transfer_recently_list);
        mListViewTransferRecently = (SwipeMenuListView) findViewById(R.id.listview_transfer_recently_list);
        mLayoutHelpDelayAccount = (LinearLayout) findViewById(R.id.layout_help_delay_account);

        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_transfer_account);
        btnCancel.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showCancelMessage();
            }
        });

        mListViewMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                MLog.d();

                TransferManager.getInstance().clear();
                mLastTransferInfo = null;

                MLog.line();
                MLog.info("position >> " + position);
                MLog.info("size >> " + mListMyAccount.size());
                MLog.line();

                MyAccountInfo accountInfo = mListMyAccount.get(position);
                if (accountInfo == null)
                    return;

                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }

                String ACNO = accountInfo.getACNO();
                String partACNO = "";
                if (!TextUtils.isEmpty(ACNO)) {
                    int lenACNO = ACNO.length();
                    if (lenACNO > 4)
                        partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                }

                mTextAccountName.setText(ACCO_ALS);
                mTextAccount.setText(" [" + partACNO + "]");

                String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                    mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    mTextAccountBalance.setText(amount + getString(R.string.won));
                }

                if (mLayoutTransferRecentlyList.isShown()) {
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);
                }

                //커플계좌상태를 보여준다.
                String SHRN_ACCO_YN = accountInfo.getSHRN_ACCO_YN();
                if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SHRN_ACCO_YN)) {
                    mImageShare.setVisibility(View.VISIBLE);
                }else{
                    mImageShare.setVisibility(View.GONE);
                }


                requestInit(ACNO, position, true);
            }
        });

        mBtnCloseMyAccountList.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideMyAccountList();
            }
        });

        LinearLayout btnDirectTransfer = (LinearLayout) findViewById(R.id.layout_send_multi_transfer_account);
        btnDirectTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (TransferManager.getInstance().size() > 0 && (mLayoutTotalTransfer.getVisibility() == View.VISIBLE)) {
                    TransferManager.getInstance().setIsNotRemove(true);
                    showVerifyTransferAccount();
                }
            }
        });

        initStep01();
        initStep02();
        initStep03();

        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                MLog.d();
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                    if (!checkVaildAmount()) {
                        AniUtils.shakeView(mEditAmount, ANIMATION_SHAKE_OFFSET);
                        mViewEditAmountLine.setBackgroundResource(R.color.red);
                        return;
                    }
                    if (mEditAmount.isFocused()) {
                        mEditAmount.clearFocus();
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAmount);
                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                requestTransferAccountList(false);
                            }
                        }, 250);
                    } else {
                        requestTransferAccountList(false);
                    }
                } else if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();
                    String amount = mEditAmount.getText().toString();
                    amount = amount.replaceAll("[^0-9]", Const.EMPTY);
                    if (DataUtil.isNull(bankName) || DataUtil.isNull(accountNum) || DataUtil.isNull(amount)) {
                        return;
                    }
                    if (mEditAccountNum.isFocused()) {
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                    }
                    requestVerifyTransferAccount(mSendBankStockCode, accountNum, amount);
                }
            }
        });

        mBtnSendTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                MLog.d();
                if ((mTransferStep != TRANSFER_ACCOUNT_STEP.STEP_03) || (DataUtil.isNull(mLastTransferInfo))) {
                    return;
                }
                if (mLayoutTransferRecentlyList.isShown()) {
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);
                }
                requestConfirmTransferAccount(mLastTransferInfo);
            }
        });

        if (CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_YN().equals("Y")){
            showCheatUseAccountDialog();
        }
    }

    /**
     * 사기이용계좌 금융거래제한 안내 팝업
     */
    private void showCheatUseAccountDialog(){
        String replaceStr = "";
        String str = "고객님께서는 금융기관에\n전자금융거래제한자로 등록되어 있어 거래가\n불가합니다";

        String FinancialInstitutionNM = CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_FIN_INST_NM(); //금융기관명
        if (!FinancialInstitutionNM.isEmpty()){
            replaceStr = str.replace("금융기관", FinancialInstitutionNM);
        }

        DialogUtil.alert(TransferAccountActivity.this,
                replaceStr ,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
    }

    @SuppressLint("HandlerLeak")
    private class WeakHandler extends Handler {

        private WeakReference<TransferAccountActivity> mWeakActivity;

        WeakHandler(TransferAccountActivity weakactivity) {
            mWeakActivity = new WeakReference<>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            TransferAccountActivity weakActivity = mWeakActivity.get();
            if (DataUtil.isNotNull(weakActivity)) {
                if (msg.what == SCROLL_CHECK_KEYBOARD) {
                    mIsShownKeyboard = true;
                } else if (msg.what == SCROLL_EDITTEXT && !mIsShownKeyboard) {
                    mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + (int) Utils.dpToPixel(TransferAccountActivity.this, 50f));
                }
            }
        }
    }
// ==============================================================================================================
// STEP.01
// ==============================================================================================================
    /**
     * 타이틀, 이체금액 입력 화면
     */
    private void initStep01() {
        if (isFinishing()) return;
        mBtnWithdrawAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLayoutHelpDelayAccount.isShown()) {
                    return;
                }
                if (mEditAmount.isFocused()) {
                    mEditAmount.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAmount);
                }
                int sendNum = TransferManager.getInstance().size();
                if (sendNum > 0) {
                    AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                    alertDialog.mPListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    };
                    alertDialog.msg = getString(R.string.msg_no_select_account_multi_transfer);
                    alertDialog.show();
                    return;
                }
                mBtnWithdrawAccount.animate().translationY(-500).withLayer();
                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showMyAccountList();
                    }
                }, 250);
            }
        });

        mEditAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (!TextUtils.isEmpty(charSequence.toString()) && !charSequence.toString().equals(mInputAmount)) {
                    //Firebase에러 - 계좌로 보내기에서 금액 클립보드 복사 텍스트 붙여넣기시 에러.
                    String amountStr = charSequence.toString().replaceAll("[^0-9]", Const.EMPTY);
                    mInputAmount = Utils.moneyFormatToWon(Double.parseDouble(amountStr));
                    mEditAmount.setText(mInputAmount);
                    mEditAmount.setSelection(mInputAmount.length());
                    String hangul = Utils.convertHangul(mInputAmount);
                    if (!TextUtils.isEmpty(hangul)) {
                        hangul += getString(R.string.won);
                        mTextHangulAmount.setText(hangul);
                    }
                } else {
                    if (TextUtils.isEmpty(charSequence.toString())) {
                        mTextHangulAmount.setText(Const.EMPTY);
                        mInputAmount = "";
                        mBtnConfirm.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String amount = editable.toString();
                amount = amount.replaceAll("[^0-9]", Const.EMPTY);
                if (!TextUtils.isEmpty(amount) && Double.valueOf(amount) > 0) {
                    if (!mTextHangulAmount.isShown())
                        mTextHangulAmount.setVisibility(View.VISIBLE);
                    if (!checkVaildAmount()) {
                        mViewEditAmountLine.setBackgroundResource(R.color.red);
                        mLayoutLimitTransfer.setVisibility(View.GONE);
                        mBtnConfirm.setEnabled(false);
                        AniUtils.shakeView(mEditAmount, ANIMATION_SHAKE_OFFSET);
                        return;
                    }
                    if (mTextErrMsg.isShown()) {
                        mTextErrMsg.setText(Const.EMPTY);
                        mTextErrMsg.setVisibility(View.GONE);
                    }
                    if (!mLayoutLimitTransfer.isShown())
                        mLayoutLimitTransfer.setVisibility(View.VISIBLE);
                    if (!mBtnEraseAmount.isShown())
                        mBtnEraseAmount.setVisibility(View.VISIBLE);
                    mViewEditAmountLine.setBackgroundResource(R.color.color00D5E7);
                } else {
                    mBtnConfirm.setEnabled(false);
                    mBtnEraseAmount.setVisibility(View.GONE);
                    if (mTextErrMsg.isShown()) {
                        mTextErrMsg.setText(Const.EMPTY);
                        mTextErrMsg.setVisibility(View.GONE);
                    }
                    if (!mLayoutLimitTransfer.isShown())
                        mLayoutLimitTransfer.setVisibility(View.VISIBLE);
                    mViewEditAmountLine.setBackgroundResource(R.color.color00D5E7);
                }
            }
        });

        mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    if (DataUtil.isNotNull(mEditAmount.getText().toString()) && !mBtnEraseAmount.isShown()) {
                        mBtnEraseAmount.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (mBtnEraseAmount.isShown())
                        mBtnEraseAmount.setVisibility(View.GONE);
                }
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
            }
        });

        mBtnEraseAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditAmount.setText(Const.EMPTY);
            }
        });

        if (mDlyTrnfSvcEntrYn || mDsgtMnrcAccoSvcEntrYn) {
            if (mDlyTrnfSvcEntrYn) {
                mImageClock.setImageResource(R.drawable.ico_clock);
            }
            if (mDsgtMnrcAccoSvcEntrYn) {
                mImageClock.setImageResource(R.drawable.ico_check_fill);
            }
            mImageClock.setVisibility(View.VISIBLE);
        }

        mImageClock.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mDsgtMnrcAccoSvcEntrYn)
                    return;
                showHelpDelay();
            }
        });


        RelativeLayout btnHideHelpDelay = (RelativeLayout) findViewById(R.id.layout_close_help_delay);
        btnHideHelpDelay.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideHelpDelay();
            }
        });
    }

    /**
     * 지연이체 계좌 설명을 보여준다.
     */
    private void showHelpDelay() {
        MLog.d();

        if (isFinishing())
            return;

        mLayoutHelpDelayAccount.setVisibility(View.INVISIBLE);
        Animation animSlideTopBody = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_list_down);
        mLayoutHelpDelayAccount.clearAnimation();
        mLayoutHelpDelayAccount.startAnimation(animSlideTopBody);
        mLayoutHelpDelayAccount.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutHelpDelayAccount.setVisibility(View.VISIBLE);

                if (isFinishing())
                    return;

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mLayoutHelpDelayAccount.isShown()) {
                            hideHelpDelay();
                        }
                    }
                }, 5000);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 입금 금액 입력화면
     *
     * @param isClearAmount 입력 금액 삭제 여부
     */
    private void showStep01(boolean isClearAmount) {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_01;

        mTextErrMsg.setVisibility(View.GONE);
        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

        mBtnConfirm.setVisibility(View.VISIBLE);
        mBtnSendTransfer.setVisibility(View.GONE);

        if (mLayoutTransferRecentlyList.isShown())
            mLayoutTransferRecentlyList.setVisibility(View.GONE);

        mLayoutStep01.setVisibility(View.VISIBLE);

        if (isClearAmount) {
            mEditAmount.setText(Const.EMPTY);
        } else {
            mBtnConfirm.setEnabled(true);
        }

        TransferManager.getInstance().setIsNotRemove(false);

        updateSendMultiTransfer(true);
    }

    /**
     * 최초 화면 진입 인터액션
     */
    private void interActionStep01() {
        MLog.d();
        Animation animSlideTopBody = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_fade_in_transfer_view);
        mLayoutStep01Body.clearAnimation();
        mLayoutStep01Body.startAnimation(animSlideTopBody);
        mLayoutStep01Body.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01Body.setVisibility(View.VISIBLE);
                showProgressLine();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 금액 입력란 사라지는 interaction
     */
    private void interActionHideStep01() {
        MLog.d();
        Animation animSlideOut = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_fade_out_transfer_view);
        mLayoutStep01.clearAnimation();
        mLayoutStep01.startAnimation(animSlideOut);
        mLayoutStep01.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTotalTransfer.setVisibility(View.GONE);
                mLayoutStep01.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mLayoutStep02.setVisibility(View.INVISIBLE);
                interActionShowStep02();
            }
        }, 100);
    }

    /**
     * 지연이체 계좌 설명 감추기
     */
    private void hideHelpDelay() {
        MLog.d();

        if (isFinishing())
            return;

        mLayoutHelpDelayAccount.setVisibility(View.INVISIBLE);
        Animation animSlideTopBody = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_list_up);
        mLayoutHelpDelayAccount.clearAnimation();
        mLayoutHelpDelayAccount.startAnimation(animSlideTopBody);
        mLayoutHelpDelayAccount.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutHelpDelayAccount.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 전체 입출금계좌 리스트 표시
     */
    private void showMyAccountList() {
        MLog.d();

        if (isFinishing())
            return;

        mLayoutMyAccountList.setVisibility(View.INVISIBLE);
        mListViewMyAccount.setVisibility(View.VISIBLE);
        mBtnCloseMyAccountList.setVisibility(View.VISIBLE);

        Animation animSlideDown = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_list_down);
        mLayoutMyAccountList.clearAnimation();
        mLayoutMyAccountList.startAnimation(animSlideDown);
        mLayoutMyAccountList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutMyAccountList.setVisibility(View.VISIBLE);
                mLayoutMyAccountList.bringToFront();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 인증 수단별 입력 횟수 오류 체크
     */
    private void checkVaildAuthMethod() {
        MLog.d();

        if (isFinishing())
            return;

        // 핀코드 오류 횟수 체크
        if (Prefer.getPinErrorCount(TransferAccountActivity.this) >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
            dismissProgressDialog();
            AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
            alertDialog.mNListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };
            alertDialog.mPListener = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.PINCODE_FORGET);
                    commonUserInfo.setMBRnumber(DataUtil.isNotNull(LoginUserInfo.getInstance().getMBR_NO()) ? LoginUserInfo.getInstance().getMBR_NO() : Const.EMPTY);
                    String accn = LoginUserInfo.getInstance().getODDP_ACCN();
                    commonUserInfo.setHasAccount((DataUtil.isNull(accn) || Integer.parseInt(accn) < 1));
                    Intent intent = new Intent(TransferAccountActivity.this, ReregLostPincodeActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivity(intent);
                    finish();
                }
            };
            alertDialog.msg = String.format(getString(R.string.msg_err_over_match_pin), Const.SSENSTONE_AUTH_COUNT_FAIL);
            alertDialog.mPBtText = getString(R.string.rereg);
            alertDialog.show();
            return;
        }

        final LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();

        if (DataUtil.isNull(SECU_MEDI_DVCD)) {
            checkMyAccount();
            return;
        }

        if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
            String OTP_STCD = loginUserInfo.getOTP_STCD();
            final String MOTP_EROR_TCNT = loginUserInfo.getMOTP_EROR_TCNT();
            if ("120".equalsIgnoreCase(OTP_STCD)) { // 사고 분실
                dismissProgressDialog();

                AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CUS0030101.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };
//                alertDialog.mLinkListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CRT0040101.getServiceUrl();
//                        String param = null;
//                        if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
//                            param = "TRTM_DVCD=ACDTLOCK";
//                        } else {
//                            param = "TRTM_DVCD=ACDT";
//                        }
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        intent.putExtra(Const.INTENT_PARAM, param);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
                alertDialog.mTopText = getString(R.string.msg_otp_accident_topmsg);
                alertDialog.msg = getString(R.string.msg_otp_accdent_contents);
                alertDialog.mPBtText = getString(R.string.msg_report_otp);
//                alertDialog.mLinkText = getString(R.string.msg_motp_issue);
                alertDialog.show();

            } else if ("130".equalsIgnoreCase(OTP_STCD)) { // 폐기된 인증서
                dismissProgressDialog();

                AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0220101.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };
                alertDialog.mTopText = getString(R.string.msg_otp_disposal_topmsg);
                alertDialog.msg = getString(R.string.msg_otp_disposal_contsnts);
                alertDialog.mPBtText = getString(R.string.msg_otp_other_disposal_termination);
                alertDialog.show();

            } else if ("140".equalsIgnoreCase(OTP_STCD)) { // 오류 횟수 초과
                dismissProgressDialog();

                AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };
                alertDialog.msg = getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = getString(R.string.msg_init_otp);
                alertDialog.show();

            } else if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                dismissProgressDialog();

                AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };
                alertDialog.msg = getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = getString(R.string.msg_init_otp);
                alertDialog.show();

            } else {
                checkMyAccount();
            }
        } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {

            if (Const.BRIDGE_RESULT_YES.equals(loginUserInfo.getMOTP_END_YN())) {
                dismissProgressDialog();
                AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
                        String param = "TRTM_DVCD=" + 1;
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        intent.putExtra(Const.INTENT_PARAM, param);
                        startActivity(intent);
                        finish();
                    }
                };
                alertDialog.msg = getString(R.string.msg_motp_expired);
                alertDialog.mPBtText = getString(R.string.common_reissue);
                alertDialog.show();
                return;
            }

            if (DataUtil.isNotNull(loginUserInfo.getMOTP_EROR_TCNT()) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                dismissProgressDialog();
                AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
                        String param = "TRTM_DVCD=" + 1;
                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        intent.putExtra(Const.INTENT_PARAM, param);
                        startActivity(intent);
                        finish();
                    }
                };
                alertDialog.msg = getString(R.string.msg_motp_lost);
                alertDialog.mPBtText = getString(R.string.common_reissue);
                alertDialog.show();
                return;
            }

            showProgressDialog();

            MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                @Override
                public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                    if (!"0000".equals(resultCode)) {

                        dismissProgressDialog();

                        AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                        alertDialog.mNListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        String deviceId = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
                        String motpDeviceId = LoginUserInfo.getInstance().getMOTP_SMPH_DEVICE_ID();
                        if (TextUtils.isEmpty(deviceId) || TextUtils.isEmpty(motpDeviceId)) {
                            alertDialog.mPListener = new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                                    String url = WasServiceUrl.CRT0040401.getServiceUrl();
                                    String param = "TRTM_DVCD=" + 1;
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                                    intent.putExtra(Const.INTENT_PARAM, param);
                                    startActivity(intent);
                                    finish();
                                }
                            };
                        } else {
                            if (deviceId.equals(motpDeviceId)) {
                                alertDialog.mPListener = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                                        String url = WasServiceUrl.CRT0040501.getServiceUrl();
                                        intent.putExtra("url", url);
                                        startActivity(intent);
                                        finish();
                                    }
                                };
                            } else {
                                alertDialog.mPListener = new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
                                        String param = "TRTM_DVCD=" + 1;
                                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                                        intent.putExtra(Const.INTENT_PARAM, param);
                                        startActivity(intent);
                                        finish();
                                    }
                                };
                            }
                        }
                        alertDialog.msg = getString(R.string.msg_motp_change_device);
                        alertDialog.mPBtText = getString(R.string.common_reissue);
                        alertDialog.show();

                    } else {
                        mMOTPSerialNumber = reqData1;
                        mMOTPTAVersion = reqData4;
                        checkMyAccount();
                    }
                }
            });
        } else {
            checkMyAccount();
        }
    }
// ==============================================================================================================
// STEP.02
// ==============================================================================================================
    /**
     * 이체계좌 선택 화면
     */
    private void initStep02() {
        mTextDisplayTransferAmount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (DataUtil.isNotNull(mLastTransferInfo)) {
                    mLastTransferInfo.setCUST_NM(mTextDisplaySending.getText().toString());
                    mLastTransferInfo.setRECV_NM(mTextDisplayRecipient.getText().toString());
                }
                if (mLayoutTransferRecentlyList.isShown()) {
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);
                }
                // 이체아이템 삭제
                if (DataUtil.isNotNull(mTextBankName.getText().toString()) && mTransferCnt > 0) {
                    mTransferCnt--;
                    MLog.i("mTransferCnt >> " + mTransferCnt);
                }
                mLayoutStep02.setVisibility(View.INVISIBLE);
                Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_out_bottom_sbi);
                mLayoutStep02.clearAnimation();
                mLayoutStep02.startAnimation(animation);
                mLayoutStep02.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        hideStep03(false);
                        hideStep02(false);
                        showStep01(false);
                        mViewEditAmountLine.setVisibility(View.GONE);
                        showProgressLine();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        mBtnSelectBank.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mListBank.isEmpty()) {
                    requestBankList();
                    return;
                }
                if (mListStock.isEmpty()) {
                    requestStockList();
                    return;
                }
                // 이체아이템 삭제
                if (DataUtil.isNotNull(mTextBankName.getText().toString()) && mTransferCnt > 0) {
                    mTransferCnt--;
                    MLog.i("mTransferCnt >> " + mTransferCnt);
                }
                showBankStockList();
            }
        });

        mEditAccountNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                        if (!mBtnConfirm.isShown())
                            mBtnConfirm.setVisibility(View.VISIBLE);
                        if (mBtnSendTransfer.isShown())
                            mBtnSendTransfer.setVisibility(View.GONE);
                        hideStep03(false);
                    }
                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_03) {
                        hideStep03(false);
                        if (!mBtnConfirm.isShown())
                            mBtnConfirm.setVisibility(View.VISIBLE);
                        if (mBtnSendTransfer.isShown())
                            mBtnSendTransfer.setVisibility(View.GONE);
                    }

                    String amount = mEditAmount.getText().toString();
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();

                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum)) {
                        mBtnConfirm.setEnabled(false);
                    } else {
                        if (TextUtils.isEmpty(amount))
                            mBtnConfirm.setEnabled(false);
                        else
                            mBtnConfirm.setEnabled(true);
                    }

                    if (!mLayoutTransferRecentlyList.isShown()) {
                        showTransferRecentlyList();
                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAccountNum.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                            }
                        }, 500);
                    }
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                }
            }
        });

        mEditAccountNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!TextUtils.isEmpty(mTextBankName.getText().toString())) {
                    if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_03) {
                        hideStep03(true);
                    }
                    if (editable.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        mBtnConfirm.setEnabled(true);
                    } else
                        mBtnConfirm.setEnabled(false);

                    mBtnEraseAccount.setVisibility(View.VISIBLE);

                    if (!mBtnConfirm.isShown())
                        mBtnConfirm.setVisibility(View.VISIBLE);

                    if (mBtnSendTransfer.isShown())
                        mBtnSendTransfer.setVisibility(View.GONE);
                } else {
                    hideStep03(true);
                    mBtnConfirm.setEnabled(false);
                }
            }
        });

        mEditAccountNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    String bankName = mTextBankName.getText().toString();
                    String accountNum = mEditAccountNum.getText().toString();
                    if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum))
                        return false;

                    if (mEditAccountNum.isFocused()) {
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                    }

                    if (accountNum.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        String amount = mEditAmount.getText().toString();
                        if (TextUtils.isEmpty(amount))
                            return false;

                        amount = amount.replaceAll("[^0-9]", Const.EMPTY);
                        requestVerifyTransferAccount(mSendBankStockCode, accountNum, amount);
                    } else {
                        showErrorMessage(String.format(getString(R.string.msg_input_limit_length_account), Const.MAX_TRANSFER_ACOOUNT_LENGTH));
                    }
                }
                return false;
            }
        });

        mBtnEraseAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideStep03(true);
                mBtnConfirm.setEnabled(false);
                mBtnConfirm.setVisibility(View.VISIBLE);
                mBtnSendTransfer.setVisibility(View.GONE);
                mEditAccountNum.setText(Const.EMPTY);
                mBtnEraseAccount.setVisibility(View.GONE);
                // 이체아이템 삭제
                if (mTransferCnt > 0) {
                    mTransferCnt--;
                    MLog.i("mTransferCnt >> " + mTransferCnt);
                }
                if (!mLayoutTransferRecentlyList.isShown()) {
                    showTransferRecentlyList();
                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mEditAccountNum.requestFocus();
                            Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                        }
                    }, 500);
                } else {
                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mEditAccountNum.requestFocus();
                            Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                        }
                    }, 100);
                }
            }
        });

        mListViewTransferRecently.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (Utils.isEnableClickEvent()) {
                    ITransferRecentlyInfo accountInfo = mListTransferRecently.get(position);
                    if (accountInfo == null)
                        return;

                    String bankStockCode = accountInfo.getMNRC_BANK_CD();
                    if ("000".equalsIgnoreCase(bankStockCode))
                        bankStockCode = "028";

                    if (!TextUtils.isEmpty(bankStockCode)) {
                        String nameBankStock = "";
                        for (int index = 0; index < mListBank.size(); index++) {
                            RequestCodeInfo codeInfo = mListBank.get(index);
                            if (codeInfo == null)
                                continue;

                            if (bankStockCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                                nameBankStock = codeInfo.getCD_NM();
                                mSendBankStockCode = codeInfo.getSCCD();
                            }
                        }

                        for (int index = 0; index < mListStock.size(); index++) {
                            RequestCodeInfo codeInfo = mListStock.get(index);
                            if (codeInfo == null)
                                continue;

                            if (bankStockCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                                nameBankStock = codeInfo.getCD_NM();
                                mSendBankStockCode = codeInfo.getSCCD();
                            }
                        }

                        if (!TextUtils.isEmpty(nameBankStock))
                            mTextBankName.setText(nameBankStock);
                    }

                    String accountNum = accountInfo.getMNRC_ACNO();
                    if (TextUtils.isEmpty(accountNum))
                        return;

                    mEditAccountNum.setText(accountNum);
                    if (mEditAccountNum.isFocused()) {
                        mEditAccountNum.clearFocus();
                        Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                    }

                    String amount = mEditAmount.getText().toString();
                    if (DataUtil.isNull(amount))
                        return;

                    amount = amount.replaceAll("[^0-9]", Const.EMPTY);

                    requestVerifyTransferAccount(mSendBankStockCode, accountNum, amount);

                }
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem editItem = new SwipeMenuItem(getApplicationContext());
                editItem.setBackground(new ColorDrawable(Color.rgb(0x8C, 0x97, 0xAC)));
                editItem.setWidth((int) Utils.dpToPixel(TransferAccountActivity.this, 66));
                editItem.setIcon(R.drawable.btn_write_w);
                menu.addMenuItem(editItem);

                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xE0, 0x25, 0x0C)));
                deleteItem.setWidth((int) Utils.dpToPixel(TransferAccountActivity.this, 66));
                deleteItem.setIcon(R.drawable.btn_delete_w);
                menu.addMenuItem(deleteItem);
            }
        };

        mListViewTransferRecently.setMenuCreator(creator);
        mListViewTransferRecently.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        mListViewTransferRecently.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        showEditAliasDialog(position);
                        break;
                    case 1:
                        showRemoveRecentlyListDialog(position);
                        break;
                    default:
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    /**
     * 입금할 계좌 입력 화면 표시
     */
    private void showStep02() {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;
        mLayoutTotalTransfer.setVisibility(View.INVISIBLE);

        String bankName = mTextBankName.getText().toString();
        String accountNum = mEditAccountNum.getText().toString();
        if (TextUtils.isEmpty(bankName) && TextUtils.isEmpty(accountNum)) {
            mBtnConfirm.setEnabled(false);
        } else {
            if (TextUtils.isEmpty(bankName) || TextUtils.isEmpty(accountNum)) {
                mBtnConfirm.setEnabled(false);
            } else
                mBtnConfirm.setEnabled(true);
        }

        interActionHideStep01();

        String amount = mEditAmount.getText().toString();
        if (!TextUtils.isEmpty(amount)) {
            mTextDisplayTransferAmount.setText(amount);

            if (mLastTransferInfo != null) {
                amount = amount.replaceAll(Const.COMMA, Const.EMPTY);
                mLastTransferInfo.setTRN_AMT(amount);
            }
        }

        if (DataUtil.isNotNull(mTagBankCode)) {
            String nameBankStock = "";
            String codeBankStock = "";

            for (int index = 0; index < mListBank.size(); index++) {
                RequestCodeInfo codeInfo = mListBank.get(index);
                if (codeInfo == null)
                    continue;

                if (mTagBankCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                    nameBankStock = codeInfo.getCD_NM();
                    codeBankStock = codeInfo.getSCCD();
                    break;
                }
            }

            if (DataUtil.isNull(codeBankStock)) {
                for (int index = 0; index < mListStock.size(); index++) {
                    RequestCodeInfo codeInfo = mListStock.get(index);
                    if (codeInfo == null)
                        continue;

                    if (mTagBankCode.equalsIgnoreCase(codeInfo.getSCCD())) {
                        nameBankStock = codeInfo.getCD_NM();
                        codeBankStock = codeInfo.getSCCD();
                        break;
                    }
                }
            }

            if (DataUtil.isNotNull(codeBankStock)) {
                mTextBankName.setText(nameBankStock);
                mSendBankStockCode = codeBankStock;
            }
            mTagBankCode = "";
        }

        if (DataUtil.isNotNull(mTagAccount)) {
            mEditAccountNum.setText(mTagAccount);
            mTagAccount = "";
        }
    }

    /**
     * 이체 계좌 입력 화면 interaction
     */
    private void interActionShowStep02() {
        MLog.d();
        Animation animSlideIn = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_in_transfer_verify);
        mLayoutStep02.clearAnimation();
        mLayoutStep02.startAnimation(animSlideIn);
        mLayoutStep02.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep02.setVisibility(View.VISIBLE);
                if (!mLayoutTransferRecentlyList.isShown())
                    showTransferRecentlyList();
                else {
                    String recipient = mTextDisplayRecipient.getText().toString();
                    String sending = mTextDisplaySending.getText().toString();
                    String memo = mTextInputMemo.getText().toString();
                    if (!TextUtils.isEmpty(recipient) || !TextUtils.isEmpty(sending) ||
                            !TextUtils.isEmpty(memo) ||
                            (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0)) {
                        showStep03();
                    }
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private void showStep02Bar() {
        MLog.d();
        Animation animSlideDown = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.fade_in_transfer_step02_bar);
        mViewStep02Bar.clearAnimation();
        mViewStep02Bar.startAnimation(animSlideDown);
        mViewStep02Bar.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewStep02Bar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 이체계좌 입력화면 숨기기
     *
     * @param isClear 입력값 삭제 여부
     */
    private void hideStep02(boolean isClear) {
        mLayoutStep02.setVisibility(View.GONE);
        mViewStep02Bar.setVisibility(View.INVISIBLE);
        clearStep02(isClear);
    }

    /**
     * 이체계좌 입력값 삭제
     *
     * @param isClear 입력값 삭제 여부
     */
    private void clearStep02(boolean isClear) {
        mTextDisplayTransferAmount.setText(Const.EMPTY);
        if (isClear) {
            mTextBankName.setText(Const.EMPTY);
            mEditAccountNum.setText(Const.EMPTY);
        }
        if (TransferManager.getInstance().size() <= 0) {
            mBtnBalanceDropdown.setVisibility(View.VISIBLE);
            mBtnLockDropdown.setVisibility(View.GONE);
            mBtnWithdrawAccount.setClickable(true);
        }
    }

    /**
     * 최초 화면 진입 금액입력 하단바 인터액션
     */
    private void showProgressLine() {
        mProgressLine.setVisibility(View.VISIBLE);
        showFadeInLimitAmount();
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mCount >= 100) {
                    timer.cancel();
                    mCount = 0;
                    showAmountLine();
                } else {
                    updateProgressLine();
                }
            }
        }, 0, 2);
    }

    /**
     * 금액입력란 하단 이체 한도 fade in
     */
    private void showFadeInLimitAmount() {
        MLog.d();
        mLayoutStep01Bottom.setVisibility(View.INVISIBLE);
        Animation animSlideTopBody = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.fade_in_transfer_limit);
        mLayoutStep01Bottom.clearAnimation();
        mLayoutStep01Bottom.startAnimation(animSlideTopBody);
        mLayoutStep01Bottom.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01Bottom.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 하단바가 좌에서 우로 이동 완료 후 실제 입력란 표시 및 포커스
     */
    private void showAmountLine() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressLine.setVisibility(View.INVISIBLE);
                mViewEditAmountLine.setVisibility(View.VISIBLE);
                mEditAmount.requestFocus();
                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
            }
        });
    }

    /**
     * 금액입력 하단바 하단바가 좌에서 우로 이동
     */
    private void updateProgressLine() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCount++;
                mProgressLine.setProgress(mCount);
            }
        });
    }

    /**
     * 은행 및 증권사 목록을 보여준다.
     */
    private void showBankStockList() {
        MLog.d();

        if (isFinishing() || (DataUtil.isNotNull(dialogBankPlus) && dialogBankPlus.isShowing()))
            return;

        if (mLayoutMyAccountList.isShown())
            hideMyAccountList();

        if (mEditAccountNum.isFocused()) {
            mEditAccountNum.clearFocus();
            Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
        }

        final BankStockAdapter adapter = new BankStockAdapter(TransferAccountActivity.this, mListBank, mListStock);
        Holder holder = new ListHolder();
        View viewHeader = getLayoutInflater().inflate(R.layout.dialog_backstock_header, null);
        TextView textTitle = viewHeader.findViewById(R.id.textview_dialog_slide_header);
        textTitle.setText(getString(R.string.title_bank_stock));

        dialogBankPlus = DialogPlus.newDialog(TransferAccountActivity.this)
                .setContentHolder(holder)
                .setAdapter(adapter)
                .setCancelable(true)
                .setExpanded(true, (int) Utils.dpToPixel(TransferAccountActivity.this, 245 + 68 + 45 + 14))
                .setHeader(viewHeader)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        if (position < 1) return;

                        RequestCodeInfo codeInfo = null;
                        if (adapter.getTabState() == Const.BANK_STOCK_MODE.BANK_MODE) {
                            codeInfo = (RequestCodeInfo) mListBank.get(position - 1);
                        } else if (adapter.getTabState() == Const.BANK_STOCK_MODE.STOCK_MODE) {
                            codeInfo = (RequestCodeInfo) mListStock.get(position - 1);
                        }

                        if (DataUtil.isNull(codeInfo))
                            return;

                        String bankName = codeInfo.getCD_NM();
                        mTextBankName.setText(bankName);
                        mSendBankStockCode = codeInfo.getSCCD();

                        mEditAccountNum.setText(Const.EMPTY);
                        if (mBtnEraseAccount.isShown())
                            mBtnEraseAccount.setVisibility(View.GONE);

                        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;

                        dialog.dismiss();

                        if (!mLayoutTransferRecentlyList.isShown()) {
                            Handler delayHandler = new Handler();
                            delayHandler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    showTransferRecentlyList();
                                }
                            }, 500);
                        }
                    }
                })
                .setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(DialogPlus dialog) {
                        if (mEditAccountNum.isFocused()) {
                            Utils.showKeyboard(TransferAccountActivity.this, mEditAccountNum);
                        }
                        dialogBankPlus = null;
                    }
                })
                .create();
        dialogBankPlus.show();
    }

    /**
     * 은행 및 증권사 목록을 닫는다.
     */
    private void hideBankStockList() {
        if (DataUtil.isNotNull(dialogBankPlus) && dialogBankPlus.isShowing()) {
            dialogBankPlus.dismiss();
            dialogBankPlus = null;
        }
    }

    /**
     * 계좌 별칭 수정 화면 표시
     *
     * @param position 선택된 리스트 아이템 위치
     */
    void showEditAliasDialog(final int position) {
        MLog.d();
        if (isFinishing()) return;

        TransferEditAliasDialog dialog = new TransferEditAliasDialog(TransferAccountActivity.this, new TransferEditAliasDialog.OnBtnClickListener() {
            @Override
            public void onBtnClick(View v, String alias) {
                requestRegisterAlias(position, alias);
            }
        });
        dialog.show();

        /*
        final Dialog dialog = new Dialog(TransferAccountActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_edit_transfer_account);

        Button btnCancel = (Button) dialog.findViewById(R.id.btn_negative);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFinishing())
                    return;
                dialog.dismiss();
            }
        });

        final EditText editAlias = (EditText) dialog.findViewById(R.id.edittext_edit_transfer_account);
        Button btnOK = (Button) dialog.findViewById(R.id.btn_positive);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (isFinishing())
                    return;
                String alias = editAlias.getText().toString();
                if (DataUtil.isNull(alias)) {
                    Toast.makeText(TransferAccountActivity.this, getString(R.string.msg_no_alias_input), Toast.LENGTH_SHORT).show();
                    return;
                }
                dialog.dismiss();
                requestRegisterAlias(position, alias);
            }
        });

        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        View view = dialog.getWindow().getDecorView();
        view.setBackgroundResource(android.R.color.transparent);

        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        lp.width = size.x;
        dialog.getWindow().setAttributes(lp);
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
         */
    }

    /**
     * 이체계좌 삭제메세지 화면 표시
     *
     * @param position 선택된 리스트 아이템 위치
     */
    void showRemoveRecentlyListDialog(final int position) {
        MLog.d();
        if (isFinishing()) return;

        DialogUtil.alert(this, getString(R.string.msg_ask_remove), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestRemoveRecentlyListItem(position);
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

    }
// ==============================================================================================================
// STEP.03
// ==============================================================================================================

    /**
     * 이체 정보 입력 화면
     */
    private void initStep03() {
        String hintRecipient = String.format(getString(R.string.msg_input_limit_length), 10);
        mTextDisplayRecipient.setHint(hintRecipient);
        RelativeLayout layoutDisplayRecipient = (RelativeLayout) findViewById(R.id.layout_display_recipient);
        layoutDisplayRecipient.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditAccountNum.isFocused()) {
                    mEditAccountNum.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                }
                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferAccountActivity.this,
                        getString(R.string.display_transfer_sending), getString(R.string.hint_display_recipient),
                        10, mTextDisplayRecipient.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextDisplayRecipient.setText(inputString);
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        final RelativeLayout btnEraseRecipient = (RelativeLayout) findViewById(R.id.imageview_erase_display_recipient);
        mTextDisplayRecipient.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseRecipient.setVisibility(View.GONE);
                } else {
                    btnEraseRecipient.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseRecipient.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextDisplayRecipient.setText(Const.EMPTY);
            }
        });

        final String hintSending = String.format(getString(R.string.msg_input_limit_length), 10);
        mTextDisplaySending.setHint(hintSending);
        RelativeLayout layoutDisplaySending = (RelativeLayout) findViewById(R.id.layout_display_sending);
        layoutDisplaySending.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditAccountNum.isFocused()) {
                    mEditAccountNum.clearFocus();
                    Utils.hideKeyboard(TransferAccountActivity.this, mEditAccountNum);
                }

                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferAccountActivity.this,
                        getString(R.string.display_transfer_recipient), hintSending, 10, mTextDisplaySending.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextDisplaySending.setText(inputString);
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        final RelativeLayout btnEraseSending = (RelativeLayout) findViewById(R.id.imageview_erase_display_sending);
        mTextDisplaySending.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseSending.setVisibility(View.GONE);
                } else {
                    btnEraseSending.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseSending.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextDisplaySending.setText(Const.EMPTY);
            }
        });

        mLayoutOpenCloseInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBtnOpenCloseInput.setChecked(!mBtnOpenCloseInput.isChecked());
            }
        });

        mBtnOpenCloseInput.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mLayoutAddInpuntItem.setVisibility(View.VISIBLE);
                    mLayoutSendDate.setVisibility(View.VISIBLE);
                    mLayoutHelp.setVisibility(View.GONE);
                    mBtnOpenCloseInput.setBackgroundResource(R.drawable.btn_acco_u);
                } else {
                    mLayoutAddInpuntItem.setVisibility(View.GONE);
                    mLayoutSendDate.setVisibility(View.VISIBLE);
                    mLayoutHelp.setVisibility(View.GONE);
                    mBtnOpenCloseInput.setBackgroundResource(R.drawable.btn_acco_d);
                }
            }
        });

        final RelativeLayout btnEraseMemo = (RelativeLayout) findViewById(R.id.imageview_erase_memo);
        final String hintMemo = String.format(getString(R.string.msg_input_limit_length), 20);
        mTextInputMemo.setHint(hintMemo);
        RelativeLayout layoutInputMemo = (RelativeLayout) findViewById(R.id.layout_input_memo);
        layoutInputMemo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferAccountActivity.this,
                        getString(R.string.memo), hintMemo, 20, mTextInputMemo.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextInputMemo.setText(inputString);

                                if (mEditAccountNum.isFocused())
                                    mEditAccountNum.clearFocus();
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        mTextInputMemo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseMemo.setVisibility(View.GONE);
                } else {
                    btnEraseMemo.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseMemo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextInputMemo.setText(Const.EMPTY);
            }
        });

        RelativeLayout layoutDisplaySendDate = (RelativeLayout) findViewById(R.id.layout_display_send_date);
        layoutDisplaySendDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLastTransferInfo == null)
                    return;

                mIsDelayService = false;
                // 수취인 조회 결과가 서비스구분코드 '지연3시간이후'로 전달될 시는 시간이 3시간 이후부터 설정 가능
                if (mDlyTrnfSvcEntrYn) {
                    String SVC_DVCD = mLastTransferInfo.getSVC_DVCD();
                    if ("2".equalsIgnoreCase(SVC_DVCD))
                        mIsDelayService = true;
                }
                showDatePicker(mIsDelayService);
            }
        });

        mBtnDropdownSendDate = (RelativeLayout) findViewById(R.id.imageview_dropdown_send_date);
        mBtnEraseSendDate = (RelativeLayout) findViewById(R.id.imageview_erase_send_date);
        mBtnEraseSendDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLastTransferInfo != null) {
                    String TRNF_DVCD = mLastTransferInfo.getTRNF_DVCD();
                    if ("2".equalsIgnoreCase(TRNF_DVCD)) {
                        mTextInputDate.setText(getString(R.string.msg_after_3_hour));
                    } else
                        mTextInputDate.setText(getString(R.string.Immediately));
                } else {
                    mTextInputDate.setText(getString(R.string.Immediately));
                }

                mInputYear = 0;
                mInputMonth = 0;
                mInputDay = 0;
                mInputHour = 0;

                mLastTransferInfo.setTRNF_DVCD("1");
                mLastTransferInfo.setTRNF_DMND_DT(Const.EMPTY);
                mLastTransferInfo.setTRNF_DMND_TM(Const.EMPTY);

                mBtnDropdownSendDate.setVisibility(View.VISIBLE);
                mBtnEraseSendDate.setVisibility(View.GONE);
            }
        });

        mLayoutSendDate = (LinearLayout) findViewById(R.id.layout_send_date);
        mLayoutHelp = (LinearLayout) findViewById(R.id.layout_help_send_date);
        RelativeLayout btnHelpOn = (RelativeLayout) findViewById(R.id.imageview_hint_on_send_date);
        btnHelpOn.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutSendDate.setVisibility(View.GONE);
                mLayoutHelp.setVisibility(View.VISIBLE);
            }
        });

        RelativeLayout btnHelpOff = (RelativeLayout) findViewById(R.id.imageview_hint_off_send_date);
        btnHelpOff.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutSendDate.setVisibility(View.VISIBLE);
                mLayoutHelp.setVisibility(View.GONE);
            }
        });

        RelativeLayout btnCloseHelp = (RelativeLayout) findViewById(R.id.imageview_close_help);
        btnCloseHelp.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutSendDate.setVisibility(View.VISIBLE);
                mLayoutHelp.setVisibility(View.GONE);
            }
        });
    }

    /**
     * 보낼시간과 메모 입력화면 표시
     */
    private void showStep03() {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_03;
        mLayoutStep03.setVisibility(View.INVISIBLE);

        if (DataUtil.isNotNull(mLastTransferInfo)) {

            if (DataUtil.isNotNull(mLastTransferInfo.getRECV_NM()))
                mTextDisplayRecipient.setText(mLastTransferInfo.getRECV_NM());

            if (DataUtil.isNotNull(mLastTransferInfo.getCUST_NM()))
                mTextDisplaySending.setText(mLastTransferInfo.getCUST_NM());

            String TRNF_DVCD = mLastTransferInfo.getTRNF_DVCD();
            if ("1".equalsIgnoreCase(TRNF_DVCD)) {
                mLayoutDelayDesc.setVisibility(mDlyTrnfSvcEntrYn ? View.VISIBLE : View.GONE);
                mTextInputDate.setText(getString(R.string.Immediately));
            } else if ("2".equalsIgnoreCase(TRNF_DVCD)) {
                mLayoutDelayDesc.setVisibility(View.VISIBLE);
                mTextInputDate.setText(getString(R.string.msg_after_3_hour));
            }
        } else {
            if (mDlyTrnfSvcEntrYn) {
                mLayoutDelayDesc.setVisibility(View.VISIBLE);
                mTextInputDate.setText(getString(R.string.msg_after_3_hour));
            } else {
                mLayoutDelayDesc.setVisibility(View.GONE);
                mTextInputDate.setText(getString(R.string.Immediately));
            }
        }
        interActionShowStep03();
    }

    /**
     * 보낼시간과 메모 입력화면 interaction
     */
    private void interActionShowStep03() {
        MLog.d();
        Animation animSlideIn = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_in_transfer_verify);
        mLayoutStep03.clearAnimation();
        mLayoutStep03.startAnimation(animSlideIn);
        mLayoutStep03.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep03.setVisibility(View.VISIBLE);
                mBtnConfirm.setVisibility(View.GONE);
                mBtnSendTransfer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 보낼시간과 메모 입력화면 숨기기
     */
    private void hideStep03(boolean isClear) {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;
        mLayoutStep03.setVisibility(View.GONE);
        mLayoutHelp.setVisibility(View.GONE);
        mLayoutSendDate.setVisibility(View.GONE);
        mLayoutAddInpuntItem.setVisibility(View.GONE);
        mBtnSendTransfer.setVisibility(View.GONE);
        mBtnOpenCloseInput.setSelected(false);
        mBtnOpenCloseInput.setBackgroundResource(R.drawable.btn_acco_d);
        mScrollviewStep03.scrollTo(0, 0);
        // 보낼시간과 메모 입력값 삭제
        if (isClear) {
            mTextDisplayRecipient.setText(Const.EMPTY);
            mTextDisplaySending.setText(Const.EMPTY);
            mTextInputDate.setText(mDlyTrnfSvcEntrYn ? getString(R.string.msg_after_3_hour) : getString(R.string.Immediately));
            mInputYear = 0;
            mInputMonth = 0;
            mInputDay = 0;
            mInputHour = 0;
        }
        mBtnDropdownSendDate.setVisibility(View.VISIBLE);
        mBtnEraseSendDate.setVisibility(View.GONE);
    }

    /**
     * 수취인조회 결과 확인 다이얼로그를 띄운다.
     */
    private void showVerifyTransferAccount() {
        int sendTransferNum = TransferManager.getInstance().size();
        if (sendTransferNum == 1) {
            showConfirmSingleFransfer();
        } else if (sendTransferNum > 1) {
            showConfirmMultiTransfer();
        }
    }

    /**
     * 수취인 조회결과(단건이체) 확인 다이얼로그를  띄운다.
     */
    private void showConfirmSingleFransfer() {
        MLog.d();

        if (isFinishing() || DataUtil.isNull(mListMyAccount) || mListMyAccount.size() < mSendAccountIndex || DataUtil.isNull(mListMyAccount.get(mSendAccountIndex)))
            return;

        // 단건이체 확인 다이얼로그가 띄어져 있을경우 닫는다.
        hideSingleTransferDialog();

        MyAccountInfo accountInfo = mListMyAccount.get(mSendAccountIndex);
        String withdrawAccount = DataUtil.isNotNull(accountInfo.getACNO()) ? accountInfo.getACNO() : Const.EMPTY;
        String alias = DataUtil.isNotNull(accountInfo.getACCO_ALS()) ? accountInfo.getACCO_ALS() : Const.EMPTY;

        singleTransferDialog = new SlidingConfirmSingleTransferDialog(
                TransferAccountActivity.this,
                withdrawAccount,
                alias,
                new SlidingConfirmSingleTransferDialog.FinishListener() {
                    @Override
                    public void OnOKListener() {
                        hideAllConfirmTransferDialog();
                        requestVoicePhising();
                    }

                    @Override
                    public void OnAddTransfer() {
                        clearAll();
                        updateSendMultiTransfer(true);

                        mBtnBalanceDropdown.setVisibility(View.GONE);
                        mBtnLockDropdown.setVisibility(View.VISIBLE);
                        mBtnWithdrawAccount.setClickable(false);

                        hideStep03(true);
                        hideStep02(true);
                        showStep01(true);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 50);
                    }
                }
        );
        singleTransferDialog.setCancelable(false);
        singleTransferDialog.show();
    }

    /**
     * 수취인 조회결과(다건이체) 확인 다이얼로그를 띄운다.
     */
    private void showConfirmMultiTransfer() {
        MLog.d();

        if (isFinishing() || DataUtil.isNull(mTextAccountName.getText().toString()) || DataUtil.isNull(mTextAccount.getText().toString()))
            return;

        hideMultiTransferDialog();

        multiTransferDialog = new SlidingConfirmMultiTransferDialog(
                TransferAccountActivity.this,
                mTextAccountName.getText().toString() + " " + mTextAccount.getText().toString(),
                new SlidingConfirmMultiTransferDialog.FinishListener() {
                    @Override
                    public void OnCancelListener(boolean isLastRemove, Double dLimitOneDay, boolean isAllRemove) {
                        MLog.d();
                        hideAllConfirmTransferDialog();
                        // 1건이상 삭제이력이 있을경우 개수를 초기화
                        if (isAllRemove) {
                            mTransferCnt = 1;
                        }
                        if (!TransferManager.getInstance().getIsNotRemove() && !isLastRemove) {
                            requestRemoveTransfer(true);
                        }
//                        else if (TransferManager.getInstance().getIsNotRemove()) {
//                            updateSendMultiTransfer(false);
//                        }
                        TransferManager.getInstance().setIsNotRemove(false);
                        if (dLimitOneDay >= 0) {
                            mTransferOneDay = dLimitOneDay;
                            mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                        }
                    }

                    @Override
                    public void OnOKListener(final boolean isLastRemove) {
                        TransferManager.getInstance().setIsNotRemove(isLastRemove);
                        hideAllConfirmTransferDialog();
                        requestVoicePhising();
                    }

                    @Override
                    public void OnRemoveItemListener() {
                        // 이체아이템 삭제
                        if (mTransferCnt > 0) {
                            mTransferCnt--;
                            MLog.i("mTransferCnt >> " + mTransferCnt);
                        }
                    }

                    @Override
                    public void OnRemoveAllListener() {
                        TransferManager.getInstance().clear();
                        hideStep03(true);
                        hideStep02(true);
                        showStep01(true);
                        mViewEditAmountLine.setVisibility(View.GONE);
                        showProgressLine();
                        mBtnWithdrawAccount.setClickable(true);
                        mBtnBalanceDropdown.setVisibility(View.VISIBLE);
                        mBtnLockDropdown.setVisibility(View.GONE);
                    }

                    @Override
                    public void OnAddTransfer() {
                        clearAll();
                        updateSendMultiTransfer(true);

                        mBtnBalanceDropdown.setVisibility(View.GONE);
                        mBtnLockDropdown.setVisibility(View.VISIBLE);
                        mBtnWithdrawAccount.setClickable(false);

                        hideMultiTransferDialog();

                        hideStep03(true);
                        hideStep02(true);
                        showStep01(true);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 50);
                    }

                    @Override
                    public void OnChangeLimit(double dLimitOneDay) {
                        mTransferOneDay = dLimitOneDay;
                        mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                    }

                });
        multiTransferDialog.setCancelable(false);
        multiTransferDialog.show();
    }

    /**
     * 수취인 조회결과(단건이체) 확인 다이얼로그를 닫는다.
     */
    private void hideSingleTransferDialog() {
        if (DataUtil.isNotNull(singleTransferDialog) && singleTransferDialog.isShowing()) {
            singleTransferDialog.dismiss();
            singleTransferDialog = null;
        }
    }

    /**
     * 수취인 조회결과(다건이체) 확인 다이얼로그를 닫는다.
     */
    private void hideMultiTransferDialog() {
        if (DataUtil.isNotNull(multiTransferDialog) && multiTransferDialog.isShowing()) {
            multiTransferDialog.dismiss();
            multiTransferDialog = null;
        }
    }

    /**
     * 수취인 조회결과 확인 다이얼로그를 모두 닫는다.
     */
    private void hideAllConfirmTransferDialog() {
        hideSingleTransferDialog();
        hideMultiTransferDialog();
    }

    /**
     * 수취인 조회결과(다건이체) 업데이트
     */
    private void updateSendMultiTransfer(boolean isShow) {

        if (DataUtil.isNull(TransferManager.getInstance()))
            return;

        int sendNum = TransferManager.getInstance().size();
        if (sendNum <= 0) {
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                mLayoutTotalTransfer.setVisibility(View.INVISIBLE);
            } else if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                mLayoutTotalTransfer.setVisibility(View.GONE);
            }
            return;
        }

        double totalAmount = 0;
        for (int index = 0; index < sendNum; index++) {
            TransferVerifyInfo transerInfo = TransferManager.getInstance().get(index);
            String amount = transerInfo.getTRN_AMT();
            if (!TextUtils.isEmpty(amount)) {
                double dAmount = Double.valueOf(amount);
                totalAmount += dAmount;
            }
        }

        String totalSendNum = String.format(getString(R.string.msg_total_send_num), sendNum);
        mTextSendNum.setText(totalSendNum);
        String totalSendAmount = String.format(getString(R.string.msg_total_send_amount), Utils.moneyFormatToWon(totalAmount));
        mTextSendAmount.setText(totalSendAmount);
        if (isShow) {
            mLayoutTotalTransfer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * 수취인 조회결과 화면을 보여준다.
     *
     * @param bankStockCode   이체계좌 은행코드
     * @param transferAccount 이체계좌
     * @param transferAmount  이체금액
     * @param object          수취인 조회 결과값 (JSON object)
     */
    private void showDlgVerifyTransferAccount(final String bankStockCode, final String transferAccount, final String transferAmount, final JSONObject object) {
        MLog.d();

        if (DataUtil.isNull(object))
            return;

        if (mEditAccountNum.hasFocus())
            mEditAccountNum.clearFocus();

        AlertRemitteeDialog alertDialog = new AlertRemitteeDialog(TransferAccountActivity.this);
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TransferVerifyInfo verifyTransferInfo = new TransferVerifyInfo();
                verifyTransferInfo.setCNTP_FIN_INST_CD(bankStockCode);
                verifyTransferInfo.setCNTP_BANK_ACNO(transferAccount);
                verifyTransferInfo.setTRN_AMT(transferAmount);
                verifyTransferInfo.setRECV_NM(object.optString("RECV_NM"));
                verifyTransferInfo.setFEE(object.optString("FEE"));
                verifyTransferInfo.setCUST_NM(object.optString("CUST_NM"));
                verifyTransferInfo.setSVC_DVCD(object.optString("SVC_DVCD"));
                verifyTransferInfo.setTRN_UNQ_NO(object.optString("TRN_UNQ_NO"));

                if (!mDlyTrnfSvcEntrYn && !mDsgtMnrcAccoSvcEntrYn) {
                    if (mInputYear == 0 || mInputMonth == 0 || mInputDay == 0 || mInputHour == 0) {
                        verifyTransferInfo.setTRNF_DVCD("1");
                        verifyTransferInfo.setTRNF_DMND_DT(Const.EMPTY);
                        verifyTransferInfo.setTRNF_DMND_TM(Const.EMPTY);
                    } else {
                        verifyTransferInfo.setTRNF_DVCD("3");
                        verifyTransferInfo.setTRNF_DMND_DT(String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay));
                        verifyTransferInfo.setTRNF_DMND_TM(String.format("%02d0000", mInputHour));
                    }
                } else if (mDlyTrnfSvcEntrYn) {
                    if ("1".equalsIgnoreCase(object.optString("SVC_DVCD"))) {            // 즉시
                        mTextInputDate.setText(getString(R.string.Immediately));
                        verifyTransferInfo.setTRNF_DVCD("1");
                    } else if ("2".equalsIgnoreCase(object.optString("SVC_DVCD"))) {    // 지연3시간이후
                        verifyTransferInfo.setTRNF_DVCD("2");
                        mTextInputDate.setText(getString(R.string.msg_after_3_hour));
                    }
                } else if (mDsgtMnrcAccoSvcEntrYn) {
                    verifyTransferInfo.setTRNF_DVCD("5");
                }

                if (DataUtil.isNotNull(mLastTransferInfo)) {
                    mLastTransferInfo = null;
                }
                mLastTransferInfo = verifyTransferInfo;

                // 이체아이템 추가
                mTransferCnt++;
                MLog.i("mTransferCnt >> " + mTransferCnt);

                if (mLayoutTransferRecentlyList.isShown()) {
                    hideTransferRecentlyListAfter();
                } else {
                    showStep03();
                }
            }
        };
        alertDialog.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MLog.d();
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                    if (DataUtil.isNull(mTextBankName.getText().toString())
                            || DataUtil.isNull(mEditAccountNum.getText().toString())) {
                        mBtnConfirm.setEnabled(false);
                    }
                    mBtnConfirm.setVisibility(View.VISIBLE);
                    mBtnSendTransfer.setVisibility(View.GONE);
                    if (!mLayoutTransferRecentlyList.isShown()) {
                        showTransferRecentlyList();
                    }
                }
            }
        };
        alertDialog.msg = String.format(getString(R.string.msg_confirm_received_account_01), object.optString("RECV_NM"));
        alertDialog.show();
    }

    /**
     * 보이스피싱 체크 일림 팝업을 띄어준다.
     */
    private void showAdvanceVoicePhishing() {
        if (isFinishing())
            return;
        SlidingVoicePhishingDialog dialog = new SlidingVoicePhishingDialog(
                TransferAccountActivity.this,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBtnConfirm.setVisibility(View.GONE);
                        mBtnSendTransfer.setVisibility(View.VISIBLE);
                        // 이체정보 전자 서명값 요청
                        transferSignData();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        hideAllConfirmTransferDialog();
                        showVoicePhishing();
                    }
                }
        );
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * 보이스피싱 문진 팝업을 띄어준다.
     */
    private void showVoicePhishing() {
        if (isFinishing())
            return;
        final SlidingVoicePhishingPreventAskDialog mVoicePhishingPreventAskDialog = new SlidingVoicePhishingPreventAskDialog(TransferAccountActivity.this);
        mVoicePhishingPreventAskDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mVoicePhishingPreventAskDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        mVoicePhishingPreventAskDialog.setCancelable(false);
        mVoicePhishingPreventAskDialog.setOnItemListener(new SlidingVoicePhishingPreventAskDialog.OnItemListener() {
            @Override
            public void onConfirm(boolean result) {
                // 문진 응답을 모두 "아니요"로 체크한 경우
                if (result) {
                    // 이체 초기화면으로 설정
                    clearAll();
                    TransferManager.getInstance().clear();
                    mListTransferReceiptInfo.clear();
                    mLastTransferInfo = null;
                    if (DataUtil.isNotNull(mMyAccountAdapter)) {
                        MyAccountInfo accountInfo = mListMyAccount.get(0);
                        String ACNO = accountInfo.getACNO();
                        requestInit(ACNO, 0, true);
                    }
                }
                // 문진 응답 중 하나라고 "예"라고 체크한 경우 팝업을 띄어준다.
                else {
                    AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                    alertDialog.mPListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // 이체 초기화면으로 설정
                            clearAll();
                            TransferManager.getInstance().clear();
                            mListTransferReceiptInfo.clear();
                            mLastTransferInfo = null;
                            if (DataUtil.isNotNull(mMyAccountAdapter)) {
                                MyAccountInfo accountInfo = mListMyAccount.get(0);
                                String ACNO = accountInfo.getACNO();
                                requestInit(ACNO, 0, true);
                            }
                        }
                    };
                    alertDialog.msg = getString(R.string.msg_voice_phishing_05);
                    alertDialog.show();
                }
                mVoicePhishingPreventAskDialog.dismiss();
            }
        });
        mVoicePhishingPreventAskDialog.show();
    }

    /**
     * mOTP, 타행 OTP, 핀코드 입력창 표시
     *
     * @param signData 전자서명된 이체정보
     */
    private void showOTPActivity(String signData) {
        MLog.d();
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (DataUtil.isNull(SECU_MEDI_DVCD)) {
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setSignData(signData);
            commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
            Intent intent = new Intent(TransferAccountActivity.this, PincodeAuthActivity.class);
            intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
            intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
            intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        } else {
            Intent intent;
            if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                intent = new Intent(TransferAccountActivity.this, OtherOtpAuthActivity.class);
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = 0;
                    for (int index = 0; index < TransferManager.getInstance().size(); index++) {
                        TransferVerifyInfo transerInfo = TransferManager.getInstance().get(index);
                        String amount = transerInfo.getTRN_AMT();
                        if (!TextUtils.isEmpty(amount)) {
                            double dAmount = Double.valueOf(amount);
                            totalAmount += dAmount;
                        }
                    }

                    if (totalAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(TransferAccountActivity.this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //TODO:sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = 0;
                    for (int index = 0; index < TransferManager.getInstance().size(); index++) {
                        TransferVerifyInfo transerInfo = TransferManager.getInstance().get(index);
                        String amount = transerInfo.getTRN_AMT();
                        if (!TextUtils.isEmpty(amount)) {
                            double dAmount = Double.valueOf(amount);
                            totalAmount += dAmount;
                        }
                    }
                    if (totalAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(TransferAccountActivity.this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //TODO:sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }

                intent = new Intent(TransferAccountActivity.this, OtpPwAuthActivity.class);
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP);
                intent.putExtra(Const.INTENT_OTP_SERIAL_NUMBER, mMOTPSerialNumber);
                intent.putExtra(Const.INTENT_OTP_TA_VERSION, mMOTPTAVersion);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else {
                intent = new Intent(TransferAccountActivity.this, PincodeAuthActivity.class);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                //TODO:sign data, service id 추가
                commonUserInfo.setSignData(signData);
                intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
                overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                return;
            }
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQUEST_OTP_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 이체실패에 따른 메세지 화면 표시
     *
     * @param type 오류 타입
     * @param ret 오류 메시지
     */
    private void showFailTransfer(int type, String ret) {
        MLog.d();
        Intent intent = new Intent(TransferAccountActivity.this, ITransferFailActivity.class);
        intent.putExtra(Const.INTENT_TRANSFER_ENTRY_TYPE, 0);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_TYPE, type);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_RET, ret);
        startActivity(intent);
        overridePendingTransition(0, 0);
        finish();
    }
// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    /**
     * 나의계좌 중 대표 및 진입 화면 선택 계좌 정보 표시
     */
    @SuppressLint("SetTextI18n")
    private void initMyAccount() {
        MLog.d();

        // 로그인 세션정보의 계좌정보를 가져온다.
        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mListMyAccount = userInfo.getMyAccountInfoArrayList();
        if (DataUtil.isNull(mListMyAccount) || mListMyAccount.isEmpty())
            return;

        MyAccountInfo accountInfo = null;

        // 인텐트로 넘어온 계좌정보가 없을경우
        if (DataUtil.isNull(mIntentMyAccount)) {
            accountInfo = mListMyAccount.get(0);
        } else {
//            MLog.i("계좌 개수 >> " + mListMyAccount.size());
            // 로그인 세션정보의 계좌번호와 넘어온 계좌번호와 일치할경우 계좌정보를 세팅한다.
            for (MyAccountInfo account : mListMyAccount) {
                if (account == null)
                    continue;
//                MLog.line();
//                MLog.i("계좌번호 (ACNO) >> " + account.getACNO());
//                MLog.i("계좌 별 (ACCO_ALS) >> " + account.getACCO_ALS());
//                MLog.i("사고신고 여부 (ACDT_DCL_CD) >> " + account.getACDT_DCL_CD()); // 1:거래정지, 2:지급정지, 3:출금정지

                if (mIntentMyAccount.equalsIgnoreCase(account.getACNO())) {
                    accountInfo = account;
                    break;
                }
            }
        }

        if (DataUtil.isNull(accountInfo)) {
            accountInfo = mListMyAccount.get(0);
        }

//        MLog.i("getACDT_DCL_CD >> " + accountInfo.getACDT_DCL_CD());

        String accoAls = accountInfo.getACCO_ALS();
        if (TextUtils.isEmpty(accoAls)) {
            accoAls = accountInfo.getPROD_NM();
        }
        mTextAccountName.setText(accoAls);

        String acno = accountInfo.getACNO();
        String partACNO = "";
        if (!TextUtils.isEmpty(acno)) {
            int lenACNO = acno.length();
            if (lenACNO > 4)
                partACNO = acno.substring(lenACNO - 4, lenACNO);
        }
        mTextAccount.setText(" [" + partACNO + "]");

        String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
        if (DataUtil.isNotNull(WTCH_POSB_AMT)) {
            mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
            String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
            mTextAccountBalance.setText(amount + getString(R.string.won));
        }

        //커플계좌여부 추가
        String SHRN_ACCO_YN = accountInfo.getSHRN_ACCO_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SHRN_ACCO_YN)) {
            mImageShare.setVisibility(View.VISIBLE);
        }else{
            mImageShare.setVisibility(View.GONE);
        }
    }

    /**
     * 전체 계좌 중 대표 및 진입 시 계좌 표시
     */
    private void showMyAccount(String accountNum) {
        MLog.d();
        int size = mListMyAccount.size();
        boolean isExistRPRS_ACCO_YN = false;

        if (DataUtil.isNull(accountNum)) {
            for (int index = 0; index < size; index++) {

                MyAccountInfo accountInfo = mListMyAccount.get(index);
                if (DataUtil.isNull(accountInfo))
                    continue;

                String rprsAccoYn = accountInfo.getRPRS_ACCO_YN();
                if (!TextUtils.isEmpty(rprsAccoYn) && Const.REQUEST_WAS_YES.equalsIgnoreCase(rprsAccoYn)) {

                    String accoAls = accountInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(accoAls)) {
                        accoAls = accountInfo.getPROD_NM();
                    }
                    mTextAccountName.setText(accoAls);

                    String acno = accountInfo.getACNO();
                    String partACNO = "";
                    if (DataUtil.isNotNull(acno)) {
                        int lenACNO = acno.length();
                        if (lenACNO > 4)
                            partACNO = acno.substring(lenACNO - 4, lenACNO);
                    }
                    mTextAccount.setText(" [" + partACNO + "]");

                    String wtchPosbAmt = accountInfo.getWTCH_POSB_AMT();
                    if (DataUtil.isNotNull(wtchPosbAmt)) {
                        mBalanceAmount = Double.valueOf(wtchPosbAmt);
                        String amount = Utils.moneyFormatToWon(Double.valueOf(wtchPosbAmt));
                        mTextAccountBalance.setText(amount + getString(R.string.won));
                    }

                    requestInit(acno, index, false);
                    isExistRPRS_ACCO_YN = true;
                    break;
                }
            }

            if (!isExistRPRS_ACCO_YN) {

                MyAccountInfo accountInfo = mListMyAccount.get(0);

                if (DataUtil.isNull(accountInfo))
                    return;

                String accoAls = accountInfo.getACCO_ALS();
                if (DataUtil.isNull(accoAls)) {
                    accoAls = accountInfo.getPROD_NM();
                }
                mTextAccountName.setText(accoAls);

                String acno = accountInfo.getACNO();
                String partACNO = "";
                if (!TextUtils.isEmpty(acno)) {
                    int lenACNO = acno.length();
                    if (lenACNO > 4)
                        partACNO = acno.substring(lenACNO - 4, lenACNO);
                }
                mTextAccount.setText(" [" + partACNO + "]");

                String wtchPosbAmt = accountInfo.getWTCH_POSB_AMT();
                if (DataUtil.isNotNull(wtchPosbAmt)) {
                    mBalanceAmount = Double.valueOf(wtchPosbAmt);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(wtchPosbAmt));
                    mTextAccountBalance.setText(amount + getString(R.string.won));
                }
                requestInit(acno, 0, false);
            }
        } else {
            for (int index = 0; index < size; index++) {

                MyAccountInfo accountInfo = mListMyAccount.get(index);

                if (DataUtil.isNull(accountInfo))
                    continue;

                String acno = accountInfo.getACNO();
                if (accountNum.equalsIgnoreCase(acno)) {
                    String accoAls = accountInfo.getACCO_ALS();
                    if (DataUtil.isNull(accoAls)) {
                        accoAls = accountInfo.getPROD_NM();
                    }
                    mTextAccountName.setText(accoAls);

                    String partACNO = "";
                    if (DataUtil.isNotNull(acno)) {
                        int lenACNO = acno.length();
                        if (lenACNO > 4)
                            partACNO = acno.substring(lenACNO - 4, lenACNO);
                    }
                    mTextAccount.setText(" [" + partACNO + "]");

                    String wtchPosbAmt = accountInfo.getWTCH_POSB_AMT();
                    if (DataUtil.isNotNull(wtchPosbAmt)) {
                        mBalanceAmount = Double.valueOf(wtchPosbAmt);
                        String amount = Utils.moneyFormatToWon(Double.valueOf(wtchPosbAmt));
                        mTextAccountBalance.setText(amount + getString(R.string.won));
                    }
                    requestInit(acno, index, false);
                    break;
                }

            }
        }
    }

    /**
     * 전체 계좌 리스트 생성
     */
    private void checkMyAccount() {

        MLog.d();

        if (DataUtil.isNotNull(mListMyAccount) && !mListMyAccount.isEmpty()) {
            mListMyAccount.clear();
        }

        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mListMyAccount = userInfo.getMyAccountInfoArrayList();

        if (DataUtil.isNull(mListMyAccount) || mListMyAccount.isEmpty()) {
            dismissProgressDialog();
            interActionStep01();
            return;
        }

        if (DataUtil.isNotNull(mMyAccountAdapter)) {
            mMyAccountAdapter = null;
        }

        mMyAccountAdapter = new MyAccountAdapter(TransferAccountActivity.this, mListMyAccount);
        mListViewMyAccount.setAdapter(mMyAccountAdapter);

        if (mListMyAccount.size() >= 6) {
            int height = 0;
            for (int i = 0; i < 6; i++) {
                View childView = mMyAccountAdapter.getView(i, null, mListViewMyAccount);
                childView.measure(
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
                );
                height += childView.getMeasuredHeight();
            }
            height += mListViewMyAccount.getDividerHeight() * 6;
            ViewGroup.LayoutParams params = mListViewMyAccount.getLayoutParams();
            params.height = height;
            mListViewMyAccount.setLayoutParams(params);
        }

        showMyAccount(DataUtil.isNotNull(mIntentMyAccount) ? mIntentMyAccount : null);
    }

    /**
     * 최근/자주이체 계좌 목록을 보여준다.
     */
    private void showTransferRecentlyList() {
        MLog.d();

        if (isFinishing())
            return;

        if (DataUtil.isNotNull(mITransferRecentlyAdapter)) {
            mITransferRecentlyAdapter = null;
        }

        mITransferRecentlyAdapter = new ITransferRecentlyAdapter(TransferAccountActivity.this, mListTransferRecently, new ITransferRecentlyAdapter.OnFavoriteItemClickListener() {
            @Override
            public void onItemClick(int position) {
                requestFavoriteAccount(position);
            }
        });
        mListViewTransferRecently.setAdapter(mITransferRecentlyAdapter);

        if (mITransferRecentlyAdapter.getCount() == 0) {
            mLayoutNoTransferRecently.setVisibility(View.VISIBLE);
            mListViewTransferRecently.setVisibility(View.GONE);
        } else {
            mLayoutNoTransferRecently.setVisibility(View.GONE);
            mListViewTransferRecently.setVisibility(View.VISIBLE);
        }

        mLayoutTransferRecentlyList.setVisibility(View.INVISIBLE);
        Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_in_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                String bankName = mTextBankName.getText().toString();
                String accountNum = mEditAccountNum.getText().toString();
                if (!TextUtils.isEmpty(bankName) || !TextUtils.isEmpty(accountNum)) {
                    mBtnConfirm.setVisibility(View.VISIBLE);
                }

                mLayoutTransferRecentlyList.setVisibility(View.VISIBLE);

                // iOS 무조건 금액 변경 후 수취인 조회한다고 하여 sync 맞춤
                /*String recipient = mTextDisplayRecipient.getText().toString();
                String sending = mTextDisplaySending.getText().toString();
                String memo = mTextInputMemo.getText().toString();
                if (!TextUtils.isEmpty(recipient) || !TextUtils.isEmpty(sending) ||
                    !TextUtils.isEmpty(memo) ||
                    (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0)) {
                    showStep03();
                }*/

                if (mViewStep02Bar.getVisibility() == View.INVISIBLE) {
                    showStep02Bar();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최근/자주이체 계좌 목록 감추기
     */
    private void hideTransferRecentlyListAfter() {
        MLog.d();

        if (isFinishing())
            return;

        Animation animation = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_out_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTransferRecentlyList.setVisibility(View.GONE);
                showStep03();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }

    /**
     * 최근/자주이체 계좌 목록 업데이트
     *
     * @param dataList 업데이트할 리스트
     */
    private void updateTransferRecentlyList(ArrayList<ITransferRecentlyInfo> dataList) {
        if (DataUtil.isNotNull(mITransferRecentlyAdapter)) {
            mITransferRecentlyAdapter.setRecentlyArrayList(dataList);
        }
    }

    /**
     * 이체 가능금액 존재 여부 체크
     *
     * @return boolean
     */
    private boolean checkUnderAmount() {
        String inputAmount = mEditAmount.getText().toString();
        if (DataUtil.isNull(inputAmount) || mBalanceAmount <= 0) {
            mBtnConfirm.setEnabled(false);
            return true;
        }
        return false;
    }

    /**
     * 잔액이체 가능금액 체크
     *
     * @return boolean
     */
    private boolean checkOverAmount() {
        String inputAmount = mEditAmount.getText().toString();
        inputAmount = inputAmount.replaceAll("[^0-9]", Const.EMPTY);
        double dInputAmount = Double.valueOf(inputAmount);
        return dInputAmount > mBalanceAmount;
    }

    /**
     * 1회 이체 가능금액 체크
     *
     * @return boolean
     */
    private boolean checkOverOneTime() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount)) {
            mBtnConfirm.setEnabled(false);
            return false;
        }
        inputAmount = inputAmount.replaceAll("[^0-9]", Const.EMPTY);
        double dInputAmount = Double.valueOf(inputAmount);
        return dInputAmount > mTransferOneTime;
    }

    /**
     * 1일 이체 가능금액 체크
     *
     * @return boolean
     */
    private boolean checkOverOneDay() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount)) {
            mBtnConfirm.setEnabled(false);
            return false;
        }
        inputAmount = inputAmount.replaceAll("[^0-9]", Const.EMPTY);
        double dInputAmount = Double.valueOf(inputAmount);
        return dInputAmount > mTransferOneDay;
    }

    /**
     * 다건이체시 잔액보다 많이 이체 여부
     *
     * @return boolean
     */
    private boolean checkOverTotalAmount() {
        if (!mLayoutTotalTransfer.isShown())
            return false;

        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount))
            return true;

        String sendAmount = mTextSendAmount.getText().toString();
        inputAmount = inputAmount.replaceAll("[^0-9]", Const.EMPTY);
        sendAmount = sendAmount.replaceAll("[^0-9]", Const.EMPTY);
        if (!TextUtils.isEmpty(sendAmount)) {
            double dSendAmount = Double.valueOf(sendAmount);
            double dInputAmount = Double.valueOf(inputAmount);
            dInputAmount = dInputAmount + dSendAmount;
            return dInputAmount > mBalanceAmount;
        }
        return false;
    }

    /**
     * 이체금액 유효성 체크
     *
     * @return boolean
     */
    private boolean checkVaildAmount() {
        if (checkUnderAmount()) {
            mTextErrMsg.setText(getString(R.string.msg_err_trnasfer_over_balance));
            mTextErrMsg.setVisibility(View.VISIBLE);
            mBtnConfirm.setEnabled(false);
            return false;
        }
        if (checkOverAmount() || checkOverTotalAmount()) {
            mTextErrMsg.setText(getString(R.string.msg_err_trnasfer_over_balance));
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverOneTime()) {
            mTextErrMsg.setText(getString(R.string.msg_err_trnasfer_over_one_time));
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverOneDay()) {
            mTextErrMsg.setText(getString(R.string.msg_err_trnasfer_over_one_day));
            mTextErrMsg.setVisibility(View.VISIBLE);
            return false;
        } else {
            mBtnConfirm.setEnabled(true);
            mTextHangulAmount.setVisibility(View.VISIBLE);
            mTextErrMsg.setVisibility(View.GONE);
        }
        return true;
    }

    private boolean validateTransferAccountInfo(int position) {
        return DataUtil.isNull(mListTransferRecently) || mListTransferRecently.isEmpty();
    }

    /**
     * 모든 입력값 초기화
     */
    private void clearAll() {
        mEditAmount.setText(Const.EMPTY);
        mTextBankName.setText(Const.EMPTY);
        mEditAccountNum.setText(Const.EMPTY);
        mTextDisplayRecipient.setText(Const.EMPTY);
        mTextDisplaySending.setText(Const.EMPTY);
        mTextInputMemo.setText(Const.EMPTY);
        mInputAmount = Const.EMPTY;
    }
// ==============================================================================================================
// ANIMATION
// ==============================================================================================================
    /**
     * 취소 메세지 출력
     */
    private void showCancelMessage() {
        MLog.d();
        if (isFinishing()) return;
        AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
        if (TransferManager.getInstance().size() > 1) {
            if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                alertDialog.msg = getString(R.string.msg_cancel_multi_trnasfer_account);
            } else {
                alertDialog.msg = getString(R.string.msg_cancel_transfer);
                alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
            }
        } else {
            alertDialog.msg = getString(R.string.msg_cancel_transfer);
            alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
        }
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MLog.d();
                finish();
            }
        };
        alertDialog.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
        alertDialog.show();
    }

    /**
     * 전체 입출금계좌 리스트 감추기
     */
    private void hideMyAccountList() {
        MLog.d();
        if (isFinishing())
            return;

        Animation animSlideUp = AnimationUtils.loadAnimation(TransferAccountActivity.this, R.anim.sliding_list_up);
        mLayoutMyAccountList.clearAnimation();
        mLayoutMyAccountList.startAnimation(animSlideUp);
        mLayoutMyAccountList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutMyAccountList.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

//        if (DataUtil.isNotNull(mMyAccountAdapter)) {
//            mMyAccountAdapter.setIsNotClick(true);
//        }

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mListViewMyAccount.setVisibility(View.GONE);
                mBtnCloseMyAccountList.setVisibility(View.GONE);
//                if (DataUtil.isNotNull(mMyAccountAdapter)) {
//                    mMyAccountAdapter.setIsNotClick(true);
//                }
                mBtnWithdrawAccount.animate().translationY(0).withLayer();
            }
        }, 200);
    }
// ==============================================================================================================
// API
// ==============================================================================================================
    /**
     * 세션동기화
     */
    private void requestSessionSync() {
        MLog.d();

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                finish();
                            }
                        });
                        return;
                    }

                    LoginUserInfo.clearInstance();
                    LoginUserInfo.getInstance().syncLoginSession(ret);
                    LoginUserInfo.getInstance().setFakeFinderAllow(true);
                    checkVaildAuthMethod();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 은행 조회
     */
    private void requestBankList() {
        MLog.d();

        Map param = new HashMap();
        param.put("LCCD", "BANK_CD");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    Prefer.setBankList(TransferAccountActivity.this, ret);

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null)
                        return;
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;
                        mListBank.add(new RequestCodeInfo(jsonObject));
                    }

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 증권사 조회
     */
    private void requestStockList() {
        MLog.d();

        Map param = new HashMap();
        param.put("LCCD", "SCCM_CD");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    Prefer.setStockList(TransferAccountActivity.this, ret);

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null)
                        return;
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;
                        mListStock.add(new RequestCodeInfo(jsonObject));
                    }

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 이체 초기정보 처리
     *
     * @param account  출금계좌
     * @param position 전체계좌 리스트에서 선택된 계좌 위치,
     * @param isUpdate 다른 스텝 화면 정보 갱신 여부
     */
    private void requestInit(String account, final int position, final boolean isUpdate) {
        MLog.d();

        Map param = new HashMap();
        param.put("WTCH_ACNO", account);
        param.put("TRN_TYCD", "1");

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (isUpdate) {
                    hideMyAccountList();
                    hideStep03(true);
                    hideStep02(true);
                    showStep01(true);
                    mBtnWithdrawAccount.setClickable(true);
                } else {
                    interActionStep01();
                }

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        });
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                finish();
                            }
                        };
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, okClick);
                        return;
                    }

                    String TI1_TRNF_LMIT_AMT = object.optString("TI1_TRNF_LMIT_AMT");
                    if (!TextUtils.isEmpty(TI1_TRNF_LMIT_AMT))
                        mTransferOneTime = Double.valueOf(TI1_TRNF_LMIT_AMT);

                    if (mTransferOneTime >= 0)
                        mTextLimitOneTime.setText(Utils.moneyFormatToWon(mTransferOneTime) + getString(R.string.won));

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                    } else {
                        String D1_TRNF_LMIT_AMT = object.optString("D1_TRNF_LMIT_AMT");
                        if (!TextUtils.isEmpty(D1_TRNF_LMIT_AMT))
                            mTransferOneDay = Double.valueOf(D1_TRNF_LMIT_AMT);
                    }

                    if (mTransferOneDay >= 0) {
                        mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                    }

                    mBalanceAmount = Double.valueOf(object.optString("WTCH_TRN_POSB_AMT"));
                    String amount = Utils.moneyFormatToWon(mBalanceAmount);
                    mTextAccountBalance.setText(amount + getString(R.string.won));

                    mSendAccountIndex = position;

                    if (DataUtil.isNotNull(mMyAccountAdapter) && mSendAccountIndex >= 0) {
                        // 선택 계좌가 변경될 경우 이체개수 초기화
                        mTransferCnt = 0;
                        mMyAccountAdapter.setSelectedIndex(mSendAccountIndex);
                        mMyAccountAdapter.notifyDataSetChanged();
                    }

                    if (mEditAmount.isShown()) {
                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferAccountActivity.this, mEditAmount);
                            }
                        }, 100);
                    }

                } catch (Exception e) {
                    MLog.e(e);
                    showErrorMessage(getString(R.string.msg_debug_no_response), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    });
                }
            }
        });
    }

    /**
     * 계좌별칭 등록/변경 요청
     *
     * @param position 선택위치
     */
    private void requestRegisterAlias(final int position, final String alias) {
        MLog.d();

        if (validateTransferAccountInfo(position))
            return;

        ITransferRecentlyInfo ITransferRecentlyInfo = mListTransferRecently.get(position);

        Map param = new HashMap();
        param.put("CHNG_DVCD", "1");
        param.put("FIN_INST_CD", ITransferRecentlyInfo.getMNRC_BANK_CD());
        param.put("BANK_ACNO", ITransferRecentlyInfo.getMNRC_ACNO());
        param.put("ACCO_ALS", alias);

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    // 계좌변경 성공여부
                    if (!Const.REQUEST_WAS_YES.equalsIgnoreCase(object.optString("TRTM_RSLT_CD"))) {
                        showErrorMessage(getString(R.string.msg_fail_edit_alias));
                        return;
                    }

                    ITransferRecentlyInfo accountInfo = mListTransferRecently.get(position);
                    accountInfo.setMNRC_ACCO_ALS(alias);
                    mListTransferRecently.set(position, accountInfo);
                    mITransferRecentlyAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    private void showDatePicker(final boolean isDelayService) {
        MLog.d();

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                try {
                    Calendar calendar = Calendar.getInstance();

                    if (DataUtil.isNotNull(ret)) {
                        JSONObject object = new JSONObject(ret);
                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        if (DataUtil.isNull(objectHead)) {
                            return;
                        }
                        String result = object.optString("SYS_DTTM");
                        if (!TextUtils.isEmpty(result)) {
                            calendar.set(Integer.parseInt(result.substring(0, 4)), Integer.parseInt(result.substring(4, 6)) - 1, Integer.parseInt(result.substring(6, 8)));
                        }
                    }

                    // 22시 30분이 넘으면 당일 예약 이체가 불가하므로 다음날부터 이체되도록 함
                    if (!isDelayService) {
                        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 && calendar.get(Calendar.MINUTE) > 29) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    } else {
                        if (calendar.get(Calendar.HOUR_OF_DAY) + 3 >= 23) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    }

                    mCurYear = calendar.get(Calendar.YEAR);
                    mCurMon = calendar.get(Calendar.MONTH);
                    mCurDay = calendar.get(Calendar.DAY_OF_MONTH);

                    Calendar minDate = Calendar.getInstance();
                    minDate.set(mCurYear, mCurMon, mCurDay);
                    long tmp = calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 90 * 1000.0);
                    DatePickerFragmentDialog.newInstance(
                            DateTimeBuilder.get()
                                    .withMinDate(minDate.getTimeInMillis()).withMaxDate(tmp)
                                    .withTheme(R.style.datepickerCustom)
                    ).show(getSupportFragmentManager(), "DatePickerFragmentDialog");

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 즐겨찾기 계좌관리 요청
     *
     * @param position 최근 이체계좌 리스트에서 선택된 계좌 위치
     */
    private void requestFavoriteAccount(int position) {
        MLog.d();

        if (validateTransferAccountInfo(position))
            return;

        ITransferRecentlyInfo ITransferRecentlyInfo = mListTransferRecently.get(position);

        // TODO. 현재 임시 통장이라 예금주가 없음
        if (DataUtil.isNull(ITransferRecentlyInfo.getFAVO_ACCO_YN())
                || DataUtil.isNull(ITransferRecentlyInfo.getMNRC_BANK_CD())
                || DataUtil.isNull(ITransferRecentlyInfo.getMNRC_ACNO())) {
            return;
        }

        Map param = new HashMap();
        param.put("REG_STCD", Const.REQUEST_WAS_YES.equalsIgnoreCase(ITransferRecentlyInfo.getFAVO_ACCO_YN()) ? "1" : "0");
        param.put("CNTP_FIN_INST_CD", ITransferRecentlyInfo.getMNRC_BANK_CD());
        param.put("CNTP_BANK_ACNO", ITransferRecentlyInfo.getMNRC_ACNO());
        param.put("CNTP_ACCO_DEPR_NM", ITransferRecentlyInfo.getMNRC_ACCO_DEPR_NM());
        if (DataUtil.isNotNull(ITransferRecentlyInfo.getMNRC_ACCO_ALS())) {
            param.put("CNTP_ACCO_ALS", ITransferRecentlyInfo.getMNRC_ACCO_ALS());
        }

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    requestTransferAccountList(true);

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * STEP_01. 최근/자주 이체 계좌 조회
     *
     * @param isUpdate 조회결과만 update 여부
     */
    private void requestTransferAccountList(final boolean isUpdate) {
        MLog.d();

        if (DataUtil.isNotNull(mListTransferRecently) && !mListTransferRecently.isEmpty()) {
            mListTransferRecently.clear();
        }

        Map param = new HashMap();
        param.put("INQ_DVCD", "1");

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0050100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (!isUpdate)
                    showStep02();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null)
                        return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        mListTransferRecently.add(new ITransferRecentlyInfo(obj));
                    }

                    if (isUpdate) {
                        updateTransferRecentlyList(mListTransferRecently);
                    }

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * STEP_02. 수취인 조회
     *
     * @param bankStockCode   이체계좌 은행코드
     * @param transferAccount 이체계좌
     * @param transferAmount  이체금액
     */
    private void requestVerifyTransferAccount(final String bankStockCode, final String transferAccount, final String transferAmount) {
        MLog.d();

        if (DataUtil.isNull(mListMyAccount) || mListMyAccount.size() <= mSendAccountIndex || DataUtil.isNull(mListMyAccount.get(mSendAccountIndex)))
            return;

        Map param = new HashMap();
        param.put("TRTM_DVCD", "1");
        param.put("WTCH_ACNO", mListMyAccount.get(mSendAccountIndex).getACNO());
        param.put("CNTP_FIN_INST_CD", bankStockCode);
        param.put("CNTP_BANK_ACNO", transferAccount);
        param.put("TRN_AMT", transferAmount);
        param.put("MNRC_CPNO", Const.EMPTY);

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(String.format(getString(R.string.msg_fail_timeout_request), getString(R.string.inquire_deposit_account)), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!mLayoutTransferRecentlyList.isShown())
                                showTransferRecentlyList();
                        }
                    });
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(String.format(getString(R.string.msg_fail_timeout_request), getString(R.string.inquire_deposit_account)), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList();
                            }
                        });
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (DataUtil.isNull(objectHead)) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList();
                            }
                        });
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (DataUtil.isNull(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if ("EEFN0306".equalsIgnoreCase(errCode) ||
                                "EEFN0307".equalsIgnoreCase(errCode) ||
                                "EEIF0516".equalsIgnoreCase(errCode)) {
                            String url = WasServiceUrl.ERR0030100.getServiceUrl();
                            String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode + "&" + Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                            Intent intent = new Intent(TransferAccountActivity.this, WebMainActivity.class);
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                            intent.putExtra(Const.INTENT_PARAM, param);
                            startActivity(intent);
                            finish();
                            return;
                        }
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                if (!mLayoutTransferRecentlyList.isShown())
                                    showTransferRecentlyList();
                            }
                        });
                        return;
                    }

                    // 동일이체건이 존재할 경우
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(object.optString("SAME_AMT_TRNF_YN"))) {
                        AlertDialog alertDialog = new AlertDialog(TransferAccountActivity.this);
                        alertDialog.mPListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                showDlgVerifyTransferAccount(bankStockCode, transferAccount, transferAmount, object);
                            }
                        };
                        alertDialog.mNListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        };
                        alertDialog.msg = "오늘 같은 분에게 동일한 금액을\n이체하셨습니다.\n정말 이체하시겠습니까?";
                        alertDialog.show();
                        return;
                    }

                    showDlgVerifyTransferAccount(bankStockCode, transferAccount, transferAmount, object);

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * STEP_03. 업무서버에 이체목록 등록
     *
     * @param verifyTransferInfo 이체정보
     */
    private void requestConfirmTransferAccount(TransferVerifyInfo verifyTransferInfo) {
        MLog.d();

        if (DataUtil.isNull(verifyTransferInfo)
                || DataUtil.isNull(mListMyAccount)
                || mListMyAccount.size() < mSendAccountIndex
                || DataUtil.isNull(mListMyAccount.get(mSendAccountIndex))) {
            return;
        }

        Map param = new HashMap();
        param.put("UUID_NO", Const.EMPTY);
        param.put("WTCH_ACNO", mListMyAccount.get(mSendAccountIndex).getACNO());
        param.put("CNTP_FIN_INST_CD", verifyTransferInfo.getCNTP_FIN_INST_CD());
        param.put("CNTP_BANK_ACNO", verifyTransferInfo.getCNTP_BANK_ACNO());
        param.put("CNTP_ACCO_DEPR_NM", verifyTransferInfo.getRECV_NM());
        param.put("FEE", verifyTransferInfo.getFEE());
        param.put("TRN_AMT", verifyTransferInfo.getTRN_AMT());
        param.put("TRNF_DVCD", verifyTransferInfo.getTRNF_DVCD());
        param.put("TRN_UNQ_NO", verifyTransferInfo.getTRN_UNQ_NO());
        param.put("TRN_MEMO_CNTN", DataUtil.isNotNull(mTextInputMemo.getText().toString()) ? mTextInputMemo.getText().toString() : Const.EMPTY);

        if (DataUtil.isNull(mTextDisplaySending.getText().toString())) {
            param.put("DEPO_BNKB_MRK_NM", DataUtil.isNotNull(verifyTransferInfo.getCUST_NM()) ? verifyTransferInfo.getCUST_NM() : Const.EMPTY);
        } else {
            param.put("DEPO_BNKB_MRK_NM", mTextDisplaySending.getText().toString());
        }

        if (DataUtil.isNull(mTextDisplayRecipient.getText().toString())) {
            param.put("TRAN_BNKB_MRK_NM", DataUtil.isNotNull(verifyTransferInfo.getRECV_NM()) ? verifyTransferInfo.getRECV_NM() : Const.EMPTY);
        } else {
            param.put("TRAN_BNKB_MRK_NM", mTextDisplayRecipient.getText().toString());
        }

        if ("3".equalsIgnoreCase(verifyTransferInfo.getTRNF_DVCD())) {
            param.put("TRNF_DMND_DT", verifyTransferInfo.getTRNF_DMND_DT());
            param.put("TRNF_DMND_TM", verifyTransferInfo.getTRNF_DMND_TM());
        } else if ("2".equalsIgnoreCase(verifyTransferInfo.getTRNF_DVCD())) {
            // 즉시
            if ("1".equalsIgnoreCase(verifyTransferInfo.getSVC_DVCD())) {
                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                    String dt = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                    String tm = String.format("%02d0000", mInputHour);
                    verifyTransferInfo.setTRNF_DMND_DT(dt);
                    verifyTransferInfo.setTRNF_DMND_TM(tm);
                    param.put("TRNF_DMND_DT", dt);
                    param.put("TRNF_DMND_TM", tm);
                }
            } else {
                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                    String dt = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                    String tm = String.format("%02d0000", mInputHour);
                    verifyTransferInfo.setTRNF_DMND_DT(dt);
                    verifyTransferInfo.setTRNF_DMND_TM(tm);
                    param.put("TRNF_DMND_DT", dt);
                    param.put("TRNF_DMND_TM", tm);
                }
            }
        } else if ("5".equalsIgnoreCase(verifyTransferInfo.getTRNF_DVCD())) {
            if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && mInputHour != 0) {
                String dt = String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay);
                String tm = String.format("%02d0000", mInputHour);
                verifyTransferInfo.setTRNF_DMND_DT(dt);
                verifyTransferInfo.setTRNF_DMND_TM(tm);
                param.put("TRNF_DMND_DT", dt);
                param.put("TRNF_DMND_TM", tm);
            }
        }

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    if (DataUtil.isNotNull(object.optString("D1_UZ_LMIT_AMT"))) {
                        mTransferOneDay = Double.valueOf(object.optString("D1_UZ_LMIT_AMT"));
                        if (mTransferOneDay >= 0) {
                            mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                        }
                    }

                    JSONArray jsonArray = object.optJSONArray("REC");
                    if (DataUtil.isNull(jsonArray) || jsonArray.length() < 1) {
                        return;
                    }

                    TransferManager.getInstance().clear();

                    for (int index = 0; index < jsonArray.length(); index++) {

                        JSONObject jsonObject = jsonArray.getJSONObject(index);

                        if (DataUtil.isNull(jsonObject))
                            continue;

                        String cntpFinInstCd = jsonObject.optString("CNTP_FIN_INST_CD");
                        if ("000".equalsIgnoreCase(cntpFinInstCd)) {
                            cntpFinInstCd = "028";
                        }

                        TransferVerifyInfo transferInfo = new TransferVerifyInfo();
                        transferInfo.setCNTP_FIN_INST_CD(cntpFinInstCd);
                        transferInfo.setUUID_NO(jsonObject.optString("UUID_NO"));
                        transferInfo.setCNTP_BANK_ACNO(jsonObject.optString("CNTP_BANK_ACNO"));
                        transferInfo.setRECV_NM(jsonObject.optString("CNTP_ACCO_DEPR_NM"));
                        transferInfo.setDEPO_BNKB_MRK_NM(jsonObject.optString("DEPO_BNKB_MRK_NM"));
                        transferInfo.setTRAN_BNKB_MRK_NM(jsonObject.optString("TRAN_BNKB_MRK_NM"));
                        transferInfo.setTRN_AMT(jsonObject.optString("TRN_AMT"));
                        transferInfo.setFEE(jsonObject.optString("FEE"));
                        transferInfo.setTRNF_DVCD(jsonObject.optString("TRNF_DVCD"));
                        transferInfo.setMNRC_BANK_NM(jsonObject.optString("MNRC_BANK_NM"));
                        transferInfo.setBANK_NM(jsonObject.optString("BANK_NM"));
                        transferInfo.setTRNF_DMND_DT(jsonObject.has("TRNF_DMND_DT") ? jsonObject.optString("TRNF_DMND_DT") : Const.EMPTY);
                        transferInfo.setTRNF_DMND_TM(jsonObject.has("TRNF_DMND_TM") ? jsonObject.optString("TRNF_DMND_TM") : Const.EMPTY);
                        transferInfo.setTRN_MEMO_CNTN(jsonObject.has("TRN_MEMO_CNTN") ? jsonObject.optString("TRN_MEMO_CNTN") : Const.EMPTY);

                        TransferManager.getInstance().add(transferInfo);
                    }

                    // 수취인 조회 결과 확인 다이얼로그를 띄운다.
                    showVerifyTransferAccount();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 보이스피싱 문진 요청
     * */
    @SuppressLint("SimpleDateFormat")
    private void requestVoicePhising() {
        MLog.d();

        Map param = new HashMap();
        param.put("TRNF_DVCD", 1);  // 1 = 일반이체, 2 = 안심이체
        param.put("DMND_CCNT", mTransferCnt);

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (DataUtil.isNull(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    if (DataUtil.isNotNull(Utils.getJsonString(object, "APRV_YN"))
                            && Utils.getJsonString(object, "APRV_YN").equals(Const.REQUEST_WAS_NO)) {
                        String saveDate = Prefer.getPrefVoicePhishingAskPopupDate(TransferAccountActivity.this);
                        String curDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
                        if (DataUtil.isNull(saveDate) || (Integer.parseInt(saveDate) < Integer.parseInt(curDate))) {
                            showAdvanceVoicePhishing();
                        } else {
                            transferSignData();
                        }
                    } else {
                        transferSignData();
                    }

                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 이체정보 전자 서명값 요청
     */
    private void transferSignData() {
        MLog.d();

        Map param = new HashMap();
        param.put("TRN_TYCD", "1");

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (DataUtil.isNull(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    if (DataUtil.isNotNull(Utils.getJsonString(object, "ELEC_SGNR_VAL_CNTN"))) {
                        // 핀코드 인증화면을 띄운다.
                        showOTPActivity(object.optString("ELEC_SGNR_VAL_CNTN"));
                    }

                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 선택된 이체계좌 삭제 처리 요청
     *
     * @param position 선택된 리스트 아이템 위치
     */
    private void requestRemoveRecentlyListItem(final int position) {
        MLog.d();

        if (validateTransferAccountInfo(position))
            return;

        ITransferRecentlyInfo ITransferRecentlyInfo = mListTransferRecently.get(position);

        Map param = new HashMap();
        param.put("REG_STCD", "0");
        param.put("CNTP_FIN_INST_CD", ITransferRecentlyInfo.getMNRC_BANK_CD());
        param.put("CNTP_BANK_ACNO", ITransferRecentlyInfo.getMNRC_ACNO());

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0050100A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    mListTransferRecently.remove(position);
                    mITransferRecentlyAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 수취인 조회 결과 화면에서 '아니오' 선택시 미지막 수취인 조회 결과 삭제
     */
    public void requestRemoveTransfer(final boolean cancel) {
        MLog.d();

        int size = TransferManager.getInstance().size();
        if (size <= 0)
            return;

        TransferVerifyInfo transferInfo = TransferManager.getInstance().get(size - 1);

        if (DataUtil.isNull(transferInfo) || DataUtil.isNull(transferInfo.getUUID_NO())) {
            return;
        }

        Map param = new HashMap();
        param.put("UUID_NO", transferInfo.getUUID_NO());

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void endHttpRequest(String ret) {

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    if (DataUtil.isNotNull(object.optString("D1_UZ_LMIT_AMT"))) {
                        mTransferOneDay = Double.valueOf(object.optString("D1_UZ_LMIT_AMT"));
                        if (mTransferOneDay >= 0) {
                            mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                        }
                    }

                    TransferManager.getInstance().clear();

                    JSONArray array = object.optJSONArray("REC");

                    if (DataUtil.isNull(array))
                        return;

                    for (int index = 0; index < array.length(); index++) {

                        JSONObject jsonObject = array.getJSONObject(index);

                        if (DataUtil.isNull(jsonObject))
                            continue;

                        TransferVerifyInfo item = new TransferVerifyInfo();
                        item.setCNTP_BANK_ACNO(jsonObject.optString("CNTP_BANK_ACNO"));
                        item.setRECV_NM(jsonObject.optString("CNTP_ACCO_DEPR_NM"));
                        item.setDEPO_BNKB_MRK_NM(jsonObject.optString("DEPO_BNKB_MRK_NM"));
                        item.setTRAN_BNKB_MRK_NM(jsonObject.optString("TRAN_BNKB_MRK_NM"));
                        item.setTRN_AMT(jsonObject.optString("TRN_AMT"));
                        item.setFEE(jsonObject.optString("FEE"));
                        item.setTRNF_DVCD(jsonObject.optString("TRNF_DVCD"));
                        item.setMNRC_BANK_NM(jsonObject.optString("MNRC_BANK_NM"));
                        item.setBANK_NM(jsonObject.optString("BANK_NM"));
                        item.setUUID_NO(jsonObject.optString("UUID_NO"));

                        String cntpFinInstCd = jsonObject.optString("CNTP_FIN_INST_CD");
                        if ("000".equalsIgnoreCase(cntpFinInstCd)) {
                            cntpFinInstCd = "028";
                        }
                        item.setCNTP_FIN_INST_CD(cntpFinInstCd);

                        String trnfDmndDt = Utils.getJsonString(jsonObject, "TRNF_DMND_DT");
                        if (DataUtil.isNotNull(trnfDmndDt)) {
                            item.setTRNF_DMND_DT(trnfDmndDt);
                        }

                        String trnfDmndTm = Utils.getJsonString(jsonObject, "TRNF_DMND_TM");
                        if (DataUtil.isNotNull(trnfDmndTm)) {
                            item.setTRNF_DMND_TM(trnfDmndTm);
                        }

                        String trnMemoCntn = Utils.getJsonString(jsonObject, "TRN_MEMO_CNTN");
                        if (DataUtil.isNotNull(trnMemoCntn)) {
                            item.setTRN_MEMO_CNTN(trnMemoCntn);
                        }

                        TransferManager.getInstance().add(index, item);
                    }

                    // 이체아이템 삭제
                    if (!cancel && mTransferCnt > 0) {
                        mTransferCnt--;
                        MLog.i("mTransferCnt >> " + mTransferCnt);
                    }

                    updateSendMultiTransfer(false);

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }
}
