package com.sbi.saidabank.activity.main2.rearrange;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.ItemTouchHelperViewHolder;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;

import java.util.ArrayList;
import java.util.Collections;


public class ReArrangeMyAccountAdapter extends RecyclerView.Adapter<ReArrangeMyAccountAdapter.MyAccountHolder> implements ItemTouchHelperAdapter{
    private Context mContext;
    private ArrayList<Main2AccountInfo> mAccountArray;
    private OnListItemDragListener mDragStartListener;

    //ViewHolder정의
    public static class MyAccountHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        Context mContext;
        RelativeLayout layoutBody;
        ImageView ivAccountMain;
        ImageView ivAccountCouple;
        TextView tvAccName;
        TextView tvAmount;
        ImageView ivHoldBar;
        LinearLayout layoutHoldBar;
        ItemTouchHelperAdapter helperAdapter;

        public MyAccountHolder(Context context,@NonNull View itemView,ItemTouchHelperAdapter adapter) {
            super(itemView);
            mContext = context;

            layoutBody = itemView.findViewById(R.id.layout_body);
            int radius = (int)Utils.dpToPixel(mContext,12f);
            layoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#eef7fc",new int[]{radius,radius,radius,radius},1,"#dfeef3"));
            ivAccountMain = itemView.findViewById(R.id.iv_account_main);
            ivAccountCouple = itemView.findViewById(R.id.iv_account_couple);
            tvAccName = itemView.findViewById(R.id.tv_account_name);
            tvAmount = itemView.findViewById(R.id.tv_amount);
            ivHoldBar = itemView.findViewById(R.id.iv_hold_bar);
            layoutHoldBar = itemView.findViewById(R.id.layout_hold_bar);
            helperAdapter = adapter;
        }

        @Override
        public void onItemSelected() {
            Logs.e("MyAccountHolder - onItemSelected");
            int radius = (int)Utils.dpToPixel(mContext,12f);
            layoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#d0e0e9",new int[]{radius,radius,radius,radius},1,"#dfeef3"));
        }

        @Override
        public void onItemClear() {
            int radius = (int)Utils.dpToPixel(mContext,12f);
            layoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#eef7fc",new int[]{radius,radius,radius,radius},1,"#dfeef3"));
            Logs.e("MyAccountHolder - onItemClear");
            //드래그가 끝나면 여기로 온다.
            helperAdapter.onItemMoveEnd();
        }


    }

    //생성자
    public ReArrangeMyAccountAdapter(Context context, OnListItemDragListener dragStartListener){
        mContext = context;
        mAccountArray = new ArrayList<Main2AccountInfo>();
        mDragStartListener = dragStartListener;
    }

    @NonNull
    @Override
    public ReArrangeMyAccountAdapter.MyAccountHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.activity_main2_myaccount_rearrange_list_item, parent, false);
        return new ReArrangeMyAccountAdapter.MyAccountHolder(mContext,view,this);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull final ReArrangeMyAccountAdapter.MyAccountHolder holder, int position) {
        Main2AccountInfo info = mAccountArray.get(position);

        if(info.getDSCT_CD().equals("300")){
            holder.ivAccountMain.setVisibility(View.VISIBLE);
            if(info.getSHRN_ACCO_YN().equals("Y")){
                holder.ivAccountCouple.setVisibility(View.VISIBLE);
            }else{
                holder.ivAccountCouple.setVisibility(View.GONE);
            }
        }else{
            holder.ivAccountMain.setVisibility(View.INVISIBLE);
            holder.ivAccountCouple.setVisibility(View.GONE);
        }

        holder.tvAccName.setText(mAccountArray.get(position).getTRNF_DEPR_NM());

        String BLNC = info.getBLNC();
        if (!TextUtils.isEmpty(BLNC)) {
            BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
            holder.tvAmount.setText(BLNC+" 원");
        }

        holder.layoutHoldBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Logs.e("MotionEvent : " + event);
                if (event.getActionMasked() == MotionEvent.ACTION_DOWN) {
                    Logs.e("ACTION_DOWN ");
                    mDragStartListener.onStartDrag(holder);
                }
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return mAccountArray.size();
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        Collections.swap(mAccountArray, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        Logs.e("onItemMove : " + fromPosition + "->" + toPosition);
        return true;
    }

    @Override
    public void onItemDismiss(int position) {
        Logs.e("onItemDismiss ");
    }

    @Override
    public void onItemMoveEnd() {
        Logs.e("onItemMoveEnd ");
        mDragStartListener.onEndDrag(mAccountArray);
    }

    public void setAccountArray(ArrayList<Main2AccountInfo> accountArray){
        mAccountArray.clear();
        mAccountArray.addAll(accountArray);
        notifyDataSetChanged();
    }

}