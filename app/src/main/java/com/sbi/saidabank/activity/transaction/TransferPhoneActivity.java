package com.sbi.saidabank.activity.transaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.ContactsPickActivity;
import com.sbi.saidabank.activity.login.ReregLostPincodeActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.adapter.MyAccountAdapter;

import com.sbi.saidabank.activity.transaction.adater.ITransferRecentlyAdapter;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmPhoneTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingInputTextDialog;
import com.sbi.saidabank.common.dialog.TransferPhoneRealNameDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CheatUseAccountInfo;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferPhoneResultInfo;

import com.sbi.saidabank.solution.motp.MOTPManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Saidabanking_android
 * <p>
 * Class: TransferPhoneActivity
 * Created by 950485 on 2019. 02. 15..
 * <p>
 * Description: 휴대폰번호로 이체를 위한 화면
 */

public class TransferPhoneActivity extends BaseActivity implements TransferPhoneRealNameDialog.OnConfirmListener, KeyboardDetectorRelativeLayout.IKeyboardChanged  {

    private static final int REQUEST_SSENSTONE_AUTH   = 20000;
    private static final int REQUEST_CONTACTS_PICK    = 20001;
    private static final float ANIMATION_SHAKE_OFFSET = 20f;

    private static final int SCROLL_EDITTEXT = 0;
    private static final int SCROLL_CHECK_KEYBOARD = 1;

    /**
     * 계좌번호 이체 단계
     */
    private enum TRANSFER_ACCOUNT_STEP {
        STEP_01,
        STEP_02,
        STEP_03
    }

    private WeakHandler mScrollHandler;
    private ScrollView mSvScview;
    private KeyboardDetectorRelativeLayout mRelativeLayout;
    private RelativeLayout mLayoutMyAccountList;
    private ListView       mListViewMyAccount;              // 상단 계좌 리스트
    private RelativeLayout mBtnCloseMyAccountList;          // 상단 계좌 리스트 닫힘
    private RelativeLayout mBtnWithdrawAccount;             // 상단 출금계좌 표시 (출금 계좌 변경 기능)
    private TextView       mTextAccountName;                // 출금 계좌명 (별칭->상품명순)
    private TextView       mTextAccount;                    // 계좌번호 (['계좌번호 뒤4자리'])
    private ImageView      mImageShare;                     // 커플통장 가입여부
    private TextView       mTextAccountBalance;             // 계좌 출금 가능 금액
    private LinearLayout   mLayoutStep01;                   // 이체 금액 입력 단계
    private TextView       mTextHangulAmount;               // 이체 금액 한글 표시
    private LinearLayout   mLayoutStep01Body;               // 실제 금액 입력란 표시 (interaction 위해 구분)
    private View           mViewEditAmountLine;             // 금액 입력란 하단 바
    private ProgressBar    mProgressLine;                   // 금액 입력란 하단 바 (interaction 위해 추가)
    private RelativeLayout mLayoutStep01Bottom;             // 금액 입력란 하단 (interaction 위해 구분)
    private EditText       mEditAmount;                     // 금액 입력란
    private RelativeLayout mBtnEraseAmount;                 // 금액 입력 삭제 버튼
    private LinearLayout   mLayoutLimitTransfer;            // 한도
    private TextView       mTextLimitOneTime;               // 1회 이체한도
    private TextView       mTextLimitOneDay;                // 1일 이체한도
    private TextView       mTextAmountMsg;                  // 금액 오류 메세지
    private LinearLayout   mLayoutStep02;                   // 이체 번호 입력 단계
    private TextView       mTextDisplayTransferAmount;      // 이체 금액 표시 (선택시, 1단계로 이동)
    private EditText       mEditPhoneNum;                   // 이체 번호
    private EditText       mEditReceiveName;                // 받는분 실명 입력
    private ImageView      mBtnEraseReceiveName;            // 받는분 실명 입력 삭제
    private View           mViewReceiveNameLine;
    private TextView       mTextNameMsg;                    // 받는분 실명 오류 메세지 표시
    private ImageView      mBtnContacts;                    // 주소록 리스트 표시 버튼
    private View           mViewStep02Bar;
    private LinearLayout   mLayoutStep03;                   // 보낼메세지 입력 단계
    private TextView       mTextInputSendMsg;               // 보낼메세지 입력
    private Button         mBtnConfirm;                     // 확인 버튼
    private Button         mBtnSendTransfer;                // 이체하기 버튼
    private LinearLayout   mLayoutTransferRecentlyList;     // 최근 이체 내역 표시
    private LinearLayout   mLayoutNoTransferRecently;       // 최근 이체 내역 없을 시 표시
    private ListView       mListViewTransferRecently;       // 최근 이체 내역 리스트

    private String         mJsonAccount = "";               // 타메뉴에서 전달된 값
    private String         mInputAmount = "";               // 이체 금액
    private Double         mBalanceAmount = 0.0;            // 계좌 잔액
    private Double         mTransferOneTime = 0.0;          // 1회 이체한도
    private Double         mTransferOneDay = 0.0;           // 일일 이체한도
    private int            mCount = 0;                      // 계좌입력 라인 인터액션을 위한 카운트
    private int            mSendAccountIndex = -1;          // 나의 계좌 리스트에서 선택된 계좌의 인덱스

    private ArrayList<MyAccountInfo>        mListMyAccount = new ArrayList<>();          // 나의 계좌 리스트값
    private ArrayList<ITransferRecentlyInfo>  mListTransferRecently = new ArrayList<>();   // 나의 최근 이체계좌 리스트값
    private ArrayList<ContactsInfo>         mListContacts = new ArrayList<>();            // 주소록 리스트

    private ContactsInfo                    mContactsInfo;  // 주소록에서 선택된 정보

    private TRANSFER_ACCOUNT_STEP mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_01; // 이체 수행 단계 (1단계 - 금액입력, 2단계 - 이체번호 입력, 3단계 - 보낼메세지)

    private MyAccountAdapter        mMyAccountAdapter;              // 나의 계좌 리스트 어댑터
    private ITransferRecentlyAdapter mITransferRecentlyAdapter;       // 최근 이체 리스트 어댑터

    private SlidingConfirmPhoneTransferDialog singleTransferDialog; // 단건확인창

    private boolean mIsShownKeyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_transfer_phone);

        getExtra();
        boolean isVaildMenu = checkVaildMenu();
        initUX();
        //interActionStep01();

        initMyAccount();

        if (isVaildMenu) {
            requestIsAgreeTransferPhone();
        }
    }

    @Override
    protected void onResume() {
        MLog.d();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        MLog.d();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
            hideSingleTransferDialog();
            return;
        }
        if (mLayoutMyAccountList.isShown()) {
            hideMyAccountList();
            return;
        }
        /*
        if (mLayoutTransferRecentlyList.isShown()) {
            hideTransferRecentlyList(false);
            return;
        }
        */
        showCancelMessage();
        //super.onBackPressed();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Rect rectMyAccountList = new Rect();
        mLayoutMyAccountList.getGlobalVisibleRect(rectMyAccountList);
        if (mLayoutMyAccountList.getVisibility() == View.VISIBLE && !rectMyAccountList.contains((int) ev.getRawX(), (int) ev.getRawY())) {
            hideMyAccountList();
            return true;
        } else if (mLayoutMyAccountList.getVisibility() == View.INVISIBLE) {
            return true;
        }

        return super.dispatchTouchEvent(ev);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_SSENSTONE_AUTH: {
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String result = commonUserInfo.getResultMsg();

                    if ("P000".equalsIgnoreCase(result)) {
                        requestTransfer(commonUserInfo.getSignData());
                    } else {
                        Logs.showToast(this, "인증에 실패했습니다.");
                    }
                } else {

                }
                break;
            }

            case REQUEST_CONTACTS_PICK: {
                if (resultCode == RESULT_OK && data != null) {
                    final ContactsInfo contactsInfo = (ContactsInfo) data.getParcelableExtra(Const.INTENT_CONTACTS_PHONE_INFO);
                    ArrayList<ContactsInfo> listContacts = data.getParcelableArrayListExtra(Const.INTENT_CONTACTS_LIST);
                    if (listContacts != null && listContacts.size() > 0)
                        mListContacts = listContacts;

                    if (contactsInfo == null)
                        return;

                    String TLNO = contactsInfo.getTLNO();
                    if (TextUtils.isEmpty(TLNO))
                        TLNO = "";

                    String nameContacts = contactsInfo.getFLNM();
                    TransferPhoneRealNameDialog transferPhoneRealNameDialog = new TransferPhoneRealNameDialog(TransferPhoneActivity.this, nameContacts, TLNO, true);
                    transferPhoneRealNameDialog.setOnConfirmListener(this);
                    transferPhoneRealNameDialog.show();
                }
                break;
            }

            default:
                break;
        }
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logs.i("onRequestPermissionsResult");
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_READ_CONTACTS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    View.OnClickListener cancelClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };

                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            PermissionUtils.goAppSettingsActivity(TransferPhoneActivity.this);
                            finish();
                        }
                    };

                    final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                    alertDialog.mNListener = cancelClick;
                    alertDialog.mPListener = okClick;
                    alertDialog.msg = getString(R.string.msg_permission_read_contacts);
                    alertDialog.mPBtText = getString(R.string.common_setting);
                    alertDialog.show();
                }
            }
            break;

            default:
                break;
        }
    }

    @Override
    public void onConfirmPress(String name, String TLNO, boolean isFromConatactList) {

        if (isFromConatactList) {
            if (TextUtils.isEmpty(TLNO))
                return;

            TLNO = TLNO.replaceAll("-", "");
            mEditPhoneNum.setText(TLNO);
        }

        mContactsInfo = null;

        if (!TextUtils.isEmpty(name)) {
            mEditReceiveName.setText(name);
            mEditReceiveName.setSelection(name.length());
        }

        boolean isVaild = checkVaildUserName(name);
        if (isVaild) {
            if (mEditPhoneNum.isFocused()) {
                mEditPhoneNum.clearFocus();
                Utils.hideKeyboard(TransferPhoneActivity.this, mEditPhoneNum);
            }

            if (mEditReceiveName.isFocused()) {
                mEditReceiveName.clearFocus();
                Utils.hideKeyboard(TransferPhoneActivity.this, mEditReceiveName);
            }

            mBtnEraseReceiveName.setVisibility(View.VISIBLE);
            mBtnConfirm.setVisibility(View.GONE);
            mBtnSendTransfer.setVisibility(View.VISIBLE);

            mContactsInfo = new ContactsInfo();
            mContactsInfo.setFLNM(name);

            String phone = mEditPhoneNum.getText().toString();
            if (!TextUtils.isEmpty(phone))
                mContactsInfo.setTLNO(phone);

            if (mLayoutTransferRecentlyList.isShown()) {
                mLayoutTransferRecentlyList.setVisibility(View.GONE);
            }

            showStep03();
        }
    }

    @Override
    public void onKeyboardShown() {
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 300);
    }

    @Override
    public void onKeyboardHidden() {
        mIsShownKeyboard = false;
    }

    @SuppressLint("HandlerLeak")
    private class WeakHandler extends Handler {

        private WeakReference<TransferPhoneActivity> mWeakActivity;

        WeakHandler(TransferPhoneActivity weakactivity) {
            mWeakActivity = new WeakReference<>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            TransferPhoneActivity weakActivity = mWeakActivity.get();
            if (DataUtil.isNotNull(weakActivity)) {
                if (msg.what == SCROLL_CHECK_KEYBOARD) {
                    mIsShownKeyboard = true;
                } else if (msg.what == SCROLL_EDITTEXT && !mIsShownKeyboard) {
                    mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + (int) Utils.dpToPixel(TransferPhoneActivity.this, 50f));
                }
            }
        }
    }
    /**
     * Extras 값 획득
     */
    private void getExtra() {
        Intent intent = getIntent();
        mListContacts = intent.getParcelableArrayListExtra(Const.INTENT_TRANSFER_PHONE_INFO);
        mJsonAccount = intent.getStringExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mScrollHandler = new WeakHandler(TransferPhoneActivity.this);
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(TransferPhoneActivity.this);
        mLayoutMyAccountList =  (RelativeLayout) findViewById(R.id.layout_my_account_list);
        mListViewMyAccount = (ListView) findViewById(R.id.listview_my_account_list);
        mBtnCloseMyAccountList = (RelativeLayout) findViewById(R.id.layout_close_my_account_list);
        mBtnWithdrawAccount = (RelativeLayout) findViewById(R.id.layout_account_transfer_phone);
        mTextAccountName = (TextView) findViewById(R.id.textview_account_name_transfer_phone);
        mTextAccount = (TextView) findViewById(R.id.textview_account_transfer_phone);
        mImageShare = (ImageView) findViewById(R.id.imageview_share);
        mTextAccountBalance = (TextView) findViewById(R.id.textview_account_balance_transfer_phone);
        mLayoutStep01 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_01);
        mTextHangulAmount = (TextView) findViewById(R.id.textview_hangul_amount_transfer_phone);
        mLayoutStep01Body = (LinearLayout) findViewById(R.id.layout_transfer_account_step_01_body);
        mViewEditAmountLine = (View)  findViewById(R.id.view_edit_amount_bar);
        mProgressLine = (ProgressBar) findViewById(R.id.progress_edit_amount_bar);
        mLayoutStep01Bottom = (RelativeLayout) findViewById(R.id.layout_transfer_account_step_01_bottom);
        mEditAmount = (EditText) findViewById(R.id.edittext_amount_transfer_phone);
        mBtnEraseAmount = (RelativeLayout) findViewById(R.id.imageview_erase_transfer_amount);
        mLayoutLimitTransfer = (LinearLayout) findViewById(R.id.layout_limit_transfer);
        mTextLimitOneTime = (TextView) findViewById(R.id.textview_limit_one_time);
        mTextLimitOneDay = (TextView) findViewById(R.id.textview_limit_one_day);
        mTextAmountMsg = (TextView) findViewById(R.id.textview_err_msg_transfer_phone);
        mLayoutStep02 = (LinearLayout) findViewById(R.id.layout_transfer_account_step_02);
        mTextDisplayTransferAmount = (TextView) findViewById(R.id.textview_display_transfer_amount);
        mEditPhoneNum = (EditText) findViewById(R.id.edittext_phone_num_transfer_phone);
        mEditReceiveName = (EditText) findViewById(R.id.edittext_receive_name_transfer_phone);
        mBtnEraseReceiveName = (ImageView) findViewById(R.id.imageview_erase_name_transfer_phone);
        mViewReceiveNameLine = (View) findViewById(R.id.view_receive_name_line);
        mTextNameMsg = (TextView) findViewById(R.id.textview_name_msg_transfer_phone);
        mBtnContacts = (ImageView) findViewById(R.id.imageview_contacts);
        mViewStep02Bar = (View) findViewById(R.id.view_step02_bar);
        mLayoutStep03 = (LinearLayout) findViewById(R.id.layout_transfer_phone_step_03);
        mTextInputSendMsg = (TextView) findViewById(R.id.textview_input_send_msg);
        mBtnConfirm = (Button) findViewById(R.id.btn_ok_transfer_phone);
        mBtnSendTransfer = (Button) findViewById(R.id.btn_send_transfer_phone);
        mLayoutTransferRecentlyList = (LinearLayout) findViewById(R.id.layout_transfer_recently_list);
        mLayoutNoTransferRecently = (LinearLayout) findViewById(R.id.layout_no_transfer_recently_list);
        mListViewTransferRecently = (ListView) findViewById(R.id.listview_transfer_recently_list);

        TextView textAlert01 = (TextView) findViewById(R.id.msg_transfer_phone_alert01);
        String alert01 = getString(R.string.msg_transfer_phone_alert01);
        alert01 = alert01.replaceAll(" ", "\u00A0");
        textAlert01.setText(alert01);

        TextView textAlert02 = (TextView) findViewById(R.id.msg_transfer_phone_alert02);
        String alert02 = getString(R.string.msg_transfer_phone_alert02);
        alert02 = alert02.replaceAll(" ", "\u00A0");
        textAlert02.setText(alert02);

        TextView textAlert03 = (TextView) findViewById(R.id.msg_transfer_phone_alert03);
        String alert03 = getString(R.string.msg_transfer_phone_alert03);
        alert03 = alert03.replaceAll(" ", "\u00A0");
        textAlert03.setText(alert03);

//        TextView textAlert04 = (TextView) findViewById(R.id.msg_transfer_phone_alert04);
//        String alert04 = getString(R.string.msg_transfer_phone_alert04);
//        alert04 = alert04.replaceAll(" ", "\u00A0");
//        textAlert04.setText(alert04);

        TextView btnCancel = (TextView) findViewById(R.id.btn_cancel_transfer_phone);
        btnCancel.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showCancelMessage();
            }
        });

        mListViewMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MyAccountInfo accountInfo = mListMyAccount.get(position);
                if (accountInfo == null)
                    return;

                String RPRS_ACCO_YN = accountInfo.getRPRS_ACCO_YN();
                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }

                String ACNO = accountInfo.getACNO();
                String partACNO = "";
                if (!TextUtils.isEmpty(ACNO)) {
                    int lenACNO = ACNO.length();
                    if (lenACNO > 4)
                        partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                }

                mTextAccountName.setText(ACCO_ALS);
                mTextAccount.setText(" [" + partACNO + "]");

                String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                    mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    mTextAccountBalance.setText(amount + getString(R.string.won));
                }

                if (mLayoutTransferRecentlyList.isShown()) {
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);
                }

                //커플계좌상태를 보여준다.
                String SHRN_ACCO_YN = accountInfo.getSHRN_ACCO_YN();
                if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SHRN_ACCO_YN)) {
                    mImageShare.setVisibility(View.VISIBLE);
                }else{
                    mImageShare.setVisibility(View.GONE);
                }

                requestInit(ACNO, position, true);
            }
        });

        mBtnCloseMyAccountList.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                hideMyAccountList();
            }
        });

        initStep01();
        initStep02();
        initStep03();

        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_01) {
                    if (!checkVaildAmount()) {
                        AniUtils.shakeView(mEditAmount, ANIMATION_SHAKE_OFFSET);
                        mViewEditAmountLine.setBackgroundResource(R.color.red);
                        return;
                    }

                    if (mEditAmount.isFocused()) {
                        mEditAmount.clearFocus();
                        Utils.hideKeyboard(TransferPhoneActivity.this, mEditAmount);

                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                requestTransferRecentlyList(false);
                            }
                        }, 250);
                    } else {
                        requestTransferRecentlyList(false);
                    }
                } else if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_02) {
                    if (mEditPhoneNum.isFocused()) {
                        mEditReceiveName.requestFocus();
                        Utils.showKeyboard(TransferPhoneActivity.this, mEditReceiveName);
                        return;
                    } else if (mEditReceiveName.isFocused()) {
                        if (mEditPhoneNum.getText().toString().length() == 11) {
                            String nameContacts = mEditReceiveName.getText().toString();
                            TransferPhoneRealNameDialog transferPhoneRealNameDialog = new TransferPhoneRealNameDialog(TransferPhoneActivity.this, nameContacts, null, false);
                            transferPhoneRealNameDialog.setOnConfirmListener(TransferPhoneActivity.this);
                            transferPhoneRealNameDialog.show();
                        } else {
                            DialogUtil.alert(TransferPhoneActivity.this, "전화번호를 확인해주세요.", new View.OnClickListener(){
                                @Override
                                public void onClick(final View v) {
                                    mEditPhoneNum.requestFocus();
                                    Handler delayHandler = new Handler();
                                    delayHandler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Utils.showKeyboard(TransferPhoneActivity.this, mEditPhoneNum);
                                        }
                                    }, 300);
                                }
                            });
                        }
                    }
                }
            }
        });

        mListViewTransferRecently.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ITransferRecentlyInfo accountInfo = mListTransferRecently.get(position);
                if (accountInfo == null)
                    return;

                String MNRC_TLNO = accountInfo.getMNRC_TLNO();
                if (!TextUtils.isEmpty(MNRC_TLNO))
                    mEditPhoneNum.setText(MNRC_TLNO);

                String MNRC_ACCO_DEPR_NM = accountInfo.getMNRC_ACCO_DEPR_NM();
                if (!TextUtils.isEmpty(MNRC_ACCO_DEPR_NM))
                    mEditReceiveName.setText(MNRC_ACCO_DEPR_NM);
                /*
                ContactsInfo contactsInfo = new ContactsInfo();
                contactsInfo.setTLNO(accountInfo.getMNRC_TLNO());
                contactsInfo.setACCO_IDNO(accountInfo.getMNRC_BANK_CD());
                contactsInfo.setACNO(accountInfo.getMNRC_ACNO());
                contactsInfo.setFLNM(accountInfo.getMNRC_ACCO_DEPR_NM());

                mContactsInfo = contactsInfo;

                mBtnConfirm.setVisibility(View.GONE);
                hideTransferRecentlyList(true);
                */

                TransferPhoneRealNameDialog transferPhoneRealNameDialog = new TransferPhoneRealNameDialog(TransferPhoneActivity.this, MNRC_ACCO_DEPR_NM, null, false);
                transferPhoneRealNameDialog.setOnConfirmListener(TransferPhoneActivity.this);
                transferPhoneRealNameDialog.show();
            }
        });

        mBtnSendTransfer.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mTransferStep != TRANSFER_ACCOUNT_STEP.STEP_03)
                    return;

                if (mLayoutTransferRecentlyList.isShown())
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);

                if (mContactsInfo == null)
                    return;

                String amount = mEditAmount.getText().toString();
                if (TextUtils.isEmpty(amount))
                    return;

                String phoneNum = mEditPhoneNum.getText().toString();
                if (TextUtils.isEmpty(phoneNum)) {
                    Toast.makeText(TransferPhoneActivity.this, R.string.msg_check_phone_number, Toast.LENGTH_SHORT).show();
                    return;
                }

                String name = mEditReceiveName.getText().toString();
                if (TextUtils.isEmpty(name)) {
                    Toast.makeText(TransferPhoneActivity.this, R.string.msg_check_transfer_phone, Toast.LENGTH_SHORT).show();
                    return;
                }

                amount = amount.replaceAll("[^0-9]", "");
                mContactsInfo.setTRN_AMT(amount);

                String inputSendMsg = mTextInputSendMsg.getText().toString();
                if (!TextUtils.isEmpty(inputSendMsg))
                    mContactsInfo.setDEPO_BNKB_MRK_NM(inputSendMsg);

                requestVerifyTransferPhone();
            }
        });

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        float density  = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        float rate = dpHeight / dpWidth;
        if (rate < 1.70) {
            ViewGroup.LayoutParams p = mLayoutStep02.getLayoutParams();
            float magin = Utils.dpToPixel(TransferPhoneActivity.this, 20);
            if (p instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) p;
                lp.setMargins(0, (int) magin, 0, 0);
                mLayoutStep02.setLayoutParams(lp);
            } else if (p instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) p;
                lp.setMargins(0, (int) magin, 0, 0);
                mLayoutStep02.setLayoutParams(lp);
            } else if (p instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) p;
                lp.setMargins(0, (int) magin, 0, 0);
                mLayoutStep02.requestLayout();
            }

            ViewGroup.LayoutParams pTop = mLayoutTransferRecentlyList.getLayoutParams();
            float maginTop = Utils.dpToPixel(TransferPhoneActivity.this, 292);
            if (pTop instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lpTop = (LinearLayout.LayoutParams) pTop;
                lpTop.setMargins(0, (int) maginTop, 0, 0);
                mLayoutTransferRecentlyList.setLayoutParams(lpTop);
            } else if (pTop instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lpTop = (RelativeLayout.LayoutParams) pTop;
                lpTop.setMargins(0, (int) maginTop, 0, 0);
                mLayoutTransferRecentlyList.setLayoutParams(lpTop);
            } else if (pTop instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams lpTop = (ViewGroup.MarginLayoutParams) pTop;
                lpTop.setMargins(0, (int) maginTop, 0, 0);
                mLayoutTransferRecentlyList.requestLayout();
            }
        }

        if (CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_YN().equals("Y")){
            showCheatUseAccountDialog();
        }
    }

    /**
     * 사기이용계좌 금융거래제한 안내 팝업
     */
    private void showCheatUseAccountDialog(){
        String replaceStr = "";
        String str = "고객님께서는 금융기관에\n전자금융거래제한자로 등록되어 있어 거래가\n불가합니다";


        String FinancialInstitutionNM = CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_FIN_INST_NM(); //금융기관명
        if (!FinancialInstitutionNM.isEmpty()){
            replaceStr = str.replace("금융기관", FinancialInstitutionNM);
        }

        DialogUtil.alert(TransferPhoneActivity.this,
                replaceStr,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
    }

    /**
     * 타이틀, 이체금액 입력 화면
     */
    private void initStep01() {
        mBtnWithdrawAccount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mEditAmount.isFocused()) {
                    mEditAmount.clearFocus();
                    Utils.hideKeyboard(TransferPhoneActivity.this, mEditAmount);
                }

                mBtnWithdrawAccount.animate().translationY(-500).withLayer();
                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showMyAccountList();
                    }
                }, 200);
            }
        });

        mEditAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                if (!TextUtils.isEmpty(charSequence.toString()) && !charSequence.toString().equals(mInputAmount)) {
                    mInputAmount = Utils.moneyFormatToWon(Double.parseDouble(charSequence.toString().replaceAll(",", "")));
                    mEditAmount.setText(mInputAmount);
                    mEditAmount.setSelection(mInputAmount.length());

                    String hangul = Utils.convertHangul(mInputAmount);
                    if (!TextUtils.isEmpty(hangul)) {
                        hangul += getString(R.string.won);
                        mTextHangulAmount.setText(hangul);
                    }
                } else {
                    if (TextUtils.isEmpty(charSequence.toString())) {
                        mTextHangulAmount.setText("");
                        mInputAmount = "";
                        mBtnConfirm.setEnabled(false);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
                String amount = editable.toString();
                amount = amount.replaceAll("[^0-9]", "");
                if (!TextUtils.isEmpty(amount) && Double.valueOf(amount) > 0) {
                    if (!mTextHangulAmount.isShown())
                        mTextHangulAmount.setVisibility(View.VISIBLE);

                    if (!checkVaildAmount()) {
                        mViewEditAmountLine.setBackgroundResource(R.color.red);
                        mLayoutLimitTransfer.setVisibility(View.GONE);
                        mBtnConfirm.setEnabled(false);
                        AniUtils.shakeView(mEditAmount, ANIMATION_SHAKE_OFFSET);
                        return;
                    }

                    if (mTextAmountMsg.isShown()) {
                        mTextAmountMsg.setText("");
                        mTextAmountMsg.setVisibility(View.GONE);
                    }

                    if (!mLayoutLimitTransfer.isShown())
                        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

                    if (!mBtnEraseAmount.isShown())
                        mBtnEraseAmount.setVisibility(View.VISIBLE);

                    mViewEditAmountLine.setBackgroundResource(R.color.color00D5E7);
                } else {
                    mBtnConfirm.setEnabled(false);
                    mBtnEraseAmount.setVisibility(View.GONE);

                    if (mTextAmountMsg.isShown()) {
                        mTextAmountMsg.setText("");
                        mTextAmountMsg.setVisibility(View.GONE);
                    }

                    if (!mLayoutLimitTransfer.isShown())
                        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

                    mViewEditAmountLine.setBackgroundResource(R.color.color00D5E7);
                }
            }
        });

        mEditAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    String amount = mEditAmount.getText().toString();
                    if (!TextUtils.isEmpty(amount)) {
                        if (!mBtnEraseAmount.isShown())
                            mBtnEraseAmount.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (mBtnEraseAmount.isShown())
                        mBtnEraseAmount.setVisibility(View.GONE);
                }
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
            }
        });

        mBtnEraseAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditAmount.setText("");
            }
        });
    }

    /**
     * 이체계좌 선택 화면
     */
    private void initStep02() {
        mTextDisplayTransferAmount.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLayoutTransferRecentlyList.isShown())
                    mLayoutTransferRecentlyList.setVisibility(View.GONE);

                mLayoutStep02.setVisibility(View.INVISIBLE);
                Animation animation = AnimationUtils.loadAnimation(TransferPhoneActivity.this, R.anim.sliding_out_bottom_sbi);
                mLayoutStep02.clearAnimation();
                mLayoutStep02.startAnimation(animation);
                mLayoutStep02.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        hideStep03(false);
                        hideStep02(false);
                        showStep01(false);

                        mViewEditAmountLine.setVisibility(View.GONE);
                        showProgressLine();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        });

        mBtnContacts.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                /*
                if (mLayoutTransferRecentlyList.isShown()) {
                    hideTransferRecentlyList(false);
                }
                */

                Intent intent = new Intent(TransferPhoneActivity.this, ContactsPickActivity.class);
                if (mListContacts != null && mListContacts.size() > 0 )
                    intent.putParcelableArrayListExtra(Const.INTENT_CONTACTS_LIST, mListContacts);

                startActivityForResult(intent, REQUEST_CONTACTS_PICK);
            }
        });

        mEditPhoneNum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mLayoutTransferRecentlyList.isShown() && mLayoutStep03.getVisibility() != View.VISIBLE) {
                    mLayoutTransferRecentlyList.setVisibility(View.VISIBLE);
                    if (mViewStep02Bar.getVisibility() != View.INVISIBLE)
                        mViewStep02Bar.setVisibility(View.VISIBLE);
                }
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
            }
        });

        mEditPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_03) {
                    mContactsInfo = null;
                    hideStep03(false);
                }

                if (editable.length() > 0) {
                    mBtnConfirm.setEnabled(true);
                } else {
                    String inputName = mEditReceiveName.getText().toString();
                    if (TextUtils.isEmpty(inputName))
                        mBtnConfirm.setEnabled(false);
                }
            }
        });

        mEditReceiveName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (mTransferStep == TRANSFER_ACCOUNT_STEP.STEP_03) {
                    mContactsInfo = null;
                    hideStep03(false);
                }

                String phone = mEditPhoneNum.getText().toString();

                String inputName = editable.toString();
                if (!inputName.matches("^[ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]*$")) {
                    mEditReceiveName.removeTextChangedListener(this);
                    mEditReceiveName.setText(inputName.replaceAll("[^ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]", ""));
                    mEditReceiveName.setSelection(mEditReceiveName.getText().length());
                    mEditReceiveName.addTextChangedListener(this);
                    inputName = mEditReceiveName.getText().toString();
                }

                if (editable.length() > 0) {
                    mBtnConfirm.setEnabled(true);
                    mBtnEraseReceiveName.setVisibility(View.VISIBLE);
                } else {
                    if (TextUtils.isEmpty(phone))
                        mBtnConfirm.setEnabled(false);
                    mTextNameMsg.setVisibility(View.GONE);
                    mViewReceiveNameLine.setBackgroundColor(getResources().getColor(R.color.colorE7E7E7));
                    mBtnEraseReceiveName.setVisibility(View.GONE);
                }
            }
        });

        mEditReceiveName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    String name = mEditReceiveName.getText().toString();
                    if (!TextUtils.isEmpty(name))
                        mBtnEraseReceiveName.setVisibility(View.VISIBLE);

                    if (!mLayoutTransferRecentlyList.isShown())
                        showTransferRecentlyList();

                    hideStep03(false);
                }
            }
        });

        mEditReceiveName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;

                switch (result) {
                    case EditorInfo.IME_ACTION_DONE :
                        String nameContacts = textView.getText().toString();
                        TransferPhoneRealNameDialog transferPhoneRealNameDialog = new TransferPhoneRealNameDialog(TransferPhoneActivity.this, nameContacts, null, false);
                        transferPhoneRealNameDialog.setOnConfirmListener(TransferPhoneActivity.this);
                        transferPhoneRealNameDialog.show();

                        if (mEditReceiveName.isFocused()) {
                            mEditReceiveName.clearFocus();
                            Utils.hideKeyboard(TransferPhoneActivity.this, mEditReceiveName);
                        }
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });

        mBtnEraseReceiveName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEditReceiveName.setText("");
                mEditReceiveName.requestFocus();

                if (!mLayoutTransferRecentlyList.isShown()) {
                    showTransferRecentlyList();

                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.showKeyboard(TransferPhoneActivity.this, mEditReceiveName);
                        }
                    }, 500);
                } else {
                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Utils.showKeyboard(TransferPhoneActivity.this, mEditReceiveName);
                        }
                    }, 100);
                }



            }
        });
    }

    /**
     * 이체 정보 입력 화면
     */
    private void initStep03() {
        final String hintMemo = String.format(getString(R.string.msg_input_limit_length), 20);
        mTextInputSendMsg.setHint(hintMemo);
        mTextInputSendMsg.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                final SlidingInputTextDialog dialog = new SlidingInputTextDialog(TransferPhoneActivity.this,
                        getString(R.string.send_msg), hintMemo, 20, mTextInputSendMsg.getText().toString(),
                        new SlidingInputTextDialog.FinishInputListener() {
                            @Override
                            public void OnFinishInputListener(String inputString) {
                                mTextInputSendMsg.setText(inputString);
                            }
                        });
                dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
                dialog.show();
            }
        });

        final ImageView btnEraseMemo = (ImageView) findViewById(R.id.imageview_erase_memo);
        mTextInputSendMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 0) {
                    btnEraseMemo.setVisibility(View.GONE);
                } else {
                    btnEraseMemo.setVisibility(View.VISIBLE);
                }
            }
        });

        btnEraseMemo.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mTextInputSendMsg.setText("");
            }
        });
    }

    /**
     * 최초 화면 진입 인터액션
     * 금액입력란이 하단에서 위로 올라옴
     */
    private void interActionStep01() {
        Animation animSlideTopBody = AnimationUtils.loadAnimation(this, R.anim.sliding_fade_in_transfer_view);
        mLayoutStep01Body.clearAnimation();
        mLayoutStep01Body.startAnimation(animSlideTopBody);
        mLayoutStep01Body.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01Body.setVisibility(View.VISIBLE);
                showProgressLine();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최초 화면 진입 금액입력 하단바 인터액션
     * 하단바가 좌에서 우로 이동, 금액입력란은 fade in
     */
    private void showProgressLine() {
        mProgressLine.setVisibility(View.VISIBLE);

        showFadeInLimitAmount();

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (mCount >= 100) {
                    timer.cancel();
                    mCount = 0;
                    showAmountLine();
                } else {
                    updateProgressLine();
                }
            }
        }, 0, 2);
    }

    /**
     * 하단바가 좌에서 우로 이동 완료 후 실제 입력란 표시 및 포커스
     */
    private void showAmountLine() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mProgressLine.setVisibility(View.INVISIBLE);
                mViewEditAmountLine.setVisibility(View.VISIBLE);
                mEditAmount.requestFocus();
                Utils.showKeyboard(TransferPhoneActivity.this, mEditAmount);
            }
        });
    }

    private void updateProgressLine() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCount++;
                mProgressLine.setProgress(mCount);
            }
        });
    }

    /**
     * 금액입력란 하단 이체 한도 fade in
     */
    private void showFadeInLimitAmount() {
        mLayoutStep01Bottom.setVisibility(View.INVISIBLE);
        Animation animSlideTopBody = AnimationUtils.loadAnimation(this, R.anim.fade_in_transfer_limit);
        mLayoutStep01Bottom.clearAnimation();
        mLayoutStep01Bottom.startAnimation(animSlideTopBody);
        mLayoutStep01Bottom.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01Bottom.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 나의 계좌 중 대표 및 진입 화면 선택 계좌 정보 표시
     */
    private void initMyAccount() {
        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mListMyAccount = userInfo.getMyAccountInfoArrayList();
        if (mListMyAccount.size() <= 0)
            return;

        MyAccountInfo accountInfo = null;
        if (TextUtils.isEmpty(mJsonAccount)) {
            accountInfo = mListMyAccount.get(0);
        } else {
            for (int index = 0; index < mListMyAccount.size(); index++) {
                MyAccountInfo account = (MyAccountInfo) mListMyAccount.get(index);
                if (account == null)
                    continue;

                String ACNO = account.getACNO();
                if (mJsonAccount.equalsIgnoreCase(ACNO)) {
                    accountInfo = account;
                    break;
                }
            }
        }

        if (accountInfo == null)
            accountInfo = mListMyAccount.get(0);

        String ACCO_ALS = accountInfo.getACCO_ALS();
        if (TextUtils.isEmpty(ACCO_ALS)) {
            ACCO_ALS = accountInfo.getPROD_NM();
        }

        String ACNO = accountInfo.getACNO();
        String partACNO = "";
        if (!TextUtils.isEmpty(ACNO)) {
            int lenACNO = ACNO.length();
            if (lenACNO > 4)
                partACNO = ACNO.substring(lenACNO - 4, lenACNO);
        }

        mTextAccountName.setText(ACCO_ALS);
        mTextAccount.setText(" [" + partACNO + "]");

        String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
        if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
            mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
            String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
            mTextAccountBalance.setText(amount + getString(R.string.won));
        }

        //커플계좌여부 추가
        String SHRN_ACCO_YN = accountInfo.getSHRN_ACCO_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SHRN_ACCO_YN)) {
            mImageShare.setVisibility(View.VISIBLE);
        }else{
            mImageShare.setVisibility(View.GONE);
        }
    }

    /**
     * 전체 계좌 리스트 생성
     */
    private void checkMyAccount() {
        if (mListMyAccount.size() > 0)
            mListMyAccount.clear();

        LoginUserInfo userInfo = LoginUserInfo.getInstance();
        mListMyAccount = userInfo.getMyAccountInfoArrayList();
        if (mListMyAccount.size() <= 0) {
            dismissProgressDialog();
            interActionStep01();
            return;
        }

        if (mMyAccountAdapter != null)
            mMyAccountAdapter = null;

        mMyAccountAdapter = new MyAccountAdapter(TransferPhoneActivity.this, mListMyAccount);
        mListViewMyAccount.setAdapter(mMyAccountAdapter);

        int size = mListMyAccount.size();
        if (size >= 6) {
            int height = 0;
            for (int i = 0; i < 6; i++) {
                View childView = mMyAccountAdapter.getView(i, null, mListViewMyAccount);
                childView.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
                height+= childView.getMeasuredHeight();
            }
            height += mListViewMyAccount.getDividerHeight() * 6;

            ViewGroup.LayoutParams params = mListViewMyAccount.getLayoutParams();
            params.height = height;
            mListViewMyAccount.setLayoutParams(params);
        }

        if (TextUtils.isEmpty(mJsonAccount)) {
            showMyAccount(null);
        } else {
            showMyAccount(mJsonAccount);
        }
    }

    /**
     * 전체 계좌 중 대표 및 진입 시 계좌 표시
     */
    private void showMyAccount(String accountNum) {
        int size = mListMyAccount.size();

        boolean isExistRPRS_ACCO_YN = false;
        if (TextUtils.isEmpty(accountNum)) {
            for (int index = 0; index < size; index++) {
                MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(index);
                if (accountInfo == null)
                    continue;

                String RPRS_ACCO_YN = accountInfo.getRPRS_ACCO_YN();
                if (!TextUtils.isEmpty(RPRS_ACCO_YN) && Const.REQUEST_WAS_YES.equalsIgnoreCase(RPRS_ACCO_YN)) {
                    String ACCO_ALS = accountInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(ACCO_ALS)) {
                        ACCO_ALS = accountInfo.getPROD_NM();
                    }

                    String ACNO = accountInfo.getACNO();
                    String partACNO = "";
                    if (!TextUtils.isEmpty(ACNO)) {
                        int lenACNO = ACNO.length();
                        if (lenACNO > 4)
                            partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                    }

                    mTextAccountName.setText(ACCO_ALS);
                    mTextAccount.setText(" [" + partACNO + "]");

                    String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                    if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                        mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                        String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                        mTextAccountBalance.setText(amount + getString(R.string.won));
                    }

                    requestInit(ACNO, index, false);
                    isExistRPRS_ACCO_YN = true;
                    break;
                }
            }

            if (!isExistRPRS_ACCO_YN) {
                MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(0);
                if (accountInfo == null)
                    return;

                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }

                String ACNO = accountInfo.getACNO();
                String partACNO = "";
                if (!TextUtils.isEmpty(ACNO)) {
                    int lenACNO = ACNO.length();
                    if (lenACNO > 4)
                        partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                }

                mTextAccountName.setText(ACCO_ALS);
                mTextAccount.setText(" [" + partACNO + "]");

                String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                    mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    mTextAccountBalance.setText(amount + getString(R.string.won));
                }
                requestInit(ACNO, 0, false);
            }
        } else {
            for (int index = 0; index < size; index++) {
                MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(index);
                if (accountInfo == null)
                    continue;

                String ACNO = accountInfo.getACNO();
                if (accountNum.equalsIgnoreCase(ACNO)) {
                    String ACCO_ALS = accountInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(ACCO_ALS)) {
                        ACCO_ALS = accountInfo.getPROD_NM();
                    }

                    String partACNO = "";
                    if (!TextUtils.isEmpty(ACNO)) {
                        int lenACNO = ACNO.length();
                        if (lenACNO > 4)
                            partACNO = ACNO.substring(lenACNO - 4, lenACNO);
                    }

                    mTextAccountName.setText(ACCO_ALS);
                    mTextAccount.setText(" [" + partACNO + "]");

                    String WTCH_POSB_AMT = accountInfo.getWTCH_POSB_AMT();
                    if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                        mBalanceAmount = Double.valueOf(WTCH_POSB_AMT);
                        String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                        mTextAccountBalance.setText(amount + getString(R.string.won));
                    }

                    requestInit(ACNO, index, false);
                    break;
                }
            }
        }
    }

    /**
     * 이체세션클리어 요청
     */
//    private void requestClearSession() {
//        Map param = new HashMap();
//        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
//            @Override
//            public void endHttpRequest(String ret) {
//                Logs.i("TRA0010100A01 : " + ret);
//                if (TextUtils.isEmpty(ret)) {
//                    Logs.e(getString(R.string.msg_debug_no_response));
//                    showErrorMessage(getString(R.string.msg_debug_no_response));
//                    return;
//                }
//
//                try {
//                    JSONObject object = new JSONObject(ret);
//                    if (object == null) {
//                        Logs.e(getString(R.string.msg_debug_err_response));
//                        Logs.e(ret);
//                        showErrorMessage(getString(R.string.msg_debug_err_response));
//                        return;
//                    }
//
//                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
//                    if (objectHead == null) {
//                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
//                        return;
//                    }
//
//                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
//                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
//                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
//                        if (TextUtils.isEmpty(msg))
//                            msg = getString(R.string.common_msg_no_reponse_value_was);
//
//                        Logs.e("error msg : " + msg + ", ret : " + ret);
//                        //showErrorMessage(msg);
//
//                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
//                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
//                        return;
//                    }
//
//                } catch (JSONException e) {
//                    Logs.printException(e);
//                }
//            }
//        });
//    }

    /**
     * 전체 입출금계좌 조회
     */
    private void requestMyAccountList() {
        if (mListMyAccount != null && mListMyAccount.size() > 0)
            mListMyAccount.clear();

        Map param = new HashMap();
        param.put("DMNB_ACCO_YN", Const.REQUEST_WAS_YES);      // 요구불계좌여부
        param.put("ISST_ACCO_YN", Const.REQUEST_WAS_NO);       // 적립식계좌여부
        param.put("DFST_ACCO_YN", Const.REQUEST_WAS_NO);       // 거치식계좌여부
        param.put("ACCO_STCD", "1");                           // 계좌상태코드 (1 - 등록), 4 - 해지)

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                Logs.i("CMM0010700A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        MyAccountInfo item = new MyAccountInfo(obj);
                        mListMyAccount.add(item);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 전체 입출금계좌 리스트 표시
     * fade in 및 alpha 조정으로 처리
     */
    private void showMyAccountList() {
        if (this.isFinishing())
            return;

        mLayoutMyAccountList.setVisibility(View.INVISIBLE);
        mListViewMyAccount.setVisibility(View.VISIBLE);
        mBtnCloseMyAccountList.setVisibility(View.VISIBLE);

        Animation animSlideDown = AnimationUtils.loadAnimation(this, R.anim.sliding_list_down);
        mLayoutMyAccountList.clearAnimation();
        mLayoutMyAccountList.startAnimation(animSlideDown);
        mLayoutMyAccountList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutMyAccountList.setVisibility(View.VISIBLE);
                mLayoutMyAccountList.bringToFront();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 전체 입출금계좌 리스트 감추기
     * fade out 및 alpha 조정으로 처리
     */
    private void hideMyAccountList() {
        if (this.isFinishing())
            return;

        Animation animSlideUp = AnimationUtils.loadAnimation(this, R.anim.sliding_list_up);
        mLayoutMyAccountList.clearAnimation();
        mLayoutMyAccountList.startAnimation(animSlideUp);
        mLayoutMyAccountList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutMyAccountList.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

//        if (mMyAccountAdapter != null)
//            mMyAccountAdapter.setIsNotClick(true);

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mListViewMyAccount.setVisibility(View.GONE);
                mBtnCloseMyAccountList.setVisibility(View.GONE);
//                if (mMyAccountAdapter != null)
//                    mMyAccountAdapter.setIsNotClick(false);
                mBtnWithdrawAccount.animate().translationY(0).withLayer();
            }
        }, 400);
    }

    /**
     * 간편이체약관동의여부관리
     */
    private void requestIsAgreeTransferPhone() {
        Map param = new HashMap();
        param.put("TRN_DVCD", "1");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0020100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    dismissProgressDialog();

                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), okClick);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        dismissProgressDialog();
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), okClick);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        dismissProgressDialog();
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), okClick);

                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        dismissProgressDialog();
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String SMPL_TRNF_STPL_AGR_YN = object.optString("SMPL_TRNF_STPL_AGR_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SMPL_TRNF_STPL_AGR_YN)) {
                        requestSessionSync();
                    } else {
                        dismissProgressDialog();
                        Intent transferIntent = new Intent(TransferPhoneActivity.this, TransferPhonePolicyActivity.class);
                        startActivity(transferIntent);
                        overridePendingTransition(0, 0);
                        finish();
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이체초기정보처리
     * @param account 출금계좌
     * @param position 전체계좌 리스트에서 선택된 계좌 위치,
     * @param isUpdate 다른 스텝 화면 정보 갱신 여부
     */
    private void requestInit(String account, final int position, final boolean isUpdate) {
        Map param = new HashMap();
        param.put("WTCH_ACNO", account);
        param.put("TRN_TYCD", "2");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if (isUpdate) {
                    hideMyAccountList();
                    hideStep03(true);
                    hideStep02(true);
                    showStep01(true);
                    mBtnWithdrawAccount.setClickable(true);
                } else {
                    interActionStep01();
                }

                Logs.i("TRA0010100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    showErrorMessage(getString(R.string.msg_debug_no_response), okClick);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        showErrorMessage(getString(R.string.msg_debug_err_response), okClick);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was), okClick);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        /*
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        };
                        showErrorMessage(msg, okClick);
                        */

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                finish();
                            }
                        };
                        showCommonErrorDialog(msg, errCode, "", objectHead, true, okClick);
                        return;
                    }

                    String TI1_TRNF_LMIT_AMT = object.optString("TI1_TRNF_LMIT_AMT");
                    if (!TextUtils.isEmpty(TI1_TRNF_LMIT_AMT))
                        mTransferOneTime = Double.valueOf(TI1_TRNF_LMIT_AMT);

                    if (mTransferOneTime >= 0)
                        mTextLimitOneTime.setText(Utils.moneyFormatToWon(mTransferOneTime) + getString(R.string.won));

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                    } else {
                        String D1_TRNF_LMIT_AMT = object.optString("D1_TRNF_LMIT_AMT");
                        if (!TextUtils.isEmpty(D1_TRNF_LMIT_AMT))
                            mTransferOneDay = Double.valueOf(D1_TRNF_LMIT_AMT);
                    }

                    if (mTransferOneDay >= 0)
                        mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));

                    String WTCH_TRN_POSB_AMT = object.optString("WTCH_TRN_POSB_AMT");
                    mBalanceAmount = Double.valueOf(WTCH_TRN_POSB_AMT);
                    String amount = Utils.moneyFormatToWon(Double.valueOf(mBalanceAmount));
                    mTextAccountBalance.setText(amount + getString(R.string.won));

                    mSendAccountIndex = position;

                    if (mMyAccountAdapter != null) {
                        if (mSendAccountIndex >= 0) {
                            mMyAccountAdapter.setSelectedIndex(mSendAccountIndex);
                            mMyAccountAdapter.notifyDataSetChanged();
                        }
                    }

                    if (mEditAmount.isShown()) {
                        Handler delayHandler = new Handler();
                        delayHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEditAmount.requestFocus();
                                Utils.showKeyboard(TransferPhoneActivity.this, mEditAmount);
                            }
                        }, 100);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 통장 한도 조회
     * @param account 통장번호
     */
    private void requestInqueryLimit(String account) {
        Map param = new HashMap();
        param.put("TRN_DVCD", "0");
        param.put("ACNO", account);
        param.put("TRN_TYCD", "2");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0010300A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                    if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                        mTransferOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                        if (mTransferOneDay >= 0)
                            mTextLimitOneDay.setText(Utils.moneyFormatToWon(mTransferOneDay) + getString(R.string.won));
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이체 가능 금액 존재 여부 체크
     * @return boolean
     */
    private boolean checkUnderAmount() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount) || mBalanceAmount <= 0) {
            mBtnConfirm.setEnabled(false);
            return true;
        }

        return false;
    }

    /**
     * 잔액 이체 가능 금액 체크
     * @return boolean
     */
    private boolean checkOverAmount() {
        String inputAmount = mEditAmount.getText().toString();
        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        double dInputAmount = Double.valueOf(inputAmount);
        if (dInputAmount > mBalanceAmount) {
            return true;
        }

        return false;
    }

    /**
     * 1회 이체 가능 금액 체크
     * @return boolean
     */
    private boolean checkOverOneTime() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount)) {
            mBtnConfirm.setEnabled(false);
            return false;
        }

        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        double dInputAmount = Double.valueOf(inputAmount);
        if (dInputAmount > mTransferOneTime)
            return true;

        return false;
    }

    /**
     * 일일이체 가능 금액 체크
     * @return boolean
     */
    private boolean checkOverOneDay() {
        String inputAmount = mEditAmount.getText().toString();
        if (TextUtils.isEmpty(inputAmount)) {
            mBtnConfirm.setEnabled(false);
            return false;
        }

        inputAmount = inputAmount.replaceAll("[^0-9]", "");
        double dInputAmount = Double.valueOf(inputAmount);
        if (dInputAmount > mTransferOneDay) {
            return true;
        }

        return false;
    }

    /**
     * 이체금액 유효성 체크
     * @return boolean
     */
    private boolean checkVaildAmount() {
        if (checkUnderAmount()) {
            String msg = getString(R.string.msg_err_trnasfer_over_balance);
            mTextAmountMsg.setText(msg);
            mTextAmountMsg.setVisibility(View.VISIBLE);
            mBtnConfirm.setEnabled(false);
            return false;
        }

        if (checkOverAmount()) {
            String msg = getString(R.string.msg_err_trnasfer_over_balance);
            mTextAmountMsg.setText(msg);
            mTextAmountMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverOneTime()) {
            String msg = getString(R.string.msg_err_trnasfer_over_one_time);
            mTextAmountMsg.setText(msg);
            mTextAmountMsg.setVisibility(View.VISIBLE);
            return false;
        } else if (checkOverOneDay()) {
            String msg = getString(R.string.msg_err_trnasfer_over_one_day);
            mTextAmountMsg.setText(msg);
            mTextAmountMsg.setVisibility(View.VISIBLE);
            return false;
        } else {
            mBtnConfirm.setEnabled(true);
            mTextHangulAmount.setVisibility(View.VISIBLE);
            mTextAmountMsg.setVisibility(View.GONE);
        }

        return true;
    }

    /**
     * 취소 메세지 출력
     */
    private void showCancelMessage() {
        View.OnClickListener okClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        };

        View.OnClickListener cancelClick = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };

        final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
        alertDialog.mPListener = okClick;
        alertDialog.mNListener = cancelClick;
        alertDialog.msg = getString(R.string.msg_cancel_transfer);
        alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
        alertDialog.show();
    }

    /**
     * 입금할 금액 입력 화면 표시
     * @param isClearAmount 입력 금액 삭제 여부
     */
    private void showStep01(boolean isClearAmount) {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_01;

        mTextAmountMsg.setVisibility(View.GONE);
        mLayoutLimitTransfer.setVisibility(View.VISIBLE);

        mBtnConfirm.setVisibility(View.VISIBLE);

        if (mLayoutTransferRecentlyList.isShown())
            mLayoutTransferRecentlyList.setVisibility(View.GONE);

        mLayoutStep01.setVisibility(View.VISIBLE);

        if (isClearAmount) {
            mEditAmount.setText("");
        } else {
            mBtnConfirm.setEnabled(true);
        }
    }

    /**
     * 금액 입력란 사라지는 interaction
     * 하단으로 이동, fade out
     */
    private void interActionHideStep01() {
        Animation animSlideOut = AnimationUtils.loadAnimation(this, R.anim.sliding_fade_out_transfer_view);
        mLayoutStep01.clearAnimation();
        mLayoutStep01.startAnimation(animSlideOut);
        mLayoutStep01.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep01.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mLayoutStep02.setVisibility(View.INVISIBLE);
                interActionShowStep02();
            }
        }, 100);
    }

    /**
     * 입금할 휴대폰번호 입력 화면 표시
     */
    private void showStep02() {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;

        mBtnConfirm.setEnabled(false);
        mBtnConfirm.setVisibility(View.VISIBLE);
        mBtnSendTransfer.setVisibility(View.GONE);

        interActionHideStep01();

        String amount = mEditAmount.getText().toString();
        if (!TextUtils.isEmpty(amount))
            mTextDisplayTransferAmount.setText(amount);
    }

    /**
     * 이체 휴대폰번호 입력 화면 interaction
     * 상단으로 이동, fade in 처리
     */
    private void interActionShowStep02() {
        Animation animSlideIn = AnimationUtils.loadAnimation(this, R.anim.sliding_in_transfer_verify);
        mLayoutStep02.clearAnimation();
        mLayoutStep02.startAnimation(animSlideIn);
        mLayoutStep02.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                String phoneNum = mEditPhoneNum.getText().toString();
                String receiveName = mEditReceiveName.getText().toString();
                if (!TextUtils.isEmpty(phoneNum) && TextUtils.isEmpty(receiveName)) {
                    mEditPhoneNum.requestFocus();
                } else if (TextUtils.isEmpty(phoneNum) && !TextUtils.isEmpty(receiveName)) {
                    mEditReceiveName.requestFocus();
                } else if (!TextUtils.isEmpty(phoneNum) && !TextUtils.isEmpty(receiveName)) {
                    mEditReceiveName.requestFocus();
                    mBtnConfirm.setEnabled(true);
                }

                mLayoutStep02.setVisibility(View.VISIBLE);
                if (!mLayoutTransferRecentlyList.isShown())
                    showTransferRecentlyList();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 이체계좌 입력화면 숨기기
     * @param isClear 입력값 삭제 여부
     */
    private void hideStep02(boolean isClear) {
        mLayoutStep02.setVisibility(View.GONE);
        mViewStep02Bar.setVisibility(View.INVISIBLE);

        clearStep02(isClear);
    }

    /**
     * 이체계좌 입력값 삭제
     * @param isClear 입력값 삭제 여부
     */
    private void clearStep02(boolean isClear) {
        mTextDisplayTransferAmount.setText("");

        if (isClear) {
            mEditPhoneNum.setText("");
            mEditReceiveName.setText("");
        }
    }

    /**
     * 보낼 메세지란 보이기
     */
    private void showStep03() {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_03;
        mLayoutStep03.setVisibility(View.INVISIBLE);

        interActionShowStep03();
    }

    private void interActionShowStep03() {
        Animation animSlideIn = AnimationUtils.loadAnimation(this, R.anim.sliding_in_transfer_verify);
        mLayoutStep03.clearAnimation();
        mLayoutStep03.startAnimation(animSlideIn);
        mLayoutStep03.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutStep03.setVisibility(View.VISIBLE);
                mBtnConfirm.setVisibility(View.GONE);
                mBtnSendTransfer.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 보낼 메세지란 숨기기
     */
    private void hideStep03(boolean isClear) {
        mTransferStep = TRANSFER_ACCOUNT_STEP.STEP_02;
        //if (isClear)
        mLayoutStep03.setVisibility(View.GONE);

        mBtnConfirm.setVisibility(View.VISIBLE);
        mBtnSendTransfer.setVisibility(View.GONE);

        clearStep03(isClear);
    }

    private void clearStep03(boolean isClear) {
        if (isClear)
            mTextInputSendMsg.setText("");
    }

    /**
     * 최근/자주 이체 휴대폰 조회
     * @param isUpdate 조회결과만 update 여부
     */
    private void requestTransferRecentlyList(final boolean isUpdate) {
        if (mListTransferRecently != null && mListTransferRecently.size() > 0)
            mListTransferRecently.clear();

        Map param = new HashMap();
        param.put("INQ_DVCD", "2");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0050100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (!isUpdate)
                    showStep02();

                Logs.i("CMM0050100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        ITransferRecentlyInfo item = new ITransferRecentlyInfo(obj);
                        mListTransferRecently.add(item);
                    }

                    if (isUpdate) {
                        updateTransferPhoneList(mListTransferRecently);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 최근/자주 이체 휴대폰번호 리스트 표시
     */
    private void showTransferRecentlyList() {
        if (this.isFinishing())
            return;

        if (mITransferRecentlyAdapter != null)
            mITransferRecentlyAdapter = null;

        mITransferRecentlyAdapter = new ITransferRecentlyAdapter(TransferPhoneActivity.this, mListTransferRecently,
                new ITransferRecentlyAdapter.OnFavoriteItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        requestFavoritePhone(position);
                    }
                });
        mListViewTransferRecently.setAdapter(mITransferRecentlyAdapter);

        if (mITransferRecentlyAdapter.getCount() == 0) {
            mLayoutNoTransferRecently.setVisibility(View.VISIBLE);
            mListViewTransferRecently.setVisibility(View.GONE);
        } else {
            mLayoutNoTransferRecently.setVisibility(View.GONE);
            mListViewTransferRecently.setVisibility(View.VISIBLE);
        }

        mLayoutTransferRecentlyList.setVisibility(View.INVISIBLE);
        Animation animation = AnimationUtils.loadAnimation(TransferPhoneActivity.this, R.anim.sliding_in_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTransferRecentlyList.setVisibility(View.VISIBLE);

                if (mViewStep02Bar.getVisibility() == View.INVISIBLE) {
                    showStep02Bar();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최근/자주 이체 휴대폰번호 리스트 감추기
     */
    private void hideTransferRecentlyList(final boolean isShowStep03) {
        if (this.isFinishing())
            return;

        if (!mLayoutTransferRecentlyList.isShown()) {
            if (isShowStep03) {
                showStep03();

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mBtnConfirm.setVisibility(View.GONE);
                        mBtnSendTransfer.setVisibility(View.VISIBLE);
                    }
                }, 500);
            }

            return;
        }

        Animation animation = AnimationUtils.loadAnimation(TransferPhoneActivity.this, R.anim.sliding_out_bottom_sbi);
        mLayoutTransferRecentlyList.clearAnimation();
        mLayoutTransferRecentlyList.startAnimation(animation);
        mLayoutTransferRecentlyList.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mLayoutTransferRecentlyList.setVisibility(View.GONE);
                if (isShowStep03) {
                    showStep03();

                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mBtnConfirm.setVisibility(View.GONE);
                            mBtnSendTransfer.setVisibility(View.VISIBLE);
                        }
                    }, 500);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 최근/자주 이체 휴대폰번호 리스트 업데이트
     * @param dataList 업데이트할 리스트
     */
    private void updateTransferPhoneList(final ArrayList<ITransferRecentlyInfo> dataList) {
        if (mITransferRecentlyAdapter == null)
            return;

        mITransferRecentlyAdapter.setRecentlyArrayList(dataList);
    }

    private void showStep02Bar() {
        Animation animSlideDown = AnimationUtils.loadAnimation(this, R.anim.fade_in_transfer_step02_bar);
        mViewStep02Bar.clearAnimation();
        mViewStep02Bar.startAnimation(animSlideDown);
        mViewStep02Bar.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mViewStep02Bar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    /**
     * 휴대폰이체 수취인 조회
     */
    private void requestVerifyTransferPhone() {
        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo == null)
            return;

        if (mContactsInfo == null)
            return;

        String WTCH_ACNO = accountInfo.getACNO();
        String TLNO = mContactsInfo.getTLNO();
        String TRN_AMT = mContactsInfo.getTRN_AMT();

        if (TextUtils.isEmpty(TLNO))
            TLNO = "";

        Map param = new HashMap();
        param.put("TRTM_DVCD", "2");
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("CNTP_FIN_INST_CD", "");
        param.put("CNTP_BANK_ACNO", "");
        param.put("MNRC_CPNO", TLNO);
        param.put("TRN_AMT", TRN_AMT);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("TRA0010400A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    dismissProgressDialog();
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        dismissProgressDialog();
                        showErrorMessage(getString(R.string.msg_debug_no_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        dismissProgressDialog();
                        String msg = getString(R.string.common_msg_no_reponse_value_was);
                        showErrorMessage(msg);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        dismissProgressDialog();
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if ("EEFN0306".equalsIgnoreCase(errCode) ||
                                "EEFN0307".equalsIgnoreCase(errCode) ||
                                "EEIF0516".equalsIgnoreCase(errCode)) {
                            Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                            String url = WasServiceUrl.ERR0030100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                            String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode +
                                    "&" +  Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                            intent.putExtra(Const.INTENT_PARAM, param);

                            startActivity(intent);
                            finish();
                            return;
                        }


                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String SAME_AMT_TRNF_YN = object.optString("SAME_AMT_TRNF_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SAME_AMT_TRNF_YN)) {
                        dismissProgressDialog();
                        View.OnClickListener cancelClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                            }
                        };

                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                requestConfirmTransferPhone();
                            }
                        };

                        final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                        alertDialog.mPListener = okClick;
                        alertDialog.mNListener = cancelClick;
                        alertDialog.msg = "오늘 같은 분에게 동일한 금액을\n이체하셨습니다.\n정말 이체하시겠습니까?";
                        alertDialog.show();
                        return;
                    }

                    requestConfirmTransferPhone();

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 휴대폰이체 한도금액 조회
     */
    private void requestConfirmTransferPhone() {
        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo == null)
            return;

        if (mContactsInfo == null)
            return;

        String WTCH_ACNO = accountInfo.getACNO();
        String TRN_AMT = mContactsInfo.getTRN_AMT();

        Map param = new HashMap();
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("TRN_AMT", TRN_AMT);
        param.put("TRNF_DVCD", "4");

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0020300A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String TRNF_FEE = object.optString("TRNF_FEE");
                    if (TextUtils.isEmpty(TRNF_FEE))
                        mContactsInfo.setTRNF_FEE("0");
                    else
                        mContactsInfo.setTRNF_FEE(TRNF_FEE);

                    showConfirmPhoneTransfer();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 휴대폰이체 확인창 표시
     */
    private void showConfirmPhoneTransfer() {
        String withdrawAccount = mTextAccountName.getText().toString() + " " + mTextAccount.getText().toString();
        singleTransferDialog = new SlidingConfirmPhoneTransferDialog(this, withdrawAccount, mContactsInfo,
                new SlidingConfirmPhoneTransferDialog.FinishListener() {
                    @Override
                    public void OnCancelListener() {
                        hideSingleTransferDialog();
                    }

                    @Override
                    public void OnOKListener() {
                        hideSingleTransferDialog();
                        transferSignData(mContactsInfo);
                    }
                });
        singleTransferDialog.setCancelable(false);
        singleTransferDialog.show();
    }

    /**
     * 핀코드 입력창 표시
     * @param signData 전자서명된 이체정보
     */
    private void showOTPActivity(String signData) {
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);

        Intent intent = new Intent(this, PincodeAuthActivity.class);
        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        commonUserInfo.setSignData(signData);
        intent.putExtra(Const.INTENT_SERVICE_ID, "");
        intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
        intent.putExtra(Const.INTENT_TRN_CD, "EIF50001");
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    /**
     * 이체 요청
     * @param elecSrno 전자서명된 이체정보
     */
    private void requestTransfer(String elecSrno) {
        Map param = new HashMap();
        param.put("ELEC_SGNR_SRNO", elecSrno);

        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo == null)
            return;

        if (mContactsInfo == null)
            return;

        String WTCH_ACNO = accountInfo.getACNO();
        String TLNO = mContactsInfo.getTLNO();
        String FLNM = mContactsInfo.getFLNM();
        String TRN_AMT = mContactsInfo.getTRN_AMT();
        String DEPO_BNKB_MRK_NM = mContactsInfo.getDEPO_BNKB_MRK_NM();
        if (TextUtils.isEmpty(DEPO_BNKB_MRK_NM))
            DEPO_BNKB_MRK_NM = "";

        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("MNRC_TRGT_DEPR_NM", FLNM);
        param.put("MNRC_CPNO", TLNO);
        param.put("MNRC_ACNO", "");
        param.put("TRAM", TRN_AMT);
        param.put("MNRC_ACCO_MRK_CNTN", DEPO_BNKB_MRK_NM);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0020400A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    final JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String TRNF_KEY_VAL = object.optString("TRNF_KEY_VAL");
                    if (TextUtils.isEmpty(TRNF_KEY_VAL)) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String MSG_CNTN = object.optString("MSG_CNTN");

                    TransferPhoneResultInfo phoneResultInfo = new TransferPhoneResultInfo(object);
                    Intent intent = new Intent(TransferPhoneActivity.this, TransferPhoneCompleteActivity.class);
                    intent.putParcelableArrayListExtra(Const.INTENT_TRANSFER_PHONE_INFO, mListContacts);
                    intent.putExtra(Const.INTENT_TRANSFER_PHONE_RET, phoneResultInfo);
                    intent.putExtra(Const.INTENT_TRANSFER_PHONE_KAKAO_MSG, MSG_CNTN);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 즐겨찾기계좌관리 요청
     * @param position 최근 이체번호 리스트에서 선택된 계좌 위치치
     */
    private void requestFavoritePhone(int position) {
        ITransferRecentlyInfo accountInfo = (ITransferRecentlyInfo) mListTransferRecently.get(position);
        if (accountInfo == null)
            return;

        String FAVO_ACCO_YN = accountInfo.getFAVO_ACCO_YN();
        String MNRC_ACCO_DEPR_NM = accountInfo.getMNRC_ACCO_DEPR_NM();
        String MNRC_TLNO = accountInfo.getMNRC_TLNO();

        if (TextUtils.isEmpty(MNRC_TLNO))
            return;

        Map param = new HashMap();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN))
            param.put("REG_STCD", "1");
        else
            param.put("REG_STCD", "0");

        param.put("CNTP_ACCO_DEPR_NM", MNRC_ACCO_DEPR_NM);
        param.put("MNRC_CPNO", MNRC_TLNO);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }
                    requestTransferRecentlyList(true);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 이름 유효성 체크
     * @param str      체크 이름값
     * @return
     */
    private boolean checkVaildUserName(String str) {
        int derrcode = Utils.isKorean(str);

        if (derrcode != Const.STATE_VALID_STR_CHAR_ERR) {
            mTextNameMsg.setVisibility(View.GONE);
            mViewReceiveNameLine.setBackgroundColor(getResources().getColor(R.color.colorE7E7E7));
        } else {
            if (derrcode == Const.STATE_VALID_STR_CHAR_ERR)
                mTextNameMsg.setText(getString(R.string.msg_invalid_special_char));
            else
                mTextNameMsg.setText(getString(R.string.msg_invalid_name_length));
            return false;
        }
        return true;
    }

    /**
     * 이체 확인창 닫기
     */
    private void hideSingleTransferDialog() {
        if (singleTransferDialog != null && singleTransferDialog.isShowing()) {
            singleTransferDialog.dismiss();
            singleTransferDialog = null;
        }
    }

    /**
     * 세션동기화
     */
    private void requestSessionSync() {
        Map param = new HashMap();
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    checkVaildAuthMethod();
                    return;
                } else {
                    try {
                        JSONObject object = new JSONObject(ret);
                        if (object == null) {
                            Logs.e(getString(R.string.msg_debug_err_response));
                            Logs.e(ret);
                            checkVaildAuthMethod();
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            Logs.e(ret);
                            checkVaildAuthMethod();
                            return;
                        }

                        Logs.e("ret : " + ret);
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().syncLoginSession(ret);
                        LoginUserInfo.getInstance().setFakeFinderAllow(true);
                        checkVaildAuthMethod();
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
            }
        });
    }

    /**
     * 인증 수단 입력 횟수 오류 체크
     */
    private void checkVaildAuthMethod() {
        // 핀코드 오류 횟수 체크
        if (Prefer.getPinErrorCount(this) >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
            dismissProgressDialog();
            View.OnClickListener cancelClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TransferPhoneActivity.this, ReregLostPincodeActivity.class);
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.PINCODE_FORGET);
                    String accn = LoginUserInfo.getInstance().getODDP_ACCN();
                    String mbrno = LoginUserInfo.getInstance().getMBR_NO();

                    Logs.e("accn : " + accn);
                    Logs.e("mbrno : " + mbrno);

                    if (TextUtils.isEmpty(mbrno))
                        mbrno = "";
                    commonUserInfo.setMBRnumber(mbrno);
                    if (TextUtils.isEmpty(accn) || Integer.parseInt(accn) < 1)
                        commonUserInfo.setHasAccount(false);
                    else
                        commonUserInfo.setHasAccount(true);

                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivity(intent);
                    finish();
                }
            };

            final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
            alertDialog.mNListener = cancelClick;
            alertDialog.mPListener = okClick;
            alertDialog.msg = String.format(getString(R.string.msg_err_over_match_pin), Const.SSENSTONE_AUTH_COUNT_FAIL);
            alertDialog.mPBtText = getString(R.string.rereg);
            alertDialog.show();
            return;
        }

        final LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (TextUtils.isEmpty(SECU_MEDI_DVCD)) {
            checkMyAccount();
            return;
        }

        if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
            String OTP_STCD = loginUserInfo.getOTP_STCD();
            final String MOTP_EROR_TCNT = loginUserInfo.getMOTP_EROR_TCNT();
            if ("120".equalsIgnoreCase(OTP_STCD)) { // 사고 분실
                dismissProgressDialog();
                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CUS0030101.getServiceUrl();
                        intent.putExtra("url", url);
                        startActivity(intent);
                        finish();
                    }
                };

//                View.OnClickListener onLinkClock = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CRT0040101.getServiceUrl();
//                        String param = null;
//                        if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
//                            param = "TRTM_DVCD=ACDTLOCK";
//                        } else {
//                            param = "TRTM_DVCD=ACDT";
//                        }
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        intent.putExtra(Const.INTENT_PARAM, param);
//                        startActivity(intent);
//                        finish();
//                    }
//                };


                final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
//                alertDialog.mLinkListener = onLinkClock;
                alertDialog.mTopText = getString(R.string.msg_otp_accident_topmsg);
                alertDialog.msg = getString(R.string.msg_otp_accdent_contents);
                alertDialog.mPBtText = getString(R.string.msg_report_otp);
//                alertDialog.mLinkText = getString(R.string.msg_motp_issue);
                alertDialog.show();
                return;
            } else if("130".equalsIgnoreCase(OTP_STCD)){ // 폐기된 인증서
                dismissProgressDialog();
                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0220101.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };

                final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
                alertDialog.mTopText = getString(R.string.msg_otp_disposal_topmsg);
                alertDialog.msg = getString(R.string.msg_otp_disposal_contsnts);
                alertDialog.mPBtText = getString(R.string.msg_otp_other_disposal_termination);
                alertDialog.show();
                return;

            }else if ("140".equalsIgnoreCase(OTP_STCD)) { // 오류 횟수 초과
                dismissProgressDialog();
                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra("url", url);
                        startActivity(intent);
                        finish();
                    }
                };

                final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
                alertDialog.msg = getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = getString(R.string.msg_init_otp);
                alertDialog.show();
                return;
            } else if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                dismissProgressDialog();

                View.OnClickListener cancelClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                };

                View.OnClickListener okClick = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                        startActivity(intent);
                        finish();
                    }
                };

                final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClick;
                alertDialog.msg = getString(R.string.msg_otp_lost);
                alertDialog.mPBtText = getString(R.string.msg_init_otp);
                alertDialog.show();
                return;
            } else {
                checkMyAccount();
            }
        } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
            final View.OnClickListener cancelClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            final View.OnClickListener okClickDiffDevice = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                    String url = WasServiceUrl.CRT0040401.getServiceUrl();
                    String param = "TRTM_DVCD=" + 1;
                    intent.putExtra("url", url);
                    intent.putExtra(Const.INTENT_PARAM, param);
                    startActivity(intent);
                    finish();
                }
            };

            final View.OnClickListener okClickSameDevice = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(TransferPhoneActivity.this, WebMainActivity.class);
                    String url = WasServiceUrl.CRT0040501.getServiceUrl();
                    intent.putExtra("url", url);
                    startActivity(intent);
                    finish();
                }
            };

            String valueStr = loginUserInfo.getMOTP_END_YN();
            if (Const.BRIDGE_RESULT_YES.equals(valueStr)) {
                dismissProgressDialog();
                final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClickDiffDevice;
                alertDialog.msg = getString(R.string.msg_motp_expired);
                alertDialog.mPBtText = getString(R.string.common_reissue);
                alertDialog.show();
                return;
            }

            valueStr = loginUserInfo.getMOTP_EROR_TCNT();
            if (!TextUtils.isEmpty(valueStr) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
                dismissProgressDialog();
                final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                alertDialog.mNListener = cancelClick;
                alertDialog.mPListener = okClickDiffDevice;
                alertDialog.msg = getString(R.string.msg_motp_lost);
                alertDialog.mPBtText = getString(R.string.common_reissue);
                alertDialog.show();
                return;
            }

            showProgressDialog();
            MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                @Override
                public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                    if (!"0000".equals(resultCode)) {
                        dismissProgressDialog();
                        final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
                        alertDialog.mNListener = cancelClick;
                        String deviceId = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
                        String motpDeviceId = LoginUserInfo.getInstance().getMOTP_SMPH_DEVICE_ID();
                        if (TextUtils.isEmpty(deviceId) || TextUtils.isEmpty(motpDeviceId)) {
                            alertDialog.mPListener = okClickDiffDevice;
                        } else {
                            if (deviceId.equals(motpDeviceId)) {
                                alertDialog.mPListener = okClickSameDevice;
                            } else {
                                alertDialog.mPListener = okClickDiffDevice;
                            }
                        }
                        alertDialog.msg = getString(R.string.msg_motp_change_device);
                        alertDialog.mPBtText = getString(R.string.common_reissue);
                        alertDialog.show();
                    } else {
                        checkMyAccount();
                    }
                    return;
                }
            });
        } else {
            checkMyAccount();
        }
    }

    /**
     * 지연, 입금계좌 지정 서비스 여부에 따른 메세지 출력
     * @return boolean
     */
    private boolean checkVaildMenu() {
        String DLY_TRNF_SVC_ENTR_YN = LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DLY_TRNF_SVC_ENTR_YN)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
            alertDialog.mPListener = okClick;
            alertDialog.msg = getString(R.string.msg_no_use_desc_delay_transfer);
            alertDialog.mPBtText = getString(R.string.common_confirm);
            alertDialog.show();
            return false;
        }

        String DSGT_MNRC_ACCO_SVC_ENTR_YN = LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DSGT_MNRC_ACCO_SVC_ENTR_YN)) {
            View.OnClickListener okClick = new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            };

            final AlertDialog alertDialog = new AlertDialog(TransferPhoneActivity.this);
            alertDialog.mPListener = okClick;
            alertDialog.msg = getString(R.string.msg_no_use_desc_assign_transfer);
            alertDialog.mPBtText = getString(R.string.common_confirm);
            alertDialog.show();
            return false;
        }
        return true;
    }

    /**
     * 이체정보 전자 서명값 요청
     * @param contactsInfo 이체정보
     */
    private void transferSignData(ContactsInfo contactsInfo) {
        Map param = new HashMap();

        MyAccountInfo accountInfo = (MyAccountInfo) mListMyAccount.get(mSendAccountIndex);
        if (accountInfo == null)
            return;

        String WTCH_ACNO = accountInfo.getACNO();
        String FLNM = contactsInfo.getFLNM();
        String TLNO = contactsInfo.getTLNO();
        String TRN_AMT = contactsInfo.getTRN_AMT();
        String DEPO_BNKB_MRK_NM = contactsInfo.getDEPO_BNKB_MRK_NM();

        if (TextUtils.isEmpty(WTCH_ACNO) || TextUtils.isEmpty(FLNM) ||
                TextUtils.isEmpty(FLNM) || TextUtils.isEmpty(TLNO))
            return;

        param.put("TRN_TYCD", "2");
        param.put("WTCH_ACNO", WTCH_ACNO);
        param.put("MNRC_TRGT_DEPR_NM", FLNM);
        param.put("MNRC_CPNO", TLNO);
        param.put("TRAM", TRN_AMT);

        if (TextUtils.isEmpty(DEPO_BNKB_MRK_NM))
            DEPO_BNKB_MRK_NM = "";

        param.put("MNRC_ACCO_MRK_CNTN", DEPO_BNKB_MRK_NM);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0019900A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    String msg = getString(R.string.msg_debug_no_response);
                    showErrorMessage(msg);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        String msg = getString(R.string.msg_debug_err_response);
                        showErrorMessage(msg);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        //showErrorMessage(msg);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String ELEC_SGNR_VAL_CNTN = object.optString("ELEC_SGNR_VAL_CNTN");
                    if (!TextUtils.isEmpty(ELEC_SGNR_VAL_CNTN)) {
                        showOTPActivity(ELEC_SGNR_VAL_CNTN);
                    }
                } catch (JSONException e) {

                }
            }
        });
    }
}