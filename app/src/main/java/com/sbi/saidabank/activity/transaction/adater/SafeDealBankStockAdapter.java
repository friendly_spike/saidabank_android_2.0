package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: BankStockAdapter
 * Created by 950485 on 2019. 01. 02..
 * <p>
 * Description:아래에서 위로 올라오는 은행/증권회사 다이얼로그 adapter
 */

public class SafeDealBankStockAdapter extends BaseAdapter {
    private Context                  context;
    private ArrayList<RequestCodeInfo> listBank;
    private ArrayList<RequestCodeInfo> listStock;
    private String       packageName;

    private Const.BANK_STOCK_MODE bankStockMode;

    public SafeDealBankStockAdapter(Context context, ArrayList<RequestCodeInfo> listBank, ArrayList<RequestCodeInfo> listStock) {
        this.context = context;
        this.listBank = listBank;
        this.listStock = listStock;

        bankStockMode = Const.BANK_STOCK_MODE.BANK_MODE;

        packageName = context.getPackageName();
    }

    @Override
    public int getCount() {
        if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE)
            return listBank.size();
        else
            return listStock.size();
    }

    @Override
    public Object getItem(int position) {
        if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE)
            return listBank.get(position);
        else
            return listStock.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final SafeDealBankStockAdapter.BankStockViewHolder viewBankHolder;

        if (convertView == null) {
            viewBankHolder = new SafeDealBankStockAdapter.BankStockViewHolder();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_transfer_safe_deal_bankstock_list_item, null);

            viewBankHolder.imageBank = (ImageView) convertView.findViewById(R.id.imageview_bankstock);
            viewBankHolder.textBank = (TextView) convertView.findViewById(R.id.textview_bankstock);
            convertView.setTag(viewBankHolder);
        } else {
            viewBankHolder = (SafeDealBankStockAdapter.BankStockViewHolder) convertView.getTag();
        }


        RequestCodeInfo bankInfo = null;
        if (bankStockMode == Const.BANK_STOCK_MODE.BANK_MODE)
            bankInfo = listBank.get(position);
        else if (bankStockMode == Const.BANK_STOCK_MODE.STOCK_MODE)
            bankInfo = listStock.get(position);

        final String name = bankInfo.getCD_NM();
        final String code = bankInfo.getSCCD();

        if (bankInfo != null) {
            if (!TextUtils.isEmpty(name))
                viewBankHolder.textBank.setText(name);


            viewBankHolder.imageBank.setVisibility(View.VISIBLE);
            Uri iconUri = ImgUtils.getIconUri(context,code);
            if (iconUri != null)
                viewBankHolder.imageBank.setImageURI(iconUri);
            else
                viewBankHolder.imageBank.setImageResource(R.drawable.img_logo_xxx);
        }


        return convertView;
    }

//    private int getImageBank(String code) {
//        int resourceID = -1;
//
//        String resourcename = "img_logo_" + code;
//        if ("028".equalsIgnoreCase(code)) {
//            resourcename = "img_logo_000";
//        } else if ("032".equalsIgnoreCase(code)) {
//            resourcename = "img_logo_224";
//        }
//
//        resourceID = context.getResources().getIdentifier(resourcename, "drawable", packageName);
//
//        return resourceID;
//    }

    public void setBankStockMode(Const.BANK_STOCK_MODE mode){
        bankStockMode = mode;
    }

    public void setBankStockList(ArrayList<RequestCodeInfo> listBank, ArrayList<RequestCodeInfo> listStock){
        this.listBank.clear();
        this.listStock.clear();
        this.listBank = listBank;
        this.listStock = listStock;
        notifyDataSetChanged();
    }

    class BankStockViewHolder {
        ImageView    imageBank;
        TextView     textBank;
    }
}