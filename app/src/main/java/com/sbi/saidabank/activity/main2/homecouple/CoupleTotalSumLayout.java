package com.sbi.saidabank.activity.main2.homecouple;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

public class CoupleTotalSumLayout extends RelativeLayout {

    private TextView mTvAccountCnt;
    private TextView mTvSumAmount;

    public CoupleTotalSumLayout(Context context) {
        super(context);
        initUX(context);
    }

    public CoupleTotalSumLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_couple_total_sum, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mTvAccountCnt = layout.findViewById(R.id.tv_account_num);
        mTvSumAmount = layout.findViewById(R.id.tv_sum_amount);
        int radius = (int) Utils.dpToPixel(getContext(), 12);
        setBackground(GraphicUtils.getRoundCornerDrawable("#ff6450", new int[]{ radius, radius, radius, radius }));
        setVisibility(GONE);
    }

    public void setTotalSumInfo(int count, String amount) {
        mTvAccountCnt.setText(String.valueOf(count));
        mTvSumAmount.setText(amount);
        setVisibility(VISIBLE);
    }
}
