package com.sbi.saidabank.activity.main2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.google.android.material.tabs.TabLayout;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.main2.common.relay.RelayLayout;
import com.sbi.saidabank.activity.main2.common.tab.CustomTabLayout;
import com.sbi.saidabank.activity.main2.common.tab.OnCustomTabLayoutListener;
import com.sbi.saidabank.activity.main2.common.tab.TabsPagerMainAdapter;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.SlidingMain2PopupDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomViewPager;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;
import com.sbi.saidabank.solution.v3.V3Manager;
import com.sbi.saidabank.web.JavaScriptApi;

import java.util.ArrayList;

public class Main2Activity extends BaseActivity implements OnCustomTabLayoutListener, KeyboardDetectorRelativeLayout.IKeyboardChanged, DatePickerCallback {

    public static Main2Activity mMainActivity;
    private Context mContext;
    private CustomViewPager mViewPager;
    private CustomTabLayout mCustomTabLayout;
    private RelayLayout mRelayLayout;
    private TabsPagerMainAdapter mPagerFragmentApdater;
    private KeyboardDetectorRelativeLayout mLayoutRoot;
    private SlidingMain2PopupDialog mPopupDialog = null;
    private ArrayList<MainPopupInfo> mList;
    private boolean isPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2);
        mMainActivity = this;
        mContext = this;

        mLayoutRoot = findViewById(R.id.layout_root);
        mLayoutRoot.addKeyboardStateChangedListener(this);

        mCustomTabLayout = findViewById(R.id.layout_tab);

        mViewPager = (CustomViewPager) findViewById(R.id.viewpager);
        mViewPager.setPagingEnabled(false);

        mPagerFragmentApdater = new TabsPagerMainAdapter(this, getSupportFragmentManager());
        mPagerFragmentApdater.setCustomTabLayout(mCustomTabLayout);

        mViewPager.setOffscreenPageLimit(mPagerFragmentApdater.getCount());
        mViewPager.setAdapter(mPagerFragmentApdater);
        //Viewpager와 tab을 연결한다.
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mCustomTabLayout.getTabLayout()));

        mCustomTabLayout.setViewPager(mViewPager);

        mRelayLayout = findViewById(R.id.layout_relay);
        mRelayLayout.setTabLayout(mCustomTabLayout);
        mRelayLayout.setOnItemCountChangeListener(new RelayLayout.OnItemCountChangeListener() {
            @Override
            public void onItemCountChange(int count) {
                if (count == 0 && mCustomTabLayout.getSelectHomeMenuIdx() == 0) {
                    Fragment fragment = mPagerFragmentApdater.getItem(0);
                    if (fragment instanceof Main2MyAccountFragment) {
                        ((Main2MyAccountFragment) fragment).displayRelayTopMaringView(false);
                    }
                }
            }
        });

        if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL)) {
            String url = getIntent().getStringExtra(Const.INTENT_MAINWEB_URL);

            if (!TextUtils.isEmpty(url)) {
                Intent intent = new Intent(Main2Activity.this, WebMainActivity.class);
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                startActivity(intent);
            }
        }else if(getIntent().hasExtra("tabIndex")){
            int tabIndex = getIntent().getIntExtra("tabIndex",0);
            selectTab(tabIndex);
        }
    }

    @Override
    protected void onResume() {
        MLog.d();
        super.onResume();
        if (isPause) {
            isPause = false;
            int index = mViewPager.getCurrentItem();
            if (index == Const.MAIN2_INDEX_HOME) {
                mCustomTabLayout.showHomeMenu();
            }
        }
    }

    @Override
    protected void onPause() {
        MLog.d();
        super.onPause();
        isPause = true;
        dismissMainPopupDialog();
        dismissFakeAppDialog();
        //mRelayLayout.changeOriginViewHeight(0);
        //mRelayLayout.slidingUp(100);
    }

    @Override
    public void onBackPressed() {
        int index = mViewPager.getCurrentItem();
        if (index == Const.MAIN2_INDEX_HOME) {
            showLogoutDialog();
        } else {
            ((BaseFragment) mPagerFragmentApdater.getItem(index)).onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        MLog.d();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int index = mViewPager.getCurrentItem();
        ((BaseFragment) mPagerFragmentApdater.getItem(index)).fragmentForPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logs.e("Main2Activity - onActivityResult - requestCode : " + requestCode + " , resultCode : " + resultCode);
        int index = mViewPager.getCurrentItem();
        ((BaseFragment) mPagerFragmentApdater.getItem(index)).fragmentForResult(requestCode, resultCode, data);
    }

    @Override
    public void onKeyboardShown() {
        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) mViewPager.getLayoutParams();
        param.bottomMargin = 0;
        mViewPager.setLayoutParams(param);
        mCustomTabLayout.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        RelativeLayout.LayoutParams param = (RelativeLayout.LayoutParams) mViewPager.getLayoutParams();
        param.bottomMargin = (int) getResources().getDimension(R.dimen.main2_tab_height);
        mViewPager.setLayoutParams(param);
        mCustomTabLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDateSet(long date) {
        Intent intent = new Intent();
        intent.putExtra("selected_date", date);
        int index = mViewPager.getCurrentItem();
        if (index != Const.MAIN2_INDEX_HOME) {
            ((BaseFragment) mPagerFragmentApdater.getItem(index)).fragmentForResult(JavaScriptApi.API_250, RESULT_OK, intent);
        }
    }

    @Override
    public void onClickHomeMenu(int position) {
        Logs.e("onClickHomeMenu pos :" + position);
        if (position == 0) {
            //mRelayLayout.slidingDown(250);
        } else {
            mRelayLayout.slidingUp(250);
        }
        viewPagerUpdate();

    }

    @Override
    public void onClickTab(int position) {
        MLog.d();
        if (position != 0) {
            mRelayLayout.slidingUp(250);
        }

        //오픈뱅킹 재약정 기간이 되어 웹 팝업이 출력된 상태에서 다른 탭으로 이동시
        //해당 Flag를 false로 초기화 하여 다시 오픈뱅킹 탭으로 왔을때 웹 팝업이 출력되도록 처리한다.
        if(position != 0 && mCustomTabLayout.getSelectHomeMenuIdx()==2){
            Fragment fragment = mPagerFragmentApdater.getItem(0);
            if(fragment instanceof Main2OpenBankFragment){
                ((Main2OpenBankFragment)fragment).setPreOneMenthExpand(false);
            }
        }
    }

    public void viewPagerUpdate() {
        mPagerFragmentApdater.notifyDataSetChanged();
    }

    /**
     * 커플 해제 후 커플계좌 탭이 네이티브일때(Main2CoupleFragment) onResume에서 커플 상태를 조회하기 위한 함수.
     * JabvaScriptBridge의 301번에서 mainupdate param 이 들어올때 호출된다.
     */
    public void forceReloadCoupleState(boolean isFromWeb) {
        MLog.d();
        //조회해본다.
        if (mCustomTabLayout.getSelectTabIdx() == Const.MAIN2_INDEX_HOME && mCustomTabLayout.getSelectHomeMenuIdx() == 1) {
            Fragment fragment = mPagerFragmentApdater.getItem(Const.MAIN2_INDEX_HOME);
            if (fragment instanceof Main2CoupleFragment) {
                ((Main2CoupleFragment) fragment).setReloadCoupleState(isFromWeb);
            }

        }
    }

    /**
     * 로그아웃 안내 다이얼로그를 띄운다.
     */
    public void showLogoutDialog() {
        if (isFinishing())
            return;
        DialogUtil.alert(Main2Activity.this,
                "로그아웃",
                "취소",
                "로그아웃 하시겠습니까?",
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        goLogoutPage();
                    }
                },

                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                    }
                });
    }

    public void goLogoutPage() {
        if (LoginUserInfo.getInstance().isFinishedLogin()) {
            V3Manager.getInstance(Main2Activity.this).stopV3MobilePlus();
        } else {
            LoginUserInfo.getInstance().setFinishedLogin(true);
        }

        LoginUserInfo.clearInstance();
        LoginUserInfo.getInstance().setLogin(false);
        LogoutTimeChecker.getInstance(Main2Activity.this).autoLogoutStop();
        SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
        if (mApplicationClass != null)
            mApplicationClass.allActivityFinish(true);
        else
            finish();
        Intent intent = new Intent(Main2Activity.this, LogoutActivity.class);
        intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
        startActivity(intent);
    }

    /**
     * 웹에서 탭바를 직접 제어할때 호출
     *
     * @param index
     */
    public void selectTab(int index) {
        mViewPager.setCurrentItem(index, true);
    }

    /**
     * 푸시가 들어오면 탭바의 알림 이미지를 변경해준다.
     */
    public void showPushNotiFragment() {
        mCustomTabLayout.reachNewNotiMsg();
    }

    /**
     * 메인팝업을 띄우기 전에 악성앱 팝업 호출여부 체크 후 띄운다.
     *
     * @param arrayList
     */
    public void showPopupDialog(ArrayList<MainPopupInfo> arrayList) {
        this.mList = (DataUtil.isNotNull(arrayList) && !arrayList.isEmpty()) ? arrayList : null;
        dismissMainPopupDialog();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utils.isEnableClickEvent() && (DataUtil.isNull(mFakeAppsListDialog) || !mFakeAppsListDialog.isShowing())) {
                    showMainPopupDialog();
                }
            }
        }, 1000);
    }

    /**
     * 메인팝업을 띄운다.
     */
    public void showMainPopupDialog() {
        if (isFinishing()) return;

        if ((DataUtil.isNull(mList) || mList.isEmpty())
                || (mViewPager.getCurrentItem() != Const.MAIN2_INDEX_HOME || mCustomTabLayout.getSelectHomeMenuIdx() != 0))
            return;

        dismissMainPopupDialog();
        dismissFakeAppDialog();

        MLog.d();

        mPopupDialog = new SlidingMain2PopupDialog(Main2Activity.this, mList, new SlidingMain2PopupDialog.OnPopupListener() {
            @Override
            public void onImageClick(String url) {
                MLog.i("Image Click Url >> " + url);
                if (isFinishing())
                    return;
                if (DataUtil.isNull(url))
                    return;
                String newUrl = SaidaUrl.getBaseWebUrl() + "/" + url;
                if (!newUrl.contains(".act")) {
                    newUrl = newUrl + ".act";
                }
                Intent intent = new Intent(Main2Activity.this, WebMainActivity.class);
                intent.putExtra(Const.INTENT_MAINWEB_URL, newUrl);
                startActivity(intent);
            }

            @Override
            public void onShowComplete(boolean ret) {
                if (!ret) {
                    mPopupDialog = null;
                } else {
                    if (DataUtil.isNotNull(mList) && !mList.isEmpty()) {
                        mList.clear();
                        mList = null;
                    }
                }
            }
        });
        // 다이얼로그 내부에서 이미지 다운로드 완료후 show한다.
        //popupDialog.show();
    }

    /**
     * 메인팝업을 종료한다.
     */
    public void dismissMainPopupDialog() {
        MLog.d();
        if (DataUtil.isNotNull(mPopupDialog) && mPopupDialog.isShowing()) {
            mPopupDialog.dismiss();
            mPopupDialog = null;
        }
    }

    /**
     * 홈의 나의 계좌 화면에서 조회한 이어하기 데이타를 넘겨준다.
     *
     * @param arrayList
     */
    public void showRelayLayout(ArrayList<Main2RelayInfo> arrayList) {
        mRelayLayout.setRelayArrayList(arrayList);
    }

    /**
     * 내계좌 화면에서 스크롤시 홈 메뉴 반 내려가게 하는 함수
     *
     * @param isStart
     */
    public void changeHomeFragmentScrollState(boolean isStart) {
        if (isStart) {
            mCustomTabLayout.hideHomeMenu();
            mRelayLayout.setSizeSmall();
        } else {
            //슬라이드 하면서 다른 탭을 선택할 경우를 위해 추가
            if (mCustomTabLayout.getSelectTabIdx() == Const.MAIN2_INDEX_HOME) {
                mCustomTabLayout.showHomeMenu();
            }
        }
    }


}
