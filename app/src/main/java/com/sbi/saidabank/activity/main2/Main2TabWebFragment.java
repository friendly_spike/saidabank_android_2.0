package com.sbi.saidabank.activity.main2;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.espider.EspiderManager;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptApi;
import com.sbi.saidabank.web.JavaScriptBridge;
import com.selvasai.selvyocr.idcard.recognizer.SelvyIDCardRecognizer;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class Main2TabWebFragment extends BaseFragment implements EspiderManager.EspiderEventListener{


    private  String serviceUrl;

    private Context mContext;
    private FrameLayout mContainer;
    private ProgressBarLayout mProgressLayout;

    private JavaScriptBridge    mJsBridge;

    private int                 mFragmentType;

    //네이티브 띄웠다가 닫혔을때 웹페이지를 Reload하지 않고 그대로 둘 필요성일 있을때 사용하기 위해.
    private boolean             mForbidWebViewReLoad;//onResume에서 릴로드 금지


    public static final Main2TabWebFragment newInstance(String page_url, int fragmentType){
        Main2TabWebFragment f = new Main2TabWebFragment();

        Bundle bundle = new Bundle();
        bundle.putString("PAGE_URL",page_url);
        bundle.putInt("FRAGMENT_TYPE",fragmentType);

        f.setArguments(bundle);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = getContext();

        if(getArguments() != null){
            serviceUrl = getArguments().getString("PAGE_URL");
            mFragmentType = getArguments().getInt("FRAGMENT_TYPE");
        }else{
            serviceUrl = WasServiceUrl.MAI0030100.getServiceUrl();
            mFragmentType = Const.MAIN2_INDEX_GOODS;
        }

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main2_web, container, false);

        mContainer = rootView.findViewById(R.id.container_webview);
        WebView webView = getCurrentWebView();
        webView.setTag(R.id.TAG_URL,serviceUrl);
        webView.setTag(R.id.TAG_LOAD_STATE,false);
        webView.setTag(R.id.TAG_PARAMS,"");

        BaseWebClient client = new BaseWebClient((Activity)mContext, this);
        webView.setWebViewClient(client);
        mJsBridge = new JavaScriptBridge((Activity)mContext, mContainer, this);
        webView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);
        BaseWebChromeClient chromeClient = new BaseWebChromeClient((Activity)mContext, mContainer, mJsBridge);
        webView.setWebChromeClient(chromeClient);

        mProgressLayout = rootView.findViewById(R.id.ll_progress);

        if( mFragmentType == Const.MAIN2_INDEX_GOODS
            || mFragmentType == Const.MAIN2_INDEX_MENU){

            webView.loadUrl(serviceUrl);
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

        if(getActivity() != null && !getActivity().isFinishing()){
            //앱이 백그라운드로 내려갔다 올라올때 어플리케이션에서 상태를 알수 있기에.. 추가함.
            boolean isNowBackGround = ((SaidaApplication)getActivity().getApplication()).getIsNowBackGround();
            if(isNowBackGround){
                //앱이 백그라운드에서 올라올 경우엔 웹뷰 새로 로드하지 않는다.
                mForbidWebViewReLoad = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        MLog.d();
        Logs.e("mFragmentType : " + mFragmentType);
        Logs.e("mForbidWebViewReLoad : " + mForbidWebViewReLoad);


        WebView webView = getCurrentWebView();
        String url = (String) webView.getTag(R.id.TAG_URL);
        String params = (String) webView.getTag(R.id.TAG_PARAMS);

        boolean pageLoadFinish = false;
        Object object = getCurrentWebView().getTag(R.id.TAG_LOAD_STATE);
        if(object != null){
            pageLoadFinish = (boolean)object;
        }
        Logs.e("url : " + url);
        Logs.e("params : " + params);
        Logs.e("pageLoadFinish : " + pageLoadFinish);

        if(!pageLoadFinish){
            showProgress();
            mContainer.setVisibility(View.GONE);
            if(TextUtils.isEmpty(params)){
                webView.loadUrl((String)webView.getTag(R.id.TAG_URL));
            }else{
                webView.postUrl(url, params.getBytes());
            }
            mForbidWebViewReLoad = false;
        }else{
            //아래 변수 설명은 선언부에 적어놓았음.
            if(mForbidWebViewReLoad){
                mForbidWebViewReLoad = false;
                return;
            }

            if(mFragmentType == Const.MAIN2_INDEX_MY){
                showProgress();
                mContainer.setVisibility(View.GONE);
                if(TextUtils.isEmpty(params)){
                    webView.loadUrl(url);
                }else{
                    webView.postUrl(url, params.getBytes());
                }
            }else if(mFragmentType == Const.MAIN2_INDEX_NOTI){
                //새로운 알림이 없으면 그냥 리턴.
                if(!Prefer.getFlagShowFirstNotiMsgBox(getContext())){
                    return;
                }
                showProgress();
                mContainer.setVisibility(View.GONE);
                if(TextUtils.isEmpty(params)){
                    webView.loadUrl(url);
                }else{
                    webView.postUrl(url, params.getBytes());
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        MLog.d();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mJsBridge.destroyJavaScriptBrigdge();
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        boolean loadState = false;
        Object object = getCurrentWebView().getTag(R.id.TAG_LOAD_STATE);
        if(object != null){
            loadState = (boolean)object;
        }

        String url = (String)getCurrentWebView().getTag(R.id.TAG_URL);
        Logs.e("loadState : " + loadState);
        Logs.e("url : " + url);
        if(loadState && !TextUtils.isEmpty(url) && url.contains(SaidaUrl.getBaseWebUrl()))
            mJsBridge.callJavascriptFunc("co.app.from.back", null);
        else{
            //((Main2Activity)mContext).showLogoutDialog();
            closeWebView();
        }
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        mProgressLayout.show();
    }

    @Override
    public void dismissProgress() {
        mProgressLayout.hide();

        if (mContainer.getVisibility() != View.VISIBLE)
            mContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void openCamera() {
//        If you need this function, you can erase this coment and add code to fragmentForResult.

//        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//        if (intent.resolveActivity(getContext().getPackageManager()) != null) {
//
//            mTargetPhotoURI = ImgUtils.createNewImageFileName(getContext(),"profile");
//            if(DataUtil.isNotNull(mTargetPhotoURI)){
//                intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
//                startActivityForResult(intent, Const.REQUEST_PROFILE_CAMERA);
//            }
//        }
    }

    /**
     * Global menu를 유지한채 웹뷰를 뛰울때 웹에서 302번호 api호출시.Main에서만 주로 처리한다.
     * @param url
     */
    @Override
    public void loadUrl(String url, String params) {
        BaseWebView newWebView = new BaseWebView(mContext);
        int webViewCnt = mContainer.getChildCount();
        newWebView.setTag(webViewCnt + 1);

        BaseWebClient client = new BaseWebClient(getActivity(),mJsBridge.getWebActionListener());
        newWebView.setWebViewClient(client);

        BaseWebChromeClient chromeClient = new BaseWebChromeClient(getActivity(), mContainer, mJsBridge);
        newWebView.setWebChromeClient(chromeClient);
        newWebView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);
        newWebView.setTag(R.id.TAG_URL,url);
        newWebView.setTag(R.id.TAG_LOAD_STATE,false);

        if (DataUtil.isNull(params)) {
            newWebView.setTag(R.id.TAG_PARAMS,"");
            newWebView.loadUrl(url);
        } else {
            newWebView.setTag(R.id.TAG_PARAMS,params);
            newWebView.postUrl(url, params.getBytes());
        }

        mContainer.addView(newWebView);
    }

    @Override
    public void refreshWebView() {
        //여기에 refresh가 들어오면 closeWebView와 같이 동작하도록 수정.
        closeWebView();
    }

    @Override
    public void closeWebView() {
        MLog.d();
        int webViewCnt = mContainer.getChildCount();
        if(webViewCnt <= 1) {
            ((Main2Activity)mContext).showLogoutDialog();
        }else{
            WebView webView = getCurrentWebView();
            //두번째 웹뷰 우측으로 사라지도록.
            Animation animMoveMoneyNum = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_right);
            webView.clearAnimation();
            webView.startAnimation(animMoveMoneyNum);
            webView.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    int webViewCnt = mContainer.getChildCount();
                    mContainer.removeViewAt(webViewCnt-1);

                    webViewCnt = mContainer.getChildCount();
                    if(webViewCnt == 1 && mFragmentType == Const.MAIN2_INDEX_MY){
//                        WebView webView = (WebView)mContainer.getChildAt(0);
//                        webView.loadUrl((String)webView.getTag(R.id.TAG_URL));
                        mJsBridge.callJavascriptFunc("co.app.from.webviewClose", null);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });



        }
    }

    @Override
    public void setCurrentPage(String url) {
        Logs.d("setCurrentPage : " + url);
        if(!TextUtils.isEmpty(url))
            getCurrentWebView().setTag(R.id.TAG_URL,url);
    }

    @Override
    public void setFinishedPageLoading(boolean flag) {
        Logs.d("setFinishedPageLoading : " + flag);
        getCurrentWebView().setTag(R.id.TAG_LOAD_STATE,flag);
    }

    @Override
    public void onWebViewSSLError(String url, String errorCd,final SslErrorHandler handler) {
        DialogUtil.alert(mContext, R.string.common_msg_error_ssl_cert, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.proceed();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.cancel();
            }
        });
    }

    @Override
    public void getProfileImage(JSONObject json,String workType) throws JSONException{
        MLog.d();

        if(getActivity() == null || getActivity().isFinishing()) return;

        MLog.i("workType >> " + workType);
        String[] perList01 = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        String[] perList02 = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
        if (workType.equals("album")) {
            if (PermissionUtils.checkPermission(getActivity(), perList01, Const.REQUEST_PROFILE_ALBUM)) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                startActivityForResult(intent, Const.REQUEST_PROFILE_ALBUM);
            } else {
                if (DataUtil.isNotNull(json))
                    mJsBridge.setJsonObjecParam(JavaScriptApi.API_907, json.toString());
            }
        } else if (workType.equals("camera")) {
            if (PermissionUtils.checkPermission(getActivity(), perList02, Const.REQUEST_PROFILE_CAMERA)) {
                openCamera();
            } else {
                if (DataUtil.isNotNull(json))
                    mJsBridge.setJsonObjecParam(JavaScriptApi.API_907, json.toString());
            }
        } else if (workType.equals("image")) {
            if (PermissionUtils.checkPermission(getActivity(), perList01, Const.REQUEST_PROFILE_IMAGE)) {
                File file = new File(Prefer.getProfileImagePath(mContext));
                if (file.exists()) {
                    int size = (int) file.length();
                    byte[] imgData = new byte[size];
                    try {
                        BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
                        buf.read(imgData, 0, imgData.length);
                        buf.close();
                    } catch (IOException e) {
                        MLog.e(e);
                    }
                    JSONObject jsonObj = new JSONObject();
                    if (DataUtil.isNotNull(imgData)) {
                        jsonObj.put("img_data", Base64.encodeToString(imgData, Base64.DEFAULT));
                    } else {
                        jsonObj.put("result", false);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
                } else {
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("result", false);
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_907), jsonObj);
                }
            } else {
                if (DataUtil.isNotNull(json))
                    mJsBridge.setJsonObjecParam(JavaScriptApi.API_907, json.toString());
            }
        }
    }

    private WebView getCurrentWebView(){
        int webViewCnt = mContainer.getChildCount();

        if(webViewCnt < 1){
            BaseWebView webView = new BaseWebView(getContext());
            webView.setTag(R.id.TAG_URL,serviceUrl);
            webView.setTag(R.id.TAG_LOAD_STATE,false);
            webView.setTag(R.id.TAG_PARAMS,"");
            mContainer.addView(webView);
            return webView;
        }

        return (WebView)mContainer.getChildAt(webViewCnt-1);
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);

        //메인에서 웹 인터페이스로 작업한 것은 Reload하지 않도록 처리한다.
        mForbidWebViewReLoad = true;

        Logs.e("fragmentForResult - requestCode : " + requestCode);
        Logs.e("fragmentForResult - resultCode : " + resultCode);

        /**
         * 로시스 라이브러리 업체의 카메라 화면의 리턴값이 1로 되어 있다.
         */
        if (requestCode == JavaScriptApi.API_800) {
            if (resultCode == 1) {
                // 신분증 이미지 영역
                byte[] imageOCR = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
                imageOCR =  SelvyIDCardRecognizer.decrypt(imageOCR, Const.ENCRYPT_KEY); // 이미지 복호화 추가

                JSONObject jsonObj = new JSONObject();
                try {
                    jsonObj.put("encode_image", Base64.encodeToString(imageOCR, Base64.DEFAULT));
                } catch (JSONException e) {
                    Logs.printException(e);
                }
                Logs.e(jsonObj.toString());
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), jsonObj);
            }else{
                mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_800,true), null);
            }
        } else if (requestCode == JavaScriptApi.API_301 || requestCode == JavaScriptApi.API_302 || requestCode == JavaScriptApi.API_400) {
            mJsBridge.callJavascriptFunc("co.app.from.webviewClose", null);
        }

        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case JavaScriptApi.API_200: {//1줄 보안키패드
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("input_id", data.getStringExtra(Const.INTENT_TRANSKEY_INPUTID1));
                        jsonObj.put("secure_data", data.getStringExtra(Const.INTENT_TRANSKEY_SECURE_DATA1));
                        jsonObj.put("input_length", data.getStringExtra(Const.INTENT_TRANSKEY_DUMMY_DATA1).length());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }

                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_201:  //dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:  //dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:  //타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:  //타기관OTP 비밀번호 인증
                case JavaScriptApi.API_205:  //모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:  //모바일OTP 비밀번호 인증
                {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                        jsonObj.put("secure_data", commonUserInfo.getSecureData());
                        if(requestCode == JavaScriptApi.API_202 || requestCode == JavaScriptApi.API_206)
                            jsonObj.put("forget_auth_no", commonUserInfo.isOtpForgetAuthNo());
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }
                case JavaScriptApi.API_207:  //타기관OTP 비밀번호 인증 - 결과값 리턴
                case JavaScriptApi.API_208: {  //모바일OTP 비밀번호 인증 - 결과값 리턴
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                }
                case JavaScriptApi.API_250: {
                    final Calendar calendar = Calendar.getInstance();
                    calendar.setTimeInMillis(data.getLongExtra("selected_date", 0));

                    int inputYear = calendar.get(Calendar.YEAR);
                    int inputMonth = calendar.get(Calendar.MONTH) + 1;
                    int inputDay = calendar.get(Calendar.DAY_OF_MONTH);

                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put("date", String.format("%04d", inputYear) + "" + String.format("%02d", inputMonth) + "" + String.format("%02d", inputDay));
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(JavaScriptApi.API_250), jsonObj);
                    break;
                }

                case JavaScriptApi.API_500: { // PIN 전자서명
                    if(data != null) {
                        CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                        String result = commonUserInfo.getResultMsg();
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put("result", Const.BRIDGE_RESULT_TRUE);
                            jsonObj.put("elec_sgnr_srno", commonUserInfo.getSignData());
                        } catch (JSONException e) {
                            Logs.printException(e);
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    }
                    break;
                }

                case JavaScriptApi.API_501: { // 지문 전자서명
                    break;
                }

                case JavaScriptApi.API_502: { // 휴대폰 본인인증

                    try {
                        JSONObject jsonObj = new JSONObject();
                        jsonObj.put("ci", "");
                        jsonObj.put("di", "");
                        jsonObj.put("cmcm_dvcd", "");
                        jsonObj.put("cpno", "");
                        if (data != null) {
                            String diNo = null;
                            String ciNo = null;

                            if (data.hasExtra("DI_NO")) {
                                diNo = data.getStringExtra("DI_NO");
                                jsonObj.put("di", diNo);
                            }
                            if (data.hasExtra("CI_NO")) {
                                ciNo = data.getStringExtra("CI_NO");
                                jsonObj.put("ci", ciNo);
                            }
                            if (data.hasExtra("CMCM_DVCD")) {
                                ciNo = data.getStringExtra("CMCM_DVCD");
                                jsonObj.put("cmcm_dvcd", ciNo);
                            }
                            if (data.hasExtra("CPNO")) {
                                ciNo = data.getStringExtra("CPNO");
                                jsonObj.put("cpno", ciNo);
                            }
                        } else {
                            Logs.showToast(mContext, "본인인증 실패. data null");
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    break;
                }

                case JavaScriptApi.API_503 : { // 타행계좌인증
                    try {
                        JSONObject jsonObj = new JSONObject();
                        if (data != null) {
                            String accountNum = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_NUMBER);
                            String bankCode = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_BANK_CODE);
                            String custName = data.getStringExtra(Const.INTENT_VERIFY_ACCOUNT_CUST_NAME);
                            jsonObj.put(Const.BRIDGE_RESULT_KEY, data.getStringExtra(Const.BRIDGE_RESULT_KEY));
                            jsonObj.put("account_num", accountNum);
                            jsonObj.put("bank_cd", bankCode);
                            jsonObj.put("deposit_cust_name", custName);
                            mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                        }
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                }
                break;

                // 스크래핑 : 건강보험납부내역, 건강보험 자격득실
                case JavaScriptApi.API_600:
                case JavaScriptApi.API_601:
                    boolean isCertOnly = data.getBooleanExtra(Const.INTENT_CERT_NO_SCRAPING, false);
                    if (isCertOnly) {
                        JSONObject jsonObj = new JSONObject();
                        try {
                            jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_COMPLETE);
                            jsonObj.put("cn", data.getStringExtra(Const.INTENT_CERT_ISSUER_DN));
                            jsonObj.put("dn", data.getStringExtra(Const.INTENT_CERT_SUBJECT_DN));
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                        mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    } else {
                        JSONObject jsonObj = new JSONObject();
                        String callBackName = mJsBridge.getCallBackFunc(requestCode, true);
                        EspiderManager spiderManager = EspiderManager.getInstance(getActivity());
                        spiderManager.setEspiderEventListener(this);
                        if (DataUtil.isNotNull(data) &&
                                spiderManager.startEspider(
                                        requestCode,
                                        callBackName,
                                        data.getStringExtra(Const.INTENT_CERT_PASSWORD),
                                        data.getStringExtra(Const.INTENT_CERT_PATH),
                                        data.getStringExtra(Const.INTENT_CERT_IDDATA),
                                        data.getStringExtra(Const.INTENT_CERT_SERVICEDATA),
                                        data.getStringExtra(Const.INTENT_CERT_ISSUER_DN),
                                        data.getStringExtra(Const.INTENT_CERT_SUBJECT_DN)) == Const.ESPIDER_TYPE_RETURN_SUCCESS) {
                            // Scraping Start
                            try {
                                jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_START);
                            } catch (JSONException e) {
                                MLog.e(e);
                            }

                        } else {
                            // Scraping Fail
                            try {
                                jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                            } catch (JSONException e) {
                                MLog.e(e);
                            }

                        }
                        mJsBridge.callJavascriptFunc(callBackName, jsonObj);
                    }
                    break;

                case JavaScriptApi.API_903: { // PDF약관보기
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_TRUE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }

                case JavaScriptApi.API_931: {
                    JSONObject jsonObj = new JSONObject();
                    if (DataUtil.isNotNull(data)) {
                        try {
                            jsonObj.put("TLNO", data.getStringExtra("TLNO"));
                            jsonObj.put("MBR_NO", data.getStringExtra("MBR_NO"));
                            jsonObj.put("FLNM", data.getStringExtra("FLNM"));
                        } catch (JSONException e) {
                            MLog.e(e);
                        }
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                }
                default:
                    break;
            }
        } else if (resultCode == RESULT_CANCELED) {
            switch (requestCode) {
                case JavaScriptApi.API_201:          // dot타입 보안키패드 호출 등록
                case JavaScriptApi.API_202:          // dot타입 보안키패드 호출 인증
                case JavaScriptApi.API_203:          // 타기관OTP 비밀번호 등록
                case JavaScriptApi.API_204:          // 타기관OTP 비밀번호 인증 - 비밀번호 리턴
                case JavaScriptApi.API_205:          // 모바일OTP 비밀번호 등록
                case JavaScriptApi.API_206:          // 모바일OTP 비밀번호 인증 - 비밀번호 리턴
                case JavaScriptApi.API_207:          // 타기관OTP 비밀번호 인증 - 결과값 리턴
                case JavaScriptApi.API_208:          // 모바일OTP 비밀번호 인증 - 결과값 리턴
                case JavaScriptApi.API_500:          // Pincode 인증
                case JavaScriptApi.API_501:          // 지문 인증
                case JavaScriptApi.API_502:          // 휴대폰 본인인증
                case JavaScriptApi.API_503:          // 타행계좌인증
                {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }
                case JavaScriptApi.API_903:          // PDF약관보기
                {
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode,true), jsonObj);
                    break;
                }
                case JavaScriptApi.API_600:
                case JavaScriptApi.API_601: { // Javainterface 에서 600, 601번 호출하여 인증서 리스트 취소 시 콜백
                    JSONObject jsonObj = new JSONObject();
                    try {
                        jsonObj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_CANCEL);
                    } catch (JSONException e) {
                        Logs.printException(e);
                    }
                    mJsBridge.callJavascriptFunc(mJsBridge.getCallBackFunc(requestCode, true), jsonObj);
                    break;
                }
                default:
                    break;
            }
        }
    }

    @Override
    public void onFinishEspider(String callbackName, JSONObject object) {
        MLog.d();
        // Javainterface 에서 600, 601번 호출되면 작업 후 값을 만들어 넘겨준다.
        if (EspiderManager.isEngineEnabled() && DataUtil.isNotNull(mJsBridge)) {
            mJsBridge.callJavascriptFunc(callbackName, object);
        }
    }

    @Override
    public void onStatusEspider(String callbackName, int status, String msg) {
        MLog.i("onStatusEspider status[" + status + "] msg[" + msg + "]");
    }
}
