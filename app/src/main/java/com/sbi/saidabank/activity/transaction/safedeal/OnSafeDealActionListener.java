package com.sbi.saidabank.activity.transaction.safedeal;

import android.view.View;

import org.json.JSONObject;

public interface OnSafeDealActionListener {

    void onActionClose();

    void onActionOpenBank();
    void onActionSearchPhoneNum();
    void onSearchAddress();

    void onActionNext(View v,Object obj);
    void onActionBack(View v);
    void onAnimationEnd(View v, SlideBaseLayout.AnimationKind aniKind);

    boolean onCheckHttpError(String ret,boolean finish);

    void showProgressDialog();
    void dismissProgressDialog();

    void showErrorMessage(String msg);
    void showCommonErrorDialog(String msg, String codeRet, String scrnId, JSONObject object, boolean isShownRetryText);


}
