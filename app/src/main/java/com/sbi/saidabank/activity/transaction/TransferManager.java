package com.sbi.saidabank.activity.transaction;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.define.datatype.transfer.TransferVerifyInfo;

import java.util.ArrayList;

/**
 * TransferManager : 수취목록 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TransferManager {

    private static TransferManager instance = null;
    private ArrayList<TransferVerifyInfo> listVerifyTransferInfo = new ArrayList<>();
    private boolean isNotRemove = false;

    public static TransferManager getInstance() {
        if (instance == null) {
            synchronized (TransferManager.class) {
                if (instance == null) {
                    instance = new TransferManager();
                }
            }
        }
        return instance;
    }

    private TransferManager() {
    }

    public void setData(ArrayList<TransferVerifyInfo> listVerifyTransferInfo) {
        this.listVerifyTransferInfo = listVerifyTransferInfo;
    }

    public ArrayList<TransferVerifyInfo> getData() {
        return listVerifyTransferInfo;
    }

    public TransferVerifyInfo get(int position) {
        return listVerifyTransferInfo.get(position);
    }

    public void add(TransferVerifyInfo transferInfo) {
        listVerifyTransferInfo.add(transferInfo);
    }

    public void add(int position, TransferVerifyInfo transferInfo) {
        listVerifyTransferInfo.add(position, transferInfo);
    }

    public void remove(int position) {
        listVerifyTransferInfo.remove(position);
    }

    public int size() {
        return listVerifyTransferInfo.size();
    }

    public void clear() {
        listVerifyTransferInfo.clear();
    }

    public void clearAll() {
        listVerifyTransferInfo.clear();
        isNotRemove = false;
    }

    /**
     * 화면에서 세션에 있는 마지막 값 삭제 안할지 여부
     */
    public boolean getIsNotRemove() {
        return isNotRemove;
    }

    /**
     * 화면에서 세션에 있는 마지막 값 삭제 안할지 여부 설정
     *
     * @param isNotRemove 삭제안할지 여부
     */
    public void setIsNotRemove(boolean isNotRemove) {
        this.isNotRemove = isNotRemove;
    }

    /**
     * 총 이체금액 확인
     *
     * @return 총 이체금액
     */
    public double getTotalTransferAmount() {
        Double dAmount = 0.;
        for (TransferVerifyInfo item : listVerifyTransferInfo) {
            if (DataUtil.isNull(item)) {
                continue;
            }
            String amount = item.getTRN_AMT();
            if (DataUtil.isNull(amount)) {
                continue;
            }
            dAmount += Double.parseDouble(amount);
        }
        return dAmount;
    }
}
