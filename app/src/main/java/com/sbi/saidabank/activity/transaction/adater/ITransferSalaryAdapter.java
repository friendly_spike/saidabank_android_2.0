package com.sbi.saidabank.activity.transaction.adater;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.raonsecure.license.a.E;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.ITransferSalaryActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSalaryActionListener;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

/**
 * 받는사람 검색 어댑터
 */
public class ITransferSalaryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final String ERROR_NOTI_TEXT_COLOR = "#ff6450";
    public static final String NORMAL_NOTI_TEXT_COLOR = "#a5a5a5";
    public static final String ERROR_AMOUNT_TEXT_COLOR = "#ff6450";
    public static final String NORMAL_AMOUNT_TEXT_COLOR = "#000000";

    public static final String ERROR_LINE_COLOR = "#ff6450";
    public static final String NORMAL_LINE_COLOR = "#e4e9ec";
    public static final String FOCUS_LINE_COLOR  = "#000000";

    public static final String NORMAL_BG_STROKE_COLOR = "#e4e9ec";
    public static final String FOCUS_BG_STROKE_COLOR = "#293542";
    public static final String ERROR_BG_STROKE_COLOR = "#ff6450";

    private Activity mContext;
    private OnITransferSalaryActionListener mListener;

    public ITransferSalaryAdapter(Activity context, OnITransferSalaryActionListener listener) {
        mContext = context;
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ViewHolder.newInstance(parent,mListener);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ((ViewHolder)holder).onBind(mContext,position);
    }

    @Override
    public int getItemCount() {
        return ITransferSalayMgr.getInstance().getSalayArrayList().size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private Context mContext;
        private OnITransferSalaryActionListener mListener;

        private View mItemView;
        private LinearLayout mLayoutBody;
        private ImageView mIvShadow;
        private View mVTopMargin;
        //계좌부분
        private ImageView mIvBankIcon;
        private ImageView mIvDeleteIcon;
        private TextView mTvAccountName;
        private TextView mTvAccountNo;

        //금액부분
        private LinearLayout mLayoutAmount;
        private TextView mTvSendText;
        private TextView mTvAmount;
        private EditText mEtAmount;
        private TextView mTvWon;
        private ImageView mIvEraseAmountText;
        private View    mVAmountLine;

        //경고노티부분
        private RelativeLayout mLayoutErrorNotice;
        private ImageView mIvErrorIcon;
        private TextView  mTvErrorText;

        private ImageView mIvNextArrow;
        private LottieAnimationView mLottieArrow;


        private View mViewComplete;

        private String mInputAmount;
        private String mInputAmountNoComma;

        public static ViewHolder newInstance(ViewGroup parent,OnITransferSalaryActionListener listener) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_itransfer_salary_list_item_account, parent, false);
            return new ViewHolder(parent.getContext(),itemView,listener);
        }

        public ViewHolder(Context context,@NonNull View itemView,OnITransferSalaryActionListener listener) {
            super(itemView);
            Logs.e("ViewHolder Create ----------------");
            mContext = context;
            mListener = listener;

            mItemView = itemView;
            mLayoutBody = itemView.findViewById(R.id.layout_body);
            mIvShadow = itemView.findViewById(R.id.iv_shadow);
            //mIvShadow.setVisibility(GONE);
            mVTopMargin  = itemView.findViewById(R.id.v_top_margin);
            mIvBankIcon = itemView.findViewById(R.id.iv_bank_icon);
            mIvDeleteIcon = itemView.findViewById(R.id.iv_delete_icon);
            mTvAccountName = itemView.findViewById(R.id.tv_account_name);
            mTvAccountNo = itemView.findViewById(R.id.tv_account_num);

            mTvSendText = itemView.findViewById(R.id.tv_send_text);
            mLayoutAmount  = itemView.findViewById(R.id.layout_amount);
            mTvAmount  = itemView.findViewById(R.id.tv_amount);
            mEtAmount  = itemView.findViewById(R.id.et_amount);

            //리스너들은 생성할때 설정해둔다.
            setAmountListener();

            mTvWon    = itemView.findViewById(R.id.tv_won);
            mIvEraseAmountText  = itemView.findViewById(R.id.iv_erase_amount_text);

            mLayoutErrorNotice = itemView.findViewById(R.id.layout_notice);
            mLayoutErrorNotice.setVisibility(GONE);
            mIvErrorIcon  = itemView.findViewById(R.id.iv_error_icon);
            mTvErrorText = itemView.findViewById(R.id.tv_error_text);

            mVAmountLine = itemView.findViewById(R.id.v_amount_line);

            mIvNextArrow  = itemView.findViewById(R.id.iv_next_arrow);
            mLottieArrow  = itemView.findViewById(R.id.lottie_icon);

            mViewComplete = itemView.findViewById(R.id.v_complete);
            mViewComplete.setVisibility(GONE);

            mLayoutBody.getViewTreeObserver()
                    .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {

                            //레이아웃의 높이를 재조정해둔다.
                            reSizeShadowHeight(false);

                            //금액라인
                            //drawAmountLine(NORMAL_LINE_COLOR);
                            ViewGroup.LayoutParams lineParams = mVAmountLine.getLayoutParams();
                            lineParams.width = mLayoutAmount.getWidth();
                            mVAmountLine.setLayoutParams(lineParams);

                            mLayoutBody.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    });
        }

        private void reSizeShadowHeight(boolean focus){
            ViewGroup.LayoutParams params = mIvShadow.getLayoutParams();
            params.width = mLayoutBody.getWidth() + (int)Utils.dpToPixel(mContext,48);
            if(focus){
                params.height = mLayoutBody.getHeight() + (int)Utils.dpToPixel(mContext,40);
            }else{
                params.height = mLayoutBody.getHeight() + (int)Utils.dpToPixel(mContext,30);
            }
            mIvShadow.setLayoutParams(params);

        }

        private void setAmountListener(){
            mEtAmount.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    Logs.e("onEditorAction : " + actionId);
                    switch (actionId) {
                        case EditorInfo.IME_ACTION_DONE:
                            Utils.hideKeyboard(mContext,mEtAmount);
                            break;
                        default:
                            return false;
                    }
                    return true;
                }
            });


            mEtAmount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    //Logs.e("beforeTextChanged : " + s.toString());
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                    if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY)
                        return;
                    if(mListener.getListBindingFlag()) return;
                    if(!mEtAmount.hasFocus()) return;
                    if(TextUtils.isEmpty(charSequence.toString())){
                        mEtAmount.setText("0");
                        mEtAmount.setSelection(1);
                        return;
                    }else if(charSequence.toString().equalsIgnoreCase("0")){
                        return;
                    }

                    if (!TextUtils.isEmpty(charSequence.toString()) && !charSequence.toString().equals(mInputAmount)) {
                        //Firebase에러 - 계좌로 보내기에서 금액 클립보드 복사 텍스트 붙여넣기시 에러.
                        mInputAmountNoComma = charSequence.toString().replaceAll("[^0-9]", Const.EMPTY);
                        mInputAmount = Utils.moneyFormatToWon(Double.parseDouble(mInputAmountNoComma));

                        mEtAmount.setText(mInputAmount);
                        mEtAmount.setSelection(mInputAmount.length());
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {
                    if(mListener.getListBindingFlag()) return;
                    if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY)
                        return;
                    if(!mEtAmount.hasFocus()) return;
                    int position = (int)mEtAmount.getTag(R.id.ITEM_POS);
                    String amount = editable.toString();
                    if(TextUtils.isEmpty(amount) || amount.equalsIgnoreCase("0")){
                        if(position == 0){
                            ITransferSalayMgr.getInstance().setTransStartAmount(position,"0");
                            //drawAmountAndWonAndLine(true,true);
                        }else{
                            ITransferSalayMgr.getInstance().setRemainAmount(position,"0");
                        }
                        return;
                    }

                    if(!editable.toString().equalsIgnoreCase(mInputAmount))
                        return;

                    if(position == 0){
                        ITransferSalayMgr.getInstance().setTransStartAmount(position,mInputAmountNoComma);
                    }else{
                        ITransferSalayMgr.getInstance().setRemainAmount(position,mInputAmountNoComma);
                    }

                    //제약조건을 확인한다.
                    String transAmount =  ITransferSalayMgr.getInstance().getSalayArrayList().get(position).getTRTM_AMT();
                    boolean validation = checkErrorInputValidation(position,transAmount,false);
                    drawAmountAndWonAndLine(validation,true);

                }
            });


            mEtAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {

                    final int position = (int)mEtAmount.getTag(R.id.ITEM_POS);
                    Logs.e("setOnFocusChangeListener - position : " + position);
                    Logs.e("setOnFocusChangeListener - hasfous : " + hasFocus);
                    Logs.e("setOnFocusChangeListener - getListBindingFlag : " + ((ITransferSalaryActivity)mContext).getListBindingFlag());

                    reSizeShadowHeight(hasFocus);
                    if(((ITransferSalaryActivity)mContext).getListBindingFlag())
                        return;

                    if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY)
                        return;

                    if(hasFocus){
                        mListener.setFocusedEditText(mEtAmount);
                        mEtAmount.setSelection(mEtAmount.getText().toString().length());

                        final String transAmount =  ITransferSalayMgr.getInstance().getSalayArrayList().get(position).getTRTM_AMT();

                        //딜레이를 주는 이유는 키패드가 올라오는 시간을 벌기위해.
                        //딜레이 안주면 키패드가 올라오지 안는다.
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                boolean IS_TOTAL_LIMIT_ERROR = ITransferSalayMgr.getInstance().getSalayArrayList().get(position).getIS_TOTAL_LIMIT_ERROR();
                                if(position == 0 && IS_TOTAL_LIMIT_ERROR){
                                    //이체 실행시 전체 한도를 초과한 경우 0번째 아이템만 색깔을 변경시켜준다.
                                    //drawAmountAndWonAndLine(true,false);
                                    int radius = (int) Utils.dpToPixel(mContext,20);
                                    mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},2,ERROR_BG_STROKE_COLOR));

                                    //mListener.setMainSubTitle("한도초과입니다. 이체금액을 내려주세요.",ITransferSalaryAdapter.ERROR_AMOUNT_TEXT_COLOR,true);
                                    ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setIS_TOTAL_LIMIT_ERROR(false);
                                    Utils.showKeyboard(mContext,mEtAmount);

                                    //잔액표시
                                    String balanceAmount = ITransferSalayMgr.getInstance().getWTCH_POSB_AMT();
                                    String commaBalanceAmount = Utils.moneyFormatToWon(Double.parseDouble(balanceAmount));
                                    drawNotiErrorText(false,"잔액 : " + commaBalanceAmount + " 원");
                                }else{
                                    boolean validationFlag = checkErrorInputValidation(position,transAmount,false);
                                    drawAmountAndWonAndLine(validationFlag,true);
                                }

                                mLayoutErrorNotice.setVisibility(VISIBLE);

                                String inputAmount = mEtAmount.getText().toString();
                                if(!TextUtils.isEmpty(inputAmount)){
                                    mEtAmount.setSelection(inputAmount.length());
                                }

                            }
                        },100);
                    }else{
                        mListener.setFocusedEditText(null);
                        mIvErrorIcon.setVisibility(GONE);
                        mLayoutErrorNotice.setVisibility(GONE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                drawAmountLine(NORMAL_LINE_COLOR);
                            }
                        },100);

                    }
                }
            });
        }

        public void onBind(final Context context, final int position){
            Logs.e("onBind - Start - position : " + position);
            
            if(position == 0){
                mVTopMargin.setVisibility(VISIBLE);
            }else{
                mVTopMargin.setVisibility(GONE);
            }

            final ITransferSalayMgr.ITransferSalayInfo info = ITransferSalayMgr.getInstance().getSalayArrayList().get(position);

            //은행 아이콘 출력
            Uri iconUri = ImgUtils.getIconUri(mContext,info.getRPRS_FNLT_CD(),info.getDTLS_FNLT_CD());
            if (iconUri != null)
                mIvBankIcon.setImageURI(iconUri);
            else
                mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);

            //계좌명 연결
            String accName = "";
            if(TextUtils.isEmpty(info.getACCO_ALS())){
                accName = info.getPROD_NM();
            }else{
                accName = info.getACCO_ALS();
            }
            if(!TextUtils.isEmpty(accName) && accName.length() > 14){
                accName = accName.substring(0,14) + Const.ELLIPSIS;
            }
            mTvAccountName.setText(accName);

            //계좌번호 연결
            String ACNO = info.getACNO();
            if(!TextUtils.isEmpty(ACNO)){
                ACNO = ACNO.substring(ACNO.length()-4,ACNO.length());
                mTvAccountNo.setText(ACNO);
            }

            //삭제버튼
            displayDeleteItemBtn(position);

            //금액입력
            displayAmountPositionFirst(context,info,position);
            displayAmountPositionMiddle(context,info,position);
            displayAmountPositionLast(context,info,position);


            mTvWon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mEtAmount.requestFocus();
                    Utils.showKeyboard(context,mEtAmount);
                }
            });

            mIvEraseAmountText.setTag(R.id.ITEM_POS,position);
            mIvEraseAmountText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int)v.getTag(R.id.ITEM_POS);
                    mEtAmount.setText("0");
                    if(position == 0){
                        ITransferSalayMgr.getInstance().setTransStartAmount(position,"0");
                    }else{
                        ITransferSalayMgr.getInstance().setRemainAmount(position,"0");
                        setTitleText(position,false,"","");
                    }

                    //포커스 가지고 있으면 키패드 내린다.
                    if(mEtAmount.hasFocus()){
                        Utils.hideKeyboard(context,mEtAmount);
                        mEtAmount.clearFocus();
                    }

                    mListener.notifyDataChanged();
                    mListener.checkRowAndPlayBtnState();
                }
            });

            //쉐도우 높이를 재조정.
            if(mIvShadow.getVisibility() == VISIBLE){
                reSizeShadowHeight(false);
            }

            //새로 그리면 포커스 사라지도록.
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if(ITransferSalayMgr.getInstance().getTransferPlayState() != ITransferSalayMgr.TRANSFER_STATE_PLAY){
                        if(info.getIS_NEED_FOCUS()){
                            mEtAmount.requestFocus();
                        }else{
                            mEtAmount.clearFocus();
                        }
                        info.setIS_NEED_FOCUS(false);
                        if(position == ITransferSalayMgr.getInstance().getSalayArrayList().size()-1){
                            mListener.setListBindingFlag(false);
                            mListener.checkRowAndPlayBtnState();
                        }
                    }else{
                        if(position == ITransferSalayMgr.getInstance().getSalayArrayList().size()-1){
                            mListener.setListBindingFlag(false);
                        }
                    }
                    Logs.e("onBind - End - position : " + position);
                }
            },50);

        }

        //삭제버튼
        private void displayDeleteItemBtn(int position){

            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY||
               ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_COMPLETE){
                mIvDeleteIcon.setVisibility(GONE);
            }else{
                if(position == 0 || ITransferSalayMgr.getInstance().getSalayArrayList().size() <= 2){
                    mIvDeleteIcon.setVisibility(GONE);
                }else if(ITransferSalayMgr.getInstance().getSalayArrayList().size() == 3){
                    String BANKCD_0 = ITransferSalayMgr.getInstance().getSalayArrayList().get(0).getRPRS_FNLT_CD();
                    String ACNO_0 = ITransferSalayMgr.getInstance().getSalayArrayList().get(0).getACNO();

                    String BANKCD_2 = ITransferSalayMgr.getInstance().getSalayArrayList().get(2).getRPRS_FNLT_CD();
                    String ACNO_2 = ITransferSalayMgr.getInstance().getSalayArrayList().get(2).getACNO();

                    if(BANKCD_0.equalsIgnoreCase(BANKCD_2) && ACNO_0.equalsIgnoreCase(ACNO_2) && position == 1){
                        mIvDeleteIcon.setVisibility(GONE);
                    }
                }else{
                    mIvDeleteIcon.setVisibility(VISIBLE);
                }
            }

            //리스트 아이템 삭제
            mIvDeleteIcon.setTag(R.id.ITEM_POS,position);
            mIvDeleteIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = (int) v.getTag(R.id.ITEM_POS);
                    if(position == ITransferSalayMgr.getInstance().getSalayArrayList().size() -1){
                        String TransAmount = ITransferSalayMgr.getInstance().getSalayArrayList().get(position).getTRTM_AMT();
                        ITransferSalayMgr.getInstance().getSalayArrayList().get(position-1).setTRTM_AMT(TransAmount);
                    }

                    ITransferSalayMgr.getInstance().getSalayArrayList().remove(position);

                    //전체 이체금액 다시 계산
                    ITransferSalayMgr.getInstance().reCalTransAmount();

                    mListener.notifyDataChanged();
                }
            });
        }

        /**
         * 0번째 아이템의 화면 표시
         *
         * @param context
         * @param info
         * @param position
         */
        private void displayAmountPositionFirst(final Context context, ITransferSalayMgr.ITransferSalayInfo info, int position){
            if(position != 0) return;

            //이체금액이 잔액보다 많으면 에러 출력해준다.
            String TRTM_AMT = info.getTRTM_AMT();
            String REMAIN_AMT = info.getREMAIN_AMT();

            mEtAmount.setTag(R.id.ITEM_POS,position);
            mTvAmount.setTag(R.id.ITEM_POS,position);
            mEtAmount.setVisibility(VISIBLE);
            mTvAmount.setVisibility(GONE);

            mLayoutErrorNotice.setVisibility(GONE);

            mTvSendText.setText("이체금액");
            mVAmountLine.setVisibility(View.VISIBLE);
            mIvEraseAmountText.setVisibility(VISIBLE);

            //0번은 이체금액을 보여준다.
            String commaTRTMAMT = Utils.moneyFormatToWon(Double.parseDouble(TRTM_AMT));
            mEtAmount.setText(commaTRTMAMT);
            mTvAmount.setText(commaTRTMAMT);

            //전체 한도초과로 0번째 아이템에만 에러 표시를 해야 해서 리턴한다.
            if(info.getIS_TOTAL_LIMIT_ERROR()){
                return;
            }

            //이체 완료가 되었으면 바로 리턴.
            if(info.getIS_TRANSFER_COMPLETE()){
                mEtAmount.setVisibility(View.INVISIBLE);
                mTvAmount.setVisibility(VISIBLE);
                mTvAmount.setTextColor(Color.parseColor("#000000"));
                mIvShadow.setVisibility(GONE);
                mIvNextArrow.setVisibility(GONE);
                mLottieArrow.setVisibility(GONE);
                mViewComplete.setVisibility(VISIBLE);
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);
                drawBackgroundStroke("#ffffff",NORMAL_BG_STROKE_COLOR);
                return;
            }

            //이체 실행중에는 금액을 변경하면 안되기에...
            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY){
                mEtAmount.setVisibility(View.INVISIBLE);
                mTvAmount.setVisibility(VISIBLE);
                mTvAmount.setTextColor(Color.parseColor("#000000"));
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);
                if(position == ITransferSalayMgr.getInstance().getCurrentPlayIndex()){
                    drawBackgroundStroke("#ffffff","#00ebff");

                    mIvNextArrow.setVisibility(GONE);
                    mLottieArrow.setVisibility(VISIBLE);
                    mLottieArrow.playAnimation();

                    mIvShadow.setVisibility(VISIBLE);
                }
                return;
            }

            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_COMPLETE){
                mEtAmount.setVisibility(GONE);
                mTvAmount.setVisibility(VISIBLE);
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);
                mIvNextArrow.setVisibility(GONE);
                mLottieArrow.setVisibility(GONE);
                if(info.getIS_NOW_ERROR()){
                    mTvAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                }else{
                    mTvAmount.setTextColor(Color.parseColor("#000000"));
                }
                drawAmountAndWonAndLine(info.getIS_NOW_ERROR(),false);
                return;
            }

            //이체중 에러발생하면 IS_NOW_ERROR에 값을 넣어주고 여기서 표현한다.
            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_STOP){
                if(info.getIS_NOW_ERROR()){
                    drawAmountAndWonAndLine(true,false);
                    info.setIS_NOW_ERROR(false);
                    ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_TITLE_ERROR("");
                    ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_SUBTITLE_ERROR("");
                    return;
                }
            }



            if(position == ITransferSalayMgr.getInstance().getCurrentPlayIndex()){
                mIvNextArrow.setVisibility(VISIBLE);

                mLottieArrow.pauseAnimation();
                mLottieArrow.setVisibility(GONE);

                mIvShadow.setVisibility(VISIBLE);
            }

            if(TextUtils.isEmpty(mEtAmount.getText().toString()) || mEtAmount.getText().toString().equalsIgnoreCase("0")){

                setTitleText(position,true,"금액을 확인해주세요","이체금액 없음");
                drawBackgroundStroke("#ffffff",ERROR_BG_STROKE_COLOR);

                mEtAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                mTvWon.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                drawAmountLine(NORMAL_LINE_COLOR);
            }else{
                boolean validation = checkErrorInputValidation(position,TRTM_AMT,true);
                drawAmountAndWonAndLine(validation,false);
                drawBackgroundStroke("#ffffff","");
            }
        }

        /**
         * 0번과 마지막 아이템을 제외한 아이템들의 화면 표시
         *
          * @param context
         * @param info
         * @param position
         */
        private void displayAmountPositionMiddle(final Context context, ITransferSalayMgr.ITransferSalayInfo info, int position){
            if(ITransferSalayMgr.getInstance().getSalayArrayList().size() == 2) return;
            if(position == 0 || position == ITransferSalayMgr.getInstance().getSalayArrayList().size()-1){
                return;
            }

            String TRTM_AMT = info.getTRTM_AMT();
            String REMAIN_AMT = info.getREMAIN_AMT();

            mEtAmount.setTag(R.id.ITEM_POS,position);
            mTvAmount.setTag(R.id.ITEM_POS,position);

            mLayoutErrorNotice.setVisibility(GONE);

            mTvSendText.setText("남길금액");
            mVAmountLine.setVisibility(View.VISIBLE);
            mIvEraseAmountText.setVisibility(VISIBLE);

            //나머지는 남은 금액을 보여준다.
            String commaREMAIN_AMT = Utils.moneyFormatToWon(Double.parseDouble(REMAIN_AMT));
            mEtAmount.setText(commaREMAIN_AMT);
            mTvAmount.setText(commaREMAIN_AMT);
            mEtAmount.setVisibility(VISIBLE);
            mTvAmount.setVisibility(GONE);
            mViewComplete.setVisibility(GONE);

            //해당 아이템이 이체완료된 경우
            if(info.getIS_TRANSFER_COMPLETE()){
                mEtAmount.setVisibility(View.INVISIBLE);
                mTvAmount.setVisibility(VISIBLE);
                mIvShadow.setVisibility(GONE);
                mIvNextArrow.setVisibility(GONE);
                mLottieArrow.pauseAnimation();
                mLottieArrow.setVisibility(GONE);
                mViewComplete.setVisibility(VISIBLE);
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);
                drawBackgroundStroke("#ffffff",NORMAL_BG_STROKE_COLOR);
                return;
            }

            //이체 실행중에는 금액을 변경하면 안되기에...
            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PLAY){
                mEtAmount.setVisibility(View.INVISIBLE);
                mTvAmount.setVisibility(VISIBLE);
                mTvAmount.setTextColor(Color.parseColor("#000000"));
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);

                if(position == ITransferSalayMgr.getInstance().getCurrentPlayIndex()){
                    drawBackgroundStroke("#ffffff","#00ebff");
                    mIvNextArrow.setVisibility(GONE);
                    mLottieArrow.setVisibility(VISIBLE);
                    mLottieArrow.playAnimation();
                    mIvShadow.setVisibility(VISIBLE);

                }else if(position == ITransferSalayMgr.getInstance().getCurrentPlayIndex()+1){
                    drawBackgroundStroke("#ffffff","#00ebff");
                    mIvShadow.setVisibility(VISIBLE);
                    mIvNextArrow.setVisibility(GONE);
                    mLottieArrow.setVisibility(GONE);
                }else{
                    drawBackgroundStroke("#ffffff",NORMAL_BG_STROKE_COLOR);
                    mIvNextArrow.setVisibility(GONE);
                    mLottieArrow.setVisibility(GONE);
                }
                return;
            }

            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_COMPLETE){
                mEtAmount.setVisibility(GONE);
                mTvAmount.setVisibility(VISIBLE);
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);
                mIvShadow.setVisibility(GONE);
                mIvNextArrow.setVisibility(GONE);
                mLottieArrow.pauseAnimation();
                mLottieArrow.setVisibility(GONE);
                if(info.getIS_NOW_ERROR()){
                    mTvAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                }else{
                    mTvAmount.setTextColor(Color.parseColor("#000000"));
                }
                drawAmountAndWonAndLine(info.getIS_NOW_ERROR(),false);
                return;
            }

            //이체중 에러발생하면 IS_NOW_ERROR에 값을 넣어주고 여기서 표현한다.
            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_STOP){
                if(info.getIS_NOW_ERROR()){
                    drawAmountAndWonAndLine(true,false);
                    info.setIS_NOW_ERROR(false);
                    ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_TITLE_ERROR("");
                    ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_SUBTITLE_ERROR("");
                    return;
                }
            }

            mIvShadow.setVisibility(GONE);
            if(position == ITransferSalayMgr.getInstance().getCurrentPlayIndex()){
                mIvNextArrow.setVisibility(VISIBLE);
                mLottieArrow.setVisibility(GONE);
            }else if(position == ITransferSalayMgr.getInstance().getSalayArrayList().size()-2){
                mIvNextArrow.setVisibility(VISIBLE);
                mLottieArrow.setVisibility(GONE);
            }else{
                mIvNextArrow.setVisibility(GONE);
                mLottieArrow.setVisibility(GONE);
            }

            //금액 관련 체크
            if(TextUtils.isEmpty(REMAIN_AMT) || REMAIN_AMT.equalsIgnoreCase("0")){
                mEtAmount.setTextColor(Color.parseColor("#000000"));
                mTvWon.setTextColor(Color.parseColor("#000000"));
                drawBackgroundStroke("#ffffff",NORMAL_BG_STROKE_COLOR);
                drawAmountLine(NORMAL_LINE_COLOR);
            }else{
                boolean validation = checkErrorInputValidation(position,TRTM_AMT,true);
                drawAmountAndWonAndLine(validation,false);
            }
        }

        /**
         * 마지막 아이템의 화면 표시
         * @param context
         * @param info
         * @param position
         */
        private void displayAmountPositionLast(final Context context, ITransferSalayMgr.ITransferSalayInfo info, int position){
            if(position != ITransferSalayMgr.getInstance().getSalayArrayList().size()-1) return;

            String TRTM_AMT = info.getTRTM_AMT();
            String REMAIN_AMT = info.getREMAIN_AMT();

            mLayoutErrorNotice.setVisibility(GONE);
            mIvNextArrow.setVisibility(GONE);
            mIvNextArrow.setVisibility(GONE);
            mLottieArrow.setVisibility(GONE);
            mIvShadow.setVisibility(GONE);

            mTvSendText.setText("입금금액");
            mVAmountLine.setVisibility(View.INVISIBLE);
            mIvEraseAmountText.setVisibility(GONE);

            //마지막은 입금금액을 보여준다.
            String commaTRTMAMT = Utils.moneyFormatToWon(Double.parseDouble(TRTM_AMT));
            mEtAmount.setText(commaTRTMAMT);
            mTvAmount.setText(commaTRTMAMT);
            mEtAmount.setVisibility(GONE);
            mTvAmount.setVisibility(VISIBLE);

            if(info.getIS_TRANSFER_COMPLETE()){
                mIvShadow.setVisibility(GONE);
                mIvNextArrow.setVisibility(GONE);
                mViewComplete.setVisibility(VISIBLE);
                mVAmountLine.setVisibility(View.INVISIBLE);
                drawBackgroundStroke("#ffffff",NORMAL_BG_STROKE_COLOR);
                return;
            }

            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_COMPLETE){
                mEtAmount.setVisibility(GONE);
                mTvAmount.setVisibility(VISIBLE);
                mVAmountLine.setVisibility(View.INVISIBLE);
                mIvEraseAmountText.setVisibility(GONE);
                mIvShadow.setVisibility(GONE);
                mIvNextArrow.setVisibility(GONE);
                if(info.getIS_NOW_ERROR()){
                    mTvAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                }
                drawAmountAndWonAndLine(info.getIS_NOW_ERROR(),false);
                return;
            }

            if(ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_NONE
               ||ITransferSalayMgr.getInstance().getTransferPlayState() == ITransferSalayMgr.TRANSFER_STATE_PAUSE){
                if(TextUtils.isEmpty(TRTM_AMT)||TRTM_AMT.equalsIgnoreCase("0")){
                    setTitleText(position,true,"금액을 확인해주세요","이체금액 없음");

                    mTvAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                    mTvWon.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                    drawBackgroundStroke("#f1f4f5",ERROR_BG_STROKE_COLOR);
                }else{
                    double trtmAmtDouble = Double.parseDouble(TRTM_AMT);
                    if(trtmAmtDouble < 0){
                        mTvAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                        mTvWon.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                        drawBackgroundStroke("#f1f4f5",ERROR_BG_STROKE_COLOR);
                    }else{
                        setTitleText(position,false,"","");

                        mTvAmount.setTextColor(Color.parseColor("#000000"));
                        mTvWon.setTextColor(Color.parseColor("#000000"));
                        drawBackgroundStroke("#f1f4f5","");
                    }
                }
            }else {
                //이체 실행중에는 금액을 변경하면 안되기에...
                if(position == ITransferSalayMgr.getInstance().getCurrentPlayIndex()+1){
                    drawBackgroundStroke("#ffffff","#00ebff");
                    mIvShadow.setVisibility(VISIBLE);
                    mIvNextArrow.setVisibility(GONE);
                }else{
                    drawBackgroundStroke("#ffffff",NORMAL_BG_STROKE_COLOR);
                }
            }
        }

        /**
         * 타이틀의 텍스트를 모델에 넣어두고 첫번째 에러사항부터 표시하도록 한다.
         *
         * @param position
         * @param isError
         * @param subTitle
         */

        private void setTitleText(int position,boolean isError,String title,String subTitle){

            ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setIS_NOW_ERROR(isError);
            ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_TITLE_ERROR(title);
            ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_SUBTITLE_ERROR(subTitle);

            mListener.checkRowAndPlayBtnState();
        }

        private void drawBackgroundStroke(String bgColor,String strokeColor){
            int radius = (int) Utils.dpToPixel(mContext,20);
            if(TextUtils.isEmpty(strokeColor)){
                mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable(bgColor,new int[]{radius,radius,radius,radius}));
            }else{
                mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable(bgColor,new int[]{radius,radius,radius,radius},2,strokeColor));
            }
        }

        /**
         * 금액,원, 라인을 그린다.
         *
         * @param isError
         * @param isFocus
         */
        private void drawAmountAndWonAndLine(boolean isError,boolean isFocus){
            if(isError){
                mEtAmount.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                mTvWon.setTextColor(Color.parseColor(ERROR_AMOUNT_TEXT_COLOR));
                if(isFocus)
                    drawAmountLine(ERROR_LINE_COLOR);
                else
                    drawAmountLine(NORMAL_LINE_COLOR);

                int radius = (int) Utils.dpToPixel(mContext,20);
                mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},2,ERROR_BG_STROKE_COLOR));

            }else{
                mEtAmount.setTextColor(Color.parseColor(NORMAL_AMOUNT_TEXT_COLOR));
                mTvWon.setTextColor(Color.parseColor(NORMAL_AMOUNT_TEXT_COLOR));
                if(isFocus){
                    drawAmountLine(FOCUS_LINE_COLOR);
                    int radius = (int) Utils.dpToPixel(mContext,20);
                    mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},2,FOCUS_BG_STROKE_COLOR));
                }else{

                    drawAmountLine(NORMAL_LINE_COLOR);
                    int radius = (int) Utils.dpToPixel(mContext,20);
                    mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},2,NORMAL_BG_STROKE_COLOR));
                }
            }
        }


        /**
         * 금액 밑의 라인을 그린다.
         *
         * @param color
         */
        private void drawAmountLine(final String color){
            mVAmountLine.setBackgroundColor(Color.parseColor(color));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ViewGroup.LayoutParams lineParams = mVAmountLine.getLayoutParams();
                    lineParams.width = mLayoutAmount.getWidth();
                    mVAmountLine.setLayoutParams(lineParams);
                }
            },50);
        }

        /**
         * 카드뷰의 한도및 잔액을 표시한다.
         *
         * @param isError
         * @param text
         */
        private void drawNotiErrorText(boolean isError,String text){
            if(isError){
                mIvErrorIcon.setVisibility(VISIBLE);
                mTvErrorText.setTextColor(Color.parseColor(ERROR_NOTI_TEXT_COLOR));
            }else{
                mIvErrorIcon.setVisibility(GONE);
                mTvErrorText.setTextColor(Color.parseColor(NORMAL_NOTI_TEXT_COLOR));
            }
            mTvErrorText.setText(text);
        }

        /**
         * 입력한 금액을 체크한다.
         *
         * @param position
         * @param transAmount
         * @param titleUpdate
         * @return
         */
        private boolean checkErrorInputValidation(int position,String transAmount,boolean titleUpdate){

            transAmount = transAmount.replace(",","");

            //먼저 잔액 or 이체 가능금액 체크
            String balanceAmount = "0";
            if(position == 0){
                //입금금액이 계좌 잔액보다 큰경우
                balanceAmount = ITransferSalayMgr.getInstance().getWTCH_POSB_AMT();
            }else{
                balanceAmount = ITransferSalayMgr.getInstance().getSalayArrayList().get(position-1).getTRTM_AMT();
            }

//            Logs.e("#########################################################");
//            Logs.e("checkInputValidation - position : " + position );
//            Logs.e("checkInputValidation - transAmount : " + transAmount );
//            Logs.e("checkInputValidation - balanceAmount : " + balanceAmount );
//            Logs.e("#########################################################");

            String commaBalanceAmount = Utils.moneyFormatToWon(Double.parseDouble(balanceAmount));
            if(Double.parseDouble(transAmount) < 0){
                String commaTransAmount = Utils.moneyFormatToWon(Double.parseDouble(transAmount));
                drawNotiErrorText(true,"잔여금액 : " + commaTransAmount + " 원");
                if(titleUpdate){
                    setTitleText(position,true,"잔여금액이 부족합니다","잔여금액 확인");
                }
                return true;
            }else if(Double.parseDouble(transAmount) > Double.parseDouble(balanceAmount)){
                drawNotiErrorText(true,"잔액 : " + commaBalanceAmount + " 원");
                if(titleUpdate){
                    setTitleText(position,true,"잔액이 부족합니다","계좌 잔액 확인");
                }
                return true;
            }else{
                drawNotiErrorText(false,"잔액 : " + commaBalanceAmount + " 원");
            }

            //이체한도체크
            String WTCH_LMIT_REMN_AMT = ITransferSalayMgr.getInstance().getWTCH_LMIT_REMN_AMT();
            if(TextUtils.isEmpty(WTCH_LMIT_REMN_AMT)) WTCH_LMIT_REMN_AMT = "0";
            if(Double.parseDouble(transAmount) > Double.parseDouble(WTCH_LMIT_REMN_AMT)){
                int differenceAmount = (int)(Double.parseDouble(WTCH_LMIT_REMN_AMT) - Double.parseDouble(transAmount));
                String commaDifferenceAmount = Utils.moneyFormatToWon((double)differenceAmount);
                drawNotiErrorText(true,"이체한도초과 : " + commaDifferenceAmount + " 원");
                if(titleUpdate){
                    WTCH_LMIT_REMN_AMT = Utils.moneyFormatToWon(Double.parseDouble(WTCH_LMIT_REMN_AMT));
                    setTitleText(position,true,"이체한도 초과입니다","이체한도 : 1일 / " + WTCH_LMIT_REMN_AMT + " 원");
                }
                return true;
            }


            //정상일때는 에러정보 삭제.
            ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setIS_NOW_ERROR(false);
            ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_TITLE_ERROR("");
            ITransferSalayMgr.getInstance().getSalayArrayList().get(position).setMSG_SUBTITLE_ERROR("");
            if(titleUpdate){
                mListener.checkRowAndPlayBtnState();
            }

            return false;
        }
    }

}
