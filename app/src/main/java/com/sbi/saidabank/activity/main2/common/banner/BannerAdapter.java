package com.sbi.saidabank.activity.main2.common.banner;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.main.MainAdsItemInfo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.BannerViewHolder> {
    private Context mContext;
    private ArrayList<MainAdsItemInfo> mBannerArray;
    private ArrayList<Bitmap> mBitmapArray;
    private OnItemSelectListener mListener;
    private int mLoadCnt;

    public interface OnItemSelectListener {
        void onSelectItem(String url);
        void onLoadBitmapResult(ArrayList<MainAdsItemInfo> bannerArray);
    }

    //ViewHolder정의
    public static class BannerViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        public BannerViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_banner);
        }
    }

    //생성자
    public BannerAdapter(Context context, OnItemSelectListener listener){
        mContext = context;
        mBannerArray = new ArrayList<MainAdsItemInfo>();
        mBitmapArray = new ArrayList<Bitmap>();
        mListener = listener;
        mLoadCnt = 0;
    }

    @NonNull
    @Override
    public BannerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_main2_banner_item, parent, false);
        return new BannerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BannerViewHolder holder, int position) {

        if(mBitmapArray == null || mBitmapArray.size() ==0 ) return;

        int index = position % mBannerArray.size();
        //Logs.e("onBindViewHolder : "+position);

        //Firebase 문제점 방어코드
        if(index >= mBitmapArray.size()) return;

        holder.imageView.setImageBitmap(mBitmapArray.get(index));
        holder.imageView.setTag(index);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int index = (int)v.getTag();
                if(mListener != null && !TextUtils.isEmpty(mBannerArray.get(index).getLINK_URL_ADDR()))
                    mListener.onSelectItem(mBannerArray.get(index).getLINK_URL_ADDR());
            }
        });
    }

    @Override
    public int getItemCount() {
        if(mBannerArray.size() < 2){
            return mBannerArray.size();
        }else{
            return Integer.MAX_VALUE;
        }
    }

    public void clearBitmap(){
        for(int i=0;i<mBitmapArray.size();i++){
            if(mBitmapArray.get(i) != null && !mBitmapArray.get(i).isRecycled()){
                mBitmapArray.get(i).recycle();
            }
        }
        mBitmapArray.clear();
    }

    public void setBannerArray(ArrayList<MainAdsItemInfo> arrayList){

        if(mBannerArray.size() == arrayList.size()){
            //꼭!!! 여기서 리스너 리턴해줘야 한다. 다음으로 진행하지 못함.
            mListener.onLoadBitmapResult(mBannerArray);
            return;
        }

        mLoadCnt=0;
        Logs.e("setBannerArray - mBitmapArray :  " + mBitmapArray.size());
        clearBitmap();

        mBannerArray.clear();
        mBannerArray.addAll(arrayList);

        Logs.e("mBannerArray.size : " + mBannerArray.size());
        for(int i=0;i<mBannerArray.size();i++){
            mBitmapArray.add(i,null);
            loadBitmap2(i);
        }
    }

    private void loadBitmap2(final int index) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Logs.e(index + ".loadBitmap2 - setBannerArray -  url : "+ SaidaUrl.getBaseWebUrl() + "/" + mBannerArray.get(index).getIMG_URL_ADDR());

                            URL url = new URL(SaidaUrl.getBaseWebUrl() + "/" + mBannerArray.get(index).getIMG_URL_ADDR());
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setDoInput(true);
                            connection.connect();
                            InputStream input = connection.getInputStream();
                            Bitmap myBitmap = BitmapFactory.decodeStream(input);

                            //홈 Fragment들을 계속 이동시키다 보면 아래부분에서 에러가 발생한다. 방어코드추가
                            if(mBitmapArray == null || mBitmapArray.size() ==0 || mBitmapArray.size() < index) return;

                            if(myBitmap != null){
                                mBitmapArray.set(index,myBitmap);
                                Logs.e("loadBitmap2 - setBannerArray -  onBitmapLoaded - OK: "+index);
                            }else{
                                mBitmapArray.set(index,null);
                                Logs.e("loadBitmap2 - setBannerArray -  onBitmapLoaded - FAIL: "+index);
                            }
                            mLoadCnt++;
                            if(mLoadCnt >= mBannerArray.size()){
                                //비트맵 로드가 끝났으면 잘 가져왔는지 체크해본다.
                                checkLoadBitmap();
                            }
                        } catch (IOException e) {
                            // Log exception

                        }
                    }
                }).start();

    }

    private void checkLoadBitmap(){
        Logs.e("checkLoadBitmap - mBitmapArray :  " + mBitmapArray.size());
        for(int i=mBitmapArray.size()-1;i>=0;i--){
            if(mBitmapArray.get(i) == null){
                mBitmapArray.remove(i);
                mBannerArray.remove(i);
            }
        }

        ((BaseActivity)mContext).runOnUiThread(
                new Runnable() {
                    public void run() {
                        // 메시지 큐에 저장될 메시지의 내용
                        notifyDataSetChanged();
                        mListener.onLoadBitmapResult(mBannerArray);
                    }
                });
    }
}
