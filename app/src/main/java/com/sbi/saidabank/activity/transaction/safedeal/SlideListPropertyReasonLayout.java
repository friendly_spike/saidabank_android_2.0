package com.sbi.saidabank.activity.transaction.safedeal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.adater.SafeDealPropertyReasonAdapter;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 부동산 거래 사유 입력 슬라이드 화면
 */
public class SlideListPropertyReasonLayout extends SlideBaseLayout implements View.OnTouchListener{

    private ArrayList<RequestCodeInfo> mArrayPropertyReason;
    private SafeDealPropertyReasonAdapter mSafeDealPropertyReasonAdapter;
    private ListView mListView;

    public SlideListPropertyReasonLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideListPropertyReasonLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUX(Context context){
        setTag("SlideListPropertyReasonLayout");
        View layout = View.inflate(context, R.layout.layout_safedeal_slide_list_property_reason, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);
        LinearLayout layoutTitle = layout.findViewById(R.id.layout_title);
        int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);
        layoutTitle.setBackground(GraphicUtils.getRoundCornerDrawable("#F5F5F5",new int[]{radius,radius,0,0}));

        layout.findViewById(R.id.layout_movebar).setOnTouchListener(this);

        mArrayPropertyReason = new ArrayList<RequestCodeInfo>();
        mSafeDealPropertyReasonAdapter = new SafeDealPropertyReasonAdapter(getContext(),mArrayPropertyReason);
        mListView = layout.findViewById(R.id.listview);
        mListView.setAdapter(mSafeDealPropertyReasonAdapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RequestCodeInfo info = mArrayPropertyReason.get(position);
                setDealReason(info.getSCCD(),info.getCD_NM());
            }
        });



        initLayoutSetBottom(true,false);

    }

    @Override
    public void onCancel() {
        //super.onCancel();
        //if(isScale) return;

        slidingDown(250,0);

    }

    private void setDealReason(String cd,String reason){
        View view = getSlideView(SlideDealReasonLayout.class);
        if(view != null){
            ((SlideDealReasonLayout)view).setPropertyReason(cd,reason);
        }

        slidingDown(250,0);
    }

    private float startdY;
    private float moveY;
    private int   startViewHeight;
    private boolean isMove;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() != R.id.layout_movebar){
            return false;
        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_DOWN - y : " + event.getRawY());
                startdY = event.getRawY();
                isMove = false;
                startViewHeight = mLayoutBody.getHeight();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY =  startdY - event.getRawY();
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - moveY : " + moveY);

                if(moveY == 0){
                    return false;
                }

                //확장하려고 하지만 이미 모두 확장했당.
                if(moveY > 0 && mScreenHeight == startViewHeight){
                    return false;
                }

//                if(moveY < 0  && (mOriginViewHeight > startViewHeight + moveY) ){
//                    return false;
//                }

                isMove = true;
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - isMove : " + isMove);
                ViewGroup.LayoutParams params = mLayoutBody.getLayoutParams();
                params.height = startViewHeight + (int)moveY;
                mLayoutBody.setLayoutParams(params);
                mLayoutBody.requestLayout();

                break;

            case MotionEvent.ACTION_UP:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_UP - y : " + event.getRawY());

                if(event.getRawY() > mOriginViewYPos){
                    slidingDown(250,0);
                    return false;
                }

                if(isMove && moveY != 0){
                    int toSize = 0;
                    if(moveY > 0){ //확장
                        if(moveY > 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }else if(moveY < 0){ //복귀
                        moveY = moveY * -1;
                        if(moveY < 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }

                    AniUtils.changeViewHeightSizeAnimation(mLayoutBody,toSize,250);
                }
                isMove = false;
                break;

            default:
                return false;
        }


        return false;
    }

    /**
     * 부동산거래 사유 코드 조회
     */
    public void requestPropertyReasonList() {
        MLog.d();

        Map param = new HashMap();
        param.put("LCCD", "SAFE_TRN_RSCD");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(mActionlistener.onCheckHttpError(ret,false))   return;

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null)
                        return;

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;
                        mArrayPropertyReason.add(new RequestCodeInfo(jsonObject));
                    }
                    mSafeDealPropertyReasonAdapter.setArrayList(mArrayPropertyReason);
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

}
