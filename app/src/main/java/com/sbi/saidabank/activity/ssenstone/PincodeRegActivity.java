package com.sbi.saidabank.activity.ssenstone;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.info.DeviceResult;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.login.CompleteChangePhoneActivity;
import com.sbi.saidabank.activity.login.CompleteMemberActivity;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.PinErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: PincodeRegActivity
 * Created by 950485 on 2018. 9. 6..
 * <p>
 * Description: 핀코드 인증 등록을 위한 화면
 */

public class PincodeRegActivity extends BaseActivity implements View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener,I3GAsyncResponse, StonePassManager.StonePassListener {
	private static final int  mKeypadType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;
	private static final int PINCODE_REG_STATE_JOIN 					= 0;
	private static final int PINCODE_REG_STATE_CHANGE_DEVICE 			= 1;
	private static final int PINCODE_REG_STATE_MANAGE					= 2;
	private static final int PINCODE_REG_STATE_MANAGE_PINCODE 			= 3;
	private static final int PINCODE_REG_STATE_MANAGE_LOGIN 			= 4;
	private static final int PINCODE_REG_STATE_MANAGE_BIO 				= 5;
	private static final int PINCODE_REG_STATE_MANAGE_PINCODE_FORGET	= 6;
	private static final int PINCODE_REG_STATE_ELSE				   		= 7;

	/**
	 * ssenstone 등록 단계
	 */
	private enum REG_STEP {
		STEP_01,
		STEP_02
	}

	private LinearLayout  mLayoutMsg;
	private TextView      mTextDesc;
	private TextView      mTextMsg;
	private EditText      mExitPincode;
	private EditText      mEditPincode01;
	private ImageView     mImagePincode01;
	private EditText      mEditPincode02;
	private ImageView     mImagePincode02;
	private EditText      mEditPincode03;
	private ImageView     mImagePincode03;
	private EditText      mEditPincode04;
	private ImageView     mImagePincode04;
	private EditText      mEditPincode05;
	private ImageView     mImagePincode05;
	private EditText      mEditPincode06;
	private ImageView     mImagePincode06;

	private boolean       mIsViewCtrlKeypad = false;
	private Handler 	  mInputCearDelayHandler;

	private String         mFirstPin = null;
	private CommonUserInfo mCommonUserInfo;

	private TransKeyCtrl  mTKMgr = null;

	private REG_STEP      mRegStep = REG_STEP.STEP_01;


	//필요권한 - 전화접근권한

	private String[] perList = new String[]{
			Manifest.permission.READ_PHONE_STATE,
	};

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pincode_reg);

		getExtra();
        initUX();

//		if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
//			AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"8", "");

		PermissionUtils.checkPermission(this,perList,Const.REQUEST_PERMISSION_READ_PHONE_STATE);
    }

    @Override
	public boolean onTouch(View v, MotionEvent event) {
    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    	if (!mIsViewCtrlKeypad) {
	    		showKeyPad();
				return true;
		    }
    	}
		return false;
	}

	@Override
	public void cancel(Intent data) {
		backStep();
	}

	@Override
	public void done(Intent data) {
		mIsViewCtrlKeypad = false;
		if (data == null)
			return;

		String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
		//String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
		byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
		int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

		if (iRealDataLength != mCommonUserInfo.getPinLength()) {
			String msg = getString(R.string.message_pin_right_length, mCommonUserInfo.getPinLength());
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(msg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		String plainData = "";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(secureKey);

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "";
			}

		} catch (Exception e) {
			Logs.printException(e);
		}

		if (TextUtils.isEmpty(plainData)) {
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(R.string.msg_err_decode_pin);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		if (mRegStep == REG_STEP.STEP_01) {
			if (Utils.isDuplicate(plainData, Const.SSENSTONE_PIN_DUPLICATE)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_reg_pin_duplicate);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}

			if (Utils.isContinueNum(plainData, 3)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_reg_pin_continue_num);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}

			// 생년월일/폰번호는 로그인 후라면 로그인 세션에서, 로그인 전이면 폰인증에서 받은 정보를 사용
			LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
			String birth = loginUserInfo.getBRDD();
			String phoneNumber0 = loginUserInfo.getCLPH_LCNO();
			String phoneNumber1 = loginUserInfo.getCLPH_TONO();
			String phoneNumber2 = loginUserInfo.getCLPH_SRNO();

			if (TextUtils.isEmpty(birth)) {
				birth = mCommonUserInfo.getBirth();
				String sGender = mCommonUserInfo.getSexCode();
				if (!TextUtils.isEmpty(birth) && birth.length() == 6) {
					if ("1".equals(sGender) || "2".equals(sGender))
						birth = "19" + birth;
					if ("3".equals(sGender) || "4".equals(sGender))
						birth = "20" + birth;
				}
			}

			if (TextUtils.isEmpty(phoneNumber1)) {
				phoneNumber1 = mCommonUserInfo.getPhoneNumber();
				if (!TextUtils.isEmpty(phoneNumber1) && phoneNumber1.length() == 11) {
					phoneNumber0 = phoneNumber1.substring(0, 3);
					phoneNumber1 = phoneNumber1.substring(3, 11);
				} else if (!TextUtils.isEmpty(phoneNumber1) && phoneNumber1.length() == 10) {
					phoneNumber0 = phoneNumber1.substring(0, 3);
					phoneNumber1 = phoneNumber1.substring(3, 10);
				}
			} else {
				phoneNumber1 = phoneNumber1 + phoneNumber2;
			}
			// 생년월일 체크
			if (!TextUtils.isEmpty(birth) && birth.length() == 8) {
				for (int i = 0 ; i < 5 ; i++) {
					String tmpStr = birth.substring(i, i + 4);
					if (plainData.contains(tmpStr)) {
						mLayoutMsg.setVisibility(View.VISIBLE);
						mTextMsg.setText("생년월일은 사용할 수 없습니다.");
						showKeyPad();
						AniUtils.shakeView(mLayoutMsg);
						return;
					}
				}
			}
			// 핸드폰번호 체크
			if (!TextUtils.isEmpty(phoneNumber1) && (phoneNumber1.length() == 8 || phoneNumber1.length() == 7)) {
				int tmpLengh = 5;
				if (phoneNumber1.length() == 7)
					tmpLengh = 4;
				for (int i = 0 ; i < tmpLengh ; i++) {
					String tmpStr = phoneNumber1.substring(i, i + 4);
					if (plainData.contains(tmpStr)) {
						mLayoutMsg.setVisibility(View.VISIBLE);
						mTextMsg.setText("핸드폰 번호는 사용할 수 없습니다.");
						showKeyPad();
						AniUtils.shakeView(mLayoutMsg);
						return;
					}
				}

				String checkExtend = phoneNumber0 + phoneNumber1.substring(0, 3);
				if (plainData.startsWith(checkExtend)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText("핸드폰 번호는 사용할 수 없습니다.");
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
			}

			if (!TextUtils.isEmpty(mCommonUserInfo.getSignData())) {
				if (mCommonUserInfo.getSignData().equalsIgnoreCase(plainData)) {
					mTextMsg.setText(R.string.msg_match_org_pin);
					mLayoutMsg.setVisibility(View.VISIBLE);
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
			}

			mTextDesc.setText(R.string.msg_retry_input_pin);
			mFirstPin = plainData;
			mRegStep = REG_STEP.STEP_02;
			showKeyPad();
		} else if (mRegStep == REG_STEP.STEP_02) {
			if (!isEqualsPin(mFirstPin, plainData)) {
				mTextMsg.setText(R.string.msg_no_match_reg_pin);
				mLayoutMsg.setVisibility(View.VISIBLE);

				Handler delayHandler = new Handler();
				delayHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						goRegStep01();
						mLayoutMsg.setVisibility(View.INVISIBLE);
					}
				}, 500);
				AniUtils.shakeView(mLayoutMsg);
				return;
			}

			final String sendData = plainData;
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					showProgressDialog();
				}
			});
			new Thread(new Runnable() {
				@Override
				public void run() {
					sendRegSSenstone(sendData);
				}
			}).start();
		}
	}
	
	@Override
	public void onBackPressed() {
		backStep();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public void input(int arg0) {
		if (arg0 == ITransKeyActionListenerEx.INPUT_CHARACTER_KEY) {
			mLayoutMsg.setVisibility(View.INVISIBLE);

			String strValue1 = mEditPincode01.getText().toString();
			String strValue2 = mEditPincode02.getText().toString();
			String strValue3 = mEditPincode03.getText().toString();
			String strValue4 = mEditPincode04.getText().toString();
			String strValue5 = mEditPincode05.getText().toString();

			if (TextUtils.isEmpty(strValue1)) {
				mEditPincode01.setText("0");
				mImagePincode01.setImageResource(R.drawable.ico_pin_on);
				mEditPincode02.requestFocus();
			} else if(TextUtils.isEmpty(strValue2)) {
				mEditPincode02.setText("0");
				mImagePincode02.setImageResource(R.drawable.ico_pin_on);
				mEditPincode03.requestFocus();
			} else if(TextUtils.isEmpty(strValue3)) {
				mEditPincode03.setText("0");
				mImagePincode03.setImageResource(R.drawable.ico_pin_on);
				mEditPincode04.requestFocus();
			} else if(TextUtils.isEmpty(strValue4)) {
				mEditPincode04.setText("0");
				mImagePincode04.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getPinLength() == 5)
					mEditPincode05.requestFocus();
			} else if(TextUtils.isEmpty(strValue5)) {
				mEditPincode05.setText("0");
				mImagePincode05.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getPinLength() == 6)
					mEditPincode06.requestFocus();
			} else {
				mEditPincode06.setText("0");
				mImagePincode06.setImageResource(R.drawable.ico_pin_on);
			}

			int length = mTKMgr.getInputLength();
			if (mRegStep == REG_STEP.STEP_01 && length == mCommonUserInfo.getPinLength()) {
				mTKMgr.done();
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_BACKSPACE_KEY) {
			String strValue1 = mEditPincode01.getText().toString();
			String strValue2 = mEditPincode02.getText().toString();
			String strValue3 = mEditPincode03.getText().toString();
			String strValue4 = mEditPincode04.getText().toString();
			String strValue5 = mEditPincode05.getText().toString();
			String strValue6 = mEditPincode06.getText().toString();

			if (mCommonUserInfo.getPinLength() == 6) {
				if (!TextUtils.isEmpty(strValue6)) {
					mEditPincode06.setText("");
					mImagePincode06.setImageResource(R.drawable.ico_pin_off);
					mEditPincode05.requestFocus();
					return;
				} else {
					if(!TextUtils.isEmpty(strValue5)) {
						mEditPincode05.setText("");
						mImagePincode05.setImageResource(R.drawable.ico_pin_off);
						mEditPincode04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getPinLength() == 5) {
				if(!TextUtils.isEmpty(strValue5)) {
					mEditPincode05.setText("");
					mImagePincode05.setImageResource(R.drawable.ico_pin_off);
					mEditPincode04.requestFocus();
					return;
				}
			}

			if (!TextUtils.isEmpty(strValue4)) {
				mEditPincode04.setText("");
				mImagePincode04.setImageResource(R.drawable.ico_pin_off);
				mEditPincode03.requestFocus();
			} else if (!TextUtils.isEmpty(strValue3)) {
				mEditPincode03.setText("");
				mImagePincode03.setImageResource(R.drawable.ico_pin_off);
				mEditPincode02.requestFocus();
			} else if (!TextUtils.isEmpty(strValue2)) {
				mEditPincode02.setText("");
				mImagePincode02.setImageResource(R.drawable.ico_pin_off);
				mEditPincode01.requestFocus();
			} else if (!TextUtils.isEmpty(strValue1)) {
				mEditPincode01.setText("");
				mImagePincode01.setImageResource(R.drawable.ico_pin_off);
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_CLEARALL_BUTTON) {
			clearInput();
		}
	}

	@Override
	public void minTextSizeCallback() {

	}

	@Override
	public void maxTextSizeCallback() {

	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		switch (requestCode) {
			case Const.REQUEST_PERMISSION_READ_PHONE_STATE:
			{
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.e("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_phone_state_allow), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(PincodeRegActivity.this);
										finish();
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                        finish();
                                    }
                                });
                    }else{
                    	finish();
					}
				}
			}
			break;

			default:
				break;
		}
	}

	@Override
	public void i3GProcessFinish(DeviceResult deviceResult) {
		final Map param = new HashMap();
		String url = null;
		if (mCommonUserInfo.isNewUser()) {
			url = WasServiceUrl.CMM0010300A03.getServiceUrl();
			param.put("CI_NO", mCommonUserInfo.getCInumber());
			param.put("DI_NO", mCommonUserInfo.getDInumber());
			param.put("CUST_NM", mCommonUserInfo.getName());
			param.put("BRDD", mCommonUserInfo.getBirth());
			param.put("SEX_CD", mCommonUserInfo.getSexCode());
			param.put("DEVICE_INFO3", FDSManager.getInstance().getEncIMEI121(PincodeRegActivity.this));
			param.put("TRMN_APSF_VRSN", Utils.getVersionName(PincodeRegActivity.this));
			param.put("TRMN_OPSY_VRSN", Build.VERSION.RELEASE);
			param.put("CMCM_NM", Utils.getOperator(PincodeRegActivity.this));
		} else {
			url = WasServiceUrl.CMM0010300A04.getServiceUrl();
			param.put("MBR_NO", mCommonUserInfo.getMBRnumber());
			param.put("DEVICE_INFO3", FDSManager.getInstance().getEncIMEI121(PincodeRegActivity.this));
			param.put("TRMN_APSF_VRSN", Utils.getVersionName(PincodeRegActivity.this));
			param.put("TRMN_OPSY_VRSN", Build.VERSION.RELEASE);
			param.put("CMCM_NM", Utils.getOperator(PincodeRegActivity.this));
		}
		//전화번호
		if (mCommonUserInfo.getPhoneNumber().length() == 11) {
			param.put("CLPH_LCNO", mCommonUserInfo.getPhoneNumber().substring(0, 3));
			param.put("CLPH_TONO", mCommonUserInfo.getPhoneNumber().substring(3, 7));
			param.put("CLPH_SRNO", mCommonUserInfo.getPhoneNumber().substring(7, 11));
		} else if (mCommonUserInfo.getPhoneNumber().length() == 10) {
			param.put("CLPH_LCNO", mCommonUserInfo.getPhoneNumber().substring(0, 3));
			param.put("CLPH_TONO", mCommonUserInfo.getPhoneNumber().substring(3, 6));
			param.put("CLPH_SRNO", mCommonUserInfo.getPhoneNumber().substring(6, 10));
		}
		// 이메일주소
		param.put("EMAD", mCommonUserInfo.getEmailAddress());

		// 생체지원가능여부
		if (StonePassUtils.hasFingerprintDevice(this) != 0) {
			param.put("DEVICE_SECU_SUPR_EQMT_YN", Const.BRIDGE_RESULT_NO);
		} else {
			param.put("DEVICE_SECU_SUPR_EQMT_YN", Const.BRIDGE_RESULT_YES);
		}

		// 패턴등록여부
		param.put("PATR_REG_YN", Const.BRIDGE_RESULT_YES);
		//생체인증등록여부
		if (!mCommonUserInfo.isNewUser()) {
			param.put("BNC_ATHN_REG_YN", Const.BRIDGE_RESULT_NO);
		}
		else {
			if (StonePassUtils.isUsableFingerprint(this) == 0 && Prefer.getFidoRegStatus(PincodeRegActivity.this))
				param.put("BNC_ATHN_REG_YN", Const.BRIDGE_RESULT_YES);
			else
				param.put("BNC_ATHN_REG_YN", Const.BRIDGE_RESULT_NO);
		}

		Logs.e("CommonUserInfo - param : " + param.toString());

		// 레코드 동의 항목
		JSONObject jsonobj = new JSONObject();
		JSONArray jsonarray = new JSONArray();

		try {
			// 필수약관
			jsonobj.put("MBR_AGR_ITEM_CD", "1008");
			jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			jsonobj.put("MBR_AGR_ITEM_CD", "1009");
			jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			jsonobj.put("MBR_AGR_ITEM_CD", "1024");
			jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			jsonobj.put("MBR_AGR_ITEM_CD", "1000");
			jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			jsonobj.put("MBR_AGR_ITEM_CD", "1030");//간편이체
			jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();

			// 선택약관
			jsonobj.put("MBR_AGR_ITEM_CD", "1002");
			if(mCommonUserInfo.isOptionalPolicyPush()) {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			}
			else {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_NO);
			}
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			jsonobj.put("MBR_AGR_ITEM_CD", "1003");
			if(mCommonUserInfo.isOptionalPolicySMS()) {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			}
			else {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_NO);
			}
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			// 금융보안서비스알림
			jsonobj.put("MBR_AGR_ITEM_CD", "304");
			if(mCommonUserInfo.isSecurityPolicyPush()) {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			}
			else {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_NO);
			}
			jsonarray.put(jsonobj);
			jsonobj = new JSONObject();
			jsonobj.put("MBR_AGR_ITEM_CD", "305");
			if(mCommonUserInfo.isSecurityPolicySMS()) {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_YES);
			}
			else {
				jsonobj.put("MBR_AGR_YN", Const.BRIDGE_RESULT_NO);
			}
			jsonarray.put(jsonobj);
		} catch (JSONException e) {
			//e.printStackTrace();
		}
		param.put("REC_AGR_ITEM", jsonarray);

		// 단말정보 801
		mCommonUserInfo.setnData(deviceResult.getNatip());
		mCommonUserInfo.setwData(deviceResult.getResultStr());
		param.put("W_DATA", mCommonUserInfo.getwData());
		param.put("N_DATA", mCommonUserInfo.getnData());

		Logs.i("i3GProcessFinish - N_DATA : " + mCommonUserInfo.getnData());
		Logs.i("i3GProcessFinish - W_DATA : " + mCommonUserInfo.getwData());

		mCommonUserInfo.setDeviceInfo01(FDSManager.getInstance().getEncIMEI(this));
		mCommonUserInfo.setDeviceInfo02(FDSManager.getInstance().getEncUUID(this));

		Logs.i("encIMEI : " + mCommonUserInfo.getDeviceInfo01());
		Logs.i("encUUID : " + mCommonUserInfo.getDeviceInfo02());

		// 단말정보 802
		param.put("DEVICE_OS", "android");
		param.put("DEVICE_INFO1", mCommonUserInfo.getDeviceInfo01());
		param.put("DEVICE_INFO2", mCommonUserInfo.getDeviceInfo02());

		Logs.i("param : " + param.toString());

		final String finalUrl = url;
		HttpUtils.sendHttpTask(finalUrl, param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				if (TextUtils.isEmpty(ret)) {
					dismissProgressDialog();
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					return;
				}

				try {
					JSONObject object = new JSONObject(ret);
					if (object == null) {
						dismissProgressDialog();
						Logs.e(getResources().getString(R.string.msg_debug_err_response));
						Logs.e(ret);
						return;
					}

					JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
					if (objectHead == null) {
						dismissProgressDialog();
						DialogUtil.alert(PincodeRegActivity.this, getString(R.string.common_msg_no_reponse_value_was), new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								finish();
							}
						});
						return;
					}

					String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
					if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
						String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
						if (TextUtils.isEmpty(msg))
							msg = getString(R.string.common_msg_no_reponse_value_was);

						Logs.e(ret);
						dismissProgressDialog();

						String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
						CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
							@Override
							public void onConfirmPress() {
								finish();
							}
						};
						showCommonErrorDialog(msg, errCode, "",  objectHead, true, okClick);
						return;
					}


					if (mCommonUserInfo.isNewUser()) {
						String mbrNo = object.optString("MBR_NO");
						String crdtLoanYn = object.optString("CRDT_LOAN_YN");

						LoginUserInfo.getInstance().setMBR_NO(mbrNo);
						mCommonUserInfo.setCRDT_LOAN_YN(crdtLoanYn);
						requestResetErrorCount(PINCODE_REG_STATE_JOIN, null);
					} else {
						requestResetErrorCount(PINCODE_REG_STATE_CHANGE_DEVICE, null);
					}

				} catch (JSONException e) {
					Logs.printException(e);
				}
			}
		});
	}

	/**
	 * Extras 값 획득
	 */
	private void getExtra() {
		mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
		if(mCommonUserInfo.getPinLength() <= 0 || mCommonUserInfo.getPinLength() > Const.SSENSTONE_PINCODE_LENGTH)
			mCommonUserInfo.setPinLength(Const.SSENSTONE_PINCODE_LENGTH);
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mLayoutMsg = (LinearLayout) findViewById(R.id.layout_pin_msg);
		mTextDesc = (TextView) findViewById(R.id.textview_pin_desc);
		mTextMsg = (TextView) findViewById(R.id.textview_pin_msg);

		mExitPincode = (EditText) findViewById(R.id.layout_pincode_input).findViewById(R.id.editText);
		mEditPincode01 = (EditText) findViewById(R.id.edittext_pincode_01);
		mEditPincode01.setOnTouchListener(this);
		mImagePincode01 = (ImageView) findViewById(R.id.imageview_pincode_01);
		mEditPincode01.setOnKeyListener(null);
		mEditPincode02 = (EditText) findViewById(R.id.edittext_pincode_02);
		mEditPincode02.setOnTouchListener(this);
		mEditPincode02.setOnKeyListener(null);
		mImagePincode02 = (ImageView) findViewById(R.id.imageview_pincode_02);
		mEditPincode03 = (EditText) findViewById(R.id.edittext_pincode_03);
		mEditPincode03.setOnTouchListener(this);
		mEditPincode03.setOnKeyListener(null);
		mImagePincode03 = (ImageView) findViewById(R.id.imageview_pincode_03);
		mEditPincode04 = (EditText) findViewById(R.id.edittext_pincode_04);
		mEditPincode04.setOnTouchListener(this);
		mEditPincode04.setOnKeyListener(null);
		mImagePincode04 = (ImageView) findViewById(R.id.imageview_pincode_04);
		RelativeLayout layoutPincode05 = (RelativeLayout) findViewById(R.id.layout_pincode_05);
		mEditPincode05 = (EditText) findViewById(R.id.edittext_pincode_05);
		mEditPincode05.setOnTouchListener(this);
		mEditPincode05.setOnKeyListener(null);
		RelativeLayout layoutPincode06 = (RelativeLayout) findViewById(R.id.layout_pincode_06);
		mImagePincode05 = (ImageView) findViewById(R.id.imageview_pincode_05);
		mEditPincode06 = (EditText) findViewById(R.id.edittext_pincode_06);
		mEditPincode06.setOnTouchListener(this);
		mEditPincode06.setOnKeyListener(null);
		mImagePincode06 = (ImageView) findViewById(R.id.imageview_pincode_06);

		if (mCommonUserInfo.getPinLength() == 4) {
			layoutPincode05.setVisibility(View.GONE);
			layoutPincode06.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getPinLength() == 5) {
			layoutPincode06.setVisibility(View.GONE);
		}

		mTKMgr = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
				TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX,
				"Pin Code",//label
				"",//hint
				mCommonUserInfo.getPinLength(),  //max length
				"",//max length msg
				mCommonUserInfo.getPinLength(),  //min length
				"", //min length msg
				5,
				true,
				(FrameLayout) findViewById(R.id.keypadContainer),
				mExitPincode,
				(HorizontalScrollView) (findViewById(R.id.layout_pincode_input)).findViewById(R.id.keyscroll),
				(LinearLayout) (findViewById(R.id.layout_pincode_input)).findViewById(R.id.keylayout),
				(ImageButton) (findViewById(R.id.layout_pincode_input)).findViewById(R.id.clearall),
				(RelativeLayout) findViewById(R.id.keypadBallon),
				null,
				false,
				true);
		mTKMgr.showKeypad(mKeypadType);

		TextView textClose = (TextView) findViewById(R.id.tv_cancel);
		if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE || mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
			textClose.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE) ?
							getResources().getString(R.string.msg_cancel_pin_change) : getResources().getString(R.string.msg_cancel_pin_rereg);
					showCancelMessage(msg);
					return;
				}
			});
		} else {
			textClose.setVisibility(View.INVISIBLE);
		}

		if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
			mTextDesc.setText(R.string.msg_reg_pin);
		else
			mTextDesc.setText(R.string.msg_reg_new_pin);

		mInputCearDelayHandler = new Handler();
	}

	@Override
	public void stonePassResult(String op, int errorCode, String errorMsg) {
		/*
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				dismissProgressDialog();
			}
		});
		*/

		Logs.e("Pattern - stonePassResult : errorCode[" + errorCode + "], errorMsg[" + errorMsg + "]");

		//화면이 종료되면 리턴시킨다.
		if(isFinishing()) return;

		String msg ="";

		switch (errorCode) {
			case 1200: {
				Logs.e("패턴 등록 1200 완료");
				Prefer.setPatternErrorCount(this, 0);
				if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
					switch (mCommonUserInfo.getEntryStart()) {
						case AUTH_TOOL_MANAGE_PATTERN: {
							msg = getResources().getString(R.string.msg_ok_rereg_pattern);
							break;
						}

						default: {
							msg = getResources().getString(R.string.msg_ok_reg);
							break;
						}
					}

					Prefer.setPatternRegStatus(this, true);
				}
				break;
			}

			default: {
				msg = getResources().getString(R.string.msg_fail_reg);
				if ((mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE))
					Prefer.setPatternRegStatus(this, false);
				break;
			}
		}

		if (errorCode == 1200) {
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
				// 가입 요청 시작 : 단말정보 801 요청
				FDSManager.getInstance().getFDSInfo(this,this);
			} else {
				final String finalMsg = msg;
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						requestResetErrorCount(PINCODE_REG_STATE_MANAGE, finalMsg);
					}
				});
			}
			return;
		} else {
			final String finalmsg = msg;
			// 등록 중 에러 발생 시 종료하지 않고 다시 등록시도
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					dismissProgressDialog();
					showErrorMessage(finalmsg);
					goRegStep01();
				}
			});
		}
	}

	/**
	 * 핀코드 입력값 화면 초기화
	 */
	private void clearInput() {
		mEditPincode01.setText("");
		mImagePincode01.setImageResource(R.drawable.ico_pin_off);
		mEditPincode02.setText("");
		mImagePincode02.setImageResource(R.drawable.ico_pin_off);
		mEditPincode03.setText("");
		mImagePincode03.setImageResource(R.drawable.ico_pin_off);
		mEditPincode04.setText("");
		mImagePincode04.setImageResource(R.drawable.ico_pin_off);
		mEditPincode05.setText("");
		mImagePincode05.setImageResource(R.drawable.ico_pin_off);
		mEditPincode06.setText("");
		mImagePincode06.setImageResource(R.drawable.ico_pin_off);

		mEditPincode01.requestFocus();
	}

	/**
	 * 등록 시, 두번의 핀코드 입력값 비교
	 * @param orgPin 첫번째 입력 핀코드값
	 * @param newPin 두번째 입력 핀코드값
	 * @return
	 */
	private boolean isEqualsPin(String orgPin, String newPin) {
		if (TextUtils.isEmpty(orgPin) || TextUtils.isEmpty(newPin))
			return false;

		if (orgPin.length() != newPin.length())
			return false;

		if (orgPin.equalsIgnoreCase(newPin))
			return true;

		return false;
	}

	/**
	 * 키패드 보이기
	 */
	private void showKeyPad() {
		//clearInput();
		mExitPincode.requestFocus();
		mTKMgr.showKeypad(mKeypadType);
		mIsViewCtrlKeypad = true;

		mInputCearDelayHandler.postDelayed(new Runnable() {
			@Override
			public void run() {
				clearInput();
			}
		}, 400);
	}

	/**
	 * ssenstion 등록 전송
	 * @param plainData
	 */
	private void sendRegSSenstone(final String plainData) {

		final String codeRet;

		codeRet = StonePassManager.getInstance(getApplication()).registPin(plainData, null);
		Logs.e("codeRet : " + codeRet);
		if (codeRet.equals("P000")) {
			Prefer.setPincodeRegStatus(this,true);
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
				//showProgressDialog();
				StonePassManager.getInstance(getApplication()).ssenstoneFIDO(
						PincodeRegActivity.this, Const.SSENSTONE_REGISTER, null, "PATTERN", mCommonUserInfo.getPatternData());
				return;
			} else if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE) {
                PincodeRegActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
						requestResetErrorCount(PINCODE_REG_STATE_MANAGE_PINCODE, getResources().getString(R.string.msg_ok_change_pincode));
                    }
                });
            } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
				PincodeRegActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
					    String msg = "";
					    switch (mCommonUserInfo.getEntryStart()) {
							case AUTH_TOOL_MANAGE_PINCODE_RESET: {
								msg = getResources().getString(R.string.msg_ok_reset_pincode_error_count);
								break;
							}

							case TRANSFER:
							case WEB_CALL:
                            case AUTH_TOOL_MANAGE_PINCODE: {
								msg = getResources().getString(R.string.msg_ok_change_pincode);
                                //msg = getResources().getString(R.string.msg_ok_rereg_pincode);
                                break;
                            }

                            case AUTH_TOOL_MANAGE_PATTERN: {
								//showProgressDialog();
								StonePassManager.getInstance(getApplication()).ssenstoneFIDO(
										PincodeRegActivity.this, Const.SSENSTONE_REGISTER, null, "PATTERN", mCommonUserInfo.getPatternData());
								return;
                            }

                            case AUTH_TOOL_MANAGE_BIO: {
                                msg = getResources().getString(R.string.msg_ok_reg_fingerprint);
                                break;
                            }

                            case LOGIN_PATTERN:
                            case LOGIN_BIO: {
								requestResetErrorCount(PINCODE_REG_STATE_MANAGE_LOGIN, null);
                                return;
                            }

							default:
								break;
                        }
						if (mCommonUserInfo.getEntryStart() == EntryPoint.AUTH_TOOL_MANAGE_BIO && mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
							requestResetErrorCount(PINCODE_REG_STATE_MANAGE_BIO, msg);
						} else if (mCommonUserInfo.getEntryStart() == EntryPoint.WEB_CALL || mCommonUserInfo.getEntryStart() == EntryPoint.TRANSFER) {
							requestResetErrorCount(PINCODE_REG_STATE_MANAGE_PINCODE, msg);
						} else {
							requestResetErrorCount(PINCODE_REG_STATE_MANAGE_PINCODE_FORGET, msg);
						}
					}
				});
				return;
			}
			else {
				final String msg = getResources().getString(R.string.msg_ok_reg_pincode);
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						requestResetErrorCount(PINCODE_REG_STATE_ELSE, msg);
					}
				});
			}
		} else {
			PincodeRegActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					clearInput();
					dismissProgressDialog();
					showKeyPad();
					PinErrorDialog errorDialog = new PinErrorDialog(PincodeRegActivity.this, codeRet, "", null);
					errorDialog.show();
				}
			});
		}
	}

	private void backStep() {
		if (mRegStep == REG_STEP.STEP_01) {
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
				// 서비스 가입 등록 시 뒤로 가기 없음
				/*
                Prefer.setPincodeRegStatus(this, false);

				Intent intent;
				boolean bRet = Prefer.getFidoRegStatus(this);
				if (bRet) {
                    Prefer.setFidoRegStatus(this, false);
					intent = new Intent(this, FingerprintActivity.class);

				} else {
                    Prefer.setPatternRegStatus(this, false);
                    intent = new Intent(this, PatternRegActivity.class);
                }

				mCommonUserInfo.setAccessType(Const.SSENSTONE_ACCESS_FIRST_REG);
				mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
				intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
				startActivity(intent);
				finish();
				*/
			} else if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE || mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
				String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) ?
						getResources().getString(R.string.msg_cancel_pin_rereg) : getResources().getString(R.string.msg_cancel_pin_change);
				showCancelMessage(msg);
				return;
			}
		} else if (mRegStep == REG_STEP.STEP_02) {
			//step1으로 뒤로 가기 없음. 틀렸을 경우에만 뒤로 감
			/*
			mFirstPin = null;
			mTextDesc.setText(R.string.msg_input_pin);
			mRegStep = REG_STEP.STEP_01;
			showKeyPad();
			*/
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE || mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
				String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) ?
						getResources().getString(R.string.msg_cancel_pin_rereg) : getResources().getString(R.string.msg_cancel_pin_change);
				showCancelMessage(msg);
				return;
			}
		}
	}

	/**
	 * 첫번째 패턴 입력 표시
	 */
	private void goRegStep01() {
		mFirstPin = null;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
					mTextDesc.setText(R.string.msg_reg_pin);
				else
					mTextDesc.setText(R.string.msg_reg_new_pin);
			}
		});
		mRegStep = REG_STEP.STEP_01;
		showKeyPad();
	}

	private void requestPinErrorSMS(boolean isReReg) {
		Map param = new HashMap();

		param.put("SMS_MSG_CD", "B002");
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0010800A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
			}
		});
	}

	private void requestResetErrorCount(final int state, final String msg) {
		final Map param = new HashMap();
		String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
		if (TextUtils.isEmpty(mbrNo))
			mbrNo = mCommonUserInfo.getMBRnumber();

		if (TextUtils.isEmpty(mbrNo))
			param.put("MBR_NO", "");
		else
			param.put("MBR_NO", mbrNo);
		param.put("PN_ATHN_RSLT_CD", "00");
		param.put("SCRN_ID", "");
		param.put("STONE_PASS_USER_ID", "");
		param.put("ELEC_SGNR_KEY_VAL", "");
		param.put("ELEC_SGNR_BZWR_DVCD", "");
		param.put("TRN_CD", "");
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0010800A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				dismissProgressDialog();
				if (TextUtils.isEmpty(ret)) {
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					PincodeRegActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
							showKeyPad();
						}
					});
					return;
				}
				try {
					if (!TextUtils.isEmpty(ret)) {
						JSONObject object = new JSONObject(ret);
						if (object == null) {
							Logs.e(getResources().getString(R.string.msg_debug_err_response));
							Logs.e(ret);
							PincodeRegActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
									showKeyPad();
								}
							});
							return;
						}

						Logs.e(ret);

						final JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
						if (objectHead == null) {
							PincodeRegActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
									showKeyPad();
								}
							});
							return;
						}

						String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
						final String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
						if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
							if ("CMM0037".equals(errCode)) {
								//로그인 중 분실신고기기 체크
								if (Utils.isAppOnForeground(PincodeRegActivity.this)) {
									LogoutTimeChecker.getInstance(PincodeRegActivity.this).autoLogoutStop();
									SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
									mApplicationClass.allActivityFinish(true);
									LoginUserInfo.clearInstance();
									LoginUserInfo.getInstance().setLogin(false);
									Intent intent = new Intent(PincodeRegActivity.this, LogoutActivity.class);
									intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
									startActivity(intent);
								} else {
									LoginUserInfo.getInstance().setLogin(false);
									LogoutTimeChecker.getInstance(PincodeRegActivity.this).autoLogoutStop();
									LogoutTimeChecker.getInstance(PincodeRegActivity.this).setBackground(true);
								}
							} else {
								String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
								if (TextUtils.isEmpty(msg))
									msg = getString(R.string.common_msg_no_reponse_value_was);
								final String finalMsg = msg;
								PincodeRegActivity.this.runOnUiThread(new Runnable() {
									@Override
									public void run() {
										showCommonErrorDialog(finalMsg, errCode, "", objectHead, true);
										showKeyPad();
									}
								});
							}
							return;
						}
					}
				} catch (JSONException e) {
					Logs.printException(e);
					PincodeRegActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
							showKeyPad();
						}
					});
					return;
				}

				Prefer.setPinErrorCount(PincodeRegActivity.this, 0);

				switch (state) {
					case PINCODE_REG_STATE_JOIN: {
						String MOTPSerial = LoginUserInfo.getInstance().getMOTPSerialNumber();
						if (!TextUtils.isEmpty(MOTPSerial)) {
							MOTPManager.getInstance(getApplication()).reqDissue(new MOTPManager.MOTPEventListener() {
								@Override
								public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
								}
							}, MOTPSerial);
						}
						Intent intent = new Intent(PincodeRegActivity.this, CompleteMemberActivity.class);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						startActivity(intent);
						finish();
						return;
					}

					case PINCODE_REG_STATE_CHANGE_DEVICE: {
						String MOTPSerial = LoginUserInfo.getInstance().getMOTPSerialNumber();
						if (!TextUtils.isEmpty(MOTPSerial)) {
							MOTPManager.getInstance(getApplication()).reqDissue(new MOTPManager.MOTPEventListener() {
								@Override
								public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
								}
							}, MOTPSerial);
						}
						Intent intent = new Intent(PincodeRegActivity.this, CompleteChangePhoneActivity.class);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						startActivity(intent);
						finish();
						return;
					}

					case PINCODE_REG_STATE_MANAGE: {
						requestPinErrorSMS(false);
						DialogUtil.alert(PincodeRegActivity.this,
							msg,
							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									LoginUserInfo.getInstance().setCompletedReg(true);
									finish();
								}
							});
						return;
					}

					case PINCODE_REG_STATE_MANAGE_PINCODE: {
						requestPinErrorSMS(false);
						DialogUtil.alert(PincodeRegActivity.this,
							msg,
							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									finish();
								}
							});
						return;
					}

					case PINCODE_REG_STATE_MANAGE_LOGIN: {
						requestPinErrorSMS(false);
						finish();
						return;
					}

					case PINCODE_REG_STATE_MANAGE_BIO: {
						FidoDialog fidoDialog = DialogUtil.fido(PincodeRegActivity.this, false,
								new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										Prefer.setFidoRegStatus(PincodeRegActivity.this, true);
										LoginUserInfo.getInstance().setCompletedReg(true);
										finish();
									}
								},
								null
						);
						fidoDialog.mBackKeyListener = new Dialog.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								Prefer.setFidoRegStatus(PincodeRegActivity.this, true);
								LoginUserInfo.getInstance().setCompletedReg(true);
							}
						};
						TextView textDesc = fidoDialog.findViewById(R.id.textview_fido_dialog_desc);
						textDesc.setText(msg);
						fidoDialog.setCanceledOnTouchOutside(false);
						fidoDialog.setCancelable(false);
						fidoDialog.show();
						return;
					}

					case PINCODE_REG_STATE_MANAGE_PINCODE_FORGET: {
						requestPinErrorSMS(true);
						DialogUtil.alert(PincodeRegActivity.this,
							msg,
							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									LoginUserInfo.getInstance().setCompletedReg(true);
									finish();
								}
							});
						return;
					}

					case PINCODE_REG_STATE_ELSE: {
						requestPinErrorSMS(false);
						Intent intent = new Intent();
						mCommonUserInfo.setResultMsg(msg);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						setResult(RESULT_OK, intent);
						finish();
						return;
					}

					default:
						break;
				}
			}
		});
	}
}