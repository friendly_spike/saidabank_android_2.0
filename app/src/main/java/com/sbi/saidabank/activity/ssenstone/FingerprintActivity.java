package com.sbi.saidabank.activity.ssenstone;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.login.ReregChangeFingerPrintActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;

/**
 * Saidabanking_android
 *
 * Class: FingerprintActivity
 * Created by 950485 on 2018. 9. 4..
 * <p>
 * Description:지문인식을 위한 화면
 */

public class FingerprintActivity extends BaseActivity implements StonePassManager.FingerPrintDlgListener, StonePassManager.StonePassListener {
    private FidoDialog    mFingerprintDlg;

    private Button        mAuthBtn;
    private String        mBioType = "FINGERPRINT";

    private CommonUserInfo mCommonUserInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint);

        getExtra();
        initUX();

//        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
//            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"7", "");

        if ((mCommonUserInfo.getOperation().equals(Const.SSENSTONE_AUTHENTICATE))
                && Prefer.getFlagFingerPrintChange(this)) {
            // 로그인/인증 시 지문변경 상태면 패턴로그인
            Logs.showToast(this, "지문정보 변경. 재등록 후 인증/로그인 가능");
            finish();
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            // 지문 재등록인 경우 바로 지문등록팝업 띄움
            showProgressDialog();
            StonePassManager.getInstance(getApplication()).ssenstoneFIDO(FingerprintActivity.this, mCommonUserInfo.getOperation(), mCommonUserInfo.getSignData(), mBioType, mBioType);
        }
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
            // 서비스 가입 등록 시 뒤로 가기 없음
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
            Intent intent = new Intent(this, Main2Activity.class);
            /*
            Intent intent = new Intent(this, ReregChangeFingerPrintActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            */
            startActivity(intent);
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void stonePassResult(String op,int errorCode, String errorMsg) {
        Logs.e("Fido - stonePassResult : errorCode[" + errorCode + "], errorMsg["+errorMsg + "]");

        //화면이 종료되면 리턴시킨다.
        if(isFinishing()) return;

        String msg="";
        dismissProgressDialog();
        switch (errorCode) {
            case 1: case 2: case 4:  case 8: case 9: case 11: case 1001:
                mFingerprintDlg.showFidoCautionMsg(true);
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
                    Prefer.setFidoRegStatus(this, false);
                return;

            default:
                break;
        }

        if (mFingerprintDlg != null)
            mFingerprintDlg.dismiss();

        switch (errorCode) {
            case 1200:
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    msg = "등록했습니다.";
                } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    msg = "인증했습니다.";
                } else {
                    msg = "해제했습니다.";
                    Prefer.setFidoRegStatus(this,false);
                }
                break;

            // 사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.
            case 1404:
                msg = "";

            case 1491:
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    msg = "다른 사용자 지문으로 이미 등록되어 있습니다.";
                } else if(op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    if (Prefer.getFidoRegStatus(this)) {
                        msg = "인증을 실패했습니다.";
                    } else {
                        msg = "사용자 정보가 없습니다. 먼저 등록하시기 바랍니다.";
                    }
                }
                break;

            case 0:
                msg = "해제했습니다.";
                break;

            case 3:
                msg = "취소하셨습니다.";
                break;

            case 10:
                msg = errorMsg;
                break;

            case 9998:
                msg = "서버로 부터 리턴 값이 널(Null)입니다..";
                break;

            case 9999:
                msg = "작업중 json에러가 발생했습니다.";
                break;

            default:
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    msg = "등록을 실패했습니다.";
                } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
                    msg = "인증을 실패했습니다.";
                } else {
                    msg = "해제를 실패했습니다.";
                }
                break;
        }

        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
            if (errorCode == 3)
                return;

            if (errorCode == 1200) {
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
                    Prefer.setFidoRegStatus(this, true);
                    Prefer.setFlagFingerPrintChange(this, false);
                    showCompletePrintDialog();
                } else {
                    Intent intent = new Intent(FingerprintActivity.this, PincodeAuthActivity.class);
                    mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                    finish();
                }
                return;
            } else {
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
                    Prefer.setFidoRegStatus(this, false);
            }
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
            if (errorCode == 200) {
                // 지문 추가 시 재등록 처리
                Prefer.setFlagFingerPrintChange(this, true);
                Prefer.setFidoRegStatus(this, false);
                Intent intent = new Intent(this, ReregChangeFingerPrintActivity.class);
                mCommonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
                mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
                finish();
                /*
                // 지문 추가되었을 때 재등록 요청
                DialogUtil.alert(this, "지문 정보가 변경되었습니다.\n다시 등록하시기 바랍니다.", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                */
            } else {
                loginPattern();
            }
        }
    }

    @Override
    public void showFingerPrintDialog() {
        dismissProgressDialog();
        if(mFingerprintDlg == null) {
            mFingerprintDlg = DialogUtil.fido(this, true,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                            Prefer.setFidoRegStatus(FingerprintActivity.this, false);
                            mFingerprintDlg.dismiss();
                        }
                    },
                    null
            );
            mFingerprintDlg.showFidoCautionMsg(false);
            mFingerprintDlg.setCanceledOnTouchOutside(false);
        }

        if (mFingerprintDlg != null) {
            mFingerprintDlg.show();
        }
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mAuthBtn = (Button) findViewById(R.id.btn_reg_fingerprint);
        mAuthBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressDialog();
                StonePassManager.getInstance(getApplication()).ssenstoneFIDO(FingerprintActivity.this, mCommonUserInfo.getOperation(), mCommonUserInfo.getSignData(), mBioType, mBioType);
            }
        });

        Button btnCancel =  (Button) findViewById(R.id.btn_cancel_fingerprint);
        btnCancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Prefer.setFidoRegStatus(FingerprintActivity.this, false);

                if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
                    Intent intent = new Intent(FingerprintActivity.this, PincodeRegActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                    finish();
                } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
                    Intent intent = new Intent(FingerprintActivity.this, Main2Activity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    /**
     * 패턴 로그인으로 이동
     */
    private void loginPattern() {
        Intent intent = new Intent(this, PatternAuthActivity.class);
        mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
        startActivity(intent);
        finish();
    }

    /**
     * 지문 등록 완료창 표시
     */
    public void showCompletePrintDialog() {
        FidoDialog fidoDialog = DialogUtil.fido(this, false,true,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent;
                        Prefer.setFidoRegStatus(FingerprintActivity.this, true);
                        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
                            intent = new Intent(FingerprintActivity.this, PincodeRegActivity.class);
                            mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                        } else {
                            intent = new Intent(FingerprintActivity.this, PincodeAuthActivity.class);
                            mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        }
                        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                        startActivity(intent);
                        finish();
                    }
                },
                new Dialog.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialog) {
                    }
                }
        );

        TextView textDesc = fidoDialog.findViewById(R.id.textview_fido_dialog_desc);
        textDesc.setText(R.string.msg_ok_reg_fingerprint);
        fidoDialog.setCanceledOnTouchOutside(false);
        fidoDialog.setCancelable(false);
        fidoDialog.show();
    }
}
