package com.sbi.saidabank.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.core.content.res.ResourcesCompat;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.netfunnel.api.Property;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Foreground;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.LicenseKey;
import com.sullegro.typekit.Typekit;

import java.util.Iterator;
import java.util.Map;

//import com.ahnlab.enginesdk.SDKUtils;

//import com.ahnlab.enginesdk.SDKUtils;

/**
 * SaidaApplication : 어플리케이션
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
@SuppressLint("StaticFieldLeak")
public class SaidaApplication extends Application implements Application.ActivityLifecycleCallbacks{

    private static SaidaApplication mInstance;
    private Activity mActivity;
    private String mTelNo;
    private Context mAppContext = null;

    //현재 앱이 백그라운드 상태인지 포그라운드인지 상태값 저장.
    private boolean isNowBackGround;
    @Override
    public void onCreate() {
        super.onCreate();

//        if ( !isHuntingService() )   {
            mInstance = this;
            mAppContext = getApplicationContext();

            try {
                AppsFlyerLib.getInstance().init(LicenseKey.AF_DEV_KEY, mConversionListener, getAppContext());
                AppsFlyerLib.getInstance().setCollectIMEI(BuildConfig.DEBUG);
                AppsFlyerLib.getInstance().setCollectAndroidID(true);
                AppsFlyerLib.getInstance().startTracking(this, LicenseKey.AF_DEV_KEY);
                AppsFlyerLib.getInstance().setDebugLog(true);
            }catch (RuntimeException e){
                MLog.d();
            }

            try {
                //폰트 일괄적용을 위해 추가함.
                //1차에서 github라이브러리를 사용하던것을 SdkVersion 29로 올리면서 에러가 발생하여
                //적접 소스를 받아 문제되는 부분 모두 수정한 후 라이브러리로 새로 만들어 포함시켰다.
                Typekit.getInstance()
                        .addNormal(ResourcesCompat.getFont(mAppContext, R.font.nanumbarungothic))
                        .addBold(ResourcesCompat.getFont(mAppContext, R.font.nanumbarungothicbold))
                        .addItalic(ResourcesCompat.getFont(mAppContext, R.font.nanumbarungothiclight));
            }catch (Exception e){
                MLog.d();
            }


            // Netfunnel의 Default Property를 수정한다.
            Property property = Property.getDefaultInstance();
            // 넷퍼넬 문구및 옵션 추가
            property.setProtocol("https");// 넷퍼넬 서버 protocol (http / https)
            property.setHost("nf.sbisb.co.kr");// 넷퍼넬 서버 host (ip / domain)
            property.setPort(443);// 넷퍼넬 서버 port (80 / 443)
            property.setTimeout(3);// 넷퍼넬 서버 장애 여부를 판단하는 기준 값 (단위:초)
            property.setRetry(1);// 넷퍼넬 서버에서 응답이 없을 경우 retry하는 횟수 (단위:횟수)

            // Forground 콜백 리스너를 등록
            Foreground.get(this).addListener(mForgroundListener);

            registerActivityLifecycleCallbacks(this);
//        }
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        // Magisk 서비스 프로세스 실행여부 확인
//        isHuntingService();
    }
    @Override
    public void onTerminate() {
        MLog.d();
        super.onTerminate();
        Foreground.get(this).unregisterActivity(this);
        unregisterActivityLifecycleCallbacks(this);
    }

    @Override
    public void onLowMemory() {
        MLog.d();
        super.onLowMemory();
    }

    /**
     * 싱글톤 객체를 얻는다.
     *
     * @return SaidaApplication
     */
    public static SaidaApplication getInstance() {
        return mInstance;
    }

    /**
     * 생성자를 얻는다.
     *
     * @return Context
     */
    public Context getAppContext() {
        return mAppContext;
    }

    /**
     * 현재 액티비티를 얻는다.
     *
     * @return Activity
     */
    public Activity getActivity() {
        return mActivity;
    }

    /**
     * 현재 액티비티를 설정
     */
    public void setActivity(Activity activity) {
        this.mActivity = activity;
    }

    /**
     * 모든 떠있는 화면을 한번에 종료한다.
     *
     * @param includeMain TRUE이면 메인화면까지 닫아버린다. FALSE이면 메인 화면은 남겨둔다.
     */
    public void allActivityFinish(boolean includeMain) {
        MLog.d();
        Foreground foreground = Foreground.get(this);
        Iterator<String> keys = foreground.getAllActivity().keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            if (key.contains(".Main2Activity") && !includeMain) {
                continue;
            }
            Activity activity = foreground.getAllActivity().get(key);
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    /**
     * 모든 떠있는 화면을 한번에 종료한다.
     *
     * @param remainClassName 남겨두고자 하는 클래스 이름
     */
    public void allActivityFinish(String remainClassName) {
        MLog.d();
        Foreground foreground = Foreground.get(this);
        Iterator<String> keys = foreground.getAllActivity().keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            Logs.e("[allActivityFinish : "+  key +  "]");
            if (!TextUtils.isEmpty(remainClassName) && key.contains(remainClassName)) {
                continue;
            }
            Activity activity = foreground.getAllActivity().get(key);
            if (!activity.isFinishing()) {
                activity.finish();
            }
        }
    }

    public Activity getOpenActivity(String activityName) {
        MLog.d();
        Foreground foreground = Foreground.get(this);
        if (foreground.getAllActivity().size() > 0) {
            Iterator<String> keys = foreground.getAllActivity().keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                Logs.e("key : " + key);
                if (key.contains(activityName)) {
                    Activity activity = foreground.getAllActivity().get(key);
                    if (!activity.isFinishing()) {
                        return activity;
                    }
                }
            }
        }

        return null;
    }

    public int getActivitySize() {
        Foreground foreground = Foreground.get(this);
        return foreground.getAllActivity().size();
    }

    /**
     * 웹에서 전화걸기 시도시 전화번호 임시저장
     *
     * @param telNo 전화번호
     */
    public void setTempTelNo(String telNo) {
        this.mTelNo = telNo;
    }

    public String getTempTelNo() {
        return mTelNo;
    }

//    private boolean isHuntingService() {
//        String cmdline = "/proc/self/cmdline";
//        String line = null;
//        BufferedReader reader = null;
//        try {
//            reader = new BufferedReader(new FileReader(cmdline));
//            while ((line = reader.readLine()) != null) {
//                if (line.contains("hunting")) {
//                    return true;
//                }
//            }
//        } catch (Throwable ignored) {
//            // ignore
//        } finally {
//            SDKUtils.close(reader);
//        }
//        return false;
//    }

    /**
     * AppsFly 리스너
     */
    private AppsFlyerConversionListener mConversionListener = new AppsFlyerConversionListener() {

        @Override
        public void onInstallConversionDataLoaded(Map<String, String> conversionData) {
        }

        @Override
        public void onAppOpenAttribution(Map<String, String> conversionData) {
        }

        @Override
        public void onInstallConversionFailure(String errorMessage) {
            MLog.i("Error getting conversion data >> " + errorMessage);
        }

        @Override
        public void onAttributionFailure(String errorMessage) {
            MLog.i("Error onAttributionFailure >> " + errorMessage);
        }
    };

    /**
     * 포그라운드 감지 리스너
     */
    private Foreground.Listener mForgroundListener = new Foreground.Listener() {

        @Override
        public void onForeground() {
            MLog.d();
            if (!DataUtil.isNull(getActivity())
                    && getActivity() instanceof BaseActivity) {
                /**
                //20200618 - Intro에서는 루팅 체크하는 함수가 있다. 그외 화면에서 루팅 체크하도록 수정.
                if(!getActivity().getLocalClassName().contains("IntroActivity")){
                    ((BaseActivity) getActivity()).checkRoot();
                }*/
                ((BaseActivity) getActivity()).setForground(true);
            }
            isNowBackGround = false;
        }

        @Override
        public void onBackground() {
            MLog.d();
            isNowBackGround = true;
        }

    };

    public boolean getIsNowBackGround(){
        return isNowBackGround;
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {
//        Logs.e("=============================================================");
//        Logs.e("onActivityResumed activity : " + activity.getLocalClassName());
//        Logs.e("=============================================================");
    }

    @Override
    public void onActivityPaused(Activity activity) {
//        Logs.e("=============================================================");
//        Logs.e("onActivityPaused activity : " + activity.getLocalClassName());
//        Logs.e("=============================================================");
    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        Logs.e("=============================================================");
        Logs.e("onActivityDestroyed activity : " + activity.getLocalClassName());
        Logs.e("=============================================================");
    }
}
