package com.sbi.saidabank.activity.login;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

/**
 * Saidabanking_android
 * <p>
 * Class: ReloginActivity
 * Created by 950485 on 2018. 01. 07..
 * <p>
 * Description:재로그인 화면
 */
public class ReloginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relogin);
        Button btnRelogin =  (Button) findViewById(R.id.btn_relogin);
        btnRelogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}
