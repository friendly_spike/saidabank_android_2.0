package com.sbi.saidabank.activity.test;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.FileProvider;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.download.DownloadFileAsync;
import com.sbi.saidabank.common.download.DownloadFileCallback;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;


public class NewApkDownloadActivity extends BaseActivity implements View.OnClickListener, DownloadFileCallback {
    private String mApkName;
    private String mHistory;
    private TextView mEtHistory;
    private TextView mFileName;
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apk_download);

        mEtHistory = findViewById(R.id.tv_history);
        mFileName = findViewById(R.id.tv_file_name);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);

        reqeustVersionInfo();

        deletePreDownloadApkFile();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(pDialog != null){
            pDialog.dismiss();
        }
    }

    //기존 받아두었던 apk파일들을 모두 삭제한다.
    private void deletePreDownloadApkFile(){
        File dirFile = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        if(!dirFile.exists()){
            dirFile.mkdirs();
        }

        String[] str = dirFile.list();
        for(String st : str) {
            if(st.endsWith(".apk")){
                File apkFile = new File(dirFile,st);
                apkFile.delete();
            }
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.btn_ok:
                startDownLoadAPK();
                break;
            case R.id.btn_cancel:
                finish();
                break;
            default:
                break;
        }

    }

    private void startDownLoadAPK(){
        if(TextUtils.isEmpty(mApkName)){
            DialogUtil.alert(NewApkDownloadActivity.this,"파일이름을 가져올 수 없습니다.");
            return;
        }

        if(pDialog == null){
            pDialog = new ProgressDialog(this);
            pDialog.setMessage(this.getString(R.string.app_name) + " 설치 파일을 다운로드 합니다.");
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

            pDialog.setMax(100);
            pDialog.setProgress(0);
            pDialog.setCancelable(false);
            pDialog.show();
        }


        String url = SaidaUrl.getBaseMWebUrl() + "/app2/aos/" + mApkName;

        DownloadFileAsync mDfa = new DownloadFileAsync(NewApkDownloadActivity.this,this);
        mDfa.execute(url);
    }

    private void reqeustVersionInfo(){
        String url = SaidaUrl.getBaseMWebUrl() + "/app2/aos/aos_history.txt";

        final String historyUrl = url;

        showProgressDialog();
        new Thread(new Runnable() {
            @Override
            public void run() {
                String ret = requestGet(historyUrl);

                dismissProgressDialog();

                if (DataUtil.isNull(ret) || ret.contains("페이지를 표시할 수 없습니다")) {
                    DialogUtil.alert(NewApkDownloadActivity.this,"변경 이력 파일을 찾을수 없습니다.");
                    return;
                }

                Logs.e(ret);

                try {
                    JSONArray objectArray = new JSONArray(ret);
                    if(objectArray.length() > 0){
                        StringBuilder builder = new StringBuilder();
                        for (int i=0;i<objectArray.length();i++){
                            JSONObject object = (JSONObject) objectArray.get(i);

                            String fileName = object.optString("file_name");
                            String history = object.optString("history");
                            if(!TextUtils.isEmpty(history)){
                                history = history.replace("<br>","\n  ");
                            }

                            if(i == 0){
                                mApkName = fileName;
                            }

                            builder.append(history);
                            builder.append("\n\n");
                        }
                        mHistory = builder.toString();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mFileName.setText(mApkName);
                                mEtHistory.setText(mHistory);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }).start();
    }

    //서버에서 txt파일을 다운로드 받아야 하는데.. 캐릭터 타입이 utf8이라 새로 만들었다.
    //SBI의 서버 타입은 euc-kr이라 기존 Http sender를 사용하면 캐릭터셋이 깨진다.
    private String requestGet(String urlStr) {

        StringBuilder sBuilder = new StringBuilder();
        String body = "";
        URL url = null;
        HttpURLConnection mConn = null;

        try {

            url = new URL(urlStr);

            if (url.getProtocol().toLowerCase().equals("https")) {
                HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
                mConn = https;
            } else {
                mConn = (HttpURLConnection) url.openConnection();
            }

            if (mConn != null) {
                mConn.setConnectTimeout(60*1000);
                mConn.setReadTimeout(60*1000);
                mConn.setRequestMethod(Const.GET);
                mConn.setUseCaches(false);
                HttpUtils.setCooKie(urlStr, mConn);
                mConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
                mConn.setRequestProperty("User-Agent", HttpUtils.mUserAgent);

                int responseCode = mConn.getResponseCode();
                if (responseCode != HttpURLConnection.HTTP_OK) {
                    mConn.disconnect();
                    return body;
                }

                HttpUtils.saveCookie(mConn);
                BufferedReader br = new BufferedReader(new InputStreamReader(mConn.getInputStream(), Const.UTF_8));

                for (; ; ) {
                    String line = br.readLine();
                    if (line == null) {
                        break;
                    }
                    sBuilder.append(line).append('\n');
                }
                br.close();
                body = sBuilder.toString();
            }
        } catch (Exception e) {
            MLog.e(e);
        } finally {
            if (DataUtil.isNotNull(mConn))
                mConn.disconnect();
        }
        return body.trim();
    }

    @Override
    public void updateProgress(int per) {
        if(this.isFinishing()) return;
        if(pDialog != null && pDialog.isShowing())
            pDialog.setProgress(per);
    }

    @Override
    public void onDownloadComplete(boolean result, String pathName, String fileName) {
        Logs.e("onDownloadComplete - download_path :" + pathName + "/" + fileName);

        if(this.isFinishing()) return;

        if(!result){
            DialogUtil.alert(this,"서버에서 파일을 찾을 수 없습니다.\n설치파일이름 : " + mApkName);
            return;
        }

        File apkFile = new File(pathName,fileName);

        if(apkFile.exists()){
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                Uri apkUri = FileProvider.getUriForFile(this,BuildConfig.APPLICATION_ID + ".fileprovider",apkFile);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                intent.setDataAndType(apkUri, "application/vnd.android.package-archive");
                startActivity(intent);
            }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.N){

                Uri uri = FileProvider.getUriForFile(NewApkDownloadActivity.this, BuildConfig.APPLICATION_ID + ".fileprovider", apkFile);
                Intent intent = new Intent(Intent.ACTION_INSTALL_PACKAGE);
                intent.setDataAndType( Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(intent);
            }else{
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setDataAndType( Uri.fromFile(apkFile), "application/vnd.android.package-archive");
                i.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(i);
            }
            //다운로드 완료후 설치전에 쿠키를 모두 지워준다.
            HttpUtils.removeCookie();
            finish();
        }else{
            Logs.showToast(NewApkDownloadActivity.this,fileName + "파일을 찾을 수 없습니다.");
        }
    }
}
