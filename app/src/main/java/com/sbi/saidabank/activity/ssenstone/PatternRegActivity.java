package com.sbi.saidabank.activity.ssenstone;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.patternlockview.PatternLockView;
import com.sbi.saidabank.customview.patternlockview.listener.PatternLockViewListener;
import com.sbi.saidabank.customview.patternlockview.utils.PatternLockUtils;
import com.sbi.saidabank.customview.patternlockview.utils.ResourceUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;

import java.util.List;

/**
 * Saidabanking_android
 *
 * Class: PatternRegActivity
 * Created by 950485 on 2018. 9. 4..
 * <p>
 * Description:패턴 인증 등록을 위한 화면
 */

public class PatternRegActivity extends BaseActivity implements StonePassManager.StonePassListener, View.OnClickListener {
    /**
     * ssenstone 등록 단계
     */
    private enum REG_STEP {
        STEP_01,
        STEP_02
    }

    private TextView            mTextDesc;
    private LinearLayout        mLayoutMsg;
    private TextView            mTextMsg;
    private PatternLockView     mPatternLockView;

    private boolean             mIsProgress = false;

    private REG_STEP            mRegStep = REG_STEP.STEP_01;
    private String              mFirstPattern = null;

    private CommonUserInfo mCommonUserInfo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pattern_reg);

        getExtra();
        initUX();

//        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
//            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"6", "");
    }

    @Override
    public void onBackPressed() {
        if (mRegStep == REG_STEP.STEP_01) {
            if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
                //서비스가입 중 뒤로 가기 없음
                /*
                Prefer.setPatternRegStatus(this, false);
				Intent intent = new Intent(this, AuthRegisterGuideActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
				startActivity(intent);
				finish();
				*/
            } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) {
                String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) ?
                        getResources().getString(R.string.msg_cancel_pattern_change) : getResources().getString(R.string.msg_cancel_pattern_rereg);
                showCancelMessage(msg);
                return;
            }
            else {
                finish();
            }
        } else if (mRegStep == REG_STEP.STEP_02) {
            //step1으로 뒤로 가기 없음. 틀렸을 경우에만 뒤로 감
            //goRegStep01();
            if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) {
                String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) ?
                        getResources().getString(R.string.msg_cancel_pattern_change) : getResources().getString(R.string.msg_cancel_pattern_rereg);
                showCancelMessage(msg);
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    /*
     * 화면 초기화
     */
    private void initUX() {
        if(mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET || mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
            TextView textClose = findViewById(R.id.textview_close);
            textClose.setVisibility(View.VISIBLE);
            textClose.setOnClickListener(this);
        }

        mPatternLockView = (PatternLockView) findViewById(R.id.pattern_lockview_reg);
        mPatternLockView.setDotCount(3);
        mPatternLockView.setDotNormalSize((int) ResourceUtils.getDimensionInPx(this, R.dimen.pattern_dot_size));
        mPatternLockView.setDotSelectedSize((int) ResourceUtils.getDimensionInPx(this, R.dimen.pattern_selected_dot_size));
        mPatternLockView.setPathWidth((int) ResourceUtils.getDimensionInPx(this, R.dimen.pattern_path_width));
        mPatternLockView.setAspectRatioEnabled(true);
        //mPatternLockView.setAspectRatio(PatternLockView.AspectRatio.ASPECT_RATIO_HEIGHT_BIAS);
        mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
        mPatternLockView.setDotAnimationDuration(150);
        mPatternLockView.setPathEndAnimationDuration(100);
        mPatternLockView.setBackgroundColor(getResources().getColor(R.color.color3F444D));
        mPatternLockView.setNormalStateColor(Color.WHITE);
        mPatternLockView.setCorrectStateColor(Color.WHITE);
        mPatternLockView.setWrongStateColor(getResources().getColor(R.color.colorErr));
        mPatternLockView.setDrawStateColor(getResources().getColor(R.color.color00EBFF));
        mPatternLockView.setInStealthMode(Prefer.getUsePatternStealth(this));
        mPatternLockView.setTactileFeedbackEnabled(true);
        mPatternLockView.setInputEnabled(true);
        mPatternLockView.addPatternLockListener(mPatternLockViewListener);

        mTextDesc = (TextView) findViewById(R.id.textview_pattern_desc);
        mLayoutMsg = (LinearLayout) findViewById(R.id.layout_pattern_msg);
        mTextMsg = (TextView) findViewById(R.id.textview_pattern_msg);
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
            mTextDesc.setText(R.string.msg_reg_pattern);
        else
            mTextDesc.setText(R.string.msg_reg_new_pattern);

        RelativeLayout layoutPattern = (RelativeLayout) findViewById(R.id.layout_pattern_reg);
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        float density  = getResources().getDisplayMetrics().density;
        float dpHeight = outMetrics.heightPixels / density;
        float dpWidth = outMetrics.widthPixels / density;
        float rate = dpHeight / dpWidth;
        if (rate < 1.70) {
            ViewGroup.LayoutParams p = layoutPattern.getLayoutParams();
            float magin = Utils.dpToPixel(PatternRegActivity.this, 40);
            if (p instanceof LinearLayout.LayoutParams) {
                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutPattern.setLayoutParams(lp);
            } else if (p instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutPattern.setLayoutParams(lp);
            } else if (p instanceof ViewGroup.MarginLayoutParams) {
                ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) p;
                lp.setMargins(0, 0, 0, (int) magin);
                layoutPattern.requestLayout();
            }
        }
    }

    /**
     * 패턴 화면 이벤트 리스너
     */
    private PatternLockViewListener mPatternLockViewListener = new PatternLockViewListener() {
        @Override
        public void onStarted() {
            mPatternLockView.setViewMode(PatternLockView.PatternViewMode.CORRECT);
            if (mLayoutMsg.isShown()) {
                mLayoutMsg.setVisibility(View.INVISIBLE);
            }
        }

        @Override
        public void onProgress(List<PatternLockView.Dot> progressPattern) {
            mIsProgress = true;
        }

        @Override
        public void onComplete(List<PatternLockView.Dot> pattern) {
            mIsProgress = false;

            if (pattern.size() < Const.SSENSTONE_MIN_PATTERN) {
                mTextMsg.setText(R.string.msg_min_length_reg_pattern);
                mTextMsg.announceForAccessibility(getString(R.string.msg_min_length_reg_pattern));
                mLayoutMsg.setVisibility(View.VISIBLE);
                mPatternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!mIsProgress) {
                            mPatternLockView.clearPattern();
                        }
                    }
                }, 500);
                AniUtils.shakeView(mLayoutMsg);
                return;
            }

            if (mRegStep == REG_STEP.STEP_01) {
                mFirstPattern = PatternLockUtils.patternToString(mPatternLockView, pattern);
                mRegStep = REG_STEP.STEP_02;

                Handler delayHandler = new Handler();
                delayHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mTextDesc.setText(R.string.msg_retry_reg_pattern);
                        mPatternLockView.clearPattern();
                    }
                }, 500);

                return;
            } else if (mRegStep == REG_STEP.STEP_02) {
                String newPattern = PatternLockUtils.patternToString(mPatternLockView, pattern);
                if (isEqualsPattern(mFirstPattern, newPattern)) {

                    mPatternLockView.clearPattern();
                    mCommonUserInfo.setPatternData(newPattern);

                    if ((mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)) {
                        int ret = StonePassUtils.isUsableFingerprint(PatternRegActivity.this);

                        // 신규 가입인 경우에만 지문등록 진행
                        if (ret == 0/* && mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN && mCommonUserInfo.isNewUser()*/) {
                            Intent intent = new Intent(PatternRegActivity.this, FingerprintActivity.class);
                            mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(PatternRegActivity.this, PincodeRegActivity.class);
                            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                            startActivity(intent);
                        }
                    } else if ((mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET)) {
                        Intent intent = new Intent(PatternRegActivity.this, PincodeAuthActivity.class);
                        mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        mCommonUserInfo.setSignData("");
                        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                        startActivity(intent);
                    }
                    finish();
                    return;
                } else {
                    mTextMsg.setText(R.string.msg_no_match_reg_pattern);
                    mTextMsg.announceForAccessibility(getString(R.string.msg_no_match_reg_pattern));
                    mLayoutMsg.setVisibility(View.VISIBLE);
                    mPatternLockView.setViewMode(PatternLockView.PatternViewMode.WRONG);
                    mPatternLockView.setEnabled(false);

                    Handler delayHandler = new Handler();
                    delayHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            goRegStep01();
                        }
                    }, 500);
                    AniUtils.shakeView(mLayoutMsg);
                    return;
                }
            }
        }

        @Override
        public void onCleared() {
        }

        @Override
        public void onCheckPoint(PatternLockView.Dot dot) {
            Logs.d("Pattern CheckPoint : "+dot.getId());
        }
    };

    @Override
    public void stonePassResult(String op, int errorCode, String errorMsg) {

        //화면이 종료되면 리턴시킨다.
        if(isFinishing()) return;

        dismissProgressDialog();

        Logs.e("Pattern - stonePassResult : errorCode[" + errorCode + "], errorMsg[" + errorMsg + "]");
        String msg ="";

        switch (errorCode) {
            case 1200:
                Prefer.setPatternErrorCount(this, 0);
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    msg = "등록했습니다.";
                    Prefer.setPatternRegStatus(this, true);
                }
                break;

            // 사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.";
            case 1404:
            // 다른 사용자 패턴으로 이미 등록되어 있습니다.";
            case 1491:
            // 해제했습니다. (패턴일때만 리턴되는 값인듯)
            case 0:
            // 취소하셨습니다
            case 3:
            // 서버로 부터 리턴 값이 널(Null)입니다
            case 9998:
            // 작업중 json에러가 발생했습니다
            case 9999:
                msg = "등록을 실패했습니다.";
                if ((mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) && op.equalsIgnoreCase(Const.SSENSTONE_REGISTER))
                    Prefer.setPatternRegStatus(this, false);
                break;

            default:
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    msg = "등록을 실패했습니다.";
                } else {
                    msg = "해제를 실패했습니다.";
                }
                break;
        }

        /*
        if (mCommonUserInfo.getEntryPoint() != EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() != EntryPoint.DEVICE_CHANGE)
            Logs.showToast(this, msg);
        */
        if ((mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
                && op.equalsIgnoreCase(Const.SSENSTONE_REGISTER) && errorCode == 1200) {
            int ret = StonePassUtils.isUsableFingerprint(PatternRegActivity.this);
            // 지문등록 가능한 상태인 경우 진행
            if (ret == 0 /*&& mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN && mCommonUserInfo.isNewUser()*/) {
                Intent intent = new Intent(this, FingerprintActivity.class);
                mCommonUserInfo.setOperation(Const.SSENSTONE_REGISTER);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
            } else {
                Intent intent = new Intent(this, PincodeRegActivity.class);
                intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                startActivity(intent);
            }
            finish();
            return;
        } else if ((mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET)
                && op.equalsIgnoreCase(Const.SSENSTONE_REGISTER) && errorCode == 1200) {
            Intent intent = new Intent(this, PincodeAuthActivity.class);
            mCommonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            mCommonUserInfo.setSignData("");
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
            finish();
            return;
        }
        else {
           if (errorCode != 1200) {
               mPatternLockView.clearPattern();
               //  등록 실패해도 화면 종료하지 않음
               /*
               if (mCommonUserInfo.getEntryPoint() != EntryPoint.SERVICE_JOIN && mCommonUserInfo.getEntryPoint() != EntryPoint.DEVICE_CHANGE) {
                   Intent intent = new Intent();
                   mCommonUserInfo.setResultMsg(msg);
                   intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                   setResult(RESULT_CANCELED, intent);
                   finish();
                   return;
               }
               */
           }
        }

        /*
        if (mCommonUserInfo.getEntryPoint() != EntryPoint.SERVICE_JOIN && mCommonUserInfo.getEntryPoint() != EntryPoint.DEVICE_CHANGE) {
            msg = "code [" + errorCode + "]\n" + msg;
            Logs.showToast(this, msg);
        }
        */
        if (errorCode != 1200) {
            msg = "code [" + errorCode + "]\n" + msg;
            showErrorMessage(msg);
        }

        mPatternLockView.clearPattern();

        // 등록 실패해도 화면 종료하지 않음
        goRegStep01();
        /*
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE) {
            goRegStep01();
        }
        else {
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
        }
        */
    }

    /**
     * 등록 시 입력된 두패턴 일치 여부 판단
     * @param orgPattern 첫번재 패턴
     * @param newPattern 두번재 패턴
     * @return
     */
    private boolean isEqualsPattern(String orgPattern, String newPattern) {
        if (TextUtils.isEmpty(orgPattern) || TextUtils.isEmpty(newPattern))
            return false;

        if (orgPattern.length() != newPattern.length())
            return false;

        if (orgPattern.equalsIgnoreCase(newPattern))
            return true;

        return false;
    }


    /**
     * 첫번째 패턴 입력 표시
     */
    private void goRegStep01() {
        mFirstPattern = null;
        mRegStep = REG_STEP.STEP_01;
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE)
            mTextDesc.setText(R.string.msg_reg_pattern);
        else
            mTextDesc.setText(R.string.msg_reg_new_pattern);
        mLayoutMsg.setVisibility(View.INVISIBLE);
        mPatternLockView.setEnabled(true);

        if (!mIsProgress)
            mPatternLockView.clearPattern();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.textview_close) {
            String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) ?
                    getResources().getString(R.string.msg_cancel_pattern_change) : getResources().getString(R.string.msg_cancel_pattern_rereg);
            showCancelMessage(msg);
        }
    }
}
