package com.sbi.saidabank.activity.main2;

import android.content.Intent;
import android.text.TextUtils;
import android.webkit.SslErrorHandler;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;
import com.sbi.saidabank.web.OnWebActionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class BaseFragment extends Fragment implements OnWebActionListener {

    interface OnRequestAfterActionListener {
        void onRequestAfterAction();
    }

    @Override
    public void setCurrentPage(String url) {
        // TODO Auto-generated method stub
    }

    @Override
    public void setFinishedPageLoading(boolean flag) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onWebViewSSLError(String url, String errorCd, SslErrorHandler handler) {
        // TODO Auto-generated method stub
    }

    @Override
    public void showProgress() {
        // TODO Auto-generated method stub
    }

    @Override
    public void dismissProgress() {
        // TODO Auto-generated method stub
    }

    @Override
    public void openCamera() {
        // TODO Auto-generated method stub
    }

    @Override
    public void loadUrl(String url, String params) {
        // TODO Auto-generated method stub
    }

    @Override
    public void refreshWebView() {
        // TODO Auto-generated method stub
    }

    @Override
    public void setNeedResumeRefresh() {

    }

    @Override
    public void closeWebView() {
        // TODO Auto-generated method stub
    }

    @Override
    public void getProfileImage(JSONObject json, String workType) throws JSONException {
        // TODO Auto-generated method stub
    }

    public void onBackPressed() {
        // TODO Auto-generated method stub
    }

    public void fragmentForResult(int reqCode, int resCode, Intent intent) {
        // TODO Auto-generated method stub
    }

    public void fragmentForPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        // TODO Auto-generated method stub
    }

    private void showActivityProgressDialog() {
        if (getActivity() != null && !getActivity().isFinishing())
            ((BaseActivity) getActivity()).showProgressDialog();
    }

    private void dismissActivityProgressDialog() {
        if (getActivity() != null && !getActivity().isFinishing())
            ((BaseActivity) getActivity()).dismissProgressDialog();
    }

    /**
     * 메인정보조회
     *
     * @param listener isNeedCoupleConnectState 이 셋되면 onResum에서 호출한다.
     */
    public void requestMainSimpleInfo(final OnRequestAfterActionListener listener) {
        MLog.d();

        if (getActivity() == null || getActivity().isFinishing()) return;
        if (!LoginUserInfo.getInstance().isLogin()) return;

        showActivityProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A02.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if (getActivity() == null || getActivity().isFinishing()) return;

                dismissActivityProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            ((BaseActivity) getActivity()).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        }
                        return;
                    }

                    //메인 레코드가 아닌 기본값 조회 - 계좌분리 리스트도 여기서 한다.
                    Main2DataInfo.getInstance().setMain2DataInfoFromMyAccount(object);

                    //20200920 - 개발자 경고!
                    //아래 계좌정보와 이어하기 정보는 커플걔좌 탭의 각 웹화면에서
                    //화면 이동시에 참고 값으로 사용한다.
                    //그러니.. 아래 코드 주석처리하거나 지우지 말도록!
                    //===============================================================
                    // 계좌정보조회
                    ArrayList<Main2AccountInfo> listAccountInfo = new ArrayList<>();
                    JSONArray account_array = object.optJSONArray("REC");
                    if (account_array != null) {
                        for (int index = 0; index < account_array.length(); index++) {
                            JSONObject jsonObject = account_array.getJSONObject(index);
                            if (jsonObject == null)
                                continue;
                            Main2AccountInfo accountInfo = new Main2AccountInfo(jsonObject);
                            String DSCT_CD = accountInfo.getDSCT_CD();
                            if (TextUtils.isEmpty(DSCT_CD))
                                continue;
                            listAccountInfo.add(accountInfo);
                        }
                    }
                    Main2DataInfo.getInstance().setMain2Main2AccountInfo(listAccountInfo);
                    //===============================================================
                    // 이어하기 정보조회
                    ArrayList<Main2RelayInfo> listRelayInfo = new ArrayList<>();
                    JSONArray relay_array = object.optJSONArray("REC_WAIT");
                    if (DataUtil.isNotNull(relay_array)) {
                        for (int index = 0; index < relay_array.length(); index++) {
                            JSONObject jsonObject = relay_array.getJSONObject(index);
                            if (jsonObject == null)
                                continue;
                            Main2RelayInfo accountInfo = new Main2RelayInfo(jsonObject);
                            String DSCT_CD = accountInfo.getDSCT_CD();
                            if (TextUtils.isEmpty(DSCT_CD))
                                continue;
                            listRelayInfo.add(accountInfo);
                        }
                    }
                    Main2DataInfo.getInstance().setMain2RelayArray(listRelayInfo);
                    //===============================================================

                    // 데이타가 만들어 지면 다음작업을 진행하도록 리스너를 호출한다.
                    listener.onRequestAfterAction();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }
}
