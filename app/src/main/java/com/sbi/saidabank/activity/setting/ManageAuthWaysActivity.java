package com.sbi.saidabank.activity.setting;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.certification.CertImportActivity;
import com.sbi.saidabank.activity.certification.CertListActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.FidoAuthGuideDialog;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 * <p>
 * Class: ReloginActivity
 * Created by 950485 on 2018. 01. 08..
 * <p>
 * Description:인증수단관리 화면
 */

public class ManageAuthWaysActivity extends BaseActivity implements StonePassManager.FingerPrintDlgListener, StonePassManager.StonePassListener {
    private TextView                mTextCountPinError;
    private ToggleButton            mBtnUseFingerPrint;
    private FidoDialog              mFingerprintDlg;
    private FidoAuthGuideDialog     mFingerprintGuideDlg;
    private boolean                 mUpdateToggleButton;
    private int                     mFailCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_manage_auth_ways);

        initUX();
        //requestCountPinError();
    }

    @Override
    protected void onResume() {
        super.onResume();
        requestCountPinError();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logs.e("resultcode : " + resultCode);
        if (resultCode == RESULT_OK) {
            mUpdateToggleButton = false;
            Prefer.setFidoRegStatus(ManageAuthWaysActivity.this, true);
            mBtnUseFingerPrint.setChecked(true);
            setIsUseFingerprint(true);
        } else {
            Prefer.setFidoRegStatus(ManageAuthWaysActivity.this, false);
            mBtnUseFingerPrint.setChecked(false);
            setIsUseFingerprint(false);
        }
    }

    @Override
    public void stonePassResult(String op, int errorCode, String errorMsg) {

        //화면이 종료되면 리턴시킨다.
        if(isFinishing()) return;

        dismissProgressDialog();
        switch (errorCode) {
            case 1:
            case 2:
            case 4:
            case 8:
            case 9:
            case 11:
            case 1001:
                mFingerprintDlg.showFidoCautionMsg(true);
                Prefer.setFidoRegStatus(this, false);
                return;
            default:
                break;
        }

        if (mFingerprintDlg != null)
            mFingerprintDlg.dismiss();

        String msg = "";
        switch (errorCode) {
            case 1200: {
                msg = "등록했습니다.";
                break;
            }

            // 사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.
            case 1404: {
                msg = "사용자를 찾을수 없습니다. 먼저 등록하시기 바랍니다.";
                break;
            }

            case 1491: {
                //msg = "다른 사용자 지문으로 이미 등록되어 있습니다.";
                msg = "인증서버 오류입니다.";
                break;
            }

            case 0: {
                msg = "해제했습니다.";
                break;
            }

            // 사용자 취소
            case 3: {
                return;
            }

            case 10: {
                msg = errorMsg;
                break;
            }

            case 9998: {
                msg = "서버응답이 없습니다.";
                break;
            }

            case 9999: {
                //msg = "작업중 json에러가 발생했습니다.";
                msg = "등록을 실패했습니다.";
                break;
            }

            default: {
                msg = "등록을 실패했습니다.";
                break;
            }
        }

        if (errorCode == 3)
            return;

        if (errorCode == 1200) {
            //Prefer.setFidoRegStatus(this, true);
            Prefer.setFlagFingerPrintChange(this, false);
            Intent intent = new Intent(ManageAuthWaysActivity.this, PincodeAuthActivity.class);
            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_BIO, EntryPoint.AUTH_TOOL_MANAGE_BIO);
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, 1000);
        } else {
            Prefer.setFidoRegStatus(this, false);
            showErrorMessage(msg);
        }
    }

    @Override
    public void showFingerPrintDialog() {
        dismissProgressDialog();
        if (mFingerprintDlg == null) {
            mFingerprintDlg = DialogUtil.fido(this, true,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                            mFingerprintDlg.dismiss();
                            mBtnUseFingerPrint.setChecked(false);
                        }
                    },
                    null
            );
            mFingerprintDlg.mBackKeyListener = new Dialog.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    StonePassManager.getInstance(getApplication()).cancelFingerPrint();
                    mBtnUseFingerPrint.setChecked(false);
                }
            };
            mFingerprintDlg.setCanceledOnTouchOutside(false);
        }

        if (mFingerprintDlg != null) {
            mFingerprintDlg.showFidoCautionMsg(false);
            mFingerprintDlg.show();
        }
    }

    private void initUX() {
        mTextCountPinError = (TextView) findViewById(R.id.textview_count_pin_error);

        ImageView btnClose = (ImageView) findViewById(R.id.btn_cancel_mamage_auth_ways);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        RelativeLayout btnChangePattern = (RelativeLayout) findViewById(R.id.layout_change_pattern);
        btnChangePattern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManageAuthWaysActivity.this, PatternAuthActivity.class);
                CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PATTERN, EntryPoint.AUTH_TOOL_MANAGE_PATTERN);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
            }
        });

        ToggleButton btnUsePatternStealth = (ToggleButton) findViewById(R.id.btn_use_pattern_in_stealth);
        btnUsePatternStealth.setChecked(Prefer.getUsePatternStealth(this));
        btnUsePatternStealth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Prefer.setUsePatternStealth(ManageAuthWaysActivity.this, isChecked);
                setIsUseFingerprint(isChecked);
            }
        });

        mBtnUseFingerPrint = (ToggleButton) findViewById(R.id.btn_use_fingerprint);
        int result = StonePassUtils.isUsableFingerprint(this);
        if (result == 0) {
            mBtnUseFingerPrint.setChecked(Prefer.getFidoRegStatus(this));
            mUpdateToggleButton = true;
            mBtnUseFingerPrint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {
                    Logs.e("checkchagned : " + isChecked);
                    if (!isChecked)
                        Prefer.setFidoRegStatus(ManageAuthWaysActivity.this, isChecked);
                    setIsUseFingerprint(isChecked);

                    if (isChecked && mUpdateToggleButton) {
                        if (mFingerprintGuideDlg == null) {
                            mFingerprintGuideDlg = new FidoAuthGuideDialog(ManageAuthWaysActivity.this);
                            mFingerprintGuideDlg.mPListener = new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mFingerprintGuideDlg.dismiss();
                                    showProgressDialog();
                                    StonePassManager.getInstance(getApplication()).ssenstoneFIDO(ManageAuthWaysActivity.this, Const.SSENSTONE_REGISTER, "", "FINGERPRINT", "FINGERPRINT");
                                }
                            };
                            mFingerprintGuideDlg.mNListener = new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    mBtnUseFingerPrint.setChecked(false);
                                    mFingerprintGuideDlg.dismiss();
                                }
                            };
                            mFingerprintGuideDlg.mDismissListener = new Dialog.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    mBtnUseFingerPrint.setChecked(false);
                                }
                            };
                        }
                        mFingerprintGuideDlg.show();
                        mFingerprintGuideDlg.setCanceledOnTouchOutside(false);
                    }
                    mUpdateToggleButton = true;
                }
            });

            boolean isUseFingerprint = Prefer.getFidoRegStatus(this);
            setIsUseFingerprint(isUseFingerprint);
        } else if (result == -3) {
            mBtnUseFingerPrint.setChecked(false);

            mBtnUseFingerPrint.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton toggleButton, boolean isChecked) {
                    if (isChecked) {
                        View.OnClickListener okClick = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        };

                        final AlertDialog alertDialog = new AlertDialog(ManageAuthWaysActivity.this);
                        alertDialog.mPListener = okClick;
                        alertDialog.msg = getString(R.string.msg_no_fingerprint);
                        alertDialog.mPBtText = getString(R.string.common_confirm);
                        alertDialog.show();
                        mBtnUseFingerPrint.setChecked(false);
                    }
                }
            });

        } else {
            findViewById(R.id.ll_bio_title).setVisibility(View.GONE);
            findViewById(R.id.ll_bio_auth).setVisibility(View.GONE);
        }

        RelativeLayout btnChangePin = (RelativeLayout) findViewById(R.id.layout_change_pin);
        btnChangePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManageAuthWaysActivity.this, PincodeAuthActivity.class);
                CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PINCODE, EntryPoint.AUTH_TOOL_MANAGE_PINCODE);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivity(intent);
            }
        });

        RelativeLayout btnInitPin = (RelativeLayout) findViewById(R.id.layout_init_pin);
        btnInitPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFailCount <= 0) {
                    DialogUtil.alert(ManageAuthWaysActivity.this,
                            "인증비밀번호 오류횟수가 없습니다.",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            });
                } else {
                    Intent intent = new Intent(ManageAuthWaysActivity.this, PincodeAuthActivity.class);
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET, EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET);
                    commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                    commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivity(intent);
                }
            }
        });

        RelativeLayout manageCert = (RelativeLayout) findViewById(R.id.layout_manage_cert);
        manageCert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManageAuthWaysActivity.this, CertListActivity.class);
                intent.putExtra(Const.INTENT_CERT_PURPOSE, Const.CertJobType.MANAGER);
                startActivity(intent);
            }
        });

        RelativeLayout receiveCert = (RelativeLayout) findViewById(R.id.layout_receive_cert);
        receiveCert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManageAuthWaysActivity.this, CertImportActivity.class);
                startActivity(intent);
            }
        });

        RelativeLayout sendCert = (RelativeLayout) findViewById(R.id.layout_send_cert);
        sendCert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ManageAuthWaysActivity.this, CertListActivity.class);
                intent.putExtra(Const.INTENT_CERT_PURPOSE, Const.CertJobType.EXPORT);
                startActivity(intent);
            }
        });
    }

    private void setIsUseFingerprint(boolean isUse) {
        float padding = Utils.dpToPixel(ManageAuthWaysActivity.this, 9);

        if (isUse) {
            mBtnUseFingerPrint.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
            mBtnUseFingerPrint.setPadding((int) padding, 0, 0, 0);
        } else {
            mBtnUseFingerPrint.setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
            mBtnUseFingerPrint.setPadding(0, 0, (int) padding, 0);
        }
    }

    private void requestCountPinError() {
        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
        if (TextUtils.isEmpty(mbrNo)) {
            return;
        }

        Map param = new HashMap();
        param.put("MBR_NO", mbrNo);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010800A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("ret : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        return;
                    }

                    String count = object.optString("PN_INPT_EROR_TCNT");
                    if (!TextUtils.isEmpty(count)) {
                        mFailCount = Integer.parseInt(count);
                        if (mFailCount >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
                            mFailCount = Const.SSENSTONE_AUTH_COUNT_FAIL;
                            count = "5";
                        }
                        mTextCountPinError.setText(count);
                        Prefer.setPinErrorCount(ManageAuthWaysActivity.this, mFailCount);
                    } else {
                        mFailCount = 0;
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}

