package com.sbi.saidabank.activity.test;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.crop.CropImageActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.util.PathToUriUtil;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CropTestActivity extends BaseActivity {
    private static final int REQUEST_PICK_FROM_CAMERA = 300;
    private static final int REQUEST_PICK_FROM_ALBUM = 301;
    private static final int REQUEST_CROP_TO_IMAGE = 302;

    private ImageView mImageView;

    private Uri       mCropURI;
    private Uri       mTargetPhotoURI;

    private String[] perList = new String[] {
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_crop);

        mImageView = ((ImageView) findViewById(R.id.imageview_crop));
        Button btnCrop = (Button) findViewById(R.id.btn_crop);
        btnCrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showCropMenu();
            }
        });

        Button btnCropUri = (Button) findViewById(R.id.btn_crop_uri);
        btnCropUri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCropURI != null)
                    Toast.makeText(CropTestActivity.this, "save crop image " + mCropURI.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_CAMERA :
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showCamera();
                } else {
                    View.OnClickListener okClick = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                        }
                    };

                    int msgId = R.string.msg_permission_request_permission;
                    DialogUtil.alert(this, msgId, okClick);
                }
            }
            break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_PICK_FROM_ALBUM :
                if (resultCode == RESULT_OK && data != null) {
                    Uri photoUri = data.getData();
                    if (photoUri == null)
                        return;

                    Intent intent = new Intent(CropTestActivity.this, CropImageActivity.class);
                    intent.putExtra(Const.CROP_IMAGE_URI, photoUri.toString());
                    startActivityForResult(intent, REQUEST_CROP_TO_IMAGE);
                }
            break;

            case REQUEST_PICK_FROM_CAMERA :
                if (resultCode == RESULT_OK) {
                    if (mTargetPhotoURI == null)
                        return;

                    //scanMedia(SolutionTestActivity.this, mMakeCameraUri);

                    Intent intent = new Intent(CropTestActivity.this, CropImageActivity.class);
                    intent.putExtra(Const.CROP_IMAGE_URI, mTargetPhotoURI.toString());
                    startActivityForResult(intent, REQUEST_CROP_TO_IMAGE);
                } else {
                    if (mTargetPhotoURI != null) {
                        String filePath  = PathToUriUtil.getRealPath(CropTestActivity.this, mTargetPhotoURI);
                        File file = new File(filePath);
                        if (file.exists())
                            file.delete();
                    }
                }
                mTargetPhotoURI = null;
            break;

            case REQUEST_CROP_TO_IMAGE :
                if (resultCode == RESULT_OK && data != null) {
                    Bundle bundle = data.getExtras();
                    String uri = bundle.getString(Const.CROP_IMAGE_URI);
                    if (!TextUtils.isEmpty(uri)) {
                        mCropURI = Uri.parse(uri);
                        if (mCropURI != null) {
                            mImageView.setImageDrawable(null);
                            mImageView.destroyDrawingCache();
                            mImageView.setImageURI(mCropURI);
                        }
                    }
                }
            break;

            default:
                break;
        }
    }

    private void showCropMenu() {
        String[] colors = {"카메라", "사진첩"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Pick a item");
        builder.setItems(colors, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == 0) {
                    if (PermissionUtils.checkPermission(CropTestActivity.this, perList, Const.REQUEST_PERMISSION_CAMERA)) {
                        showCamera();
                    }
                } else if (which == 1) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                    startActivityForResult(intent, REQUEST_PICK_FROM_ALBUM);
                }
            }
        });
        builder.show();
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "sbi_" + timeStamp;
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );
        return image;
    }

    private void showCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion < Build.VERSION_CODES.N) {
                    mTargetPhotoURI =  Uri.fromFile(photoFile);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                    startActivityForResult(intent, REQUEST_PICK_FROM_CAMERA);
                } else {
                    ContentValues contentValues = new ContentValues(1);
                    contentValues.put(MediaStore.Images.Media.DATA, photoFile.getAbsolutePath());
                    mTargetPhotoURI = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mTargetPhotoURI);
                    startActivityForResult(intent, REQUEST_PICK_FROM_CAMERA);
                }
            }
        }
    }
}