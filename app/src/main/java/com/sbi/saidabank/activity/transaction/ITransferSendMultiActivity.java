package com.sbi.saidabank.activity.transaction;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.adater.ITransferMultiRemitteAdapter;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferMultiActionListener;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.AlertInfoDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmMultiITransferDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectSenderAccountDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingPreventAskDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomRecyclerView;
import com.sbi.saidabank.customview.CustomScrollView;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.view.View.OVER_SCROLL_NEVER;

public class ITransferSendMultiActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged,View.OnClickListener, OnITransferMultiActionListener, DatePickerCallback {
    public static final int REQUEST_SSENSTONE_AUTH   = 20000;

    private Context mContext;
    private CustomScrollView mScrollView;
    private RecyclerView mRecyclerView;
    private ITransferMultiRemitteAdapter mListAdapter;

    private Button mBtnTranfer;
    private int    mCurrentFocusItem;

    //타이틀 출금계좌정보
    private RelativeLayout mLayoutTitleAccountInfo;
    private ImageView mIvBankIcon;
    private TextView mTvAccountName;
    private ImageView mTvCoupeIcon;
    private TextView mTvBalanceAmount;
    //++ DatePicker values
    private int mItemPosition;
    private boolean mIstoday;
    private int mCurYear;
    private int mCurMon;
    private int mCurDay;
    private int mInputYear;
    private int mInputMonth;
    private int mInputDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itransfer_send_multi);
        //출금계좌 다이얼로그 에서 사용하기 위해 셋팅한다.
        OpenBankDataMgr.getInstance().setIS_NEED_SEARCH_AMOUNT(true);
        mContext = this;
        initViews();
        dispWithDrawAccount();
        //한도조회를 한다.
        if(ITransferDataMgr.getInstance().getTransferOneTime() == 0d){
            TransferRequestUtils.requestAccountLimit(this, new HttpSenderTask.HttpRequestListener() {
                @Override
                public void endHttpRequest(String ret) {
                    if(ret.equalsIgnoreCase("TRUE")){
                        String amount = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getBalanceAmount());
                        mTvBalanceAmount.setText(amount + " 원");

                        //금액 입력 화면에 한도를 넣어주도록 하자.
                        mListAdapter = new ITransferMultiRemitteAdapter(ITransferSendMultiActivity.this,ITransferSendMultiActivity.this);
                        mRecyclerView.setAdapter(mListAdapter);
                        changeStateBtnTransfer();
                    }
                }
            });
        }else{
            mListAdapter = new ITransferMultiRemitteAdapter(this,this);
            mRecyclerView.setAdapter(mListAdapter);
            changeStateBtnTransfer();
        }
    }

    @Override
    public void onBackPressed() {
        showCancelMessage();
    }



    private void initViews(){
        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.layout_main);
        mRelativeLayout.addKeyboardStateChangedListener(this);

        findViewById(R.id.tv_close).setOnClickListener(this);

        mLayoutTitleAccountInfo = findViewById(R.id.layout_title_account_info);
        int radius = (int) Utils.dpToPixel(mContext,13);
        mLayoutTitleAccountInfo.setAlpha(0);

        mLayoutTitleAccountInfo.setBackground(GraphicUtils.getRoundCornerDrawable("#eaeaea",new int[]{radius,radius,radius,radius}));
        mLayoutTitleAccountInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float alpha = mLayoutTitleAccountInfo.getAlpha();
                if(alpha == 0) return;
                showSelectSendAccountDialog();
            }
        });


        mIvBankIcon = findViewById(R.id.iv_bank_icon);
        mTvAccountName = findViewById(R.id.tv_account_name);
        mTvCoupeIcon = findViewById(R.id.iv_couple_icon);
        mTvBalanceAmount = findViewById(R.id.tv_balance_amount);

        mScrollView = findViewById(R.id.scrollview);
        mScrollView.setOnScrollActionListener(new CustomScrollView.OnScrollActionListener() {
            @Override
            public void onScrollStart(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                Logs.e("onScrollStart - oldScrollY : " + oldScrollY);
                Logs.e("onScrollStart - scrollY : " + scrollY);

            }

            @Override
            public void onScrollStop(boolean isBottom) {
                Logs.e("onScrollStop - isBottom : " +isBottom);


            }

            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                //Logs.e("onScrollChange - onScrollChange - v.getHeight() : " + v.getHeight());
                Logs.e("onScrollChange - onScrollChange - oldScrollY : " + oldScrollY);
                Logs.e("onScrollChange - onScrollChange - scrollY : " + scrollY);

                if(scrollY >=0 && scrollY <= 200){

                    float alpha = (float)scrollY/216f;
                    Logs.e("onScrollChange - %%%%%%%%%%%%%%%%% 1.alpha : " + alpha);
                    mLayoutTitleAccountInfo.setAlpha(alpha);
                }else{
                    Logs.e("onScrollChange - %%%%%%%%%%%%%%%%% 2.alpha : " + mLayoutTitleAccountInfo.getAlpha());
                   if(mLayoutTitleAccountInfo.getAlpha() != 1){
                       mLayoutTitleAccountInfo.setAlpha(1);
                   }
                }
            }
        });


        mRecyclerView = findViewById(R.id.listview);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setOverScrollMode(OVER_SCROLL_NEVER);

        mBtnTranfer = findViewById(R.id.btn_transfer);
        mBtnTranfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(changeStateBtnTransfer()){
                    mBtnTranfer.setEnabled(false);
                    checkAmountMultiBeforeUpload();
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_close:
                showCancelMessage();
                break;
        }
    }

    /**
     * 이체 추가를 눌러 수취인 조회가 끝난 후 호출되는 함수
     * 리스트를 재구성한다.
     */
    @Override
    public void makeMultiTransInfoList() {
        if(mListAdapter != null)
            mListAdapter.makeMultiTransInfoList();

        notifyDataChange();
        changeStateBtnTransfer();

        //새로 입력된 카드로 이동한다.
        mRecyclerView.scrollToPosition(mListAdapter.getItemCount()-1);
    }

    @Override
    public void notifyDataChange() {
        if(mListAdapter != null)
            mListAdapter.notifyDataSetChanged();
    }

    //입력 금액의 비교로 이체버튼의 상태를 변경해준다.
    @Override
    public boolean changeStateBtnTransfer(){
        boolean isHaveError=false;
        for(int i=0;i<ITransferDataMgr.getInstance().getRemitteInfoArraySize();i++){
            ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(i);

            if(!TextUtils.isEmpty(remitteeInfo.getEROR_MSG_CNTN())){
                isHaveError = true;
                break;
            }

            if(TextUtils.isEmpty(remitteeInfo.getTRN_AMT()) || remitteeInfo.getTRN_AMT().equalsIgnoreCase("0")){
                isHaveError = true;
                break;
            }

            double trans_amount = Double.valueOf(remitteeInfo.getTRN_AMT());
            if(trans_amount > ITransferDataMgr.getInstance().getTransferOneTime()){
                isHaveError = true;
                break;
            }
        }

        if(!isHaveError){
            double totlaSumAmount = ITransferDataMgr.getInstance().getTotalTranSumAmount();
            if(totlaSumAmount > ITransferDataMgr.getInstance().getBalanceAmount()){
                isHaveError = true;
            }else  if(totlaSumAmount > ITransferDataMgr.getInstance().getTransferOneDay()){
                isHaveError = true;
            }
        }

        if(isHaveError){
            mBtnTranfer.setEnabled(false);
            return false;
        }else{
            mBtnTranfer.setEnabled(true);
            return true;
        }

    }

    @Override
    public void setCurrentFocusItem(int position) {
        mCurrentFocusItem = position;
    }

    @Override
    public void notifyItemChange(int position) {
        if(mListAdapter != null)
            mListAdapter.notifyItemChanged(position);
    }

    @Override
    public void showDatePicker(int position) {
        MLog.d();
        mItemPosition = position;

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                try {
                    Calendar calendar = Calendar.getInstance();

                    if (DataUtil.isNotNull(ret)) {
                        JSONObject object = new JSONObject(ret);
                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        if (DataUtil.isNull(objectHead)) {
                            return;
                        }
                        String SYS_DTTM = object.optString("SYS_DTTM");
                        if (!TextUtils.isEmpty(SYS_DTTM)) {
                            calendar.set(Integer.parseInt(SYS_DTTM.substring(0, 4)), Integer.parseInt(SYS_DTTM.substring(4, 6)) - 1, Integer.parseInt(SYS_DTTM.substring(6, 8)));
                        }
                    }


                    if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 && calendar.get(Calendar.MINUTE) > 29) {
                        calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                        mIstoday = false;
                    } else {
                        mIstoday = true;
                    }


                    mCurYear = calendar.get(Calendar.YEAR);
                    mCurMon = calendar.get(Calendar.MONTH);
                    mCurDay = calendar.get(Calendar.DAY_OF_MONTH);

                    Calendar minDate = Calendar.getInstance();
                    minDate.set(mCurYear, mCurMon, mCurDay);
                    long tmp = calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 90 * 1000.0);
                    DatePickerFragmentDialog.newInstance(
                            DateTimeBuilder.get()
                                    .withMinDate(minDate.getTimeInMillis()).withMaxDate(tmp)
                                    .withTheme(R.style.datepickerCustom)
                    ).show(getSupportFragmentManager(), "DatePickerFragmentDialog");

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    @Override
    public void onDateSet(long date) {
        MLog.d();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        mInputYear = calendar.get(Calendar.YEAR);
        mInputMonth = calendar.get(Calendar.MONTH) + 1;
        mInputDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (mIstoday && (mCurYear != mInputYear || mCurDay != mInputDay)) {
            mIstoday = false;
        }

        SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(
                ITransferSendMultiActivity.this,
                Const.PICKER_TYPE_TIME,
                mIstoday,
                false
        );
        slidingDateTimerPickerDialog.setOnConfirmListener(new SlidingDateTimerPickerDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress(int year, int month, int day, int time, boolean needtimeselect, boolean istoday, boolean isDelayService) {

                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && time != 0) {
                    ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(mItemPosition);
                    remitteeInfo.setTRNF_DVCD("3");
                    remitteeInfo.setTRNF_DMND_DT(String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay));
                    remitteeInfo.setTRNF_DMND_TM(String.format("%02d00", time));
                }

                notifyDataChange();

//                String inputDate = String.format("%04d.%02d.%02d", mInputYear, mInputMonth, mInputDay) +  " "  + String.format("%02d", time) + ":00";
//                mLayoutInputDetailAccountInfo.setSendDate(inputDate);

            }
        });
        slidingDateTimerPickerDialog.show();
    }

    /**
     * 취소 메세지 출력
     */
    private void showCancelMessage() {
        MLog.d();
        if (isFinishing()) return;
        AlertDialog alertDialog = new AlertDialog(ITransferSendMultiActivity.this);
        alertDialog.msg = getString(R.string.msg_cancel_transfer);
        alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MLog.d();
                finish();
            }
        };
        alertDialog.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
        alertDialog.show();
    }

    @Override
    public void onKeyboardShown() {
        Logs.e("onKeyboardShown");
        mBtnTranfer.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        Logs.e("onKeyboardHidden");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                notifyDataChange();
            }
        },100);

        mBtnTranfer.setVisibility(View.VISIBLE);
    }

    /**
     * 타이틀의 출금계좌를 표시한다.
     */
    private void dispWithDrawAccount(){

        String bankCd = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        String detailBankCd = ITransferDataMgr.getInstance().getWTCH_DTLS_BANK_CD();
        String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();

        Uri iconUri = ImgUtils.getIconUri(mContext,bankCd,detailBankCd);
        if(iconUri != null)
            mIvBankIcon.setImageURI(iconUri);
        else
            mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);

        ITransferDataMgr.getInstance().setWTCH_BANK_NM(Const.BANK_FULL_NAME);

        //사이다뱅크계좌
        String accountName = "";
        ArrayList<MyAccountInfo> accountInfoArrayList =  LoginUserInfo.getInstance().getMyAccountInfoArrayList();
        for(int i=0;i<accountInfoArrayList.size();i++){
            MyAccountInfo accountInfo = accountInfoArrayList.get(i);
            if(accountInfo.getACNO().equalsIgnoreCase(accountNo)){
                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }
                accountName = ACCO_ALS;
                ITransferDataMgr.getInstance().setWTCH_ACNO_NM(accountName);

                //커플통장여부
                if(accountInfo.getSHRN_ACCO_YN().equalsIgnoreCase("Y")){
                    mTvCoupeIcon.setVisibility(View.VISIBLE);
                }else{
                    mTvCoupeIcon.setVisibility(View.GONE);
                }

                break;
            }
        }

        String shortAccNo = accountNo.substring(accountNo.length()-4,accountNo.length());
        mTvAccountName.setText(accountName + "(" + shortAccNo +")");

    }

    @Override
    public void showSelectSendAccountDialog(){
        SlidingExpandSelectSenderAccountDialog dialog = new SlidingExpandSelectSenderAccountDialog((Activity) mContext, true,new SlidingExpandSelectSenderAccountDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(final String bankCd,final String accountNo,final Object objInfo) {
                //오픈뱅킹계좌이면..단건으로 이동하자.
                if(!bankCd.equalsIgnoreCase("028")){
                    AlertInfoDialog alertInfoDialog = new AlertInfoDialog((Activity) mContext);
                    alertInfoDialog.mNListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    };
                    alertInfoDialog.mPListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ITransferDataMgr.getInstance().getRemitteInfoArrayList().clear();
                            ITransferDataMgr.getInstance().setTRANSFER_TYPE(ITransferDataMgr.TR_TYPE_NONE);

                            Intent intent = new Intent(mContext, ITransferSendSingleActivity.class);
                            mContext.startActivity(intent);
                            ((Activity) mContext).finish();
                        }
                    };

                    alertInfoDialog.mTopText = "출금계좌를 변경하시겠습니까?";
                    alertInfoDialog.mSubText = "다른계좌에서는 다건이체가 불가하며, \n입력된 내용은 삭제됩니다.";
                    alertInfoDialog.show();
                    return;
                }else{
                    //거래정지 계좌인지 체크
                    int cnt = LoginUserInfo.getInstance().getMyAccountInfoArrayList().size();
                    for(int i=0;i<cnt;i++){
                        MyAccountInfo info = LoginUserInfo.getInstance().getMyAccountInfoArrayList().get(i);
                        if(info.getACNO().equalsIgnoreCase(accountNo) && info.getACCO_PRGS_STCD().equalsIgnoreCase("01")){
                            TransferUtils.showDialogSuspantionAccount((BaseActivity)mContext,accountNo);
                            return;
                        }
                    }
                }

                //보내는 사람 정보 변경표시
                ITransferDataMgr.getInstance().setWTCH_BANK_CD(bankCd);
                ITransferDataMgr.getInstance().setWTCH_ACNO(accountNo);
                ITransferDataMgr.getInstance().setWTCH_BANK_NM(Const.BANK_FULL_NAME); //풀네임.
                ITransferDataMgr.getInstance().setWTCH_BANK_ALS(Const.BANK_ALS_NAME);//단축네임.

                //각 수취인 입력정보 초기화
                ITransferDataMgr.getInstance().initRemiteeInfo();

                //다건계좌중에 변경한 계좌가 같은 계좌가 있는지 체크한다.
                for(int i=0;i<ITransferDataMgr.getInstance().getRemitteInfoArraySize();i++){
                    ITransferRemitteeInfo info = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(i);
                    //if(info.getCNTP_FIN_INST_CD().equalsIgnoreCase(bankCd) && info.getCNTP_BANK_ACNO().equalsIgnoreCase(accountNo)){
                    if(info.getCNTP_BANK_ACNO().equalsIgnoreCase(accountNo)){
                        info.setEROR_MSG_CNTN("출금계좌와 입금계좌가 동일합니다.");
                    }else{
                        String EROR_MSG_CNTN = info.getEROR_MSG_CNTN();
                        if(TextUtils.isEmpty(EROR_MSG_CNTN)) EROR_MSG_CNTN = "";
                        if(EROR_MSG_CNTN.contains("출금계좌와 입금계좌가 동일")){
                            info.setEROR_MSG_CNTN("");
                        }
                    }
                }

                //헤더의 계좌 정보를 변경한다.
                mListAdapter.notifyItemChanged(0);
                dispWithDrawAccount();

                //한도조회를 진행한다.
                //한도조회는 조회중 에러가 발생하면 이체화면을 종료한다.
                if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() != ITransferDataMgr.TR_TYPE_NONE){
                    TransferRequestUtils.requestAccountLimit((Activity) mContext, new HttpSenderTask.HttpRequestListener() {
                        @Override
                        public void endHttpRequest(String ret) {
                            if(ret.equalsIgnoreCase("TRUE")){
                                //타이틀의 출금가능 금액 표시
                                String amount = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getBalanceAmount());
                                mTvBalanceAmount.setText(amount + " 원");

                                notifyDataChange();
                                changeStateBtnTransfer();
                            }
                        }
                    });
                }

            }
        });
        dialog.show();
    }



    //========================================
    //  여기서 부터 이체를 시작한다.
    //++++++++++++++++++++++++++++++++++++++++
    /**
     * 1.이체전 금액 체크
     */
    private void checkAmountMultiBeforeUpload(){
        //이체확인전 웹에서 금액및 여러 제한사항을 체크한다.
        TransferRequestUtils.requestCheckAmountToday(ITransferSendMultiActivity.this, new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {
                if(result){
                    showConfirmMultiTransfer();
                }else{
                    notifyDataChange();
                    changeStateBtnTransfer();
                }
            }
        });
    }

    /**
     * 2.이체전(다건이체) 확인 다이얼로그를 띄운다.
     */
    private void showConfirmMultiTransfer() {
        MLog.d();

        String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String shortAccNo = accountNo.substring(accountNo.length()-4,accountNo.length());
        String bankAccount = ITransferDataMgr.getInstance().getWTCH_ACNO_NM() + "[" + shortAccNo + "]";

        SlidingConfirmMultiITransferDialog multiTransferDialog = new SlidingConfirmMultiITransferDialog(
                ITransferSendMultiActivity.this,
                bankAccount,
                new SlidingConfirmMultiITransferDialog.FinishListener() {
                    @Override
                    public void OnCancelListener(boolean isLastRemove, Double dLimitOneDay, boolean isAllRemove) {
                        MLog.d();
                        mBtnTranfer.setEnabled(true);
                    }

                    @Override
                    public void OnOKListener(final boolean isLastRemove) {
                        requestUploadTransferInfo();
                    }
                });
        multiTransferDialog.setCancelable(false);
        multiTransferDialog.show();
    }

    /**
     * 3. 업무서버에 이체목록 등록
     *
     */
    private void requestUploadTransferInfo() {
        MLog.d();

        TransferRequestUtils.requestUploadTransferInfo(this, new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {
                if(result){
                    checkVoicePhishingState();
                }else{
                    mBtnTranfer.setEnabled(true);
                }
            }
        });
    }

    /**
     * 4. 보이스피싱여부를 체크한다.
     */
    private void checkVoicePhishingState(){
        TransferRequestUtils.requestVoicePhising(this, new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {

                if(!result){
                    mBtnTranfer.setEnabled(true);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    if (DataUtil.isNotNull(Utils.getJsonString(object, "APRV_YN"))
                            && Utils.getJsonString(object, "APRV_YN").equals(Const.REQUEST_WAS_NO)) {
                        String saveDate = Prefer.getPrefVoicePhishingAskPopupDate(ITransferSendMultiActivity.this);
                        String curDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
                        if (DataUtil.isNull(saveDate) || (Integer.parseInt(saveDate) < Integer.parseInt(curDate))) {
                            //showAdvanceVoicePhishing();
                            showVoicePhishing();
                        } else {
                            requestElectroSignData();
                        }
                    } else {
                        requestElectroSignData();
                    }
                } catch (Exception e) {
                    MLog.e(e);
                    mBtnTranfer.setEnabled(true);
                }
            }
        });
    }

    /**
     * 20210408 - 알림팝업 띄우지 않도록 수정.
     * 4-1.보이스피싱 체크 일림 팝업을 띄어준다.     *
     */
//    private void showAdvanceVoicePhishing() {
//        if (isFinishing())
//            return;
//        SlidingVoicePhishingDialog dialog = new SlidingVoicePhishingDialog(
//                this,
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        // 이체정보 전자 서명값 요청
//                        requestElectroSignData();
//                    }
//                },
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        showVoicePhishing();
//                    }
//                }
//        );
//        dialog.setCancelable(false);
//        dialog.show();
//    }

    /**
     * 4-2.보이스피싱 문진 팝업을 띄어준다.
     */
    private void showVoicePhishing() {
        if (isFinishing())
            return;

        final SlidingVoicePhishingPreventAskDialog mVoicePhishingPreventAskDialog = new SlidingVoicePhishingPreventAskDialog(this);
        mVoicePhishingPreventAskDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mVoicePhishingPreventAskDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        mVoicePhishingPreventAskDialog.setCancelable(false);
        mVoicePhishingPreventAskDialog.setOnItemListener(new SlidingVoicePhishingPreventAskDialog.OnItemListener() {
            @Override
            public void onConfirm(boolean result) {
                mVoicePhishingPreventAskDialog.dismiss();
                // 문진 응답을 모두 "아니요"로 체크한 경우
                if (result) {
                    ITransferDataMgr.getInstance().clearTransferData();
                    finish();
                }else {
                    // 문진 응답 중 하나라고 "예"라고 체크한 경우 팝업을 띄어준다.
                    DialogUtil.alert(ITransferSendMultiActivity.this,getString(R.string.msg_voice_phishing_05), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ITransferDataMgr.getInstance().clearTransferData();
                            finish();
                        }
                    });
                }

            }
        });
        mVoicePhishingPreventAskDialog.show();
    }

    /**
     * 5. 이체 싸인 데이타 생
     * 확인 다이얼로그에서 예 클릭시 웹에 올려두었던 이체 정보를 가지고
     * 싸인 데이타를 만들어 내려준다.
     */
    private void requestElectroSignData(){
        TransferRequestUtils.transferSignData(this, new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {
                if(true){
                    showOTPActivity(ret);
                }else{
                    mBtnTranfer.setEnabled(true);
                }
            }
        });
    }

    /**
     * 6.mOTP, 타행 OTP, 핀코드 입력창 표시
     *
     * @param signData 전자서명된 이체정보
     */
    private void showOTPActivity(String signData) {
        MLog.d();
        TransferUtils.showOTPActivity(this,signData);
    }

    /**
     * 6.번은 ITransferSendSingleActivity에서 처리한다.
     * 7.이체 요청
     * @param elecSrno 전자서명된 이체정보
     */
    public void requestTransfer(String elecSrno) {
        Map param = new HashMap();
        param.put("ELEC_SGNR_SRNO", elecSrno);
        param.put("DMND_CCNT", ITransferDataMgr.getInstance().getRemitteInfoArraySize());

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

               dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    TransferUtils.showFailTransfer(ITransferSendMultiActivity.this,0, Const.EMPTY);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        TransferUtils.showFailTransfer(ITransferSendMultiActivity.this,0, Const.EMPTY);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        TransferUtils.showFailTransfer(ITransferSendMultiActivity.this,0, Const.EMPTY);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC_OUT");
                    if (DataUtil.isNull(array))
                        return;

                    ArrayList<TransferReceiptInfo> listTransferReceiptInfo = new ArrayList<>();

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;
                        listTransferReceiptInfo.add(new TransferReceiptInfo(jsonObject));
                    }

                    String ACCO_ALS = object.optString("ACCO_ALS");
                    String mWithdrawName = Const.EMPTY;

                    if (DataUtil.isNull(ACCO_ALS)) {
                        ACCO_ALS = object.optString("PROD_NM");
                    }
                    String ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
                    if (DataUtil.isNotNull(ACNO)) {
                        int lenACNO = ACNO.length();
                        if (lenACNO > 4) {
                            ACNO = ACNO.substring(lenACNO - 4, lenACNO);
                        }
                        ACNO = " [" + ACNO + "]";
                    }
                    if(!TextUtils.isEmpty(ACCO_ALS) && ACCO_ALS.length() > 14){
                        ACCO_ALS = ACCO_ALS.substring(0,14) + Const.ELLIPSIS;
                    }
                    mWithdrawName = ACCO_ALS + ACNO;


                    if (DataUtil.isNotNull(listTransferReceiptInfo) && listTransferReceiptInfo.size() == 1) {
                        String RESP_CD = listTransferReceiptInfo.get(0).getRESP_CD();
                        if ("XEEL0140".equalsIgnoreCase(RESP_CD) || "XEEL0141".equalsIgnoreCase(RESP_CD) ||
                                "XEEL0142".equalsIgnoreCase(RESP_CD) || "XEEL0143".equalsIgnoreCase(RESP_CD) ||
                                "XEKM0089".equalsIgnoreCase(RESP_CD) || "XEKM0091".equalsIgnoreCase(RESP_CD) ||
                                "XEKM0096".equalsIgnoreCase(RESP_CD)) {
                            TransferUtils.showFailTransfer(ITransferSendMultiActivity.this,0, Const.EMPTY);
                            return;
                        }
                    }

                    // 이체완료 페이지로 이동
                    Intent intent = new Intent(ITransferSendMultiActivity.this, ITransferCompleteActivity.class);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO, listTransferReceiptInfo);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_NAME, LoginUserInfo.getInstance().getCUST_NM());
                    intent.putExtra(Const.INTENT_TRANSFER_BALANCE, object.optString("AFTR_BLNC"));
                    intent.putExtra(Const.INTENT_PROD_NAME, object.optString("PROD_NM"));
                    intent.putExtra(Const.INTENT_BANK_NM, DataUtil.isNotNull(object.optString("BANK_NM")) ? object.optString("BANK_NM") : "SBI저축(사이다뱅크)");
                    intent.putExtra(Const.INTENT_WITHDRAWABLE_AMOUNT, object.optString("WTCH_TRN_POSB_AMT"));
                    intent.putExtra(Const.INTENT_WITHDRAW_NAME, mWithdrawName);
                    intent.putExtra(Const.INTENT_ACCO_ALS, ACCO_ALS);
                    intent.putExtra(Const.INTENT_GRP_SERIAL_NO, object.optString("GRP_SRNO"));
                    intent.putExtra(Const.INTENT_GRP_REVS_YN, object.optString("GRP_REVS_YN"));

                    startActivity(intent);
                    finish();

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.REQUEST_SSENSTONE_AUTH: {
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String result = commonUserInfo.getResultMsg();

                    if ("P000".equalsIgnoreCase(result)) {
                        requestTransfer(commonUserInfo.getSignData());
                    } else {
                        Logs.showToast(this, "인증에 실패했습니다.");
                    }
                } else {
                    mBtnTranfer.setEnabled(true);
                }
                break;
            }
            case Const.REQUEST_OTP_AUTH:
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String otpAuthTools = data.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
                    if (DataUtil.isNotNull(commonUserInfo)
                            && DataUtil.isNotNull(otpAuthTools)
                            && ("3".equalsIgnoreCase(otpAuthTools) || "2".equalsIgnoreCase(otpAuthTools))
                            && data.hasExtra(Const.INTENT_OTP_AUTH_SIGN)) {
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        commonUserInfo.setSignData(data.getStringExtra(Const.INTENT_OTP_AUTH_SIGN));
                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                        Intent intent = new Intent(ITransferSendMultiActivity.this, PincodeAuthActivity.class);
                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                    }
                } else {
                    mBtnTranfer.setEnabled(true);
                }
                break;
            default:
                break;
        }
    }


}
