package com.sbi.saidabank.activity.login;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomEditText;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.web.JavaScriptApi;

import java.lang.ref.WeakReference;
import java.util.regex.Pattern;

/**
 * Saidabanking_android
 * Class: CertifyPhonePersonalInfoActivity
 * Created by 950546
 * Date: 2018-10-18
 * Time: 오전 10:40
 * Description: 개인정보 입력 화면 (이름, 생년월일, 성별코드)
 */

public class CertifyPhonePersonalInfoActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged, View.OnClickListener, TextView.OnEditorActionListener, View.OnFocusChangeListener, TextWatcher, CustomEditText.OnBackPressListener {
    private static final int SCROLL_EDITTEXT        = 0;
    private static final int SCROLL_CHECK_KEYBOARD  = 1;
    private static final int SCROLL_EDITTEXT_NAME   = 2;

    private Context mContext;
    private RelativeLayout mLayoutScreen;
    private ScrollView mSvScview;
    private LinearLayout mLayoutName;
    private TextView mTvNameError01;
    private TextView mTvNameError02;
    private View mVNamePadding;
    private TextView mTvBirthError;
    private LinearLayout mLayoutIdNumber;
    private LinearLayout mLayoutBirth;
    private CustomEditText mEtName;
    private CustomEditText mEtIdnumber01;
    private CustomEditText mEtIdnumber02;
    private TextView mTvNameCaption;
    private TextView mTvIdCaption;
    private Button mBtnPlaceholderDot;
    private Button mBtnConfirm;
    private View mVLine1;
    private int mCurFocusedViewId;
    private int mCurFocesedViewState;

    private int mScrollOffset;
    private int mScrollNameOffset;
    private boolean mIsShownKeyboard;
    private InputMethodManager imm = null;
    private boolean bIsfirstrun;
    private WeakHandler mScrollHandler;

    private CommonUserInfo mCommonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_certify_phone_personalinfo);

        getExtra();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logs.e("onResume");
        setEditTextBackground(mCurFocusedViewId, mCurFocesedViewState);
    }

    @Override
    protected void onStop() {
        super.onStop();

        //++ activity stop될 때 마지막 포커스 뷰와 상태 저장
        if (getCurrentFocus() != null) {
            mCurFocusedViewId = getCurrentFocus().getId();
        } else {
            mCurFocusedViewId = mEtName.getId();
        }

        mCurFocesedViewState = Const.STATE_EDITBOX_FOCUSED;
        switch (mCurFocusedViewId) {
            case R.id.et_name:
                mCurFocusedViewId = Const.INDEX_EDITBOX_NAME;
                if (Utils.isKorean(mEtName.getText().toString()) != Const.STATE_VALID_STR_NORMAL
                        && (mTvNameError01.getVisibility() != View.GONE || mTvNameError02.getVisibility() != View.GONE))
                    mCurFocesedViewState = Const.STATE_EDITBOX_ERROR;
                break;
            case R.id.et_idnumber1:
            case R.id.et_idnumber2:
                mCurFocusedViewId = Const.INDEX_EDITBOX_BIRTH;
                boolean bisvalidBirth = isValidBirth(mEtIdnumber01.getText().toString());
                boolean bisvalidIdnumber = isValidIdNumber(mEtIdnumber02.getText().toString());
                if ((!bisvalidBirth || !bisvalidIdnumber)
                        && mTvBirthError.getVisibility() != View.GONE)
                    mCurFocesedViewState = Const.STATE_EDITBOX_ERROR;
                break;
            default:
                break;
        }
        //--
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        setResult(resultCode, data);
        finish();
    }

    @Override
    public void onKeyboardShown() {
        mScrollHandler.sendEmptyMessageDelayed(SCROLL_CHECK_KEYBOARD, 300);
        mBtnConfirm.setVisibility(View.GONE);
    }

    @Override
    public void onKeyboardHidden() {
        mIsShownKeyboard = false;
        mBtnConfirm.setVisibility(View.VISIBLE);
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    /**
     * UI 초기화
     */
    private void initView() {
        mContext = this;
        bIsfirstrun = false;
        mCurFocesedViewState = Const.STATE_EDITBOX_NORMAL;
        mSvScview = (ScrollView) findViewById(R.id.sv_scview);
        mLayoutName = (LinearLayout) findViewById(R.id.ll_name);
        mLayoutScreen = (RelativeLayout) findViewById(R.id.ll_root);
        mTvNameError01 = (TextView) findViewById(R.id.tv_nameerrmsg01);
        mTvNameError02 = (TextView) findViewById(R.id.tv_nameerrmsg02);
        mVNamePadding = (View) findViewById(R.id.v_nameboxpadding);
        mTvBirthError = (TextView) findViewById(R.id.tv_birtherrmsg);
        mLayoutIdNumber = (LinearLayout) findViewById(R.id.ll_idnumber);
        mLayoutBirth = (LinearLayout) findViewById(R.id.ll_birth);
        mEtName = (CustomEditText) findViewById(R.id.et_name);
        mEtIdnumber01 = (CustomEditText) findViewById(R.id.et_idnumber1);
        mEtIdnumber02 = (CustomEditText) findViewById(R.id.et_idnumber2);
        mVLine1 = (View) findViewById(R.id.v_line_01);
        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mTvNameCaption = (TextView) findViewById(R.id.tv_namecaption);
        mTvIdCaption = (TextView) findViewById(R.id.tv_idcaption);
        mBtnPlaceholderDot = (Button) findViewById(R.id.btn_placeohlder_dot);

        mScrollOffset =  (int)Utils.dpToPixel(this, (float)40f);
        mScrollNameOffset = (int)Utils.dpToPixel(this, (float)20f);
        mScrollHandler = new WeakHandler(this);
        mCurFocusedViewId = Const.INDEX_EDITBOX_NAME;

        mLayoutName.setOnClickListener(this);
        findViewById(R.id.ll_idbirth).setOnClickListener(this);
        mLayoutScreen.setOnClickListener(this);
        mLayoutIdNumber.setOnClickListener(this);
        mEtName.setOnEditorActionListener(this);
        mEtName.setOnFocusChangeListener(this);
        mEtName.setFilters(new InputFilter[]{specialCharacterFilter, new InputFilter.LengthFilter(14)});
        mEtName.addTextChangedListener(this);
        mEtName.setOnBackPressListener(this);
        mEtName.setOnClickListener(this);
        mEtIdnumber01.setOnEditorActionListener(this);
        mEtIdnumber01.setOnFocusChangeListener(this);
        mEtIdnumber01.setOnClickListener(this);
        mEtIdnumber01.addTextChangedListener(this);
        mEtIdnumber01.setOnBackPressListener(this);
        mEtIdnumber02.setOnEditorActionListener(this);
        mEtIdnumber02.addTextChangedListener(this);
        mEtIdnumber02.setOnFocusChangeListener(this);
        mEtIdnumber02.setOnBackPressListener(this);
        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (checkDataValidation()) {
                    // 휴대폰 본인인증 화면 이동 시 생년월일 정보 전달
                    Intent authphoneintent = new Intent(CertifyPhonePersonalInfoActivity.this, CertifyPhoneActivity.class);
                    mCommonUserInfo.setBirth(mEtIdnumber01.getText().toString());
                    mCommonUserInfo.setSexCode(mEtIdnumber02.getText().toString());
                    mCommonUserInfo.setName(mEtName.getText().toString());
                    mCommonUserInfo.setFullIdnumber(false);
                    authphoneintent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);

                    if(mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
                        startActivityForResult(authphoneintent, JavaScriptApi.API_502);
                    }
                    else {
                        startActivity(authphoneintent);
                        finish();
                    }
                } else {
                    mBtnConfirm.setEnabled(false);
                }
            }
        });

        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.ll_wrapper);
        mRelativeLayout.addKeyboardStateChangedListener(this);

        TextView textClose = findViewById(R.id.tv_cancel);

        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) {
            textClose.setText(getResources().getString(R.string.common_cancel));
        }
        textClose.setOnClickListener(this);

        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        Logs.i("onclick");
        switch (v.getId()) {
            case R.id.ll_name: {
                boolean hasFocuesd = mEtName.hasFocus();
                mEtName.requestFocus();
                imm.showSoftInput(mEtName, 0);
                checkVaildUserName(mEtName.getText().toString(), true);
                if (hasFocuesd)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_NAME, 300);
                break;
            }
            case R.id.et_name: {
                checkVaildUserName(mEtName.getText().toString(), true);
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_NAME, 300);
                break;
            }
            case R.id.ll_idbirth: {
                boolean hasFocuesd = mEtIdnumber01.hasFocus();
                mEtIdnumber01.requestFocus();
                imm.showSoftInput(mEtIdnumber01, 0);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                if (hasFocuesd)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_idnumber1: {
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_idnumber2: {
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.tv_cancel: {
                if (mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET || mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET) {
                    String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) ?
                            getResources().getString(R.string.msg_cancel_pattern_rereg) : getResources().getString(R.string.msg_cancel_pin_rereg);
                    showCancelMessage(msg);
                    return;
                } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
                    Intent intent = new Intent();
                    intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
                    setResult(RESULT_CANCELED, intent);
                    finish();
                }
                break;
            }
            case R.id.ll_root: {
                Logs.i("screen touch");
                imm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEtIdnumber01.getWindowToken(), 0);
                imm.hideSoftInputFromWindow(mEtIdnumber02.getWindowToken(), 0);
                trimEditTextString(mEtName);
                View curview = getCurrentFocus();
                if (curview != null) {
                    switch (curview.getId()) {
                        case R.id.et_name: {
                            checkVaildUserName(mEtName.getText().toString(), true);
                            break;
                        }
                        case R.id.et_idnumber1:
                        case R.id.et_idnumber2: {
                            checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                            break;
                        }
                        default:
                            break;
                    }
                } else {
                    checkVaildUserName(mEtName.getText().toString(), false);
                    checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), false);
                }

                mBtnConfirm.setEnabled(checkDataValidation());
                break;
            }
            case R.id.ll_idnumber: {
                Logs.i("touch ll_idnumber");
                boolean hasFocuesd = mEtIdnumber02.hasFocus();
                mEtIdnumber02.requestFocus();
                imm.showSoftInput(mEtIdnumber02, 0);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
                if (hasFocuesd)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            default:
                break;
        }
    }

    /**
     * 최종 확인 버튼 활성화 체크
     */
    private boolean checkDataValidation() {
        String namestr = mEtName.getText().toString();
        String birthstr1 = mEtIdnumber01.getText().toString();
        String birthstr2 = mEtIdnumber02.getText().toString();

        // 입력 데이터가 없을 때
        if (namestr.length() == 0 || birthstr1.length() == 0 || birthstr2.length() == 0) {
            Logs.e("no data");
            return false;
        }

        // 데이터 유효성 체크
        if (Utils.isKorean(namestr) != Const.STATE_VALID_STR_NORMAL || isValidIdNumber(birthstr2) == false
                || isValidBirth(birthstr1) == false) {
            Logs.e("invalid data");
            return false;
        }

        return true;
    }

    /**
     * 키보드 하단 back key 버튼 입력 처리
     *
     * @param vid 선택된 view id
     * @return vid가 edittext면 true
     */
    private boolean keyboardBackKeyPressed(int vid) {
        Logs.i("keyboardBackKeyPressed");
        if (vid == mEtName.getId()) {
            trimEditTextString(mEtName);
            checkVaildUserName(mEtName.getText().toString(), true);
            return true;
        } else if (vid == mEtIdnumber02.getId()) {
            Logs.i("idnumber2 back");
            checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
            return true;
        } else if (vid == mEtIdnumber01.getId()) {
            checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), true);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        Logs.i("onBackPressed");
        // 패턴 분실재등록
        if (mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET || mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) {
            String msg = (mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET) ?
                    getResources().getString(R.string.msg_cancel_pattern_rereg) : getResources().getString(R.string.msg_cancel_pin_rereg);
            showCancelMessage(msg);
            return;
        } else if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
            Intent intent = new Intent();
            intent.putExtra(Const.BRIDGE_RESULT_KEY, Const.BRIDGE_RESULT_FALSE);
            setResult(RESULT_CANCELED, intent);
        }
        finish();
    }

    /**
     * 주민번호 뒷자리 첫번째 숫자 유효성 체크
     *
     * @param strnumber : 주민번호 뒷자리 첫번째 숫자
     * @return 1-4사이면 true
     */
    private boolean isValidIdNumber(String strnumber) {
        if (strnumber.length() > 0) {
            if (strnumber.matches("^[1-4]*$")) {  // 1-4
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * 생년월일 유효성 체크
     *
     * @param strbirth 생년월일
     * @return 6자리면 true
     */
    private boolean isValidBirth(String strbirth) {
        if (strbirth.length() > 0) {
            if (strbirth.length() == 6) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * 키보드 이모티콘 입력 금지
     */
    private InputFilter specialCharacterFilter = new InputFilter() {
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            for (int i = start; i < end; i++) {
                // 이모티콘 패턴
                Pattern unicodeOutliers = Pattern.compile("[\\uD83C-\\uDBFF\\uDC00-\\uDFFF]+");
                if (unicodeOutliers.matcher(source).matches()) {
                    return "";
                }
            }
            return null;
        }
    };

    /**
     * 이름 유효성 체크
     *
     * @param strname  이름
     * @param hasfocus 현재 포커스된 상태인지
     */
    private void checkVaildUserName(String strname, boolean hasfocus) {

        int derrcode = Utils.isKorean(strname);

        if (derrcode == Const.STATE_VALID_STR_NORMAL) {
            mTvNameError01.setVisibility(View.GONE);
            mTvNameError02.setVisibility(View.GONE);
            mVNamePadding.setVisibility(View.GONE);
            mEtName.setTextColor(getResources().getColor(R.color.black));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_FOCUSED);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_NORMAL);
        } else {
            mEtName.setTextColor(getResources().getColor(R.color.colorRed));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_ERROR);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_NAME, Const.STATE_EDITBOX_NORMAL);

            if (derrcode == Const.STATE_VALID_STR_LENGTH_ERR) {
                mTvNameError01.setVisibility(View.GONE);
                mTvNameError02.setVisibility(View.VISIBLE);
                mVNamePadding.setVisibility(View.VISIBLE);
            }
            if (derrcode == Const.STATE_VALID_STR_CHAR_ERR) {
                mTvNameError01.setVisibility(View.VISIBLE);
                mVNamePadding.setVisibility(View.VISIBLE);
                if (strname.length() < 2 || strname.length() >= 15) {
                    mTvNameError02.setVisibility(View.VISIBLE);
                } else {
                    mTvNameError02.setVisibility(View.GONE);
                }
            }
        }
    }

    /**
     * 생년월일, 주민번호 뒷자리 첫번째 숫자 유효성 체크
     *
     * @param strbirth  생년월일
     * @param strnumber 주민번호 뒷자리 첫번째 숫자
     * @param hasfocus  현재 포커스된 상태인지
     */
    private void checkVaildBrith(String strbirth, String strnumber, boolean hasfocus) {
        boolean bisvalidBirth = isValidBirth(strbirth);
        boolean bisvalidIdnumber = isValidIdNumber(strnumber);

        if (bisvalidBirth && bisvalidIdnumber) {
            mTvBirthError.setVisibility(View.GONE);
            mEtIdnumber01.setTextColor(getResources().getColor(R.color.black));
            mEtIdnumber02.setTextColor(getResources().getColor(R.color.black));
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_FOCUSED);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_NORMAL);
        } else {
            mTvBirthError.setVisibility(View.VISIBLE);
            if (hasfocus)
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_ERROR);
            else
                setEditTextBackground(Const.INDEX_EDITBOX_BIRTH, Const.STATE_EDITBOX_NORMAL);

            if (!bisvalidBirth) {
                mTvBirthError.setText(R.string.msg_invalid_birth);
                mEtIdnumber01.setTextColor(getResources().getColor(R.color.colorRed));
                if (!bisvalidIdnumber)
                    mEtIdnumber02.setTextColor(getResources().getColor(R.color.colorRed));
                else
                    mEtIdnumber02.setTextColor(getResources().getColor(R.color.black));
            } else {
                mTvBirthError.setText(R.string.msg_invalid_idnumber);
                mEtIdnumber02.setTextColor(getResources().getColor(R.color.colorRed));
                if (!bisvalidBirth)
                    mEtIdnumber01.setTextColor(getResources().getColor(R.color.black));
                else
                    mEtIdnumber01.setTextColor(getResources().getColor(R.color.colorRed));
            }
        }
    }

    /**
     * 텍스트 입력 박스 background 처리
     *
     * @param boxdindex editbox의 index(name, birth, email)
     * @param state     editbox의 상태(normal, focus, error)
     */
    private void setEditTextBackground(int boxdindex, int state) {
        switch (boxdindex) {
            case Const.INDEX_EDITBOX_NAME: {
                switch (state) {
                    case Const.STATE_EDITBOX_NORMAL: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top);
                        mVLine1.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        mTvNameCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    case Const.STATE_EDITBOX_FOCUSED: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top_focused);
                        mVLine1.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        mTvNameCaption.setTextColor(getResources().getColor(R.color.color00A2B3));
                        break;
                    }
                    case Const.STATE_EDITBOX_ERROR: {
                        mLayoutName.setBackgroundResource(R.drawable.background_box_top_error);
                        mVLine1.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        mTvNameCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            case Const.INDEX_EDITBOX_BIRTH: {
                switch (state) {
                    case Const.STATE_EDITBOX_NORMAL: {
                        mLayoutBirth.setBackgroundResource(R.drawable.background_box_bottom);
                        mVLine1.setBackgroundColor(Color.rgb(0xe7, 0xe7, 0xe7));
                        mTvIdCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    case Const.STATE_EDITBOX_FOCUSED: {
                        mLayoutBirth.setBackgroundResource(R.drawable.background_box_bottom_focused);
                        mVLine1.setBackgroundColor(Color.rgb(0x0, 0xa2, 0xb3));
                        mTvIdCaption.setTextColor(getResources().getColor(R.color.color00A2B3));
                        break;
                    }
                    case Const.STATE_EDITBOX_ERROR: {
                        mLayoutBirth.setBackgroundResource(R.drawable.background_box_bottom_error);
                        mVLine1.setBackgroundColor(Color.rgb(0xf8, 0x65, 0x65));
                        mTvIdCaption.setTextColor(getResources().getColor(R.color.color555555));
                        break;
                    }
                    default:
                        break;
                }
                break;
            }
            default:
                break;
        }
    }

    /**
     * 이메일 주소 입력 후 완료 버튼 누를 때 처리
     */
    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Logs.i("oneditoraction");
        mBtnConfirm.setEnabled(checkDataValidation());
        return false;
    }

    /**
     * edittext 간 포커스 전환 처리
     */
    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        Logs.i("focus change" + v.getId() + ", hasfocus : " + hasFocus);
        if (hasFocus) {
            if (!bIsfirstrun)
                bIsfirstrun = true;
        }

        switch (v.getId()) {
            case R.id.et_name: {
                Logs.i("name focus : " + hasFocus);
                if (!hasFocus) {
                    trimEditTextString(mEtName);
                }
                checkVaildUserName(mEtName.getText().toString(), hasFocus);
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT_NAME, 300);
                break;
            }
            case R.id.et_idnumber2: {
                Logs.i("idnumber2 focus : " + hasFocus);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), hasFocus);
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            case R.id.et_idnumber1: {
                Logs.i("idnumber1 focus : " + hasFocus);
                checkVaildBrith(mEtIdnumber01.getText().toString(), mEtIdnumber02.getText().toString(), hasFocus);
                if (hasFocus)
                    mScrollHandler.sendEmptyMessageDelayed(SCROLL_EDITTEXT, 300);
                break;
            }
            default:
                break;
        }
        mBtnConfirm.setEnabled(checkDataValidation());
    }

    /**
     * edittext에 입력된 문자열을 trim처리
     *
     * @param curet trim처리할 edittext
     */
    private void trimEditTextString(CustomEditText curet) {
        String strname = curet.getText().toString().trim();

        if (curet.getId() == R.id.et_name) {
            // textchangeedlistener를 가진 eidttext인 경우 settext 전후로 remove listener, add listener를 해야함
            curet.removeTextChangedListener(this);
            curet.setText(strname);
            curet.setSelection(strname.length());
            curet.addTextChangedListener(this);
        } else {
            curet.setText(strname);
            curet.setSelection(strname.length());
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Logs.d("beforeTextChanged : " + s + ";");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Logs.d("onTextChanged : " + s + ";");
    }

    /**
     * edittext 문자 입력 시 마다 처리
     */
    @Override
    public void afterTextChanged(Editable s) {
        Logs.i("afterTextChanged : " + s + ";");
        View curFocusView = getCurrentFocus();
        if (curFocusView == null) {
            return;
        }
        switch (curFocusView.getId()) {
            case R.id.et_name: {
                //이름 입력 시 한글 이외 문자 필터
                if (!s.toString().matches("^[ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]*$")) {
                    mEtName.removeTextChangedListener(this);
                    mEtName.setText(s.toString().replaceAll("[^ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]", ""));
                    mEtName.setSelection(mEtName.getText().length());
                    mEtName.addTextChangedListener(this);
                }
                break;
            }

            case R.id.et_idnumber1: {
                if (s.toString().length() == 6) {
                    mEtIdnumber02.requestFocus();
                }
                break;
            }

            case R.id.et_idnumber2: {
                checkVaildBrith(mEtIdnumber01.getText().toString(), s.toString(), true);
                if (s.toString().length() == 1 && mTvBirthError.getVisibility() == View.GONE) {
                    imm.hideSoftInputFromWindow(mEtIdnumber02.getWindowToken(), 0);
                    mBtnConfirm.setEnabled(checkDataValidation());
                    mBtnPlaceholderDot.setVisibility(View.GONE);
                } else if (s.toString().length() == 0) {
                    mBtnPlaceholderDot.setVisibility(View.VISIBLE);
                } else {
                    mBtnPlaceholderDot.setVisibility(View.GONE);
                }
                break;
            }

            default:
                break;
        }
    }

    /**
     * 키보드에서 back key 입력 시 처리
     */
    @Override
    public void onKeyboardBackPress() {
        if (keyboardBackKeyPressed(getCurrentFocus().getId())) {
            mBtnConfirm.setEnabled(checkDataValidation());
            return;
        }
    }

    private class WeakHandler extends Handler {
        private WeakReference<CertifyPhonePersonalInfoActivity> mWeakActivity;

        WeakHandler(CertifyPhonePersonalInfoActivity weakactivity) {
            mWeakActivity = new WeakReference<CertifyPhonePersonalInfoActivity>(weakactivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            CertifyPhonePersonalInfoActivity weakactivity = mWeakActivity.get();
            if (weakactivity != null) {
                switch (msg.what) {
                    case SCROLL_EDITTEXT: {
                        if (!mIsShownKeyboard) {
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollOffset);
                        }
                        break;
                    }
                    case SCROLL_EDITTEXT_NAME: {
                        if (!mIsShownKeyboard) {
                            mSvScview.smoothScrollTo(0, mSvScview.getScrollY() + mScrollNameOffset);
                        }
                        break;
                    }
                    case SCROLL_CHECK_KEYBOARD: {
                        mIsShownKeyboard = true;
                        break;
                    }
                    default:
                        break;
                }
            }
        }
    }
}
