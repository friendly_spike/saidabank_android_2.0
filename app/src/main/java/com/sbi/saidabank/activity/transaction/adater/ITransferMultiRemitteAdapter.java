package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.activity.transaction.itransfer.OnITransferMultiActionListener;
import com.sbi.saidabank.activity.transaction.itransfer.multi.ViewHolderHeader;
import com.sbi.saidabank.activity.transaction.itransfer.multi.ViewHolderAddRcv;
import com.sbi.saidabank.activity.transaction.itransfer.multi.ViewHolderRemitte;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import java.util.ArrayList;

/**
 * 받는사람 검색 어댑터
 */
public class ITransferMultiRemitteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private OnITransferMultiActionListener mListener;
    private ArrayList<MultiListType> mListItems;

    public ITransferMultiRemitteAdapter(Context context, OnITransferMultiActionListener listener) {
        mContext = context;
        mListener = listener;
        mListItems = new ArrayList<MultiListType>();
        makeMultiTransInfoList();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case MultiListType.HEADER:
                return ViewHolderHeader.newInstance(parent,mListener);
            case MultiListType.ITEM_REMITTE:
                return ViewHolderRemitte.newInstance(parent,mListener);
            case MultiListType.ITEM_ADD_TRANSFER:
                return ViewHolderAddRcv.newInstance(parent,mListener);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderHeader) {
            ((ViewHolderHeader)holder).onBind();
        }else if (holder instanceof ViewHolderRemitte) {
            ((ViewHolderRemitte)holder).onBind(position,mListItems.get(position).getItemPosition());
        }else{
            ((ViewHolderAddRcv)holder).onBind();
        }
    }

    @Override
    public int getItemCount() {
        return mListItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mListItems.get(position).getType();
    }


    public void makeMultiTransInfoList(){
        mListItems.clear();
        mListItems.add(new MultiListType(MultiListType.HEADER));
        for(int i=0;i<ITransferDataMgr.getInstance().getRemitteInfoArraySize();i++){
            mListItems.add(new MultiListType(MultiListType.ITEM_REMITTE,i));
        }
        mListItems.add(new MultiListType(MultiListType.ITEM_ADD_TRANSFER));

        notifyDataSetChanged();
    }


    public static class MultiListType {
        public static final int HEADER       = 0;
        public static final int ITEM_REMITTE = 1;
        public static final int ITEM_ADD_TRANSFER     = 2;


        private int type;
        private int itemPosition;


        public MultiListType(int type){
            this.type = type;
            this.itemPosition = -1;
        }

        public MultiListType(int type, int itemPosition){
            this.type = type;
            this.itemPosition = itemPosition;
        }

        public int getType() {
            return type;
        }

        public int getItemPosition() {
            return itemPosition;
        }
    }
}
