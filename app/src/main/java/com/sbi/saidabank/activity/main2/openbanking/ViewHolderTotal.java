package com.sbi.saidabank.activity.main2.openbanking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

/**
 * Create by 20210119
 * 오픈뱅킹 리스트중 첫번째 아이템인 총 계좌정보  부분을 출력
 */

public class ViewHolderTotal extends RecyclerView.ViewHolder{
    private TextView mTvAccountCnt;
    private TextView mTvSumAmount;
    private LinearLayout mLayoutTotal;
    public static ViewHolderTotal newInstance(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main2_openbank_list_item_totalsum, parent, false);
        return new ViewHolderTotal(parent.getContext(),itemView);
    }

    public ViewHolderTotal(Context context, @NonNull View itemView) {
        super(itemView);

        mTvAccountCnt = itemView.findViewById(R.id.tv_account_cnt);
        mTvSumAmount = itemView.findViewById(R.id.tv_sum_amount);
        mLayoutTotal = itemView.findViewById(R.id.layout_total_info);
        int radius = (int) Utils.dpToPixel(context, 12);
        mLayoutTotal.setBackground(GraphicUtils.getRoundCornerDrawable("#47597f", new int[]{ radius, radius, radius, radius }));

    }

    public void onBindView(int cnt,String totalAmount) {
        mTvAccountCnt.setText(String.valueOf(cnt));
        mTvSumAmount.setText(totalAmount);
    }

}
