package com.sbi.saidabank.activity.common;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.contacts.ContactsInfoAdapter;
import com.sbi.saidabank.common.contacts.ContactsUtil;
import com.sbi.saidabank.common.contacts.GetContactListAsyncTask;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: PincodeRegActivity
 * Created by 950485 on 2018. 12. 5..
 * <p>
 * Description: 주소록 정보 출력 화면
 */

public class ContactsPickActivity extends BaseActivity {
	private ListView         mListContacts;
	private EditText         mEditSearch;
	private TextView         mTextSearchCount;
	private RelativeLayout   mLayoutEmpty;

	private ArrayList<ContactsInfo> mContactsList;

	private ContactsInfoAdapter mListAdapter;

	private String[] perList = new String[]{
			Manifest.permission.READ_CONTACTS,
			Manifest.permission.WRITE_EXTERNAL_STORAGE
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contacts_pick);

		getExtra();
		initUX();

		if (PermissionUtils.checkPermission(ContactsPickActivity.this, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
			File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), Const.CONTACTS_INFO_PATH);
			if (file.exists())
				getListContacts();
			else
				showContactsList(false);
		} else {
			showContactsList(false);
		}

		new Handler().postDelayed(new Runnable() {
			public void run() {
				mEditSearch.requestFocus();
				Utils.showKeyboard(ContactsPickActivity.this, mEditSearch);
			}
		}, 300);
	}

	@Override
	public void onDestroy() {
		if (mListAdapter != null) {
			mListAdapter.release();
			mListAdapter = null;
		}

		if (mContactsList != null) {
			mContactsList.clear();
			mContactsList = null;
		}

		super.onDestroy();
	}

	@SuppressWarnings("ConstantConditions")
	@Override
	public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		Logs.i("onRequestPermissionsResult");
		switch (requestCode) {
			case Const.REQUEST_PERMISSION_SYNC_CONTACTS: {
				if (grantResults.length > 0 &&
					grantResults[0] == PackageManager.PERMISSION_GRANTED &&
					grantResults[1] == PackageManager.PERMISSION_GRANTED) {
					syncListContacts();
				} else {
					View.OnClickListener cancelClick = new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							finish();
						}
					};

					View.OnClickListener okClick = new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							PermissionUtils.goAppSettingsActivity(ContactsPickActivity.this);
							finish();
						}
					};

					final AlertDialog alertDialog = new AlertDialog(ContactsPickActivity.this);
					alertDialog.mNListener = cancelClick;
					alertDialog.mPListener = okClick;
					alertDialog.msg = getString(R.string.msg_permission_read_contacts);
					alertDialog.mPBtText = getString(R.string.common_setting);
					alertDialog.show();
				}
			}
			break;

			default:
				break;
		}
	}

	private void getExtra() {
		mContactsList = getIntent().getParcelableArrayListExtra(Const.INTENT_CONTACTS_LIST);
		if (mContactsList == null)
			mContactsList = new ArrayList<>();
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mListContacts = (ListView) findViewById(R.id.listview_contact_contacts_pick);
		mEditSearch = (EditText) findViewById(R.id.editview_search_contacts_pick);
		mTextSearchCount = (TextView) findViewById(R.id.textview_result_count_search_contact);
		mLayoutEmpty = (RelativeLayout) findViewById(R.id.layout_empty_contacts);

		TextView btnClose = (TextView) findViewById(R.id.textview_close_contacts_pick);
		btnClose.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				finish();
			}
		});

        LinearLayout btnRefresh = (LinearLayout) findViewById(R.id.ll_research);
        btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkContactsPermission();
                mEditSearch.setText("");
            }
        });

		mEditSearch.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {

			}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				boolean digitsOnly = TextUtils.isDigitsOnly(s.toString());
				if (digitsOnly) {
					mListAdapter.getPhoneFilter().filter(s.toString());
				} else{
					mListAdapter.getNameFilter().filter(s.toString());
				}
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		mListContacts.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
				final ContactsInfo contactsInfo = mListAdapter.getFilterList().get(position);
				if (contactsInfo == null)
					return;

				Intent intent = new Intent();
				intent.putExtra(Const.INTENT_CONTACTS_PHONE_INFO, contactsInfo);
				intent.putExtra(Const.INTENT_CONTACTS_LIST, mContactsList);
				setResult(RESULT_OK, intent);
				finish();

			}
		});
	}

	/**
	 * 키패드 닫기
	 */
	private void hideSoftInput() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mEditSearch.getWindowToken(), 0);
	}

	/**
	 * 휴대폰 주소록 동기화를 위한 퍼시션 획득
	 */
	private void checkContactsPermission() {
		if (PermissionUtils.checkPermission(ContactsPickActivity.this, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
			syncListContacts();
		}
	}

	/**
	 * 폰에 등록된 리스트 획득 후 동기화 요청
	 */
	private void syncListContacts() {

		GetContactListAsyncTask gpbat = new GetContactListAsyncTask(this,
				GetContactListAsyncTask.GET_TYPE_CONTACT_APP,
				new GetContactListAsyncTask.OnFinishReadPhoneBook() {
					@Override
					public void onFinishReadPhoneBook(ArrayList<ContactsInfo> array) {
						if(array != null && array.size() > 0){
							requestCheckContacts(array);
						}
					}
		});
		gpbat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	/**
	 * 휴대전화번호로 고객정보조회
	 * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
	 */
	void requestCheckContacts(final ArrayList<ContactsInfo> listContactsInPhonebook) {
		if (mContactsList != null)
		    mContactsList.clear();

		JSONArray jsonArray = ContactsUtil.convertToJsonArray(listContactsInPhonebook);
		//폰에 저장된 연락처가 없으면 동기화 하지 않는다.
		if(jsonArray.length() == 0 ) return;

		Map param = new HashMap();
		param.put("REC_IN_TLNO", jsonArray);
		showProgressDialog();
		HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				dismissProgressDialog();
				Logs.i("TRA0020200A01 : " + ret);

				if(onCheckHttpError(ret,false)){
					return;
				}

				try {
					JSONObject object = new JSONObject(ret);

					JSONArray array = object.optJSONArray("REC_OUT_TLNO");
					if (array == null) {
						mContactsList = listContactsInPhonebook;
						return;
					}

					ContactsUtil.saveSyncContactData(ContactsPickActivity.this,ret);


					if (mContactsList != null)
					    mContactsList.clear();
					else
						mContactsList = new ArrayList<>();

					for (int index = 0; index < array.length(); index++) {
						JSONObject objItem = array.getJSONObject(index);
						ContactsInfo contactsInfo = new ContactsInfo(objItem);
						if (contactsInfo == null)
							continue;

						mContactsList.add(contactsInfo);
					}
					showContactsList(true);
				} catch (JSONException e) {
					Logs.printException(e);
				}
			}
		});
	}

	/**
	 * 저장된 주소록 파일에서 등록된 휴대폰 리스트 획득
	 */
	private void getListContacts() {
		if (mContactsList != null)
			mContactsList.clear();
		else
			mContactsList = new ArrayList<>();

		GetContactListAsyncTask gpbat = new GetContactListAsyncTask(this,
				GetContactListAsyncTask.GET_TYPE_STORAGE_SAVEFILE,
				new GetContactListAsyncTask.OnFinishReadPhoneBook() {
					@Override
					public void onFinishReadPhoneBook(ArrayList<ContactsInfo> array) {
						if(array != null && array.size() > 0){
							mContactsList.addAll(array);
						}
					}
				});
		gpbat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

	}

	private void showContactsList(boolean isUpate) {
		if (isUpate) {
			if (mListAdapter != null)
				mListAdapter = null;
		}

		if (mListAdapter == null) {
			mListAdapter = new ContactsInfoAdapter(this, mContactsList, mTextSearchCount);
			mListContacts.setAdapter(mListAdapter);
		}

		if (mContactsList == null || mContactsList.size() < 1) {
			mListContacts.setVisibility(View.GONE);
			mLayoutEmpty.setVisibility(View.VISIBLE);
			mTextSearchCount.setText(getString(R.string.contacts_result_no_contact_list));
			//mTextSearchCount.setText(getString(R.string.contacts_result_search_count, "0"));
		} else {
			mListContacts.setVisibility(View.VISIBLE);
			mLayoutEmpty.setVisibility(View.GONE);
			mTextSearchCount.setText(getString(R.string.contacts_result_search_count, String.valueOf(mContactsList.size())));
		}
	}
}