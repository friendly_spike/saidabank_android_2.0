package com.sbi.saidabank.activity.transaction;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebTermsActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * siadabank_android
 * Class: TransferPolicyActivity
 * Created by 950546
 * Date: 2019-02-14
 * Time: 오전 11:26
 * Description: 휴대폰이체 약관 화면
 */
public class TransferPhonePolicyActivity extends BaseActivity implements View.OnClickListener {
    private CheckBox mChboxNecessary;
    private Button mBtnEasyTransferPolicy;
    private Button mBtnOK;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_phone_policy);

        initView();
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_SYNC_CONTACTS: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    syncListContacts();
                } else {
                    showErrorMessage(getString(R.string.common_msg_sync_permission));
                }
                break;
            }

            default:
                break;
        }
    }

    /**
     * UI 초기화
     */
    private void initView() {
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.ll_necessarypolicy).setOnClickListener(this);
        findViewById(R.id.layout_easytransferpolicy).setOnClickListener(this);
        mChboxNecessary = findViewById(R.id.chbox_necessary);
        mBtnEasyTransferPolicy = findViewById(R.id.btn_easytransferpolicy);
        mBtnOK = findViewById(R.id.btn_confirm);
        mBtnOK.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel: {
                finish();
                break;
            }

            case R.id.btn_confirm: {
                requestAgressPhoneTransfer();
                break;
            }

            case R.id.ll_necessarypolicy: {
                if (mChboxNecessary.isChecked()) {
                    mChboxNecessary.setChecked(false);
                    mBtnEasyTransferPolicy.setEnabled(false);
                    mBtnOK.setEnabled(false);
                } else {
                    mChboxNecessary.setChecked(true);
                    mBtnEasyTransferPolicy.setEnabled(true);
                    mBtnOK.setEnabled(true);
                    Intent intent = new Intent(TransferPhonePolicyActivity.this, WebTermsActivity.class);
                    intent.putExtra("title", "약관");
                    intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1030");
                    startActivity(intent);
                }
                break;
            }

            case R.id.layout_easytransferpolicy: {
                Intent intent = new Intent(TransferPhonePolicyActivity.this, WebTermsActivity.class);
                intent.putExtra("title", "약관");
                intent.putExtra("url", WasServiceUrl.CMM0010600A01.getServiceUrl() + "?CHNL_STPL_FILE_ID=1030");
                startActivity(intent);
                break;
            }

            default:
                break;
        }
    }

    /**
     * 간편이체약관동의 등록
     */
    private void requestAgressPhoneTransfer() {
        Map param = new HashMap();
        param.put("TRN_DVCD", "2");
        param.put("SMPL_TRNF_STPL_AGR_YN", "Y");

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0020100A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    mChboxNecessary.setChecked(false);
                    mBtnEasyTransferPolicy.setEnabled(false);
                    mBtnOK.setEnabled(false);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        mChboxNecessary.setChecked(false);
                        mBtnEasyTransferPolicy.setEnabled(false);
                        mBtnOK.setEnabled(false);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        mChboxNecessary.setChecked(false);
                        mBtnEasyTransferPolicy.setEnabled(false);
                        mBtnOK.setEnabled(false);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        mChboxNecessary.setChecked(false);
                        mBtnEasyTransferPolicy.setEnabled(false);
                        mBtnOK.setEnabled(false);
                        return;
                    }

                    String result = object.optString("SMPL_TRNF_STPL_AGR_YN");
                    if (Const.REQUEST_WAS_YES.equalsIgnoreCase(result)) {
                        mChboxNecessary.setChecked(true);
                        mBtnEasyTransferPolicy.setEnabled(true);
                        mBtnOK.setEnabled(true);

                        Intent intent = new Intent(TransferPhonePolicyActivity.this, TransferPhoneActivity.class);
                        startActivity(intent);
                        finish();
                        return;
                    } else {
                        showErrorMessage("약관 동의 처리에 실패하였습니다.");
                        mChboxNecessary.setChecked(false);
                        mBtnEasyTransferPolicy.setEnabled(false);
                        mBtnOK.setEnabled(false);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 휴대폰 주소록 동기화를 위한 퍼시션 획득
     */
    private void checkContactsPermission() {
        String[] perList = new String[]{
                Manifest.permission.READ_CONTACTS,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if (PermissionUtils.checkPermission(TransferPhonePolicyActivity.this, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), Const.CONTACTS_INFO_PATH);
            if (file.exists()) {
                getListContacts();
            } else {
                syncListContacts();
            }
        }
    }

    /**
     * 저장된 주소록 파일에서 등록된 휴대폰 리스트 획득
     */
    private void getListContacts() {
        StringBuffer sb = new StringBuffer("");
        try {
            File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            String path = storageDir + "/" + Const.CONTACTS_INFO_PATH;
            FileInputStream fIn = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fIn);
            BufferedReader buffreader = new BufferedReader(isr);
            String readString = buffreader.readLine();
            while (readString != null) {
                sb.append(readString);
                readString = buffreader.readLine();
            }
            isr.close();

            if (!TextUtils.isEmpty(sb)) {
                try {
                    JSONObject object = new JSONObject(sb.toString());
                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null)
                        return;

                    ArrayList<ContactsInfo> listContacts = new ArrayList<>();
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);
                        if (contactsInfo == null)
                            continue;

                        listContacts.add(contactsInfo);
                    }
                    showTransferPhone(listContacts);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }

        } catch (IOException e) {
            Logs.printException(e);
        }
    }

    /**
     * 폰에 등록된 리스트 획득 후 동기화 요청
     */
    private void syncListContacts() {
        ArrayList<ContactsInfo> listContactsInPhonebook = new ArrayList<>();

        ContactsInfo contactsInfo;
        Cursor contactCursor = null;

        String name;
        String phonenumber;

        try {
            Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            String[] projection = new String[]{
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_ID};

            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

            contactCursor = getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
            if (contactCursor.moveToFirst()) {
                do {
                    phonenumber = contactCursor.getString(1);
                    if (phonenumber.length() <= 0)
                        continue;

                    name = contactCursor.getString(2);
                    phonenumber = phonenumber.replaceAll("-", "");
                    Logs.e("phonenumber : " + phonenumber);
                    if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                        continue;
                    }

                    // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 유효한 핸드폰 번호인 경우
                    if (!TextUtils.isEmpty(name) && listContactsInPhonebook.indexOf(new ContactsInfo(name)) == -1 && "010".equals(phonenumber.substring(0, 3))) {
                        contactsInfo = new ContactsInfo();
                        contactsInfo.setTLNO(phonenumber);
                        contactsInfo.setFLNM(name);
                        listContactsInPhonebook.add(contactsInfo);
                    }
                } while (contactCursor.moveToNext());
            }
        } catch (Exception e) {
            Logs.printException(e);
        } finally {
            if (contactCursor != null) {
                contactCursor.close();
            }
            requestCheckContacts(listContactsInPhonebook);
        }
    }

    /**
     * 휴대전화번호로 고객정보조회
     *
     * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
     */
    void requestCheckContacts(final ArrayList<ContactsInfo> listContactsInPhonebook) {
        Map param = new HashMap();
        JSONArray jsonArray = new JSONArray();
        for (int index = 0; index < listContactsInPhonebook.size(); index++) {
            ContactsInfo contactsInfo = listContactsInPhonebook.get(index);
            String TLNO = contactsInfo.getTLNO();
            if (TextUtils.isEmpty(TLNO))
                continue;

            try {
                JSONObject itemObject = new JSONObject();
                itemObject.put("TLNO", TLNO);

                String FLNM = contactsInfo.getFLNM();
                if (TextUtils.isEmpty(FLNM)) FLNM = "";
                itemObject.put("FLNM", FLNM);

                jsonArray.put(itemObject);
            } catch (JSONException e) {
                Logs.printException(e);
            }
        }
        param.put("REC_IN_TLNO", jsonArray);
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("TRA0020200A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showTransferPhone(listContactsInPhonebook);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null) {
                        showTransferPhone(listContactsInPhonebook);
                        return;
                    }

                    File storageDir = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
                    if (!storageDir.exists())
                        storageDir.mkdir();

                    String path = storageDir + "/" + Const.CONTACTS_INFO_PATH;
                    try {
                        FileWriter fw = new FileWriter(path, false);
                        fw.write(ret);
                        fw.flush();
                        fw.close();
                    } catch (Exception e) {
                        Logs.printException(e);
                    }

                    ArrayList<ContactsInfo> listContacts = new ArrayList<>();
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);
                        if (contactsInfo == null)
                            continue;

                        listContacts.add(contactsInfo);
                    }

                    showTransferPhone(listContacts);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 휴대전화번호 이체화면으로 이동
     */
    private void showTransferPhone(ArrayList<ContactsInfo> listContacts) {
        Intent intent = new Intent(TransferPhonePolicyActivity.this, TransferPhoneActivity.class);
        intent.putParcelableArrayListExtra(Const.INTENT_TRANSFER_PHONE_INFO, listContacts);
        startActivity(intent);
    }
}
