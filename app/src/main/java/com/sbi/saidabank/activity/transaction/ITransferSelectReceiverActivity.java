package com.sbi.saidabank.activity.transaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.adater.ITransferRecentlyAdapter;
import com.sbi.saidabank.activity.transaction.adater.ITransferSelectRcvMyAccountAdapter;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.contacts.ContactsInfoAdapter;
import com.sbi.saidabank.common.contacts.ContactsUtil;
import com.sbi.saidabank.common.contacts.GetContactListAsyncTask;
import com.sbi.saidabank.common.dialog.SlidingAccountInputDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.dialog.TransferEditAliasDialog;
import com.sbi.saidabank.common.dialog.TransferPhoneRealNameDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CheatUseAccountInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Create 210125
 * 받는분 선택 화면
 * 최근/즐겨찾기,내계좌,연락처의 3개의 탭으로 구성
 * 각 탭별 통합 검색 지원.
 */
public class ITransferSelectReceiverActivity extends BaseActivity implements View.OnClickListener{
    public static final int REQUEST_CODE_SEARCH  = 1000;

    public static final int TAB_RECENTLY  = 0;
    public static final int TAB_MYACCOUNT = 1;
    public static final int TAB_CONTACT   = 2;


    //연락처 접근 권한
    private String[] perList = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    //탭영역
    private TextView mTvTabRecently;
    private TextView mTvTabMyAccount;
    private TextView mTvTabContact;

    //최근,즐겨찾기기 관련
    private RelativeLayout    mLayoutRecently;
    private SwipeMenuListView mListViewRecently;
    private LinearLayout      mLayoutEmptyRecently;
    private ArrayList<ITransferRecentlyInfo> mRecentlyArrayList;
    private ITransferRecentlyAdapter mRecentlyAdapter;

    //내계좌 관련
    private RelativeLayout    mLayoutMyAccount;
    private ListView      mListViewMyAccount;
    private ArrayList<MyAccountInfo> mMyAccountArrayList;
    private ArrayList<OpenBankAccountInfo> mOpenBankAccountArrayList;//오픈뱅킹계좌목록
    private ITransferSelectRcvMyAccountAdapter  mMyAccountAdapter;
    private LinearLayout      mLayoutEmptyMyAccount;

    //연락처 관련
    private RelativeLayout    mLayoutContact;
    private ListView mListViewContact;
    private LinearLayout    mLayoutEmptycontact;
    private ContactsInfoAdapter     mContactAdapter;
    private TextView            mTvContactCount;
    private ArrayList<ContactsInfo> mContactArrayList;

    //직접입력버튼
    private LinearLayout        mLayoutDirectInput;
    private TextView            mTvDirecctInput;

    //선택된 현재 탭
    private int            mCurrentTab = TAB_RECENTLY;


    //받는 사람 정보 - 메인화면에서 물고 들어옴.클립보드 복사로 진입할경우
    private String         mRcvBankCd;
    private String         mRcvAccountNo;

    private boolean        isAddTransfer;

    //최근/즐겨찾기,오픈뱅킹계좌,연락처 조회 완료 체크
    private Handler        mRequestCheckHandler;
    private boolean        isEndRequestFavorite;
    private boolean        isEndRequestMyAccount;


    /**
     * 타임체크로 시작시의 즐겨찾기와 은행/증권사 코드의 request상태를 확인한다.
     */
    private Runnable mRequestCheckRunnable = new Runnable() {
        @Override
        public void run() {

            Logs.e("TransferSafeDealActivity - Runnable - check request ~~");

            if (isEndRequestFavorite && isEndRequestMyAccount) {
                dismissProgressDialog();


                //오픈뱅킹 계좌 만료 1개월 전 체크
                //만료는 initAndSyncSessionAndCheckAuthMethod에서 체크한다.
                String OBA_RESP_CD = LoginUserInfo.getInstance().getOBA_RESP_CD();
                if(TextUtils.isEmpty(OBA_RESP_CD)) OBA_RESP_CD = "0";
                if(OBA_RESP_CD.equalsIgnoreCase("1")){
                    Intent intent = new Intent(ITransferSelectReceiverActivity.this, WebMainActivity.class);
                    intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0110100.getServiceUrl());
                    startActivity(intent);
                }else{
                    //메인에서 물고들어온 받는사람정보가 있으면 다이얼로그 출력.
                    if(!TextUtils.isEmpty(mRcvAccountNo) && !TextUtils.isEmpty(mRcvBankCd)){
                        showAccountInputDialog(mRcvBankCd,"",mRcvAccountNo);
                    }
                }
            } else {
                mRequestCheckHandler.postDelayed(mRequestCheckRunnable, 500);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itransfer_select_receiver);

        //메인에서 물고들어온 받는사람 정보
        mRcvAccountNo = getIntent().getStringExtra(Const.INTENT_RCV_ACCOUNT);
        mRcvBankCd = getIntent().getStringExtra(Const.INTENT_RCV_BANK_CODE);
        //주석 - 000을 028로 바꾸면 안된다. 000은 오픈뱅킹 SBI계좌이다.
//        if (!TextUtils.isEmpty(mRcvAccountNo) && "000".equalsIgnoreCase(mRcvBankCd))
//            mRcvBankCd = "028";

        //이체추가로 진입한 경우
        isAddTransfer = getIntent().getBooleanExtra(Const.INTENT_IS_ADD_TRANSFER,false);

        mRecentlyArrayList = new ArrayList<ITransferRecentlyInfo>();
        mMyAccountArrayList = new ArrayList<MyAccountInfo>() ;
        mOpenBankAccountArrayList = new ArrayList<OpenBankAccountInfo>() ;
        mContactArrayList = new ArrayList<ContactsInfo>();

        initView();

        //취근/즐겨찾기와 내계좌 리스트만 만든다.
        makeTabList(false);

    }

    private void initView(){

        //닫기버튼
        findViewById(R.id.tv_close).setOnClickListener(this);

        //검색버튼
        findViewById(R.id.iv_search).setOnClickListener(this);

        //탭영역
        mTvTabRecently = findViewById(R.id.tv_tab_recently);
        mTvTabMyAccount = findViewById(R.id.tv_tab_myaccount);
        mTvTabContact = findViewById(R.id.tv_tab_contact);
        mTvTabRecently.setOnClickListener(this);
        mTvTabMyAccount.setOnClickListener(this);
        mTvTabContact.setOnClickListener(this);

        //컨텐츠 영역 배경
        int radius = (int) Utils.dpToPixel(this,20);
        findViewById(R.id.layout_content_area).setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));


        //최근,즐겨찾기
        mLayoutRecently = findViewById(R.id.layout_recently);
        mListViewRecently = findViewById(R.id.listview_recently);
        mLayoutEmptyRecently = findViewById(R.id.layout_empty_recently);
        mRecentlyAdapter = new ITransferRecentlyAdapter(this, mRecentlyArrayList, new ITransferRecentlyAdapter.OnFavoriteItemClickListener() {
            @Override
            public void onItemClick(int position) {
                ITransferRecentlyInfo recentlyInfo = mRecentlyArrayList.get(position);
                TransferRequestUtils.requestChangeFavoriteState(ITransferSelectReceiverActivity.this, recentlyInfo, new HttpSenderTask.HttpRequestListener() {
                    @Override
                    public void endHttpRequest(String ret) {
                        requestRecentlyList(true);
                    }
                });
            }
        });
        mListViewRecently.setAdapter(mRecentlyAdapter);
        setSwipMenuRecentlyList();
        mListViewRecently.addHeaderView(getEmptyHeaderFooterView(false));
        mListViewRecently.addFooterView(getEmptyHeaderFooterView(true));

        //내계좌 관련
        mLayoutMyAccount= findViewById(R.id.layout_myaccount);
        mListViewMyAccount = findViewById(R.id.listview_myaccount);
        mMyAccountAdapter = new ITransferSelectRcvMyAccountAdapter(this);
        mListViewMyAccount.setAdapter(mMyAccountAdapter);
        View myAccountHeader = getLayoutInflater().inflate(R.layout.layout_listview_header_myaccount, null, false) ;
        mListViewMyAccount.addHeaderView(myAccountHeader);
        mListViewMyAccount.addFooterView(getEmptyHeaderFooterView(true));
        mListViewMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Logs.e("position : " + position);
                Logs.e("mMyAccountAdapter.getCount() : " + mMyAccountAdapter.getCount());
                int realPos = position - 1;
                //Header와 Footer를 추가했더니.. Header는 0번, Footer는 마지막번으로 넘어오는 문제가 발생한다.
                if(realPos < 0 || realPos >= mMyAccountAdapter.getCount()) return;

                Object accountInfo = mMyAccountAdapter.getAccountInfo(realPos);

                String BANK_CD = "";
                String DETAIL_BANK_CD = "";
                String ACNO = "";
                if(accountInfo instanceof MyAccountInfo){
                    BANK_CD = "028";
                    ACNO = ((MyAccountInfo)accountInfo).getACNO();
                }else{
                    BANK_CD = ((OpenBankAccountInfo)accountInfo).RPRS_FNLT_CD;
                    DETAIL_BANK_CD = ((OpenBankAccountInfo)accountInfo).DTLS_FNLT_CD;
                    ACNO = ((OpenBankAccountInfo)accountInfo).ACNO;
                }

                //계좌이체 수취인조회 *******
                checkRemitteeAccount(BANK_CD,DETAIL_BANK_CD,ACNO,LoginUserInfo.getInstance().getCUST_NM());
            }
        });
        mLayoutEmptyMyAccount = findViewById(R.id.layout_empty_myaccount);
        mLayoutEmptyMyAccount.setVisibility(View.GONE);

        //연락처 관련
        mTvContactCount = findViewById(R.id.tv_contact_count);
        mContactAdapter = new ContactsInfoAdapter(this,mContactArrayList,null);
        mLayoutContact  = findViewById(R.id.layout_contact);
        mListViewContact = findViewById(R.id.listview_contact);
        mListViewContact.setAdapter(mContactAdapter);

        mListViewContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactsInfo contactsInfo = mContactArrayList.get(position);

                final String TLNO = contactsInfo.getTLNO();
                final String FLNM = contactsInfo.getFLNM();

                TransferPhoneRealNameDialog dialog = new TransferPhoneRealNameDialog(ITransferSelectReceiverActivity.this, FLNM, TLNO, false);
                dialog.setOnConfirmListener(new TransferPhoneRealNameDialog.OnConfirmListener() {
                    @Override
                    public void onConfirmPress(String name, String TLNO, boolean isFromContactList) {
                        ContactsInfo cInfo = new ContactsInfo(name,TLNO);
                        ITransferDataMgr.getInstance().setTransferContactsInfo(cInfo);

                        Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendSingleActivity.class);
                        startActivity(intent);
                        finishActivity(false);
                    }
                });
                dialog.show();
            }
        });
        findViewById(R.id.layout_btn_resync).setOnClickListener(this);
        mLayoutEmptycontact = findViewById(R.id.layout_empty_contacts);



        //은행/계좌 직접입력 버튼
        mLayoutDirectInput = findViewById(R.id.layout_direct_input);
        mTvDirecctInput = findViewById(R.id.btn_direct_input);
        mTvDirecctInput.setOnClickListener(this);
        radius = (int) Utils.dpToPixel(this,6);
        mTvDirecctInput.setBackground(GraphicUtils.getRoundCornerDrawable("#00ebff",new int[]{radius,radius,radius,radius}));

        changeTabState();

        if (CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_YN().equals("Y")){
            showCheatUseAccountDialog();
        }
    }

    /**
     * 사기이용계좌 금융거래제한 안내 팝업
     */
    private void showCheatUseAccountDialog(){
        String replaceStr = "";
        String str = "고객님께서는 금융기관에\n전자금융거래제한자로 등록되어 있어 거래가\n불가합니다";

        String FinancialInstitutionNM = CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_FIN_INST_NM(); //금융기관명
        if (!FinancialInstitutionNM.isEmpty()){
            replaceStr = str.replace("금융기관", FinancialInstitutionNM);
        }

        DialogUtil.alert(ITransferSelectReceiverActivity.this,
                replaceStr ,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
    }

    private void finishActivity(boolean result){
        if(isAddTransfer && result){
            setResult(RESULT_OK);
        }
        finish();
    }

    /**
     * 각 리스트뷰의 TOP 마진값이 위치상 이상하여 헤더와 푸터를 넣어준다.
     *
     * @param isFooter true - 리스트뷰의 풋터, false - 리스트뷰의 헤더뷰
     * @return
     */
    private View getEmptyHeaderFooterView(boolean isFooter){

        int height = (int)Utils.dpToPixel(this,19f);
        if(isFooter){
            height = (int)Utils.dpToPixel(this,80f);
        }

        View footer = getLayoutInflater().inflate(R.layout.layout_listview_header_footer_empty, null, false) ;

        View view = footer.findViewById(R.id.view);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = height;
        view.setLayoutParams(params);


        return footer;
    }

    /**
     * 각 탭의 리스트 들을 생성한다.
     */
    private void makeTabList(boolean isSyncServerContact){

        isEndRequestFavorite = false;
        isEndRequestMyAccount = false;


        mRequestCheckHandler = new Handler();
        mRequestCheckHandler.postDelayed(mRequestCheckRunnable, 500);

        showProgressDialog();

        //최근,즐겨찾기 조회
        requestRecentlyList(false);

        //내계좌,오픈뱅킹 계좌 리스트 만든다.
        if(OpenBankDataMgr.getInstance().getAccountCnt() > 0){
            makeMyAccountList();
        }else {
            requestOpenBankList();
        }
    }

    private void makeTabContactList(boolean isSyncServerContact){
        //출금계좌가 사이다계좌일때만 휴대폰 리스트조회 한다.
        if(TransferUtils.isSaidaAccount()){
            if(isSyncServerContact){
                //연락처 조회후 사이다 서버와 비교.
                getContactListFromContactApp(true,false);
            }else{
                //연락처
                File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), Const.CONTACTS_INFO_PATH);
                if (file.exists()){
                    getContactListFromStorageFile();
                }
            }
        }
    }

    /**
     * 최근/자주 리스트의 Swip메뉴를 처리한다.
     */
    private void setSwipMenuRecentlyList(){
        mListViewRecently.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Logs.e("mListViewRecently.setOnItemClickListener position : " + position );

                int realPos = position - 1;
                //Header와 Footer를 추가했더니.. Header는 0번, Footer는 마지막번으로 넘어오는 문제가 발생한다.
                if(realPos < 0 || realPos >= mRecentlyArrayList.size()) return;

                ITransferRecentlyInfo recentlyInfo = mRecentlyArrayList.get(realPos);

                if(!TextUtils.isEmpty(recentlyInfo.getMNRC_TLNO())){ //휴대폰이체
                    //사이다이체가 아니면...
                    if(!TransferUtils.isSaidaAccount()){
                        //오픈뱅킹 계좌일경우엔 휴대폰 이체 사용불가
                        DialogUtil.alert(ITransferSelectReceiverActivity.this,getString(R.string.itransfer_msg_phone_only_able_saida));
                        return;
                    }

                    //지연이체, 입금지정계좌서비스 체크
                    if(!TransferUtils.checkAbleServiceCondition(ITransferSelectReceiverActivity.this,"휴대폰이체")){
                        return;
                    }

                    //이체 추가로 들어왔으면 휴대폰은 추가할수 없다.
                    if(isAddTransfer){
                        DialogUtil.alert(ITransferSelectReceiverActivity.this,getString(R.string.itransfer_msg_able_add_transfer));
                        return;
                    }

                    //휴대폰이체 *******
                    final String MNRC_TLNO = recentlyInfo.getMNRC_TLNO();
                    final String MNRC_ACCO_DEPR_NM = recentlyInfo.getMNRC_ACCO_DEPR_NM();


                    TransferPhoneRealNameDialog transferPhoneRealNameDialog = new TransferPhoneRealNameDialog(ITransferSelectReceiverActivity.this, MNRC_ACCO_DEPR_NM, MNRC_TLNO, false);
                    transferPhoneRealNameDialog.setOnConfirmListener(new TransferPhoneRealNameDialog.OnConfirmListener() {
                        @Override
                        public void onConfirmPress(String name, String TLNO, boolean isFromContactList) {
                            ContactsInfo cInfo = new ContactsInfo(name,TLNO);
                            ITransferDataMgr.getInstance().setTransferContactsInfo(cInfo);

                            Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendSingleActivity.class);
                            startActivity(intent);
                            finishActivity(false);
                        }
                    });
                    transferPhoneRealNameDialog.show();
                }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){ //그룹이체.
                    //그룹이체 *********
                    if(!TransferUtils.isSaidaAccount()){
                        DialogUtil.alert(ITransferSelectReceiverActivity.this,getString(R.string.itransfer_msg_group_only_able_saida));
                        return;
                    }

                    //지연이체, 입금지정계좌서비스 체크
                    if(!TransferUtils.checkAbleServiceCondition(ITransferSelectReceiverActivity.this,"다건이체")){
                        return;
                    }

                    //그룹 등록 가능인원 체크
                    String groupCntText = recentlyInfo.getGRP_CCNT();
                    if(!TextUtils.isEmpty(groupCntText)){
                        int goupCnt = Integer.parseInt(groupCntText);
                        int saveRemitteeCnt = ITransferDataMgr.getInstance().getRemitteInfoArraySize();
                        if(goupCnt + saveRemitteeCnt > 5){
                            DialogUtil.alert(ITransferSelectReceiverActivity.this,"최대 5건까지\n다건 이체 등록이 가능합니다.");
                            return;
                        }
                    }

                    //여기서 그룹 그룹 수취조회한다.
                    checkRemitteeGroup(recentlyInfo.getGRP_SRNO(),recentlyInfo.getFAVO_ACCO_YN());
                }else{
                    //계좌이체 수취인조회 *******
                    checkRemitteeAccount(recentlyInfo.getMNRC_BANK_CD(),recentlyInfo.getDTLS_FNLT_CD(),recentlyInfo.getMNRC_ACNO(),recentlyInfo.getMNRC_ACCO_DEPR_NM());

                }
            }
        });

        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem editItem = new SwipeMenuItem(getApplicationContext());
                editItem.setBackground(new ColorDrawable(Color.rgb(0x8C, 0x97, 0xAC)));
                editItem.setWidth((int) Utils.dpToPixel(ITransferSelectReceiverActivity.this, 66));
                editItem.setIcon(R.drawable.btn_write_w);
                menu.addMenuItem(editItem);

                SwipeMenuItem deleteItem = new SwipeMenuItem(getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xE0, 0x25, 0x0C)));
                deleteItem.setWidth((int) Utils.dpToPixel(ITransferSelectReceiverActivity.this, 66));
                deleteItem.setIcon(R.drawable.btn_delete_w);
                menu.addMenuItem(deleteItem);
            }
        };

        mListViewRecently.setMenuCreator(creator);
        mListViewRecently.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        mListViewRecently.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(final int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        TransferEditAliasDialog dialog = new TransferEditAliasDialog(ITransferSelectReceiverActivity.this, new TransferEditAliasDialog.OnBtnClickListener() {
                            @Override
                            public void onBtnClick(View v, String alias) {
                                final String newAlias = alias;
                                ITransferRecentlyInfo info = mRecentlyArrayList.get(position);
                                TransferRequestUtils.requestRegisterAlias(ITransferSelectReceiverActivity.this, info, newAlias, new HttpSenderTask.HttpRequestListener() {
                                    @Override
                                    public void endHttpRequest(String ret) {
                                        try {
                                            JSONObject object = new JSONObject(ret);

                                            // 계좌변경 성공여부
                                            if (!Const.REQUEST_WAS_YES.equalsIgnoreCase(object.optString("TRTM_RSLT_CD"))) {
                                                showErrorMessage(getString(R.string.msg_fail_edit_alias));
                                                return;
                                            }

                                            ITransferRecentlyInfo recentlyInfo = mRecentlyArrayList.get(position);
                                            recentlyInfo.setMNRC_ACCO_ALS(newAlias);
                                            mRecentlyArrayList.set(position, recentlyInfo);
                                            mRecentlyAdapter.setRecentlyArrayList(mRecentlyArrayList);
                                            ITransferDataMgr.getInstance().setRecentlyArrayList(mRecentlyArrayList);

                                        } catch (JSONException e) {
                                            MLog.e(e);
                                        }
                                    }
                                });
                            }
                        });
                        dialog.show();
                        break;
                    case 1:
                        DialogUtil.alert(ITransferSelectReceiverActivity.this,
                                getString(R.string.msg_ask_remove),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        ITransferRecentlyInfo info = mRecentlyArrayList.get(position);
                                        TransferRequestUtils.requestRemoveRecentlyItem(ITransferSelectReceiverActivity.this, info, new HttpSenderTask.HttpRequestListener() {
                                            @Override
                                            public void endHttpRequest(String ret) {
                                                mRecentlyArrayList.remove(position);
                                                mRecentlyAdapter.setRecentlyArrayList(mRecentlyArrayList);
                                                ITransferDataMgr.getInstance().setRecentlyArrayList(mRecentlyArrayList);
                                                if(mRecentlyArrayList.size() == 0){
                                                    mLayoutEmptyRecently.setVisibility(View.VISIBLE);
                                                    mListViewRecently.setVisibility(View.GONE);
                                                }
                                            }
                                        });
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                    }
                                });
                        break;
                    default:
                        break;
                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_close:
                finishActivity(false);
                break;
            case R.id.iv_search:
                Intent intent = new Intent(ITransferSelectReceiverActivity.this,ITransferSearchReceiverActivity.class);
                intent.putExtra("ARRAY_RECENTLY",mRecentlyArrayList);
                intent.putExtra("ARRAY_MYACCOUNT",mMyAccountArrayList);
                intent.putExtra("ARRAY_OPENBANK",mOpenBankAccountArrayList);
                intent.putExtra("ARRAY_CONTACT",mContactArrayList);
                intent.putExtra(Const.INTENT_IS_ADD_TRANSFER,isAddTransfer);
                startActivityForResult(intent,REQUEST_CODE_SEARCH);
                break;
            case R.id.tv_tab_recently:
                hideCurrentTabList();
                mCurrentTab = TAB_RECENTLY;
                changeTabState();
                showCurrentTabList();
                break;
            case R.id.tv_tab_myaccount:
                hideCurrentTabList();
                mCurrentTab = TAB_MYACCOUNT;
                changeTabState();
                showCurrentTabList();
                break;
            case R.id.tv_tab_contact:

                //사이다이체가 아니면...
                if(!TransferUtils.isSaidaAccount()){
                    //오픈뱅킹 계좌일경우엔 휴대폰 이체 사용불가
                    DialogUtil.alert(ITransferSelectReceiverActivity.this,getString(R.string.itransfer_msg_phone_only_able_saida));
                    return;
                }

                //지연이체, 입금지정계좌서비스 체크
                if(!TransferUtils.checkAbleServiceCondition(ITransferSelectReceiverActivity.this,"휴대폰이체")){
                    return;
                }

                //이체 추가로 들어왔으면 휴대폰은 추가할수 없다.
                if(isAddTransfer){
                    DialogUtil.alert(ITransferSelectReceiverActivity.this,getString(R.string.itransfer_msg_able_add_transfer));
                    return;
                }else{
                    if (!PermissionUtils.checkPermission(ITransferSelectReceiverActivity.this, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
                        return;
                    }
                    if(mContactArrayList.size() == 0)
                        makeTabContactList(false);
                }

                showProgressDialog();
                hideCurrentTabList();
                mCurrentTab = TAB_CONTACT;
                changeTabState();
                showCurrentTabList();
                dismissProgressDialog();
                break;
            case R.id.layout_btn_resync:
                getContactListFromContactApp(true,true);
                break;
            case R.id.btn_direct_input:
                showBankStockAccountDialog();
                break;
        }
    }

    /**
     * 탭의 상태를 변경한다.
     */
    private void changeTabState(){
        int radius = (int) Utils.dpToPixel(this,15);
        mTvTabRecently.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mTvTabRecently.setTextColor(Color.parseColor("#999999"));
        mTvTabMyAccount.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mTvTabMyAccount.setTextColor(Color.parseColor("#999999"));
        mTvTabContact.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mTvTabContact.setTextColor(Color.parseColor("#999999"));
        switch (mCurrentTab){
            case TAB_RECENTLY:
                mTvTabRecently.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{radius,radius,radius,radius}));
                mTvTabRecently.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            case TAB_MYACCOUNT:
                mTvTabMyAccount.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{radius,radius,radius,radius}));
                mTvTabMyAccount.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            case TAB_CONTACT:
                mTvTabContact.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{radius,radius,radius,radius}));
                mTvTabContact.setTextColor(Color.parseColor("#FFFFFF"));
                break;
        }
    }

    /**
     * 최근,즐겨찾기 리스트 조회
     */
    private void requestRecentlyList(final boolean showProgress){
        Map param = new HashMap();
        param.put("INQ_DVCD", "9"); // 9번이 전체조회이다.-계좌,휴대폰,그룹 모두 조회. 값 않넣어줘도 된다.

        if(showProgress)
            showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0140100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(showProgress)
                    dismissProgressDialog();

                isEndRequestFavorite = true;

                if(onCheckHttpError(ret,false)){
                    if(mCurrentTab == TAB_RECENTLY){
                        showCurrentTabList();
                    }
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) {
                        if(mCurrentTab == TAB_RECENTLY){
                            showCurrentTabList();
                        }
                        return;
                    }

                    mRecentlyArrayList.clear();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        mRecentlyArrayList.add(new ITransferRecentlyInfo(obj));
                    }

                    mRecentlyAdapter.setRecentlyArrayList(mRecentlyArrayList);

                    //추후 받는분선택 다이얼로그에서 사용하기 위해 넣어둔다.
                    ITransferDataMgr.getInstance().setRecentlyArrayList(mRecentlyArrayList);

                    //화면을 Visible한다.
                    if(mCurrentTab == TAB_RECENTLY){
                        showCurrentTabList();
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 오픈뱅킹 계좌 조회
     */
    private void requestOpenBankList(){
        Map param = new HashMap();
        param.put("BLNC_INQ_YN", "N");

        mOpenBankAccountArrayList.clear();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                try {

                    JSONObject object = new JSONObject(ret);

                    //여기는 입금이 가능한 리스트만 보여주면 되기에 모델링한 값은 건들지 않도록.
                    //모델링한 값은 출금계좌에 사용한다.
                    OpenBankDataMgr.getInstance().parsorAccountList(object);
                    //mOpenBankAccountArrayList.addAll(TransferUtils.getOpenBankDepositPosibleAccountList(object));


                    makeMyAccountList();

                } catch (JSONException e) {
                    Logs.printException(e);
                }
                //isEndRequestMyAccount=true;
            }
        });
    }

    /**
     * 물고 들어온 계좌를 리스트에서 보이지 않기위해...
     */
    private void makeMyAccountList(){
        isEndRequestMyAccount=true;
        mMyAccountArrayList.clear();
        mMyAccountArrayList.addAll(LoginUserInfo.getInstance().getMyAccountInfoArrayList());

        mOpenBankAccountArrayList.clear();
        mOpenBankAccountArrayList.addAll(OpenBankDataMgr.getInstance().getOpenBankInOutAccountList());

        if(!TextUtils.isEmpty(ITransferDataMgr.getInstance().getWTCH_ACNO())){
            //물고들어온 출금계좌가 있으면 리스트에서 삭제해준다.
            String BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
            String ACNO    = ITransferDataMgr.getInstance().getWTCH_ACNO();

            if(BANK_CD.equalsIgnoreCase("028")){
                for(int i=0;i<mMyAccountArrayList.size();i++){
                    MyAccountInfo accountInfo = mMyAccountArrayList.get(i);
                    if(accountInfo.getACNO().equalsIgnoreCase(ACNO)){
                        mMyAccountArrayList.remove(i);
                    }
                }
            }else{
                for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                    OpenBankAccountInfo accountInfo = mOpenBankAccountArrayList.get(i);
                    if(accountInfo.RPRS_FNLT_CD.equalsIgnoreCase(BANK_CD)
                        &&accountInfo.ACNO.equalsIgnoreCase(ACNO)){
                        mOpenBankAccountArrayList.remove(i);
                    }
                }
            }
        }else{
            //물고 들어온 계좌가 없으면 사이다계좌리스트의 0번을 기본으로 할당한다. 메인계좌이다.
            if(mMyAccountArrayList.size() > 0){
                MyAccountInfo accountInfo = mMyAccountArrayList.get(0);
                ITransferDataMgr.getInstance().setWTCH_BANK_CD("028");
                ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.getACNO());
                mMyAccountArrayList.remove(0);
            }
        }


        mMyAccountAdapter.setMyAccOpenBankAccArray(mMyAccountArrayList,mOpenBankAccountArrayList);

        if(mMyAccountArrayList.size() + mOpenBankAccountArrayList.size() == 0){
            mListViewMyAccount.setVisibility(View.GONE);
            mLayoutEmptyMyAccount.setVisibility(View.VISIBLE);
        }else{
            mListViewMyAccount.setVisibility(View.VISIBLE);
            mLayoutEmptyMyAccount.setVisibility(View.GONE);
        }
    }

    /**
     * 연락처 처리 관련 함수
     */
    //이미 서버와 싱크해둔 파일에 스토리지에 저장되어 있을때
    private void getContactListFromStorageFile(){
        getContactList(GetContactListAsyncTask.GET_TYPE_STORAGE_SAVEFILE,false,false);
    }

    /**
     * 처음 연락처 권한을 설정하고 연락처 앱에서 주소록을 가져올때.
     * @param syncServer - 사이다 서버와 동기화를 진행 할것인지.
     */
    private void getContactListFromContactApp(boolean syncServer,boolean showProgress){
        getContactList(GetContactListAsyncTask.GET_TYPE_CONTACT_APP,syncServer,showProgress);
    }

    /**
     * 연락처를 가져온다.
     *
     * @param getType
     * GetContactListAsyncTask.GET_TYPE_SAIDA_SERVER - 핸드폰에 저장된 연락처앱에서 가져온다. 서버와 싱크가 필요하다.
     * GetContactListAsyncTask.GET_TYPE_STORAGE_SAVEFILE - 이미 싱크가 되어 저장되어 있는 파일에서 가져온다.
     *
     * @param syncServer : 연락처 앱에서 조회후에 사이다 서버와 싱크할지 그냥 보여줄지.
     * @param showProgress
     */
    private void getContactList(final int getType,final boolean syncServer,final boolean showProgress){
        if(showProgress)
            showProgressDialog();
        //우선 연락처앱에서 전화번호 조회해온다.
        GetContactListAsyncTask gpbat = new GetContactListAsyncTask(this,
                getType,
                new GetContactListAsyncTask.OnFinishReadPhoneBook() {
                    @Override
                    public void onFinishReadPhoneBook(ArrayList<ContactsInfo> array) {
                        Logs.e("getContactList end - size : " + array.size());
                        mContactArrayList.clear();
                        if(array.size() > 0){

                            if(getType == GetContactListAsyncTask.GET_TYPE_CONTACT_APP){
                                if(syncServer){
                                    //모바일폰북에서 전화번호를 가져왔으면 사이다서버와 싱크를 맞춘다.
                                    requestContactListCompareSaidaServer(array,showProgress);
                                    return;
                                }
                            }

                            mContactArrayList.addAll(array);
                            mContactAdapter.setContactList(mContactArrayList);


                            String msg = "전체 " + mContactArrayList.size()+ "건";
                            Utils.setTextWithSpan(mTvContactCount,msg,String.valueOf(mContactArrayList.size()), Typeface.BOLD,"#a1a9bb");

                            if(mCurrentTab == TAB_CONTACT){
                                showCurrentTabList();
                            }
                        }
                        if(showProgress)
                            dismissProgressDialog();
                    }
        });
        gpbat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * 휴대전화번호로 사이다 가입여부 조회
     * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
     */
    void requestContactListCompareSaidaServer(final ArrayList<ContactsInfo> listContactsInPhonebook,final boolean showProgress) {

        JSONArray jsonArray = ContactsUtil.convertToJsonArray(listContactsInPhonebook);
        //폰에 저장된 연락처가 없으면 동기화 하지 않는다.
        if(jsonArray.length() == 0 ){
            return;
        }

        Map param = new HashMap();
        param.put("REC_IN_TLNO", jsonArray);
        if(showProgress)
            showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                Logs.i("TRA0020200A01 : " + ret);
                dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null) {
                        mContactArrayList = listContactsInPhonebook;
                        return;
                    }

                    ContactsUtil.saveSyncContactData(ITransferSelectReceiverActivity.this,ret);

                    mContactArrayList.clear();

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);
                        mContactArrayList.add(contactsInfo);
                    }

                    mContactAdapter.setContactList(mContactArrayList);

                    String msg = "전체 " + mContactArrayList.size()+ "건";
                    Utils.setTextWithSpan(mTvContactCount,msg,String.valueOf(mContactArrayList.size()), Typeface.BOLD,"#a1a9bb");

                    //화면을 보여준다.
                    if(mCurrentTab == TAB_CONTACT){
                        showCurrentTabList();
                    }

                    //메인에서 물고들어온 받는사람정보가 있으면 다이얼로그 출력.
                    if(showProgress && !TextUtils.isEmpty(mRcvAccountNo) && !TextUtils.isEmpty(mRcvBankCd)){
                        showAccountInputDialog(mRcvBankCd,"",mRcvAccountNo);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void hideCurrentTabList(){
        switch (mCurrentTab){
            case TAB_RECENTLY:
                mLayoutRecently.setVisibility(View.GONE);
                break;
            case TAB_MYACCOUNT:
                mLayoutMyAccount.setVisibility(View.GONE);
                break;
            case TAB_CONTACT:
                mLayoutContact.setVisibility(View.GONE);
                break;
        }
    }

    private void showCurrentTabList(){
        Logs.e("showCurrentTabList : " + mCurrentTab);
        mLayoutDirectInput.setVisibility(View.GONE);
        switch (mCurrentTab){
            case TAB_RECENTLY:
                if(mRecentlyArrayList.size() > 0){
                    mListViewRecently.setVisibility(View.VISIBLE);
                    mLayoutEmptyRecently.setVisibility(View.GONE);
                }else{
                    mListViewRecently.setVisibility(View.GONE);
                    mLayoutEmptyRecently.setVisibility(View.VISIBLE);
                }
                mLayoutRecently.setVisibility(View.VISIBLE);
                mLayoutDirectInput.setVisibility(View.VISIBLE);
                break;
            case TAB_MYACCOUNT:
                mLayoutMyAccount.setVisibility(View.VISIBLE);
                mLayoutDirectInput.setVisibility(View.VISIBLE);
                break;
            case TAB_CONTACT:
                Logs.e("showCurrentTabList - mContactArrayList: " + mContactArrayList.size());
                if(mContactArrayList.size() > 0){
                    mListViewContact.setVisibility(View.VISIBLE);
                    mLayoutEmptycontact.setVisibility(View.GONE);
                }else{
                    mListViewContact.setVisibility(View.GONE);
                    mLayoutEmptycontact.setVisibility(View.VISIBLE);
                }
                mLayoutContact.setVisibility(View.VISIBLE);
                break;
        }
    }

    /**
     * 은행 직접 선택 다이얼로그
     */
    private void showBankStockAccountDialog(){
        MLog.d();

        String bankListStr = Prefer.getBankList(this);
        String stockListStr = Prefer.getStockList(this);

        ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
        ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);

        final SlidingExpandBankStockDialog.OnSelectBankStockListener bankStockListener = new SlidingExpandBankStockDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bank, String code) {
                showAccountInputDialog(code,bank,"");
            };
        };


        SlidingExpandBankStockDialog bankStockDialog = new SlidingExpandBankStockDialog(this, bankList, stockList, bankStockListener);
        bankStockDialog.show();
    }

    /**
     * 계좌번호 직접입력 다이얼로그
     *
     * @param bankCode
     * @param bankName
     */
    private String backUpBankCd,backUpBankNm;
    private void showAccountInputDialog(final String bankCode,final String bankName,String accountNo){
        MLog.d();

        backUpBankCd = bankCode;
        backUpBankNm = bankName;
        String newBankName = bankName;

        //메인 클립보드 이체에서 받은사람 은행코드만 물고들어오기에 여기서 은행 명을 찾아낸다.
        if(TextUtils.isEmpty(newBankName)){
            String bankListStr = Prefer.getBankList(this);
            String stockListStr = Prefer.getStockList(this);

            ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
            ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);

            ArrayList<RequestCodeInfo> totalList = new ArrayList<RequestCodeInfo>();
            if (bankList != null) {
                totalList.addAll(bankList);
            }

            if (stockList != null) {
                totalList.addAll(stockList);
            }

            for(int i=0;i<totalList.size();i++){
                RequestCodeInfo info = totalList.get(i);
                if(info.getSCCD().equalsIgnoreCase(bankCode)){
                    newBankName = info.getCD_NM();
                    break;
                }
            }
        }

        final SlidingAccountInputDialog.OnFinishAccountInputListener accountInputListener = new SlidingAccountInputDialog.OnFinishAccountInputListener() {
            @Override
            public void onClickBack() {
                showBankStockAccountDialog();
            }

            @Override
            public void onEditFinishListener(String bankCode, String bankName, String accountNum) {
                String msg = "bankCdoe : " + bankCode + " , bankName : " + bankName + " , accountNum " + accountNum;
                if(BuildConfig.DEBUG)
                    Logs.showToast(ITransferSelectReceiverActivity.this,msg);

                //여기서 수치조회 해야한다.
                checkRemitteeAccount(bankCode, "", accountNum, "",new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result, String ret) {
                        if(result){

                            if(ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 1){
                                Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendMultiActivity.class);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendSingleActivity.class);
                                startActivity(intent);
                            }
                            finishActivity(true);
                        }else{
                            showAccountInputDialog(backUpBankCd,backUpBankNm,"");
                        }
                    }
                });
            }
        };

        SlidingAccountInputDialog accountInputDialog = new SlidingAccountInputDialog(this, bankCode, newBankName,false,accountNo,accountInputListener);
        accountInputDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //물고들어온 값을 초기화 시켜줘야 반복적으로 출력되지 않음.
                mRcvBankCd="";
                mRcvAccountNo="";
            }
        });

        accountInputDialog.show();

    }

    //수취인을 확인한다.
    private void checkRemitteeAccount(String bankCode, String detailBankCd, String accountNo,String recvNm){
        TransferRequestUtils.requestRemitteeAccountBySingle(ITransferSelectReceiverActivity.this,
                bankCode,
                detailBankCd,
                accountNo,
                recvNm,
                isAddTransfer,
                new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result,String ret) {
                        if(result){

                            if(ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 1){
                                Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendMultiActivity.class);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendSingleActivity.class);
                                startActivity(intent);
                            }
                            finishActivity(true);
                        }
                    }
                });
    }

    private void checkRemitteeAccount(String bankCode, String detailBankCd, String accountNo,String recvNm,HttpSenderTask.HttpRequestListener2 listener2){
        TransferRequestUtils.requestRemitteeAccountBySingle(ITransferSelectReceiverActivity.this,
                bankCode,
                detailBankCd,
                accountNo,
                recvNm,
                isAddTransfer,
                listener2);
    }


    //다건 수취인을 확인한다.
    private void checkRemitteeGroup(String groupIdx,String favorite){
        TransferRequestUtils.requestRemitteeAccountByMulti(ITransferSelectReceiverActivity.this,
                groupIdx,
                favorite,
                false,
                new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result,String ret) {
                        if(result){

                            if(ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 1){
                                Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendMultiActivity.class);
                                startActivity(intent);
                            }else{
                                Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendSingleActivity.class);
                                startActivity(intent);
                            }
                            finishActivity(true);
                        }
                    }
                });
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logs.i("onRequestPermissionsResult");
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_SYNC_CONTACTS: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {


                    hideCurrentTabList();
                    mCurrentTab = TAB_CONTACT;
                    changeTabState();
                    showCurrentTabList();

                    showProgressDialog();
                    makeTabContactList(true);

                }else{
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.e("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_read_contacts), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(ITransferSelectReceiverActivity.this);
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                        //Utils.finishAffinity(AuthRegisterGuideActivity.this);
                                    }
                                });
                    }
                }
            }
            break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case REQUEST_CODE_SEARCH:
                if(resultCode == RESULT_OK){
                    if(ITransferDataMgr.getInstance().getRemitteInfoArraySize() > 1){
                        Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendMultiActivity.class);
                        startActivity(intent);
                    }else{
                        Intent intent = new Intent(ITransferSelectReceiverActivity.this, ITransferSendSingleActivity.class);
                        startActivity(intent);
                    }
                    finishActivity(true);
                }
                break;
        }
    }
}
