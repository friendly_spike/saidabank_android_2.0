package com.sbi.saidabank.activity.test;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.certification.CertListActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.solution.espider.EspiderData;
import com.sbi.saidabank.solution.espider.EspiderManager;

import org.json.JSONObject;

import java.util.Calendar;
import java.util.List;

/**
 * Saidabank_android
 * Class: ESpiderTestActivity
 * Created by 950469 on 2018. 9. 4..
 * <p>
 * Description:
 */
public class ESpiderTestActivity extends BaseActivity implements View.OnClickListener, EspiderManager.EspiderEventListener {
    private static final int REQUEST_CERT = 100;

    private EspiderManager mEspiderManager = null;

    private TextView mTextCertPath;
    private TextView mTextPw;
    private EditText mEditIdentifyNo;
    private CheckBox mCheckHealthPay;
    private CheckBox mCheckHealthQualification;
    private CheckBox mCheckNationalSubscribers;
    private CheckBox mCheckNationalJoin;
    private CheckBox mCheckNationalJoinHistory;
    private CheckBox mCheckRevenueRegister;
    private CheckBox mCheckRevenueTax;
    private CheckBox mCheckRevenueTaxIncome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_scraping);

        findViewById(R.id.espider_test_01).setOnClickListener(this);
        findViewById(R.id.espider_test_02).setOnClickListener(this);
        findViewById(R.id.espider_test_03).setOnClickListener(this);
        findViewById(R.id.espider_test_04).setOnClickListener(this);
        findViewById(R.id.espider_test_05).setOnClickListener(this);
        findViewById(R.id.espider_test_06).setOnClickListener(this);
        findViewById(R.id.espider_test_07).setOnClickListener(this);
        findViewById(R.id.espider_test_08).setOnClickListener(this);
        findViewById(R.id.espider_test_09).setOnClickListener(this);

        findViewById(R.id.espider_test_scraping).setOnClickListener(this);

        mTextCertPath = findViewById(R.id.edit_cert_path);
        mTextPw = findViewById(R.id.edit_pw);
        mEditIdentifyNo = findViewById(R.id.edit_identify_no);

        mCheckHealthPay = findViewById(R.id.checkbox_health_pay);
        mCheckHealthQualification = findViewById(R.id.checkbox_health_qualification);
        mCheckNationalSubscribers = findViewById(R.id.checkbox_national_subscribers);
        mCheckNationalJoin = findViewById(R.id.checkbox_national_join);
        mCheckNationalJoinHistory = findViewById(R.id.checkbox_national_join_history);
        mCheckRevenueRegister = findViewById(R.id.checkbox_revenue_register);
        mCheckRevenueTax = findViewById(R.id.checkbox_revenue_tax);
        mCheckRevenueTaxIncome = findViewById(R.id.checkbox_revenue_tax_income);

        initEspider();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EspiderManager.clearInstance();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.espider_test_01:
                showEspiderCert();
                break;

            case R.id.espider_test_02:
                initEspiderUX();
                checkHealthInsurancePay();
                break;

            case R.id.espider_test_03:
                initEspiderUX();
                checkHealthInsuranceQualification();
                break;

            case R.id.espider_test_04:
                initEspiderUX();
                checkNationalPensionSubscribers();
                break;

            case R.id.espider_test_05:
                initEspiderUX();
                checkNationalPensionJoin();
                break;

            case R.id.espider_test_06:
                initEspiderUX();
                checkNationalPensionJoinHistory();
                break;

            case R.id.espider_test_07:
                initEspiderUX();
                checkRevenueBusinessRegister();
                break;

            case R.id.espider_test_08:
                initEspiderUX();
                checkRevenueTax();
                break;

            case R.id.espider_test_09:
                initEspiderUX();
                checkRevenueTaxIncome();
                break;

            case R.id.espider_test_scraping:
                checkScraping();
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CERT:
                if (resultCode == RESULT_OK) {
                    String pass = data.getStringExtra(Const.INTENT_CERT_PASSWORD);
                    mTextPw.setText(pass);
                    mEspiderManager.setPassword(pass);

                    String path = data.getStringExtra(Const.INTENT_CERT_PATH);
                    mEspiderManager.setCertPath(path);
                    mTextCertPath.setText(path);
                }
                break;

            default:
                break;
        }
    }

    private void initEspider() {
        if (mEspiderManager == null) {
            mEspiderManager = EspiderManager.getInstance(this);
            mEspiderManager.setEspiderEventListener(this);
        }

    }

    private void showEspiderCert() {
        Intent intent = new Intent(this, CertListActivity.class);
        intent.putExtra(Const.INTENT_CERT_PURPOSE, Const.CertJobType.SCRAPING);
        startActivityForResult(intent, REQUEST_CERT);
    }

    private void initEspiderUX() {
        if (mTextPw.isFocused()) {
            mTextPw.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mTextPw.getWindowToken(), 0);
        }

        if (mEditIdentifyNo.isFocused()) {
            mEditIdentifyNo.clearFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mEditIdentifyNo.getWindowToken(), 0);
        }
    }


    private void checkScraping() {
        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(identifyNo)) {
            String title = getString(R.string.app_name);
            String msg = "주민번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);

        if (mCheckHealthPay.isChecked()) {
            String startDate = "201801";
            String endDate = "201808";

            boolean isValidateStartDate = EspiderManager.validateDateFormat(startDate);
            boolean isValidatEndDate = EspiderManager.validateDateFormat(endDate);
            if (!isValidateStartDate || !isValidatEndDate) {
                String title = getString(R.string.app_name);
                String msg = "날짜 형식이 잘못되었습니다.";
                DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
                return;
            }

            mEspiderManager.setHealthInsurancePay(startDate, endDate, false);
        }

        if (mCheckHealthQualification.isChecked()) {
            mEspiderManager.setHealthInsuranceQualification(false, false);
        }

        if (mCheckNationalSubscribers.isChecked()) {
            mEspiderManager.setNationalPensionSubscribers();
        }

        if (mCheckNationalJoin.isChecked()) {
            mEspiderManager.setNationalPensionJoin();
        }

        if (mCheckNationalJoinHistory.isChecked()) {
            mEspiderManager.setNationalPensionJoinHistory();
        }

        if (mCheckRevenueRegister.isChecked()) {
            mEspiderManager.setRevenueBusinessRegister("", false, false, true);
        }

        if (mCheckRevenueTax.isChecked()) {
            String startDate = "201801";
            String endDate = "201808";

            boolean isValidateStartDate = EspiderManager.validateDateFormat(startDate);
            boolean isValidatEndDate = EspiderManager.validateDateFormat(endDate);
            if (!isValidateStartDate || !isValidatEndDate) {
                String title = getString(R.string.app_name);
                String msg = "날짜 형식이 잘못되었습니다.";
                DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                });
                return;
            }

            mEspiderManager.setRevenueTax("", startDate, endDate, true, true, true);
        }

        if (mCheckRevenueTaxIncome.isChecked()) {
            String dete = "2017";
            mEspiderManager.setRevenueTaxIncome("", dete, true);
        }

        showProgressDialog();

        mEspiderManager.startEspider();
    }

    private void checkHealthInsurancePay() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

            return;
        }

        if (TextUtils.isEmpty(identifyNo)) {
            String title = getString(R.string.app_name);
            String msg = "주민번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);

        int yearThis = Calendar.getInstance().get(Calendar.YEAR);
        int yearLast = yearThis - 1;
        int yearBefore = yearThis - 2;

        for (int index = 0; index < 2; index++) {
            if (index == 0) {
                mEspiderManager.setHealthInsurancePay(yearLast + "01", yearLast + "12", false);
            } else if (index == 1) {
                mEspiderManager.setHealthInsurancePay(yearBefore + "01", yearBefore + "12", false);
            }
        }

        //mEspiderManager.setHealthInsurancePay(startDate, endDate, false);
        mEspiderManager.startEspider();
    }

    private void checkHealthInsuranceQualification() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(identifyNo)) {
            String title = getString(R.string.app_name);
            String msg = "주민번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setHealthInsuranceQualification(true, false);
        mEspiderManager.startEspider();
    }

    private void checkNationalPensionSubscribers() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(identifyNo)) {
            String title = getString(R.string.app_name);
            String msg = "주민번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setNationalPensionSubscribers();
        mEspiderManager.startEspider();
    }

    private void checkNationalPensionJoin() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(identifyNo)) {
            String title = getString(R.string.app_name);
            String msg = "주민번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setNationalPensionJoin();
        mEspiderManager.startEspider();
    }

    private void checkNationalPensionJoinHistory() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(identifyNo)) {
            String title = getString(R.string.app_name);
            String msg = "주민번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setNationalPensionJoinHistory();
        mEspiderManager.startEspider();
    }

    private void checkRevenueBusinessRegister() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setRevenueBusinessRegister("", true, true, true);
        mEspiderManager.startEspider();
    }

    private void checkRevenueTax() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        String startDate = "201801";
        String endDate = "201807";

        boolean isValidateStartDate = EspiderManager.validateDateFormat(startDate);
        boolean isValidatEndDate = EspiderManager.validateDateFormat(endDate);
        if (!isValidateStartDate || !isValidatEndDate) {
            String title = getString(R.string.app_name);
            String msg = "날짜 형식이 잘못되었습니다.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setRevenueTax("", startDate, endDate, true, true, true);
        mEspiderManager.startEspider();
    }

    private void checkRevenueTaxIncome() {
        if (mEspiderManager == null)
            return;

        String pw = mTextPw.getText().toString();
        String identifyNo = mEditIdentifyNo.getText().toString();

        if (TextUtils.isEmpty(mEspiderManager.getCertPath())) {
            String title = getString(R.string.app_name);
            String msg = "인증서를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        if (TextUtils.isEmpty(pw)) {
            String title = getString(R.string.app_name);
            String msg = "인증서 비밀번호를 입력해주세요.";
            DialogUtil.alert(this, title, msg, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });
            return;
        }

        String date = "2017";

        showProgressDialog();

        mEspiderManager.initList();
        mEspiderManager.setPassword(pw);
        mEspiderManager.setIdentifyNo(identifyNo);
        mEspiderManager.setRevenueTaxIncome("", date, true);
        mEspiderManager.startEspider();
    }

    @Override
    public void onFinishEspider(String javaCallBackName, JSONObject returnjsonobj) {
        Logs.li("----------- onFinishEspider return : " + returnjsonobj.toString());
    }

    @Override
    public void onStatusEspider(String javaCallBackName, int status, String msg) {
        dismissProgressDialog();
        if (status == 0) {
            if (EspiderManager.isEngineEnabled()) {
                List<EspiderData> list = mEspiderManager.getResultList();
            }
        }
    }
}
