package com.sbi.saidabank.activity.common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.IntroActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class SchemeEntryActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        MLog.d();
        super.onStart();

        Logs.e("SchemeEntryActivity");

        String urlStr = "";
        // Sheme 호출일 때 url parsing
        if (DataUtil.isNotNull(getIntent().getData())) {
            try {
                urlStr = URLDecoder.decode(getIntent().getData().toString(), Const.UTF_8);
                urlStr = HttpUtils.getMobileWebParam(urlStr);

                Logs.e("SchemeEntryActivity - urlStr : " + urlStr );
                //웹에서 아무런 값을 넣어주지 않고 앱을 호출했을때.
                if(urlStr.equals("{}")){
                    urlStr = "";
                }

                if(!TextUtils.isEmpty(urlStr)){
                    StringBuilder param = new StringBuilder();
                    JSONObject jsonObject = new JSONObject(urlStr);
                    if (jsonObject.has("url")) {
                        urlStr = jsonObject.optString("url");
                        for (int i = 0; i < jsonObject.length(); i++) {
                            if (i != 0)
                                param.append("&");
                            param.append(jsonObject.names().getString(i) + "=" + URLEncoder.encode(jsonObject.getString(jsonObject.names().getString(i)), Const.EUC_KR));
                        }
                        urlStr = urlStr + "?" + param.toString();
                    }
                }
            } catch (JSONException | UnsupportedEncodingException e) {
                MLog.e(e);
            }
        }

        Logs.e("SchemeEntryActivity - urlStr : " + urlStr );

        //카카오에서 일반 연결로 들어왓는데 페이지가 없는 경우 그냥 메인만 뜨도록 한다.
        if(urlStr.contains(getString(R.string.kakao_app_key)) && urlStr.contains("//kakaolink")){
            urlStr = "";
        }


        SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();

        Activity main2Activity = mApplicationClass.getOpenActivity("Main2Activity");

        if(main2Activity != null || LoginUserInfo.getInstance().isLogin()){
            if(!TextUtils.isEmpty(urlStr)){
                Intent intent = new Intent(SchemeEntryActivity.this, WebMainActivity.class);
                intent.putExtra(Const.INTENT_MAINWEB_URL, urlStr);
                startActivity(intent);
            }else{
                mApplicationClass.allActivityFinish(false);
            }
        }else{
            mApplicationClass.allActivityFinish(true);
            Intent intent = new Intent(SchemeEntryActivity.this, IntroActivity.class);
            if(!TextUtils.isEmpty(urlStr))
                intent.putExtra(Const.INTENT_MAINWEB_URL, urlStr);
            startActivity(intent);
        }
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
