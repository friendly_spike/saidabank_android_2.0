package com.sbi.saidabank.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.OnWebActionListener;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * WebTermsActivity : 웹뷰 약관동의 화면 (모질라 PDF라이브러리를 사용)
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class WebTermsActivity extends BaseWebActivity {

    private WebView mWebView;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        enableScreenCapture();
        setContentView(R.layout.activity_web_terms);

        if (DataUtil.isNull(getIntent())) {
            return;
        }

        String url = getIntent().getStringExtra("url");
        String title = getIntent().getStringExtra("title");

        TextView tvTitle = findViewById(R.id.tv_title);
        tvTitle.setText(DataUtil.isNotNull(title) ? title : Const.EMPTY);

        Button btnConfirm = findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_OK);
                finish();
            }
        });

        mWebView = (WebView) findViewById(R.id.webview);

        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowFileAccessFromFileURLs(true);
        settings.setAllowUniversalAccessFromFileURLs(true);

        FrameLayout webViewContainer = (FrameLayout) findViewById(R.id.webview_container);
        mWebView.setWebViewClient(new BaseWebClient(WebTermsActivity.this, this));
        mWebView.setWebChromeClient(new WebChromeClient());

        try {
            String pathStr = URLEncoder.encode(url, Const.UTF_8);
            mWebView.loadUrl("file:///android_asset/pdfjs/web/viewer.html?file=" + pathStr);
        } catch (UnsupportedEncodingException e) {
            MLog.e(e);
        }
    }

    @Override
    protected void onResume() {
        MLog.d();
        super.onResume();
    }

    @Override
    protected void onPause() {
        MLog.d();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        MLog.d();
        super.onDestroy();
        if (DataUtil.isNotNull(mWebView)) {
            mWebView.clearCache(true);
            mWebView.destroy();
        }
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    @Override
    public void showProgress() {
        showProgressDialog();
    }

    @Override
    public void dismissProgress() {
        dismissProgressDialog();
    }

}
