package com.sbi.saidabank.activity.transaction.safedeal;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.TransferSafeDealActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

/**
 * 안전거래 사유 슬라이드 화면
 */
public class SlideDealReasonLayout extends SlideBaseLayout implements View.OnClickListener{


    private RelativeLayout mLayoutMoney;
    private RelativeLayout mLayoutProperty;

    //금전거래,물품거래 화면 같음
    private EditText mEtReason;
    private TextView mTvStrLen;

    //부동산거래
    private LinearLayout   mLayoutPropertyInputDirect;
    private ImageView      mIvInputDirect;
    private RelativeLayout mLayoutPropertyReasonWhole;  //부동산거래 사유 전체 레이아웃
    private RelativeLayout mLayoutPropertySelectReason;
    private RelativeLayout mLayoutPropertyEditText;
    private TextView       mTvSelectReason;
    private EditText       mEtPropertyReason;

    private RelativeLayout mLayoutAddress;
    private TextView       mTvAddress;

    private Button mBtnOk;
    private LinearLayout mLayoutBtnTransfer;

    private int mSafeDealKind;
    private String mPropertyKind;
    private String mPropertyKindName;
    private String mAddress;
    public SlideDealReasonLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideDealReasonLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @Override
    public void clearData(){
        mEtReason.setText("");

        mIvInputDirect.setImageResource(R.drawable.btn_checkout_rs);
        mLayoutPropertySelectReason.setVisibility(VISIBLE);
        mLayoutPropertyEditText.setVisibility(INVISIBLE);

        mTvSelectReason.setText("거래사유선택");
        mEtPropertyReason.setText("");
        mTvAddress.setTextColor(Color.parseColor("#888888"));
        mTvAddress.setText("부동산주소 입력");
    }

    private void initUX(Context context){
        setTag("SlideDealReason");

        View layout = View.inflate(context, R.layout.layout_safedeal_slide_deal_reason, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);

        mLayoutMoney = layout.findViewById(R.id.layout_money);
        mLayoutProperty = layout.findViewById(R.id.layout_property);
        //mLayoutGoods = layout.findViewById(R.id.layout_goods);

        //금전거래 사유
        RelativeLayout editTextLayout = layout.findViewById(R.id.layout_edittext);
        int radius_et = (int) Utils.dpToPixel(getContext(),7f);
        editTextLayout.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF5F5F5),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5)));

        mEtReason = layout.findViewById(R.id.et_reason);
        mEtReason.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mEtReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Logs.e("afterTextChanged : " + editable.length());
                //mTvStrLen.setText(editable.length()+"");
                if (editable.length() > 0) {
                    //mTvStrLen.setVisibility(VISIBLE);
                    mBtnOk.setVisibility(GONE);
                    mLayoutBtnTransfer.setVisibility(VISIBLE);
                } else{
                    //mTvStrLen.setVisibility(GONE);
                    mBtnOk.setVisibility(VISIBLE);
                    mLayoutBtnTransfer.setVisibility(GONE);
                }
            }
        });

        mEtReason.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Logs.e("onEditorAction : " + actionId);
                //if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Utils.hideKeyboard(getContext(), mEtReason);
                    mEtReason.clearFocus();
                //}
                return true;
            }
        });


        //거래내역에 포커스가 들어오거나 사라질때 화면을 위로 올리거나 내린다.
        //이광호 과장님요청. 입력필드를 가리지 말아달라...
        mEtReason.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v,final boolean hasFocus) {
                doEditViewFocus(hasFocus);
            }
        });

        mTvStrLen = layout.findViewById(R.id.tv_strlen);


        //부동산 거래 사유
        mLayoutPropertyInputDirect = layout.findViewById(R.id.layout_property_input_direct);
        mLayoutPropertyInputDirect.setOnClickListener(this);
        mIvInputDirect = layout.findViewById(R.id.iv_input_direct);


        //부동산거래 사유 선택일 경우
        mLayoutPropertyReasonWhole = layout.findViewById(R.id.layout_property_reason_whole);
        mLayoutPropertyReasonWhole.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF5F5F5),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5)));

        mLayoutPropertySelectReason = layout.findViewById(R.id.layout_property_select_reason);
        layout.findViewById(R.id.btn_property_select_reason).setOnClickListener(this);
        mTvSelectReason     = layout.findViewById(R.id.tv_select_reason);

        //부동산 거래 직접입력일 경우
        mLayoutPropertyEditText  = layout.findViewById(R.id.layout_property_edittext);
        mEtPropertyReason = layout.findViewById(R.id.et_property_reason);
        mEtPropertyReason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Logs.e("afterTextChanged : " + editable.length());
                //mTvStrLen.setText(editable.length()+"");
                checkPropertyInputField();
            }
        });
        mEtPropertyReason.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Logs.e("onEditorAction : " + actionId);
                //if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utils.hideKeyboard(getContext(), mEtPropertyReason);
                mEtPropertyReason.clearFocus();
                //}
                return true;
            }
        });

        mEtPropertyReason.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v,final boolean hasFocus) {
                doEditViewFocus(hasFocus);
            }
        });

        //부동산 거래 주소록
        mLayoutAddress = layout.findViewById(R.id.layout_address);
        mLayoutAddress.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF5F5F5),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5)));
        layout.findViewById(R.id.btn_address).setOnClickListener(this);
        mTvAddress     = layout.findViewById(R.id.tv_address);


        mBtnOk = layout.findViewById(R.id.btn_ok);

        mLayoutBtnTransfer = layout.findViewById(R.id.layout_btn_transfer);
        mLayoutBtnTransfer.setOnClickListener(this);

        initLayoutSetBottom(true,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_property_input_direct:
                if(mLayoutPropertySelectReason.getVisibility() == VISIBLE){
                    mIvInputDirect.setImageResource(R.drawable.btn_checkin_rs);
                    startAnimateFadeInOut(mLayoutPropertySelectReason,false,250,0,null);
                    startAnimateFadeInOut(mLayoutPropertyEditText,true,250,0,new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            mLayoutPropertyEditText.setVisibility(VISIBLE);
                            mEtPropertyReason.requestFocus();
                            Utils.showKeyboard(getContext(), mEtPropertyReason);
                            checkPropertyInputField();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                }else{
                    mIvInputDirect.setImageResource(R.drawable.btn_checkout_rs);
                    mEtPropertyReason.clearFocus();
                    Utils.hideKeyboard(getContext(), mEtPropertyReason);
                    startAnimateFadeInOut(mLayoutPropertyEditText,false,250,0,null);
                    startAnimateFadeInOut(mLayoutPropertySelectReason,true,250,250,new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            checkPropertyInputField();
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });

                }
                break;
            case R.id.btn_property_select_reason:
                View view = getSlideView(SlideListPropertyReasonLayout.class);
                if(view != null){
                    ((SlideListPropertyReasonLayout)view).slidingUp(250,0);
                }
                break;
            case R.id.btn_address:
                mActionlistener.onSearchAddress();
                break;
            case R.id.layout_btn_transfer:
                mEtReason.clearFocus();
                switch (mSafeDealKind) {
                    case Const.SAFEDEAL_TYPE_MOENY:
                    case Const.SAFEDEAL_TYPE_GOODS: {

                        String strReason = mEtReason.getText().toString();
                        if (TextUtils.isEmpty(strReason)) {
                            if (mSafeDealKind == Const.SAFEDEAL_TYPE_MOENY)
                                DialogUtil.alert(getContext(), "거래사유를 입력해주세요.");
                            else
                                DialogUtil.alert(getContext(), "거래물품을 입력해주세요.");
                            return;
                        }
                        TransferSafeDealDataMgr.getInstance().setSAFE_TRN_RSCD(Const.SAFEDEAL_REASON_WRITE_DIRECT);
                        TransferSafeDealDataMgr.getInstance().setSAFE_TRN_RSN_CNTN(strReason);
                        break;
                    }
                    case Const.SAFEDEAL_TYPE_PROPERTY: {
                        if (mLayoutPropertySelectReason.getVisibility() == VISIBLE) {
                            TransferSafeDealDataMgr.getInstance().setSAFE_TRN_RSCD(mPropertyKind);
                            TransferSafeDealDataMgr.getInstance().setSAFE_TRN_RSN_CNTN(mTvSelectReason.getText().toString());
                        } else {

                            String strReason = mEtPropertyReason.getText().toString();
                            if (TextUtils.isEmpty(strReason)) {
                                DialogUtil.alert(getContext(), "거래사유를 입력해주세요.");
                                return;
                            }
                            TransferSafeDealDataMgr.getInstance().setSAFE_TRN_RSCD(Const.SAFEDEAL_REASON_WRITE_DIRECT);
                            TransferSafeDealDataMgr.getInstance().setSAFE_TRN_RSN_CNTN(mEtPropertyReason.getText().toString());
                        }
                    }
                }

                mActionlistener.onActionNext(SlideDealReasonLayout.this,null);
                break;
        }
    }

    @Override
    public void slidingDown(int aniTime, int delayTime) {
        mEtReason.clearFocus();
        Utils.hideKeyboard(getContext(), mEtReason);
        super.slidingDown(aniTime, delayTime);
    }

    public void setSafeDealKind(int kind){
        mLayoutMoney.setVisibility(GONE);
        mLayoutProperty.setVisibility(GONE);
        mSafeDealKind = kind;

        switch (kind) {
            case Const.SAFEDEAL_TYPE_MOENY:
                mLayoutMoney.setVisibility(VISIBLE);
                mLayoutPropertyInputDirect.setVisibility(GONE);
                break;
            case Const.SAFEDEAL_TYPE_PROPERTY:
                mLayoutProperty.setVisibility(VISIBLE);
                mLayoutPropertyInputDirect.setVisibility(VISIBLE);
                break;
            case Const.SAFEDEAL_TYPE_GOODS:
                mLayoutMoney.setVisibility(VISIBLE);
                mEtReason.setHint("거래물품 입력");
                mLayoutPropertyInputDirect.setVisibility(GONE);
                break;

        }
    }

    public void setPropertyReason(String code,String reason){

        if(code.equals("99")){
            mIvInputDirect.setImageResource(R.drawable.btn_checkin_rs);
            startAnimateFadeInOut(mLayoutPropertySelectReason,false,250,0,null);
            startAnimateFadeInOut(mLayoutPropertyEditText,true,250,0,new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    mLayoutPropertyEditText.setVisibility(VISIBLE);
                    mEtPropertyReason.requestFocus();
                    Utils.showKeyboard(getContext(), mEtPropertyReason);
                    checkPropertyInputField();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }else{
            mPropertyKind = code;
            mPropertyKindName = reason;
            mTvSelectReason.setText(reason);

            checkPropertyInputField();
        }
    }

    private boolean checkPropertyInputField(){
        if(mLayoutPropertySelectReason.getVisibility() == VISIBLE){
            if(TextUtils.isEmpty(mPropertyKind) || TextUtils.isEmpty(mPropertyKindName)){
                mBtnOk.setVisibility(VISIBLE);
                mLayoutBtnTransfer.setVisibility(GONE);
                return false;
            }
        }else{
            if(TextUtils.isEmpty(mEtPropertyReason.getText().toString())){
                mBtnOk.setVisibility(VISIBLE);
                mLayoutBtnTransfer.setVisibility(GONE);
                return false;
            }
        }

        if(TextUtils.isEmpty(mAddress)){
            mBtnOk.setVisibility(VISIBLE);
            mLayoutBtnTransfer.setVisibility(GONE);
            return false;
        }

        mBtnOk.setVisibility(GONE);
        mLayoutBtnTransfer.setVisibility(VISIBLE);
        return true;
    }

    public void setPropertyAddress(String address){
        mAddress = address;
        mTvAddress.setTextColor(Color.parseColor("#000000"));
        mTvAddress.setText(address);
        mTvAddress.setTextSize(13);

        checkPropertyInputField();
    }

    public void hideKeypad(){
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mEtReason.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mEtPropertyReason.getWindowToken(), 0);
        mEtReason.clearFocus();
        mEtPropertyReason.clearFocus();
    }

    private void doEditViewFocus(final boolean hasFocus){
        if(hasFocus){
            ((TransferSafeDealActivity)getContext()).moveUpRootView(true);
            int radius = (int)Utils.dpToPixel(getContext(),6);
            mLayoutBtnTransfer.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.color293542),new int[]{radius,radius,radius,radius}));
        }else{
            ((TransferSafeDealActivity)getContext()).moveUpRootView(false);
            mLayoutBtnTransfer.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.color293542),new int[]{0,0,0,0}));
        }

        //이체버튼모양을 변경시켜준다.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mLayoutBtnTransfer.getLayoutParams();
                if(hasFocus){
                    int pixel = (int)Utils.dpToPixel(getContext(),35);
                    params.rightMargin = pixel;
                    params.leftMargin = pixel;



                }else{
                    params.rightMargin = 0;
                    params.leftMargin = 0;

                }

                mLayoutBtnTransfer.setLayoutParams(params);
            }
        },200);
    }


}
