package com.sbi.saidabank.activity.transaction.safedeal;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mangosteen.falldownnumbereditor.FallDownNumberEditor;
import com.sbi.saidabank.R;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * 이체금액 입력 슬라이드 화면
 */
public class SlideInputMoneyLayout extends SlideBaseLayout implements View.OnClickListener,CustomNumPadLayout.OnNumPadClickListener{



    private RelativeLayout mLayoutScaleTitle;
    private TextView mScaleTitleFirst;
    private TextView mScaleTitleSecond;

    private TextView mTvHangleMoney;
    private TextView mTvNotoce;
    private LinearLayout mLayoutError;
    private TextView mTvError;
    private FallDownNumberEditor mFallDownNumberEditor;
    private Button  mOKBtn;

    private Double mBalanceAmount = 0.0;                // 계좌 잔액
    private Double mTransferOneTime = 0.0;              // 1회 이체한도
    private Double mTransferOneDay = 0.0;               // 일일 이체한도

    private int mSafeDealKind;

    public SlideInputMoneyLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideInputMoneyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @Override
    public void clearData(){
        onNumPadClick("cancel");
    }

    private void initUX(Context context){
        setTag("SlideInputMoneyLayout");

        View layout = View.inflate(context, R.layout.layout_safedeal_slide_input_money, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);

        mLayoutScaleTitle = layout.findViewById(R.id.layout_scale_title);
        mLayoutScaleTitle.setOnClickListener(this);
        mScaleTitleFirst = layout.findViewById(R.id.tv_scale_title_first);
        mScaleTitleSecond = layout.findViewById(R.id.tv_scale_title_second);

        mTvHangleMoney = findViewById(R.id.tv_hangle_money);
        mTvHangleMoney.setVisibility(GONE);

        mTvNotoce = findViewById(R.id.tv_notice);
        mLayoutError = findViewById(R.id.layout_error);
        mTvError = findViewById(R.id.tv_error);

        mFallDownNumberEditor = findViewById(R.id.falldowneditor);
        int size = (int)Utils.dpToPixel(getContext(),30f);
        mFallDownNumberEditor.setNumberTextSize(size);
        mFallDownNumberEditor.setNumberTextColor("#000000");

        mOKBtn = findViewById(R.id.btn_ok);
        mOKBtn.setOnClickListener(this);

        CustomNumPadLayout numPadLayout = layout.findViewById(R.id.numpad);
        numPadLayout.setNumPadClickListener(this);

        initLayoutSetBottom(true,false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_scale_title: {
                //나의 계좌의 스케일 타이틀의 형태를 변경
                View view = getSlideView(SlideMyAccountLayout.class);
                if (view != null) {
                    ((SlideMyAccountLayout) view).setDefaultScaleTitle(true);
                }

                //거래사유 내린다.
                view = getSlideView(SlideDealReasonLayout.class);
                if (view != null) {
                    ((SlideDealReasonLayout) view).slidingDown(250, 0);
                    scaleOrigin(500, 0);
                }
                break;
            }
            case R.id.btn_ok: {
                String inputMoney = mFallDownNumberEditor.getMoney();
                requestVerifyTransferAccountReceiver(inputMoney);

                break;
            }
        }
    }

    @Override
    public void onNumPadClick(String numStr) {
        switch (numStr) {
            case "cancel": {
                mFallDownNumberEditor.removeAllView();
                mTvHangleMoney.setVisibility(GONE);
                displayLimitAmount();
                mTvNotoce.setVisibility(VISIBLE);
                mLayoutError.setVisibility(GONE);
                mOKBtn.setEnabled(false);
                break;
            }
            case "del": {
                mFallDownNumberEditor.delNumber();
                String inputMoney = mFallDownNumberEditor.getMoney();
                if(Double.parseDouble(inputMoney) < 10000){
                    mTvHangleMoney.setVisibility(GONE);
                }else{
                    String hangul = Utils.convertHangul(inputMoney);
                    mTvHangleMoney.setText(hangul+"원");
                }

                if(inputMoney.equals("0")){
                    mOKBtn.setEnabled(false);
                    return;
                }

                if(checkVaildAmount(inputMoney)){
                    mTvNotoce.setVisibility(VISIBLE);
                    mLayoutError.setVisibility(GONE);
                    mOKBtn.setEnabled(true);
                }

                //부동산거래일 경우만.
                if(mSafeDealKind == Const.SAFEDEAL_TYPE_PROPERTY){
                    //if(Double.parseDouble(inputMoney) < Const.SAFE_DEAL_PROPERTY_NOCONTACT_AMOUNT){
                    if (Double.parseDouble(inputMoney) > TransferSafeDealDataMgr.getInstance().getLIMIT_ONE_TIME()) {
                        mTvNotoce.setText("안전한 부동산거래를 위한 인증을 진행합니다.\n신분증을 꼭 준비해주세요");
                        mTvNotoce.setTextColor(getContext().getResources().getColor(R.color.color009BEB));
                    }else{
                        displayLimitAmount();
                    }
                    mLayoutError.setVisibility(GONE);
                    mTvNotoce.setVisibility(VISIBLE);
                }

                break;
            }
            default: {
                String inputMoney = mFallDownNumberEditor.getMoney();
                //천억자리까지만 입력 가능하도록...
                if(inputMoney.length() >= 12) return;


                if(!checkVaildAmount(inputMoney)){
                    mTvNotoce.setVisibility(GONE);
                    mLayoutError.setVisibility(VISIBLE);
                    AniUtils.shakeView(mFallDownNumberEditor, 20f);
                    return;
                }

                mFallDownNumberEditor.setNumber(numStr);

                inputMoney = mFallDownNumberEditor.getMoney();
                if(Double.parseDouble(inputMoney) >= 10000){
                    String hangul = Utils.convertHangul(inputMoney);
                    mTvHangleMoney.setText(hangul+"원");
                    if(mTvHangleMoney.getVisibility() != VISIBLE){
                        mTvHangleMoney.setVisibility(VISIBLE);
                    }
                }

                if(!checkVaildAmount(inputMoney)){
                    mTvNotoce.setVisibility(GONE);
                    mLayoutError.setVisibility(VISIBLE);
                    AniUtils.shakeView(mFallDownNumberEditor, 20f);
                    mOKBtn.setEnabled(false);
                    return;
                }else{
                    mOKBtn.setEnabled(true);
                }

                //부동산거래일 경우만.
                if(mSafeDealKind == Const.SAFEDEAL_TYPE_PROPERTY){
                    //if(Double.parseDouble(inputMoney) >= Const.SAFE_DEAL_PROPERTY_NOCONTACT_AMOUNT){
                    if (Double.parseDouble(inputMoney) > TransferSafeDealDataMgr.getInstance().getLIMIT_ONE_TIME()) {
                        mTvNotoce.setText("안전한 부동산거래를 위한 인증을 진행합니다.\n신분증을 꼭 준비해주세요");
                        mTvNotoce.setTextColor(getContext().getResources().getColor(R.color.color009BEB));

                        mLayoutError.setVisibility(GONE);
                        mTvNotoce.setVisibility(VISIBLE);
                    }else{
                        displayLimitAmount();

                        mLayoutError.setVisibility(GONE);
                        mTvNotoce.setVisibility(VISIBLE);
                    }
                }
                break;
            }
        }
    }

    public void setOneTimeOneDayLimit(Double oneTimeLimit,Double oneDayLimit){
        Logs.e("setOneTimeOneDayLimit - oneTimeLimit : " + oneTimeLimit);
        Logs.e("setOneTimeOneDayLimit - oneDayLimit : " + oneDayLimit);

        //나중에 체크할 일이 있을 수 있으므로 제한 값을 가지고 있는다.
        TransferSafeDealDataMgr.getInstance().setLIMIT_ONE_TIME(oneTimeLimit);
        TransferSafeDealDataMgr.getInstance().setLIMIT_ONE_DAY(oneDayLimit);

        mTransferOneTime = oneTimeLimit;              // 1회 이체한도
        mTransferOneDay = oneDayLimit;

        displayLimitAmount();

    }

    public void setBalanceAmount(Double balanceAmount){
        Logs.e("setBalanceAmount - balanceAmount : " + balanceAmount);
        mBalanceAmount = balanceAmount;                // 계좌 잔액

        mTvNotoce.setVisibility(VISIBLE);
        mLayoutError.setVisibility(GONE);
    }


    private void displayLimitAmount(){


        String dispLimit = "1회: " + Utils.moneyFormatToWon(mTransferOneTime) + getContext().getString(R.string.won) + " / 1일: " + Utils.moneyFormatToWon(mTransferOneDay) + getContext().getString(R.string.won);
        if(mSafeDealKind == Const.SAFEDEAL_TYPE_PROPERTY){
//            View view = getSlideView(SlideMyAccountLayout.class);
//            MyAccountInfo accountInfo = ((SlideMyAccountLayout)view).getCurrentAccountInfo();
//            //한도계좌일 경우 아래와 같이 출력한다.
//            if(!accountInfo.getLMIT_LMT_ACCO_YN().equals("Y")){
            if(!TransferSafeDealDataMgr.getInstance().getLMIT_LMT_ACCO_YN().equals("Y")){
                dispLimit = "1회 / 1일 : " + Utils.moneyFormatToWon(mTransferOneDay) + getContext().getString(R.string.won);
            }
        }

        mTvNotoce.setText(dispLimit);
        mTvNotoce.setTextColor(getContext().getResources().getColor(R.color.black));
    }

    private boolean checkVaildAmount(String inputAmount) {
        Logs.e("setOneTimeOneDayLimit - oneTimeLimit : " + mTransferOneTime);
        Logs.e("setOneTimeOneDayLimit - oneDayLimit : " + mTransferOneDay);
        Logs.e("setOneTimeOneDayLimit - balanceAmount : " + mBalanceAmount);

        if (mBalanceAmount <= 0) {
            mTvError.setText(getContext().getString(R.string.msg_err_trnasfer_over_balance));
            return false;
        }

        inputAmount = inputAmount.replaceAll("[^0-9]", Const.EMPTY);
        double dInputAmount = Double.valueOf(inputAmount);
        //잔액체크
        if (dInputAmount > mBalanceAmount) {
            mTvError.setText(getContext().getString(R.string.msg_err_trnasfer_over_balance));
            return false;
        }

        //일회이체한도 체크
        if(mSafeDealKind != Const.SAFEDEAL_TYPE_PROPERTY){
            if (dInputAmount > mTransferOneTime) {
                mTvError.setText(getContext().getString(R.string.msg_err_trnasfer_over_one_time));
                return false;
            }
        }

        //일일 이체한도 체크
        if (dInputAmount > mTransferOneDay) {
            mTvError.setText(getContext().getString(R.string.msg_err_trnasfer_over_one_day));
            return false;
        }


        return true;
    }

    @Override
    public void scaleSmall(final int aniTime, int delayTime) {
        //if(isScale) return;

        mLayoutScaleTitle.setAlpha(0f);
        mLayoutScaleTitle.setVisibility(VISIBLE);
        mLayoutScaleTitle.animate()
                .alpha(1f)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }
                } )
                .start();

        super.scaleSmall(aniTime, delayTime);

        //거래사화면 올린다.
        View view = getSlideView(SlideDealReasonLayout.class);
        if(view != null){
            ((SlideDealReasonLayout)view).slidingUp(250,aniTime - 300);
        }


        //계좌 리스트가 이미 보이지 않으면 리턴한다.
        if(mLayoutBody.getVisibility() != VISIBLE) return;

        //계좌 리스트는 사라지는 에니메이션.
        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_out_transfer_view);
        ani.setDuration(aniTime);
        mLayoutBody.clearAnimation();
        mLayoutBody.startAnimation(ani);
        mLayoutBody.getAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                mLayoutBody.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //mLayoutMoney.invalidate();
                //displayComma();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    @Override
    public void scaleOrigin(int aniTime, int delayTime) {
        //if(!isScale) return;

        mLayoutScaleTitle.animate()
                .alpha(0f)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutScaleTitle.setVisibility(INVISIBLE);
                        mLayoutScaleTitle.setAlpha(1f);
                    }
                } )
                .start();

        //계좌 리스트는 사라지는 에니메이션.
        mLayoutBody.setAlpha(1f);
        mLayoutBody.setVisibility(VISIBLE);
        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_in_transfer_view);
        ani.setDuration(aniTime);
        mLayoutBody.clearAnimation();
        mLayoutBody.startAnimation(ani);

        super.scaleOrigin(aniTime, delayTime);
    }

    public void setSafeDealKind(int kind){
        mSafeDealKind = kind;
    }

    /**
     * 두번째 수취인 조회
     *
     */
    public void requestVerifyTransferAccountReceiver(final String inputMoney) {

        Map param = new HashMap();
        param.put("TRTM_DVCD", "1");
        param.put("TRNF_KNCD", "4");
        param.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());
        param.put("CNTP_FIN_INST_CD",  TransferSafeDealDataMgr.getInstance().getMNRC_BANK_CD());
        param.put("CNTP_BANK_ACNO",  TransferSafeDealDataMgr.getInstance().getMNRC_ACNO());
        param.put("TRN_AMT", inputMoney);
        param.put("MNRC_CPNO", "");

        mActionlistener.showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                mActionlistener.dismissProgressDialog();
                Logs.i("TRA0010400A01 : " + ret);

                if(mActionlistener.onCheckHttpError(ret,false)) return;


                try {
                    final JSONObject object = new JSONObject(ret);

                    //안심이체정보에 이체금액을 넣어둔다.
                    TransferSafeDealDataMgr.getInstance().setTRAM(inputMoney);
                    //안심이체정보에 수수료를 넣어둔다.
                    TransferSafeDealDataMgr.getInstance().setTRNF_FEE(object.optString("FEE"));

                    String amount = Utils.moneyFormatToWon(Double.valueOf(inputMoney));
                    mScaleTitleSecond.setText(amount + "원");

                    scaleSmall(500, 0);

                    //나의 계좌의 스케일 타이틀의 형태를 변경
                    View view = getSlideView(SlideMyAccountLayout.class);
                    if (view != null) {
                        ((SlideMyAccountLayout) view).changeScaleTitle();
                    }

                    //거래사유 올린다.
                    view = getSlideView(SlideDealReasonLayout.class);
                    if (view != null) {
                        ((SlideDealReasonLayout) view).slidingUp(250, 150);
                    }

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });

    }
}
