package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Saidabanking_android
 *
 * Class: OtpPwAuthActivity
 * Created by 950546 on 2019. 02. 19..
 * <p>
 * Description: 타기관OTP 등록을 위한 화면
 */

public class OtherOtpRegActivity extends BaseActivity implements View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener {
	private static int  mKeypadType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;

	/**
	 * 등록 단계
	 */
	private enum REG_STEP {
		STEP_01,
		STEP_02
	}


	private LinearLayout  mLayoutMsg;
	private TextView      mTextDesc;
	private TextView      mTextMsg;
	private EditText      mExitOtpPw;
	private EditText      mEditOtpPw01;
	private ImageView     mImageOtpPw01;
	private EditText      mEditOtpPw02;
	private ImageView     mImageOtpPw02;
	private EditText      mEditOtpPw03;
	private ImageView     mImageOtpPw03;
	private EditText      mEditOtpPw04;
	private ImageView     mImageOtpPw04;
	private EditText      mEditOtpPw05;
	private ImageView     mImageOtpPw05;
	private EditText      mEditOtpPw06;
	private ImageView     mImageOtpPw06;

	private int[]         mArrayNumber = new int[10];
	private boolean       mIsViewCtrlKeypad = false;

	private CommonUserInfo mCommonUserInfo;

	private TransKeyCtrl  mTKMgr;

	private String        mFirstPin = null;
	private REG_STEP mRegStep = REG_STEP.STEP_01;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_other_otp_pw_auth);

		getExtra();
        initUX();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    	if (mIsViewCtrlKeypad == false) {
	    		showKeyPad();
				return true;
		    }
    	}
		return false;
	}

	@Override
	public void cancel(Intent data) {
		backRegStep();
	}

	@Override
	public void done(Intent data) {
		mIsViewCtrlKeypad = false;
		if (data == null)
			return;

		String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);
		String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
		String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
		byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
		int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

		if (iRealDataLength != mCommonUserInfo.getOtpPWLength()) {
			String msg = getString(R.string.message_pin_right_length, mCommonUserInfo.getOtpPWLength());
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(msg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		String plainData = "";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(secureKey);

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "";
			}

			if (TextUtils.isEmpty(plainData)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_decode_pin);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}

        } catch (Exception e) {
			Logs.printException(e);
		}

		if (mRegStep == REG_STEP.STEP_01) {
			if (Utils.isDuplicate(plainData, Const.OTP_PW_NUM_DUPLICATE)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_reg_pin_duplicate);
				showKeyPad();
				return;
			}
			if (Utils.isContinueNum(plainData, 3)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_reg_pin_continue_num);
				showKeyPad();
				return;
			}

			String desc = getString(R.string.msg_retry_input_otp_pw);
			mTextDesc.setText(desc);
			mFirstPin = plainData;
			mRegStep = REG_STEP.STEP_02;
			showKeyPad();
		} else {
			if (!isEqualsPin(mFirstPin, plainData)) {
				if(mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
					mTextMsg.setText(R.string.msg_no_match_reg_pin);
				} else {
					mTextMsg.setText(mCommonUserInfo.getOtpTitle() + "가 일치하지 않습니다.");
				}
				mLayoutMsg.setVisibility(View.VISIBLE);

				Handler delayHandler = new Handler();
				delayHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						backRegStep();
						mLayoutMsg.setVisibility(View.INVISIBLE);
					}
				}, 500);
				return;
			}

			if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
					//requestAuthOtp(plainData);
			} else {
				//입력된 비밀번호를 자바스크립트 콜백으로 리턴
				Intent intent = new Intent();
				mCommonUserInfo.setSecureData(secureData);
				mCommonUserInfo.setPlainData(plainData);
				mCommonUserInfo.setDummyData(dummyData);
				mCommonUserInfo.setCipherData(cipherData);
				mCommonUserInfo.setOtpForgetAuthNo(false);
				intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
				setResult(RESULT_OK, intent);
				finish();
				overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
			}
		}
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}
	
	@Override
	public void input(int arg0) {
		if (arg0 == ITransKeyActionListenerEx.INPUT_CHARACTER_KEY) {
			mLayoutMsg.setVisibility(View.INVISIBLE);

			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();

            if (TextUtils.isEmpty(strValue1)) {
				mImageOtpPw01.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(1))]);
				mEditOtpPw01.setText("0");
				mEditOtpPw02.requestFocus();
			} else if (TextUtils.isEmpty(strValue2)) {
				mImageOtpPw02.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(2))]);
				mEditOtpPw02.setText("0");
				mEditOtpPw03.requestFocus();
			} else if (TextUtils.isEmpty(strValue3)) {
				mImageOtpPw03.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(3))]);
				mEditOtpPw03.setText("0");
				mEditOtpPw04.requestFocus();
			} else if (TextUtils.isEmpty(strValue4)) {
				mImageOtpPw04.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(4))]);
				mEditOtpPw04.setText("0");
				if (mCommonUserInfo.getOtpPWLength() == 5)
					mEditOtpPw05.requestFocus();
			} else if (TextUtils.isEmpty(strValue5)) {
				mImageOtpPw05.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(5))]);
				mEditOtpPw05.setText("0");
				if (mCommonUserInfo.getOtpPWLength() == 6)
					mEditOtpPw06.requestFocus();
			} else {
				mImageOtpPw06.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(6))]);
				mEditOtpPw06.setText("0");
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_BACKSPACE_KEY) {
			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();
			String strValue6 = mEditOtpPw06.getText().toString();

			if (mCommonUserInfo.getOtpPWLength() == 6) {
				if (!TextUtils.isEmpty(strValue6)) {
					mEditOtpPw06.setText("");
					mImageOtpPw06.setImageResource(R.drawable.ico_otp_num_off);
					mEditOtpPw05.requestFocus();
					return;
				} else {
					if (!TextUtils.isEmpty(strValue5)) {
						mEditOtpPw05.setText("");
						mImageOtpPw05.setImageResource(R.drawable.ico_otp_num_off);
						mEditOtpPw04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getOtpPWLength() == 5) {
				if (!TextUtils.isEmpty(strValue5)) {
					mEditOtpPw05.setText("");
					mImageOtpPw05.setImageResource(R.drawable.ico_otp_num_off);
					mEditOtpPw04.requestFocus();
					return;
				}
			}

			if (!TextUtils.isEmpty(strValue4)) {
				mEditOtpPw04.setText("");
				mImageOtpPw04.setImageResource(R.drawable.ico_otp_num_off);
				mEditOtpPw03.requestFocus();
			} else if (!TextUtils.isEmpty(strValue3)) {
				mEditOtpPw03.setText("");
				mImageOtpPw03.setImageResource(R.drawable.ico_otp_num_off);
				mEditOtpPw02.requestFocus();
			} else if (!TextUtils.isEmpty(strValue2)) {
				mEditOtpPw02.setText("");
				mImageOtpPw02.setImageResource(R.drawable.ico_otp_num_off);
				mEditOtpPw01.requestFocus();
			} else if (!TextUtils.isEmpty(strValue1)) {
				mEditOtpPw01.setText("");
				mImageOtpPw01.setImageResource(R.drawable.ico_otp_num_off);
			}
		}
		else if (arg0 == ITransKeyActionListenerEx.INPUT_CLEARALL_BUTTON) {
			clearInput();
		}
	}

	@Override
	public void minTextSizeCallback() {

	}

	@Override
	public void maxTextSizeCallback() {

	}

	private String getPlainData(int iRealDataLength) {
		String plainData = "0";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(mTKMgr.getSecureKey());

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(mTKMgr.getCipherData(), pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "0";
			}

		} catch (Exception e) {
			Logs.printException(e);
		}
		if (!TextUtils.isEmpty(plainData))
			plainData = plainData.substring(iRealDataLength -1, iRealDataLength);
		return plainData;
	}

	/**
	 * Extras 값 획득
	 */
	private void getExtra() {
		mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
		if(mCommonUserInfo.getOtpPWLength() <= 0 || mCommonUserInfo.getOtpPWLength() > Const.OTP_PW_JUMIN_LENGTH)
			mCommonUserInfo.setOtpPWLength(Const.OTP_PW_NUM_LENGTH);
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mLayoutMsg = (LinearLayout) findViewById(R.id.layout_otp_pw_auth_msg);
		mTextDesc = (TextView) findViewById(R.id.textview_otp_pw_auth_desc);
		mTextDesc.setText(getResources().getString(R.string.title_reg_other_otp));
		mTextMsg = (TextView) findViewById(R.id.textview_otp_pw_auth_msg);

		mExitOtpPw = (EditText) findViewById(R.id.layout_otp_pw_input_auth).findViewById(R.id.editText);
		mEditOtpPw01 = (EditText) findViewById(R.id.edittext_otp_pw_auth_01);
		mEditOtpPw01.setOnTouchListener(this);
		mImageOtpPw01 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_01);
		mEditOtpPw01.setOnKeyListener(null);
		mEditOtpPw02 = (EditText) findViewById(R.id.edittext_otp_pw_auth_02);
		mEditOtpPw02.setOnTouchListener(this);
		mEditOtpPw02.setOnKeyListener(null);
		mImageOtpPw02 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_02);
		mEditOtpPw03 = (EditText) findViewById(R.id.edittext_otp_pw_auth_03);
		mEditOtpPw03.setOnTouchListener(this);
		mEditOtpPw03.setOnKeyListener(null);
		mImageOtpPw03 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_03);
		mEditOtpPw04 = (EditText) findViewById(R.id.edittext_otp_pw_auth_04);
		mEditOtpPw04.setOnTouchListener(this);
		mEditOtpPw04.setOnKeyListener(null);
		mImageOtpPw04 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_04);
		RelativeLayout layoutOtpPw05 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_05);
		mEditOtpPw05 = (EditText) findViewById(R.id.edittext_otp_pw_auth_05);
		mEditOtpPw05.setOnTouchListener(this);
		mEditOtpPw05.setOnKeyListener(null);
		mImageOtpPw05 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_05);
		RelativeLayout layoutOtpPw06 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_06);
		mEditOtpPw06 = (EditText) findViewById(R.id.edittext_otp_pw_auth_06);
		mEditOtpPw06.setOnTouchListener(this);
		mEditOtpPw06.setOnKeyListener(null);
		mImageOtpPw06 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_06);

		mArrayNumber[0] = R.drawable.ico_otp_num_0_on;
		mArrayNumber[1] = R.drawable.ico_otp_num_1_on;
		mArrayNumber[2] = R.drawable.ico_otp_num_2_on;
		mArrayNumber[3] = R.drawable.ico_otp_num_3_on;
		mArrayNumber[4] = R.drawable.ico_otp_num_4_on;
		mArrayNumber[5] = R.drawable.ico_otp_num_5_on;
		mArrayNumber[6] = R.drawable.ico_otp_num_6_on;
		mArrayNumber[7] = R.drawable.ico_otp_num_7_on;
		mArrayNumber[8] = R.drawable.ico_otp_num_8_on;
		mArrayNumber[9] = R.drawable.ico_otp_num_9_on;

		(findViewById(R.id.tv_close)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
				overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
			}
		});

		if (mCommonUserInfo.getOtpPWLength() == 4) {
			layoutOtpPw05.setVisibility(View.GONE);
			layoutOtpPw06.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getOtpPWLength() == 5) {
			layoutOtpPw06.setVisibility(View.GONE);
		}

		mTKMgr = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
				TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX,
				"Pin Code",//label
				"",//hint
				6,  //max length
				"",//max length msg
				6,  //min length
				"",
				5,
				true,
				(FrameLayout) findViewById(R.id.keypadContainer),
				mExitOtpPw,
				(HorizontalScrollView) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.keyscroll),
				(LinearLayout) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.keylayout),
				(ImageButton) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.clearall),
				(RelativeLayout) findViewById(R.id.keypadBallon),
				null,
				false,
				true);
		mTKMgr.showKeypad(mKeypadType);
	}

	/**
	 * 핀코드 입력값 화면 초기화
	 */
	private void clearInput() {
		mEditOtpPw01.setText("");
		mImageOtpPw01.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw02.setText("");
		mImageOtpPw02.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw03.setText("");
		mImageOtpPw03.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw04.setText("");
		mImageOtpPw04.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw05.setText("");
		mImageOtpPw05.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw06.setText("");
		mImageOtpPw06.setImageResource(R.drawable.ico_otp_num_off);

		mEditOtpPw01.requestFocus();
	}

	/**
	 * 등록 시, 두번의 핀코드 입력값 비교
	 * @param orgPin 첫번째 입력 핀코드값
	 * @param newPin 두번째 입력 핀코드값
	 * @return
	 */
	private boolean isEqualsPin(String orgPin, String newPin) {
		if (TextUtils.isEmpty(orgPin) || TextUtils.isEmpty(newPin))
			return false;

		if (orgPin.length() != newPin.length())
			return false;

		if (orgPin.equalsIgnoreCase(newPin))
			return true;

		return false;
	}

	/**
	 * 키패드 보이기
	 */
	private void showKeyPad() {
		clearInput();
		mExitOtpPw.requestFocus();
		mTKMgr.showKeypad(mKeypadType);
		mIsViewCtrlKeypad = true;
	}

	/**
	 * 등록 시 입력 프로세스 뒤로 이동
	 */
	private void backRegStep() {
		if (mRegStep == REG_STEP.STEP_01) {
			if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_OTP) {

			} else if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB) {
				setResult(RESULT_CANCELED);
			}
			finish();
			overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);

		} else if (mRegStep == REG_STEP.STEP_02) {
			mFirstPin = null;
			mTextDesc.setText(R.string.title_reg_other_otp);
			mRegStep = REG_STEP.STEP_01;
			showKeyPad();
		}
	}
}