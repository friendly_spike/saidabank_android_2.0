package com.sbi.saidabank.activity.transaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.login.ReregLostPincodeActivity;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;
import com.sbi.saidabank.solution.motp.MOTPManager;

import java.util.HashMap;
import java.util.Map;

public class TransferSafeDealSelectActivity extends BaseActivity implements View.OnClickListener{
    private LinearLayout mLayoutBody;
    private LinearLayout mLayoutClose;

    //내계좌 물고 들어올때
    private String mIntentMyAccount;

    //앱수트를 적용하면 다음화면으로 넘어가는데 오래걸려서 더블클릭을 유발하게 된다.
    private boolean isProtectDoubleClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MLog.d();
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorFFEA0A));

        setContentView(R.layout.activity_transfer_safe_deal_select);

        //내계좌를 물고 들어 오는지 확인.
        mIntentMyAccount = getIntent().getStringExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT);

        mLayoutBody = findViewById(R.id.layout_body);
        int radius = (int)Utils.dpToPixel(this,18f);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(this,R.color.white),new int[]{radius,radius,0,0}));

        mLayoutClose = findViewById(R.id.layout_close);
        mLayoutClose.setOnClickListener(this);

        //글자강조
        TextView tvDesc = (TextView) findViewById(R.id.tv_desc);
        Utils.setTextWithSpan(tvDesc, tvDesc.getText().toString(), "안심이체", Typeface.BOLD,"");


        radius = (int)Utils.dpToPixel(this,6f);
        Drawable kindAreaBg = GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(this,R.color.colorF5F5F5),new int[]{radius,radius,radius,radius});

        RelativeLayout layout_money = findViewById(R.id.layout_money);
        layout_money.setOnClickListener(this);
        layout_money.setBackground(kindAreaBg);

        RelativeLayout layout_property = findViewById(R.id.layout_property);
        layout_property.setOnClickListener(this);
        layout_property.setBackground(kindAreaBg);

        RelativeLayout layout_goods = findViewById(R.id.layout_goods);
        layout_goods.setOnClickListener(this);
        layout_goods.setBackground(kindAreaBg);

        //다른 거래에 남아잇는 정보 모두 클리어.
        TransferSafeDealDataMgr.clear();

        //웹 세션 정보 클리어 후 동기화 진행.
        requestClearTransferSession();

        //화면 사이즈에 따라 타이틀 및 body layout의 높이를 조절한다.
        mLayoutBody.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        int activityHeight = GraphicUtils.getActivityHeight(TransferSafeDealSelectActivity.this) - GraphicUtils.getStatusBarHeight(TransferSafeDealSelectActivity.this);
                        int titleHeight = (int)getResources().getDimension(R.dimen.safe_deal_select_title_height);
                        int bodyHeight  = mLayoutBody.getHeight();

                        if(activityHeight < (titleHeight + bodyHeight)){
                            ViewGroup.LayoutParams param = mLayoutBody.getLayoutParams();
                            param.height = activityHeight - titleHeight;
                            mLayoutBody.setLayoutParams(param);
                        }

                        mLayoutBody.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

    }

    @Override
    public void onBackPressed() {
        mLayoutClose.performClick();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_close:
                DialogUtil.alert(TransferSafeDealSelectActivity.this, "다음에 하기","계속하기", "안심이체를 취소하시겠습니까?", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
                break;
            case R.id.layout_money: {
                goSafeDealActivity(Const.SAFEDEAL_TYPE_MOENY);
                break;
            }
            case R.id.layout_property: {
                goSafeDealActivity(Const.SAFEDEAL_TYPE_PROPERTY);
                break;
            }
            case R.id.layout_goods: {
                goSafeDealActivity(Const.SAFEDEAL_TYPE_GOODS);
                break;
            }
        }
    }


    private void goSafeDealActivity(int type){
        if(isProtectDoubleClick) return;

        isProtectDoubleClick = true;
        Intent intent = new Intent(TransferSafeDealSelectActivity.this, TransferSafeDealActivity.class);
        intent.putExtra("safe_deal_kind", type);
        if(!TextUtils.isEmpty(mIntentMyAccount))
            intent.putExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT, mIntentMyAccount);
        startActivity(intent);
        finish();
    }

    /**
     * 세션 클리어
     * 안심이체 진입시 세션 모두 클리어하고 다시 동기화
     */
    private void requestClearTransferSession() {
        Map param = new HashMap();

        param.put("WTCH_ACNO", "");
        param.put("TRN_TYCD", "3");

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

//20210216-안전거래 진입시 동기화하도록 변경되어 주석처리함.
//                TransferRequestUtils.requestSessionSync(TransferSafeDealSelectActivity.this, new HttpSenderTask.HttpRequestListener() {
//                    @Override
//                    public void endHttpRequest(String ret) {
//                        //checkVaildAuthMethod();
//                        TransferUtils.checkErrorCntAndVaildAuthMethod(TransferSafeDealSelectActivity.this, new TransferUtils.OnCheckFinishListener() {
//                            @Override
//                            public void onCheckFinish() {
//                                dismissProgressDialog();
//                            }
//                        });
//                    }
//                });

            }
        });
    }

    /**
     * 세션동기화
     */
//    private void requestSessionSync() {
//        MLog.d();
//        Map param = new HashMap();
//        param.put("BLNC_INQ_YN", "N");//오픈뱅킹 잔액조회 여부. - 오픈뱅킹은 안전거래를 할수 없다.
//
//        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
//            @Override
//            public void endHttpRequest(String ret) {
//
//                if (TextUtils.isEmpty(ret)) {
//                    dismissProgressDialog();
//                    showErrorMessage(getString(R.string.msg_debug_no_response), new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            finish();
//                        }
//                    });
//                    return;
//                }
//
//                try {
//
//                    JSONObject object = new JSONObject(ret);
//                    if (DataUtil.isNull(object)) {
//                        dismissProgressDialog();
//                        showErrorMessage(getString(R.string.msg_debug_err_response), new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                finish();
//                            }
//                        });
//                        return;
//                    }
//
//                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
//                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
//                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
//                        dismissProgressDialog();
//                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
//                        if (TextUtils.isEmpty(msg))
//                            msg = getString(R.string.common_msg_no_reponse_value_was);
//                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
//                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, new CommonErrorDialog.OnConfirmListener() {
//                            @Override
//                            public void onConfirmPress() {
//                                finish();
//                            }
//                        });
//                        return;
//                    }
//
//                    LoginUserInfo.clearInstance();
//                    LoginUserInfo.getInstance().syncLoginSession(ret);
//                    LoginUserInfo.getInstance().setFakeFinderAllow(true);
//
//                    checkVaildAuthMethod();
//                } catch (JSONException e) {
//                    dismissProgressDialog();
//                    MLog.e(e);
//                }
//            }
//        });
//    }

    /**
     * 인증 수단별 입력 횟수 오류 체크
     */
//    private void checkVaildAuthMethod() {
//        MLog.d();
//        dismissProgressDialog();
//        //OTP값 초기화
//        TransferSafeDealDataMgr.getInstance().setOTP_SERIAL_NO("");
//        TransferSafeDealDataMgr.getInstance().setOTP_TA_VERSION("");
//
//        if (isFinishing()) {
//            return;
//        }
//
//        // 핀코드 오류 횟수 체크
//        if (Prefer.getPinErrorCount(TransferSafeDealSelectActivity.this) >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
//            AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//            alertDialog.mNListener = new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    finish();
//                }
//            };
//            alertDialog.mPListener = new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.PINCODE_FORGET);
//                    commonUserInfo.setMBRnumber(DataUtil.isNotNull(LoginUserInfo.getInstance().getMBR_NO()) ? LoginUserInfo.getInstance().getMBR_NO() : Const.EMPTY);
//                    String accn = LoginUserInfo.getInstance().getODDP_ACCN();
//                    commonUserInfo.setHasAccount((DataUtil.isNull(accn) || Integer.parseInt(accn) < 1));
//                    Intent intent = new Intent(TransferSafeDealSelectActivity.this, ReregLostPincodeActivity.class);
//                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
//                    startActivity(intent);
//                    finish();
//                }
//            };
//            alertDialog.msg = String.format(getString(R.string.msg_err_over_match_pin), Const.SSENSTONE_AUTH_COUNT_FAIL);
//            alertDialog.mPBtText = getString(R.string.rereg);
//            alertDialog.show();
//            return;
//        }
//
//        final LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
//        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
//
//        if (DataUtil.isNull(SECU_MEDI_DVCD)) {
//
//            return;
//        }
//
//        if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {
//            String OTP_STCD = loginUserInfo.getOTP_STCD();
//            final String MOTP_EROR_TCNT = loginUserInfo.getMOTP_EROR_TCNT();
//            if ("120".equalsIgnoreCase(OTP_STCD)) { // 사고 분실
//
//
//                AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                alertDialog.mNListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                };
//                alertDialog.mPListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CUS0030101.getServiceUrl();
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.mLinkListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CRT0040101.getServiceUrl();
//                        String param = null;
//                        if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
//                            param = "TRTM_DVCD=ACDTLOCK";
//                        } else {
//                            param = "TRTM_DVCD=ACDT";
//                        }
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        intent.putExtra(Const.INTENT_PARAM, param);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.mTopText = getString(R.string.msg_otp_accident_topmsg);
//                alertDialog.msg = getString(R.string.msg_otp_accdent_contents);
//                alertDialog.mPBtText = getString(R.string.msg_report_otp);
//                alertDialog.mLinkText = getString(R.string.msg_motp_issue);
//                alertDialog.show();
//
//            } else if ("130".equalsIgnoreCase(OTP_STCD)) { // 폐기된 인증서
//
//
//                AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                alertDialog.mNListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                };
//                alertDialog.mPListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CRT0220101.getServiceUrl();
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.mTopText = getString(R.string.msg_otp_disposal_topmsg);
//                alertDialog.msg = getString(R.string.msg_otp_disposal_contsnts);
//                alertDialog.mPBtText = getString(R.string.msg_otp_other_disposal_termination);
//                alertDialog.show();
//
//            } else if ("140".equalsIgnoreCase(OTP_STCD)) { // 오류 횟수 초과
//
//
//                AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                alertDialog.mNListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                };
//                alertDialog.mPListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.msg = getString(R.string.msg_otp_lost);
//                alertDialog.mPBtText = getString(R.string.msg_init_otp);
//                alertDialog.show();
//
//            } else if (!TextUtils.isEmpty(MOTP_EROR_TCNT) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
//
//
//                AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                alertDialog.mNListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                };
//                alertDialog.mPListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        String url = WasServiceUrl.CRT0080401.getServiceUrl();
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.msg = getString(R.string.msg_otp_lost);
//                alertDialog.mPBtText = getString(R.string.msg_init_otp);
//                alertDialog.show();
//
//            }
//        } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
//
//            if (Const.BRIDGE_RESULT_YES.equals(loginUserInfo.getMOTP_END_YN())) {
//
//
//                AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                alertDialog.mNListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                };
//                alertDialog.mPListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
//                        String param = "TRTM_DVCD=" + 1;
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        intent.putExtra(Const.INTENT_PARAM, param);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.msg = getString(R.string.msg_motp_expired);
//                alertDialog.mPBtText = getString(R.string.common_reissue);
//                alertDialog.show();
//                return;
//            }
//
//            if (DataUtil.isNotNull(loginUserInfo.getMOTP_EROR_TCNT()) && (Integer.parseInt(loginUserInfo.getMOTP_EROR_TCNT()) >= Const.OTP_PW_OTP_AUTH_COUNT_FAIL)) {
//
//
//                AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                alertDialog.mNListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        finish();
//                    }
//                };
//                alertDialog.mPListener = new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
//                        String param = "TRTM_DVCD=" + 1;
//                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                        intent.putExtra(Const.INTENT_PARAM, param);
//                        startActivity(intent);
//                        finish();
//                    }
//                };
//                alertDialog.msg = getString(R.string.msg_motp_lost);
//                alertDialog.mPBtText = getString(R.string.common_reissue);
//                alertDialog.show();
//                return;
//            }
//
//            MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
//                @Override
//                public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
//
//                    if (!"0000".equals(resultCode)) {
//                        AlertDialog alertDialog = new AlertDialog(TransferSafeDealSelectActivity.this);
//                        alertDialog.mNListener = new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                finish();
//                            }
//                        };
//                        String deviceId = LoginUserInfo.getInstance().getSMPH_DEVICE_ID();
//                        String motpDeviceId = LoginUserInfo.getInstance().getMOTP_SMPH_DEVICE_ID();
//                        if (TextUtils.isEmpty(deviceId) || TextUtils.isEmpty(motpDeviceId)) {
//                            alertDialog.mPListener = new View.OnClickListener() {
//                                @Override
//                                public void onClick(View view) {
//                                    Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                                    String url = WasServiceUrl.CRT0040401.getServiceUrl();
//                                    String param = "TRTM_DVCD=" + 1;
//                                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                                    intent.putExtra(Const.INTENT_PARAM, param);
//                                    startActivity(intent);
//                                    finish();
//                                }
//                            };
//                        } else {
//                            if (deviceId.equals(motpDeviceId)) {
//                                alertDialog.mPListener = new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                                        String url = WasServiceUrl.CRT0040501.getServiceUrl();
//                                        intent.putExtra("url", url);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                };
//                            } else {
//                                alertDialog.mPListener = new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View view) {
//                                        Intent intent = new Intent(TransferSafeDealSelectActivity.this, WebMainActivity.class);
//                                        String url = WasServiceUrl.CRT0040401.getServiceUrl();
//                                        String param = "TRTM_DVCD=" + 1;
//                                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
//                                        intent.putExtra(Const.INTENT_PARAM, param);
//                                        startActivity(intent);
//                                        finish();
//                                    }
//                                };
//                            }
//                        }
//                        alertDialog.msg = getString(R.string.msg_motp_change_device);
//                        alertDialog.mPBtText = getString(R.string.common_reissue);
//                        alertDialog.show();
//
//                    } else {
//                        TransferSafeDealDataMgr.getInstance().setOTP_SERIAL_NO(reqData1);
//                        TransferSafeDealDataMgr.getInstance().setOTP_TA_VERSION(reqData4);
//                    }
//                }
//            });
//        }
//    }
}
