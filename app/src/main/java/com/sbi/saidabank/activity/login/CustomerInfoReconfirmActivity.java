package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.Logs;

/**
 * siadabank_work
 * Class: CustomerInfoReconfirmActivity
 * Created by 950546
 * Date: 2018-11-16
 * Time: 오전 11:00
 * Description: 고객정보 재확인 화면
 */
public class CustomerInfoReconfirmActivity extends BaseActivity implements View.OnClickListener {

    private boolean bIsbeforeexpired;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_info_reconfirm);

        bIsbeforeexpired = false;

        initView();
    }

    private void initView() {
        TextView tvExpiredmsg1 = findViewById(R.id.tv_msg1);
        TextView tvExpiredmsg = findViewById(R.id.tv_expiredmsg);

        if (bIsbeforeexpired) {
            tvExpiredmsg.setText(String.format(getString(R.string.msg_reconfirm_customer_info_before_expired_01), "2018년 12월 28일"));
        } else {
            tvExpiredmsg.setText(R.string.msg_reconfirm_customer_info_after_expired_01);
            tvExpiredmsg1.setText(R.string.msg_reconfirm_customer_info_after_expired);
            findViewById(R.id.btn_cancel).setVisibility(View.GONE);
        }

        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_confirm: {
                Intent intent = new Intent(this, CertifyIdActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case R.id.btn_cancel: {
                Logs.showToast(this,"메인으로 이동");
                break;
            }
            default:
                break;
        }
    }
}
