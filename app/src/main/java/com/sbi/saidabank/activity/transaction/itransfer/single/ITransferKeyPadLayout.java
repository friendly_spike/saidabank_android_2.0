package com.sbi.saidabank.activity.transaction.itransfer.single;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mangosteen.falldownnumbereditor.FallDownNumberEditor;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.ITransferSearchReceiverActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSingleActionListener;
import com.sbi.saidabank.activity.transaction.safedeal.CustomNumPadLayout;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;

public class ITransferKeyPadLayout extends ITransferBaseLayout implements CustomNumPadLayout.OnNumPadClickListener{


    private Button mOKBtn;
    private ITransferInputMoneyLayout mInputMoneyLayout;
    private OnITransferSingleActionListener mActionListener;

    public ITransferKeyPadLayout(Context context) {
        super(context);
        initUX(context);
    }

    public ITransferKeyPadLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }



    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_itransfer_keypad, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));


        mOKBtn = findViewById(R.id.btn_ok);
        mOKBtn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                final String inputMoney = mInputMoneyLayout.getInputMoney();

                if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_ACCOUNT){
                    //가상계좌는 앞에서 수취조회를 패스했기에 금액이 입력된 후 수취인 조회를 한번 더한다.
                    ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                    String VERTUAL_ACCOUNT_YN = remitteeInfo.getVERTUAL_ACCOUNT_YN();
                    if(TextUtils.isEmpty(VERTUAL_ACCOUNT_YN)) VERTUAL_ACCOUNT_YN = "N";
                    if(VERTUAL_ACCOUNT_YN.equalsIgnoreCase("Y")){
                        String bankCd = remitteeInfo.getCNTP_FIN_INST_CD();
                        String detailBankCd = remitteeInfo.getDTLS_FNLT_CD();
                        String accountNo = remitteeInfo.getCNTP_BANK_ACNO();
                        TransferRequestUtils.requestRemitteeAccountBySingle((BaseActivity)getContext(),
                                bankCd,
                                detailBankCd,
                                accountNo,
                                "",
                                inputMoney,
                                false,
                                new HttpSenderTask.HttpRequestListener2() {
                                    @Override
                                    public void endHttpRequest(boolean result, String ret) {
                                        if(result){
                                            //정상적으로 수취조회가 되면 VERTUAL_ACCOUNT_YN은 N로 변경되어 아래 코드를 타게된다.
                                            View accountLayout = getSlideView(ITransferSelectAccountLayout.class);
                                            if(accountLayout != null){
                                                ((ITransferSelectAccountLayout)accountLayout).dispRcvAccount(false);
                                            }
                                            mOKBtn.performClick();
                                        }
                                    }
                                });
                        return;
                    }
                }



                //사이다 뱅킹 일때만 아래 서비스 체크한다.
                String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
                String DLY_TRNF_SVC_ENTR_YN = LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN();
                if(TextUtils.isEmpty(DLY_TRNF_SVC_ENTR_YN)) DLY_TRNF_SVC_ENTR_YN = "N";
                String DSGT_MNRC_ACCO_SVC_ENTR_YN = LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN();
                if(TextUtils.isEmpty(DSGT_MNRC_ACCO_SVC_ENTR_YN)) DSGT_MNRC_ACCO_SVC_ENTR_YN = "N";

                if(WTCH_BANK_CD.equalsIgnoreCase("028")){
                    if( DLY_TRNF_SVC_ENTR_YN.equalsIgnoreCase("Y")||
                            DSGT_MNRC_ACCO_SVC_ENTR_YN.equalsIgnoreCase("Y")){

                        TransferRequestUtils.requestDlyDsgtServiceCheck((Activity) getContext(), inputMoney, new HttpSenderTask.HttpRequestListener2() {
                            @Override
                            public void endHttpRequest(boolean result, String ret) {
                                if(result){
                                    //hideView();
                                    mInputMoneyLayout.moveTopPosAmountView();
                                    fadeOutKeypad();
                                    if(mActionListener != null)
                                        mActionListener.openInputDetailView(inputMoney);
                                }
                            }
                        });
                        return;
                    }
                }

                //확인 누를경우 가져오기, 내보내기 상태 초기화.
                ITransferDataMgr.getInstance().setTRANSFER_ACCESS_TYPE(ITransferDataMgr.ACCESS_TYPE_NORMAL);

                mInputMoneyLayout.moveTopPosAmountView();
                fadeOutKeypad();
                if(mActionListener != null)
                    mActionListener.openInputDetailView(inputMoney);
                else
                    Logs.showToast(getContext(),"ActionListener를 등록해주세요.");
            }
        });

        CustomNumPadLayout numPadLayout = layout.findViewById(R.id.numpad);
        numPadLayout.setNumPadClickListener(this);


    }

    public void setActionListener(OnITransferSingleActionListener listener){
        mActionListener = listener;
    }



    @Override
    public void onNumPadClick(String numStr) {
        if(mInputMoneyLayout != null)
            mInputMoneyLayout.onNumPadClick(numStr);
    }

    public void setInputMoneyLayout(ITransferInputMoneyLayout inputMoneyLayout){
        mInputMoneyLayout = inputMoneyLayout;
    }

    public void setOKBtnState(boolean flag){
        mOKBtn.setEnabled(flag);
    }

    public void fadeInKeypad(){
        setVisibility(VISIBLE);
        this.animate()
                .translationY(0)
                .setDuration(250)
                .alpha(1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }
                } )
                .start();
    }

    public void fadeOutKeypad(){
        this.animate()
                .translationY(this.getHeight())
                .setDuration(250)
                .alpha(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(GONE);
                    }
                } )
                .start();
    }
}
