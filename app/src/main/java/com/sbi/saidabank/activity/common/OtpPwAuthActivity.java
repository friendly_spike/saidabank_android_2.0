package com.sbi.saidabank.activity.common;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;
import com.sbi.saidabank.solution.fds.FDSManager;
import com.sbi.saidabank.solution.motp.MOTPManager;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: OtpPwAuthActivity
 * Created by 950485 on 2018. 12. 17..
 * <p>
 * Description: OTP, password 인증을 위한 화면
 */

public class OtpPwAuthActivity extends BaseActivity implements View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener {
	private static int MOTP_REREG_REQCODE = 1000;
	private static int mKeypadType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;
	private static final long MIN_CLICK_INTERVAL = 500;    // ms

	private LinearLayout  mLayoutMsg;
	private TextView      mTextDesc;
	private TextView      mTextMsg;
	private EditText      mExitOtpPw;
	private EditText      mEditOtpPw01;
	private ImageView     mImageOtpPw01;
	private EditText      mEditOtpPw02;
	private ImageView     mImageOtpPw02;
	private EditText      mEditOtpPw03;
	private ImageView     mImageOtpPw03;
	private EditText      mEditOtpPw04;
	private ImageView     mImageOtpPw04;
	private EditText      mEditOtpPw05;
	private ImageView     mImageOtpPw05;
	private EditText      mEditOtpPw06;
	private ImageView     mImageOtpPw06;
	private EditText      mEditOtpPw07;
	private ImageView     mImageOtpPw07;

	private boolean       mIsViewCtrlKeypad = false;

	private CommonUserInfo mCommonUserInfo;

	private TransKeyCtrl  mTKMgr = null;

	private String        mNativeOtpAuthTools;
	private String        mNativeOtpSignData;
	private String        mMOTPSerialNumber;
	private String        mMOTPTAVersion;
	private long 		  mLastClickTime;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_otp_pw_auth);

		getExtra();
        initUX();


	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    	if (mIsViewCtrlKeypad == false) {
	    		showKeyPad();
				return true;
		    }
    	}
		return false;
	}

	@Override
	public void cancel(Intent data) {
		finish();
	}

	@Override
	public void done(Intent data) {
		mIsViewCtrlKeypad = false;
		if (data == null)
			return;

		long currentClickTime = SystemClock.uptimeMillis();
		long elapsedTime = currentClickTime - mLastClickTime;
		mLastClickTime = currentClickTime;

		if (elapsedTime <= MIN_CLICK_INTERVAL) {
			// 중복클릭 방지
			return;
		}

		String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);
		String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
		String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
		byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
		int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

		if (iRealDataLength != mCommonUserInfo.getOtpPWLength()) {
			String msg = getString(R.string.message_pin_right_length, mCommonUserInfo.getOtpPWLength());
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(msg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		String plainData = "";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(secureKey);

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "";
			}

			if (TextUtils.isEmpty(plainData)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_decode_pin);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}

			String firstNum = plainData.substring(0, 1);
            if ((mCommonUserInfo.getOtpPWLength() == 7) &&
                    (!"1".equals(firstNum) && !"2".equals(firstNum)
                            && !"3".equals(firstNum) && !"4".equals(firstNum))) {
                String msg = getString(R.string.msg_invalid_backidnumber);
                mLayoutMsg.setVisibility(View.VISIBLE);
                mTextMsg.setText(msg);
                showKeyPad();
                AniUtils.shakeView(mLayoutMsg);
                return;
            }

        } catch (Exception e) {
			Logs.printException(e);
		}

		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
			if (	mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP
						|| mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_CHNG_MOTP
						|| mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL
					    || mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY
			) {

				requestMOTPTimeInfo(plainData);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_LOSTPIN) {
				requestLostRelease(secureData);
			}
		} else {
			//입력된 비밀번호를 자바스크립트 콜백으로 리턴
			Intent intent = new Intent();
			mCommonUserInfo.setSecureData(secureData);
			mCommonUserInfo.setPlainData(plainData);
			mCommonUserInfo.setDummyData(dummyData);
			mCommonUserInfo.setCipherData(cipherData);
			mCommonUserInfo.setOtpForgetAuthNo(false);
			intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
			setResult(RESULT_OK, intent);
			finish();
			overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
		}
	}
	
	@Override
	public void onBackPressed() {
		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB) {
			setResult(RESULT_CANCELED);
		} else if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
			if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_LOSTPIN) {
				Intent intent = new Intent(this, ReportLostGuideActivity.class);
				intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
				startActivity(intent);
			} else {
				setResult(RESULT_CANCELED);
			}
		}
		super.onBackPressed();
		overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
	}
	
	@Override
	public void input(int arg0) {
		if (arg0 == ITransKeyActionListenerEx.INPUT_CHARACTER_KEY) {
			mLayoutMsg.setVisibility(View.INVISIBLE);

			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();
			String strValue6 = mEditOtpPw06.getText().toString();

			if (TextUtils.isEmpty(strValue1)) {
				mEditOtpPw01.setText("0");
				mImageOtpPw01.setImageResource(R.drawable.ico_pin_on);
				mEditOtpPw02.requestFocus();
			} else if (TextUtils.isEmpty(strValue2)) {
				mEditOtpPw02.setText("0");
				mImageOtpPw02.setImageResource(R.drawable.ico_pin_on);
				mEditOtpPw03.requestFocus();
			} else if (TextUtils.isEmpty(strValue3)) {
				mEditOtpPw03.setText("0");
				mImageOtpPw03.setImageResource(R.drawable.ico_pin_on);
				mEditOtpPw04.requestFocus();
			} else if (TextUtils.isEmpty(strValue4)) {
				mEditOtpPw04.setText("0");
				mImageOtpPw04.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getOtpPWLength() == 5)
					mEditOtpPw05.requestFocus();
			} else if (TextUtils.isEmpty(strValue5)) {
				mEditOtpPw05.setText("0");
				mImageOtpPw05.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getOtpPWLength() == 6)
					mEditOtpPw06.requestFocus();
			} else if (TextUtils.isEmpty(strValue6)) {
				mEditOtpPw06.setText("0");
				mImageOtpPw06.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getOtpPWLength() == 7)
					mEditOtpPw07.requestFocus();
			} else {
				mEditOtpPw07.setText("0");
				mImageOtpPw07.setImageResource(R.drawable.ico_pin_on);
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_BACKSPACE_KEY) {
			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();
			String strValue6 = mEditOtpPw06.getText().toString();
			String strValue7 = mEditOtpPw07.getText().toString();

			if (mCommonUserInfo.getOtpPWLength() == 7) {
				if (!TextUtils.isEmpty(strValue7)) {
					mEditOtpPw07.setText("");
					mImageOtpPw07.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw06.requestFocus();
					return;
				} else if (!TextUtils.isEmpty(strValue6)) {
						mEditOtpPw06.setText("");
						mImageOtpPw06.setImageResource(R.drawable.ico_pin_line_off);
						mEditOtpPw05.requestFocus();
						return;
				} else {
					if (!TextUtils.isEmpty(strValue5)) {
						mEditOtpPw05.setText("");
						mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
						mEditOtpPw04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getOtpPWLength() == 6) {
				if (!TextUtils.isEmpty(strValue6)) {
					mEditOtpPw06.setText("");
					mImageOtpPw06.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw05.requestFocus();
					return;
				} else {
					if (!TextUtils.isEmpty(strValue5)) {
						mEditOtpPw05.setText("");
						mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
						mEditOtpPw04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getOtpPWLength() == 5) {
				if (!TextUtils.isEmpty(strValue5)) {
					mEditOtpPw05.setText("");
					mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw04.requestFocus();
					return;
				}
			}

			if (!TextUtils.isEmpty(strValue4)) {
				mEditOtpPw04.setText("");
				mImageOtpPw04.setImageResource(R.drawable.ico_pin_line_off);
				mEditOtpPw03.requestFocus();
			} else if (!TextUtils.isEmpty(strValue3)) {
				mEditOtpPw03.setText("");
				mImageOtpPw03.setImageResource(R.drawable.ico_pin_line_off);
				mEditOtpPw02.requestFocus();
			} else if (!TextUtils.isEmpty(strValue2)) {
				mEditOtpPw02.setText("");
				mImageOtpPw02.setImageResource(R.drawable.ico_pin_line_off);
				mEditOtpPw01.requestFocus();
			} else if (!TextUtils.isEmpty(strValue1)) {
				mEditOtpPw01.setText("");
				mImageOtpPw01.setImageResource(R.drawable.ico_pin_line_off);
			}
		}
		else if (arg0 == ITransKeyActionListenerEx.INPUT_CLEARALL_BUTTON) {
			clearInput();
		}
	}

	@Override
	public void minTextSizeCallback() {

	}

	@Override
	public void maxTextSizeCallback() {

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (resultCode == RESULT_OK) {
			Logs.e("activity result OK");
			showProgressDialog();
			MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
				@Override
				public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
					dismissProgressDialog();
					if ("0000".equals(resultCode)) {
						Logs.e("new motp serial number : " + reqData1);
						mMOTPSerialNumber = reqData1;
					} else {
						Logs.e("fail to get motp serial number");
						String SessionMOTPSerialNumber = LoginUserInfo.getInstance().getMOTPSerialNumber();
						if (!TextUtils.isEmpty(SessionMOTPSerialNumber))
							mMOTPSerialNumber = SessionMOTPSerialNumber;
					}
					return;
				}
			});
		} else {
			Logs.e("activity result canceled");
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		boolean isPerDeny = false;

		for (int grant : grantResults) {
			if (grant == PackageManager.PERMISSION_DENIED)
				isPerDeny = true;
		}

		if (isPerDeny) {
			DialogUtil.alert(this, R.string.common_msg_call_permission, new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(OtpPwAuthActivity.this, ReportLostGuideActivity.class);
					intent.putExtra(Const.INTENT_COMMON_INFO,mCommonUserInfo);
					startActivity(intent);
					finish();
				}
			});
		} else {
			String telNo = ((SaidaApplication) getApplication()).getTempTelNo();
			if (!TextUtils.isEmpty(telNo)) {
				if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
					return;
				}
				Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telNo));
				startActivity(intent);
			}
		}
	}

	/**
	 * Extras 값 획득
	 */
	private void getExtra() {
		Intent intent = getIntent();
		mCommonUserInfo = intent.getParcelableExtra(Const.INTENT_COMMON_INFO);
		if(mCommonUserInfo.getOtpPWLength() <= 0 || mCommonUserInfo.getOtpPWLength() > Const.OTP_PW_JUMIN_LENGTH)
			mCommonUserInfo.setOtpPWLength(Const.OTP_PW_NUM_LENGTH);

		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE){
			if( mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP
				||mCommonUserInfo.getOtpPWAccessType() ==	Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL
				||mCommonUserInfo.getOtpPWAccessType() ==	Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY) {

				mNativeOtpAuthTools = intent.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
				mNativeOtpSignData = intent.getStringExtra(Const.INTENT_OTP_AUTH_SIGN);
				mMOTPSerialNumber = intent.getStringExtra(Const.INTENT_OTP_SERIAL_NUMBER);
				mMOTPTAVersion = intent.getStringExtra(Const.INTENT_OTP_TA_VERSION);
			}
		}
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mLayoutMsg = (LinearLayout) findViewById(R.id.layout_otp_pw_auth_msg);
		mTextDesc = (TextView) findViewById(R.id.textview_otp_pw_auth_desc);
		mTextMsg = (TextView) findViewById(R.id.textview_otp_pw_auth_msg);

		mExitOtpPw = (EditText) findViewById(R.id.layout_otp_pw_input_auth).findViewById(R.id.editText);
		mEditOtpPw01 = (EditText) findViewById(R.id.edittext_otp_pw_auth_01);
		mEditOtpPw01.setOnTouchListener(this);
		mImageOtpPw01 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_01);
		mEditOtpPw01.setOnKeyListener(null);
		mEditOtpPw02 = (EditText) findViewById(R.id.edittext_otp_pw_auth_02);
		mEditOtpPw02.setOnTouchListener(this);
		mEditOtpPw02.setOnKeyListener(null);
		mImageOtpPw02 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_02);
		mEditOtpPw03 = (EditText) findViewById(R.id.edittext_otp_pw_auth_03);
		mEditOtpPw03.setOnTouchListener(this);
		mEditOtpPw03.setOnKeyListener(null);
		mImageOtpPw03 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_03);
		mEditOtpPw04 = (EditText) findViewById(R.id.edittext_otp_pw_auth_04);
		mEditOtpPw04.setOnTouchListener(this);
		mEditOtpPw04.setOnKeyListener(null);
		mImageOtpPw04 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_04);
		RelativeLayout layoutOtpPw05 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_05);
		mEditOtpPw05 = (EditText) findViewById(R.id.edittext_otp_pw_auth_05);
		mEditOtpPw05.setOnTouchListener(this);
		mEditOtpPw05.setOnKeyListener(null);
		mImageOtpPw05 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_05);
		RelativeLayout layoutOtpPw06 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_06);
		mEditOtpPw06 = (EditText) findViewById(R.id.edittext_otp_pw_auth_06);
		mEditOtpPw06.setOnTouchListener(this);
		mEditOtpPw06.setOnKeyListener(null);
		mImageOtpPw06 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_06);
		RelativeLayout layoutOtpPw07 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_07);
		mEditOtpPw07 = (EditText) findViewById(R.id.edittext_otp_pw_auth_07);
		mEditOtpPw07.setOnTouchListener(this);
		mEditOtpPw07.setOnKeyListener(null);
		mImageOtpPw07 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_07);

		TextView textLink = (TextView) findViewById(R.id.textview_link_lost_otp_pw_auth);

		(findViewById(R.id.tv_close)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB) {
					setResult(RESULT_CANCELED);
				} else if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
					if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_LOSTPIN) {
						Intent intent = new Intent(OtpPwAuthActivity.this, ReportLostGuideActivity.class);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						startActivity(intent);
					} else {
						setResult(RESULT_CANCELED);
					}
				}
				finish();
				overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
			}
		});

		/*
		LinearLayout layoutlostotppwauth = (LinearLayout) findViewById(R.id.layout_lost_otp_pw_auth);
		layoutlostotppwauth.setOnClickListener(new OnSingleClickListener() {
			@Override
			public void onSingleClick(View v) {
				Intent intent = new Intent(OtpPwAuthActivity.this, WebMainActivity.class);
				String url = WasServiceUrl.CRT0040401.getServiceUrl();
				Map param = new HashMap();
				param.put("TRTM_DVCD", "1");
				intent.putExtra("url", url);
				intent.putExtra(Const.INTENT_PARAM, param.toString());
				mLayoutMsg.setVisibility(View.INVISIBLE);
				startActivityForResult(intent, MOTP_REREG_REQCODE);
			}
		});
		*/

		String desc = "";
		String link = "";
		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
			if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_OTP) {
				desc = getString(R.string.verify) + getString(R.string.msg_input_otp_pw);
				link = getString(R.string.verify) + getString(R.string.msg_auth_lost_otp_pw);
			} else if (	mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP
					  ||mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL
					  ||mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY) {
				desc = getString(R.string.msg_verify_motp) + " " + getString(R.string.msg_input_otp_pw);
				link = getString(R.string.msg_verify_motp) + " " + getString(R.string.msg_auth_lost_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_PW) {
				desc = getString(R.string.msg_input_otp_pw);
				link = getString(R.string.msg_auth_lost_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_CHNG_OTP) {
				desc = getString(R.string.msg_now_setting) + "\n" + getString(R.string.verify) + getString(R.string.msg_input_otp_pw);
				link = getString(R.string.verify) + getString(R.string.msg_auth_lost_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_CHNG_MOTP) {
				desc = getString(R.string.msg_now_setting) + "\n" + getString(R.string.msg_verify_motp) + " " + getString(R.string.msg_input_otp_pw);
				link = getString(R.string.msg_verify_motp) + " " + getString(R.string.msg_auth_lost_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_CHNG_PW) {
				desc = getString(R.string.msg_input_otp_pw);
				link = getString(R.string.msg_auth_lost_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_LOSTPIN) {
				desc = getString(R.string.msg_reportlost_msg);
				link = getString(R.string.msg_auth_lost_otp_pw);
			} else {
				desc = getString(R.string.msg_input_otp_pw);
				link = getString(R.string.msg_auth_lost_otp_pw);
			}
		} else {
			String titleStr = "를\n입력해주세요.";
			String linkStr = "를 잊으셨나요?";
			desc = mCommonUserInfo.getOtpTitle() + titleStr;
			link = mCommonUserInfo.getOtpTitle() + linkStr;

			if (mCommonUserInfo.getOtpTitle().equals(Const.OTP_PW_TITLE_JUMIN)) {
				findViewById(R.id.layout_lost_otp).setVisibility(View.GONE);
			}
		}

		mTextDesc.setText(desc);
		textLink.setText(link);

		if (mCommonUserInfo.getOtpPWLength() == 4) {
			layoutOtpPw05.setVisibility(View.GONE);
			layoutOtpPw06.setVisibility(View.GONE);
			layoutOtpPw07.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getOtpPWLength() == 5) {
			layoutOtpPw06.setVisibility(View.GONE);
			layoutOtpPw07.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getOtpPWLength() == 6) {
			layoutOtpPw07.setVisibility(View.GONE);
		}

		//boolean isAutoFocus = (mCommonUserInfo.getOtpPWLength() != 7 && mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB) ? true : false;

		mTKMgr = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
				TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX,
				"Pin Code",//label
				"",//hint
				mCommonUserInfo.getOtpPWLength(),  //max length
				"",//max length msg
				mCommonUserInfo.getOtpPWLength(),  //min length
				"",
				5,
				true,
				(FrameLayout) findViewById(R.id.keypadContainer),
				mExitOtpPw,
				(HorizontalScrollView) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.keyscroll),
				(LinearLayout) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.keylayout),
				(ImageButton) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.clearall),
				(RelativeLayout) findViewById(R.id.keypadBallon),
				null,
				false,
				true);
		mTKMgr.showKeypad(mKeypadType);

		LinearLayout btnLostPin = (LinearLayout) findViewById(R.id.layout_lost_otp);
		btnLostPin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
					if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP
							|| mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL
							|| mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY
							|| mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_CHNG_MOTP) {
						showReregLostMotp();
					} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_PW || mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_CHNG_PW) {
						showReregLostPw();
					}
				} else {
					showReregLostPw();
				}
			}
		});

		if ((!mCommonUserInfo.isOtpUseForgetAuth() && mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB)
				|| mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_LOSTPIN) {
			btnLostPin.setVisibility(View.GONE);
		}
	}

	/**
	 * 핀코드 입력값 화면 초기화
	 */
	private void clearInput() {
		mEditOtpPw01.setText("");
		mImageOtpPw01.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw02.setText("");
		mImageOtpPw02.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw03.setText("");
		mImageOtpPw03.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw04.setText("");
		mImageOtpPw04.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw05.setText("");
		mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw06.setText("");
		mImageOtpPw06.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw07.setText("");
		mImageOtpPw07.setImageResource(R.drawable.ico_pin_line_off);

		mEditOtpPw01.requestFocus();
	}

	/**
	 * 키패드 보이기
	 */
	private void showKeyPad() {
		clearInput();
		mExitOtpPw.requestFocus();
		mTKMgr.showKeypad(mKeypadType);
		mIsViewCtrlKeypad = true;
	}

	// MOTP 검증
	private void requestMOTPTimeInfo(String pincode) {
		Map param = new HashMap();

		param.put("TRN_DVCD", "1");
		param.put("OTP_SRNO", mMOTPSerialNumber);
		param.put("TAP_VRSN", mMOTPTAVersion);
		param.put("OTP_PWD", pincode);

		if(mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL){
			param.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());
			param.put("MNRC_BANK_NM", TransferSafeDealDataMgr.getInstance().getMNRC_BANK_NAME());
			param.put("CNTP_BANK_ACNO", TransferSafeDealDataMgr.getInstance().getMNRC_ACNO());
			param.put("CNTP_ACCO_DEPR_NM", TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM());
			param.put("TRN_AMT", TransferSafeDealDataMgr.getInstance().getTRAM());
			param.put("FEE", TransferSafeDealDataMgr.getInstance().getTRNF_FEE());
		}else if(mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY){
			//급여이체 관련 변수.
			try{
				JSONArray jsonArray = new JSONArray();

				ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();

				for(int i=0;i<salayInfoArrayList.size()-1;i++){
					ITransferSalayMgr.ITransferSalayInfo outSalayInfo = salayInfoArrayList.get(i);
					ITransferSalayMgr.ITransferSalayInfo inSalayInfo = salayInfoArrayList.get(i+1);


					JSONObject jsonObject = new JSONObject();

					jsonObject.put("WTCH_ACNO",outSalayInfo.getACNO());         		//출금계좌번호
					jsonObject.put("TRN_AMT",outSalayInfo.getTRTM_AMT());         	//거래금액


					jsonObject.put("MNRC_BANK_NM",inSalayInfo.getMNRC_BANK_NM()); 	//입금은행명
					jsonObject.put("CNTP_BANK_ACNO",inSalayInfo.getDTLS_FNLT_CD());//상대은행계좌번호
					jsonObject.put("CNTP_ACCO_DEPR_NM",inSalayInfo.getCNTP_ACCO_DEPR_NM());//상대계좌예금주명

					jsonObject.put("FEE","0");       						//수수료

					jsonArray.put(jsonObject);
				}
				param.put("REC_IN", jsonArray);
			}catch (JSONException e){
				Logs.printException(e);
				return;
			}

		}


		showProgressDialog();
		HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(final String ret) {
				if (!TextUtils.isEmpty(ret)) {
					try {
						JSONObject object = new JSONObject(ret);
						if (object == null) {
							dismissProgressDialog();
							Logs.e(getResources().getString(R.string.msg_debug_err_response));
							Logs.e(ret);
							showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
							showKeyPad();
							return;
						}
						JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
						if (objectHead == null) {
							dismissProgressDialog();
							showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
							showKeyPad();
							return;
						}

						String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
						if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
							dismissProgressDialog();
							String code = objectHead.optString(Const.REQUEST_COMMON_CODE);
							String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
							if ("EEIF0509".equals(code)) {
								Logs.e("motp pin lock");
								DialogUtil.alert(OtpPwAuthActivity.this,
										getResources().getString(R.string.common_reissue),
										getResources().getString(R.string.common_cancel),
										getResources().getString(R.string.msg_motp_lost),
										new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												Intent intent = new Intent(OtpPwAuthActivity.this, WebMainActivity.class);
												String url = WasServiceUrl.CRT0040401.getServiceUrl();
												intent.putExtra("url", url);
												/*
												Map param = new HashMap();
												param.put("TRTM_DVCD", "1");
												intent.putExtra(Const.INTENT_PARAM, param.toString());
												*/
												mLayoutMsg.setVisibility(View.INVISIBLE);
												startActivityForResult(intent, MOTP_REREG_REQCODE);
												return;
											}
										},
										new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												setResult(RESULT_CANCELED);
												finish();
											}
										});
								showKeyPad();
								return;
							} else if ("EEIF0518".equals(code)) {
								Logs.e("ret : " + ret);
								Logs.e("motp pin error");
								mLayoutMsg.setVisibility(View.VISIBLE);
								mTextMsg.setText(msg);
								showKeyPad();
								AniUtils.shakeView(mLayoutMsg);
								return;
							}

							if (TextUtils.isEmpty(msg))
								msg = getString(R.string.common_msg_no_reponse_value_was);

							String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
							showCommonErrorDialog(msg, errCode, "", objectHead, true);
							Logs.e(ret);
							showKeyPad();
							return;
						}

						Logs.e("ret : " + object);

						String otpTrnHash = object.optString("TRN_INFO_HASH");
						final String otpTrnNo = object.optString("OTP_VRFC_TRN_NO");
						String otpTimeInfo = object.optString("OTP_TKTM_INFO");

						MOTPManager.getInstance(getApplication()).reqOTPCode(new MOTPManager.MOTPEventListener() {
							@Override
							public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
								if ("0000".equals(resultCode)) {
									checkMOTP(otpTrnNo, reqData1);
								} else {
									dismissProgressDialog();
									showErrorMessage("OTP코드 생성에 실패하였습니다.");
									showKeyPad();
									return;
								}
							}
						}, otpTimeInfo, mMOTPSerialNumber, otpTrnHash);

					} catch (JSONException e) {
						dismissProgressDialog();
						Logs.printException(e);
						showErrorMessage("OTP코드 생성에 실패하였습니다.");
						showKeyPad();
						return;
					}
				} else {
					dismissProgressDialog();
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
					showKeyPad();
					return;
				}
			}
		});
	}

	private void checkMOTP(String otpTrnNo, String otpEncNo) {
		Map param = new HashMap();

		param.put("TRN_DVCD", "2");
		param.put("OTP_SRNO", mMOTPSerialNumber);
		param.put("TAP_VRSN", mMOTPTAVersion);
		param.put("OTP_VRFC_TRN_NO", otpTrnNo);
		param.put("OTP_ENC_NO", otpEncNo);

		if(mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL){
			param.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());
			param.put("MNRC_BANK_NM", TransferSafeDealDataMgr.getInstance().getMNRC_BANK_NAME());
			param.put("CNTP_BANK_ACNO", TransferSafeDealDataMgr.getInstance().getMNRC_ACNO());
			param.put("CNTP_ACCO_DEPR_NM", TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM());
			param.put("TRN_AMT", TransferSafeDealDataMgr.getInstance().getTRAM());
			param.put("FEE", TransferSafeDealDataMgr.getInstance().getTRNF_FEE());
		}else if(mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY){
			//급여이체 관련 변수.
			try{
				JSONArray jsonArray = new JSONArray();

				ArrayList<ITransferSalayMgr.ITransferSalayInfo> salayInfoArrayList = ITransferSalayMgr.getInstance().getSalayArrayList();

				for(int i=0;i<salayInfoArrayList.size()-1;i++){
					ITransferSalayMgr.ITransferSalayInfo outSalayInfo = salayInfoArrayList.get(i);
					ITransferSalayMgr.ITransferSalayInfo inSalayInfo = salayInfoArrayList.get(i+1);


					JSONObject jsonObject = new JSONObject();

					jsonObject.put("WTCH_ACNO",outSalayInfo.getACNO());         		//출금계좌번호
					jsonObject.put("TRTM_AMT",outSalayInfo.getTRTM_AMT());         	//거래금액


					jsonObject.put("MNRC_BANK_NM",inSalayInfo.getMNRC_BANK_NM()); 	//입금은행명
					jsonObject.put("CNTP_BANK_ACNO",inSalayInfo.getDTLS_FNLT_CD());//상대은행계좌번호
					jsonObject.put("CNTP_ACCO_DEPR_NM",inSalayInfo.getCNTP_ACCO_DEPR_NM());//상대계좌예금주명

					jsonObject.put("FEE","0");       						//수수료

					jsonArray.put(jsonObject);
				}
				param.put("REC_IN", jsonArray);
			}catch (JSONException e){
				Logs.printException(e);
				return;
			}
		}



		HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				dismissProgressDialog();

				if (!TextUtils.isEmpty(ret)) {
					try {
						JSONObject object = new JSONObject(ret);
						if (object == null) {
							Logs.e(getResources().getString(R.string.msg_debug_err_response));
							Logs.e(ret);
							showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
							showKeyPad();
							return;
						}
						JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
						if (objectHead == null) {
							showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
							showKeyPad();
							return;
						}

						String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
						if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
							String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
							if (TextUtils.isEmpty(msg))
								msg = getString(R.string.common_msg_no_reponse_value_was);

							String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
							showCommonErrorDialog(msg, errCode, "", null, true);
							Logs.e(ret);
							showKeyPad();
							return;
						}

						Logs.e("ret : " + object);

						if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE){
							if(	mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP
							    ||mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL
								||mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_MOTP_SALARY) {
								Intent intent = new Intent();
								intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, mNativeOtpAuthTools);
								intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, mNativeOtpSignData);
								intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
								setResult(RESULT_OK, intent);
								finish();
								return;
							}
						}


					} catch (JSONException e) {
						Logs.printException(e);
						Logs.e(ret);
						showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
						showKeyPad();
						return;
					}
				} else {
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
					showKeyPad();
					return;
				}
			}
		});
	}

	private void requestLostRelease(final String secureData) {
		Map param = new HashMap();
		String item1 = FDSManager.getInstance().getEncIMEI(this);
		String item2 = FDSManager.getInstance().getEncUUID(this);

		param.put("DEVICE_OS", "android");
		param.put("DEVICE_INFO1", item1);
		param.put("DEVICE_INFO2", item2);
		param.put("EQMT_LOS_RVCT_NO__E2E__", secureData);
		param.put("DEVICE_INFO3", FDSManager.getInstance().getEncIMEI121(OtpPwAuthActivity.this));
		param.put("TRMN_APSF_VRSN", Utils.getVersionName(OtpPwAuthActivity.this));
		param.put("TRMN_OPSY_VRSN", Build.VERSION.RELEASE);
		param.put("CMCM_NM", Utils.getOperator(OtpPwAuthActivity.this));

		showProgressDialog();
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A07.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				dismissProgressDialog();

				if (!TextUtils.isEmpty(ret)) {
					try {
						JSONObject object = new JSONObject(ret);
						if (object == null) {
							Logs.e(getResources().getString(R.string.msg_debug_err_response));
							Logs.e(ret);
							showKeyPad();
							return;
						}
						JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
						String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
						if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
							String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
							if (TextUtils.isEmpty(msg))
								msg = getString(R.string.common_msg_no_reponse_value_was);

							String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
							showCommonErrorDialog(msg, errCode, "", null, true);
							Logs.e(ret);
							showKeyPad();
							return;
						}

						String errCount = object.optString("EQMT_LOS_RVCT_EROR_TCNT");
						if (Integer.parseInt(errCount) == 0) {
							Intent intent = new Intent(OtpPwAuthActivity.this, CompleteReportLostReleaseActivity.class);
							intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
							startActivity(intent);
							finish();
						} else {
							if (Integer.parseInt(errCount) >= 5) {
								DialogUtil.alert(OtpPwAuthActivity.this,
										getResources().getString(R.string.txt_reportlost_callcenter),
										getResources().getString(R.string.common_cancel),
										getResources().getString(R.string.msg_reportlost_callcenter),
										new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												Utils.makeCall(OtpPwAuthActivity.this, "16700042");
											}
										},
										new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												Intent intent = new Intent(OtpPwAuthActivity.this, ReportLostGuideActivity.class);
												intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
												startActivity(intent);
												finish();
											}
										});
								showKeyPad();
							} else {
								mLayoutMsg.setVisibility(View.VISIBLE);
								String msg = String.format(getString(R.string.msg_err_no_match_pin), Integer.parseInt(errCount), Const.OTP_PW_PW_AUTH_COUNT_FAIL);
								mTextMsg.setText(msg);
								showKeyPad();
								AniUtils.shakeView(mLayoutMsg);
							}
						}
					} catch (JSONException e) {
						Logs.printException(e);
					}
				} else {
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					showKeyPad();
				}
			}
		});
	}

	private void showReregLostMotp() {
		Intent intent = new Intent(OtpPwAuthActivity.this, WebMainActivity.class);
		String url = WasServiceUrl.CRT0040401.getServiceUrl();
		intent.putExtra("url", url);
		/*
		Map param = new HashMap();
		param.put("TRTM_DVCD", "1");
		intent.putExtra(Const.INTENT_PARAM, param.toString());
		*/
		mLayoutMsg.setVisibility(View.INVISIBLE);
		startActivityForResult(intent, MOTP_REREG_REQCODE);
	}

	private void showReregLostPw() {
		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {

		} else {
			Intent intent = new Intent();
			mCommonUserInfo.setOtpForgetAuthNo(true);
			mCommonUserInfo.setSecureData("");
			mCommonUserInfo.setPlainData("");
			mCommonUserInfo.setDummyData("");
			mCommonUserInfo.setCipherData("");
			intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
			setResult(RESULT_OK, intent);
			finish();
		}
	}
}