package com.sbi.saidabank.activity.transaction.safedeal;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.dialog.KCBComfirmDialog;
import com.sbi.saidabank.common.dialog.KCBProgressDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * 받는분 전화번호 입력 슬라이드 화면
 */
public class SlideReceiverPhoneNumLayout extends SlideBaseLayout implements CustomNumPadLayout.OnNumPadClickListener,View.OnClickListener{
    private static final String TAG = SlideReceiverPhoneNumLayout.class.getSimpleName();

    private TextView mTvReciever;
    private EditText mEtPhoneNum;
    private Button mBtnOk;


    private boolean mIsOpenContactList;
    private String  mRecieverName;

    private KCBProgressDialog mKCBProgressDialog;

    public SlideReceiverPhoneNumLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideReceiverPhoneNumLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @Override
    public void clearData(){
        mTvReciever.setText("");
        mEtPhoneNum.setText("");
        mRecieverName = "";
        mIsOpenContactList = false;
        mKCBProgressDialog = null;
    }

    private void initUX(Context context){
        setTag("SlideReceiverPhoneNumLayout");

        View layout = View.inflate(context, R.layout.layout_safedeal_slide_receiver_phone_num, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);

        mTvReciever = layout.findViewById(R.id.tv_reciver_name);

        layout.findViewById(R.id.iv_contact).setOnClickListener(this);


        mEtPhoneNum = layout.findViewById(R.id.et_phone_num);
        mEtPhoneNum.setTextIsSelectable(true);
        mEtPhoneNum.setShowSoftInputOnFocus(false);
        mEtPhoneNum.requestFocus();

        mEtPhoneNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() >= 10) {
                    mBtnOk.setEnabled(true);
                } else{
                    mBtnOk.setEnabled(false);
                }
            }
        });

        mEtPhoneNum.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    Utils.hideKeyboard(getContext(), mEtPhoneNum);
                }
                return false;
            }
        });



        CustomNumPadLayout numPadLayout = layout.findViewById(R.id.numpad);
        numPadLayout.setNumPadClickListener(this);


        //입력창 레이아웃
        RelativeLayout editTextLayout = layout.findViewById(R.id.layout_edittext);
        int radius_et = (int) Utils.dpToPixel(getContext(),7f);
        editTextLayout.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF5F5F5),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5)));

        //확인버튼
        mBtnOk = layout.findViewById(R.id.btn_ok);
        mBtnOk.setOnClickListener(this);

        initLayoutSetBottom(true,false);
    }


    @Override
    public void onNumPadClick(String numStr) {


        switch (numStr){
            case "cancel": {
                mEtPhoneNum.setText("");
                break;
            }
            case "del": {
                deleteString();
                break;
            }
            default: {
                if(mEtPhoneNum.getText().toString().length() >= 11) return;
                setNumberString(numStr);
                break;
            }
        }
    }

    @Override
    public void onCancel() {

        if(mIsOpenContactList) return;

        super.onCancel();

        slidingDown(250,0);
        mActionlistener.onActionBack(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_contact:
                mActionlistener.onActionSearchPhoneNum();
                mIsOpenContactList=true;
                 break;
            case R.id.btn_ok:
                requestAuthKCB();
                break;
        }
    }

    private void deleteString(){
        int cursor_start = mEtPhoneNum.getSelectionStart();
        int cursor_end = mEtPhoneNum.getSelectionEnd();

        String inputText = mEtPhoneNum.getText().toString();

        String frontStr = inputText.substring(0,cursor_start);
        String backStr = inputText.substring(cursor_end,inputText.length());

         if(cursor_start == cursor_end){
             if(cursor_start == 0) return;

             String newFrontStr = frontStr.substring(0,cursor_start-1);
             mEtPhoneNum.setText(newFrontStr+backStr);
             mEtPhoneNum.setSelection(newFrontStr.length());
         }else{
             mEtPhoneNum.setText(frontStr+backStr);
             mEtPhoneNum.setSelection(frontStr.length());
         }
    }

    private void setNumberString(String numStr){
        int cursor_start = mEtPhoneNum.getSelectionStart();
        int cursor_end = mEtPhoneNum.getSelectionEnd();

        String inputText = mEtPhoneNum.getText().toString();

        String frontStr = inputText.substring(0,cursor_start);
        String backStr = inputText.substring(cursor_end,inputText.length());

        String newStr = frontStr + numStr + backStr;

        mEtPhoneNum.setText(newStr);
        mEtPhoneNum.setSelection(frontStr.length() + 1);
    }

    public String getRecieverName(){
        return mRecieverName;
    }

    public void setRecieverName(String name){
        mTvReciever.setText(name+"님 연락처");
        mRecieverName = name;
    }

    public String getPhoneNum(){
        return mEtPhoneNum.getText().toString();
    }

    public void setPhoneNum(String phoneNum){
        if(TextUtils.isEmpty(phoneNum)) return;

        String replacePhoneNum = phoneNum.replace(" ","");
        mEtPhoneNum.setText(replacePhoneNum);
        mEtPhoneNum.setSelection(replacePhoneNum.length());
        mEtPhoneNum.requestFocus();
    }

    public void setIsOpenContactList(boolean flag){
        mIsOpenContactList = flag;
    }



    private void requestAuthKCB(){
        MLog.d();
        mKCBProgressDialog = new KCBProgressDialog(getContext(), new KCBProgressDialog.OnKCBResultListener() {
            @Override
            public void onKCBResult(boolean result,String errorCode, String errorMsg) {
                if(result){
                    String msg = getContext().getString(R.string.dialog_msg_kcb_comfirm,mRecieverName);
                    KCBComfirmDialog kcbComfirmDialog = new KCBComfirmDialog(getContext(),msg);
                    kcbComfirmDialog.mPListener = new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {
                            mActionlistener.onActionNext(mSlideLayout,null);
                            TransferSafeDealDataMgr.getInstance().setMNRC_CPNO(mEtPhoneNum.getText().toString());
                        }
                    };
                    kcbComfirmDialog.show();
                }else{
                    String msg = "";
                    if(errorCode.equals("V099")){
                        msg = getContext().getString(R.string.dialog_msg_kcb_error_v099);
                    }else{
                        msg = getContext().getString(R.string.dialog_msg_kcb_error_vxxx);
                    }
                    KCBComfirmDialog kcbComfirmDialog = new KCBComfirmDialog(getContext(),msg);
                    kcbComfirmDialog.mPListener = new View.OnClickListener(){
                        @Override
                        public void onClick(View v) {

                        }
                    };
                    kcbComfirmDialog.show();
                }
            }
        });
        mKCBProgressDialog.show();

        HashMap map = new HashMap();
        map.put("CUST_NM",mRecieverName);
        map.put("MNRC_CPNO",mEtPhoneNum.getText().toString());
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0090100A02.getServiceUrl(), map, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if (TextUtils.isEmpty(ret)) {
                    mKCBProgressDialog.setRequestEnd(false,"V999",getContext().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    if (DataUtil.isNull(object)) {
                        mKCBProgressDialog.setRequestEnd(false,"V999",getContext().getString(R.string.msg_debug_err_response));
                        return;
                    }

                    String kcbRespCd = object.optString("KCB_RESP_CD");
                    if(kcbRespCd.equals("V000"))
                        mKCBProgressDialog.setRequestEnd(true,"","");
                    else{
                        mKCBProgressDialog.setRequestEnd(false,kcbRespCd,"");
                    }

                } catch (JSONException e) {

                    MLog.e(e);
                }
            }
        });

    }
}
