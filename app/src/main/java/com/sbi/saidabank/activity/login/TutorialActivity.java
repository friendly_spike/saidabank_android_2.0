package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.adapter.TutorialAdapter;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.v3.V3Manager;

import java.util.ArrayList;

/**
 * TutorialActivity : Tutorial 화면
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TutorialActivity extends BaseActivity {

    private static final int DEVICE_STATE_NORMAL = 0;
    private static final int DEVICE_STATE_NEWUSER = 1;

    private ViewPager mViewPager;
    private Button mBtnConfirm;
//    private ImageButton mBtnLeft;
//    private ImageButton mBtnRight;

    private ArrayList<ImageView> mIndicatorList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);

        mViewPager = (ViewPager) findViewById(R.id.viewpager_tutorial);
        TutorialAdapter adapter = new TutorialAdapter(TutorialActivity.this);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                MLog.d();
            }

            @Override
            public void onPageSelected(int position) {
                changeIndicatorStatus(position);

                if (position >= 3) {
                    if (mBtnConfirm.getVisibility() == View.VISIBLE)
                        return;
                    Animation animation = AnimationUtils.loadAnimation(TutorialActivity.this, R.anim.sliding_in_bottom_confirm);
                    mBtnConfirm.setVisibility(View.INVISIBLE);
                    mBtnConfirm.clearAnimation();
                    mBtnConfirm.startAnimation(animation);
                    mBtnConfirm.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            MLog.d();
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            MLog.d();
                            mBtnConfirm.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                            MLog.d();
                        }
                    });
                } else {
                    if (mBtnConfirm.getVisibility() == View.INVISIBLE)
                        return;
                    Animation animation = AnimationUtils.loadAnimation(TutorialActivity.this, R.anim.sliding_out_bottom_confirm);
                    mBtnConfirm.setVisibility(View.INVISIBLE);
                    mBtnConfirm.clearAnimation();
                    mBtnConfirm.startAnimation(animation);
                    mBtnConfirm.getAnimation().setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                            MLog.d();
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            MLog.d();
                            mBtnConfirm.setVisibility(View.INVISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {
                            MLog.d();
                        }
                    });
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                MLog.d();
            }
        });

        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mBtnConfirm.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Prefer.setTutorialStatus(TutorialActivity.this, true);
                int deviceState = getIntent().getIntExtra("device_state", DEVICE_STATE_NORMAL);
                if (deviceState != DEVICE_STATE_NORMAL) {
                    Prefer.setPatternRegStatus(TutorialActivity.this, false);
                    Prefer.setPincodeRegStatus(TutorialActivity.this, false);
                    Prefer.setFidoRegStatus(TutorialActivity.this, false);
                    Prefer.setFlagFingerPrintChange(TutorialActivity.this, false);
                    EntryPoint curEntryPoint = (deviceState == DEVICE_STATE_NEWUSER) ? EntryPoint.SERVICE_JOIN : EntryPoint.DEVICE_CHANGE;
                    CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.DEVICE_CHANGE, curEntryPoint);
                    Intent intent = new Intent(TutorialActivity.this, SplashActivity.class);
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivity(intent);
                    finish();
                } else {
                    boolean status = Prefer.getFidoRegStatus(TutorialActivity.this);
                    CommonUserInfo commonUserInfo = new CommonUserInfo();
                    commonUserInfo.setEntryStart(status ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
                    commonUserInfo.setEntryPoint(status ? EntryPoint.LOGIN_BIO : EntryPoint.LOGIN_PATTERN);
                    commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                    commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                    Intent intent = new Intent(TutorialActivity.this, PatternAuthActivity.class);
                    if (getIntent().hasExtra(Const.INTENT_MAINWEB_URL))
                        intent.putExtra(Const.INTENT_MAINWEB_URL, getIntent().getStringExtra(Const.INTENT_MAINWEB_URL));
                    intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                    startActivity(intent);
                    finish();
                }
            }
        });


        mIndicatorList.clear();
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_01));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_02));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_03));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_04));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_05));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_06));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_07));
        mIndicatorList.add((ImageView) findViewById(R.id.imageview_tutorial_08));
        changeIndicatorStatus(0);
    }

    @Override
    public void onBackPressed() {
        V3Manager.getInstance(getApplicationContext()).stopV3MobilePlus();
        ActivityCompat.finishAffinity(TutorialActivity.this);
        System.exit(0);
    }

    private void changeIndicatorStatus(int position) {
        for (int i = 0; i < mIndicatorList.size(); i++) {
            if(i == position){
                mIndicatorList.get(i).setImageResource(R.drawable.icon_tutorial_pageindicator_on);
            }else{
                mIndicatorList.get(i).setImageResource(R.drawable.icon_tutorial_pageindicator_off);
            }

            //mIndicatorList.get(i).setSelected((i == position));
        }
    }

}
