package com.sbi.saidabank.activity.transaction;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TransferSafeDealCompleteActivity extends BaseActivity {
    ImageView mIvIconFav;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MLog.d();
        setContentView(R.layout.activity_transfer_safe_deal_complete);

        TextView tvTitleMsg1 = findViewById(R.id.tv_title_msg_1);
        TextView tvTitleMsg2 = findViewById(R.id.tv_title_msg_2);

        int sdKind = TransferSafeDealDataMgr.getInstance().getSAFE_TRN_KNCD();
        String expireDate = getExpireDateStr(sdKind);
        tvTitleMsg1.setText(expireDate + "\n동의 시 이체가 처리됩니다.");
        tvTitleMsg2.setText(TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM() + "님에게 안심이체 승인을 요청하였습니다.");
        switch (sdKind){
            case Const.SAFEDEAL_TYPE_MOENY:
                //tvTitleMsg2.setText("금전거래 신청이 완료되었습니다.");
                ((TextView)findViewById(R.id.tv_deal_kind)).setText("안심이체(금전거래)");
                break;
            case Const.SAFEDEAL_TYPE_PROPERTY:
                //tvTitleMsg2.setText("부동산거래 신청이 완료되었습니다.");
                ((TextView)findViewById(R.id.tv_deal_kind)).setText("안심이체(부동산거래)");
                break;
            case Const.SAFEDEAL_TYPE_GOODS:
                //tvTitleMsg2.setText("물품거래 신청이 완료되었습니다.");
                ((TextView)findViewById(R.id.tv_deal_kind)).setText("안심이체(물거래)");
                break;
        }

        dispFavorite();
        initItemUX(sdKind);
        dispBallencMoney();

        ((Button)findViewById(R.id.btn_confirm)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(TransferSafeDealCompleteActivity.this, WebMainActivity.class);
//                intent.putExtra("url", WasServiceUrl.TRA0090100.getServiceUrl());
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        ((Button)findViewById(R.id.btn_confirm)).performClick();
    }

    private void dispFavorite(){
        mIvIconFav = findViewById(R.id.iv_favorite);
        String retStr = TransferSafeDealDataMgr.getInstance().getRET_COMPLET_STR();
        if(!TextUtils.isEmpty(retStr)){
            try {
                JSONObject object = new JSONObject(retStr);
                String favo_acco_yn = object.optString("FAVO_ACCO_YN");
                mIvIconFav.setTag(favo_acco_yn);
                if(favo_acco_yn.equals("Y")){
                    mIvIconFav.setImageResource(R.drawable.btn_favorite_on);
                }else{
                    mIvIconFav.setImageResource(R.drawable.btn_favorite_off);
                }

                mIvIconFav.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestFavoriteAccount();
                    }
                });

            } catch (JSONException e) {
                Logs.printException(e);
            }

        }
    }

    private void initItemUX(int kind){
        TransferSafeDealDataMgr info = TransferSafeDealDataMgr.getInstance();

        //거래목적
        if(kind == Const.SAFEDEAL_TYPE_GOODS){
            ((TextView)findViewById(R.id.tv_deal_reason)).setText("물품거래");
            //((TextView)findViewById(R.id.tv_deal_reason)).setText(info.getSAFE_TRN_RSN_CNTN());
        }else{
            ((TextView)findViewById(R.id.tv_deal_reason)).setText(info.getSAFE_TRN_RSN_CNTN());
        }


        //부동산거래 주소
        if(kind == Const.SAFEDEAL_TYPE_PROPERTY){
            findViewById(R.id.ll_property_address).setVisibility(View.VISIBLE);
            ((TextView)findViewById(R.id.tv_address)).setText(info.getLCTN_ADDR() + " " + info.getLCTN_DTL_ADDR());
            //((TextView)findViewById(R.id.tv_address_detail)).setText(info.getLCTN_DTL_ADDR());
        }else if(kind == Const.SAFEDEAL_TYPE_GOODS){
            //물품거래는 부동산 주소 영역에 출력하도록 한다.
            ((TextView)findViewById(R.id.property_title)).setText("거래물품");
            ((TextView)findViewById(R.id.tv_address)).setText(info.getSAFE_TRN_RSN_CNTN());
        }else{
            findViewById(R.id.ll_property_address).setVisibility(View.GONE);
        }


        ((TextView)findViewById(R.id.tv_rcv_name)).setText(info.getMNRC_ACCO_DEPR_NM());

        if (!TextUtils.isEmpty(info.getTRAM())) {
            String tram = Utils.moneyFormatToWon(Double.valueOf(info.getTRAM()));
            ((TextView)findViewById(R.id.tv_tr_amount)).setText(tram+" 원");
        }

        if (!TextUtils.isEmpty(info.getTRNF_FEE())) {
            if(info.getTRNF_FEE().equals("0")){
                ((TextView)findViewById(R.id.tv_fee)).setText("2,000 원(면제)");
            }else{
                String trFee = Utils.moneyFormatToWon(Double.valueOf(info.getTRNF_FEE()));
                ((TextView)findViewById(R.id.tv_fee)).setText(trFee+" 원");
            }
        }

        ((TextView)findViewById(R.id.tv_rcv_bank)).setText(info.getMNRC_BANK_NAME());

        if (!TextUtils.isEmpty(info.getMNRC_ACNO())) {
            String CNTP_BANK_ACNO = info.getMNRC_ACNO();
            String CNTP_FIN_INST_CD = info.getMNRC_BANK_CD();
            if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD) || "028".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                String account = CNTP_BANK_ACNO.substring(0, 5) + "-" + CNTP_BANK_ACNO.substring(5, 7) + "-" + CNTP_BANK_ACNO.substring(7, CNTP_BANK_ACNO.length());
                ((TextView)findViewById(R.id.tv_rcv_account)).setText(account);
            } else {
                ((TextView)findViewById(R.id.tv_rcv_account)).setText(CNTP_BANK_ACNO);
            }
        }

        ((TextView)findViewById(R.id.tv_rcv_phone_num)).setText(info.getMNRC_CPNO());
        ((TextView)findViewById(R.id.tv_rcv_memo)).setText(LoginUserInfo.getInstance().getCUST_NM());
        ((TextView)findViewById(R.id.tv_send_memo)).setText(info.getMNRC_ACCO_DEPR_NM());

    }

    private void dispBallencMoney(){
        String retStr = TransferSafeDealDataMgr.getInstance().getRET_COMPLET_STR();
        if(!TextUtils.isEmpty(retStr)){
            try {
                JSONObject object = new JSONObject(retStr);
                String acco_als = object.optString("ACCO_ALS");
                String prod_nm  = object.optString("PROD_NM");

                if (DataUtil.isNull(acco_als)) {
                    acco_als = prod_nm;
                }
                String ACNO = TransferSafeDealDataMgr.getInstance().getWTCH_ACNO();
                if (DataUtil.isNotNull(ACNO)) {
                    int lenACNO = ACNO.length();
                    if (lenACNO > 4) {
                        ACNO = ACNO.substring(lenACNO - 4, lenACNO);
                    }
                    ACNO = " [" + ACNO + "]";
                }
                String mWithdrawName = acco_als + ACNO;
                ((TextView)findViewById(R.id.tv_account_name)).setText(mWithdrawName);

                String ACCO_BLNC = object.optString("ACCO_BLNC");
                if(!TextUtils.isEmpty(ACCO_BLNC)){
                    ACCO_BLNC = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
                    ((TextView)findViewById(R.id.tv_balance)).setText(ACCO_BLNC);
                }


                String WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");
                if(!TextUtils.isEmpty(WTCH_POSB_AMT)){
                    WTCH_POSB_AMT = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    ((TextView)findViewById(R.id.tv_withdrawbleamount)).setText(WTCH_POSB_AMT);
                }


            } catch (JSONException e) {
                Logs.printException(e);
            }

        }
    }

    private String getExpireDateStr(int kind){
        String retStr = TransferSafeDealDataMgr.getInstance().getRET_COMPLET_STR();
        if(!TextUtils.isEmpty(retStr)){
            try {
                JSONObject object = new JSONObject(retStr);
                String expireDate = object.optString("MNRC_AGR_END_DTTM");
                if(!TextUtils.isEmpty(expireDate) && expireDate.length() >= 12){
                    String year = expireDate.substring(0, 4);
                    String month = expireDate.substring(4, 6);
                    String day = expireDate.substring(6, 8);
                    String hour = expireDate.substring(8, 10);
                    String min = expireDate.substring(10, 12);


                    return month +"월"+day + "일 " + hour + "시" + min + "분까지";
                }
            } catch (JSONException e) {
                Logs.printException(e);
            }

        }
        return "";
    }

    /**
     * 즐겨찾기계좌관리 요청
     */
    private void requestFavoriteAccount() {
        MLog.d();

        String FAVO_ACCO_YN = (String)mIvIconFav.getTag();
        String CNTP_FIN_INST_CD = TransferSafeDealDataMgr.getInstance().getMNRC_BANK_CD();//상대은행 코드
        String CNTP_BANK_ACNO = TransferSafeDealDataMgr.getInstance().getMNRC_ACNO(); //상대은행계좌
        String CNTP_ACCO_DEPR_NM = TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM();//상대계좌 예금주

        if (TextUtils.isEmpty(FAVO_ACCO_YN) || TextUtils.isEmpty(CNTP_FIN_INST_CD) ||
                TextUtils.isEmpty(CNTP_BANK_ACNO) || TextUtils.isEmpty(CNTP_ACCO_DEPR_NM))
            return;

        Map param = new HashMap();
        if (FAVO_ACCO_YN.equals("Y"))
            param.put("REG_STCD", "1");
        else
            param.put("REG_STCD", "0");

        param.put("CNTP_FIN_INST_CD", CNTP_FIN_INST_CD);
        param.put("CNTP_BANK_ACNO", CNTP_BANK_ACNO);
        param.put("CNTP_ACCO_DEPR_NM", CNTP_ACCO_DEPR_NM);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    String FAVO_ACCO_YN = (String)mIvIconFav.getTag();
                    if(FAVO_ACCO_YN.equals("Y")){
                        mIvIconFav.setImageResource(R.drawable.btn_favorite_off);
                        mIvIconFav.setTag("N");
                    }else{
                        mIvIconFav.setImageResource(R.drawable.btn_favorite_on);
                        mIvIconFav.setTag("Y");
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}
