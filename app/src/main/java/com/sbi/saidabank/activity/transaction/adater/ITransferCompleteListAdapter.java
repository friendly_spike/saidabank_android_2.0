package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;

import java.util.ArrayList;

/**
 * Saidabanking_android
 * <p>
 * Class: MultiTransferListAdapter
 * Created by 950485 on 2018. 11. 21..
 * <p>
 * Description: 계좌번호 다건이체 완료 후 화면 리스트 어댑터
 */

public class ITransferCompleteListAdapter extends RecyclerView.Adapter<ITransferCompleteListAdapter.ReceiptInfoListViewHolder> {
    private Context mContext;

    private ArrayList<TransferReceiptInfo> mListTransferReceiptInfos;
    private FavoriteListener favoriteListener;
    private ShareListener shareListener;

    public ITransferCompleteListAdapter(Context context, ArrayList<TransferReceiptInfo> listTransferInfo, FavoriteListener favoriteListener, ShareListener shareListener) {
        this.mContext = context;
        this.mListTransferReceiptInfos = listTransferInfo;
        this.favoriteListener = favoriteListener;
        this.shareListener = shareListener;
    }

    public class ReceiptInfoListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView       textName;
        public ImageView      ivShare;
        public ImageView      ivFavorite;
        public TextView       textError;
        public TextView       textAmount;
        public TextView       textFee;
        public TextView       textbank;
        public TextView       textAccount;
        public TextView       textDisplayRecipient;
        public TextView       textDisplaySeding;
        public TextView       textTitleSendDate;
        public TextView       textSendDate;
        public RelativeLayout layoutSendDate;
        public View           vLine;
        public ReceiptInfoListViewHolder(Context context, View view, int viewType) {
            super(view);

            textName = (TextView) view.findViewById(R.id.tv_accountname);
            textError = (TextView) view.findViewById(R.id.tv_transfer_error);
            ivShare = (ImageView) view.findViewById(R.id.iv_share);
            ivShare.setOnClickListener(this);
            ivFavorite = (ImageView) view.findViewById(R.id.iv_favorite);
            ivFavorite.setOnClickListener(this);
            textAmount = (TextView) view.findViewById(R.id.tv_transferamount);
            textFee = (TextView) view.findViewById(R.id.tv_fee);
            textbank = (TextView) view.findViewById(R.id.tv_receivedbank);
            textAccount = (TextView) view.findViewById(R.id.tv_receivedaccount);
            textDisplayRecipient = (TextView) view.findViewById(R.id.tv_displayrecipient);
            textDisplaySeding = (TextView) view.findViewById(R.id.tv_displaysending);
            textTitleSendDate = (TextView) view.findViewById(R.id.tv_title_senddate);
            textSendDate = (TextView) view.findViewById(R.id.tv_senddate);
            layoutSendDate = (RelativeLayout) view.findViewById(R.id.ll_senddate);
            vLine = view.findViewById(R.id.v_line);
        }

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if (view == ivShare) {
                shareListener.OnShareListener(position);
            } else if (view == ivFavorite) {
                favoriteListener.OnFavoriteListener(position);
            }
        }
    }

    @Override
    public ITransferCompleteListAdapter.ReceiptInfoListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_transfer_complete_item, parent, false);
        ReceiptInfoListViewHolder viewHolder = new ReceiptInfoListViewHolder(mContext, itemView, viewType);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ReceiptInfoListViewHolder viewHolder, int position) {
        viewHolder.ivShare.setTag(position);
        viewHolder.ivFavorite.setTag(position);

        TransferReceiptInfo listTransferReceiptInfo = mListTransferReceiptInfos.get(position);
        if (listTransferReceiptInfo == null)
            return;

        String FAVO_ACCO_YN = listTransferReceiptInfo.getFAVO_ACCO_YN();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN)) {
            viewHolder.ivFavorite.setImageResource(R.drawable.btn_favorite_on);
        } else {
            viewHolder.ivFavorite.setImageResource(R.drawable.btn_favorite_off);
        }

        String CNTP_ACCO_DEPR_NM = listTransferReceiptInfo.getCNTP_ACCO_DEPR_NM();
        if (!TextUtils.isEmpty(CNTP_ACCO_DEPR_NM))
            viewHolder.textName.setText(CNTP_ACCO_DEPR_NM);

        String RESP_CD = listTransferReceiptInfo.getRESP_CD();
        if ("NEFN0000".equalsIgnoreCase(RESP_CD) ||
            "XEKM0000".equalsIgnoreCase(RESP_CD)) {
            viewHolder.ivShare.setVisibility(View.VISIBLE);
            viewHolder.ivFavorite.setVisibility(View.VISIBLE);
            viewHolder.textError.setVisibility(View.GONE);
        } else {
            String RESP_CNTN = listTransferReceiptInfo.getRESP_CNTN();
            if (!TextUtils.isEmpty(RESP_CNTN)) {
                viewHolder.textError.setText(RESP_CNTN);
            }

            viewHolder.ivShare.setVisibility(View.GONE);
            viewHolder.ivFavorite.setVisibility(View.GONE);
            if (mListTransferReceiptInfos.size() > 1)
                viewHolder.textError.setVisibility(View.VISIBLE);
        }

        //다건에서는 즐겨찾기 보이지 않아야 한다. 그룹으로 대처.
        if(mListTransferReceiptInfos.size() > 1){
            viewHolder.ivFavorite.setVisibility(View.GONE);
        }

        String TRN_AMT = listTransferReceiptInfo.getTRN_AMT();
        if (!TextUtils.isEmpty(TRN_AMT)) {
            TRN_AMT = Utils.moneyFormatToWon(Double.valueOf(TRN_AMT));
            viewHolder.textAmount.setText(TRN_AMT);
        }

        String TRN_FEE = listTransferReceiptInfo.getTRN_FEE();
        if (!TextUtils.isEmpty(TRN_FEE)) {
            TRN_FEE = Utils.moneyFormatToWon(Double.valueOf(TRN_FEE));
            viewHolder.textFee.setText(TRN_FEE + " " + mContext.getString(R.string.won));
        }

        String MNRC_BANK_NM = listTransferReceiptInfo.getMNRC_BANK_NM();
        if (!TextUtils.isEmpty(MNRC_BANK_NM)) {
            viewHolder.textbank.setText(MNRC_BANK_NM);
        }

        String CNTP_BANK_ACNO = listTransferReceiptInfo.getCNTP_BANK_ACNO();
        if (!TextUtils.isEmpty(CNTP_BANK_ACNO)) {
            String CNTP_FIN_INST_CD = listTransferReceiptInfo.getCNTP_FIN_INST_CD();
            if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD) || "028".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                String account = CNTP_BANK_ACNO.substring(0, 5) + "-" + CNTP_BANK_ACNO.substring(5, 7) + "-" + CNTP_BANK_ACNO.substring(7, CNTP_BANK_ACNO.length());
                viewHolder.textAccount.setText(account);
            } else {
                viewHolder.textAccount.setText(CNTP_BANK_ACNO);
            }
        }

        String WTCH_ACCO_MRK_CNTN = listTransferReceiptInfo.getWTCH_ACCO_MRK_CNTN();
        if (!TextUtils.isEmpty(WTCH_ACCO_MRK_CNTN))
            viewHolder.textDisplaySeding.setText(WTCH_ACCO_MRK_CNTN);

        String MNRC_ACCO_MRK_CNTN = listTransferReceiptInfo.getMNRC_ACCO_MRK_CNTN();
        if (!TextUtils.isEmpty(MNRC_ACCO_MRK_CNTN))
            viewHolder.textDisplayRecipient.setText(MNRC_ACCO_MRK_CNTN);


        String TRNF_DVCD = listTransferReceiptInfo.getTRNF_DVCD();
        if (mListTransferReceiptInfos.size() == 1) {
            if ("1".equalsIgnoreCase(TRNF_DVCD)) {
                viewHolder.layoutSendDate.setVisibility(View.GONE);
                viewHolder.textSendDate.setText(mContext.getString(R.string.Immediately));
            } else if ("2".equalsIgnoreCase(TRNF_DVCD)) {
                viewHolder.textTitleSendDate.setText(mContext.getString(R.string.send_time));
                viewHolder.textSendDate.setText(mContext.getString(R.string.msg_after_3_hour));
                
            } else if ("3".equalsIgnoreCase(TRNF_DVCD)) {
                String TRNF_DT = listTransferReceiptInfo.getTRNF_DT();
                String TRNF_MT = listTransferReceiptInfo.getTRNF_TM();
                if (!TextUtils.isEmpty(TRNF_DT) && !TextUtils.isEmpty(TRNF_MT) ) {
                    String year = TRNF_DT.substring(0, 4);
                    String month = TRNF_DT.substring(4, 6);
                    String day = TRNF_DT.substring(6, 8);
                    String time = TRNF_MT.substring(0, 2);

                    String date = year + "년 " + month + "월 " + day + "일 " + time + "시";
                    viewHolder.textSendDate.setText(date);
                }
            }
        } else {
            if (!"1".equals(TRNF_DVCD)) {
                String TRNF_DT = listTransferReceiptInfo.getTRNF_DT();
                String TRNF_MT = listTransferReceiptInfo.getTRNF_TM();
                if (!TextUtils.isEmpty(TRNF_DT) && !TextUtils.isEmpty(TRNF_MT) ) {
                    String year = TRNF_DT.substring(0, 4);
                    String month = TRNF_DT.substring(4, 6);
                    String day = TRNF_DT.substring(6, 8);
                    String time = TRNF_MT.substring(0, 2);

                    String date = year + "년" + month + "월" + day + "일" + " " + time + "시";
                    viewHolder.textSendDate.setText(date);
                }
            } else {
                viewHolder.textSendDate.setText(mContext.getString(R.string.Immediately));
            }
        }
    }

    public void setListTransferReceiptInfos(ArrayList<TransferReceiptInfo> listTransferReceiptInfos) {
        mListTransferReceiptInfos = listTransferReceiptInfos;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mListTransferReceiptInfos.size();
    }

    public interface FavoriteListener {
        void OnFavoriteListener(int position);
    }

    public interface ShareListener {
        void OnShareListener(int position);
    }
}
