package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: OtpPwAuthActivity
 * Created by 950546 on 2019. 02. 19..
 * <p>
 * Description: 타기관OTP 인증을 위한 화면
 */

public class OtherOtpAuthActivity extends BaseActivity implements View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener {
	private static int  mKeypadType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;
	private static final long MIN_CLICK_INTERVAL = 500;    // ms

	private LinearLayout  mLayoutMsg;
	private TextView      mTextMsg;
	private EditText      mExitOtpPw;
	private EditText      mEditOtpPw01;
	private ImageView     mImageOtpPw01;
	private EditText      mEditOtpPw02;
	private ImageView     mImageOtpPw02;
	private EditText      mEditOtpPw03;
	private ImageView     mImageOtpPw03;
	private EditText      mEditOtpPw04;
	private ImageView     mImageOtpPw04;
	private EditText      mEditOtpPw05;
	private ImageView     mImageOtpPw05;
	private EditText      mEditOtpPw06;
	private ImageView     mImageOtpPw06;

	private int[]         mArrayNumber = new int[10];
	private boolean       mIsViewCtrlKeypad = false;
	private String        mNativeOtpAuthTools;
	private String        mNativeOtpSignData;
	private long 		  mLastClickTime;

	private CommonUserInfo mCommonUserInfo;

	private TransKeyCtrl  mTKMgr;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_other_otp_pw_auth);

		getExtra();
        initUX();
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    	if (mIsViewCtrlKeypad == false) {
	    		showKeyPad();
				return true;
		    }
    	}
		return false;
	}

	@Override
	public void cancel(Intent data) {
		finish();
	}

	@Override
	public void done(Intent data) {
		mIsViewCtrlKeypad = false;
		if (data == null)
			return;

		long currentClickTime = SystemClock.uptimeMillis();
		long elapsedTime = currentClickTime - mLastClickTime;
		mLastClickTime = currentClickTime;

		if (elapsedTime <= MIN_CLICK_INTERVAL) {
			// 중복클릭 방지
			return;
		}

		String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);
		String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
		String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
		byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
		int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

		if (iRealDataLength != mCommonUserInfo.getOtpPWLength()) {
			String msg = getString(R.string.message_pin_right_length, mCommonUserInfo.getOtpPWLength());
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(msg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		String plainData = "";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(secureKey);

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "";
			}

			if (TextUtils.isEmpty(plainData)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_decode_pin);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}

        } catch (Exception e) {
			Logs.printException(e);
		}

		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
			if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_AUTH_OTP || mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_OTP) {
				requestAuthOtp(plainData);
			}
		} else {
			//입력된 비밀번호를 자바스크립트 콜백으로 리턴
			Intent intent = new Intent();
			mCommonUserInfo.setSecureData(secureData);
			mCommonUserInfo.setPlainData(plainData);
			mCommonUserInfo.setDummyData(dummyData);
			mCommonUserInfo.setCipherData(cipherData);
			mCommonUserInfo.setOtpForgetAuthNo(false);
			intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
			setResult(RESULT_OK, intent);
			finish();
			overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
		}
	}
	
	@Override
	public void onBackPressed() {
		setResult(RESULT_CANCELED);
		super.onBackPressed();
		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB)
			overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
	}
	
	@Override
	public void input(int arg0) {
		if (arg0 == ITransKeyActionListenerEx.INPUT_CHARACTER_KEY) {
			mLayoutMsg.setVisibility(View.INVISIBLE);

			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();

            if (TextUtils.isEmpty(strValue1)) {
				mImageOtpPw01.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(1))]);
				mEditOtpPw01.setText("0");
				mEditOtpPw02.requestFocus();
			} else if (TextUtils.isEmpty(strValue2)) {
				mImageOtpPw02.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(2))]);
				mEditOtpPw02.setText("0");
				mEditOtpPw03.requestFocus();
			} else if (TextUtils.isEmpty(strValue3)) {
				mImageOtpPw03.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(3))]);
				mEditOtpPw03.setText("0");
				mEditOtpPw04.requestFocus();
			} else if (TextUtils.isEmpty(strValue4)) {
				mImageOtpPw04.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(4))]);
				mEditOtpPw04.setText("0");
				if (mCommonUserInfo.getOtpPWLength() == 5)
					mEditOtpPw05.requestFocus();
			} else if (TextUtils.isEmpty(strValue5)) {
				mImageOtpPw05.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(5))]);
				mEditOtpPw05.setText("0");
				if (mCommonUserInfo.getOtpPWLength() == 6)
					mEditOtpPw06.requestFocus();
			} else {
				mImageOtpPw06.setImageResource(mArrayNumber[Integer.parseInt(getPlainData(6))]);
				mEditOtpPw06.setText("0");
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_BACKSPACE_KEY) {
			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();
			String strValue6 = mEditOtpPw06.getText().toString();

			if (mCommonUserInfo.getOtpPWLength() == 6) {
				if (!TextUtils.isEmpty(strValue6)) {
					mEditOtpPw06.setText("");
					mImageOtpPw06.setImageResource(R.drawable.ico_otp_num_off);
					mEditOtpPw05.requestFocus();
					return;
				} else {
					if (!TextUtils.isEmpty(strValue5)) {
						mEditOtpPw05.setText("");
						mImageOtpPw05.setImageResource(R.drawable.ico_otp_num_off);
						mEditOtpPw04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getOtpPWLength() == 5) {
				if (!TextUtils.isEmpty(strValue5)) {
					mEditOtpPw05.setText("");
					mImageOtpPw05.setImageResource(R.drawable.ico_otp_num_off);
					mEditOtpPw04.requestFocus();
					return;
				}
			}

			if (!TextUtils.isEmpty(strValue4)) {
				mEditOtpPw04.setText("");
				mImageOtpPw04.setImageResource(R.drawable.ico_otp_num_off);
				mEditOtpPw03.requestFocus();
			} else if (!TextUtils.isEmpty(strValue3)) {
				mEditOtpPw03.setText("");
				mImageOtpPw03.setImageResource(R.drawable.ico_otp_num_off);
				mEditOtpPw02.requestFocus();
			} else if (!TextUtils.isEmpty(strValue2)) {
				mEditOtpPw02.setText("");
				mImageOtpPw02.setImageResource(R.drawable.ico_otp_num_off);
				mEditOtpPw01.requestFocus();
			} else if (!TextUtils.isEmpty(strValue1)) {
				mEditOtpPw01.setText("");
				mImageOtpPw01.setImageResource(R.drawable.ico_otp_num_off);
			}
		}
		else if (arg0 == ITransKeyActionListenerEx.INPUT_CLEARALL_BUTTON) {
			clearInput();
		}
	}

	@Override
	public void minTextSizeCallback() {

	}

	@Override
	public void maxTextSizeCallback() {

	}

	private String getPlainData(int iRealDataLength) {
		String plainData = "0";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(mTKMgr.getSecureKey());

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(mTKMgr.getCipherData(), pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "0";
			}

		} catch (Exception e) {
			Logs.printException(e);
		}
		if (!TextUtils.isEmpty(plainData))
			plainData = plainData.substring(iRealDataLength -1, iRealDataLength);
		return plainData;
	}

	/**
	 * Extras 값 획득
	 */
	private void getExtra() {
		mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
		if(mCommonUserInfo.getOtpPWLength() <= 0 || mCommonUserInfo.getOtpPWLength() > Const.OTP_PW_JUMIN_LENGTH)
			mCommonUserInfo.setOtpPWLength(Const.OTP_PW_NUM_LENGTH);

		mNativeOtpAuthTools = getIntent().getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
		mNativeOtpSignData = getIntent().getStringExtra(Const.INTENT_OTP_AUTH_SIGN);
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mLayoutMsg = (LinearLayout) findViewById(R.id.layout_otp_pw_auth_msg);
		mTextMsg = (TextView) findViewById(R.id.textview_otp_pw_auth_msg);

		mExitOtpPw = (EditText) findViewById(R.id.layout_otp_pw_input_auth).findViewById(R.id.editText);
		mEditOtpPw01 = (EditText) findViewById(R.id.edittext_otp_pw_auth_01);
		mEditOtpPw01.setOnTouchListener(this);
		mImageOtpPw01 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_01);
		mEditOtpPw01.setOnKeyListener(null);
		mEditOtpPw02 = (EditText) findViewById(R.id.edittext_otp_pw_auth_02);
		mEditOtpPw02.setOnTouchListener(this);
		mEditOtpPw02.setOnKeyListener(null);
		mImageOtpPw02 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_02);
		mEditOtpPw03 = (EditText) findViewById(R.id.edittext_otp_pw_auth_03);
		mEditOtpPw03.setOnTouchListener(this);
		mEditOtpPw03.setOnKeyListener(null);
		mImageOtpPw03 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_03);
		mEditOtpPw04 = (EditText) findViewById(R.id.edittext_otp_pw_auth_04);
		mEditOtpPw04.setOnTouchListener(this);
		mEditOtpPw04.setOnKeyListener(null);
		mImageOtpPw04 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_04);
		RelativeLayout layoutOtpPw05 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_05);
		mEditOtpPw05 = (EditText) findViewById(R.id.edittext_otp_pw_auth_05);
		mEditOtpPw05.setOnTouchListener(this);
		mEditOtpPw05.setOnKeyListener(null);
		mImageOtpPw05 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_05);
		RelativeLayout layoutOtpPw06 = (RelativeLayout) findViewById(R.id.layout_otp_pw_auth_06);
		mEditOtpPw06 = (EditText) findViewById(R.id.edittext_otp_pw_auth_06);
		mEditOtpPw06.setOnTouchListener(this);
		mEditOtpPw06.setOnKeyListener(null);
		mImageOtpPw06 = (ImageView) findViewById(R.id.imageview_otp_pw_auth_06);

		mArrayNumber[0] = R.drawable.ico_otp_num_0_on;
		mArrayNumber[1] = R.drawable.ico_otp_num_1_on;
		mArrayNumber[2] = R.drawable.ico_otp_num_2_on;
		mArrayNumber[3] = R.drawable.ico_otp_num_3_on;
		mArrayNumber[4] = R.drawable.ico_otp_num_4_on;
		mArrayNumber[5] = R.drawable.ico_otp_num_5_on;
		mArrayNumber[6] = R.drawable.ico_otp_num_6_on;
		mArrayNumber[7] = R.drawable.ico_otp_num_7_on;
		mArrayNumber[8] = R.drawable.ico_otp_num_8_on;
		mArrayNumber[9] = R.drawable.ico_otp_num_9_on;

		(findViewById(R.id.tv_close)).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				setResult(RESULT_CANCELED);
				finish();
				if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB)
					overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
			}
		});

		if (mCommonUserInfo.getOtpPWLength() == 4) {
			layoutOtpPw05.setVisibility(View.GONE);
			layoutOtpPw06.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getOtpPWLength() == 5) {
			layoutOtpPw06.setVisibility(View.GONE);
		}

		mTKMgr = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
				TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX,
				"Pin Code",//label
				"",//hint
				6,  //max length
				"",//max length msg
				6,  //min length
				"",
				5,
				true,
				(FrameLayout) findViewById(R.id.keypadContainer),
				mExitOtpPw,
				(HorizontalScrollView) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.keyscroll),
				(LinearLayout) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.keylayout),
				(ImageButton) (findViewById(R.id.layout_otp_pw_input_auth)).findViewById(R.id.clearall),
				(RelativeLayout) findViewById(R.id.keypadBallon),
				null,
				false,
				true);
		mTKMgr.showKeypad(mKeypadType);
	}

	/**
	 * 핀코드 입력값 화면 초기화
	 */
	private void clearInput() {
		mEditOtpPw01.setText("");
		mImageOtpPw01.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw02.setText("");
		mImageOtpPw02.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw03.setText("");
		mImageOtpPw03.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw04.setText("");
		mImageOtpPw04.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw05.setText("");
		mImageOtpPw05.setImageResource(R.drawable.ico_otp_num_off);
		mEditOtpPw06.setText("");
		mImageOtpPw06.setImageResource(R.drawable.ico_otp_num_off);

		mEditOtpPw01.requestFocus();
	}

	/**
	 * 키패드 보이기
	 */
	private void showKeyPad() {
		clearInput();
		mExitOtpPw.requestFocus();
		mTKMgr.showKeypad(mKeypadType);
		mIsViewCtrlKeypad = true;
	}

	private void requestAuthOtp(final String plainData) {
		Map param = new HashMap();
		param.put("OTP_PWD",plainData);
		showProgressDialog();
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0011700A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				dismissProgressDialog();

				if (!TextUtils.isEmpty(ret)) {
					try {
						JSONObject object = new JSONObject(ret);
						if (object == null) {
							Logs.e(getResources().getString(R.string.msg_debug_err_response));
							Logs.e(ret);
							showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
							showKeyPad();
							return;
						}
						JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
						String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
						String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
						String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
						if (TextUtils.isEmpty(msg))
							msg = getString(R.string.common_msg_no_reponse_value_was);

						if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
							if ("EEIF0509".equals(errCode) || "XEOP3114".equals(errCode)) {
								Logs.e("otp pin lock");
								DialogUtil.alert(OtherOtpAuthActivity.this,
										getResources().getString(R.string.common_reissue),
										getResources().getString(R.string.common_cancel),
										getResources().getString(R.string.msg_otp_lost),
										new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												Intent intent = new Intent(OtherOtpAuthActivity.this, WebMainActivity.class);
												String url = WasServiceUrl.CRT0080401.getServiceUrl();
												intent.putExtra("url", url);
												mLayoutMsg.setVisibility(View.INVISIBLE);
												startActivity(intent);
												return;
											}
										},
										new View.OnClickListener() {
											@Override
											public void onClick(View v) {
												setResult(RESULT_CANCELED);
												finish();
											}
										});
								showKeyPad();
								return;
							} else if ("EEIF0508".equals(errCode)) {
								Logs.e("otp pin error");
								mLayoutMsg.setVisibility(View.VISIBLE);
								mTextMsg.setText(msg);
								showKeyPad();
								AniUtils.shakeView(mLayoutMsg);
								return;
							}

							Logs.e("error msg : " + msg + ", ret : " + ret);
							showCommonErrorDialog(msg, errCode, "", objectHead, true);
							showKeyPad();
							return;
						}

						Intent intent = new Intent();
						intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, mNativeOtpAuthTools);
						intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, mNativeOtpSignData);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						setResult(RESULT_OK, intent);
						finish();
						return;

					} catch (JSONException e) {
						Logs.printException(e);
						showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
						showKeyPad();
						return;
					}
				} else {
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
					showKeyPad();
					return;
				}
			}
		});
	}
}