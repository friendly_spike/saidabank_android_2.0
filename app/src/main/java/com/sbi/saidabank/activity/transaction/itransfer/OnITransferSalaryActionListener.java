package com.sbi.saidabank.activity.transaction.itransfer;

import android.widget.EditText;

public interface OnITransferSalaryActionListener {

    void showBottomNoti(boolean flag);
    void setTitle(String mainText,String subText,boolean isError);
    void setFocusedEditText(EditText editText);
    void checkRowAndPlayBtnState();

    void notifyDataChanged();
    void setListBindingFlag(boolean flag);
    boolean getListBindingFlag();
}
