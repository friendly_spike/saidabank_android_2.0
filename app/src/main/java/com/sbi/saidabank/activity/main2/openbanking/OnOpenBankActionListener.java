package com.sbi.saidabank.activity.main2.openbanking;

import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;

public interface OnOpenBankActionListener {

    void requestOpenBankList();
    void revokeAccountInfo(OpenBankAccountInfo accountInfo);
    void requestElectroSignData();
}
