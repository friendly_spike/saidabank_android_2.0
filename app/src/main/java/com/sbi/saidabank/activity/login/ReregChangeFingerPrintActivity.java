package com.sbi.saidabank.activity.login;

import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

/**
 * siadabank_android
 * Class: ReregChangeFingerPrintActivity
 * Created by 950546
 * Date: 2019-01-04
 * Time: 오후 1:36
 * Description: ReregChangeFingerPrintActivity
 */
public class ReregChangeFingerPrintActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fingerprint_change);
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
