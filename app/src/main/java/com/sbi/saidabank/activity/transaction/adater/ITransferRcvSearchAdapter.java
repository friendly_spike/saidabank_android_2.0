package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.ITransferSearchReceiverActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSearchActionListener;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRcvSearchInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;

import java.util.ArrayList;

/**
 * 받는사람 검색 어댑터
 */
public class ITransferRcvSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private ArrayList<ITransferRcvSearchInfo> mSearchArrayList;
    private String mInputText;
    private OnITransferSearchActionListener mListener;

    public ITransferRcvSearchAdapter(Context context,OnITransferSearchActionListener listener) {
        mContext = context;
        mSearchArrayList = new ArrayList<ITransferRcvSearchInfo>();
        mListener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return ViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ITransferRcvSearchInfo info = mSearchArrayList.get(position);

        ((ViewHolder)holder).onBind(mContext,info,mInputText,mListener);
    }

    @Override
    public int getItemCount() {
        return mSearchArrayList.size();
    }

    public void setSearchArray(ArrayList<ITransferRcvSearchInfo> array,String inputText) {
        this.mInputText = inputText;
        mSearchArrayList.clear();
        mSearchArrayList.addAll(array);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private RelativeLayout layoutBody;
        private ImageView ivIcon;
        private TextView tvTitle;
        private TextView tvTitleNickname;
        private TextView tvSubTitle;
        private TextView tvTabName;

        public static ViewHolder newInstance(ViewGroup parent) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_itransfer_search_receiver_list_item, parent, false);
            return new ViewHolder(itemView);
        }

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            layoutBody = itemView.findViewById(R.id.layout_body);
            ivIcon = itemView.findViewById(R.id.iv_icon);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvTitleNickname = itemView.findViewById(R.id.tv_title_nickname);
            tvSubTitle = itemView.findViewById(R.id.tv_subtitle);
            tvTabName = itemView.findViewById(R.id.tv_tabname);
        }

        public void onBind(Context context,ITransferRcvSearchInfo info,String inputText,final OnITransferSearchActionListener listener){
            switch(info.getTabType()){
                case ITransferSearchReceiverActivity.ITEM_TYPE_RECENTLY: {
                    tvTabName.setText(R.string.itransfer_tab_recently);

                    ITransferRecentlyInfo recentlyInfo = (ITransferRecentlyInfo) info.getObjectInfo();

                    layoutBody.setTag(recentlyInfo);
                    layoutBody.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ITransferRecentlyInfo recentlyInfo = (ITransferRecentlyInfo)v.getTag();

                            if(!TextUtils.isEmpty(recentlyInfo.getMNRC_TLNO())){
                                String MNRC_TLNO = recentlyInfo.getMNRC_TLNO();
                                String MNRC_ACCO_DEPR_NM = recentlyInfo.getMNRC_ACCO_DEPR_NM();
                                listener.showPhoneRealNameDialog(MNRC_ACCO_DEPR_NM,MNRC_TLNO);

                            }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){
                                //그룹이체 *********

                                listener.checkGroupAccount(recentlyInfo.getGRP_SRNO(),recentlyInfo.getFAVO_ACCO_YN(),recentlyInfo.getGRP_CCNT());
                            }else{
                                //계좌이체 수취인조회 *******
                                listener.checkRemitteeAccount(recentlyInfo.getMNRC_BANK_CD(),recentlyInfo.getDTLS_FNLT_CD(),recentlyInfo.getMNRC_ACNO(),recentlyInfo.getMNRC_ACCO_DEPR_NM());

                            }
                        }
                    });

                    //이름
                    String name = recentlyInfo.getMNRC_ACCO_DEPR_NM();
                    Utils.setTextWithSpan(tvTitle, name, inputText, Typeface.BOLD, "#009beb");
                    //닉네임
                    String nicName = recentlyInfo.getMNRC_ACCO_ALS();
                    if (!TextUtils.isEmpty(nicName)) {
                        Utils.setTextWithSpan(tvTitleNickname, "(" + nicName + ")", inputText, Typeface.BOLD, "#009beb");
                        tvTitleNickname.setVisibility(View.VISIBLE);
                    } else {
                        tvTitleNickname.setVisibility(View.GONE);
                    }

                    //휴대폰 이체의 즐겨찾기가 있는지 체크
                    String MNRC_TLNO = recentlyInfo.getMNRC_TLNO();
                    if(!TextUtils.isEmpty(MNRC_TLNO)){
                        Utils.setTextWithSpan(tvSubTitle, MNRC_TLNO, inputText, Typeface.BOLD, "#009beb");
                        ivIcon.setImageResource(R.drawable.ico_cellphone);
                    }else{
                        String MNRC_BANK_CD = recentlyInfo.getMNRC_BANK_CD();
                        String bankName = recentlyInfo.getMNRC_BANK_NM();
//                        if (!TextUtils.isEmpty(MNRC_BANK_CD) && "000".equalsIgnoreCase(MNRC_BANK_CD) || "028".equalsIgnoreCase(MNRC_BANK_CD)) {
//                            bankName = "사이다";
//                        }
                        // 계좌번호
                        String acno = recentlyInfo.getMNRC_ACNO();
                        Utils.setTextWithSpan(tvSubTitle, bankName + " " + acno, inputText, Typeface.BOLD, "#009beb");
                        
                        if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){
                            ivIcon.setImageResource(R.drawable.ico_group);
                        }else{
                            //은행 아이콘 가져온다.
                            String bankCd = recentlyInfo.getMNRC_BANK_CD();
                            if (!TextUtils.isEmpty(bankCd)) {

                                Uri iconUri = ImgUtils.getIconUri(context,bankCd,recentlyInfo.getDTLS_FNLT_CD());
                                if (iconUri != null)
                                    ivIcon.setImageURI(iconUri);
                                else
                                    ivIcon.setImageResource(R.drawable.img_logo_xxx);
                            }else{
                                ivIcon.setImageResource(R.drawable.img_logo_xxx);
                            }
                        }
                    }
                    break;
                }
                case ITransferSearchReceiverActivity.ITEM_TYPE_MYACCOUNT: {

                    tvTabName.setText(R.string.itransfer_tab_myaccount);
                    tvTitleNickname.setVisibility(View.GONE);

                    MyAccountInfo myAccInfo = (MyAccountInfo) info.getObjectInfo();

                    layoutBody.setTag(myAccInfo);
                    layoutBody.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            MyAccountInfo myAccInfo = (MyAccountInfo)v.getTag();
                            listener.checkRemitteeAccount("028","",myAccInfo.getACNO(), LoginUserInfo.getInstance().getCUST_NM());
                        }
                    });



                    String accoAls = myAccInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(accoAls)) {
                        accoAls = myAccInfo.getPROD_NM();
                    }
                    Utils.setTextWithSpan(tvTitle, accoAls, inputText, Typeface.BOLD, "#009beb");

                    String accountNo = myAccInfo.getACNO();
                    //Utils.setTextWithSpan(tvSubTitle, accountNo, inputText, Typeface.BOLD, "#009beb");
                    String bankName = myAccInfo.getBANK_NM();
                    Utils.setTextWithSpan(tvSubTitle, bankName + " " + accountNo, inputText, Typeface.BOLD, "#009beb");

                    //은행 아이콘 출력
                    Uri iconUri = ImgUtils.getIconUri(context, "028");
                    if (iconUri != null)
                        ivIcon.setImageURI(iconUri);
                    else
                        ivIcon.setImageResource(R.drawable.img_logo_xxx);


                    break;
                }
                case ITransferSearchReceiverActivity.ITEM_TYPE_OPENBANK: {

                    tvTabName.setText(R.string.itransfer_tab_myaccount);
                    tvTitleNickname.setVisibility(View.GONE);

                    OpenBankAccountInfo openBankInfo = (OpenBankAccountInfo) info.getObjectInfo();

                    layoutBody.setTag(openBankInfo);
                    layoutBody.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            OpenBankAccountInfo openBankAccountInfo = (OpenBankAccountInfo)v.getTag();
                            listener.checkRemitteeAccount(openBankAccountInfo.RPRS_FNLT_CD,openBankAccountInfo.DTLS_FNLT_CD,openBankAccountInfo.ACNO,LoginUserInfo.getInstance().getCUST_NM());
                        }
                    });

                    Utils.setTextWithSpan(tvTitle, openBankInfo.PROD_NM, inputText, Typeface.BOLD, "#009beb");
                    Utils.setTextWithSpan(tvSubTitle, openBankInfo.BANK_NM + " " + openBankInfo.ACNO, inputText, Typeface.BOLD, "#009beb");


                    //은행 아이콘 출력
                    Uri iconUri = ImgUtils.getIconUri(context, openBankInfo.RPRS_FNLT_CD,openBankInfo.DTLS_FNLT_CD);
                    if (iconUri != null)
                        ivIcon.setImageURI(iconUri);
                    else
                        ivIcon.setImageResource(R.drawable.img_logo_xxx);
                    break;
                }
                case ITransferSearchReceiverActivity.ITEM_TYPE_CONTACT: {

                    tvTabName.setText(R.string.itransfer_tab_contact);
                    tvTitleNickname.setVisibility(View.GONE);

                    ContactsInfo contactsInfo = (ContactsInfo) info.getObjectInfo();

                    layoutBody.setTag(contactsInfo);
                    layoutBody.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ContactsInfo contactsInfo = (ContactsInfo)v.getTag();

                            listener.showPhoneRealNameDialog(contactsInfo.getFLNM(),contactsInfo.getTLNO());
                        }
                    });

                    //이름
                    Utils.setTextWithSpan(tvTitle, contactsInfo.getFLNM(), inputText, Typeface.BOLD, "#009beb");

                    //전화번호
                    String phone = contactsInfo.getTLNO();
                    //phone = PhoneNumberUtils.formatNumber(phone, Locale.getDefault().getCountry());
                    Utils.setTextWithSpan(tvSubTitle, phone, inputText, Typeface.BOLD, "#009beb");


                    ivIcon.setImageResource(R.drawable.ico_cellphone);
                    break;
                }
            }
        }
    }
}
