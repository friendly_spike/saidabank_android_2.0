package com.sbi.saidabank.activity.transaction.itransfer.single;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mangosteen.falldownnumbereditor.FallDownNumberEditor;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSingleActionListener;
import com.sbi.saidabank.activity.transaction.safedeal.CustomNumPadLayout;
import com.sbi.saidabank.activity.transaction.safedeal.CustomTitleLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideBaseLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideMyAccountLayout;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

public class ITransferInputMoneyLayout extends ITransferBaseLayout implements View.OnClickListener{

    private LinearLayout mLayoutBody;

    private TextView mTvHangleMoney;
    private TextView mTvNotoce;
    private LinearLayout mLayoutError;
    private TextView mTvError;
    private FallDownNumberEditor mFallDownNumberEditor;
    private TextView mTvPosibleAmount;

    private RelativeLayout mLayoutAmount;
    private RelativeLayout mLayoutNotice;
    private RelativeLayout mLayoutPosibleAmount;


    private Double mBalanceAmount;                // 계좌 잔액
    private Double mTransferOneTime;              // 1회 이체한도
    private Double mTransferOneDay;               // 일일 이체한도

    private OnITransferSingleActionListener mActionListener;

    private ITransferKeyPadLayout   mLayoutKeypad;

    public ITransferInputMoneyLayout(Context context) {
        super(context);
        clearData();
        initUX(context);
    }

    public ITransferInputMoneyLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        clearData();
        initUX(context);
    }

    /**
     * 초기화 함수
     * Override해서 사용하도록 한다.
     */
    public void clearData(){
        mBalanceAmount=0d;                // 계좌 잔액
        mTransferOneTime=0d;              // 1회 이체한도
        mTransferOneDay=0d;
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_itransfer_input_money, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));


        mLayoutBody = findViewById(R.id.layout_body);

        mLayoutAmount = findViewById(R.id.layout_amount);
        mLayoutAmount.setOnClickListener(this);
        mLayoutNotice = findViewById(R.id.layout_notice);
        mLayoutPosibleAmount = findViewById(R.id.layout_posible_amount);
        int radius = (int) Utils.dpToPixel(getContext(),3);
        mLayoutPosibleAmount.setBackground(GraphicUtils.getRoundCornerDrawable("#f5f5f5",new int[]{radius,radius,radius,radius}));



        mTvHangleMoney = findViewById(R.id.tv_hangle_money);
        mTvHangleMoney.setVisibility(GONE);

        mTvNotoce = findViewById(R.id.tv_notice);
        mTvNotoce.setVisibility(INVISIBLE);
        mLayoutError = findViewById(R.id.layout_error);
        mTvError = findViewById(R.id.tv_error);
        mTvPosibleAmount = findViewById(R.id.tv_posible_amount);

        mFallDownNumberEditor = findViewById(R.id.falldowneditor);
        int size = (int)Utils.dpToPixel(getContext(),32f);
        mFallDownNumberEditor.setNumberTextSize(size);
        mFallDownNumberEditor.setNumberTextColor("#000000");

        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        int activityPixelHeight = GraphicUtils.getActivityHeight(getContext());
                        int activityDipHeight = (int)Utils.pixelToDp(getContext(),activityPixelHeight);
                        Logs.e("activityDipHeight : " + activityDipHeight);

                        int titleAreaHeight = (int) getResources().getDimension(R.dimen.itransfer_title_area_height);
                        int accountAreaHeight = (int) getResources().getDimension(R.dimen.itransfer_select_account_area_height);
                        int keypadAreaHeight = (int) getResources().getDimension(R.dimen.itransfer_keypad_area_height) + (int)Utils.dpToPixel(getContext(),20);

                        int viewHeight = activityPixelHeight - titleAreaHeight - accountAreaHeight - keypadAreaHeight;

                        ViewGroup.LayoutParams params = getLayoutParams();
                        params.height = viewHeight;
                        setLayoutParams(params);

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    public void setActionListener(OnITransferSingleActionListener listener){
        mActionListener = listener;
    }

    public void setLayoutKeypad(ITransferKeyPadLayout layoutKeypad){
        mLayoutKeypad = layoutKeypad;
    }

    public String getInputMoney(){
        return mFallDownNumberEditor.getMoney();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.btn_ok: {
//                final String inputMoney = mFallDownNumberEditor.getMoney();
//
//                //사이다 뱅킹 일때만 아래 서비스 체크한다.
//                String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
//                String DLY_TRNF_SVC_ENTR_YN = LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN();
//                if(TextUtils.isEmpty(DLY_TRNF_SVC_ENTR_YN)) DLY_TRNF_SVC_ENTR_YN = "N";
//                String DSGT_MNRC_ACCO_SVC_ENTR_YN = LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN();
//                if(TextUtils.isEmpty(DSGT_MNRC_ACCO_SVC_ENTR_YN)) DSGT_MNRC_ACCO_SVC_ENTR_YN = "N";
//
//                if(WTCH_BANK_CD.equalsIgnoreCase("028")){
//                    if( DLY_TRNF_SVC_ENTR_YN.equalsIgnoreCase("Y")||
//                        DSGT_MNRC_ACCO_SVC_ENTR_YN.equalsIgnoreCase("Y")){
//
//                        TransferRequestUtils.requestDlyDsgtServiceCheck((Activity) getContext(), inputMoney, new HttpSenderTask.HttpRequestListener2() {
//                            @Override
//                            public void endHttpRequest(boolean result, String ret) {
//                                if(result){
//                                    //hideView();
//                                    moveTopPosAmountView();
//                                    if(mActionListener != null)
//                                        mActionListener.openInputDetailView(inputMoney);
//                                }
//                            }
//                        });
//                        return;
//                    }
//                }
//
//
//                //hideView();
//                moveTopPosAmountView();
//                if(mActionListener != null)
//                    mActionListener.openInputDetailView(inputMoney);
//                else
//                    Logs.showToast(getContext(),"ActionListener를 등록해주세요.");
//                break;
//            }
            case R.id.layout_amount:
                if(mLayoutKeypad.getVisibility() != VISIBLE){
                    mActionListener.closeInputDetailView();
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            moveOriginPosAmountView();
                        }
                    },200);
                }
                break;
        }
    }

    public void onNumPadClick(String numStr) {
        switch (numStr) {
            case "cancel": {
                mFallDownNumberEditor.removeAllView();
                mTvHangleMoney.setVisibility(GONE);
                displayLimitAmount();

                mLayoutError.setVisibility(INVISIBLE);
                //mOKBtn.setEnabled(false);
                mLayoutKeypad.setOKBtnState(false);
                mFallDownNumberEditor.setNumberTextColor("#000000");
                break;
            }
            case "del": {
                String inputMoney = mFallDownNumberEditor.getMoney();
                if(inputMoney.length() == 1){
                    mFallDownNumberEditor.removeAllView();
                    mTvHangleMoney.setVisibility(GONE);
                    displayLimitAmount();

                    mLayoutError.setVisibility(INVISIBLE);
                    //mOKBtn.setEnabled(false);
                    mLayoutKeypad.setOKBtnState(false);
                    mFallDownNumberEditor.setNumberTextColor("#000000");
                }else{
                    mFallDownNumberEditor.delNumber();
                    inputMoney = mFallDownNumberEditor.getMoney();
                    if(Double.parseDouble(inputMoney) < 10000){
                        mTvHangleMoney.setVisibility(GONE);
                    }else{
                        String hangul = Utils.convertHangul(inputMoney);
                        mTvHangleMoney.setText(hangul+"원");
                    }

                    if(inputMoney.equals("0")){
                        //mOKBtn.setEnabled(false);
                        mLayoutKeypad.setOKBtnState(false);
                        return;
                    }

                    if(checkVaildAmount(inputMoney)){
                        displayLimitAmount();

                        mLayoutError.setVisibility(INVISIBLE);
                        //mOKBtn.setEnabled(true);
                        mLayoutKeypad.setOKBtnState(true);
                        mFallDownNumberEditor.setNumberTextColor("#000000");
                    }
                }

                break;
            }
            default: {
                //출금 입금계좌가 모두 선택되어 있는지 체크한다.
                if(!checkSendRecieveAccount()){
                    return;
                }

                String inputMoney = mFallDownNumberEditor.getMoney();
                //천억자리까지만 입력 가능하도록...
                if(inputMoney.length() >= 10) return;

                if(!checkVaildAmount(inputMoney)){
                    mTvNotoce.setVisibility(INVISIBLE);
                    mLayoutError.setVisibility(VISIBLE);
                    AniUtils.shakeView(mFallDownNumberEditor, 20f);
                    return;
                }

                mFallDownNumberEditor.setNumber(numStr);

                checkInputAmountState();
                break;
            }
        }
    }

    /**
     * 금액이 입력된 후에 금액에 대한 여러 조건을 체크하여 에러 상태를 표시하기 위해.
     */
    private void checkInputAmountState(){
        String inputMoney = mFallDownNumberEditor.getMoney();
        if(Double.parseDouble(inputMoney) >= 10000){
            String hangul = Utils.convertHangul(inputMoney);
            mTvHangleMoney.setText(hangul+"원");
            if(mTvHangleMoney.getVisibility() != VISIBLE){
                mTvHangleMoney.setVisibility(VISIBLE);
            }
        }

        if(Double.parseDouble(inputMoney) == 0) return;

        if(!checkVaildAmount(inputMoney)){
            mTvNotoce.setVisibility(INVISIBLE);
            mLayoutError.setVisibility(VISIBLE);
            AniUtils.shakeView(mFallDownNumberEditor, 20f);

            mLayoutKeypad.setOKBtnState(false);
            mFallDownNumberEditor.setNumberTextColor("#e0250c");
            return;
        }else{

            mLayoutKeypad.setOKBtnState(true);
            mFallDownNumberEditor.setNumberTextColor("#000000");
        }
    }

    private boolean checkSendRecieveAccount(){
        //보내는 계좌 받는계좌가 모두 설정되어 있는지 확인한다.
        if(TextUtils.isEmpty(ITransferDataMgr.getInstance().getWTCH_ACNO())){
            DialogUtil.alert(getContext(),"출금계좌를 선택해주세요.", new OnClickListener() {
                @Override
                public void onClick(View v) {
                    View view = getSlideView(ITransferSelectAccountLayout.class);
                    if(view != null){
                        ((ITransferSelectAccountLayout)view).showSelectSendAccountDialog();
                    }
                }
            });
            return false;
        }

        //받는계좌가 선택되어 있는지 체크한다.
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_NONE){
            DialogUtil.alert(getContext(),"받는분을 선택해주세요", new OnClickListener() {
                @Override
                public void onClick(View v) {
                    View view = getSlideView(ITransferSelectAccountLayout.class);
                    if(view != null){
                        ((ITransferSelectAccountLayout)view).showSelectRcvAccountDialog();
                    }
                }
            });

            return false;
        }

        return true;
    }

    /**
     * 조회된 한도를 넣어준다.
     * ITransferSelectAccountLayout에서 넣어준다.
     */
    public void updateAccountLimit(){
        //onNumPadClick("cancel");//입력된 금액은 지워주지 않는다.
        mTransferOneTime = ITransferDataMgr.getInstance().getTransferOneTime();
        mTransferOneDay = ITransferDataMgr.getInstance().getTransferOneDay();
        mBalanceAmount = ITransferDataMgr.getInstance().getBalanceAmount();
        mTvPosibleAmount.setText("출금가능 " + Utils.moneyFormatToWon(mBalanceAmount) + " 원");

        displayLimitAmount();
        mLayoutError.setVisibility(INVISIBLE);

        //혹시 계좌정보에서 내보내기 할때 금액을 물고 들어오면 여기서 넣어준다.
        String inputMoney = mFallDownNumberEditor.getMoney();
        if(TextUtils.isEmpty(inputMoney)) inputMoney = "0";

        if(inputMoney.equalsIgnoreCase("0")&&ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_ACCOUNT){
            String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
            String TRAN_AMT = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getTRN_AMT();
            if(!TextUtils.isEmpty(WTCH_ACNO) && !TextUtils.isEmpty(TRAN_AMT) && !TRAN_AMT.equalsIgnoreCase("0")){
                String[] arr = TRAN_AMT.split("(?<!^)");
                for(int i=0;i<arr.length;i++){
                    mFallDownNumberEditor.setNumber(arr[i]);
                }

            }
        }

        //출금계좌를 변경한 경우 잔액이나 조건이 변경될 수 있어서 금액 상태를 한번더 체크해야 한다.
        checkInputAmountState();
    }

    /**
     * 한도를 화면에 표시한다.
     */
    private void displayLimitAmount(){
        String dispLimit = "";
        String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        if(TextUtils.isEmpty(WTCH_BANK_CD)){
            mTvNotoce.setVisibility(INVISIBLE);
        }else if(WTCH_BANK_CD.equalsIgnoreCase("028")) {
            dispLimit = "1회 : " + Utils.moneyFormatToWon(mTransferOneTime) + getContext().getString(R.string.won) + " / 1일 : " + Utils.moneyFormatToWon(mTransferOneDay) + getContext().getString(R.string.won);
            mTvNotoce.setVisibility(VISIBLE);
            mTvNotoce.setText(dispLimit);
            mTvNotoce.setTextColor(getContext().getResources().getColor(R.color.black));
        }else{
            if(mTransferOneDay == 10000000){
                dispLimit = "전 금융기관 합산 1일 10,000,000 원";
            }else{
                dispLimit = "1일 : " + Utils.moneyFormatToWon(mTransferOneDay) + "원";
            }
            //오픈뱅킹은 한도를 표시하지 않는다고 함.
            mTvNotoce.setVisibility(INVISIBLE);
            //mTvNotoce.setText(dispLimit);
            //mTvNotoce.setTextColor(getContext().getResources().getColor(R.color.black));
        }

    }

    /**
     * 입력한 금액을 한도와 체크한다.
     * @param inputAmount
     * @return
     */
    private boolean checkVaildAmount(String inputAmount) {
        Logs.e("setOneTimeOneDayLimit - oneTimeLimit : " + mTransferOneTime);
        Logs.e("setOneTimeOneDayLimit - oneDayLimit : " + mTransferOneDay);
        Logs.e("setOneTimeOneDayLimit - balanceAmount : " + mBalanceAmount);

        if (mBalanceAmount <= 0) {
            String msg = getContext().getString(R.string.msg_err_trnasfer_over_balance)+" 출금가능 : " + Utils.moneyFormatToWon(mBalanceAmount) + "원";
            mTvError.setText(msg);
            return false;
        }

        inputAmount = inputAmount.replaceAll("[^0-9]", Const.EMPTY);
        double dInputAmount = Double.valueOf(inputAmount);
        //잔액체크
        if (dInputAmount > mBalanceAmount) {
            String msg = getContext().getString(R.string.msg_err_trnasfer_over_balance)+" 출금가능 : " + Utils.moneyFormatToWon(mBalanceAmount) + "원";
            mTvError.setText(msg);
            return false;
        }

        //일회이체한도 체크
        if(ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
            if (dInputAmount > mTransferOneTime) {
                mTvError.setText(getContext().getString(R.string.msg_err_trnasfer_over_one_time));
                return false;
            }
        }

        //일일 이체한도 체크
        if (dInputAmount > mTransferOneDay) {
            if(ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
                mTvError.setText(getContext().getString(R.string.msg_err_trnasfer_over_one_day));
            }else{
                //오픈뱅킹은 일자로 계산한다.. 어짜피 1회와 1일의 값이 같다.
                mTvError.setText("1일 : " + Utils.moneyFormatToWon(mTransferOneDay)+ "원");
            }
            return false;
        }

        return true;
    }

    public void hideView(){
        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.slide_out_left);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(ani);
    }

    public void showView(){
        setVisibility(VISIBLE);
        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.slide_in_left);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(ani);
    }

    public void moveTopPosAmountView(){
        float pixel = Utils.dpToPixel(getContext(),10f);
        float moveYPos = mLayoutAmount.getY() * -1 + pixel;

        mLayoutAmount.animate()
                .translationY(moveYPos)
                .setDuration(250)
                .start();


        mLayoutNotice.animate()
                .translationY(moveYPos)
                .setDuration(250)
                .alpha(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutNotice.setVisibility(INVISIBLE);
                    }
                } )
                .start();

        mLayoutPosibleAmount.animate()
                .translationY(moveYPos)
                .setDuration(250)
                .alpha(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutPosibleAmount.setVisibility(INVISIBLE);
                    }
                } )
                .start();

        mLayoutKeypad.fadeOutKeypad();
    }

    public void moveOriginPosAmountView(){

        mLayoutAmount.animate()
                .translationY(0)
                .setDuration(250)
                .start();

        mLayoutNotice.animate()
                .translationY(0)
                .setDuration(250)
                .alpha(1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutNotice.setVisibility(VISIBLE);

                    }
                } )
                .start();

        mLayoutPosibleAmount.animate()
                .translationY(0)
                .setDuration(250)
                .alpha(1)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mLayoutPosibleAmount.setVisibility(VISIBLE);

                    }
                } )
                .start();

        mLayoutKeypad.fadeInKeypad();
    }
}
