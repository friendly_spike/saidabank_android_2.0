package com.sbi.saidabank.activity.main2.homecouple;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;

/**
 * 커플 가입한 달 통계정보가 없을때 레이아웃
 * 커플가입 9월. 현재 9월 일때
 */
public class Statistics1Layout extends RelativeLayout {
    public Statistics1Layout(Context context) {
        super(context);
        initUX(context);
    }

    public Statistics1Layout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_couple_statistics1, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
    }

    /**
     * 해당 아이콘은 Statistic2,3에서 지출내역별로 사용하고 있다.
     * Statistic2,3의 코드길이가 길어서 여기에 Define한다.
     *
     * @param code
     * @return
     */
    public static int getExpenceIconResourceId(String code) {
        if (DataUtil.isNull(code))
            return R.drawable.ico_category_pay;
        switch (code) {
            // 계좌송금
            case "01":
                return R.drawable.ico_category_send;
            // 자동이체 - 고정 or 변경
            case "02":
            case "03":
                return R.drawable.ico_category_auto;
            // 현금출금
            case "04":
                return R.drawable.ico_category_cash;
            // 간편결제,송금
            case "05":
                return R.drawable.ico_category_card;
        }
        // 카드출금
        return R.drawable.ico_category_pay;
    }
}
