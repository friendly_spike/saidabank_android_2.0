package com.sbi.saidabank.activity.main2.openbanking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Create by 20210119
 * 오픈뱅킹 리스트중 내부적을 리스트 계좌를 가지고 있는 아이템 부분을 출력
 */
public class ViewHolderAcc extends RecyclerView.ViewHolder{
    private Context mContext;

    private View mItemView;
    private LinearLayout mLayoutBody;   //전체몸통
    private RelativeLayout mLayoutSubBody;
    private LinearLayout mLayoutAccState;
    private TextView     mTvAccState;

    private ImageView mIvBankIcon;      //은행 아이콘
    private TextView mTvAccName;        //계좌명
    private TextView mTvAccNum;         //계좌번호
    private TextView mTvAmount;         //계좌금액
    private TextView mTvWon;         //계좌금액
    private ImageView mIvArrowCircle;
    private View mVCellLine;

    //이체 충전 버튼 레이아웃
    private LinearLayout mLayoutBtn;    //버튼 전체 레이아웃
    private TextView mLayoutBtnText;

    private OnOpenBankActionListener mListener;

    public static ViewHolderAcc newInstance(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main2_openbank_list_item_account, parent, false);
        return new ViewHolderAcc(parent.getContext(),itemView);
    }

    public ViewHolderAcc(Context context, @NonNull View itemView) {
        super(itemView);
        mContext=context;

        mItemView       = itemView;
        mLayoutBody     = itemView.findViewById(R.id.layout_body);
        mLayoutSubBody  = itemView.findViewById(R.id.layout_subbody);
        mLayoutAccState = itemView.findViewById(R.id.layout_acc_state);
        mTvAccState     = itemView.findViewById(R.id.tv_acc_state);

        mIvBankIcon     = itemView.findViewById(R.id.iv_bank_icon);
        mTvAccName      = itemView.findViewById(R.id.tv_account_name);
        mTvAccNum       = itemView.findViewById(R.id.tv_account_num);
        mTvAmount       = itemView.findViewById(R.id.tv_amount);
        mTvWon          = itemView.findViewById(R.id.tv_won);
        mIvArrowCircle  = itemView.findViewById(R.id.iv_arrow_circle);
        mVCellLine      = itemView.findViewById(R.id.v_cell_line);

        //이체,충전 버튼 영역
        mLayoutBtn  = itemView.findViewById(R.id.layout_btn);
        mLayoutBtnText = itemView.findViewById(R.id.tv_btn_1);
    }

    public void onBindView(int groupPos, OpenBankDataMgr.OpenBankGroupInfo groupInfo, int accountPos,OnOpenBankActionListener listener) {
        Logs.e("onBindView --------");
        mListener = listener;

        OpenBankAccountInfo accountInfo = groupInfo.getAccountList().get(accountPos);

        Logs.e("accountInfo.PROD_NM : " + accountInfo.PROD_NM);
        Logs.e("accountInfo.ACNO_REG_STCD : " + accountInfo.ACNO_REG_STCD);

        mVCellLine.setVisibility(View.GONE);



        if(TextUtils.isEmpty(accountInfo.ACCO_ALS)){
            mTvAccName.setText(accountInfo.PROD_NM);
        }else{
            mTvAccName.setText(accountInfo.ACCO_ALS);
        }



        String accountNum = accountInfo.ACNO;
        if(accountInfo.RPRS_FNLT_CD.equalsIgnoreCase("000")||accountInfo.RPRS_FNLT_CD.equalsIgnoreCase("028")){
            accountNum = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
        }
        mTvAccNum.setText(accountInfo.BANK_NM + " " + accountNum);

        int radius = (int) Utils.dpToPixel(mContext,20);
        if(groupInfo.getGroupCd().equalsIgnoreCase("1")){ //입출금계좌 그룹
            String bgColor = "#4D"+accountInfo.TM_CLR_VAL.replace("#","");
            mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable(bgColor,new int[]{radius,radius,radius,radius}));
            mLayoutBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#08000000",new int[]{0,0,radius,radius}));

            setCellMargin(0,16);

            //계좌조회가 정상이 아니면 이체버튼 보이지 않도록.
            if(!accountInfo.ACNO_REG_STCD.equalsIgnoreCase("1")){
                mLayoutBtn.setVisibility(View.GONE);
            }else{
                mLayoutBtn.setVisibility(View.VISIBLE);
            }
        }else{ //그밖의 예금,적금,수입증권 그룹
            mLayoutBtn.setVisibility(View.GONE);
            mVCellLine.setVisibility(View.VISIBLE);

            if(accountPos == 0){
                if(groupInfo.getGroupAccCnt() == 1) {
                    mVCellLine.setVisibility(View.INVISIBLE);
                    mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff", new int[]{radius, radius, radius, radius}));
                    setCellMargin(0,16);
                }else {
                    mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff", new int[]{radius, radius, 0, 0}));
                    setCellMargin(0,0);
                }
            }else if(accountPos+1 == groupInfo.getAccountList().size()){
                mVCellLine.setVisibility(View.INVISIBLE);
                mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{0,0,radius,radius}));
                setCellMargin(0,16);
            }else{
                mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{0,0,0,0}));
                setCellMargin(0,0);
            }
        }

        //은행 아이콘 출력
        Uri iconUri = ImgUtils.getIconUri(mContext,accountInfo.RPRS_FNLT_CD,accountInfo.DTLS_FNLT_CD);
        if (iconUri != null)
            mIvBankIcon.setImageURI(iconUri);
        else
            mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);

        //거래내역으로 이동.
        mLayoutSubBody.setClickable(true);
        mLayoutSubBody.setTag(accountInfo);
        mLayoutSubBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutSubBody.setClickable(false);
                final OpenBankAccountInfo accountInfo = (OpenBankAccountInfo)v.getTag();

                if(!accountInfo.isFinishSearchAmount) return;

                if(accountInfo.ACNO_REG_STCD.equalsIgnoreCase("2")){
                    //연결실패
                    DialogUtil.alert(mContext, "재연결하기", "해지하기", "연결되지 않은 계좌입니다.\n해당계좌를 재연결하시겠습니까?",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(mContext, WebMainActivity.class);
                                    String url = WasServiceUrl.MAI0090400.getServiceUrl();
                                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                                    StringBuilder builder = new StringBuilder();
                                    builder.append("ACNO=" + accountInfo.ACNO);
                                    builder.append("&RPRS_FNLT_CD=" + accountInfo.RPRS_FNLT_CD);
                                    builder.append("&ACNO_REG_STCD=" + accountInfo.ACNO_REG_STCD);
                                    builder.append("&OBA_ACCO_KNCD=" + accountInfo.OBA_ACCO_KNCD);
                                    builder.append("&DTLS_FNLT_CD=" + accountInfo.DTLS_FNLT_CD);
                                    builder.append("&BLNC_AMT=" + accountInfo.BLNC_AMT);
                                    builder.append("&NEW_DT=" + accountInfo.NEW_DT);
                                    builder.append("&ACCO_ALS=" + accountInfo.ACCO_ALS);
                                    builder.append("&PROD_NM=" + accountInfo.PROD_NM);

                                    String param =  builder.toString();
                                    intent.putExtra(Const.INTENT_PARAM, param);
                                    mContext.startActivity(intent);
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    requestElectroSignData((Activity)mContext,accountInfo);
                                }
                            });

                }else{
                    //계좌조회가 정상일때만 거래내역으로 이동
                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    if(accountInfo.OBA_ACCO_KNCD.equalsIgnoreCase("2")){
                        String url = WasServiceUrl.INQ0080100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    }else{
                        String url = WasServiceUrl.INQ0070100.getServiceUrl();
                        intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    }


                    StringBuilder builder = new StringBuilder();
                    builder.append("ACNO=" + accountInfo.ACNO);
                    builder.append("&RPRS_FNLT_CD=" + accountInfo.RPRS_FNLT_CD);
                    builder.append("&ACNO_REG_STCD=" + accountInfo.ACNO_REG_STCD);
                    builder.append("&OBA_ACCO_KNCD=" + accountInfo.OBA_ACCO_KNCD);
                    builder.append("&BLNC_AMT=" + accountInfo.BLNC_AMT);
                    builder.append("&NEW_DT=" + accountInfo.NEW_DT);
                    if(!TextUtils.isEmpty(accountInfo.DTLS_FNLT_CD))
                        builder.append("&DTLS_FNLT_CD=" + accountInfo.DTLS_FNLT_CD);


                    String param =  builder.toString();
                    intent.putExtra(Const.INTENT_PARAM, param);
                    mContext.startActivity(intent);
                }
            }
        });

        //통합이체로 이동.
        mLayoutBtn.setClickable(true);
        mLayoutBtn.setTag(accountInfo);
        mLayoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutBtn.setClickable(false);
                final OpenBankAccountInfo accountInfo = (OpenBankAccountInfo)v.getTag();
                TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true,new TransferUtils.OnCheckFinishListener() {
                    @Override
                    public void onCheckFinish() {

                        //통합이체 화면으로 이동하기 전에 출금계좌정보를 등록해둔다.
                        ITransferDataMgr.getInstance().setWTCH_BANK_CD(accountInfo.RPRS_FNLT_CD);
                        ITransferDataMgr.getInstance().setWTCH_DTLS_BANK_CD(accountInfo.DTLS_FNLT_CD);
                        ITransferDataMgr.getInstance().setWTCH_ACNO(accountInfo.ACNO);
                        ITransferDataMgr.getInstance().setWTCH_BANK_NM(accountInfo.BANK_NM); //풀네임.
                        ITransferDataMgr.getInstance().setWTCH_BANK_ALS(accountInfo.MNRC_BANK_NM);//단축네임.

                        Intent intent = new Intent(mContext, ITransferSelectReceiverActivity.class);
                        mContext.startActivity(intent);
                    }
                });

            }
        });

        //비동기 잔액조회를 하기때문에 금액 표시는 마지막에 한다.

        if(!accountInfo.isFinishSearchAmount){
            mTvAmount.setText("");
            mTvWon.setVisibility(View.GONE);
            mIvArrowCircle.setVisibility(View.VISIBLE);
            mIvArrowCircle.setImageResource(R.drawable.ico_loading);
            mLayoutAccState.setVisibility(View.GONE);
            mLayoutBtn.setClickable(false);
            mLayoutBtnText.setTextColor(Color.parseColor("#33000000"));

            Animation anim = AnimationUtils.loadAnimation(
                    mContext, // 현재 화면의 제어권자
                    R.anim.rotate);    // 설정한 에니메이션 파일
            mIvArrowCircle.startAnimation(anim);

            return;
        }else{
            mIvArrowCircle.clearAnimation();
            mLayoutBtn.setClickable(true);
            mLayoutBtnText.setTextColor(Color.parseColor("#000000"));
        }

        radius = (int) Utils.dpToPixel(mContext,10);
        mLayoutAccState.setBackground(GraphicUtils.getRoundCornerDrawable("#1A293542",new int[]{radius,radius,radius,radius}));

        if(accountInfo.ACNO_REG_STCD.equalsIgnoreCase("1")){
            //잔액
            String BLNC = "0";
            if(!TextUtils.isEmpty(accountInfo.BLNC_AMT)){
                BLNC = Utils.moneyFormatToWon(Double.valueOf(accountInfo.BLNC_AMT));
            }
            mTvAmount.setText(BLNC);
            mTvWon.setVisibility(View.VISIBLE);
            mIvArrowCircle.setVisibility(View.GONE);
            mLayoutAccState.setVisibility(View.GONE);
        }else{
            mTvWon.setVisibility(View.GONE);
            mTvAmount.setText("다시시도");
            mTvAmount.setTypeface(Typeface.DEFAULT_BOLD);
            mIvArrowCircle.setImageResource(R.drawable.btn_arrincircle);
            mIvArrowCircle.setVisibility(View.VISIBLE);
            mLayoutAccState.setVisibility(View.VISIBLE);
            if(accountInfo.ACNO_REG_STCD.equalsIgnoreCase("2")){
                mTvAccState.setText("연결실패");
            }else if(accountInfo.ACNO_REG_STCD.equalsIgnoreCase("3")){
                mTvAccState.setText("장애");
            }
        }
    }

    private void setCellMargin(int top_dp,int bottom_dp){
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)mLayoutBody.getLayoutParams();
        params.topMargin = (int)Utils.dpToPixel(mContext,top_dp);
        params.bottomMargin = (int) Utils.dpToPixel(mContext,bottom_dp);
        mLayoutBody.setLayoutParams(params);
    }

    //연결 해지시 사인데이타 요청
    private void requestElectroSignData(final Activity activity, final OpenBankAccountInfo accountInfo){

        Map param = new HashMap();
        param.put("BANK_NM",accountInfo.BANK_NM);
        param.put("ACNO",accountInfo.ACNO);

        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A06.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    String ELEC_SGNR_VAL_CNTN = Utils.getJsonString(object, "ELEC_SGNR_VAL_CNTN");

                    if (DataUtil.isNotNull(ELEC_SGNR_VAL_CNTN)) {
                        //삭제하는 계좌의 정보를 보내둔다.
                        mListener.revokeAccountInfo(accountInfo);

                        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        commonUserInfo.setSignData(ELEC_SGNR_VAL_CNTN);
                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());

                        Intent intent = new Intent(activity, PincodeAuthActivity.class);
                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "010");//오픈뱅킹 업무코드
                        intent.putExtra(Const.INTENT_TRN_CD, "EIF55015");
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        activity.startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH_REVOKE_CONNECT);
                        activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);

                    }
                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });

    }
}
