package com.sbi.saidabank.activity.transaction;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.adater.ITransferCompleteListAdapter;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.TransferGroupNameDialog;
import com.sbi.saidabank.common.dialog.TransferReceiptDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * TransferCompleteActivity : 이체완료 팝업
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-03-26
 */
public class ITransferCompleteActivity extends BaseActivity {

    private final int MAX_TRANSFER_COUNT = 5;
    private String mName;
    private String mBalance;
    private String mWithdrawName;
    private String mWithdrawable;
    private String mBankName;
    private Button mBtPositive;
    private Button mBtNegative;
    private ImageButton mBtReciept;
    private ArrayList<TransferReceiptInfo> mTransferReceiptInfos;
    private ArrayList<TransferReceiptInfo> mTransferReceiptInfosForReciet;//완료이체증
    private ITransferCompleteListAdapter mITransferCompleteListAdapter;
    private RecyclerView mRvTransferList;
    private LinearLayout mLayoutAmountInfo;

    //오픈뱅킹 잔액정보
    private LinearLayout mLayoutAmountOpenBankInfo;
    private TextView       mTvLimitOneDayOpenBank;

    //그룹정보관련
    private String mGrpSerialNo;
    private String mGrpRevsYn;
    private String mGrpName;
    private RelativeLayout mLayoutGrp;
    private TextView       mTvGrpTitle;
    private TextView       mTvGrpSubTitle;
    private ImageView      mIvGrpImageArrow;
    private ImageView      mIvGrpImageCheck;

    private int mFailCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_transfer_complete);

        if (DataUtil.isNull(getIntent()))
            return;

        Intent intent = getIntent();
        mName = intent.getStringExtra(Const.INTENT_LIST_TRANSFER_NAME);
        mBalance = intent.getStringExtra(Const.INTENT_TRANSFER_BALANCE);
        mWithdrawName = intent.getStringExtra(Const.INTENT_WITHDRAW_NAME);
        mWithdrawable = intent.getStringExtra(Const.INTENT_WITHDRAWABLE_AMOUNT);
        mBankName = intent.getStringExtra(Const.INTENT_BANK_NM);
        mGrpSerialNo = intent.getStringExtra(Const.INTENT_GRP_SERIAL_NO);
        mGrpRevsYn = intent.getStringExtra(Const.INTENT_GRP_REVS_YN);
        if(TextUtils.isEmpty(mGrpRevsYn)) mGrpRevsYn = "X";

        mTransferReceiptInfos = new ArrayList<TransferReceiptInfo>();

        ArrayList<TransferReceiptInfo> transferReceiptArrayInfos = (ArrayList<TransferReceiptInfo>) intent.getSerializableExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO);
        if(transferReceiptArrayInfos != null && transferReceiptArrayInfos.size() > 0){
            mTransferReceiptInfos.addAll(transferReceiptArrayInfos);
        }

        if (DataUtil.isNull(mTransferReceiptInfos) || mTransferReceiptInfos.isEmpty() || mTransferReceiptInfos.size() > MAX_TRANSFER_COUNT)
            return;

        mTransferReceiptInfosForReciet = new ArrayList<>();
        for (TransferReceiptInfo item : mTransferReceiptInfos) {
            // 성공한 즉시이체 건만 이체완료증 화면으로 전달
            if ("1".equals(item.getTRNF_DVCD()) && ("NEFN0000".equals(item.getRESP_CD())) || ("XEKM0000".equals(item.getRESP_CD()))) {
                mTransferReceiptInfosForReciet.add(item);
            }
        }

        initUX();
    }

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ScrollView scrollView = (ScrollView) findViewById(R.id.sv_main);
                scrollView.scrollTo(0,0);
            }
        },100);
    }

    /**
     * 화면 초기화
     */
    private void initUX() {

        mBtPositive = (Button) findViewById(R.id.btn_confirm);
        mBtNegative = (Button) findViewById(R.id.btn_cancel);
        mBtReciept = (ImageButton) findViewById(R.id.btn_download);

        mRvTransferList = (RecyclerView) findViewById(R.id.recyclerview_transfercomplete);

        //계좌 잔액여부
        mLayoutAmountInfo = findViewById(R.id.ll_amount_info);
        mLayoutAmountOpenBankInfo = findViewById(R.id.ll_ammount_openbank);
        mTvLimitOneDayOpenBank = findViewById(R.id.tv_limit_oneday);

        //그룹정보 화면 표시
        mLayoutGrp = findViewById(R.id.layout_group);
        mTvGrpTitle = findViewById(R.id.tv_grp_title);
        mTvGrpSubTitle = findViewById(R.id.tv_grp_subtitle);
        mIvGrpImageArrow = findViewById(R.id.iv_grp_arrow);
        mIvGrpImageCheck = findViewById(R.id.iv_grp_check);
        mIvGrpImageCheck.setTag(false);

        //헤더 타이틀
        makeHeaderTitleArea();

        //리스트와 어댑터를 만든다.
        makeListInfo();

        //계좌 잔액 표시
        makeRemainAmountArea();

        //그룹등록 - 에러가 없을때만 나타나도록한다.
        if(mFailCount == 0){
            makeGrpInfoArea();
        }

        //바닥 버튼
        makeBottomButton();

    }

    private void makeHeaderTitleArea(){
        TextView textCountBracket = (TextView) findViewById(R.id.tv_transfer_count_bracket);
        TextView textCount = (TextView) findViewById(R.id.tv_transfer_count);
        TextView textTotlaCount = (TextView) findViewById(R.id.tv_total_transfercount);
        ImageView imageSuccess = (ImageView) findViewById(R.id.iv_transfer_success);
        ImageView imageFail = (ImageView) findViewById(R.id.iv_transfer_fail);
        TextView textTransferComlete = (TextView) findViewById(R.id.tv_transfercomlete);
        TextView textMsg = (TextView) findViewById(R.id.textview_transfer_msg);


        if(mTransferReceiptInfos.size() == 0){
            textTransferComlete.setText("이체실패");
            return;
        }



        int countImmediately = 0;
        for (TransferReceiptInfo item : mTransferReceiptInfos) {
            if (DataUtil.isNull(item))
                continue;

            String RESP_CD = item.getRESP_CD();
            if (TextUtils.isEmpty(RESP_CD))
                continue;

            String TRNF_DVCD = item.getTRNF_DVCD();
            if ("NEFN0000".equalsIgnoreCase(RESP_CD) || "XEKM0000".equalsIgnoreCase(RESP_CD)) {
                if ("1".equalsIgnoreCase(TRNF_DVCD))
                    countImmediately++;
            } else {
                mFailCount++;
            }
        }



        if (mFailCount == 0) {
            imageSuccess.setVisibility(View.VISIBLE);
            imageFail.setVisibility(View.GONE);
            if (mTransferReceiptInfos.size() > 1) {
                textCount.setText(String.valueOf(mTransferReceiptInfos.size()));
                textTotlaCount.setText("/" + mTransferReceiptInfos.size() + ")");
                if (countImmediately == 0) {
                    textTransferComlete.setText(R.string.msg_transfer_reserv_complete);
                    textMsg.setText(R.string.msg_transfer_complete_reservation_desc);
                } else {
                    textTransferComlete.setText(R.string.msg_transfer_complete);
                    textMsg.setText(R.string.msg_transfer_complete_desc);
                }
            } else if (mTransferReceiptInfos.size() == 1) {
                textCountBracket.setVisibility(View.GONE);
                textCount.setVisibility(View.GONE);
                textTotlaCount.setVisibility(View.GONE);
                String TRNF_DVCD = mTransferReceiptInfos.get(0).getTRNF_DVCD();
                if (!"1".equals(TRNF_DVCD)) {
                    textTransferComlete.setText(R.string.msg_transfer_reserv_complete);
                    mBtReciept.setVisibility(View.GONE);
                    textMsg.setText(R.string.msg_transfer_complete_reservation_desc);
                } else {
                    textTransferComlete.setText(R.string.msg_transfer_complete);
                    textMsg.setText(R.string.msg_transfer_complete_desc);
                }
            }
        } else {
            if (mTransferReceiptInfos.size() > 1) {
                textCount.setTextColor(getResources().getColor(R.color.red));
                if (mFailCount == mTransferReceiptInfos.size()) {
                    textTransferComlete.setText("이체실패");
                    textMsg.setText(R.string.msg_transfer_fail_all_desc);
                    mBtReciept.setVisibility(View.GONE);
                } else {
                    textTransferComlete.setText("이체 일부실패");
                    textMsg.setText(R.string.msg_transfer_fail_desc);
                }

                textCount.setText(String.valueOf(mFailCount));
                textTotlaCount.setText("/" + mTransferReceiptInfos.size() + ")");
            } else if (mTransferReceiptInfos.size() == 1) {
                textCountBracket.setVisibility(View.GONE);
                textCount.setVisibility(View.GONE);
                textTotlaCount.setVisibility(View.GONE);

                textTransferComlete.setText("이체실패");

                TransferReceiptInfo receiptInfo = mTransferReceiptInfos.get(0);
                if (receiptInfo != null) {
                    String RESP_CD = receiptInfo.getRESP_CD();
                    if (!TextUtils.isEmpty(RESP_CD) &&
                            (!"NEFN0000".equalsIgnoreCase(RESP_CD) &&
                                    !"XEKM0000".equalsIgnoreCase(RESP_CD))) {
                        String RESP_CNTN = receiptInfo.getRESP_CNTN();
                        if (!TextUtils.isEmpty(RESP_CNTN))
                            textMsg.setText(RESP_CNTN);
                    }
                }

                mBtReciept.setVisibility(View.GONE);
            }

            imageSuccess.setVisibility(View.GONE);
            imageFail.setVisibility(View.VISIBLE);
            textMsg.setVisibility(View.VISIBLE);
        }

        TextView textAccountName = (TextView) findViewById(R.id.tv_account_name);
        if (!TextUtils.isEmpty(mWithdrawName))
            textAccountName.setText(mWithdrawName);
    }

    private void makeListInfo(){
        if(mTransferReceiptInfos.size() == 0){
            return;
        }
        mITransferCompleteListAdapter = new ITransferCompleteListAdapter(ITransferCompleteActivity.this,mTransferReceiptInfos,
                new ITransferCompleteListAdapter.FavoriteListener() {
                    @Override
                    public void OnFavoriteListener(int position) {
                        MLog.d();
                        requestFavoriteAccount(position);
                    }
                },
                new ITransferCompleteListAdapter.ShareListener() {
                    @Override
                    public void OnShareListener(int position) {
                        MLog.d();
                        Intent share = new Intent(Intent.ACTION_SEND);
                        share.setType("text/plain");
                        //String text = "공유할 내용";
                        TransferReceiptInfo transferReceiptInfo = mTransferReceiptInfos.get(position);
                        StringBuilder shareText = new StringBuilder();
                        shareText.append("[사이다뱅크]\n");
                        String TRNF_DT = transferReceiptInfo.getTRNF_DT();
                        String TRNF_MT = transferReceiptInfo.getTRNF_TM();
                        if (!TextUtils.isEmpty(TRNF_DT) && !TextUtils.isEmpty(TRNF_MT)) {
                            String year = TRNF_DT.substring(0, 4);
                            String month = TRNF_DT.substring(4, 6);
                            String day = TRNF_DT.substring(6, 8);
                            String time = TRNF_MT.substring(0, 2);
                            String min = TRNF_MT.substring(2, 4);

                            String date = year + "." + month + "." + day + " " + time + ":" + min;
                            shareText.append(date);
                        }
                        shareText.append("\n");
                        StringBuilder strmyNameBuilder = new StringBuilder();
                        String myName = LoginUserInfo.getInstance().getCUST_NM();
                        if (!TextUtils.isEmpty(myName) && myName.length() >= 2) {
                            strmyNameBuilder.append(myName);
                            if (myName.length() == 2) {
                                strmyNameBuilder.replace(1, 1, "*");
                            } else {
                                strmyNameBuilder.replace(1, myName.length() - 1, "*");
                            }
                            shareText.append(strmyNameBuilder.toString() + "님(");
                        } else {
                            shareText.append("OOO님(");
                        }
                        String WTCH_NO = transferReceiptInfo.getWTCH_ACNO();
                        if (!TextUtils.isEmpty(WTCH_NO) && WTCH_NO.length() > 4) {
                            WTCH_NO = WTCH_NO.substring(WTCH_NO.length() - 4, WTCH_NO.length());
                            shareText.append(WTCH_NO);
                        }
                        shareText.append(")계좌에서\n");
                        StringBuilder strDeprNameBuilder = new StringBuilder();
                        String CNTP_ACCO_DEPR_NM = transferReceiptInfo.getCNTP_ACCO_DEPR_NM();
                        if (!TextUtils.isEmpty(CNTP_ACCO_DEPR_NM) && CNTP_ACCO_DEPR_NM.length() >= 2) {
                            strDeprNameBuilder.append(CNTP_ACCO_DEPR_NM);
                            if (CNTP_ACCO_DEPR_NM.length() == 2) {
                                strDeprNameBuilder.replace(1, 1, "*");
                            } else {
                                strDeprNameBuilder.replace(1, CNTP_ACCO_DEPR_NM.length() - 1, "*");
                            }
                            shareText.append(strDeprNameBuilder.toString() + "님 ");
                        } else {
                            shareText.append("OOO님 ");
                        }
                        String MNRC_BANK_NM = transferReceiptInfo.getMNRC_BANK_NM();
                        if (!TextUtils.isEmpty(MNRC_BANK_NM))
                            shareText.append(MNRC_BANK_NM);

                        shareText.append(" 계좌로\n");

                        String TRN_AMT = transferReceiptInfo.getTRN_AMT();
                        if (!TextUtils.isEmpty(TRN_AMT)) {
                            TRN_AMT = Utils.moneyFormatToWon(Double.valueOf(TRN_AMT));
                            shareText.append(TRN_AMT);
                        }
                        shareText.append("원을 이체하였습니다.");

                        share.putExtra(Intent.EXTRA_TEXT, shareText.toString());
                        Intent chooser = Intent.createChooser(share, "이체내역 공유");
                        startActivity(chooser);
                    }
                });
        mRvTransferList.setAdapter(mITransferCompleteListAdapter);
        mRvTransferList.setLayoutManager(new LinearLayoutManager(ITransferCompleteActivity.this, LinearLayoutManager.VERTICAL, false));
        mRvTransferList.setHasFixedSize(true);

    }

    private void makeRemainAmountArea(){
        if(mTransferReceiptInfos.size() == 0){
            return;
        }


        String bankCd = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        if(bankCd.equalsIgnoreCase("000") || bankCd.equalsIgnoreCase("028")){
            //사이다 잔액여부 출력
            mLayoutAmountInfo.setVisibility(View.VISIBLE);
            mLayoutAmountOpenBankInfo.setVisibility(View.GONE);

            String amount = DataUtil.isNotNull(mBalance) ? Utils.moneyFormatToWon(Double.parseDouble(mBalance)) : "0";
            ((TextView) findViewById(R.id.tv_balance)).setText(amount);

            String withdrawableAmount = DataUtil.isNotNull(mWithdrawable) ? Utils.moneyFormatToWon(Double.parseDouble(mWithdrawable)) : "0";
            ((TextView) findViewById(R.id.tv_withdrawbleamount)).setText(withdrawableAmount);
        }else{
            //오픈뱅킹 잔액여부 출력
            mLayoutAmountInfo.setVisibility(View.GONE);
            mLayoutAmountOpenBankInfo.setVisibility(View.VISIBLE);

            String WTCH_LMIT_REMN_AMT = mTransferReceiptInfos.get(0).getWTCH_LMIT_REMN_AMT();
            String remainOnedayAmount = Utils.moneyFormatToWon(Double.parseDouble(WTCH_LMIT_REMN_AMT));
            mTvLimitOneDayOpenBank.setText(remainOnedayAmount);
        }
    }

    private void makeGrpInfoArea(){


        if(mTransferReceiptInfos.size() > 1 && !TextUtils.isEmpty(mGrpRevsYn) && mGrpRevsYn.equalsIgnoreCase("N")){
            mLayoutGrp.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TransferGroupNameDialog dialog = new TransferGroupNameDialog(ITransferCompleteActivity.this,mGrpName,
                            new TransferGroupNameDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress(String name) {
                                    mGrpName = name;
                                    requestRegGroup(mGrpName);
                                }
                            });
                    dialog.show();
                }
            });
        }

        mIvGrpImageCheck.setTag(true);
        mIvGrpImageCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean flag = (boolean)v.getTag();
                if(flag){
                    mIvGrpImageCheck.setImageResource(R.drawable.btn_transfer_radio_off);
                }else{
                    mIvGrpImageCheck.setImageResource(R.drawable.btn_transfer_radio_on);
                }
                mIvGrpImageCheck.setTag(!flag);
            }
        });

        mLayoutGrp.setVisibility(View.VISIBLE);
        if(mTransferReceiptInfos.size()==1 || mGrpRevsYn.equalsIgnoreCase("X")){
            mLayoutGrp.setVisibility(View.GONE);
        }else if(mGrpRevsYn.equalsIgnoreCase("N")){
            mIvGrpImageArrow.setVisibility(View.VISIBLE);
            mIvGrpImageCheck.setVisibility(View.GONE);
        }else{
            mTvGrpTitle.setText("그룹이체 수정");
            mTvGrpSubTitle.setText("변경된 이체내용으로 수정하시겠습니까?");
            mIvGrpImageArrow.setVisibility(View.GONE);
            mIvGrpImageCheck.setImageResource(R.drawable.btn_transfer_radio_on);
        }
    }

    private void makeBottomButton(){
        //하단 버튼 설정

        mBtPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 이체시작 전 화면으로 이동

                boolean flag = (boolean)mIvGrpImageCheck.getTag();
                if(!TextUtils.isEmpty(mGrpRevsYn) && mGrpRevsYn.equalsIgnoreCase("Y")&& flag){
                    requestModifyGroup();
                }else{
                    finish();
                }

            }
        });

        //이체 계속하기
        mBtNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //이전 이체한 출금계좌를 가지고 있는다.
                final String BANKCD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
                final String ACNO   = ITransferDataMgr.getInstance().getWTCH_ACNO();
                TransferUtils.initAndSyncSessionAndCheckAuthMethod(ITransferCompleteActivity.this, true,new TransferUtils.OnCheckFinishListener() {
                    @Override
                    public void onCheckFinish() {
                        // 이체 초기 화면으로 이동
                        ITransferDataMgr.getInstance().clearTransferData();
                        ITransferDataMgr.getInstance().setWTCH_BANK_CD(BANKCD);
                        ITransferDataMgr.getInstance().setWTCH_ACNO(ACNO);
                        Intent intent = new Intent(ITransferCompleteActivity.this, ITransferSelectReceiverActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });

        //영수증
        mBtReciept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mTransferReceiptInfosForReciet.size() == 1) {
                    Intent intent = new Intent(ITransferCompleteActivity.this, TransferReceiptSaveActivity.class);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO, mTransferReceiptInfos.get(0));
                    intent.putExtra("name", mName);
                    intent.putExtra(Const.INTENT_BANK_NM, mBankName);
                    startActivity(intent);
                    overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                } else {
                    TransferReceiptDialog transferDialog = new TransferReceiptDialog(ITransferCompleteActivity.this, mTransferReceiptInfosForReciet, mName, mBankName);
                    transferDialog.show();
                }
            }
        });

        if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
            mBtReciept.setVisibility(View.GONE);
        }else{
            // 성공한 즉시이체 건이 없으면 이체완료증 버튼 숨기기
            mBtReciept.setVisibility(mTransferReceiptInfosForReciet.isEmpty() ? View.GONE : View.VISIBLE);
        }

    }

    /**
     * 즐겨찾기계좌관리 요청
     */
    private void requestFavoriteAccount(final int position) {
        MLog.d();

        final TransferReceiptInfo receiptInfo = (TransferReceiptInfo) mTransferReceiptInfos.get(position);
        if (receiptInfo == null)
            return;

        final String FAVO_ACCO_YN = receiptInfo.getFAVO_ACCO_YN();
        String CNTP_FIN_INST_CD = receiptInfo.getCNTP_FIN_INST_CD();
        String CNTP_BANK_ACNO = receiptInfo.getCNTP_BANK_ACNO();
        String CNTP_ACCO_DEPR_NM = receiptInfo.getCNTP_ACCO_DEPR_NM();

        if (TextUtils.isEmpty(FAVO_ACCO_YN) || TextUtils.isEmpty(CNTP_FIN_INST_CD) ||
                TextUtils.isEmpty(CNTP_BANK_ACNO) || TextUtils.isEmpty(CNTP_ACCO_DEPR_NM))
            return;

        Map param = new HashMap();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN))
            param.put("REG_STCD", "1");
        else
            param.put("REG_STCD", "0");

        param.put("CNTP_FIN_INST_CD", CNTP_FIN_INST_CD);
        param.put("CNTP_BANK_ACNO", CNTP_BANK_ACNO);
        param.put("CNTP_ACCO_DEPR_NM", CNTP_ACCO_DEPR_NM);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getString(R.string.msg_debug_no_response));
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, "", objectHead, true);
                        return;
                    }

                    /*if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN))
                        receiptInfo.setFAVO_ACCO_YN(Const.REQUEST_WAS_NO);
                    else
                        receiptInfo.setFAVO_ACCO_YN(Const.REQUEST_WAS_YES);
                    mTransferReceiptInfos.set(position, receiptInfo);*/

                    for (int index = 0; index < mTransferReceiptInfos.size(); index++) {
                        String chgCNTP_FIN_INST_CD = receiptInfo.getCNTP_FIN_INST_CD();
                        String chgCNTP_BANK_ACNO = receiptInfo.getCNTP_BANK_ACNO();

                        if (TextUtils.isEmpty(chgCNTP_FIN_INST_CD) || TextUtils.isEmpty(chgCNTP_BANK_ACNO))
                            continue;

                        TransferReceiptInfo destReceiptInfo = mTransferReceiptInfos.get(index);
                        if (destReceiptInfo == null)
                            continue;

                        String destCNTP_FIN_INST_CD = destReceiptInfo.getCNTP_FIN_INST_CD();
                        String destCNTP_BANK_ACNO = destReceiptInfo.getCNTP_BANK_ACNO();
                        if (TextUtils.isEmpty(destCNTP_FIN_INST_CD) || TextUtils.isEmpty(destCNTP_BANK_ACNO))
                            continue;

                        if (chgCNTP_FIN_INST_CD.equalsIgnoreCase(destCNTP_FIN_INST_CD) &&
                                chgCNTP_BANK_ACNO.equalsIgnoreCase(destCNTP_BANK_ACNO)) {
                            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(FAVO_ACCO_YN))
                                destReceiptInfo.setFAVO_ACCO_YN(Const.REQUEST_WAS_NO);
                            else
                                destReceiptInfo.setFAVO_ACCO_YN(Const.REQUEST_WAS_YES);

                            mTransferReceiptInfos.set(index, destReceiptInfo);
                        }
                    }

                    mITransferCompleteListAdapter.setListTransferReceiptInfos(mTransferReceiptInfos);
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 그룹등록
     */
    private void requestRegGroup(String name){
        Map param = new HashMap();
        param.put("GRP_REVS_YN", "N");
        param.put("GRP_SRNO", mGrpSerialNo);
        param.put("MNRC_ACCO_ALS", name);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);

                if(onCheckHttpError(ret,false)){
                    return;
                }

                DialogUtil.alert(ITransferCompleteActivity.this, "그룹이체 즐겨찾기 등록이 완료 되었습니다.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTvGrpTitle.setText("그룹이체 즐겨찾기 등록 완료 및 별명 수정");
                        mTvGrpSubTitle.setText("즐겨찾기로 등록한 그룹이체는 이체시 '받는분 선택' 화면에서 확인이 가능합니다.");
                    }
                });


            }
        });

    }

    /**
     * 그룹수정
     */
    private void requestModifyGroup(){
        Map param = new HashMap();
        param.put("GRP_REVS_YN", "Y");
        param.put("GRP_SRNO", mGrpSerialNo);

        try {
            JSONArray jsonArray = new JSONArray();
            for(int i=0;i<mTransferReceiptInfos.size();i++){
                TransferReceiptInfo info = mTransferReceiptInfos.get(i);

                JSONObject jsonObject = new JSONObject();

                jsonObject.put("MNRC_BANK_CD",info.getCNTP_FIN_INST_CD());
                jsonObject.put("MNRC_ACNO",info.getCNTP_BANK_ACNO());
                jsonObject.put("MNRC_AMT",info.getTRN_AMT());
                jsonObject.put("MNRC_ACCO_MRK_CNTN",info.getMNRC_ACCO_MRK_CNTN());
                jsonObject.put("WTCH_ACCO_MRK_CNTN",info.getWTCH_ACCO_MRK_CNTN());
                jsonObject.put("DEPR_NM",info.getCNTP_ACCO_DEPR_NM());
                jsonArray.put(jsonObject);
            }
            param.put("REC_GRP_IN", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190400A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                Logs.i("CMM0010700A02 : " + ret);

                if(onCheckHttpError(ret,false)){
                    return;
                }

                finish();

            }
        });

    }
}
