package com.sbi.saidabank.activity.main2.myaccountlist;


import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DivideAccountInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public abstract class BaseViewHolder<T> extends RecyclerView.ViewHolder {
    protected Context mContext;
    protected int mDispType;
    public BaseViewHolder(Context context,View itemView,int dispType) {
        super(itemView);
        mContext = context;
        mDispType = dispType;
    }
    public abstract void onBindView(T item);


    //커플통장일 경우 커플 체결 상태를 항상 체크해야 되어서 추가함.
    //커플이 아닐경우는 바로 이동 시킴.
    protected void startWebAction(Object mainAccountInfo,Main2AccountAdapter.OnProcWebActionListener listener){
        //커플계좌에서 들어와서 공유자이면
        if(mDispType == Main2AccountAdapter.DISP_TYPE_COUPLE && Main2DataInfo.getInstance().getSHRP_DVCD().equals("01")){
            requestCheckCoupleState(mainAccountInfo,listener);
        }else{
            listener.onOKWebAction(mainAccountInfo);
        }
    }


    /**
     * 커플계좌가 연결되어 있고 고유자인 경우 커플의 연결상태를 체크하여 다음동작을 진행하도록 한다.
     * 보통예금,계좌분리,적금,예금의 ViewHolder에서 호출해서 사용한다.
     * @param accountInfo
     * @param listener
     */
    protected void requestCheckCoupleState(final Object accountInfo, final Main2AccountAdapter.OnProcWebActionListener listener){
        Map param = new HashMap();

        if(accountInfo instanceof Main2AccountInfo){
            param.put("ACNO", ((Main2AccountInfo)accountInfo).getACNO());
        }else if(accountInfo instanceof Main2DivideAccountInfo){
            param.put("ACNO", ((Main2DivideAccountInfo)accountInfo).getACNO());
        }


        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A05.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if (DataUtil.isNull(ret)) {
                    ((BaseActivity)mContext).showErrorMessage(mContext.getString(R.string.msg_debug_no_response), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            updateMain2ViewPager();
                        }
                    });
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        ((BaseActivity)mContext).showErrorMessage(mContext.getString(R.string.msg_debug_err_response), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                updateMain2ViewPager();
                            }
                        });
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = mContext.getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);

                        if(errCode.equals("UNT0025")){
                            DialogUtil.alert(mContext, msg, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //에러가 발생한 경우는 공유를 해제해서 발생하는 경우가 대부분 이기에 여기서 공유상태를 확인 후에 웹으로 이동시키도록 한다.
                                    updateMain2ViewPager();
                                }
                            });
                        }else{
                            ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true,new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    //에러가 발생한 경우는 공유를 해제해서 발생하는 경우가 대부분 이기에 여기서 공유상태를 확인 후에 웹으로 이동시키도록 한다.
                                    updateMain2ViewPager();
                                }
                            });
                        }
                        return;
                    }
                    String SHRN_PRGS_STCD = object.getString("SHRN_PRGS_STCD");
                    if(!TextUtils.isEmpty(SHRN_PRGS_STCD) && SHRN_PRGS_STCD.equals("99")){
                        listener.onErrorWebAction(SHRN_PRGS_STCD,accountInfo);
                    }else{
                        listener.onOKWebAction(accountInfo);
                    }

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    //커플의 연결상태가 변한 경우 메인 커플탭을 새로 변경한다.
    private void updateMain2ViewPager(){
        if(!((Main2Activity)mContext).isFinishing())
            ((Main2Activity)mContext).forceReloadCoupleState(false);
    }
}