package com.sbi.saidabank.activity.main2.myaccountlist;

import android.content.Context;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;

import java.util.ArrayList;

/**
 * 메인의 Item Type은 3가지로 구분하도록 한다.
 * 1.일반적인 가장많이 사용하는 레이아웃
 * 2.통장쪼개기 레이아웃
 * 3.신용대출 금
 */
public class Main2AccountAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * 각각으로 분리는 해두었으나 보통예금, 정기예금, 적금,대출계좌는 하나의 xml을 사용한다.
     * 어짜피 화면은 모두 비슷하나 처리하는 내부에서 if문을 최소화 하고 종류별로 모듈화를 위해 분리했다.
     * */
    public static final int VIEW_TYPE_DEVIDE = 0;       // 계좌분리
    public static final int VIEW_TYPE_REGULAR = 1;      // 보통예금
    public static final int VIEW_TYPE_FDIS = 2;         // 정기예금,적금
    public static final int VIEW_TYPE_LOAN = 3;         // 대출계좌

    public static final int DISP_TYPE_MY = 0;           // 내계좌에서 만들어진경우
    public static final int DISP_TYPE_COUPLE = 1;       // 커플계좌에서 만들어진경

    private ArrayList<Main2AccountInfo> mAccountArray;
    private int mAccDispType;

    /**
     * 커플계좌탭의 계좌 리스트에서 공유자 일경우 커플연결 상태를 체크해야 되어서 리스너를 추가한다.
     */
    public interface OnProcWebActionListener {
        void onOKWebAction(Object accountInfo);
        void onErrorWebAction(String code, Object accountInfo);
    }

    public Main2AccountAdapter(Context context, int dispType) {
        this.mAccountArray = new ArrayList<>();
        this.mAccDispType = dispType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_DEVIDE:
                return ViewHolderDevide.newInstance(parent, mAccDispType);
            case VIEW_TYPE_REGULAR:
                return ViewHolderRegular.newInstance(parent, mAccDispType);
            case VIEW_TYPE_FDIS:
                return ViewHolderFDIS.newInstance(parent, mAccDispType);
            case VIEW_TYPE_LOAN:
                return ViewHolderLoan.newInstance(parent, mAccDispType);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Main2AccountInfo item = getItem(position);
        if (holder instanceof ViewHolderRegular) {
            ((ViewHolderRegular) holder).onBindView(item);
        } else if (holder instanceof ViewHolderFDIS) {
            ((ViewHolderFDIS) holder).onBindView(item);
        } else if (holder instanceof ViewHolderLoan) {
            ((ViewHolderLoan) holder).onBindView(item);
        } else if (holder instanceof ViewHolderDevide) {
            ((ViewHolderDevide) holder).onBindView(item);
        }
    }

    @Override
    public int getItemCount() {
        return DataUtil.isNotNull(mAccountArray) ? mAccountArray.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        if (DataUtil.isNotNull(mAccountArray) && mAccountArray.size() > position) {
            Main2AccountInfo info = mAccountArray.get(position);
            // 300:보통예금_대표계좌 | 330:일반계좌(마이너스통장 포함)
            if (info.getDSCT_CD().equals("300") || info.getDSCT_CD().equals("330")) {
                // 계좌분리여부
                if (info.getACDIV_ACCO_YN().equals(Const.REQUEST_WAS_YES)) {
                    return VIEW_TYPE_DEVIDE;
                } else {
                    return VIEW_TYPE_REGULAR;
                }
            } else if (info.getDSCT_CD().equals("400") || info.getDSCT_CD().equals("500")) {
                // 정기예금 , 적금
                return VIEW_TYPE_FDIS;
            } else {
                // 대출계좌
                return VIEW_TYPE_LOAN;
            }
        } else {
            // 대출계좌
            return VIEW_TYPE_LOAN;
        }
    }

    private Main2AccountInfo getItem(int position) {
        return mAccountArray.get(position);
    }

    public void setMyAccountArray(ArrayList<Main2AccountInfo> accountArray) {
        if (!mAccountArray.isEmpty())
            mAccountArray.clear();
        mAccountArray.addAll(accountArray);
        notifyDataSetChanged();
    }
}
