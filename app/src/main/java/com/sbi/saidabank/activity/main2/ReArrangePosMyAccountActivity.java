package com.sbi.saidabank.activity.main2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.main2.rearrange.OnListItemDragListener;
import com.sbi.saidabank.activity.main2.rearrange.ReArrangeMyAccountAdapter;
import com.sbi.saidabank.activity.main2.rearrange.SimpleItemTouchHelperCallback;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.JsonUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ReArrangePosMyAccountActivity extends BaseActivity implements OnListItemDragListener {

    ArrayList<Main2AccountInfo> mAccountArray;
    private ReArrangeMyAccountAdapter mReArrangeMyAccountAdapter;
    private RecyclerView mListView;

    private ItemTouchHelper mItemTouchHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main2_myaccount_rearrange_pos);

        findViewById(R.id.btn_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mAccountArray = new ArrayList<Main2AccountInfo>();
        mAccountArray.addAll(Main2DataInfo.getInstance().getMain2Main2AccountInfo());

        mReArrangeMyAccountAdapter = new ReArrangeMyAccountAdapter(this,this);
        mReArrangeMyAccountAdapter.setAccountArray(mAccountArray);

        LinearLayout layoutBtnReset = findViewById(R.id.layout_btn_reset);
        layoutBtnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logs.e("reset click");
                mAccountArray.clear();
                mAccountArray.addAll(Main2DataInfo.getInstance().getMain2Main2AccountInfo());
                mReArrangeMyAccountAdapter.setAccountArray(mAccountArray);
            }
        });
        int radius = (int) Utils.dpToPixel(this,12f);
        layoutBtnReset.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,radius,radius},1,"#E5E5E5"));


        mListView = findViewById(R.id.listview);
        mListView.setAdapter(mReArrangeMyAccountAdapter);
        mListView.setLayoutManager(new LinearLayoutManager(this));

        Button btnSave = findViewById(R.id.btn_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestReArrange();
            }
        });

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(mReArrangeMyAccountAdapter);
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mListView);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }

    @Override
    public void onEndDrag(ArrayList<?> list) {
        mAccountArray.clear();
        mAccountArray.addAll((ArrayList<Main2AccountInfo>)list);

    }

    private void requestReArrange(){

        JSONArray jsonArray = new JSONArray();

        for(int i=0;i<mAccountArray.size();i++){
            Map param = new HashMap();
            param.put("ACNO", mAccountArray.get(i).getACNO());
            param.put("ACCO_IDNO", mAccountArray.get(i).getACCO_IDNO());
            param.put("SCRN_MRK_SQNC", i+1);
            JSONObject object = JsonUtils.mapToJson(param);

            jsonArray.put(object);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("REC_IN",jsonArray);
        } catch (JSONException e) {
            Logs.printException(e);
            return;
        }

        Logs.e("requestReArrange : " + jsonObject.toString());

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A03.getServiceUrl(), jsonObject.toString(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();
                if(onCheckHttpError(ret,false)) return;

                DialogUtil.alert(ReArrangePosMyAccountActivity.this, "설정한 순서로 저장되었습니다.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
            }
        });
    }
}
