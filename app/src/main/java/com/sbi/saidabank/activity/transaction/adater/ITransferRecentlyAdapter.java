package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.graphics.Typeface;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;

import java.util.ArrayList;
import java.util.Locale;

public class ITransferRecentlyAdapter extends BaseAdapter {

    private ArrayList<ITransferRecentlyInfo> mListRecentlyInfo;
    private Context mContext;
    private OnFavoriteItemClickListener mListener;

    public interface OnFavoriteItemClickListener {
        void onItemClick(int position);
    }

    public ITransferRecentlyAdapter(Context context, ArrayList<ITransferRecentlyInfo> listRecentlyInfo, OnFavoriteItemClickListener listener) {
        this.mContext = context;
        this.mListRecentlyInfo = new ArrayList<ITransferRecentlyInfo>();
        this.mListRecentlyInfo.addAll(listRecentlyInfo);
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return mListRecentlyInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return mListRecentlyInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ITransferRecentlyAdapter.AccountViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_recently_list_item, null);
            viewHolder = new ITransferRecentlyAdapter.AccountViewHolder();

            viewHolder.imageIcon = convertView.findViewById(R.id.iv_icon);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.tv_name);
            viewHolder.imageSaida = convertView.findViewById(R.id.iv_contacts_saida);
            viewHolder.textAccountNum = (TextView) convertView.findViewById(R.id.tv_account);
            viewHolder.layoutFavirte = (RelativeLayout) convertView.findViewById(R.id.layout_favorite_account);
            viewHolder.btnFavirte = (ImageView) convertView.findViewById(R.id.btn_favorite_account);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ITransferRecentlyAdapter.AccountViewHolder) convertView.getTag();
        }

        ITransferRecentlyInfo recentlyInfo = mListRecentlyInfo.get(position);

        if (DataUtil.isNull(recentlyInfo)) return null;



        //이름및 닉네임을 출력한다.
        String name = recentlyInfo.getMNRC_ACCO_DEPR_NM();
        String nicName = recentlyInfo.getMNRC_ACCO_ALS();
        if(!TextUtils.isEmpty(nicName)){
            name = name + "(" + nicName + ")";
        }
        Utils.setTextWithSpan(viewHolder.textName,name,nicName, Typeface.NORMAL,"#777777");

        //휴대폰 이체의 즐겨찾기가 있는지 체크
        String MNRC_TLNO = recentlyInfo.getMNRC_TLNO();
        if(!TextUtils.isEmpty(MNRC_TLNO)){
            String phoneNo = PhoneNumberUtils.formatNumber(MNRC_TLNO, Locale.getDefault().getCountry());
            viewHolder.textAccountNum.setText(phoneNo);
            viewHolder.imageIcon.setImageResource(R.drawable.ico_cellphone);
        }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")) {
            viewHolder.imageIcon.setImageResource(R.drawable.ico_group);
            String acno = recentlyInfo.getMNRC_ACNO(); //그룹일경우 acno에 그룹 참여자들이 표시된다.
            viewHolder.textAccountNum.setText(acno);
        }else{
            String bankName = recentlyInfo.getMNRC_BANK_NM();
            String acno = recentlyInfo.getMNRC_ACNO();
            String account = acno;
            if (DataUtil.isNotNull(acno)) {
                String MNRC_BANK_CD = recentlyInfo.getMNRC_BANK_CD();

                if (!TextUtils.isEmpty(MNRC_BANK_CD) && "000".equalsIgnoreCase(MNRC_BANK_CD) || "028".equalsIgnoreCase(MNRC_BANK_CD)) {
                    //bankName = recentlyInfo.getMNRC_BANK_NM();
                    account = acno.substring(0, 5) + "-" + acno.substring(5, 7) + "-" + acno.substring(7, acno.length());
                }
            }
            viewHolder.textAccountNum.setText(bankName + " " +account);

            //은행 아이콘 가져온다.
            String bankCd = recentlyInfo.getMNRC_BANK_CD();
            if (!TextUtils.isEmpty(bankCd)) {

                Uri iconUri = ImgUtils.getIconUri(mContext,bankCd,recentlyInfo.getDTLS_FNLT_CD());
                if (iconUri != null)
                    viewHolder.imageIcon.setImageURI(iconUri);
                else
                    viewHolder.imageIcon.setImageResource(R.drawable.img_logo_xxx);
            }
        }

        // 즐겨찾기 여부
        viewHolder.btnFavirte.setImageResource(
                DataUtil.isNotNull(recentlyInfo.getFAVO_ACCO_YN())
                        && Const.REQUEST_WAS_YES.equalsIgnoreCase(recentlyInfo.getFAVO_ACCO_YN())
                        ? R.drawable.btn_favorite_on
                        : R.drawable.btn_favorite_off
        );

        viewHolder.layoutFavirte.setTag(position);
        viewHolder.layoutFavirte.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                int pos = (int)v.getTag();
                mListener.onItemClick(pos);
            }
        });

        //당행여부를 표시한다.
        if(recentlyInfo.getOWBK_CUST_YN().equalsIgnoreCase("Y")){
            viewHolder.imageSaida.setVisibility(View.VISIBLE);
        }else{
            viewHolder.imageSaida.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void setRecentlyArrayList(ArrayList<ITransferRecentlyInfo> listRecentlyInfo) {
        this.mListRecentlyInfo.clear();
        this.mListRecentlyInfo.addAll(listRecentlyInfo);
        this.notifyDataSetChanged();
    }

    private static class AccountViewHolder {
        ImageView imageIcon;
        TextView textName;
        ImageView imageSaida;
        TextView textAccountNum;
        RelativeLayout layoutFavirte;
        ImageView btnFavirte;
    }
}
