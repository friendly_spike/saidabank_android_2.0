package com.sbi.saidabank.activity.ssenstone;

import android.Manifest;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.login.LogoutActivity;
import com.sbi.saidabank.activity.login.ReregLostPincodeActivity;
import com.sbi.saidabank.activity.main2.Main2Activity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.PinErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabanking_android
 *
 * Class: PincodeAuthActivity
 * Created by 950485 on 2018. 9. 6..
 * <p>
 * Description: 핀코드 인증 등록을 위한 화면
 */

public class PincodeAuthActivity extends BaseActivity implements View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener, View.OnClickListener, StonePassManager.StonePassListener {
	private static final int  mKeypadType 					    = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;
	private static final int  PINCODE_AUTH_STATE_CHANGE			= 0;
	private static final int  PINCODE_AUTH_STATE_MANAGE_BIO		= 1;
	private static final int  PINCODE_AUTH_STATE_MANAGE_ELSE	= 2;
	private static final int  PINCODE_AUTH_STATE_MANAGE_PINCODE	= 3;
	private static final int  PINCODE_AUTH_STATE_WEB			= 4;
	private static final int  PINCODE_AUTH_STATE_ELSE			= 5;
	private static final int  PINCODE_AUTH_STATE_FAIL			= 6;
	private static final long MIN_CLICK_INTERVAL 			    = 500;    // ms

	private LinearLayout  mLayoutMsg;
	private TextView	  mTextTitle;
	private TextView      mTextMsg;
	private EditText      mExitPincode;
	private EditText      mEditPincode01;
	private ImageView     mImagePincode01;
	private EditText      mEditPincode02;
	private ImageView     mImagePincode02;
	private EditText      mEditPincode03;
	private ImageView     mImagePincode03;
	private EditText      mEditPincode04;
	private ImageView     mImagePincode04;
	private EditText      mEditPincode05;
	private ImageView     mImagePincode05;
	private EditText      mEditPincode06;
	private ImageView     mImagePincode06;

	private boolean       mIsViewCtrlKeypad = false;
	private EntryPoint    mBackupEntryPoint;
	private String		  mServiceId;
	private String		  mBizDvCd;
	private String		  mTrnCd;
	private String		  mSsId;
	private long 	      mStartTime = 0;
	private long 		  mLastClickTime;

	private CommonUserInfo mCommonUserInfo;
	private int            mCountFail = 0;


	private TransKeyCtrl  mTKMgr = null;
	
	// 필요권한 - 전화접근권한
	private String[] perList = new String[]{
			Manifest.permission.READ_PHONE_STATE,
	};

	private static MsgHandler msgHandler = null;

    private static class MsgHandler extends Handler {
		private WeakReference<PincodeAuthActivity> weakReference;

		public MsgHandler(PincodeAuthActivity activity) {
			this.weakReference = new WeakReference<PincodeAuthActivity>(activity);
		}

		public void setTarget(PincodeAuthActivity target) {
			this.weakReference.clear();
			this.weakReference = new WeakReference<PincodeAuthActivity>(target);
		}

		@Override
		public void handleMessage(Message msg) {
			if (weakReference != null)
				weakReference.get().showMessage();
		}
	}

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_pincode_auth);

		if(msgHandler == null)
			msgHandler = new MsgHandler(this);
		else
			msgHandler.setTarget(this);

		mBackupEntryPoint = EntryPoint.NONE;
		mStartTime = System.currentTimeMillis();

		getExtra();
        initUX();

		requestCountPinError();

		PermissionUtils.checkPermission(this, perList, Const.REQUEST_PERMISSION_READ_PHONE_STATE);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (LoginUserInfo.getInstance().isCompletedReg()) {
			LoginUserInfo.getInstance().setCompletedReg(false);
			if (mBackupEntryPoint == EntryPoint.AUTH_TOOL_MANAGE_BIO) {
				setResult(RESULT_OK);
			}
			finish();
		} else if (mBackupEntryPoint == EntryPoint.NONE) {
			showKeyPad();
		} else if (mBackupEntryPoint == EntryPoint.LOGIN_BIO
				|| mBackupEntryPoint == EntryPoint.PATTERN_FORGET
				|| mBackupEntryPoint == EntryPoint.WEB_CALL
				|| mBackupEntryPoint == EntryPoint.TRANSFER
				|| mBackupEntryPoint == EntryPoint.TRANSFER_SAFEDEAL
				|| mBackupEntryPoint == EntryPoint.AUTH_TOOL_MANAGE_BIO
				|| mBackupEntryPoint == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {
			// 로그인 패턴,지문 재등록 중 핀코드 등록 완료 시 핀코드 인증 재시도
			mCommonUserInfo.setEntryPoint(mBackupEntryPoint);
			mBackupEntryPoint = EntryPoint.NONE;
			showKeyPad();
			requestCountPinError();
		} else {
			finish();
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		switch (requestCode) {
			case Const.REQUEST_PERMISSION_READ_PHONE_STATE :
			{
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {

					boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
					Logs.e("state : " + state);
					if(!state){
						DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_phone_state_allow), new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										PermissionUtils.goAppSettingsActivity(PincodeAuthActivity.this);
										finish();
									}
								},
								new View.OnClickListener(){
									@Override
									public void onClick(View v) {
										finish();
									}
								});
					}else{
						finish();
					}
				}
			}
			break;

			default:
				break;
		}
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    	if (mIsViewCtrlKeypad == false) {
	    		showKeyPad();
				return true;
		    }
    	}
		return false;
	}

	@Override
	public void cancel(Intent data) {
		finish();
	}

	@Override
	public void done(Intent data) {
		mIsViewCtrlKeypad = false;
		showKeyPad();
		if (data == null)
			return;

		long currentClickTime = SystemClock.uptimeMillis();
		long elapsedTime = currentClickTime - mLastClickTime;
		mLastClickTime = currentClickTime;

		if (elapsedTime <= MIN_CLICK_INTERVAL) {
			// 중복클릭 방지
			return;
		}

		if (mCountFail >= Const.SSENSTONE_AUTH_COUNT_FAIL) {
			new Thread() {
				@Override
				public void run() {
					Message message = msgHandler.obtainMessage();
					msgHandler.sendMessage(message);
				}
			}.start();
			return;
		}

		String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
		String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
		byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
		int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

		if (iRealDataLength != mCommonUserInfo.getPinLength()) {
			String msg = getString(R.string.message_pin_right_length, mCommonUserInfo.getPinLength());
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(msg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		String plainData = "";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(secureKey);

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "";
			}

			if (TextUtils.isEmpty(plainData)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_decode_pin);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}
		} catch (Exception e) {
			Logs.printException(e);
		}

		final String sendData = plainData;

		if (mCommonUserInfo.getOperation().equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
			showProgressDialog();

			new Thread() {
				public void run() {
					sendAuthSSenstone(sendData);
				}
			}.start();

		} else if (mCommonUserInfo.getOperation().equalsIgnoreCase(Const.SSENSTONE_DEREGISTER)) {
			String codeRet = StonePassManager.getInstance(getApplication()).deregistPin(plainData, null);
			if (codeRet.equals("P000")) {
				Prefer.setPincodeRegStatus(this,false);
				Toast.makeText(PincodeAuthActivity.this, "핀번호를 삭제했습니다.", Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(PincodeAuthActivity.this, "핀번호를 삭제 중 에러가 발생했습니다.", Toast.LENGTH_SHORT).show();
			}
			finish();
		}
	}
	
	@Override
	public void onBackPressed() {
		if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
			String msg = "";
			switch (mCommonUserInfo.getEntryPoint()) {
				case AUTH_TOOL_MANAGE_PINCODE_RESET: {
					msg = getResources().getString(R.string.msg_cancel_pin_errcount_reset);
					break;
				}

				case PATTERN_FORGET: {
					msg = getResources().getString(R.string.msg_cancel_pattern_rereg);
					break;
				}

				case AUTH_TOOL_MANAGE_PATTERN: {
					msg = getResources().getString(R.string.msg_cancel_pattern_change);
					break;
				}

				case LOGIN_BIO: {
					msg = getResources().getString(R.string.msg_cancel_bio_rereg);
					break;
				}

				case AUTH_TOOL_MANAGE_BIO: {
					msg = getResources().getString(R.string.msg_cancel_bio_change);
					break;
				}
				case PINCODE_FORGET: {
					switch (mCommonUserInfo.getEntryStart()) {
						case LOGIN_PATTERN: {
							msg = getResources().getString(R.string.msg_cancel_pattern_rereg);
							break;
						}
						case LOGIN_BIO: {
							msg = getResources().getString(R.string.msg_cancel_bio_rereg);
							break;
						}
					}
				}
				default:
					break;
			}

			if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO) {
				DialogUtil.alert(this,
						"나가기",
						"취소",
						msg,

						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								setResult(RESULT_CANCELED);
								finish();
							}
						},
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
							}
						});
			} else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
				DialogUtil.alert(this,
						"나가기",
						"취소",
						msg,

						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								Intent intent = new Intent(PincodeAuthActivity.this, Main2Activity.class);
								startActivity(intent);
								finish();
							}
						},
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
							}
						});
			} else {
				showCancelMessage(msg);
			}
			return;
		} else {
			setResult(RESULT_CANCELED);
			finish();
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
					||mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL)
				overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
		}
	}
	
	@Override
	public void input(int arg0) {
		if (arg0 == ITransKeyActionListenerEx.INPUT_CHARACTER_KEY) {
			mLayoutMsg.setVisibility(View.INVISIBLE);

			String strValue1 = mEditPincode01.getText().toString();
			String strValue2 = mEditPincode02.getText().toString();
			String strValue3 = mEditPincode03.getText().toString();
			String strValue4 = mEditPincode04.getText().toString();
			String strValue5 = mEditPincode05.getText().toString();

			if (TextUtils.isEmpty(strValue1)) {
				mEditPincode01.setText("0");
				mImagePincode01.setImageResource(R.drawable.ico_pin_on);
				mEditPincode02.requestFocus();
			} else if (TextUtils.isEmpty(strValue2)) {
				mEditPincode02.setText("0");
				mImagePincode02.setImageResource(R.drawable.ico_pin_on);
				mEditPincode03.requestFocus();
			} else if (TextUtils.isEmpty(strValue3)) {
				mEditPincode03.setText("0");
				mImagePincode03.setImageResource(R.drawable.ico_pin_on);
				mEditPincode04.requestFocus();
			} else if (TextUtils.isEmpty(strValue4)) {
				mEditPincode04.setText("0");
				mImagePincode04.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getPinLength() == 5)
					mEditPincode05.requestFocus();
			} else if (TextUtils.isEmpty(strValue5)) {
				mEditPincode05.setText("0");
				mImagePincode05.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getPinLength() == 6)
					mEditPincode06.requestFocus();
			} else {
				mEditPincode06.setText("0");
				mImagePincode06.setImageResource(R.drawable.ico_pin_on);
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_BACKSPACE_KEY) {
			String strValue1 = mEditPincode01.getText().toString();
			String strValue2 = mEditPincode02.getText().toString();
			String strValue3 = mEditPincode03.getText().toString();
			String strValue4 = mEditPincode04.getText().toString();
			String strValue5 = mEditPincode05.getText().toString();
			String strValue6 = mEditPincode06.getText().toString();

			if (mCommonUserInfo.getPinLength() == 6) {
				if (!TextUtils.isEmpty(strValue6)) {
					mEditPincode06.setText("");
					mImagePincode06.setImageResource(R.drawable.ico_pin_off);
					mEditPincode05.requestFocus();
					return;
				} else {
					if (!TextUtils.isEmpty(strValue5)) {
						mEditPincode05.setText("");
						mImagePincode05.setImageResource(R.drawable.ico_pin_off);
						mEditPincode04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getPinLength() == 5) {
				if (!TextUtils.isEmpty(strValue5)) {
					mEditPincode05.setText("");
					mImagePincode05.setImageResource(R.drawable.ico_pin_off);
					mEditPincode04.requestFocus();
					return;
				}
			}

			if (!TextUtils.isEmpty(strValue4)) {
				mEditPincode04.setText("");
				mImagePincode04.setImageResource(R.drawable.ico_pin_off);
				mEditPincode03.requestFocus();
			} else if (!TextUtils.isEmpty(strValue3)) {
				mEditPincode03.setText("");
				mImagePincode03.setImageResource(R.drawable.ico_pin_off);
				mEditPincode02.requestFocus();
			} else if (!TextUtils.isEmpty(strValue2)) {
				mEditPincode02.setText("");
				mImagePincode02.setImageResource(R.drawable.ico_pin_off);
				mEditPincode01.requestFocus();
			} else if (!TextUtils.isEmpty(strValue1)) {
				mEditPincode01.setText("");
				mImagePincode01.setImageResource(R.drawable.ico_pin_off);
			}
		}
		else if (arg0 == ITransKeyActionListenerEx.INPUT_CLEARALL_BUTTON) {
			clearInput();
		}
	}

	@Override
	public void minTextSizeCallback() {

	}

	@Override
	public void maxTextSizeCallback() {

	}

    @Override
    public void stonePassResult(String op, int errorCode, String errorMsg) {
        Logs.e("Pattern - stonePassResult : errorCode[" + errorCode + "], errorMsg[" + errorMsg + "]");

		//화면이 종료되면 리턴시킨다.
		if(isFinishing()) return;

        String msg ="";

        switch (errorCode) {
            case 1200: {
				Prefer.setPatternErrorCount(this, 0);
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                	switch (mCommonUserInfo.getEntryPoint()) {
						case AUTH_TOOL_MANAGE_PATTERN: {
							msg = getResources().getString(R.string.msg_ok_change_pattern);
							break;
						}

						case PATTERN_FORGET: {
							msg = getResources().getString(R.string.msg_ok_rereg_pattern);
							break;
						}

						default: {
							msg = getResources().getString(R.string.msg_ok_reg);
							break;
						}
					}

                    Prefer.setPatternRegStatus(this, true);
                }
                break;
            }

            default: {
				msg = getResources().getString(R.string.msg_fail_reg);
                if ((mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN || mCommonUserInfo.getEntryPoint() == EntryPoint.DEVICE_CHANGE))
                    Prefer.setPatternRegStatus(this, false);
                break;
            }
        }

        if (errorCode == 1200) {
			requestCountPincodeError(PINCODE_AUTH_STATE_CHANGE, "00", msg);
            return;
        }else {
            final String finalMsg = msg;
            PincodeAuthActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
					dismissProgressDialog();
                    showErrorMessage(finalMsg);
                    showKeyPad();
                }
            });
        }
    }


    /**
	 * Extras 값 획득
	 */
	private void getExtra() {
		Intent intent = getIntent();
		mCommonUserInfo = intent.getParcelableExtra(Const.INTENT_COMMON_INFO);
		if (mCommonUserInfo.getPinLength() <=0 || mCommonUserInfo.getPinLength() > Const.SSENSTONE_PINCODE_LENGTH)
			mCommonUserInfo.setPinLength(Const.SSENSTONE_PINCODE_LENGTH);

		if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL) {
			if (intent.hasExtra(Const.INTENT_SERVICE_ID))
				mServiceId = intent.getStringExtra(Const.INTENT_SERVICE_ID);
			else
				mServiceId = "";

			if (intent.hasExtra(Const.INTENT_TRN_CD))
				mTrnCd = intent.getStringExtra(Const.INTENT_TRN_CD);
			else
				mTrnCd = "";

			if (intent.hasExtra(Const.INTENT_BIZ_DV_CD))
				mBizDvCd = intent.getStringExtra(Const.INTENT_BIZ_DV_CD);
			else
				mBizDvCd = "";
		} else {
			mServiceId = "";
			mTrnCd = "";
			mBizDvCd = "";
		}
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mLayoutMsg = (LinearLayout) findViewById(R.id.layout_pin_msg);

		TextView textClose = findViewById(R.id.textview_close);

		// 로그인 시 지문변경 중 뒤로가기 없음
		if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO
				|| (mCommonUserInfo.getEntryStart() == EntryPoint.LOGIN_BIO && mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET)) {
			textClose.setVisibility(View.INVISIBLE);
		}
		textClose.setOnClickListener(this);

		if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL) {
			textClose.setText(getResources().getString(R.string.common_close));
		}

		mTextTitle = (TextView) findViewById(R.id.tv_title);
		if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE)
			mTextTitle.setText(getString(R.string.msg_input_current_pin));
		mTextMsg = (TextView) findViewById(R.id.textview_pin_msg);

		mExitPincode = (EditText) findViewById(R.id.layout_pincode_input_auth).findViewById(R.id.editText);
		mEditPincode01 = (EditText) findViewById(R.id.edittext_pincode_01);
		mEditPincode01.setOnTouchListener(this);
		mImagePincode01 = (ImageView) findViewById(R.id.imageview_pincode_01);
		mEditPincode01.setOnKeyListener(null);
		mEditPincode02 = (EditText) findViewById(R.id.edittext_pincode_02);
		mEditPincode02.setOnTouchListener(this);
		mEditPincode02.setOnKeyListener(null);
		mImagePincode02 = (ImageView) findViewById(R.id.imageview_pincode_02);
		mEditPincode03 = (EditText) findViewById(R.id.edittext_pincode_03);
		mEditPincode03.setOnTouchListener(this);
		mEditPincode03.setOnKeyListener(null);
		mImagePincode03 = (ImageView) findViewById(R.id.imageview_pincode_03);
		mEditPincode04 = (EditText) findViewById(R.id.edittext_pincode_04);
		mEditPincode04.setOnTouchListener(this);
		mEditPincode04.setOnKeyListener(null);
		RelativeLayout layoutPincode05 = (RelativeLayout) findViewById(R.id.layout_pincode_05);
		mImagePincode04 = (ImageView) findViewById(R.id.imageview_pincode_04);
		mEditPincode05 = (EditText) findViewById(R.id.edittext_pincode_05);
		mEditPincode05.setOnTouchListener(this);
		mEditPincode05.setOnKeyListener(null);
		mImagePincode05 = (ImageView) findViewById(R.id.imageview_pincode_05);
		RelativeLayout layoutPincode06 = (RelativeLayout) findViewById(R.id.layout_pincode_06);
		mEditPincode06 = (EditText) findViewById(R.id.edittext_pincode_06);
		mEditPincode06.setOnTouchListener(this);
		mEditPincode06.setOnKeyListener(null);
		mImagePincode06 = (ImageView) findViewById(R.id.imageview_pincode_06);

		if (mCommonUserInfo.getPinLength() == 4) {
			layoutPincode05.setVisibility(View.GONE);
			layoutPincode06.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getPinLength() == 5) {
			layoutPincode06.setVisibility(View.GONE);
		}

		mTKMgr = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
				TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX,
				"Pin Code",//label
				"",//hint
				mCommonUserInfo.getPinLength(),  //max length
				"",//max length msg
				mCommonUserInfo.getPinLength(),  //min length
				"",
				5,
				true,
				(FrameLayout) findViewById(R.id.keypadContainer),
				mExitPincode,
				(HorizontalScrollView) (findViewById(R.id.layout_pincode_input_auth)).findViewById(R.id.keyscroll),
				(LinearLayout) (findViewById(R.id.layout_pincode_input_auth)).findViewById(R.id.keylayout),
				(ImageButton) (findViewById(R.id.layout_pincode_input_auth)).findViewById(R.id.clearall),
				(RelativeLayout) findViewById(R.id.keypadBallon),
				null,
				false,
				true);
		mTKMgr.showKeypad(mKeypadType);

		LinearLayout btnLostPin = (LinearLayout) findViewById(R.id.layout_auth_lost_pin);
		btnLostPin.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showReregLostPincode();
			}
		});
	}

	/**
	 * 핀코드 입력값 화면 초기화
	 */
	private void clearInput() {
		mEditPincode01.setText("");
		mImagePincode01.setImageResource(R.drawable.ico_pin_off);
		mEditPincode02.setText("");
		mImagePincode02.setImageResource(R.drawable.ico_pin_off);
		mEditPincode03.setText("");
		mImagePincode03.setImageResource(R.drawable.ico_pin_off);
		mEditPincode04.setText("");
		mImagePincode04.setImageResource(R.drawable.ico_pin_off);
		mEditPincode05.setText("");
		mImagePincode05.setImageResource(R.drawable.ico_pin_off);
		mEditPincode06.setText("");
		mImagePincode06.setImageResource(R.drawable.ico_pin_off);

		mEditPincode01.requestFocus();
	}

	/**
	 * 키패드 보이기
	 */
	private void showKeyPad() {
		//clearInput();
		mExitPincode.requestFocus();
		mTKMgr.showKeypad(mKeypadType);
		mIsViewCtrlKeypad = true;
	}

	/**
	 *  핀코드 입력값 인증
	 * @param plainData 핀코드 입력값
	 */
	private void sendAuthSSenstone(final String plainData) {
		mSsId = getSSID();

		final String codeRet = StonePassManager.getInstance(getApplication()).authenticatePin(plainData, null, mCommonUserInfo.getSignData(), mCommonUserInfo.getSignDocData(), mSsId, mStartTime);
		if (codeRet.equals("P000")) {
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO || mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET || mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO || mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET) {
				PincodeAuthActivity.this.runOnUiThread(new Runnable() {
					@Override
					public void run() {
						String msg = "";

						switch (mCommonUserInfo.getEntryPoint()) {
							case AUTH_TOOL_MANAGE_PINCODE_RESET: {
								msg = getResources().getString(R.string.msg_ok_reset_pincode_error_count);
								break;
							}

							case LOGIN_BIO: {
								msg = getResources().getString(R.string.msg_ok_reg_fingerprint);
								Prefer.setFidoRegStatus(PincodeAuthActivity.this, true);
								Prefer.setFlagFingerPrintChange(PincodeAuthActivity.this, false);
								break;
							}

							case AUTH_TOOL_MANAGE_BIO:{
								msg = getResources().getString(R.string.msg_ok_reg_fingerprint);
								Prefer.setFidoRegStatus(PincodeAuthActivity.this, true);
								break;
							}

							case PATTERN_FORGET:
							case AUTH_TOOL_MANAGE_PATTERN:{
                                StonePassManager.getInstance(getApplication()).ssenstoneFIDO(
                                        PincodeAuthActivity.this, Const.SSENSTONE_REGISTER, null, "PATTERN", mCommonUserInfo.getPatternData());
								return;
							}

							case PINCODE_FORGET: {
								switch (mCommonUserInfo.getEntryStart()) {
									case PATTERN_FORGET: {
										msg = getResources().getString(R.string.msg_ok_rereg_pattern);
										break;
									}
									case LOGIN_BIO: {
										msg = getResources().getString(R.string.msg_ok_reg_fingerprint);
										Prefer.setFidoRegStatus(PincodeAuthActivity.this, true);
										Prefer.setFlagFingerPrintChange(PincodeAuthActivity.this, false);
										break;
									}
								}
								break;
							}
							default:
								break;
						}

						if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO || mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO
								|| (mCommonUserInfo.getEntryStart() == EntryPoint.LOGIN_BIO && mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET)) {
							requestCountPincodeError(PINCODE_AUTH_STATE_MANAGE_BIO, "00", msg);
						} else {
							requestCountPincodeError(PINCODE_AUTH_STATE_MANAGE_ELSE, "00", msg);
						}
					}
				});
				return;
			} else if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE) {
				requestCountPincodeError(PINCODE_AUTH_STATE_MANAGE_PINCODE, "00", null);
			} else if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL) {
				requestCountPincodeError(PINCODE_AUTH_STATE_WEB, "00", null);
			} else {
				requestCountPincodeError(PINCODE_AUTH_STATE_ELSE, "00", codeRet);
			}
		} else if (codeRet.equals("P003")) {
			PincodeAuthActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					clearInput();
				}
		    });
			mCountFail++;
			requestCountPincodeError(PINCODE_AUTH_STATE_FAIL, "11", codeRet);
			return;
		} /*else if (codeRet.equals("O130")) {
			final String finalMsg = "인증서버와 시간정보가 일치하지 않습니다.\n정상적인 날짜 및 시간으로 설정하시기 바랍니다.";
			PincodeAuthActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					dismissProgressDialog();
					showErrorMessage(finalMsg);
					showKeyPad();
				}
			});
		}*/ else {
			PincodeAuthActivity.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					clearInput();
					dismissProgressDialog();
					showKeyPad();
					PinErrorDialog errorDialog = new PinErrorDialog(PincodeAuthActivity.this, codeRet, mServiceId, null);
					errorDialog.show();
				}
			});
		}
	}

	/**
	 * 인증 요청 후 메세지 표시
	 */
	private void showMessage() {
		String strMsg;

		if (mCountFail < Const.SSENSTONE_AUTH_COUNT_FAIL) {
			strMsg = String.format(getString(R.string.msg_err_no_match_pin), mCountFail, Const.SSENSTONE_AUTH_COUNT_FAIL);
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(strMsg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
		} else {
			requestPinErrorSMS();
			showKeyPad();
			strMsg = String.format(getString(R.string.msg_err_over_match_pin), Const.SSENSTONE_AUTH_COUNT_FAIL);
			DialogUtil.alert(PincodeAuthActivity.this,
				getResources().getString(R.string.rereg),
				getResources().getString(R.string.common_cancel),
				strMsg,
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						showReregLostPincode();
					}
				},
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
								|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
								|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL) {
							Intent intent = new Intent();
							mCommonUserInfo.setResultMsg("핀번호를 인증 중 에러가 발생했습니다.");
							intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
							setResult(RESULT_CANCELED, intent);
						} else {
							setResult(RESULT_CANCELED);
						}

						finish();
						if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
								|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
								|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL)
							overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
					}
				});
		}
	}

	/**
	 * 인증비밀번호를 잊으셨나요? 를 눌렀을때.
	 */
	private void showReregLostPincode() {
		Intent intent = new Intent(this, ReregLostPincodeActivity.class);
		mLayoutMsg.setVisibility(View.INVISIBLE);

		if(mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET
				||mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE
				|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN) {

			mBackupEntryPoint = mCommonUserInfo.getEntryPoint();
			mCommonUserInfo.setEntryPoint(EntryPoint.PINCODE_FORGET);

			if (mBackupEntryPoint != EntryPoint.PATTERN_FORGET) {
				String accn = LoginUserInfo.getInstance().getODDP_ACCN();
				String mbrno = LoginUserInfo.getInstance().getMBR_NO();

				Logs.e("accn : " + accn);
				Logs.e("mbrno : " + mbrno);

				if (TextUtils.isEmpty(mbrno))
					mbrno = "";
				mCommonUserInfo.setMBRnumber(mbrno);
				if (TextUtils.isEmpty(accn) || Integer.parseInt(accn) < 1)
					mCommonUserInfo.setHasAccount(false);
				else
					mCommonUserInfo.setHasAccount(true);
			}
		}
		clearInput();
		intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
		startActivity(intent);
		// 재등록 프로세스를 진행할 경우 핀코드인증화면을 종료하지 않고 재등록 완료 후 분기 처리
		if (mBackupEntryPoint == EntryPoint.AUTH_TOOL_MANAGE_PINCODE
				|| mBackupEntryPoint == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET)
			finish();
	}

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.textview_close) {
			if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PATTERN
				    || mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.PATTERN_FORGET
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.PINCODE_FORGET
					|| mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
				String msg = "";
				switch (mCommonUserInfo.getEntryPoint()) {
					case AUTH_TOOL_MANAGE_PINCODE_RESET: {
						msg = getResources().getString(R.string.msg_cancel_pin_errcount_reset);
						break;
					}

					case PATTERN_FORGET: {
						msg = getResources().getString(R.string.msg_cancel_pattern_rereg);
						break;
					}

					case AUTH_TOOL_MANAGE_PATTERN: {
						msg = getResources().getString(R.string.msg_cancel_pattern_change);
						break;
					}

					case LOGIN_BIO: {
						msg = getResources().getString(R.string.msg_cancel_bio_rereg);
						return;
					}

					case AUTH_TOOL_MANAGE_BIO: {
						msg = getResources().getString(R.string.msg_cancel_bio_change);
						break;
					}
					case PINCODE_FORGET: {
						switch (mCommonUserInfo.getEntryStart()) {
							case LOGIN_PATTERN: {
								msg = getResources().getString(R.string.msg_cancel_pattern_rereg);
								break;
							}

							case LOGIN_BIO: {
								msg = getResources().getString(R.string.msg_cancel_bio_rereg);
								return;
							}
						}
                    }
					default:
						break;
				}

				if (mCommonUserInfo.getEntryPoint() == EntryPoint.AUTH_TOOL_MANAGE_BIO) {
					DialogUtil.alert(this,
							"나가기",
							"취소",
							msg,

							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									setResult(RESULT_CANCELED);
									finish();
								}
							},
							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
								}
							});
					return;
				} else if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
					DialogUtil.alert(this,
							"나가기",
							"취소",
							msg,

							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									Intent intent = new Intent(PincodeAuthActivity.this, Main2Activity.class);
									startActivity(intent);
									finish();
								}
							},
							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
								}
							});
				} else {
					showCancelMessage(msg);
				}
			} else {
				setResult(RESULT_CANCELED);
				finish();
				if (mCommonUserInfo.getEntryPoint() == EntryPoint.WEB_CALL
						|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER
						|| mCommonUserInfo.getEntryPoint() == EntryPoint.TRANSFER_SAFEDEAL)
					overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
			}
        }
    }

	private void requestPinErrorSMS() {
		Map param = new HashMap();
		param.put("SMS_MSG_CD", "B004");
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0010800A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
			}
		});
	}

	private void requestCountPinError() {
		String mbrNo = mCommonUserInfo.getMBRnumber();
		if (TextUtils.isEmpty(mbrNo)) {
			mbrNo = LoginUserInfo.getInstance().getMBR_NO();
			if (TextUtils.isEmpty(mbrNo))
				return;
		}

		Map param = new HashMap();
		param.put("MBR_NO", mbrNo);
		showProgressDialog();
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0010800A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				dismissProgressDialog();
				Logs.i("ret : " + ret);
				if (TextUtils.isEmpty(ret)) {
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					return;
				}

				try {
					JSONObject object = new JSONObject(ret);
					if (object == null) {
						Logs.e(getResources().getString(R.string.msg_debug_err_response));
						Logs.e(ret);
						return;
					}

					JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
					if (objectHead == null) {
						return;
					}

					String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
					if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
						String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
						if (TextUtils.isEmpty(msg))
							msg = getString(R.string.common_msg_no_reponse_value_was);

						Logs.e("error msg : " + msg + ", ret : " + ret);
						return;
					}

					String count = object.optString("PN_INPT_EROR_TCNT");
					if (!TextUtils.isEmpty(count)) {
						mCountFail = Integer.parseInt(count);
						if (mCountFail >= Const.SSENSTONE_AUTH_COUNT_FAIL)
							mCountFail = Const.SSENSTONE_AUTH_COUNT_FAIL;
						Prefer.setPinErrorCount(PincodeAuthActivity.this, mCountFail);
					}
				} catch (JSONException e) {
					Logs.printException(e);
				}
			}
		});
	}

	private void requestCountPincodeError(final int state, final String result, final String msg) {
		final Map param = new HashMap();
		final boolean[] isSuccessed = new boolean[1];
		/*
		if (TextUtils.isEmpty(mCommonUserInfo.getMBRnumber())) {
			return;
		}
		*/

		// 5회 실패 후에는 실패 전문을 날리지 않고 처리
		if ((mCountFail > Const.SSENSTONE_AUTH_COUNT_FAIL) && state == PINCODE_AUTH_STATE_FAIL) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					dismissProgressDialog();
				}
			});

			if (isFinishing())
				return;

			new Thread() {
				public void run() {
					Message message = msgHandler.obtainMessage();
					msgHandler.sendMessage(message);
				}
			}.start();
			return;
		}

		String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
		if (TextUtils.isEmpty(mbrNo))
			mbrNo = mCommonUserInfo.getMBRnumber();

		if (TextUtils.isEmpty(mbrNo))
			param.put("MBR_NO", "");
		else
			param.put("MBR_NO", mbrNo);
		param.put("PN_ATHN_RSLT_CD", result);
		param.put("SCRN_ID", mServiceId);
		param.put("STONE_PASS_USER_ID", getUserName());
		if (TextUtils.isEmpty(mCommonUserInfo.getSignData()) || !"00".equals(result))
		    param.put("ELEC_SGNR_KEY_VAL", "");
		else
            param.put("ELEC_SGNR_KEY_VAL", mSsId);
		param.put("ELEC_SGNR_BZWR_DVCD", mBizDvCd);
		param.put("TRN_CD", mTrnCd);



		isSuccessed[0] = true;
		HttpUtils.sendHttpTask(WasServiceUrl.CMM0010800A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
			@Override
			public void endHttpRequest(String ret) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						dismissProgressDialog();
					}
				});
				if (TextUtils.isEmpty(ret)) {
					Logs.e(getResources().getString(R.string.msg_debug_no_response));
					isSuccessed[0] = false;

					PincodeAuthActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showErrorMessage(getResources().getString(R.string.msg_debug_no_response));
							showKeyPad();
						}
					});
					return;
				}
				try {
					if (!TextUtils.isEmpty(ret)) {
						JSONObject object = new JSONObject(ret);
						if (object == null) {
							Logs.e(getResources().getString(R.string.msg_debug_err_response));
							Logs.e(ret);
							isSuccessed[0] = false;

							PincodeAuthActivity.this.runOnUiThread(new Runnable() {
								@Override
								public void run() {
									showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
									showKeyPad();
								}
							});
							return;
						}

						Logs.e(ret);

						final JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
						String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
						final String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
						if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
							if ("CMM0037".equals(errCode)) {
								//로그인 중 분실신고기기 체크
								if (Utils.isAppOnForeground(PincodeAuthActivity.this)) {
									LogoutTimeChecker.getInstance(PincodeAuthActivity.this).autoLogoutStop();
									SaidaApplication mApplicationClass = (SaidaApplication) getApplicationContext();
									mApplicationClass.allActivityFinish(true);
									LoginUserInfo.clearInstance();
									LoginUserInfo.getInstance().setLogin(false);
									Intent intent = new Intent(PincodeAuthActivity.this, LogoutActivity.class);
									intent.putExtra(Const.INTENT_LOGOUT_TYPE, Const.LOGOUT_TYPE_USER);
									startActivity(intent);
								} else {
									LoginUserInfo.getInstance().setLogin(false);
									LogoutTimeChecker.getInstance(PincodeAuthActivity.this).autoLogoutStop();
									LogoutTimeChecker.getInstance(PincodeAuthActivity.this).setBackground(true);
								}
							} else {
								isSuccessed[0] = false;
								String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
								if (TextUtils.isEmpty(msg))
									msg = getString(R.string.common_msg_no_reponse_value_was);

								final String finalMsg = msg;
								PincodeAuthActivity.this.runOnUiThread(new Runnable() {
									@Override
									public void run() {
										showCommonErrorDialog(finalMsg, errCode, "", objectHead, true);
										showKeyPad();
									}
								});
							}
							return;
						}
					}
				} catch (JSONException e) {
					Logs.printException(e);
					isSuccessed[0] = false;

					PincodeAuthActivity.this.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							showErrorMessage(getResources().getString(R.string.msg_debug_err_response));
							showKeyPad();
						}
					});
					return;
				}

				if ("00".equals(result))
					Prefer.setPinErrorCount(PincodeAuthActivity.this, 0);
				else
					Prefer.setPinErrorCount(PincodeAuthActivity.this, mCountFail);

				switch (state) {
					case PINCODE_AUTH_STATE_CHANGE: {
						DialogUtil.alert(PincodeAuthActivity.this,
							msg,
							new View.OnClickListener() {
								@Override
								public void onClick(View v) {
									finish();
								}
							});
						return;
					}

					case PINCODE_AUTH_STATE_MANAGE_BIO: {
						FidoDialog fidoDialog = DialogUtil.fido(PincodeAuthActivity.this, false,true,
								new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										if (mCommonUserInfo.getEntryPoint() == EntryPoint.LOGIN_BIO) {
											Intent intent = new Intent(PincodeAuthActivity.this, Main2Activity.class);
											startActivity(intent);
										} else {
											setResult(RESULT_OK);
										}
										finish();
									}
								},
								null
						);
						fidoDialog.mBackKeyListener = new Dialog.OnDismissListener() {
							@Override
							public void onDismiss(DialogInterface dialog) {
								Prefer.setFidoRegStatus(PincodeAuthActivity.this, true);
								setResult(RESULT_OK);
							}
						};
						TextView textDesc = fidoDialog.findViewById(R.id.textview_fido_dialog_desc);
						textDesc.setText(msg);
						fidoDialog.setCanceledOnTouchOutside(false);
						fidoDialog.setCancelable(false);
						fidoDialog.show();
						return;
					}

					case PINCODE_AUTH_STATE_MANAGE_ELSE: {
						DialogUtil.alert(PincodeAuthActivity.this,
								msg,
								new View.OnClickListener() {
									@Override
									public void onClick(View v) {
										finish();
									}
								});
						return;
					}

					case PINCODE_AUTH_STATE_MANAGE_PINCODE: {
						Intent intent = new Intent(PincodeAuthActivity.this, PincodeRegActivity.class);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						startActivity(intent);
						finish();
						return;
					}

					case PINCODE_AUTH_STATE_WEB: {
						Intent intent = new Intent();
						String dbKey = "";
						try {
							JSONObject object = new JSONObject(ret);
							dbKey = object.optString("ELEC_SGNR_SRNO");
						} catch (JSONException e) {
							//e.printStackTrace();
						}
						mCommonUserInfo.setSignData(dbKey);
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						if (isSuccessed[0])
							setResult(RESULT_OK, intent);
						else
							setResult(RESULT_CANCELED, intent);

						finish();
						overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
						break;
					}

					case PINCODE_AUTH_STATE_ELSE: {
						Intent intent = new Intent();
						if (isSuccessed[0]) {
							mCommonUserInfo.setResultMsg(msg);
							String dbKey = "";
							try {
								JSONObject object = new JSONObject(ret);
								dbKey = object.optString("ELEC_SGNR_SRNO");
							} catch (JSONException e) {
								//e.printStackTrace();
							}
							mCommonUserInfo.setSignData(dbKey);
						}
						else {
							mCommonUserInfo.setResultMsg("elec sign error");
						}
						intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
						setResult(RESULT_OK, intent);
						finish();
						overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
						return;
					}

					case PINCODE_AUTH_STATE_FAIL: {
						if (isFinishing())
							return;

						new Thread() {
							public void run() {
								Message message = msgHandler.obtainMessage();
								msgHandler.sendMessage(message);
							}
						}.start();
						return;
					}
					default:
						break;
				}
			}
		});
	}

	private String getUserName() {
		String userName = Prefer.getAuthPhoneCI(this);
		try {
			if (!TextUtils.isEmpty(userName)) {
				byte[] data = userName.getBytes("UTF-8");
				userName = Base64.encodeToString(data, Base64.NO_WRAP);
				Logs.e("mUserName : " + userName);
			} else {
				Logs.e("mUserName is null");
				userName = "";
			}
		} catch (UnsupportedEncodingException e) {
			//e.printStackTrace();
			userName = "";
		}
		return userName;
	}

	private String getSSID() {
		String ssid = null;
		String userName = getUserName();
		long endTime = System.currentTimeMillis();
		if (!TextUtils.isEmpty(userName) && userName.length() > 18) {
			ssid = userName.substring(0, 19) + Long.toString(endTime).substring(0,13);
		} else {
			ssid = "abcdefghijklmnopqrs" + Long.toString(endTime).substring(0,13);
		}
		Logs.e("ssid : " + ssid);

		return ssid;
	}
}