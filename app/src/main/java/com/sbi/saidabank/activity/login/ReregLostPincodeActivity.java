package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;

/**
 * Saidabanking_android
 * <p>
 * Class: ReregLostPincodeActivity
 * Created by 950485 on 2018. 12. 07..
 * <p>
 * Description:인증번호 분실 후 등록 화면
 */

public class ReregLostPincodeActivity extends BaseActivity implements View.OnClickListener {

    CommonUserInfo mCommonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_rereg_lost_pincode);

        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        LinearLayout layoutId = findViewById(R.id.ll_identification);
        LinearLayout layoutAccount = findViewById(R.id.ll_account);

        if (mCommonUserInfo.hasAccount()) {
            layoutId.setVisibility(View.VISIBLE);
            layoutAccount.setVisibility(View.VISIBLE);
        } else {
            layoutId.setVisibility(View.GONE);
            layoutAccount.setVisibility(View.GONE);
        }

        TextView textDesc01 = (TextView) findViewById(R.id.textview_rereg_pin_desc_01);
        String strDesc01 = getString(R.string.auth_pincode);
        String strDesc02 = getString(R.string.rereg_lost_pincode_desc_01);
        Utils.setTextWithSpan(textDesc01, strDesc01 + strDesc02, strDesc01, Typeface.BOLD,"");

        findViewById(R.id.btn_cancel_rereg_pin).setOnClickListener(this);
        findViewById(R.id.btn_confirm_rereg_pin).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (mCommonUserInfo.getEntryStart() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE
                || mCommonUserInfo.getEntryStart() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET) {
            Intent intent = new Intent(this, PincodeAuthActivity.class);
            mCommonUserInfo.setEntryPoint(mCommonUserInfo.getEntryStart());
            intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
            startActivity(intent);
        }
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel_rereg_pin: {
                if (mCommonUserInfo.getEntryStart() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE
                        || mCommonUserInfo.getEntryStart() == EntryPoint.AUTH_TOOL_MANAGE_PINCODE_RESET) {
                    Intent intent = new Intent(this, PincodeAuthActivity.class);
                    mCommonUserInfo.setEntryPoint(mCommonUserInfo.getEntryStart());
                    intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
                    startActivity(intent);
                }
                finish();
                break;
            }

            case R.id.btn_confirm_rereg_pin: {
                showCertifyPhone();
                break;
            }

            default:
                break;
        }
    }

    /**
     * 휴대폰 본인 인증 화면으로 이동
     */
    private void showCertifyPhone() {
        Intent intent = new Intent(this, CertifyPhonePersonalInfoActivity.class);
        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
        startActivity(intent);
        finish();
    }
}
