package com.sbi.saidabank.activity.transaction.itransfer;

public interface OnITransferSingleActionListener {

    void openInputMoneyView();
    void openInputDetailView(String inputMoney);
    void closeInputDetailView();
    void clearDetailInfo();
    /**
     * 보낼일시 선택할때
     */
    void showDatePicker();
}
