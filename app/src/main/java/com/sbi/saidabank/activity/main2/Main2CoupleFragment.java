package com.sbi.saidabank.activity.main2;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.crop.CropImageActivity;
import com.sbi.saidabank.activity.main2.myaccountlist.Main2AccountAdapter;
import com.sbi.saidabank.activity.main2.common.beforeload.CoupleBeforeLoadLayout;
import com.sbi.saidabank.activity.main2.homecouple.CoupleTitleLayout;
import com.sbi.saidabank.activity.main2.homecouple.CoupleTotalSumLayout;
import com.sbi.saidabank.activity.main2.homecouple.OnCoupleActionLayoutListener;
import com.sbi.saidabank.activity.main2.homecouple.Statistics1Layout;
import com.sbi.saidabank.activity.main2.homecouple.Statistics2Layout;
import com.sbi.saidabank.activity.main2.homecouple.Statistics3Layout;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomScrollView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2AccountInfo;
import com.sbi.saidabank.define.datatype.main2.Main2CoupleExpenceAmount;
import com.sbi.saidabank.define.datatype.main2.Main2CoupleExpenceCount;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static android.app.Activity.RESULT_OK;

public class Main2CoupleFragment extends BaseFragment implements OnCoupleActionLayoutListener {

    private String[] mPermissionStorage = new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE };
    private String[] mPermissionCamera = new String[]{ Manifest.permission.CAMERA };

    private Context mContext;
    private ProgressBarLayout mProgressLayout;
    private CustomScrollView mScrollView;
    private CoupleBeforeLoadLayout mBeforeLoadLayout;
    private CoupleTitleLayout mCoupleTitleLayout;
    private CoupleTotalSumLayout mCoupleTotalSumLayout;

    private RecyclerView mAccListView;
    private Main2AccountAdapter mCoupleAccountAdapter;
    private RelativeLayout mLayoutStatistics;

    private ArrayList<Main2AccountInfo> mCoupleAccountArray;
    private Uri mTempBackImgUri;
    private boolean isNeedCoupleConnectState;

    public static final Main2CoupleFragment newInstance() {
        return new Main2CoupleFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        this.mContext = getContext();
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main2_home_couple, container, false);

        mProgressLayout = rootView.findViewById(R.id.ll_progress);

        mCoupleTitleLayout = rootView.findViewById(R.id.layout_title);
        mCoupleTitleLayout.setOnCoupleActionLayoutListener(this);

        // 계좌가 여러개일경우 표시할 summary 레이아웃.
        mCoupleTotalSumLayout = rootView.findViewById(R.id.layout_total_sum);
        mCoupleTotalSumLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MLog.d();
                Intent intent = new Intent(mContext, WebMainActivity.class);
                intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.UNT0480100.getServiceUrl());
                mContext.startActivity(intent);
            }
        });

        mScrollView = rootView.findViewById(R.id.scrollview);
        mScrollView.setOnScrollActionListener(new CustomScrollView.OnScrollActionListener() {
            @Override
            public void onScrollStart(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                ((Main2Activity) mContext).changeHomeFragmentScrollState(true);
            }

            @Override
            public void onScrollStop(boolean isBottom) {
                ((Main2Activity) mContext).changeHomeFragmentScrollState(false);
            }

            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                mCoupleTitleLayout.changeViewHeight(scrollY);
            }
        });

        mCoupleAccountArray = new ArrayList<>();
        mCoupleAccountAdapter = new Main2AccountAdapter(mContext, Main2AccountAdapter.DISP_TYPE_COUPLE);
        mAccListView = rootView.findViewById(R.id.acclistview);
        mAccListView.setAdapter(mCoupleAccountAdapter);
        mAccListView.setLayoutManager(new LinearLayoutManager(mContext));

        int radius = (int) Utils.dpToPixel(mContext, 20);
        mLayoutStatistics = rootView.findViewById(R.id.layout_statistics);
        mLayoutStatistics.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff", new int[]{radius, radius, radius, radius}));

        mBeforeLoadLayout = rootView.findViewById(R.id.layout_before_loaded);

        return rootView;
    }

    @Override
    public void onResume() {
        MLog.d();
        super.onResume();
        // 리스트를 처음으로 돌려놓는다.
        mCoupleTitleLayout.setOriginSize();
        mScrollView.post(new Runnable() {
            @Override
            public void run() {
                mScrollView.scrollTo(0, 0);
            }
        });

        if (isNeedCoupleConnectState) {
            isNeedCoupleConnectState = false;
            requestMainInfoAfterAction();
        } else {
            requestCoupleInfo();
        }

        // Fragment가 시작할대는 홈메뉴는 항상보여야 한다.
        ((Main2Activity) mContext).changeHomeFragmentScrollState(false);
    }

    @Override
    public void onPause() {
        MLog.d();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        MLog.d();
        super.onDestroy();
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        mProgressLayout.show();
    }

    @Override
    public void dismissProgress() {
        mProgressLayout.hide();
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onOpenCamera(int position) {
        MLog.d();
        switch (position) {
            case 1:
                //여기엔 들어오지 않는다. 들어오기 전에 처리함.
                break;
            // 갤러리에서 가져오기
            case 2:
                if (PermissionUtils.checkPermission(getActivity(), mPermissionStorage, Const.REQUEST_COUPLE_BG_ALBUM)) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                    if (DataUtil.isNotNull(getActivity()) && !getActivity().isFinishing())
                        getActivity().startActivityForResult(intent, Const.REQUEST_COUPLE_BG_ALBUM);
                }
                break;
            // 사진찍어서 가져오기
            case 3:
                if (PermissionUtils.checkPermission(getActivity(), mPermissionCamera, Const.REQUEST_COUPLE_BG_CAMERA)) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                        File photoFile = ImgUtils.createNewImageFilePathName(mContext, "couple_title");
                        if (DataUtil.isNotNull(photoFile)) {
                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                                mTempBackImgUri = Uri.fromFile(photoFile);
                            } else {
                                mTempBackImgUri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".fileprovider", photoFile);
                            }
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, mTempBackImgUri);
                            getActivity().startActivityForResult(intent, Const.REQUEST_COUPLE_BG_CAMERA);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void fragmentForPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.fragmentForPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            // 갤러리에서 가져오기
            case Const.REQUEST_COUPLE_BG_ALBUM:
                if (DataUtil.isNotNull(grantResults) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.setType(MediaStore.Images.Media.CONTENT_TYPE);
                    if (DataUtil.isNotNull(getActivity()) && !getActivity().isFinishing())
                        getActivity().startActivityForResult(intent, Const.REQUEST_COUPLE_BG_ALBUM);
                } else {
                    if (DataUtil.isNotNull(permissions)) {
                        final boolean showFlag = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0]);
                        DialogUtil.alert(
                                mContext,
                                "권한설정",
                                "확인",
                                getString(R.string.msg_permission_storage_allow),
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if (showFlag)
                                            PermissionUtils.checkPermission(getActivity(), mPermissionStorage, Const.REQUEST_COUPLE_BG_ALBUM);
                                        else {
                                            PermissionUtils.goAppSettingsActivity(getActivity());
                                            getActivity().finish();
                                        }
                                    }
                                },
                                new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        //Utils.finishAffinity(getActivity());
                                    }
                                }
                        );
                    }
                }
                break;
            // 사진찍어서 가져오기
            case Const.REQUEST_COUPLE_BG_CAMERA:
                if (DataUtil.isNotNull(grantResults) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    if (intent.resolveActivity(mContext.getPackageManager()) != null) {
                        File photoFile = ImgUtils.createNewImageFilePathName(mContext, "couple_title");
                        if (DataUtil.isNotNull(photoFile)) {

                            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
                                mTempBackImgUri = Uri.fromFile(photoFile);
                            } else {
                                mTempBackImgUri = FileProvider.getUriForFile(mContext, mContext.getPackageName() + ".fileprovider", photoFile);
                            }
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, mTempBackImgUri);
                            getActivity().startActivityForResult(intent, Const.REQUEST_COUPLE_BG_CAMERA);
                        }
                    }
                } else {
                    final boolean showFlag = ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), permissions[0]);
                    DialogUtil.alert(
                            mContext,
                            "권한설정",
                            "확인",
                            getString(R.string.msg_permission_camera_allow),
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (showFlag)
                                        PermissionUtils.checkPermission(getActivity(), mPermissionCamera, Const.REQUEST_COUPLE_BG_CAMERA);
                                    else {
                                        PermissionUtils.goAppSettingsActivity(getActivity());
                                        getActivity().finish();
                                    }
                                }
                            },
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // TODO Auto-generated method stub
                                }
                            }
                    );
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            MLog.i("requestCode >> " + requestCode);
            switch (requestCode) {
                // 앨범에서 가져오기
                case Const.REQUEST_COUPLE_BG_ALBUM:
                    if (DataUtil.isNotNull(data)) {
                        Uri photoUri = data.getData();
                        if (DataUtil.isNull(photoUri))
                            return;
                        MLog.i("Album Uri >> " + photoUri.toString());
                        Intent intent = new Intent(mContext, CropImageActivity.class);
                        intent.putExtra(Const.CROP_IMAGE_URI, photoUri.toString());
                        intent.putExtra(Const.CROP_IMAGE_TYPE, Const.CROP_TYPE_RECT);
                        if (DataUtil.isNotNull(getActivity()) && !getActivity().isFinishing())
                            getActivity().startActivityForResult(intent, Const.REQUEST_CROP_TO_IMAGE);
                    }
                    break;

                // 카메라 촬영
                case Const.REQUEST_COUPLE_BG_CAMERA:
                    if (DataUtil.isNull(mTempBackImgUri))
                        return;
                    MLog.i("Camera Uri >> " + mTempBackImgUri);
                    Intent intent = new Intent(getActivity(), CropImageActivity.class);
                    intent.putExtra(Const.CROP_IMAGE_URI, mTempBackImgUri.toString());
                    intent.putExtra(Const.CROP_IMAGE_TYPE, Const.CROP_TYPE_RECT);
                    if (DataUtil.isNotNull(getActivity()) && !getActivity().isFinishing())
                        getActivity().startActivityForResult(intent, Const.REQUEST_CROP_TO_IMAGE);
                    break;

                // 이미지 크롭
                case Const.REQUEST_CROP_TO_IMAGE:
                    if (DataUtil.isNotNull(data)) {
                        Bundle bundle = data.getExtras();
                        if (DataUtil.isNotNull(bundle.getString(Const.CROP_IMAGE_URI)) && DataUtil.isNotNull(Uri.parse(bundle.getString(Const.CROP_IMAGE_URI)))) {
                            MLog.i("Uri >> " + bundle.getString(Const.CROP_IMAGE_URI));
                            //카메라 촬영한 사진이 있다면 삭제한다.
                            ImgUtils.deleteImageFile(mContext, mTempBackImgUri);
                            Uri cropUri = Uri.parse(bundle.getString(Const.CROP_IMAGE_URI));
                            if (DataUtil.isNotNull(cropUri)) {
                                mCoupleTitleLayout.setTitleBackImage(cropUri.getPath(), true);
                            }
                        }
                    }
                    break;

                default:
                    break;
            }
        } else {
            switch (requestCode) {
                // 카메라 촬영
                case Const.REQUEST_COUPLE_BG_CAMERA:
                    ImgUtils.deleteImageFile(mContext, mTempBackImgUri);
                    break;
                default:
                    break;
            }
        }
    }

// ==============================================================================================================
// VIEW
// ==============================================================================================================
    private void displayStatisticsLayout() {

        MLog.d();

        if (DataUtil.isNull(mContext)) return;

        // 기존의 뷰를 초기화 시킨다.
        mLayoutStatistics.removeAllViews();

        // 간혹 계정계에서 가입일자가 않내려 오늘 경우가 있나보다...Firebase에 문제점 등록됨.
        String startDateStr = Main2DataInfo.getInstance().getSHRP_SETP_DT();
        if (DataUtil.isNull(startDateStr)) {
            mLayoutStatistics.addView(new Statistics1Layout(mContext));
            return;
        }

        try {
            Calendar startCalendar = Calendar.getInstance();
            startCalendar.setTime(new SimpleDateFormat("yyyyMMdd").parse(startDateStr));
            Calendar toCalendar = new GregorianCalendar();
            int month1 = startCalendar.get(Calendar.YEAR) * 12 + startCalendar.get(Calendar.MONTH);
            int month2 = toCalendar.get(Calendar.YEAR) * 12 + toCalendar.get(Calendar.MONTH);
            int diffMonth = month2 - month1;
            MLog.i("diffMonth >> " + diffMonth);
            switch (diffMonth) {
                // 커플 가입한 달.
                case 0:
                    mLayoutStatistics.addView(new Statistics1Layout(mContext));
                    break;
                // 커플 가입한지 한달 지났어요.
                case 1:
                    mLayoutStatistics.addView(new Statistics2Layout(mContext));
                    break;
                // 커플 가입한지 두달 이상 되었어요.
                default:
                    mLayoutStatistics.addView(new Statistics3Layout(mContext));
                    break;
            }
        } catch (ParseException e) {
            Statistics1Layout layout = new Statistics1Layout(mContext);
            mLayoutStatistics.addView(layout);
            MLog.e(e);
        }
    }

// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    /**
     * Main2Activity의 forceReloadCoupleState에서 호출해준다.
     *
     * @param isFromWeb
     */
    public void setReloadCoupleState(boolean isFromWeb) {
        if (isFromWeb) {
            isNeedCoupleConnectState = true;
        } else {
            requestMainInfoAfterAction();
        }
    }

    private void requestMainInfoAfterAction() {
        requestMainSimpleInfo(new OnRequestAfterActionListener() {
            @Override
            public void onRequestAfterAction() {
                String SHRN_PRGS_STCD = Main2DataInfo.getInstance().getSHRN_PRGS_STCD();
                if (DataUtil.isNotNull(SHRN_PRGS_STCD) && SHRN_PRGS_STCD.equals("03")) {
                    requestCoupleInfo();
                    return;
                }
                // 내가 계좌주로 커플통장이 연결되어 있는 상태
                if (!LoginUserInfo.getInstance().getMyAccountInfoArrayList().isEmpty()) {
                    // 공유자 이름이 있다면 커플통장이 연결된 상태인것이다.
                    if (DataUtil.isNotNull(Main2DataInfo.getInstance().getSHRP_NM())) {
                        requestCoupleInfo();
                        return;
                    }
                }
                if (DataUtil.isNotNull(getActivity()) && !(getActivity()).isFinishing()) {
                    ((Main2Activity) getActivity()).viewPagerUpdate();
                }
            }
        });
    }

// ==============================================================================================================
// API
// ==============================================================================================================
    /**
     * 커플계좌정보조회
     */
    private void requestCoupleInfo() {
        MLog.d();

        if (DataUtil.isNull(getActivity()) || getActivity().isFinishing()) return;
        if (!LoginUserInfo.getInstance().isLogin()) return;

        showProgress();

        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010100A04.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if (DataUtil.isNull(getActivity()) || getActivity().isFinishing()) return;

                dismissProgress();

                if (DataUtil.isNull(ret))
                    return;

                try {
                    JSONObject object = new JSONObject(ret);
                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (DataUtil.isNull(msg)) {
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        }
                        msg = msg.replace("<br>", "\n");
                        if (errCode.equals("UNT0025")) {
                            DialogUtil.alert(mContext, msg, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //에러가 발생한 경우는 공유를 해제해서 발생하는 경우가 대부분 이기에 여기서 공유상태를 확인 후에 웹으로 이동시키도록 한다.
                                    requestMainInfoAfterAction();
                                }
                            });
                        } else {
                            ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, new CommonErrorDialog.OnConfirmListener() {
                                @Override
                                public void onConfirmPress() {
                                    //에러가 발생한 경우는 공유를 해제해서 발생하는 경우가 대부분 이기에 여기서 공유상태를 확인 후에 웹으로 이동시키도록 한다.
                                    requestMainInfoAfterAction();
                                }
                            });
                        }
                        return;
                    }
                    //========================================================================
                    // 메인 레코드가 아닌 기본값 조회 - 계좌분리 리스트도 여기서 한다.
                    Main2DataInfo.getInstance().setMain2DataInfoFromCoupleAccount(object);
                    int accCnt = Integer.parseInt(Main2DataInfo.getInstance().getSHRP_ACCO_CCNT());
                    if (accCnt > 0) {
                        String amount = Utils.moneyFormatToWon(Double.valueOf(Main2DataInfo.getInstance().getSHRP_ACCO_SUM_AMT()));
                        mCoupleTotalSumLayout.setTotalSumInfo(accCnt, amount);
                        mCoupleTotalSumLayout.setVisibility(View.VISIBLE);
                    }
                    mCoupleTitleLayout.setShareDate(Main2DataInfo.getInstance().getSHRP_SETP_DT());
                    //========================================================================
                    // 계좌정보조회
                    boolean isSetCharge = false;
                    ArrayList<Main2AccountInfo> listAccountInfo = new ArrayList<>();
                    JSONArray account_array = object.optJSONArray("REC_SHRP");
                    if (DataUtil.isNull(account_array)) return;
                    for (int index = 0; index < account_array.length(); index++) {
                        JSONObject jsonObject = account_array.getJSONObject(index);
                        if (DataUtil.isNull(jsonObject))
                            continue;
                        Main2AccountInfo accountInfo = new Main2AccountInfo(jsonObject);
                        if (DataUtil.isNull(accountInfo.getDSCT_CD()))
                            continue;
                        // 충전하기 설정여부를 가지고 있는다.
                        String IMMD_WTCH_ACCO_YN = accountInfo.getIMMD_WTCH_ACCO_YN();
                        if (!isSetCharge && IMMD_WTCH_ACCO_YN.equals(Const.REQUEST_WAS_YES)) {
                            isSetCharge = true;
                            Main2DataInfo.getInstance().setACCOUNT_CHARGE_SET_YN(Const.REQUEST_WAS_YES);
                        }
                        listAccountInfo.add(accountInfo);
                    }
                    if (!listAccountInfo.isEmpty()) {
                        mCoupleAccountArray.clear();
                        mCoupleAccountArray.addAll(listAccountInfo);
                        mCoupleAccountAdapter.setMyAccountArray(mCoupleAccountArray);
                    }
                    //========================================================================
                    // 지출 금액별
                    ArrayList<Main2CoupleExpenceAmount> expenceAmounts = new ArrayList<>();
                    JSONArray expence_amount_array = object.optJSONArray("REC_SETT_AMT");
                    if (DataUtil.isNull(expence_amount_array)) return;
                    for (int index = 0; index < expence_amount_array.length(); index++) {
                        JSONObject jsonObject = expence_amount_array.getJSONObject(index);
                        if (DataUtil.isNull(jsonObject))
                            continue;
                        expenceAmounts.add(new Main2CoupleExpenceAmount(jsonObject));
                    }
                    if (!expenceAmounts.isEmpty()) {
                        Main2DataInfo.getInstance().setMain2CoupleExpenceAmountArray(expenceAmounts);
                    }
                    //========================================================================
                    // 지출 건수별
                    ArrayList<Main2CoupleExpenceCount> expenceCounts = new ArrayList<>();
                    JSONArray expence_count_array = object.optJSONArray("REC_SETT_CCNT");
                    if (DataUtil.isNull(expence_count_array)) return;
                    for (int index = 0; index < expence_count_array.length(); index++) {
                        JSONObject jsonObject = expence_count_array.getJSONObject(index);
                        if (DataUtil.isNull(jsonObject))
                            continue;
                        expenceCounts.add(new Main2CoupleExpenceCount(jsonObject));
                    }
                    if (!expenceCounts.isEmpty()) {
                        Main2DataInfo.getInstance().setMain2CoupleExpenceCountArray(expenceCounts);
                    }
                    //========================================================================

                    // 가입한 년월에 따라 통계 레이아웃을 출력한다.
                    displayStatisticsLayout();

                    mBeforeLoadLayout.hideLayout();
//                    mScrollView.setAlpha(0.0f);
//                    mScrollView.animate()
//                            .alpha(1)
//                            .setDuration(500)
//                            .setStartDelay(400)
//                            .start();
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }
}
