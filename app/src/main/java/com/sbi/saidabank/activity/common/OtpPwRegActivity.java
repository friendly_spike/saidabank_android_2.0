package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Saidabanking_android
 *
 * Class: OtpPwRegActivity
 * Created by 950485 on 2018. 12. 17..
 * <p>
 * Description: OTP, password 인증 등록을 위한 화면
 */

public class OtpPwRegActivity extends BaseActivity implements View.OnTouchListener, ITransKeyActionListener, ITransKeyActionListenerEx, ITransKeyCallbackListener {
	private static int  mKeypadType = TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER;

	/**
	 * 등록 단계
	 */
	private enum REG_STEP {
		STEP_01,
		STEP_02
	}

	private LinearLayout  mLayoutMsg;
	private TextView      mTextDesc;
	private TextView      mTextMsg;
	private EditText      mExitOtpPw;
	private EditText      mEditOtpPw01;
	private ImageView     mImageOtpPw01;
	private EditText      mEditOtpPw02;
	private ImageView     mImageOtpPw02;
	private EditText      mEditOtpPw03;
	private ImageView     mImageOtpPw03;
	private EditText      mEditOtpPw04;
	private ImageView     mImageOtpPw04;
	private EditText      mEditOtpPw05;
	private ImageView     mImageOtpPw05;
	private EditText      mEditOtpPw06;
	private ImageView     mImageOtpPw06;
	private EditText      mEditOtpPw07;
	private ImageView     mImageOtpPw07;

	private boolean       mIsViewCtrlKeypad = false;

	private CommonUserInfo mCommonUserInfo;

	private TransKeyCtrl  mTKMgr = null;

	private REG_STEP      mRegStep = REG_STEP.STEP_01;
	private String        mFirstPin = null;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_otp_pw_reg);

		getExtra();
        initUX();
    }

    @Override
	public boolean onTouch(View v, MotionEvent event) {
    	if (event.getAction() == MotionEvent.ACTION_DOWN) {
	    	if (!mIsViewCtrlKeypad) {
	    		showKeyPad();
				return true;
		    }
    	}
		return false;
	}

	@Override
	public void cancel(Intent data) {
		backRegStep();
	}

	@Override
	public void done(Intent data) {
		mIsViewCtrlKeypad = false;
		if (data == null)
			return;

		String secureData = data.getStringExtra(TransKeyActivity.mTK_PARAM_SECURE_DATA);
		String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
		String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
		byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
		int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);

		if (iRealDataLength != mCommonUserInfo.getOtpPWLength()) {
			String msg = getString(R.string.message_pin_right_length, mCommonUserInfo.getOtpPWLength());
			mLayoutMsg.setVisibility(View.VISIBLE);
			mTextMsg.setText(msg);
			showKeyPad();
			AniUtils.shakeView(mLayoutMsg);
			return;
		}

		String plainData = "";
		try {
			TransKeyCipher tkc = new TransKeyCipher("SEED");
			tkc.setSecureKey(secureKey);

			byte pbPlainData[] = new byte[iRealDataLength];
			if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
				plainData = new String(pbPlainData);
			} else {
				plainData = "";
			}

			if (TextUtils.isEmpty(plainData)) {
				mLayoutMsg.setVisibility(View.VISIBLE);
				mTextMsg.setText(R.string.msg_err_decode_pin);
				showKeyPad();
				AniUtils.shakeView(mLayoutMsg);
				return;
			}
		} catch (Exception e) {
			Logs.printException(e);
		}

		if (mRegStep == REG_STEP.STEP_01) {
			if (mCommonUserInfo.getOtpPWLength() == 4) {
				// 중복 4자리 체크
				if (Utils.isDuplicate(plainData, 4)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText("4개의 동일한 숫자는 사용할 수 없습니다.");
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
				// 연속 4자리 체크
				if (Utils.isContinueNum(plainData, 4)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText("4개의 연속된 숫자는 사용할 수 없습니다.");
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
				// 생년월일/폰번호는 로그인 후라면 로그인 세션에서, 로그인 전이면 폰인증에서 받은 정보를 사용
				LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
				String birth = loginUserInfo.getBRDD();
				String phoneNumber = loginUserInfo.getCLPH_TONO() + loginUserInfo.getCLPH_SRNO();

				// 생년월일 체크
				if (!TextUtils.isEmpty(birth) && birth.contains(plainData)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText("생년월일은 사용할 수 없습니다.");
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
				// 핸드폰번호 체크
				if (!TextUtils.isEmpty(phoneNumber) && phoneNumber.contains(plainData)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText("핸드폰 번호는 사용할 수 없습니다.");
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
			} else {
				if (Utils.isDuplicate(plainData, Const.OTP_PW_NUM_DUPLICATE)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText(R.string.msg_err_reg_pin_duplicate);
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
				if (Utils.isContinueNum(plainData, Const.OTP_PW_NUM_DUPLICATE)) {
					mLayoutMsg.setVisibility(View.VISIBLE);
					mTextMsg.setText(R.string.msg_err_reg_pin_continue_num);
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}

				// 생년월일/폰번호는 로그인 후라면 로그인 세션에서, 로그인 전이면 폰인증에서 받은 정보를 사용
				LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
				String birth = loginUserInfo.getBRDD();
				String phoneNumber0 = loginUserInfo.getCLPH_LCNO();
				String phoneNumber = loginUserInfo.getCLPH_TONO() + loginUserInfo.getCLPH_SRNO();

				// 생년월일 체크
				if (!TextUtils.isEmpty(birth) && birth.length() == 8) {
					for (int i = 0 ; i < 5 ; i++) {
						String tmpStr = birth.substring(i, i + 4);
						if (plainData.contains(tmpStr)) {
							mLayoutMsg.setVisibility(View.VISIBLE);
							mTextMsg.setText("생년월일은 사용할 수 없습니다.");
							showKeyPad();
							AniUtils.shakeView(mLayoutMsg);
							return;
						}
					}
				}
				// 핸드폰번호 체크
				if (!TextUtils.isEmpty(phoneNumber) && (phoneNumber.length() == 8 || phoneNumber.length() == 7)) {
					int tmpLengh = 5;
					if (phoneNumber.length() == 7)
						tmpLengh = 4;
					for (int i = 0 ; i < tmpLengh ; i++) {
						String tmpStr = phoneNumber.substring(i, i + 4);
						if (plainData.contains(tmpStr)) {
							mLayoutMsg.setVisibility(View.VISIBLE);
							mTextMsg.setText("핸드폰 번호는 사용할 수 없습니다.");
							showKeyPad();
							AniUtils.shakeView(mLayoutMsg);
							return;
						}

					}

					String checkExtend = phoneNumber0 + phoneNumber.substring(0, 3);
					if (plainData.startsWith(checkExtend)) {
						mLayoutMsg.setVisibility(View.VISIBLE);
						mTextMsg.setText("핸드폰 번호는 사용할 수 없습니다.");
						showKeyPad();
						AniUtils.shakeView(mLayoutMsg);
						return;
					}
				}
			}

			if (!TextUtils.isEmpty(mCommonUserInfo.getOtpPWAuthData())) {
				if (mCommonUserInfo.getOtpPWAuthData().equalsIgnoreCase(plainData)) {
					mTextMsg.setText(R.string.msg_match_org_pin);
					mLayoutMsg.setVisibility(View.VISIBLE);
					showKeyPad();
					AniUtils.shakeView(mLayoutMsg);
					return;
				}
			}

			String desc = "";
			if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
				if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_OTP) {
					desc = getString(R.string.verify) + getString(R.string.msg_retry_input_otp_pw);
				} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_MOTP) {
					desc = getString(R.string.msg_verify_motp) + " " + getString(R.string.msg_retry_input_otp_pw);
				} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_PW) {
					desc = getString(R.string.msg_retry_input_otp_pw);
				}
			} else {
				String titleStr = "를\n다시 한번 입력해 주세요.";
				desc = mCommonUserInfo.getOtpTitle() + titleStr;
			}

			mTextDesc.setText(desc);
			mFirstPin = plainData;
			mRegStep = REG_STEP.STEP_02;
			showKeyPad();

		} else if (mRegStep == REG_STEP.STEP_02) {
			if (!isEqualsPin(mFirstPin, plainData)) {
				if(mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
					mTextMsg.setText(R.string.msg_no_match_reg_pin);
				} else {
					mTextMsg.setText(mCommonUserInfo.getOtpTitle() + "가 일치하지 않습니다.");
				}
				mLayoutMsg.setVisibility(View.VISIBLE);
				AniUtils.shakeView(mLayoutMsg);

				Handler delayHandler = new Handler();
				delayHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						backRegStep();
						mLayoutMsg.setVisibility(View.INVISIBLE);
					}
				}, 500);
				return;
			}

			//해당 Activity는 웹에서만 사용한다.
			//입력된 비밀번호를 자바스크립트 콜백으로 리턴
			Intent intent = new Intent();
			mCommonUserInfo.setSecureData(secureData);
			mCommonUserInfo.setPlainData(plainData);
			mCommonUserInfo.setDummyData(dummyData);
			mCommonUserInfo.setCipherData(cipherData);
			intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
			setResult(RESULT_OK, intent);
			finish();
			overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);

		}
	}
	
	@Override
	public void onBackPressed() {
		backRegStep();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	public void input(int arg0) {
		if (arg0 == ITransKeyActionListenerEx.INPUT_CHARACTER_KEY) {
			mLayoutMsg.setVisibility(View.INVISIBLE);

			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();
			String strValue6 = mEditOtpPw06.getText().toString();

			if (TextUtils.isEmpty(strValue1)) {
				mEditOtpPw01.setText("0");
				mImageOtpPw01.setImageResource(R.drawable.ico_pin_on);
				mEditOtpPw02.requestFocus();
			} else if(TextUtils.isEmpty(strValue2)) {
				mEditOtpPw02.setText("0");
				mImageOtpPw02.setImageResource(R.drawable.ico_pin_on);
				mEditOtpPw03.requestFocus();
			} else if(TextUtils.isEmpty(strValue3)) {
				mEditOtpPw03.setText("0");
				mImageOtpPw03.setImageResource(R.drawable.ico_pin_on);
				mEditOtpPw04.requestFocus();
			} else if(TextUtils.isEmpty(strValue4)) {
				mEditOtpPw04.setText("0");
				mImageOtpPw04.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getOtpPWLength() == 5)
					mEditOtpPw05.requestFocus();
			} else if(TextUtils.isEmpty(strValue5)) {
				mEditOtpPw05.setText("0");
				mImageOtpPw05.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getOtpPWLength() == 6)
					mEditOtpPw06.requestFocus();
			} else if(TextUtils.isEmpty(strValue6)) {
				mEditOtpPw06.setText("0");
				mImageOtpPw06.setImageResource(R.drawable.ico_pin_on);
				if (mCommonUserInfo.getOtpPWLength() == 7)
					mEditOtpPw07.requestFocus();
			} else {
				mEditOtpPw07.setText("0");
				mImageOtpPw07.setImageResource(R.drawable.ico_pin_on);
			}

			int length = mTKMgr.getInputLength();
			if (mRegStep == REG_STEP.STEP_01 && length == mCommonUserInfo.getOtpPWLength()) {
				mTKMgr.done();
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_BACKSPACE_KEY) {
			String strValue1 = mEditOtpPw01.getText().toString();
			String strValue2 = mEditOtpPw02.getText().toString();
			String strValue3 = mEditOtpPw03.getText().toString();
			String strValue4 = mEditOtpPw04.getText().toString();
			String strValue5 = mEditOtpPw05.getText().toString();
			String strValue6 = mEditOtpPw06.getText().toString();
			String strValue7 = mEditOtpPw07.getText().toString();

			if (mCommonUserInfo.getOtpPWLength() == 7) {
				if (!TextUtils.isEmpty(strValue7)) {
					mEditOtpPw07.setText("");
					mImageOtpPw07.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw06.requestFocus();
					return;
				} else  if (!TextUtils.isEmpty(strValue6)) {
					mEditOtpPw06.setText("");
					mImageOtpPw06.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw05.requestFocus();
					return;
				} else {
					if(!TextUtils.isEmpty(strValue5)) {
						mEditOtpPw05.setText("");
						mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
						mEditOtpPw04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getOtpPWLength() == 6) {
				if (!TextUtils.isEmpty(strValue6)) {
					mEditOtpPw06.setText("");
					mImageOtpPw06.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw05.requestFocus();
					return;
				} else {
					if(!TextUtils.isEmpty(strValue5)) {
						mEditOtpPw05.setText("");
						mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
						mEditOtpPw04.requestFocus();
						return;
					}
				}
			} else if (mCommonUserInfo.getOtpPWLength() == 5) {
				if(!TextUtils.isEmpty(strValue5)) {
					mEditOtpPw05.setText("");
					mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
					mEditOtpPw04.requestFocus();
					return;
				}
			}

			if (!TextUtils.isEmpty(strValue4)) {
				mEditOtpPw04.setText("");
				mImageOtpPw04.setImageResource(R.drawable.ico_pin_line_off);
				mEditOtpPw03.requestFocus();
			} else if (!TextUtils.isEmpty(strValue3)) {
				mEditOtpPw03.setText("");
				mImageOtpPw03.setImageResource(R.drawable.ico_pin_line_off);
				mEditOtpPw02.requestFocus();
			} else if (!TextUtils.isEmpty(strValue2)) {
				mEditOtpPw02.setText("");
				mImageOtpPw02.setImageResource(R.drawable.ico_pin_line_off);
				mEditOtpPw01.requestFocus();
			} else if (!TextUtils.isEmpty(strValue1)) {
				mEditOtpPw01.setText("");
				mImageOtpPw01.setImageResource(R.drawable.ico_pin_line_off);
			}
		} else if (arg0 == ITransKeyActionListenerEx.INPUT_CLEARALL_BUTTON) {
			clearInput();
		}
	}

	@Override
	public void minTextSizeCallback() {

	}

	@Override
	public void maxTextSizeCallback() {

	}

	/**
	 * Extras 값 획득
	 */
	private void getExtra() {
		mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
		if(mCommonUserInfo.getOtpPWLength() <= 0 || mCommonUserInfo.getOtpPWLength() > Const.OTP_PW_JUMIN_LENGTH)
			mCommonUserInfo.setOtpPWLength(Const.OTP_PW_NUM_LENGTH);
	}

	/**
	 * 화면 초기화
	 */
	private void initUX() {
		mLayoutMsg = (LinearLayout) findViewById(R.id.layout_otp_pw_reg_msg);
		mTextDesc = (TextView) findViewById(R.id.textview_otp_pw_reg_desc);
		mTextMsg = (TextView) findViewById(R.id.textview_otp_pw_reg_msg);

		mExitOtpPw = (EditText) findViewById(R.id.layout_otp_pw_input_reg).findViewById(R.id.editText);
		mEditOtpPw01 = (EditText) findViewById(R.id.edittext_otp_pw_reg_01);
		mEditOtpPw01.setOnTouchListener(this);
		mImageOtpPw01 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_01);
		mEditOtpPw01.setOnKeyListener(null);
		mEditOtpPw02 = (EditText) findViewById(R.id.edittext_otp_pw_reg_02);
		mEditOtpPw02.setOnTouchListener(this);
		mEditOtpPw02.setOnKeyListener(null);
		mImageOtpPw02 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_02);
		mEditOtpPw03 = (EditText) findViewById(R.id.edittext_otp_pw_reg_03);
		mEditOtpPw03.setOnTouchListener(this);
		mEditOtpPw03.setOnKeyListener(null);
		mImageOtpPw03 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_03);
		mEditOtpPw04 = (EditText) findViewById(R.id.edittext_otp_pw_reg_04);
		mEditOtpPw04.setOnTouchListener(this);
		mEditOtpPw04.setOnKeyListener(null);
		mImageOtpPw04 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_04);
		RelativeLayout layoutOtpPw05 = (RelativeLayout) findViewById(R.id.layout_otp_pw_reg_05);
		mEditOtpPw05 = (EditText) findViewById(R.id.edittext_otp_pw_reg_05);
		mEditOtpPw05.setOnTouchListener(this);
		mEditOtpPw05.setOnKeyListener(null);
		mImageOtpPw05 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_05);
		RelativeLayout layoutOtpPw06 = (RelativeLayout) findViewById(R.id.layout_otp_pw_reg_06);
		mEditOtpPw06 = (EditText) findViewById(R.id.edittext_otp_pw_reg_06);
		mEditOtpPw06.setOnTouchListener(this);
		mEditOtpPw06.setOnKeyListener(null);
		mImageOtpPw06 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_06);
		RelativeLayout layoutOtpPw07 = (RelativeLayout) findViewById(R.id.layout_otp_pw_reg_07);
		mEditOtpPw07 = (EditText) findViewById(R.id.edittext_otp_pw_reg_07);
		mEditOtpPw07.setOnTouchListener(this);
		mEditOtpPw07.setOnKeyListener(null);
		mImageOtpPw07 = (ImageView) findViewById(R.id.imageview_otp_pw_reg_07);
		findViewById(R.id.tv_close).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_OTP) {

				} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_MOTP) {

				} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_PW) {

				} else if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB) {
					setResult(RESULT_CANCELED);
				}
				finish();
				overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);
			}
		});

		String desc = "";

		if (mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_NATIVE) {
			if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_OTP) {
				if (TextUtils.isEmpty(mCommonUserInfo.getOtpPWAuthData()))
					desc = getString(R.string.verify) + getString(R.string.msg_reg_otp_pw);
				else
					desc = getString(R.string.msg_new) + " " + getString(R.string.verify) + getString(R.string.msg_reg_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_MOTP) {
				if (TextUtils.isEmpty(mCommonUserInfo.getOtpPWAuthData()))
					desc = getString(R.string.msg_verify_motp) + getString(R.string.msg_reg_otp_pw);
				else
					desc = getString(R.string.msg_new) + " " + getString(R.string.msg_verify_motp) + "\n" + getString(R.string.msg_reg_otp_pw);
			} else if (mCommonUserInfo.getOtpPWAccessType() == Const.OTP_PW_ACCESS_REG_PW) {
				if (TextUtils.isEmpty(mCommonUserInfo.getOtpPWAuthData()))
					desc = getString(R.string.msg_reg_otp_pw);
				else
					desc = getString(R.string.msg_new) + " " + getString(R.string.msg_reg_otp_pw);
			}
		} else {
			String titleStr = "를\n등록해주세요.";
			desc = mCommonUserInfo.getOtpTitle() + titleStr;
		}

        mTextDesc.setText(desc);

		if (mCommonUserInfo.getOtpPWLength() == 4) {
			layoutOtpPw05.setVisibility(View.GONE);
			layoutOtpPw06.setVisibility(View.GONE);
			layoutOtpPw07.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getOtpPWLength() == 5) {
			layoutOtpPw06.setVisibility(View.GONE);
			layoutOtpPw07.setVisibility(View.GONE);
		} else if (mCommonUserInfo.getOtpPWLength() == 6) {
			layoutOtpPw07.setVisibility(View.GONE);
		}

		//boolean isAutoFocus = (mCommonUserInfo.getOtpPWLength() != 7 && mCommonUserInfo.getOtpPWEntryType() == Const.OTP_PW_ENTRY_WEB) ? true : false;

		mTKMgr = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
				TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX,
				"Pin Code",//label
				"",//hint
				mCommonUserInfo.getOtpPWLength(),  //max length
				"",//max length msg
				mCommonUserInfo.getOtpPWLength(),  //min length
				"", //min length msg
				5,
				true,
				(FrameLayout) findViewById(R.id.keypadContainer),
				mExitOtpPw,
				(HorizontalScrollView) (findViewById(R.id.layout_otp_pw_input_reg)).findViewById(R.id.keyscroll),
				(LinearLayout) (findViewById(R.id.layout_otp_pw_input_reg)).findViewById(R.id.keylayout),
				(ImageButton) (findViewById(R.id.layout_otp_pw_input_reg)).findViewById(R.id.clearall),
				(RelativeLayout) findViewById(R.id.keypadBallon),
				null,
				false,
				true);
		mTKMgr.showKeypad(mKeypadType);
	}

	/**
	 * 핀코드 입력값 화면 초기화
	 */
	private void clearInput() {
		mEditOtpPw01.setText("");
		mImageOtpPw01.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw02.setText("");
		mImageOtpPw02.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw03.setText("");
		mImageOtpPw03.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw04.setText("");
		mImageOtpPw04.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw05.setText("");
		mImageOtpPw05.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw06.setText("");
		mImageOtpPw06.setImageResource(R.drawable.ico_pin_line_off);
		mEditOtpPw07.setText("");
		mImageOtpPw07.setImageResource(R.drawable.ico_pin_line_off);

		mEditOtpPw01.requestFocus();
	}

	/**
	 * 등록 시, 두번의 핀코드 입력값 비교
	 * @param orgPin 첫번째 입력 핀코드값
	 * @param newPin 두번째 입력 핀코드값
	 * @return
	 */
	private boolean isEqualsPin(String orgPin, String newPin) {
		if (TextUtils.isEmpty(orgPin) || TextUtils.isEmpty(newPin))
			return false;

		if (orgPin.length() != newPin.length())
			return false;

		if (orgPin.equalsIgnoreCase(newPin))
			return true;

		return false;
	}

	/**
	 * 키패드 보이기
	 */
	private void showKeyPad() {
		mTKMgr.showKeypad(mKeypadType);
		clearInput();
		mExitOtpPw.requestFocus();
		mIsViewCtrlKeypad = true;
	}

	/**
	 * 등록 시 입력 프로세스 뒤로 이동
	 */
	private void backRegStep() {
		if (mRegStep == REG_STEP.STEP_01) {
			setResult(RESULT_CANCELED);
			finish();
			overridePendingTransition(R.anim.stay, R.anim.sliding_dialog_down);

		} else if (mRegStep == REG_STEP.STEP_02) {
			mFirstPin = null;
			mTextDesc.setText(mCommonUserInfo.getOtpTitle() + "를\n입력해주세요.");
			mRegStep = REG_STEP.STEP_01;
			showKeyPad();
		}
	}
}