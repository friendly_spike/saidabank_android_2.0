package com.sbi.saidabank.activity.transaction;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

/**
 * Saidabanking_android
 * <p>
 * Class: TransferFailActivity
 * Created by 950485 on 2019. 02. 21..
 * <p>
 * Description: 이체 실패 화면 */

public class ITransferFailActivity extends BaseActivity {
    private int mEntryType = 0;
    private int mFailType = 0;
    private String mFailRet = "";

    private Button mBtnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_fail);

        Intent intent = getIntent();
        mEntryType = intent.getIntExtra(Const.INTENT_TRANSFER_ENTRY_TYPE, 0);
        mFailType = intent.getIntExtra(Const.INTENT_TRANSFER_FAIL_TYPE, 0);
        mFailRet = intent.getStringExtra(Const.INTENT_TRANSFER_FAIL_RET);

        if(mEntryType == 3){
            initUXSafeDeal();
        }else{
            initUX();
        }

    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        mBtnConfirm.performClick();
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        TextView textMsgFail = (TextView) findViewById(R.id.textview_msg_fail);
        RelativeLayout layoutFailNomal = (RelativeLayout) findViewById(R.id.layout_fail_nomal);
        LinearLayout layoutFailBalance = (LinearLayout) findViewById(R.id.layout_fail_low_balance);
        if (mFailType != 0) {
            layoutFailNomal.setVisibility(View.GONE);
            layoutFailBalance.setVisibility(View.VISIBLE);
        }

        if (mFailType == 0) {
            /*if (!TextUtils.isEmpty(mFailRet)) {
                textMsgFail.setText(mFailRet);
            }*/
        } else if (mEntryType == 1)
            textMsgFail.setText(R.string.msg_transfer_fail_phone_desc);

        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mEntryType == 0) {
                    // 이체 초기 화면으로 이동
                    ITransferDataMgr.getInstance().clearTransferData();
                    Intent intent = new Intent(ITransferFailActivity.this, ITransferSelectReceiverActivity.class);
                    startActivity(intent);
                    finish();
                }

                finish();
            }
        });
    }

    private void initUXSafeDeal() {

        ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.ico_sd_fail);


        TextView textMsgFail = (TextView) findViewById(R.id.textview_msg_fail);
        textMsgFail.setText("일시적인 오류로 이체가 실패되었습니다.\n이체를 다시 시도해 주세요.");

        mBtnConfirm = (Button) findViewById(R.id.btn_confirm);
        mBtnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(TransferFailActivity.this, WebMainActivity.class);
//                intent.putExtra("url", WasServiceUrl.TRA0090100.getServiceUrl());
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();

            }
        });

        mBtnConfirm.setBackgroundColor(Color.parseColor("#293542"));
        mBtnConfirm.setTextColor(Color.parseColor("#00ebff"));
    }

}
