package com.sbi.saidabank.activity.main2.common.beforeload;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.main2.common.banner.BannerLayout;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

public class MyAccBeforeLoadLayout extends RelativeLayout {
    private static final int MAX_VIEW_COUNT = 4;
    private static final int MAX_ROLLING_CNT = 5;
    private static final int DELAY_TIME = 400;
    private static final int ANI_TIME = 500;

    private TextView[] mTvList;
    private boolean isAlphaZero;

    private LinearLayout mLayoutBody;

    public MyAccBeforeLoadLayout(Context context) {
        super(context);
        initUX(context);
    }

    public MyAccBeforeLoadLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_main2_before_loaded_myaccount, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mLayoutBody = layout.findViewById(R.id.layout_body);

        mTvList =  new TextView[MAX_VIEW_COUNT];

        isAlphaZero = true;

        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        //Logs.e("BannerLayout - onGlobalLayout - height : " + getHeight());
                        int lcdWidth = GraphicUtils.getActivityWidth(getContext());
                        int widthDip = (int)Utils.pixelToDp(getContext(),lcdWidth);

                        int pagerHeightDip = BannerLayout.RECOM_BANNER_HEIGHT;
                        if(BannerLayout.RECOM_BANNER_WIDHT != widthDip){
                            pagerHeightDip = widthDip * BannerLayout.RECOM_BANNER_HEIGHT / BannerLayout.RECOM_BANNER_WIDHT;
                        }

                        int topMargin = (int)Utils.dpToPixel(getContext(),pagerHeightDip) + (int) Utils.dpToPixel(getContext(),62);

                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)mLayoutBody.getLayoutParams();
                        params.topMargin = topMargin;
                        mLayoutBody.setLayoutParams(params);

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });

    }

    private void startAnimation(final int position,final View v,int delayTime){


        float startAlpha = 0f;
        float endAlpha = 0f;
        if(isAlphaZero){
            startAlpha = 1f;
            endAlpha = 0f;
        }else{
            startAlpha = 0f;
            endAlpha = 1f;
        }

        final float endAlphaFinal = endAlpha;

        v.setAlpha(startAlpha);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                v.animate()
                        .alpha(endAlphaFinal)
                        .setDuration(ANI_TIME)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {

                                if(position+1 == MAX_VIEW_COUNT){
                                    isAlphaZero = !isAlphaZero;

                                    for(int i=0;i<MAX_VIEW_COUNT;i++){
                                        mTvList[i].setAlpha(1f);
                                        startAnimation(i,mTvList[i],DELAY_TIME * i);
                                    }

                                }
                            }
                        } )
                        .start();
            }
        }, delayTime);
    }

    public void hideLayout(){

        animate()
                .alpha(0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(GONE);
                    }
                } )
                .start();

    }
}
