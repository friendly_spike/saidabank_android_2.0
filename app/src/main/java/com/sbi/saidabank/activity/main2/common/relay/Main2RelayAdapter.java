package com.sbi.saidabank.activity.main2.common.relay;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;

import java.util.ArrayList;

public class Main2RelayAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<Main2RelayInfo> mRelayArray;
    private OnItemSelectListener mOnItemSelectListener;
    private boolean isOriginSize;

    static class ViewHolder {
        RelativeLayout llBody;
        ImageView ivTheDay;
        TextView tvDay;
        TextView tvLabel1;
        TextView tvLabel2;
        ImageView ivDelete;
        LinearLayout llRelayBtn;
        TextView    tvBtnName;

        LinearLayout llDeleteBg;
        TextView     tvDelMsg;
        LinearLayout llBtnOk;
        LinearLayout llBtnCancel;

        View     v_line;
    }

    public interface OnItemSelectListener{
        void onItemSelect(int position);
        void onItemClickRelayBtn(int position);
        void onItemDelete(int position);
    }

    public Main2RelayAdapter(Context context, OnItemSelectListener listener) {
        this.mContext = context;
        this.mRelayArray = new ArrayList<Main2RelayInfo>();
        this.mOnItemSelectListener = listener;
        isOriginSize = true;
    }

    @Override
    public int getCount() {
        return mRelayArray.size();
    }

    @Override
    public Object getItem(int position) {
        return mRelayArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();

            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_main2_relay_list_item, null);

            viewHolder.llBody = (RelativeLayout) convertView.findViewById(R.id.layout_body);
            viewHolder.ivTheDay = (ImageView) convertView.findViewById(R.id.iv_theday);
            viewHolder.tvDay = (TextView) convertView.findViewById(R.id.tv_day);
            viewHolder.tvLabel1 = (TextView) convertView.findViewById(R.id.tv_label_1);
            viewHolder.tvLabel2 = (TextView) convertView.findViewById(R.id.tv_label_2);
            viewHolder.llRelayBtn = (LinearLayout) convertView.findViewById(R.id.layout_relay_btn);
            viewHolder.tvBtnName = (TextView) convertView.findViewById(R.id.tv_btn_name);
            viewHolder.ivDelete = (ImageView) convertView.findViewById(R.id.iv_delete);

            viewHolder.llDeleteBg = (LinearLayout) convertView.findViewById(R.id.layout_delete);
            viewHolder.tvDelMsg =  (TextView) convertView.findViewById(R.id.tv_del_msg);
            viewHolder.llBtnOk = (LinearLayout) convertView.findViewById(R.id.layout_btn_del_ok);
            viewHolder.llBtnCancel = (LinearLayout) convertView.findViewById(R.id.layout_btn_del_cancel);


            viewHolder.v_line  = (View) convertView.findViewById(R.id.v_line);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Main2RelayInfo relayInfo = (Main2RelayInfo) mRelayArray.get(position);

        viewHolder.llBody.setTag(position);
        viewHolder.llBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                mOnItemSelectListener.onItemSelect(pos);
            }
        });



        switch (relayInfo.getDSCT_CD()){
            case "310"://보통예금
                display_310(viewHolder,relayInfo);
                break;
            case "320"://신용대출
            case "321"://마이너스통장
            case "322"://소액마이너스통장
            case "323"://표준사잇돌2
            case "324"://대출가조회 - 신용대출
            case "325"://대출가조회 - 마이너스통장
            case "326"://프리미엄중금리신용대출
            case "327"://대출가조회 - 프리미엄중금리신용대출
            case "328"://비상금대출
                display_320328(viewHolder,relayInfo);
                break;
            case "311"://커플통장 이체요청
                display_311(viewHolder,relayInfo);
                break;
            case "312"://커플통장 초대요청
                display_312(viewHolder,relayInfo);
                break;
            case "313"://가족카드 이어하기
                display_313(viewHolder,relayInfo);
                break;

        }

        viewHolder.llDeleteBg.setVisibility(View.GONE);

        if(!relayInfo.getDSCT_CD().equals("313")) {
            viewHolder.tvDay.setText("D-" + relayInfo.getREMN_DCNT());
        }else{
            if(relayInfo.getFAM_CARD_PRGS_STEP_CD().equals("04")){
                viewHolder.tvDay.setText("심사\n대기");
                viewHolder.tvDay.setTextColor(Color.parseColor("#d0ddde"));
                viewHolder.ivTheDay.setImageResource(R.drawable.img_status_circle);
            }else{
                viewHolder.tvDay.setText("D-" + relayInfo.getREMN_DCNT());
            }
        }


        int radius = (int) Utils.dpToPixel(mContext,14);
        viewHolder.llRelayBtn.setBackground(GraphicUtils.getRoundCornerDrawable("#34446B",new int[]{radius,radius,radius,radius}));

        viewHolder.llRelayBtn.setTag(position);
        viewHolder.llRelayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int)v.getTag();
                mOnItemSelectListener.onItemClickRelayBtn(pos);
            }
        });

        viewHolder.ivDelete.setTag(viewHolder.llDeleteBg);
        viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = (View)v.getTag();
                view.setVisibility(View.VISIBLE);
            }
        });

        if(position == mRelayArray.size()-1){
            viewHolder.v_line.setVisibility(View.GONE);
        }else{
            if(isOriginSize){
                viewHolder.v_line.setVisibility(View.GONE);
            }else{
                viewHolder.v_line.setVisibility(View.VISIBLE);
            }
        }

        //삭제 화면 처리
        viewHolder.llBtnOk.setTag(position);
        viewHolder.llBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = (int) v.getTag();
                mOnItemSelectListener.onItemDelete(pos);

            }
        });
        viewHolder.llBtnOk.setBackground(GraphicUtils.getRoundCornerDrawable("#34446B",new int[]{radius,radius,radius,radius}));

        viewHolder.llBtnCancel.setTag(viewHolder.llDeleteBg);
        viewHolder.llBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = (View)v.getTag();
                view.setVisibility(View.GONE);
            }
        });
        viewHolder.llBtnCancel.setBackground(GraphicUtils.getRoundCornerDrawable("#8993aa",new int[]{radius,radius,radius,radius}));


        return convertView;
    }

    public void setRelayArray(ArrayList<Main2RelayInfo> arrayList){
        if(this.mRelayArray.size() > 0){
            this.mRelayArray.clear();
        }
        if(arrayList.size() > 0) {
            this.mRelayArray.addAll(arrayList);
        }
        notifyDataSetChanged();
    }

    //입출금통장
    private void display_310(ViewHolder viewHolder,Main2RelayInfo relayInfo){

        viewHolder.tvLabel1.setText(relayInfo.getTRNF_DEPR_NM() + " 개설 진행중");
        viewHolder.tvLabel2.setText("신청정보는 " + relayInfo.getREMN_DCNT() + "일간 유효합니다.");
        viewHolder.tvBtnName.setText("이어하기");

        viewHolder.tvDelMsg.setText("이어하기를 취소할 경우 처음부터 다시 진행합니다.");
    }

    /**
     * 신용대출 이어하기 화면표시
     * @param viewHolder
     * @param relayInfo
     */
    private void display_320328(ViewHolder viewHolder,Main2RelayInfo relayInfo){
        viewHolder.tvLabel1.setText(relayInfo.getTRNF_DEPR_NM() + " 신청 진행중");
        viewHolder.tvLabel2.setText("신청정보는 " + relayInfo.getREMN_DCNT() + "일간 유효합니다.");
        viewHolder.tvBtnName.setText("이어하기");

        viewHolder.tvDelMsg.setText("이어하기를 취소할 경우 처음부터 다시 진행합니다.");
    }

    //커플통장 - 이체요청
    private void display_311(ViewHolder viewHolder,Main2RelayInfo relayInfo){
        viewHolder.tvLabel1.setText("커플이체 승인 요청");

        String TRAM = relayInfo.getTRAM();
        if(!TextUtils.isEmpty(TRAM)){
            TRAM = Utils.moneyFormatToWon(Double.valueOf(TRAM));
        }

        viewHolder.tvLabel2.setText("받는분 : "+relayInfo.getTRNF_DEPR_NM() + "\n이체금액 : " + TRAM + "원");
        viewHolder.tvBtnName.setText("확인하기");

        viewHolder.tvDelMsg.setText("이체요청을 거절하시겠습니까?");
    }


    //커플통장 - 초대요청
    private void display_312(ViewHolder viewHolder,Main2RelayInfo relayInfo){
        viewHolder.tvLabel1.setText("커플통장 초대");
        viewHolder.tvLabel2.setText(relayInfo.getFAX_SEND_PRGS_CNTN());
        viewHolder.tvBtnName.setText("수락하기");

        viewHolder.tvDelMsg.setText("커플 초대요청을 거절하시겠습니까?");
    }

    //가족카드
    private void display_313(ViewHolder viewHolder,Main2RelayInfo relayInfo){
        String label1Str="";
        String label2Str="";
        switch (relayInfo.getFAM_CARD_PRGS_STEP_CD()){
            case "01":
                label1Str="가족카드 신청";
                //label2Str="신청정보 작성을 요청받았습니다";
                viewHolder.tvBtnName.setText("입력하기");
                viewHolder.tvDelMsg.setText("가족카드 신청요청을 거절하시겠습니까?");
                break;
            case "02":
                label1Str="가족카드 신청";
                //label2Str="신청정보를 작성 중입니다.";
                viewHolder.tvBtnName.setText("입력하기");
                viewHolder.tvDelMsg.setText("가족카드 신청을 취소하시겠습니까?");
                break;
            case "03":
                label1Str="가족카드 신청";
                //label2Str="상대방에게 신청정보 작성 요청중입니다.";
                viewHolder.tvBtnName.setText("입력하기");
                viewHolder.tvDelMsg.setText("가족카드 신청을 취소하시겠습니까?");
                break;
            case "04":
                label1Str="가족카드 신청";
                //label2Str="가족카드 발급 심사대기중입니다.";
                viewHolder.tvBtnName.setText("자세히보기");
                viewHolder.tvDelMsg.setText("가족카드 신청을 취소하시겠습니까?");
                break;
            case "05":
                label1Str="가족카드 심사보완";
                //label2Str="가족관계 증명서를 확인 후 다시 제출해주세요.";
                viewHolder.tvBtnName.setText("확인하기");
                viewHolder.tvDelMsg.setText("가족카드 신청을 취소하시겠습니까?");
                break;
        }
        label2Str=relayInfo.getFAX_SEND_PRGS_CNTN();
        viewHolder.tvLabel1.setText(label1Str);
        viewHolder.tvLabel2.setText(label2Str);
    }

    public void setNowOriginSize(boolean flag){
        isOriginSize = flag;
        notifyDataSetInvalidated();
    }
}
