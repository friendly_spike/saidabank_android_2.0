package com.sbi.saidabank.activity.main2.homecouple;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.safedeal.SlideBaseLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideMyAccountLayout;
import com.sbi.saidabank.common.dialog.SlidingMain2CouplePhotoSettingDialog;
import com.sbi.saidabank.common.dialog.SlidingMain2TransferDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CoupleIconBigView;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CoupleTitleLayout extends RelativeLayout {

    private static final int SMALL_SCALE_ICON_HEIGHT = 24;
    private static final int SMALL_SCALE_ICON_TOP_MARGIN = 13;

    private static final int BIG_SCALE_ICON_HEIGHT = 54;
    private static final int BIG_SCALE_ICON_TOP_MARGIN = 57;

    private int mOriginTitleViewHeight;
    private int mMinTitleViewHeight;
    private int mChangedtitleHeight;

    //Scale을 위한 기본값을 가지고 있도록 한다.
    private int mCountViewWidth;
    private int mCountiewHeight;
    private int mCoupleViewWidth;
    private int mCoupleViewHeight;

    private ImageView mBackgroundImageView;

    private ImageView mIvSetting;

    private CoupleIconBigView mCoupleIconBigView;
    private View mVBackScreen;

    private LinearLayout mLayoutCoupleCntSmall;
    private TextView mTvCountCntSmall;

    private RelativeLayout mLayoutCoupleCountBig;
    private TextView mTvCountLablel1, mTvCountLablel2;
    private View mVLine;

    private OnCoupleActionLayoutListener mActionListion;

    public void setOnCoupleActionLayoutListener(OnCoupleActionLayoutListener listener) {
        this.mActionListion = listener;
    }

    public CoupleTitleLayout(Context context) {
        super(context);
        initUX(context);
    }

    public CoupleTitleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_couple_title, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        // 스크롤 올릴때 배경 가려주는 뷰.
        mVBackScreen = layout.findViewById(R.id.v_back_screen);

        mBackgroundImageView = layout.findViewById(R.id.iv_back_img);
        String bgImgPath = Prefer.getCoupleBackgroundImagePath(getContext());
        if (!TextUtils.isEmpty(bgImgPath)) {
            setTitleBackImage(bgImgPath, false);
        }

        mIvSetting = layout.findViewById(R.id.iv_setting);
        mIvSetting.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SlidingMain2CouplePhotoSettingDialog dialog = new SlidingMain2CouplePhotoSettingDialog(getContext(), new SlidingMain2CouplePhotoSettingDialog.OnConfirmListener() {
                    @Override
                    public void onConfirmPress(int selectIndex) {
                        if (selectIndex == 1) {
                            //기존에 설정된 이미지가 있으면 지워준다.
                            String bgImgPath = Prefer.getCoupleBackgroundImagePath(getContext());
                            if (!TextUtils.isEmpty(bgImgPath)) {
                                delTitleBackImage(bgImgPath);
                                Prefer.setCoupleBackgroundImagePath(getContext(), "");
                            }

                            mBackgroundImageView.setAlpha(0.0f);
                            mBackgroundImageView.setImageResource(R.drawable.img_couple_bg_temp);
                            mBackgroundImageView.animate()
                                    .alpha(1)
                                    .setDuration(1000)
                                    .start();

                        } else {
                            if (mActionListion != null)
                                mActionListion.onOpenCamera(selectIndex);
                        }
                    }
                });
                dialog.show();
            }
        });

        mLayoutCoupleCntSmall = layout.findViewById(R.id.layout_couple_count_small);
        mTvCountCntSmall = layout.findViewById(R.id.tv_couple_count_small);


        //우리 만난지 몇일? 텍스트
        mLayoutCoupleCountBig = layout.findViewById(R.id.layout_couple_count_big);
        mTvCountLablel1 = layout.findViewById(R.id.tv_label_1);
        mTvCountLablel2 = layout.findViewById(R.id.tv_label_2);
        mVLine = layout.findViewById(R.id.v_line);

        //커플 이미지 뷰
        mCoupleIconBigView = layout.findViewById(R.id.layout_couple_icon);
        String ower_img_data = Main2DataInfo.getInstance().getUSER_PROFL_IMG_CNTN();
        String ower_name = Main2DataInfo.getInstance().getUSER_NM();
        String share_img_data = Main2DataInfo.getInstance().getSHRP_PROFL_IMG_CNTN();
        String share_name = Main2DataInfo.getInstance().getSHRP_NM();
        mCoupleIconBigView.setData(ower_img_data, ower_name, share_img_data, share_name);


        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mOriginTitleViewHeight = getHeight();
                        mMinTitleViewHeight = (int) getResources().getDimension(R.dimen.main2_home_couple_title_min_height);

                        mCountViewWidth = mLayoutCoupleCountBig.getWidth();
                        mCountiewHeight = mLayoutCoupleCountBig.getHeight();

                        mCoupleViewWidth = mCoupleIconBigView.getWidth();
                        mCoupleViewHeight = mCoupleIconBigView.getHeight();

                        ViewGroup.LayoutParams params = mVLine.getLayoutParams();
                        params.width = mTvCountLablel2.getWidth();
                        mVLine.setLayoutParams(params);

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    /**
     * 타이틀 변경을 위한 메인 함수.. 여기서 부터 시작이다.
     *
     * @param moveYpos 스크롤뷰 이동 Y좌표
     */
    public void changeViewHeight(int moveYpos) {

        //스크롤 이동에 따른 변경된 타이틀의 높이를 계산한다.
        int height = mOriginTitleViewHeight - moveYpos;
        if (height <= mMinTitleViewHeight) {
            if (mChangedtitleHeight != mMinTitleViewHeight) {
                mChangedtitleHeight = mMinTitleViewHeight;
            } else {
                return;
            }
        } else {
            mChangedtitleHeight = height;
        }

        //백스크린은 타이틀 영역 아래부터 그려서 올라옴으로 전체 타이틀 높이에서 변경된 타이틀 높이를 빼서 구한다.
        int backScreenHeight = mOriginTitleViewHeight - mChangedtitleHeight;

        //헌데 백스크린은 스롤하는 것보다 1.36의 가중치를 두어서 더 빨리 배경색으로 칠해지도록 한다.
        float ratioBackScreenHeight = backScreenHeight * 1.36f;

        //가중치로 인해 원래 타이틀높이 에서 백스크린 가중치 높이를 뺀 값이
        // 정해진 최소 타이틀 높이에 약간 않맞는 경우가 발생할수 있다. 그럴경우 무조건 최소 높이로 고정.
        if (mOriginTitleViewHeight - ratioBackScreenHeight <= mMinTitleViewHeight) {
            ratioBackScreenHeight = mOriginTitleViewHeight - mMinTitleViewHeight;
        }

        //리스트가 올라올때 하얀 스크린으로 올린만큼 덮어준다.
        ViewGroup.LayoutParams params = mVBackScreen.getLayoutParams();
        params.height = (int) ratioBackScreenHeight;
        mVBackScreen.setLayoutParams(params);


        //백그라운드 이미지는 원래 고정이었는데... 백그라운드 이미지를 백스크린 높이만큼 위로 올려달라고 해서 추가.
        mBackgroundImageView.setY(ratioBackScreenHeight * -1);

        //커플 아이콘과 D-??의 스케일을 위해 비율을 계산한다.
        //(int)Utils.dpToPixel(getContext(),39f)는 아이콘,D-??의 시작 Y위치.
        int difMaxMin = mOriginTitleViewHeight - (int) Utils.dpToPixel(getContext(), 39f) - mMinTitleViewHeight;
        int difVal = mChangedtitleHeight - (int) Utils.dpToPixel(getContext(), 39f) - mMinTitleViewHeight;

        float ratio = (float) difVal / (float) difMaxMin;
        Logs.e("retio : " + ratio);
        if (ratio < 0) {
            ratio = 0;
        }
        scaleCoupleIconView(ratio);
        scaleCoupleCountTextView(ratio);

    }

    private void scaleCoupleIconView(float ratio) {


        float viewRatio = Utils.dpToPixel(getContext(), SMALL_SCALE_ICON_HEIGHT) / Utils.dpToPixel(getContext(), BIG_SCALE_ICON_HEIGHT);
        float scaleRatio = viewRatio + (1f - viewRatio) * ratio;


        float topMarginDiff = Utils.dpToPixel(getContext(), BIG_SCALE_ICON_TOP_MARGIN) - Utils.dpToPixel(getContext(), SMALL_SCALE_ICON_TOP_MARGIN);
        final float moveMargin = Utils.dpToPixel(getContext(), SMALL_SCALE_ICON_TOP_MARGIN) + (topMarginDiff * ratio);

        float offsetX = (mCoupleViewWidth * (1 - scaleRatio)) / 2;
        float offsetY = (mCoupleViewHeight * (1 - scaleRatio)) / 2;

        mCoupleIconBigView.animate()
                .scaleX(scaleRatio)
                .scaleY(scaleRatio)
                .translationX(offsetX)
                .translationY(offsetY * -1)
                .setDuration(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mCoupleIconBigView.getLayoutParams();
                        params.topMargin = (int) moveMargin;
                        mCoupleIconBigView.setLayoutParams(params);
                    }
                })
                .start();
    }


    private void scaleCoupleCountTextView(float ratio) {

        float viewRatio = Utils.dpToPixel(getContext(), SMALL_SCALE_ICON_HEIGHT) / Utils.dpToPixel(getContext(), BIG_SCALE_ICON_HEIGHT);
        float scaleRatio = viewRatio + (1f - viewRatio) * ratio;

        float topMarginDiff = Utils.dpToPixel(getContext(), BIG_SCALE_ICON_TOP_MARGIN) - Utils.dpToPixel(getContext(), SMALL_SCALE_ICON_TOP_MARGIN);
        final float moveMargin = Utils.dpToPixel(getContext(), SMALL_SCALE_ICON_TOP_MARGIN) + (topMarginDiff * ratio);

        float offsetX = (mCountViewWidth * (1 - scaleRatio)) / 2;
        float offsetY = (mCountiewHeight * (1 - scaleRatio)) / 2;

        mLayoutCoupleCountBig.animate()
                .scaleX(scaleRatio)
                .scaleY(scaleRatio)
                .translationX(offsetX * -1)
                .translationY(offsetY * -1)
                .setDuration(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mLayoutCoupleCountBig.getLayoutParams();
                        params.topMargin = (int) moveMargin;
                        mLayoutCoupleCountBig.setLayoutParams(params);
                    }
                })
                .start();

        if (ratio == 0.0) {
            if (mLayoutCoupleCntSmall.getVisibility() != VISIBLE)
                mLayoutCoupleCntSmall.setVisibility(VISIBLE);

            if (mLayoutCoupleCountBig.getVisibility() == VISIBLE)
                mLayoutCoupleCountBig.setVisibility(GONE);

            mCoupleIconBigView.displayFamilyName(true);
        } else {
            if (mLayoutCoupleCntSmall.getVisibility() == VISIBLE)
                mLayoutCoupleCntSmall.setVisibility(GONE);

            if (mLayoutCoupleCountBig.getVisibility() != VISIBLE)
                mLayoutCoupleCountBig.setVisibility(VISIBLE);

            mCoupleIconBigView.displayFamilyName(false);
        }
    }

    public void setOriginSize() {

        //리스트가 올라올때 하얀 스크린으로 올린만큼 덮어준다.
        ViewGroup.LayoutParams params = mVBackScreen.getLayoutParams();
        params.height = 0;
        mVBackScreen.setLayoutParams(params);

        //백그라운드 이미지를 백스크린 높이만큼 위로 올려준다.
        mBackgroundImageView.setY(0);

        scaleCoupleIconView(1f);
        scaleCoupleCountTextView(1f);
    }

    public void setTitleBackImage(String filePath, boolean isSavePath) {
        MLog.d();
        try {
            File file = new File(filePath);
            if (file.exists()) {

                mBackgroundImageView.setImageURI(Uri.fromFile(file));
                if (isSavePath) {
                    String oldFilePath = Prefer.getCoupleBackgroundImagePath(getContext());
                    if (!TextUtils.isEmpty(oldFilePath)) {
                        ImgUtils.deleteImageFile(getContext(), Uri.parse(oldFilePath));
                    }
                    Prefer.setCoupleBackgroundImagePath(getContext(), filePath);
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    public void delTitleBackImage(String filePath) {
        MLog.d();
        try {
            File file = new File(filePath);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }


    @SuppressLint("SimpleDateFormat")
    public void setShareDate(String startDateStr) {
        if (DataUtil.isNotNull(startDateStr)) {

            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");

                Date startDate = format.parse(startDateStr);

                Calendar calendar = new GregorianCalendar();
                Date toDate = calendar.getTime();


                long calDate = toDate.getTime() - startDate.getTime();

                long diffDays = calDate / (24 * 60 * 60 * 1000) + 1;

                mTvCountLablel2.setText(diffDays + "일");
                mTvCountLablel2.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                ViewGroup.LayoutParams params = mVLine.getLayoutParams();
                params.width = mTvCountLablel2.getMeasuredWidth();
                mVLine.setLayoutParams(params);

                mTvCountCntSmall.setText("D+ " + diffDays);
            } catch (ParseException e) {
                // 예외 처리
                MLog.e(e);
            }
        }
    }
}
