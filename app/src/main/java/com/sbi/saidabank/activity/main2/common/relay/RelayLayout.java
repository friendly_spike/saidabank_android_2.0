package com.sbi.saidabank.activity.main2.common.relay;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.main2.common.tab.CustomTabLayout;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RelayLayout extends RelativeLayout implements View.OnTouchListener{
    private static final int ANI_TIME_SIZE_CHANGE = 150;
    //private static final int SMALL_VIEW_HEIGHT_DIP = 54;

    private Context mContext;
    private ArrayList<Main2RelayInfo> mRelayArray;
    private Main2RelayAdapter mMain2RelayAdapter;
    private ListView mListView;
    private RelativeLayout mLayoutGuard;
    private RelativeLayout mLayoutBody;
    private TextView mTvCount;


    private RelativeLayout mLayoutCount;
    private RelativeLayout mLayoutMoveBar;
    private ImageView mIvOpen;

    private CustomTabLayout mCustomTabLayout;

    private int mScreenHeight;
    private int mOriginViewHeight;

    private OnItemCountChangeListener mOnItemCountChangeListener;

    public interface OnItemCountChangeListener{
        void onItemCountChange(int count);
    }

    public RelayLayout(Context context) {
        super(context);
        initUX(context);
    }

    public RelayLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUX(Context context){
        mContext = context;
        View layout = View.inflate(context, R.layout.layout_main2_relay, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mScreenHeight = GraphicUtils.getActivityHeight(getContext());

        //백그라운드 딤처리하는 뷰
        mLayoutGuard = layout.findViewById(R.id.layout_guard);
        mLayoutGuard.setAlpha(0f);
        mLayoutGuard.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                changeOriginViewHeight();
            }
        });


        mLayoutBody = layout.findViewById(R.id.layout_list_body);
        mLayoutBody.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(getHeight() < mOriginViewHeight){
                    mIvOpen.setVisibility(GONE);

                    mLayoutMoveBar.setVisibility(VISIBLE);

                    mListView.setVisibility(VISIBLE);
                    mMain2RelayAdapter.setNowOriginSize(true);
                    AniUtils.changeViewHeightSizeAnimation(mLayoutBody,mOriginViewHeight,250);
                }
            }
        });

        mTvCount = findViewById(R.id.tv_count);
        mIvOpen = findViewById(R.id.iv_open);

        mRelayArray = new ArrayList<Main2RelayInfo>();
        mMain2RelayAdapter = new Main2RelayAdapter(getContext(), new Main2RelayAdapter.OnItemSelectListener() {
            @Override
            public void onItemSelect(int position) {
                changeMaxViewHeight();
            }

            @Override
            public void onItemClickRelayBtn(int position) {
                requestSelectRelay(position);
            }

            @Override
            public void onItemDelete(int position) {
                requestDelRelay(position);
            }
        });
        mListView = layout.findViewById(R.id.listview);
        mListView.setAdapter(mMain2RelayAdapter);
        mListView.setOnTouchListener(this);

        mLayoutMoveBar = layout.findViewById(R.id.layout_movebar);
        mLayoutMoveBar.setOnTouchListener(this);


        int radius = (int)getContext().getResources().getDimension(R.dimen.relay_layout_redius);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{0,0,radius,radius}));

        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        Logs.e("RelayLayout - onGlobalLayout - height : " + getHeight());

                        Logs.e("RelayLayout - onGlobalLayout - mListView height : " + mListView.getHeight());
                        Logs.e("RelayLayout - onGlobalLayout - mListView height : " + Utils.pixelToDp(getContext(), mListView.getHeight()));
                        Logs.e("RelayLayout - onGlobalLayout - Dim Set height : " + (int)getContext().getResources().getDimension(R.dimen.relay_list_item_height));


                        mOriginViewHeight = getHeight();
                        int activityWidth = GraphicUtils.getActivityWidth(getContext());

                        float widthDp = Utils.pixelToDp(getContext(),activityWidth);
                        Logs.e("RelayLayout - onGlobalLayout - Dim Set activityWidth : " + widthDp);
                        if(widthDp > Const.MAX_WIDTH_DIP){
                            int maxWidth = (int)Utils.dpToPixel(getContext(),Const.MAX_WIDTH_DIP);
                            ViewGroup.LayoutParams params = getLayoutParams();
                            params.width = maxWidth;
                            setLayoutParams(params);
                            invalidate();
                        }
                        slidingUp(0);
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });


    }

    public void setRelayArrayList(ArrayList<Main2RelayInfo> arrayList){
        if(mRelayArray.size() > 0)
            mRelayArray.clear();

        mMain2RelayAdapter.setRelayArray(arrayList);
        if(arrayList.size() > 0){
            mRelayArray.addAll(arrayList);
            mTvCount.setText(String.valueOf(mRelayArray.size()));

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    changeOriginViewHeight(0);
                    slidingDown(250);
                }
            }, 100);
        }else{
            slidingUp(0);
        }

        mLayoutMoveBar.setVisibility(VISIBLE);
    }

    /**
     * 뷰를 바닥으로 내림
     * @param aniTime 밀리세컨드 초
     */

    public void slidingDown(final int aniTime){
        if(mRelayArray.size() == 0) return;
        if(getVisibility() == VISIBLE) return;

        if(mCustomTabLayout.getSelectTabIdx() != 0) return;
        if(mCustomTabLayout.getSelectHomeMenuIdx() != 0) return;

        setVisibility(VISIBLE);
        this.animate()
                .translationY(0)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }
                } );
    }

    /**
     * 뷰를 위로 올림
     * @param aniTime 밀리세컨드 초
     */
    public void slidingUp(final int aniTime){
        if(getVisibility() != VISIBLE) return;

        this.animate()
                .translationY(-1*getHeight())
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(INVISIBLE);
                    }
                } );

    }

    //메인 스크롤 이동에 의해 최소사이즈로 변경
    public void setSizeSmall(){
        if(mRelayArray.size() == 0) return;
        if(getVisibility() != VISIBLE) return;

        //int height = (int)Utils.dpToPixel(getContext(),SMALL_VIEW_HEIGHT_DIP);
        int height = (int)(int)getContext().getResources().getDimension(R.dimen.relay_small_mode_height_margin);
        if(getHeight() == height) return;

        AniUtils.changeViewHeightSizeAnimation(mLayoutBody,height,250);

        mIvOpen.setVisibility(VISIBLE);
        mLayoutMoveBar.setVisibility(INVISIBLE);
        mListView.setVisibility(INVISIBLE);
    }


    //Move bar를 이용한 레이아웃 조절.
    private float startdY;
    private float moveY;
    private int   startViewHeight;
    private boolean isMove;
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(event.getActionMasked() == MotionEvent.ACTION_MOVE){
            if(v.getId() != R.id.layout_movebar){
                return true;
            }
        }


        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                //Logs.e("dispatchTouchEvent - MotionEvent.ACTION_DOWN - y : " + event.getRawY());
                startdY = event.getRawY();
                isMove = false;
                startViewHeight = mLayoutBody.getHeight();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY =  event.getRawY() - startdY;
//                Logs.e("========================================================================");
//                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - event.getRawY() : " + event.getRawY());
//
//                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - mScreenHeight : " + mScreenHeight);
//                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - startViewHeight : " + startViewHeight);
//                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - moveY : " + moveY);
//
//                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - startViewHeight + moveY : " + (startViewHeight + (int)moveY));
//                Logs.e("========================================================================");

                if(moveY == 0 || (startViewHeight + (int)moveY) <= 0){
                    return false;
                }


                //확장하려고 하지만 이미 모두 확장했당.
                if(moveY > 0 && mScreenHeight == startViewHeight){
                    return false;
                }

                isMove = true;
                //Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - isMove : " + isMove);
                ViewGroup.LayoutParams params = mLayoutBody.getLayoutParams();
                params.height = startViewHeight + (int)moveY;
                mLayoutBody.setLayoutParams(params);
                mLayoutBody.requestLayout();

                break;

            case MotionEvent.ACTION_UP:
                //Logs.e("dispatchTouchEvent - MotionEvent.ACTION_UP - y : " + event.getRawY());
                //Logs.e("dispatchTouchEvent - moveY : " + moveY);

                if(isMove && moveY != 0){
                    if(moveY > 0){ //확장
                        if(moveY > 200){
                            changeMaxViewHeight();
                        }else{
                            changeOriginViewHeight();
                        }
                    }else if(moveY < 0){ //복귀
                        //Logs.e("dispatchTouchEvent - startViewHeight : " + startViewHeight);
                        //Logs.e("dispatchTouchEvent - mOriginViewHeight : " + mOriginViewHeight);
                        if(startViewHeight == mOriginViewHeight){
                            setSizeSmall();
                        }else{
                            changeOriginViewHeight();
                        }

                    }
                }
                isMove = false;
                break;

            default:
                return false;
        }
        return false;
    }

    private void changeMaxViewHeight(){
        if(mLayoutGuard.getHeight() > 10){
            return;
        }

        if(mRelayArray.size() == 1){
            changeOriginViewHeight();
            return;
        }

        Logs.e("changeMaxViewHeight");
        int listRowHeight = (int)getContext().getResources().getDimension(R.dimen.relay_list_item_height);
        //리스트를 제외한 뷰들의 높이
        float extraSize = getContext().getResources().getDimension(R.dimen.relay_body_height) - listRowHeight;


        int maxViewHeight = (int)(extraSize + (mRelayArray.size()* listRowHeight));
        int allowViewHeight = mScreenHeight - (int)Utils.dpToPixel(getContext(),48);//48은 리스트가 많을때 최대로 늘렸을때 바닥에 남길 여분공간이다.

        if(maxViewHeight >= allowViewHeight){
            maxViewHeight =  allowViewHeight;
        }

        //백그라운드 가이드 레이아웃은 화면 전체 크기를 넘겨준다.
        showAlphaGuardView(mScreenHeight);

        mMain2RelayAdapter.setNowOriginSize(false);
        AniUtils.changeViewHeightSizeAnimation(mLayoutBody,maxViewHeight,250);
        mListView.setVisibility(VISIBLE);
    }

    public void changeOriginViewHeight(int aniTime){
        showAlphaGuardView(0);
        mMain2RelayAdapter.setNowOriginSize(true);
        //AniUtils.changeViewSizeAnimation(mLayoutBody,mOriginViewHeight  - (int)Utils.dpToPixel(getContext(),5));
        AniUtils.changeViewHeightSizeAnimation(mLayoutBody,mOriginViewHeight,aniTime);
        mListView.setVisibility(VISIBLE);
        mIvOpen.setVisibility(INVISIBLE);

    }

    public void changeOriginViewHeight(){
        Logs.e("getOriginViewHeight");
        changeOriginViewHeight(250);
    }

    /**
     * 이어하기 레이어가 2개이상 보일 경우 백그라운드에 딤처리된 뷰가 나타난다.
     * 보이는 리스트가 1개일때는 사라진다.
     *
     * @param height
     */
    private void showAlphaGuardView(int height){

        if(height > 0){
            ViewGroup.LayoutParams params = mLayoutGuard.getLayoutParams();
            params.height = height;
            mLayoutGuard.setLayoutParams(params);
            mLayoutGuard.requestLayout();

            mLayoutGuard.animate()
                    .alpha(0.5f)
                    .setDuration(ANI_TIME_SIZE_CHANGE)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLayoutGuard.setAlpha(0.5f);
                        }
                    } )
                    .start();
        }else{
            mLayoutGuard.animate()
                    .alpha(0f)
                    .setDuration(ANI_TIME_SIZE_CHANGE)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            mLayoutGuard.setAlpha(0f);
                            ViewGroup.LayoutParams params = mLayoutGuard.getLayoutParams();
                            params.height = 0;
                            mLayoutGuard.setLayoutParams(params);
                            mLayoutGuard.requestLayout();
                        }
                    } )
                    .start();
        }
    }

    public void setOnItemCountChangeListener(OnItemCountChangeListener listener){
        mOnItemCountChangeListener = listener;
    }

    public void setTabLayout(CustomTabLayout layout){
        mCustomTabLayout = layout;
    }

    private void showProgressDialog(){
        ((BaseActivity)mContext).showProgressDialog();
    }

    private void dismissProgressDialog(){
        ((BaseActivity)mContext).dismissProgressDialog();
    }

    private boolean checkErrorHttp(String ret){
        return ((BaseActivity)mContext).onCheckHttpError(ret,false);
    }

    /**
     * 이어하기 클릭
     */
    private void requestSelectRelay(final int index){
        Main2RelayInfo relayInfo = mRelayArray.get(index);

        String DSCT_CD = relayInfo.getDSCT_CD();
        if (TextUtils.isEmpty(DSCT_CD))
            return;

        String url="";
        String params="";

        Logs.e("requestSelectRelay - DSCT_CD : " + DSCT_CD);

        switch (DSCT_CD){
            case "310"://보통예금 이어하기
                goRelay_310(relayInfo.getACCO_IDNO());
                return;
            case "320"://신용대출
            case "321"://마이너스통장
            case "322"://소액마이너스통장
            case "323"://표준사잇돌2
            case "324"://대출가조회 - 중금리신용대출
            case "325"://대출가조회 - 마이너스통장
            case "326"://프리미엄중금리신용대출
            case "327"://대출가조회 - 프리미엄중금리신용대출
            case "328"://비상금대출
                goRelay_320328(relayInfo);
                return;
            case "311": {//커플통장 이체요청
                url = WasServiceUrl.INQ0040100.getServiceUrl();
                break;
            }
            case "312": {//커플통장 초대요청
                url = WasServiceUrl.UNT0470501.getServiceUrl();
                params = "SHRN_DMND_KEY_VAL=" + relayInfo.getCANO();
                break;
            }
            case "313": {//가족카드 이어하기
                switch (relayInfo.getFAM_CARD_PRGS_STEP_CD()) {
                    case "01":
                        url = WasServiceUrl.CKC0110200.getServiceUrl();
                        params = "DMND_SRNO=" + relayInfo.getCANO();
                        break;
                    case "02":
                        url = WasServiceUrl.CKC0130300.getServiceUrl();
                        params = "DMND_SRNO=" + relayInfo.getCANO();
                        break;
                    case "03":
                        url = WasServiceUrl.CKC0120100.getServiceUrl();
                        break;
                    case "04":
                        url = WasServiceUrl.CKC0140100.getServiceUrl();
                        break;
                    case "05":
                        url = WasServiceUrl.CKC0140300.getServiceUrl();
                        params = "DMND_SRNO=" + relayInfo.getCANO();
                        break;
                }
                break;
            }
        }
        if(!TextUtils.isEmpty(url)){
            Logs.d("이어하기 클릭 ==================================");
            Logs.d("url : " + url);
            Intent intent = new Intent(mContext, WebMainActivity.class);
            intent.putExtra(Const.INTENT_MAINWEB_URL, url);
            if(!TextUtils.isEmpty(params)){
                Logs.d("params : " + params);
                intent.putExtra(Const.INTENT_PARAM, params);
            }
            Logs.d("이어하기 종료 ==================================");
            mContext.startActivity(intent);
        }
    }

    /**
     * 보통예금이어하기
     */
    private void goRelay_310(final String PROP_NO) {

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0010300A04.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if(checkErrorHttp(ret)) return;

                try {
                    final JSONObject object = new JSONObject(ret);

                    String SCRN_ID = object.optString("SCRN_ID");
                    String SBK_PROP_STEP_CD = object.optString("SBK_PROP_STEP_CD");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = SaidaUrl.getBaseWebUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                    String param = "";
                    if (TextUtils.isEmpty(SBK_PROP_STEP_CD) && !TextUtils.isEmpty(PROP_NO)) {
                        param = "PROP_NO=" + PROP_NO;
                    } else if (!TextUtils.isEmpty(SBK_PROP_STEP_CD) && !TextUtils.isEmpty(PROP_NO)) {
                        param = "SBK_PROP_STEP_CD=" + SBK_PROP_STEP_CD + "&PROP_NO=" + PROP_NO;
                    }
                    intent.putExtra(Const.INTENT_PARAM, param);
                    mContext.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 신용대출 이어하기
     */
    private void goRelay_320328(Main2RelayInfo relayInfo) {
        String DSCT_CD = relayInfo.getDSCT_CD();
        String CANO = relayInfo.getCANO();
        String PROP_STEP_CD = relayInfo.getPROP_STEP_CD();
        if (TextUtils.isEmpty(CANO) || TextUtils.isEmpty(PROP_STEP_CD))
            return;

        Map param = new HashMap();
        param.put("PROP_NO", CANO);
        param.put("LOAN_LNKN_PRGS_STEP_DVCD", PROP_STEP_CD);

        String url = "";
        if (Const.ACCOUNT_GO_ON_LOAN.equalsIgnoreCase(DSCT_CD)) {//중금리신용대출
            url = WasServiceUrl.MAI0010300A01.getServiceUrl();

        } else if (Const.ACCOUNT_GO_ON_MINUS.equalsIgnoreCase(DSCT_CD)) {//마이너스대출
            url = WasServiceUrl.MAI0010300A02.getServiceUrl();

        } else if (Const.ACCOUNT_GO_ON_SMALL_MINUS.equalsIgnoreCase(DSCT_CD)) {//소액마이너스대출
            url = WasServiceUrl.MAI0010300A03.getServiceUrl();
        } else if (Const.ACCOUNT_GO_ON_SAIDOL.equalsIgnoreCase(DSCT_CD)){ //표준사잇돌2
            url = WasServiceUrl.MAI0010300A07.getServiceUrl();
        } else if (Const.ACCOUNT_LOAN_PRICE_CHECK.equalsIgnoreCase(DSCT_CD)){    //대출가조회 - 중금리신용대출
            url = WasServiceUrl.MAI0010300A08.getServiceUrl();
            param.put("CON_NO", CANO);
        } else if (Const.ACCOUNT_MINUS_PRICE_CHECK.equalsIgnoreCase(DSCT_CD)){    //대출가조회 - 마이너스통장
            url = WasServiceUrl.MAI0010300A09.getServiceUrl();
            param.put("CON_NO", CANO);
        } else if (Const.ACCOUNT_GO_ON_PREM_LOAN.equalsIgnoreCase(DSCT_CD)){    //프리미엄중금리신용대출
            url = WasServiceUrl.MAI0010300A10.getServiceUrl();
        } else if (Const.ACCOUNT_PREM_LOAN_PRICE_CHECK.equalsIgnoreCase(DSCT_CD)){    //대출가조회 - 프리미엄중금리신용대출
            url = WasServiceUrl.MAI0010300A11.getServiceUrl();
            param.put("CON_NO", CANO);
        } else if (Const.ACCOUNT_GO_ON_KEEP_EMER_FUND.equalsIgnoreCase(DSCT_CD)){    //비상금대출
            url = WasServiceUrl.MAI0010300A12.getServiceUrl();
        }

        showProgressDialog();
        HttpUtils.sendHttpTask(url, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if(checkErrorHttp(ret)) return;

                try {
                    final JSONObject object = new JSONObject(ret);


                    String SCRN_ID = object.optString("SCRN_ID");
                    if (TextUtils.isEmpty(SCRN_ID))
                        return;

                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = SaidaUrl.getBaseWebUrl() + "/" + SCRN_ID + ".act";
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    mContext.startActivity(intent);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }



    /**
     * 이어하기 삭제
     */
    private void requestDelRelay(final int index) {
        MLog.d();
        Main2RelayInfo relayInfo = mRelayArray.get(index);


        Map param = new HashMap();

        String DSCT_CD = relayInfo.getDSCT_CD();
        Logs.e("DSCT_CD : " + DSCT_CD);
        if (TextUtils.isEmpty(DSCT_CD))
            return;

        String url = "";

        switch (DSCT_CD){
            case "310"://보통예금 이어하기
                url = WasServiceUrl.MAI0010400A01.getServiceUrl();
                String ACCO_IDNO = relayInfo.getACCO_IDNO();
                if (TextUtils.isEmpty(ACCO_IDNO))
                    return;

                param.put("PROP_NO", ACCO_IDNO);
                break;
            case "320"://신용대출
            case "321"://마이너스통장
            case "322"://소액마이너스통장
            case "323"://표준사잇돌2
            case "326"://프리미엄중금리신용대출
            case "328"://비상금대출
            {
                url = WasServiceUrl.MAI0010400A02.getServiceUrl();
                String CANO = relayInfo.getCANO();
                if (TextUtils.isEmpty(CANO))
                    return;

                param.put("PROP_NO", CANO);
                break;
            }
            case "324"://대출가조회 - 중금리신용대출
            case "325"://대출가조회 - 마이너스통장
            case "327"://대출가조회 - 프리미엄중금리신용대출
                {
                url = WasServiceUrl.MAI0010400A03.getServiceUrl();
                String CANO = relayInfo.getCANO();
                if (TextUtils.isEmpty(CANO))
                    return;

                param.put("CON_NO", CANO);
                break;
            }
            case "311"://커플통장 이체요청 삭제
                url = WasServiceUrl.INQ0050100A02.getServiceUrl();
                param.put("TRTM_DVCD", "02");
                param.put("SHRN_IOMN_ACNO", relayInfo.getACNO());
                param.put("SRNO", relayInfo.getCANO());
                break;
            case "312"://커플통장 초대요청 삭제
                url = WasServiceUrl.MAI0010600A02.getServiceUrl();
                param.put("TRTM_DVCD", "02");
                param.put("SHRN_DMND_KEY_VAL", relayInfo.getCANO());
                break;
            case "313"://가족카드 이어하기 삭제
                switch (relayInfo.getFAM_CARD_PRGS_STEP_CD()){
                    case "01"://신청요청 - 약관동의
                    case "02"://신청요청 - 증명서제출
                        url = WasServiceUrl.CKC0130100A01.getServiceUrl();
                        param.put("TRTM_DVCD", "03");
                        param.put("DMND_SRNO", relayInfo.getCANO());
                        param.put("FAM_CARD_PRGS_STEP_CD", "04");
                        break;
                    case "03"://신청요청
                        url = WasServiceUrl.CKC0110300A01.getServiceUrl();
                        param.put("TRTM_DVCD", "02");
                        param.put("DMND_SRNO", relayInfo.getCANO());
                        break;
                    case "04"://심사대기
                        if(relayInfo.getSHRN_ACCO_YN().equals("Y")){
                            url = WasServiceUrl.CKC0110300A01.getServiceUrl();
                            param.put("TRTM_DVCD", "02");
                            param.put("DMND_SRNO", relayInfo.getCANO());
                        }else{
                            url = WasServiceUrl.CKC0130100A01.getServiceUrl();
                            param.put("TRTM_DVCD", "03");
                            param.put("DMND_SRNO", relayInfo.getCANO());
                            param.put("FAM_CARD_PRGS_STEP_CD", "04");
                        }
                        break;
                    case "05"://심사보완
                        url = WasServiceUrl.CKC0130100A01.getServiceUrl();
                        param.put("TRTM_DVCD", "03");
                        param.put("DMND_SRNO", relayInfo.getCANO());
                        param.put("FAM_CARD_PRGS_STEP_CD", "04");
                        break;
                }
                break;

        }

        ((BaseActivity)mContext).showProgressDialog();
        HttpUtils.sendHttpTask(url, param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity)mContext).dismissProgressDialog();
                if(((BaseActivity)mContext).onCheckHttpError(ret,false))   return;



                mRelayArray.remove(index);
                mMain2RelayAdapter.setRelayArray(mRelayArray);
                mTvCount.setText(String.valueOf(mRelayArray.size()));
                if(mRelayArray.size() ==0){
                    slidingUp(250);

                }else if(mRelayArray.size() == 1 ){
                    changeOriginViewHeight();
                }else{
                    changeMaxViewHeight();
                }

                if(mOnItemCountChangeListener != null)
                    mOnItemCountChangeListener.onItemCountChange(mRelayArray.size());

                mLayoutMoveBar.setVisibility(VISIBLE);

            }
        });
    }
}
