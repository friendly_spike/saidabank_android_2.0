package com.sbi.saidabank.activity.transaction.itransfer.multi;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferMultiActionListener;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.dialog.AlertInfoDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectSenderAccountDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;

import java.util.ArrayList;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ViewHolderHeader extends RecyclerView.ViewHolder{

    private Context mContext;
    private RelativeLayout mLayoutBody;
    private ImageView mIvBankIcon;
    private TextView  mTvAccountName;
    private TextView  mBalanceAmount;
    private ImageView mTvCoupeIcon;

    private TextView  mTvAccountCnt;
    private TextView  mTvTotalAmount;

    private OnITransferMultiActionListener mListener;

    public static ViewHolderHeader newInstance(ViewGroup parent, OnITransferMultiActionListener listener) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_itransfer_multi_withdraw_account, parent, false);
        return new ViewHolderHeader(parent.getContext(),itemView,listener);
    }

    public ViewHolderHeader(Context context, @NonNull View itemView, OnITransferMultiActionListener listener) {
        super(itemView);

        mContext = context;
        mListener = listener;

        mLayoutBody = itemView.findViewById(R.id.layout_body);
        int radius = (int) Utils.dpToPixel(mContext,13);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#eaeaea",new int[]{radius,radius,radius,radius}));


        mIvBankIcon = itemView.findViewById(R.id.iv_bank_icon);
        mTvAccountName = itemView.findViewById(R.id.tv_account_name);
        mTvCoupeIcon = itemView.findViewById(R.id.iv_couple_icon);
        mBalanceAmount = itemView.findViewById(R.id.tv_balance_amount);

        mTvAccountCnt = itemView.findViewById(R.id.tv_account_cnt);
        mTvTotalAmount = itemView.findViewById(R.id.tv_total_amount);
    }

    public void onBind() {
        mLayoutBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.showSelectSendAccountDialog();
            }
        });

        String amount = Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getBalanceAmount());
        mBalanceAmount.setText("출금가능 " + amount + " 원");

        dispWithDrawAccount();

        mTvAccountCnt.setText(String.valueOf(ITransferDataMgr.getInstance().getRemitteInfoArraySize()));

        Double totalSum = 0d;
        //총 입금 금액을 표시한다.
        for(int i=0;i<ITransferDataMgr.getInstance().getRemitteInfoArraySize();i++){
            ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(i);
            String inputAmount = remitteeInfo.getTRN_AMT();
            if(TextUtils.isEmpty(remitteeInfo.getEROR_MSG_CNTN()) && !TextUtils.isEmpty(inputAmount)){
                totalSum += Double.parseDouble(inputAmount);
            }
        }
        String transTotlaMoney = Utils.moneyFormatToWon(totalSum);
        mTvTotalAmount.setText(transTotlaMoney);
    }

    private void dispWithDrawAccount(){
        String bankCd = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        String detailBankCd = ITransferDataMgr.getInstance().getWTCH_DTLS_BANK_CD();
        String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();

        Uri iconUri = ImgUtils.getIconUri(mContext,bankCd,detailBankCd);
        if(iconUri != null)
            mIvBankIcon.setImageURI(iconUri);
        else
            mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);

        ITransferDataMgr.getInstance().setWTCH_BANK_NM(Const.BANK_FULL_NAME);

        //사이다뱅크계좌
        String accountName = "";
        ArrayList<MyAccountInfo> accountInfoArrayList =  LoginUserInfo.getInstance().getMyAccountInfoArrayList();
        for(int i=0;i<accountInfoArrayList.size();i++){
            MyAccountInfo accountInfo = accountInfoArrayList.get(i);
            if(accountInfo.getACNO().equalsIgnoreCase(accountNo)){
                String ACCO_ALS = accountInfo.getACCO_ALS();
                if (TextUtils.isEmpty(ACCO_ALS)) {
                    ACCO_ALS = accountInfo.getPROD_NM();
                }
                accountName = ACCO_ALS;
                ITransferDataMgr.getInstance().setWTCH_ACNO_NM(accountName);

                //커플통장여부
                if(accountInfo.getSHRN_ACCO_YN().equalsIgnoreCase("Y")){
                    mTvCoupeIcon.setVisibility(View.VISIBLE);
                }else{
                    mTvCoupeIcon.setVisibility(View.GONE);
                }

                break;
            }
        }

        String shortAccNo = accountNo.substring(accountNo.length()-4,accountNo.length());
        mTvAccountName.setText(accountName + "(" + shortAccNo +")");

    }
}
