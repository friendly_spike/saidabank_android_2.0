package com.sbi.saidabank.activity.certification;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;

import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.CertItemInfo;

public class CertDetailActivity extends BaseActivity {
    private CertItemInfo mCertItemInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cert_detail);
        getExtra();
        initUX();
    }

    private void getExtra() {
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            mCertItemInfo = (CertItemInfo) bundle.getParcelable(Const.INTENT_CERT_DATA);
        }
    }

    private void initUX() {
        if (mCertItemInfo == null)
            return;

        TextView textName = (TextView) findViewById(R.id.textview_name_cert_detail);
        String name = mCertItemInfo.getName();
        if (!TextUtils.isEmpty(name)) {
            int index = name.indexOf("(");
            if (index >= 0) {
                name = name.substring(0, index);
            }
            textName.setText(name);
        }

        TextView textType = (TextView) findViewById(R.id.textview_type_cert_detail);
        String type = mCertItemInfo.getType();
        if (!TextUtils.isEmpty(type)) {
            textType.setText(type);
        }

        TextView textVersion = (TextView) findViewById(R.id.textview_version_cert_detail);
        String version = mCertItemInfo.getVer();
        if (!TextUtils.isEmpty(version)) {
            textVersion.setText(version);
        }

        TextView textNo = (TextView) findViewById(R.id.textview_serial_cert_detail);
        String no = mCertItemInfo.getNo();
        if (!TextUtils.isEmpty(no)) {
            textNo.setText(no);
        }

        TextView textAuth = (TextView) findViewById(R.id.textview_auth_cert_detail);
        String auth = mCertItemInfo.getAuth();
        if (!TextUtils.isEmpty(auth)) {
            textAuth.setText(auth);
        }

        TextView textAgency = (TextView) findViewById(R.id.textview_agency_cert_detail);
        String agency = mCertItemInfo.getIssueAgency();
        if (!TextUtils.isEmpty(agency)) {
            textAgency.setText(agency);
        }

        TextView textState = (TextView) findViewById(R.id.textview_state_cert_detail);
        String state = mCertItemInfo.getState();
        if (!TextUtils.isEmpty(state)) {
            textState.setText(state);
        }

        TextView textIssueDate = (TextView) findViewById(R.id.textview_issue_date_cert_detail);
        String issueDate = mCertItemInfo.getIssueDate();
        if (!TextUtils.isEmpty(issueDate)) {
            textIssueDate.setText(issueDate);
        }

        TextView textExpireDate = (TextView) findViewById(R.id.textview_expire_date_cert_detail);
        String expireDate = mCertItemInfo.getExpireDate();
        if (!TextUtils.isEmpty(expireDate)) {
            textExpireDate.setText(expireDate);
        }

        TextView textSignAlog = (TextView) findViewById(R.id.textview_sign_alog_cert_detail);
        String signAlog = mCertItemInfo.getSignAlog();
        if (!TextUtils.isEmpty(signAlog)) {
            textSignAlog.setText(signAlog);
        }

        TextView textIssuer = (TextView) findViewById(R.id.textview_issuer_cert_detail);
        String issuer = mCertItemInfo.getIssuer();
        if (!TextUtils.isEmpty(issuer)) {
            textIssuer.setText(issuer);
        }

        TextView textCertCN = (TextView) findViewById(R.id.textview_cert_cn_cert_detail);
        String certCN = mCertItemInfo.getCertCN();
        if (!TextUtils.isEmpty(certCN)) {
            textCertCN.setText(certCN);
        }

        Button btnCitform = (Button) findViewById(R.id.btn_confirm_cert_detail);
        btnCitform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
