package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.core.app.ActivityCompat;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.test.SolutionTestActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.motp.MOTPManager;

/**
 * SplashActivity : Splash 화면
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class SplashActivity extends BaseActivity {

    CommonUserInfo commonUserInfo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);

        if (BuildConfig.DEBUG) {
            findViewById(R.id.splash_title).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(SplashActivity.this, SolutionTestActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
        }

        findViewById(R.id.btn_splash_next).setOnClickListener(onNextClickListener);
        findViewById(R.id.ll_already_reg_user).setOnClickListener(onNextClickListener);
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(SplashActivity.this);
        System.exit(0);
    }

    /**
     * 이용약관 화면으로 이동
     */
    public void goNextActivity() {
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        if (DataUtil.isNotNull(loginUserInfo)) {
            if (DataUtil.isNotNull(loginUserInfo.getSMPH_DEVICE_ID())) {
                MOTPManager.getInstance(getApplication()).getSerialNum(new MOTPManager.MOTPEventListener() {
                    @Override
                    public void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4) {
                        LoginUserInfo.getInstance().setMOTPSerialNumber(!"0000".equals(resultCode) ? Const.EMPTY : reqData1);
                    }
                });
            } else {
                LoginUserInfo.getInstance().setMOTPSerialNumber(Const.EMPTY);
            }
            Intent intent = new Intent(SplashActivity.this, PolicyActivity.class);
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivity(intent);
        }
    }

    private View.OnClickListener onNextClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View v) {
            goNextActivity();
        }
    };
}
