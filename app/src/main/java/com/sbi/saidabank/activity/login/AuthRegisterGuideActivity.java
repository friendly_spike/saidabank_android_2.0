package com.sbi.saidabank.activity.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;

import android.view.View;
import android.widget.LinearLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternRegActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassUtils;

/**
 * Saidabanking_android
 * Class: AuthRegisterGuideActivity
 * Created by 950546
 * Date: 2018-10-18
 * Time: 오전 10:40
 * Description: 인증수단 선택
 */

public class AuthRegisterGuideActivity extends BaseActivity {


    //필요권한 - 전화접근권한
    private String[] perList = new String[]{
            Manifest.permission.READ_PHONE_STATE,
    };

    private CommonUserInfo mCommonUserInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_authregister);

        LinearLayout layout_bio = findViewById(R.id.ll_bio);

        if (StonePassUtils.isUsableFingerprint(this) != 0) {
            layout_bio.setVisibility(View.GONE);
            //Logs.showToast(this, "지문인증 불가능 단말입니다.\n팝업 처리 예정");
        }
        else{
            layout_bio.setVisibility(View.VISIBLE);
        }

        getExtra();

        findViewById(R.id.btn_confirm).setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if(PermissionUtils.checkPermission(AuthRegisterGuideActivity.this,perList,Const.REQUEST_PERMISSION_READ_PHONE_STATE)){
                    showRegPattern();
                }
            }
        });

//        if (mCommonUserInfo.getEntryPoint() == EntryPoint.SERVICE_JOIN)
//            AppsFlyerManager.getInstance(this).sendAppsFlyerTrackEvent(Const.APPSFLYER_MEMBER+"5", "");
    }

    /**
     * Extras 값 획득
     */
    private void getExtra() {
        mCommonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_READ_PHONE_STATE :
            {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showRegPattern();
                } else {
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.e("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_phone_state_allow), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(AuthRegisterGuideActivity.this);
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                        //Utils.finishAffinity(AuthRegisterGuideActivity.this);
                                    }
                                });
                    }

                }
            }
            break;

            default:
                break;
        }
    }

    /**
     * 패턴등록 화면으로 이동
     */
    private void showRegPattern() {
        Intent intent = new Intent(this, PatternRegActivity.class);
        intent.putExtra(Const.INTENT_COMMON_INFO, mCommonUserInfo);
        startActivity(intent);
        finish();
    }
}
