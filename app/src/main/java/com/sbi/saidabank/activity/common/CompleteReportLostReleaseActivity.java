package com.sbi.saidabank.activity.common;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;

/**
 * siadabank_android
 * Class: CompleteReportLostReleaseActivity
 * Created by 950546.
 * Date: 2019-01-30
 * Time: 오전 9:46
 * Description: 분실신고 해제완료 안내화면
 */
public class CompleteReportLostReleaseActivity extends BaseActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_reportlost_release);
        findViewById(R.id.btn_confirm).setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, PatternAuthActivity.class);
        CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        if (Prefer.getFidoRegStatus(this))
            commonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
        else
            commonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivity(intent);
        finish();
    }
}
