package com.sbi.saidabank.activity.main2.homecouple;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2CoupleExpenceAmount;
import com.sbi.saidabank.define.datatype.main2.Main2CoupleExpenceCount;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import java.util.ArrayList;

/**
 * 커플 가입한지 2달이 지나 과거 2달치의  통계정보가 있을때 레이아웃
 * 커플가입 9월. 현재 11월 이후
 */
public class Statistics3Layout extends RelativeLayout {

    private RelativeLayout mLayoutTooltipMsg;
    private ImageView mIvToolTip;

    public Statistics3Layout(Context context) {
        super(context);
        initUX(context);
    }

    public Statistics3Layout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_couple_statistics3, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        // 통계타이틀 - 웹 통계화면으로 이동한다.
        LinearLayout statisticsTitle = layout.findViewById(R.id.layout_statistics_title);
        statisticsTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WebMainActivity.class);
                String url = WasServiceUrl.UNT0484100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                getContext().startActivity(intent);
            }
        });

        // 지출 코멘트
        TextView mTvExpenceComent = layout.findViewById(R.id.tv_expence_comment);

        // 전월사용금액*****
        String lastMonthAmount = Main2DataInfo.getInstance().getPVMN_USE_AMT();
        if (TextUtils.isEmpty(lastMonthAmount))
            lastMonthAmount = Const.ZERO;
        String monthBeforeLastAmount = Main2DataInfo.getInstance().getPVMN2_USE_AMT();
        if (TextUtils.isEmpty(monthBeforeLastAmount))
            monthBeforeLastAmount = Const.ZERO;

        int intLastMonthAmount = Integer.parseInt(lastMonthAmount);
        int intMonthBeforeLastAmount = Integer.parseInt(monthBeforeLastAmount);

        // 전전월문자****
        String monthBeforeLastText = Main2DataInfo.getInstance().getPVMN2_BSMN();
        if (DataUtil.isNotNull(monthBeforeLastText)) {
            monthBeforeLastText = monthBeforeLastText.equals("10") ? monthBeforeLastText : monthBeforeLastText.replace(Const.ZERO, Const.EMPTY);
        }

        if (intLastMonthAmount == intMonthBeforeLastAmount) {
            mTvExpenceComent.setText(monthBeforeLastText + "월과 동일하게 지출했어요!");
            mTvExpenceComent.setTextColor(Color.parseColor("#000000"));
        } else if (intLastMonthAmount > intMonthBeforeLastAmount) {
            mTvExpenceComent.setText(monthBeforeLastText + "월보다 지출이 늘었어요!");
            mTvExpenceComent.setTextColor(Color.parseColor("#ff6450"));
        } else {
            mTvExpenceComent.setText(monthBeforeLastText + "월보다 지출이 줄었어요!");
            mTvExpenceComent.setTextColor(Color.parseColor("#009beb"));
        }

        // 그래프*******
        RelativeLayout layoutMonthBeforeLastGraph = layout.findViewById(R.id.layout_month_before_last_graph);
        RelativeLayout layoutMonthLastGraph = layout.findViewById(R.id.layout_month_last_graph);

        // 전전월 처리
        displayGraphArea(-2, layoutMonthBeforeLastGraph, intMonthBeforeLastAmount, intLastMonthAmount);

        // 전월 처리
        displayGraphArea(-1, layoutMonthLastGraph, intMonthBeforeLastAmount, intLastMonthAmount);

        // 툴팁*******
        mLayoutTooltipMsg = (RelativeLayout) layout.findViewById(R.id.layout_tooltip_msg);
        int radius = (int) Utils.dpToPixel(getContext(), 3);
        int line = (int) Utils.dpToPixel(getContext(), 1);
        mLayoutTooltipMsg.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF", new int[]{radius, radius, radius, radius}, line, "#000000"));
        mLayoutTooltipMsg.setVisibility(GONE);

        mIvToolTip = layout.findViewById(R.id.iv_tooltip);
        mIvToolTip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLayoutTooltipMsg.getVisibility() == VISIBLE) {
                    mLayoutTooltipMsg.setVisibility(GONE);
                    mIvToolTip.setImageResource(R.drawable.btn_tooltip_off);
                } else {
                    mLayoutTooltipMsg.setVisibility(VISIBLE);
                    mIvToolTip.setImageResource(R.drawable.btn_tooltip_on);
                }
            }
        });

        ImageView ivTooltipClose = layout.findViewById(R.id.iv_tooltip_close);
        ivTooltipClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutTooltipMsg.setVisibility(GONE);
                mIvToolTip.setImageResource(R.drawable.btn_tooltip_off);
            }
        });

        // 지출내역******
        // 지지난달
        LinearLayout layoutMonthBeforeLast = layout.findViewById(R.id.layout_month_before_last);
        displayExpenceInfoArea(-2, layoutMonthBeforeLast);
        // 지난달
        LinearLayout layoutMonthLast = layout.findViewById(R.id.layout_month_last);
        displayExpenceInfoArea(-1, layoutMonthLast);
    }

    /**
     * 그래프 영역을 그려준다.
     *
     * @param month                    전월/전전월
     * @param view                     그래프 레이아웃
     * @param intMonthBeforeLastAmount 전전월 금액
     * @param intLastMonthAmount       전월 금액
     */
    private void displayGraphArea(int month, View view, int intMonthBeforeLastAmount, int intLastMonthAmount) {
        LinearLayout layoutBubble = view.findViewById(R.id.layout_bubble);
        LinearLayout layoutBubbleInner = view.findViewById(R.id.layout_bubble_inner);
        ImageView ivBubbleIcon = view.findViewById(R.id.iv_bubble_icon);
        TextView tvBubbleAmount = view.findViewById(R.id.tv_bubble_amount);
        ImageView tvBubbleTailIcon = view.findViewById(R.id.iv_bubble_tail_icon);


        TextView tvGraphExpenceAmonut = view.findViewById(R.id.tv_graph_expence_amount);
        TextView tvExpenceMonth = view.findViewById(R.id.tv_expence_month);
        View vGraphRect = view.findViewById(R.id.v_graph_expence_rect);
        //전전월 그래프 표시
        if (month == -2) {

            //말풍선 - 전전월은 풍선금액을 보이지 않는다.
            layoutBubble.setVisibility(INVISIBLE);

            //그래프 금액
            tvGraphExpenceAmonut.setText(Utils.moneyFormatToWon((double) intMonthBeforeLastAmount) + " 원");
            tvGraphExpenceAmonut.setTextColor(Color.parseColor("#888888"));

            //전월문자***
            String monthText = Main2DataInfo.getInstance().getPVMN2_BSMN();
            if (!TextUtils.isEmpty(monthText))
                monthText = String.valueOf(Integer.parseInt(monthText));

            tvExpenceMonth.setText(monthText + "월");

            int radius = (int) Utils.dpToPixel(getContext(), 2);
            if (intMonthBeforeLastAmount == 0)
                vGraphRect.setBackground(GraphicUtils.getRoundCornerDrawable("#c3c3c3", new int[]{radius, radius, radius, radius}));//회색
            else
                vGraphRect.setBackground(GraphicUtils.getRoundCornerDrawable("#4864a8", new int[]{radius, radius, radius, radius}));

            float graphRectHeight = getResources().getDimension(R.dimen.main2_home_couple_graph_rect_height);
            if (intMonthBeforeLastAmount == 0) {
                graphRectHeight = graphRectHeight * 0.1f;
            } else if (intMonthBeforeLastAmount == intLastMonthAmount) {
                graphRectHeight = graphRectHeight * 0.7f;
            } else if (intMonthBeforeLastAmount < intLastMonthAmount) {
                graphRectHeight = graphRectHeight * intMonthBeforeLastAmount / intLastMonthAmount;
            }
            ViewGroup.LayoutParams params = vGraphRect.getLayoutParams();
            params.height = (int) graphRectHeight;
            vGraphRect.setLayoutParams(params);

        } else {

            int radius = (int) Utils.dpToPixel(getContext(), 2);
            //말풍선
            if (intMonthBeforeLastAmount == intLastMonthAmount) {
                //동일하게 사용.
                ivBubbleIcon.setVisibility(GONE);
                tvBubbleAmount.setText("동일");
                layoutBubbleInner.setBackground(GraphicUtils.getRoundCornerDrawable("#8993aa", new int[]{radius, radius, radius, radius}));
                tvBubbleTailIcon.setImageResource(R.drawable.ico_bubble_tail_gr);
            } else if (intMonthBeforeLastAmount > intLastMonthAmount) {
                //전전월보다 적게 사용
                ivBubbleIcon.setImageResource(R.drawable.ico_arr_down);

                tvBubbleAmount.setText(Utils.moneyFormatToWon((double) intMonthBeforeLastAmount - intLastMonthAmount) + " 원");
                layoutBubbleInner.setBackground(GraphicUtils.getRoundCornerDrawable("#009beb", new int[]{radius, radius, radius, radius}));
                tvBubbleTailIcon.setImageResource(R.drawable.ico_bubble_tail_bl);
            } else {
                ivBubbleIcon.setImageResource(R.drawable.ico_arr_up);
                tvBubbleAmount.setText(Utils.moneyFormatToWon((double) intLastMonthAmount - intMonthBeforeLastAmount) + " 원");
                layoutBubbleInner.setBackground(GraphicUtils.getRoundCornerDrawable("#ff6450", new int[]{radius, radius, radius, radius}));
                tvBubbleTailIcon.setImageResource(R.drawable.ico_bubble_tail_rd);
            }

            //그래프 금액
            tvGraphExpenceAmonut.setText(Utils.moneyFormatToWon((double) intLastMonthAmount) + " 원");

            //전월문자***
            String monthText = Main2DataInfo.getInstance().getPVMN_BSMN();
            if (!TextUtils.isEmpty(monthText))
                monthText = String.valueOf(Integer.parseInt(monthText));

            tvExpenceMonth.setText(monthText + "월");

            //그래프 그리기
            //int radius = (int)Utils.dpToPixel(getContext(),2);
            vGraphRect.setBackground(GraphicUtils.getRoundCornerDrawable("#00ebff", new int[]{radius, radius, radius, radius}));

            float graphRectHeight = getResources().getDimension(R.dimen.main2_home_couple_graph_rect_height);
            if (intLastMonthAmount == 0) {
                graphRectHeight = graphRectHeight * 0.1f;
            } else if (intMonthBeforeLastAmount == intLastMonthAmount) {
                graphRectHeight = graphRectHeight * 0.7f;
            } else if (intMonthBeforeLastAmount > intLastMonthAmount) {
                graphRectHeight = graphRectHeight * intLastMonthAmount / intMonthBeforeLastAmount;
            }
            ViewGroup.LayoutParams params = vGraphRect.getLayoutParams();
            params.height = (int) graphRectHeight;
            vGraphRect.setLayoutParams(params);
        }
    }


    /**
     * 지출내역을 출력한다.
     *
     * @param month  -1:전달, -2:전전달
     * @param layout 출력할 레이아웃
     */
    private void displayExpenceInfoArea(int month, View layout) {
        String lastMonth = Utils.getAddMonth(month);

        int radius = (int) Utils.dpToPixel(getContext(), 28);

        //지난달 내용 출력할때 많이썻어요/자주써서어요.. 텍스트 문구 삭제
        if (month == -1) {
            layout.findViewById(R.id.tv_amount_title).setVisibility(INVISIBLE);
            layout.findViewById(R.id.tv_count_title).setVisibility(INVISIBLE);
        }


        RelativeLayout mLayoutAmountIcon = layout.findViewById(R.id.layout_amount_icon);
        //지출금액별
        Main2CoupleExpenceAmount amountInfo = getExpenceAmount(lastMonth);
        if (amountInfo != null) {
            //아이콘 배경 동그랗게 만들자.

            if (month == -2)
                mLayoutAmountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#4864a8", new int[]{radius, radius, radius, radius}));
            else
                mLayoutAmountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#00EBFF", new int[]{radius, radius, radius, radius}));

            //결제 종류별 아이콘 표시
            ImageView iconAmount = layout.findViewById(R.id.iv_amount_icon);
            iconAmount.setImageResource(Statistics1Layout.getExpenceIconResourceId(amountInfo.getSETT_TYCD()));

            //결제 유형명
            TextView tvAmountKind = layout.findViewById(R.id.tv_amount_kind);
            tvAmountKind.setText(amountInfo.getFNTR_DVCD_NM());

            //결제 항목명
            TextView tvAmountProdName = layout.findViewById(R.id.tv_amount_prod_name);
            tvAmountProdName.setText(amountInfo.getSETT_ITEM_NM());

            //결제금액
            TextView tvAmountAmount = layout.findViewById(R.id.tv_amount_amount);
            String amountAmount = amountInfo.getSETT_AMT();
            if (!TextUtils.isEmpty(amountAmount)) {
                amountAmount = Utils.moneyFormatToWon(Double.valueOf(amountAmount));
            }
            tvAmountAmount.setText(amountAmount + " 원");
        } else {
            //회색으로 바꿔준다.
            if (month == -2)
                mLayoutAmountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#c3c3c3", new int[]{radius, radius, radius, radius}));

            //사용이력 없음.
            ImageView iconAmount = layout.findViewById(R.id.iv_amount_icon);
            iconAmount.setImageResource(R.drawable.ico_empty);

            TextView tvAmountKind = layout.findViewById(R.id.tv_amount_kind);
            tvAmountKind.setText("사용이력없음");

            TextView tvAmountProdName = layout.findViewById(R.id.tv_amount_prod_name);
            tvAmountProdName.setVisibility(GONE);

            TextView tvAmountAmount = layout.findViewById(R.id.tv_amount_amount);
            tvAmountAmount.setVisibility(GONE);
        }

        //##########################################################################################
        RelativeLayout mLayoutCountIcon = layout.findViewById(R.id.layout_count_icon);
        //지출건수별
        Main2CoupleExpenceCount countInfo = getExpenceCount(lastMonth);
        if (countInfo != null) {
            //아이콘 배경 동그랗게 만들자.

            if (month == -2)
                mLayoutCountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#4864a8", new int[]{radius, radius, radius, radius}));
            else
                mLayoutCountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#00EBFF", new int[]{radius, radius, radius, radius}));

            //결제 종류별 아이콘 표시
            ImageView iconCount = layout.findViewById(R.id.iv_count_icon);
            iconCount.setImageResource(Statistics1Layout.getExpenceIconResourceId(countInfo.getSETT_TYCD()));

            //결제 유형명
            TextView tvCountKind = layout.findViewById(R.id.tv_count_kind);
            tvCountKind.setText(countInfo.getFNTR_DVCD_NM());

            //결제 항목명
            TextView tvCountProdName = layout.findViewById(R.id.tv_count_prod_name);
            tvCountProdName.setText(countInfo.getSETT_ITEM_NM());

            //결제건수
            TextView tvCountCount = layout.findViewById(R.id.tv_count_count);
            String countCount = countInfo.getSETT_CCNT();
            tvCountCount.setText(countCount + " 회");
        } else {
            //회색으로 바꿔준다.
            if (month == -2)
                mLayoutCountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#c3c3c3", new int[]{radius, radius, radius, radius}));

            //결제 종류별 아이콘 표시
            ImageView iconCount = layout.findViewById(R.id.iv_count_icon);
            iconCount.setImageResource(R.drawable.ico_empty);

            //결제 유형명
            TextView tvCountKind = layout.findViewById(R.id.tv_count_kind);
            tvCountKind.setText("사용이력없음");

            //결제 항목명
            TextView tvCountProdName = layout.findViewById(R.id.tv_count_prod_name);
            tvCountProdName.setVisibility(GONE);

            //결제건수
            TextView tvCountCount = layout.findViewById(R.id.tv_count_count);
            tvCountCount.setVisibility(GONE);
        }
    }

    private Main2CoupleExpenceAmount getExpenceAmount(String lastMonth) {
        ArrayList<Main2CoupleExpenceAmount> arrayList = Main2DataInfo.getInstance().getMain2CoupleExpenceAmountArray();
        if (DataUtil.isNotNull(arrayList)) {

            for (int i = 0; i < arrayList.size(); i++) {
                Main2CoupleExpenceAmount info = arrayList.get(i);
                if (info.getSETT_YM().equals(lastMonth)) {
                    return info;
                }
            }
        }
        return null;
    }

    private Main2CoupleExpenceCount getExpenceCount(String lastMonth) {
        ArrayList<Main2CoupleExpenceCount> arrayList = Main2DataInfo.getInstance().getMain2CoupleExpenceCountArray();
        if (DataUtil.isNotNull(arrayList)) {
            for (int i = 0; i < arrayList.size(); i++) {
                Main2CoupleExpenceCount info = arrayList.get(i);
                if (info.getSETT_YM().equals(lastMonth)) {
                    return info;
                }
            }
        }
        return null;
    }
}
