package com.sbi.saidabank.activity.transaction.itransfer.single;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

public class ITransferBaseLayout extends RelativeLayout {
    public ITransferBaseLayout(Context context) {
        super(context);
    }

    public ITransferBaseLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ITransferBaseLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ITransferBaseLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    //각뷰의 레벨이 같아야 한다.
    public View getSlideView(Class slideClass){
        RelativeLayout rootLayout = (RelativeLayout) (this.getParent());

        if(rootLayout != null) {
            for(int i=0;i<rootLayout.getChildCount();i++){
                View view = rootLayout.getChildAt(i);
                if(slideClass.isInstance(view) ){
                    return view;
                }
            }
        }

        return null;
    }
}
