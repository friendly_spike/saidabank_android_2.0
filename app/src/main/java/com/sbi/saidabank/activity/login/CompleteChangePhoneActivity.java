package com.sbi.saidabank.activity.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.ssenstone.PatternAuthActivity;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;

/**
 * Saidabanking_android
 * <p>
 * Class: CompleteChangePhoneActivity
 * Created by 950485 on 2018. 11. 16..
 * <p>
 * Description:기기변경 완료 화면
 */

public class CompleteChangePhoneActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_complete_change_phone);

        //현재 화면을 제외한 이전까지 진행했던 가입 화면은 모두 제거한다.
        ((SaidaApplication)getApplication()).allActivityFinish(this.getLocalClassName());

        initUX();

        //인트로의 서버선택을 위해 가입한 서버의 인덱스를 저장해둔다.
        if(BuildConfig.DEBUG){
            Prefer.setJoinDebugServerIndex(this, SaidaUrl.serverIndex);
        }
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        Button btnConfirm = (Button) findViewById(R.id.btn_ok_change_phone);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogin();
            }
        });
    }

    private void showLogin() {
        Intent intent = new Intent(CompleteChangePhoneActivity.this, PatternAuthActivity.class);
        CommonUserInfo commonUserInfo = getIntent().getParcelableExtra(Const.INTENT_COMMON_INFO);
        //Prefer.setFidoRegStatus(this, false);
        //Prefer.setFlagFingerPrintChange(this, false);
        if (Prefer.getFidoRegStatus(this)) {
            commonUserInfo.setEntryStart(EntryPoint.LOGIN_BIO);
            commonUserInfo.setEntryPoint(EntryPoint.LOGIN_BIO);
        } else {
            commonUserInfo.setEntryStart(EntryPoint.LOGIN_PATTERN);
            commonUserInfo.setEntryPoint(EntryPoint.LOGIN_PATTERN);
        }
        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
        startActivity(intent);

        finish();
    }
}