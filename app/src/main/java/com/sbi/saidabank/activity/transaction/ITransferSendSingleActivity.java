package com.sbi.saidabank.activity.transaction;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;

import com.fastaccess.datetimepicker.DatePickerFragmentDialog;
import com.fastaccess.datetimepicker.DateTimeBuilder;
import com.fastaccess.datetimepicker.callback.DatePickerCallback;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.itransfer.single.ITransferInputDetailAccountLayout;
import com.sbi.saidabank.activity.transaction.itransfer.single.ITransferInputDetailPhoneLayout;
import com.sbi.saidabank.activity.transaction.itransfer.single.ITransferInputMoneyLayout;
import com.sbi.saidabank.activity.transaction.itransfer.single.ITransferKeyPadLayout;
import com.sbi.saidabank.activity.transaction.itransfer.single.ITransferSelectAccountLayout;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSingleActionListener;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.contacts.ContactsUtil;
import com.sbi.saidabank.common.contacts.GetContactListAsyncTask;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.SlidingDateTimerPickerDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.KeyboardDetectorRelativeLayout;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class ITransferSendSingleActivity extends BaseActivity implements KeyboardDetectorRelativeLayout.IKeyboardChanged,View.OnClickListener, OnITransferSingleActionListener, DatePickerCallback {
    public static final int REQUEST_SELECT_RCV = 1234;


    private ITransferSelectAccountLayout    mLayoutSelectAccount;
    private ITransferKeyPadLayout           mLayoutKeyPad;
    private ITransferInputMoneyLayout       mLayoutInputMoney;
    private ITransferInputDetailAccountLayout mLayoutInputDetailAccountInfo;
    private ITransferInputDetailPhoneLayout mLayoutInputDetailPhoneInfo;

    //++ DatePicker values
    private boolean mIstoday;
    private boolean mIsDelayService;
    private int mCurYear;
    private int mCurMon;
    private int mCurDay;
    private int mInputYear;
    private int mInputMonth;
    private int mInputDay;

    private boolean isPause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itransfer_send_single);

        //출금계좌 다이얼로그 에서 사용하기 위해 셋팅한다.
        OpenBankDataMgr.getInstance().setIS_NEED_SEARCH_AMOUNT(true);

        KeyboardDetectorRelativeLayout mRelativeLayout = (KeyboardDetectorRelativeLayout) findViewById(R.id.layout_main);
        mRelativeLayout.addKeyboardStateChangedListener(this);

        findViewById(R.id.tv_close).setOnClickListener(this);

        mLayoutSelectAccount    = findViewById(R.id.view_select_account);
        mLayoutSelectAccount.setActionListener(this);

        mLayoutInputMoney       = findViewById(R.id.view_input_money);
        mLayoutInputMoney.setActionListener(this);

        mLayoutKeyPad         = findViewById(R.id.view_keypad);
        mLayoutKeyPad.setActionListener(this);

        //금액화면에 키패드 뷰를 넣어준다.
        mLayoutKeyPad.setInputMoneyLayout(mLayoutInputMoney);
        mLayoutInputMoney.setLayoutKeypad(mLayoutKeyPad);

        mLayoutInputDetailAccountInfo  = findViewById(R.id.view_input_detail_account_info);
        mLayoutInputDetailAccountInfo.setActionListener(this);

        mLayoutInputDetailPhoneInfo  = findViewById(R.id.view_input_detail_phone_info);
        mLayoutInputDetailPhoneInfo.setActionListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(isPause){
            isPause = false;
            mLayoutSelectAccount.dispSendAccount();

            if(mLayoutInputDetailAccountInfo.getVisibility() == View.VISIBLE){
                mLayoutInputDetailAccountInfo.updateDetailAccountView();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isPause = true;
    }

    @Override
    public void onBackPressed() {
        showCancelMessage();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_close:
                showCancelMessage();
                break;
        }
    }

    @Override
    public void openInputMoneyView() {
        if(mLayoutInputDetailAccountInfo.getVisibility() == View.VISIBLE){
            mLayoutInputDetailAccountInfo.hideView();
        }else if(mLayoutInputDetailPhoneInfo.getVisibility() == View.VISIBLE){
            mLayoutInputDetailPhoneInfo.hideView();
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLayoutInputMoney.moveOriginPosAmountView();
                mLayoutKeyPad.fadeInKeypad();
            }
        },200);
    }

    @Override
    public void openInputDetailView(String inputMoney) {
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
            mLayoutInputDetailPhoneInfo.setInputMoney(inputMoney);
            mLayoutInputDetailPhoneInfo.showView();
        }else{
            mLayoutInputDetailAccountInfo.setInputMoney(inputMoney);
            mLayoutInputDetailAccountInfo.showView();
        }

    }

    @Override
    public void closeInputDetailView() {
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
            mLayoutInputDetailPhoneInfo.hideView();
        }else{
            mLayoutInputDetailAccountInfo.hideView();
        }
    }

    @Override
    public void clearDetailInfo() {
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
            mLayoutInputDetailPhoneInfo.clearInputText();
        }else{
            mLayoutInputDetailAccountInfo.clearInputText();
        }
    }

    @Override
    public void showDatePicker() {
        MLog.d();

        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
        if(remitteeInfo.getTRNF_DVCD().equalsIgnoreCase("2"))
            mIsDelayService = true;
        else
            mIsDelayService = false;

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                try {
                    Calendar calendar = Calendar.getInstance();

                    if (DataUtil.isNotNull(ret)) {
                        JSONObject object = new JSONObject(ret);
                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        if (DataUtil.isNull(objectHead)) {
                            return;
                        }
                        String result = object.optString("SYS_DTTM");
                        if (!TextUtils.isEmpty(result)) {
                            calendar.set(Integer.parseInt(result.substring(0, 4)), Integer.parseInt(result.substring(4, 6)) - 1, Integer.parseInt(result.substring(6, 8)));
                        }
                    }

                    // 22시 30분이 넘으면 당일 예약 이체가 불가하므로 다음날부터 이체되도록 함
                    if (!mIsDelayService) {
                        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 && calendar.get(Calendar.MINUTE) > 29) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    } else {
                        if (calendar.get(Calendar.HOUR_OF_DAY) + 3 >= 23) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    }

                    mCurYear = calendar.get(Calendar.YEAR);
                    mCurMon = calendar.get(Calendar.MONTH);
                    mCurDay = calendar.get(Calendar.DAY_OF_MONTH);

                    Calendar minDate = Calendar.getInstance();
                    minDate.set(mCurYear, mCurMon, mCurDay);
                    long tmp = calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 90 * 1000.0);
                    DatePickerFragmentDialog.newInstance(
                            DateTimeBuilder.get()
                                    .withMinDate(minDate.getTimeInMillis()).withMaxDate(tmp)
                                    .withTheme(R.style.datepickerCustom)
                    ).show(getSupportFragmentManager(), "DatePickerFragmentDialog");

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    @Override
    public void onDateSet(long date) {
        MLog.d();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date);
        mInputYear = calendar.get(Calendar.YEAR);
        mInputMonth = calendar.get(Calendar.MONTH) + 1;
        mInputDay = calendar.get(Calendar.DAY_OF_MONTH);

        if (mIstoday && (mCurYear != mInputYear || mCurDay != mInputDay)) {
            mIstoday = false;
        }

        SlidingDateTimerPickerDialog slidingDateTimerPickerDialog = new SlidingDateTimerPickerDialog(
                ITransferSendSingleActivity.this,
                Const.PICKER_TYPE_TIME,
                mIstoday,
                mIsDelayService
        );
        slidingDateTimerPickerDialog.setOnConfirmListener(new SlidingDateTimerPickerDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress(int year, int month, int day, int time, boolean needtimeselect, boolean istoday, boolean isDelayService) {

                if (mInputYear != 0 && mInputMonth != 0 && mInputDay != 0 && time != 0) {
                    ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                    remitteeInfo.setTRNF_DVCD("3");
                    remitteeInfo.setTRNF_DMND_DT(String.format("%04d%02d%02d", mInputYear, mInputMonth, mInputDay));
                    remitteeInfo.setTRNF_DMND_TM(String.format("%02d00", time));
                }

                String inputDate = String.format("%04d.%02d.%02d", mInputYear, mInputMonth, mInputDay) +  " "  + String.format("%02d", time) + ":00";
                mLayoutInputDetailAccountInfo.setSendDate(inputDate);

            }
        });
        slidingDateTimerPickerDialog.show();
    }

    /**
     * 취소 메세지 출력
     */
    private void showCancelMessage() {
        MLog.d();
        if (isFinishing()) return;
        AlertDialog alertDialog = new AlertDialog(ITransferSendSingleActivity.this);
        alertDialog.msg = getString(R.string.msg_cancel_transfer);
        alertDialog.mSubText = getString(R.string.msg_transfer_no_save_info);
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MLog.d();
                finish();
            }
        };
        alertDialog.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        };
        alertDialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logs.i("onRequestPermissionsResult");
        switch (requestCode) {
            case Const.REQUEST_PERMISSION_SYNC_CONTACTS: {
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {


                    showProgressDialog();
                    //우선 연락처앱에서 전화번호 조회해온다.
                    GetContactListAsyncTask gpbat = new GetContactListAsyncTask(this,
                            GetContactListAsyncTask.GET_TYPE_CONTACT_APP,
                            new GetContactListAsyncTask.OnFinishReadPhoneBook() {
                                @Override
                                public void onFinishReadPhoneBook(ArrayList<ContactsInfo> array) {
                                    Logs.e("getContactList end - size : " + array.size());

                                    if(array.size() > 0){
                                        requestContactListCompareSaidaServer(array);
                                    }
                                    dismissProgressDialog();
                                }
                            });
                    gpbat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                }else{
                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this,permissions[0]);
                    Logs.e("state : " + state);
                    if(!state){
                        DialogUtil.alert(this,"권한설정","닫기", getString(R.string.msg_permission_read_contacts), new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        PermissionUtils.goAppSettingsActivity(ITransferSendSingleActivity.this);
                                    }
                                },
                                new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                        mLayoutSelectAccount.showSelectRcvAccountDialog();
                                    }
                                });
                    }
                }
            }
            break;

            default:
                break;
        }
    }

    /**
     * 사이다 서버 조회후 동기화된 연락처 파일로 저장
     * @param listContactsInPhonebook
     */
    private void requestContactListCompareSaidaServer(final ArrayList<ContactsInfo> listContactsInPhonebook) {

        JSONArray jsonArray = ContactsUtil.convertToJsonArray(listContactsInPhonebook);
        //폰에 저장된 연락처가 없으면 동기화 하지 않는다.
        if(jsonArray.length() == 0 ){
            return;
        }

        Map param = new HashMap();
        param.put("REC_IN_TLNO", jsonArray);

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                Logs.i("TRA0020200A01 : " + ret);
                dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null) {
                        //에러메세지 출력.
                        DialogUtil.alert(ITransferSendSingleActivity.this,"휴대폰정보를 조회 할 수 없습니다.");
                        return;
                    }

                    ContactsUtil.saveSyncContactData(ITransferSendSingleActivity.this,ret);

                    //여기서 다이얼로그 다시 호출....
                    mLayoutSelectAccount.showSelectRcvAccountDialog();
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Const.REQUEST_SSENSTONE_AUTH: {
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String result = commonUserInfo.getResultMsg();

                    if ("P000".equalsIgnoreCase(result)) {
                        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
                            mLayoutInputDetailPhoneInfo.requestTransfer(commonUserInfo.getSignData());
                        }else{
                            mLayoutInputDetailAccountInfo.requestTransfer(commonUserInfo.getSignData());
                        }
                    } else {
                        Logs.showToast(this, "인증에 실패했습니다.");
                    }
                } else {
                    if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
                        mLayoutInputDetailPhoneInfo.cancelAuthMethod();
                    }else{
                        mLayoutInputDetailAccountInfo.cancelAuthMethod();
                    }
                }
                break;
            }
            case Const.REQUEST_OTP_AUTH:
                if (resultCode == RESULT_OK && data != null) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String otpAuthTools = data.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
                    if (DataUtil.isNotNull(commonUserInfo)
                            && DataUtil.isNotNull(otpAuthTools)
                            && ("3".equalsIgnoreCase(otpAuthTools) || "2".equalsIgnoreCase(otpAuthTools))
                            && data.hasExtra(Const.INTENT_OTP_AUTH_SIGN)) {
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        commonUserInfo.setSignData(data.getStringExtra(Const.INTENT_OTP_AUTH_SIGN));
                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                        Intent intent = new Intent(ITransferSendSingleActivity.this, PincodeAuthActivity.class);
                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                    }
                } else {
                    if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
                        mLayoutInputDetailPhoneInfo.cancelAuthMethod();
                    }else{
                        mLayoutInputDetailAccountInfo.cancelAuthMethod();
                    }
                }
                break;
            case REQUEST_SELECT_RCV:
                if (resultCode == RESULT_OK){
                    finish();
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onKeyboardShown() {
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
            mLayoutInputDetailPhoneInfo.showHideSoftKeyboard(true);
        }else{
            mLayoutInputDetailAccountInfo.showHideSoftKeyboard(true);
        }
    }

    @Override
    public void onKeyboardHidden() {
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){
            mLayoutInputDetailPhoneInfo.showHideSoftKeyboard(false);
        }else{
            mLayoutInputDetailAccountInfo.showHideSoftKeyboard(false);
        }
    }
}
