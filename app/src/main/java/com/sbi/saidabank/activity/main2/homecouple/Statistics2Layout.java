package com.sbi.saidabank.activity.main2.homecouple;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2CoupleExpenceAmount;
import com.sbi.saidabank.define.datatype.main2.Main2CoupleExpenceCount;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import java.util.ArrayList;

/**
 * 커플 가입한 다음달 통계정보 1건 있을때 레이아웃
 * 커플가입 9월. 현재 10월 일때
 */
public class Statistics2Layout extends RelativeLayout {

    private RelativeLayout mLayoutTooltipMsg;
    private ImageView mIvToolTip;

    public Statistics2Layout(Context context) {
        super(context);
        initUX(context);
    }

    public Statistics2Layout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_couple_statistics2, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        //통계타이틀 - 웹 통계화면으로 이동한다.
        LinearLayout statisticsTitle = layout.findViewById(R.id.layout_statistics_title);
        statisticsTitle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), WebMainActivity.class);
                String url = WasServiceUrl.UNT0484100.getServiceUrl();
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                getContext().startActivity(intent);
            }
        });


        TextView mTvExpenceAmonut = layout.findViewById(R.id.tv_expence_amount);
        TextView mTvGraphExpenceAmonut = layout.findViewById(R.id.tv_graph_expence_amount);
        View mVGraphRect = layout.findViewById(R.id.v_graph_expence_rect);
        TextView mTvExpenceMonth = layout.findViewById(R.id.tv_expence_month);

        //전월사용금액
        String amount = Main2DataInfo.getInstance().getPVMN_USE_AMT();
        Double doubleAmount = Double.valueOf(amount);
        if (!TextUtils.isEmpty(amount))
            amount = Utils.moneyFormatToWon(doubleAmount);
        //전월 기준월
        String month = Main2DataInfo.getInstance().getPVMN_BSMN();
        if (!TextUtils.isEmpty(month)) {
            month = String.valueOf(Integer.parseInt(month));
        }

        mTvExpenceAmonut.setText("지난달은 " + amount + "원 지출하였습니다.");
        mTvGraphExpenceAmonut.setText(amount + "원");
        mTvExpenceMonth.setText(month + "월");

        //그래프 높이 설정
        if (doubleAmount == 0) {
            int rectHeight = (int) Utils.dpToPixel(getContext(), 8);

            ViewGroup.LayoutParams params = mVGraphRect.getLayoutParams();
            params.height = rectHeight;
            mVGraphRect.setLayoutParams(params);
        }


        mLayoutTooltipMsg = (RelativeLayout) layout.findViewById(R.id.layout_tooltip_msg);
        int radius = (int) Utils.dpToPixel(getContext(), 3);
        int line = (int) Utils.dpToPixel(getContext(), 1);
        mLayoutTooltipMsg.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF", new int[]{radius, radius, radius, radius}, line, "#000000"));
        mLayoutTooltipMsg.setVisibility(GONE);

        mIvToolTip = layout.findViewById(R.id.iv_tooltip);
        mIvToolTip.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mLayoutTooltipMsg.getVisibility() == VISIBLE) {
                    mLayoutTooltipMsg.setVisibility(GONE);
                    mIvToolTip.setImageResource(R.drawable.btn_tooltip_off);
                } else {
                    mLayoutTooltipMsg.setVisibility(VISIBLE);
                    mIvToolTip.setImageResource(R.drawable.btn_tooltip_on);
                }
            }
        });

        ImageView ivTooltipClose = layout.findViewById(R.id.iv_tooltip_close);
        ivTooltipClose.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mLayoutTooltipMsg.setVisibility(GONE);
                mIvToolTip.setImageResource(R.drawable.btn_tooltip_off);
            }
        });

        //지출 금액별/건수별 화면을 구성한다.
        displayExpenceInfoArea(layout);
    }

    private void displayExpenceInfoArea(View layout) {
        String lastMonth = Utils.getAddMonth(-1);

        int radius = (int) Utils.dpToPixel(getContext(), 28);

        //지출금액별
        Main2CoupleExpenceAmount amountInfo = getExpenceAmount(lastMonth);
        if (amountInfo != null) {
            //아이콘 배경 동그랗게 만들자.
            RelativeLayout mLayoutAmountIcon = layout.findViewById(R.id.layout_amount_icon);

            mLayoutAmountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#00EBFF", new int[]{radius, radius, radius, radius}));

            //결제 종류별 아이콘 표시
            ImageView iconAmount = layout.findViewById(R.id.iv_amount_icon);
            iconAmount.setImageResource(Statistics1Layout.getExpenceIconResourceId(amountInfo.getSETT_TYCD()));

            //결제 유형명
            TextView tvAmountKind = layout.findViewById(R.id.tv_amount_kind);
            tvAmountKind.setText(amountInfo.getFNTR_DVCD_NM());

            //결제 항목명
            TextView tvAmountProdName = layout.findViewById(R.id.tv_amount_prod_name);
            tvAmountProdName.setText(amountInfo.getSETT_ITEM_NM());

            //결제금액
            TextView tvAmountAmount = layout.findViewById(R.id.tv_amount_amount);
            String amountAmount = amountInfo.getSETT_AMT();
            if (!TextUtils.isEmpty(amountAmount)) {
                amountAmount = Utils.moneyFormatToWon(Double.valueOf(amountAmount));
            }
            tvAmountAmount.setText(amountAmount + " 원");
        } else {
            //아이콘 배경 동그랗게 만들자.
            RelativeLayout mLayoutAmountIcon = layout.findViewById(R.id.layout_amount_icon);
            mLayoutAmountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#c3c3c3", new int[]{radius, radius, radius, radius}));

            //사용이력 없음.
            ImageView iconAmount = layout.findViewById(R.id.iv_amount_icon);
            iconAmount.setImageResource(R.drawable.ico_empty);

            TextView tvAmountKind = layout.findViewById(R.id.tv_amount_kind);
            tvAmountKind.setText("사용이력없음");

            TextView tvAmountProdName = layout.findViewById(R.id.tv_amount_prod_name);
            tvAmountProdName.setVisibility(GONE);

            TextView tvAmountAmount = layout.findViewById(R.id.tv_amount_amount);
            tvAmountAmount.setVisibility(INVISIBLE);
        }

        //##########################################################################################
        //지출건수별
        Main2CoupleExpenceCount countInfo = getExpenceCount(lastMonth);
        if (countInfo != null) {
            //아이콘 배경 동그랗게 만들자.
            RelativeLayout mLayoutCountIcon = layout.findViewById(R.id.layout_count_icon);
            mLayoutCountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#00EBFF", new int[]{radius, radius, radius, radius}));

            //결제 종류별 아이콘 표시
            ImageView iconCount = layout.findViewById(R.id.iv_count_icon);
            iconCount.setImageResource(Statistics1Layout.getExpenceIconResourceId(countInfo.getSETT_TYCD()));

            //결제 유형명
            TextView tvCountKind = layout.findViewById(R.id.tv_count_kind);
            tvCountKind.setText(countInfo.getFNTR_DVCD_NM());

            //결제 항목명
            TextView tvCountProdName = layout.findViewById(R.id.tv_count_prod_name);
            tvCountProdName.setText(countInfo.getSETT_ITEM_NM());

            //결제건수
            TextView tvCountCount = layout.findViewById(R.id.tv_count_count);
            String countCount = countInfo.getSETT_CCNT();
            tvCountCount.setText(countCount + " 회");
        } else {
            //회색으로 바꿔준다.
            RelativeLayout mLayoutCountIcon = layout.findViewById(R.id.layout_count_icon);
            mLayoutCountIcon.setBackground(GraphicUtils.getRoundCornerDrawable("#c3c3c3", new int[]{radius, radius, radius, radius}));

            //결제 종류별 아이콘 표시
            ImageView iconCount = layout.findViewById(R.id.iv_count_icon);
            iconCount.setImageResource(R.drawable.ico_empty);

            //결제 유형명
            TextView tvCountKind = layout.findViewById(R.id.tv_count_kind);
            tvCountKind.setText("사용이력없음");

            //결제 항목명
            TextView tvCountProdName = layout.findViewById(R.id.tv_count_prod_name);
            tvCountProdName.setVisibility(GONE);

            //결제건수
            TextView tvCountCount = layout.findViewById(R.id.tv_count_count);
            tvCountCount.setVisibility(INVISIBLE);
        }
    }

    private Main2CoupleExpenceAmount getExpenceAmount(String lastMonth) {
        ArrayList<Main2CoupleExpenceAmount> arrayList = Main2DataInfo.getInstance().getMain2CoupleExpenceAmountArray();
        if (DataUtil.isNotNull(arrayList)) {

            for (int i = 0; i < arrayList.size(); i++) {
                Main2CoupleExpenceAmount info = arrayList.get(i);
                if (info.getSETT_YM().equals(lastMonth)) {
                    return info;
                }
            }
        }

        return null;
    }

    private Main2CoupleExpenceCount getExpenceCount(String lastMonth) {
        ArrayList<Main2CoupleExpenceCount> arrayList = Main2DataInfo.getInstance().getMain2CoupleExpenceCountArray();
        if (DataUtil.isNotNull(arrayList)) {
            for (int i = 0; i < arrayList.size(); i++) {
                Main2CoupleExpenceCount info = arrayList.get(i);
                if (info.getSETT_YM().equals(lastMonth)) {
                    return info;
                }
            }
        }
        return null;
    }
}
