package com.sbi.saidabank.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.dialog.SlidingDriverAreaCodeDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.customview.CustomEditText;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * siadabank_work
 * Class: CertifyIdActivity
 * Created by 950546
 * Date: 2018-11-16
 * Time: 오후 1:36
 * Description: 신분증 본인확인 화면
 */
public class CertifyIdActivity extends BaseActivity implements View.OnClickListener, TextWatcher, TextView.OnEditorActionListener, CustomEditText.OnBackPressListener {

    private static final int STR_TYPE_BIRTH = 1;
    private static final int STR_TYPE_LICENSE_NUMBER = 2;
    private static final int STR_LENGTH_ID_NUMBER = 8;
    private static final int STR_LENGTH_LICENSE_NUMBER = 10;
    private static final int STR_LENGTH_FORMAT_LICENSE_NUMBER = 12;

    private LinearLayout mLayoutIdnumber;
    private LinearLayout mLayoutLicense;
    private Button mBtnId;
    private Button mBtnLicense;
    private Button mBtnOk;
    private CustomEditText mEtId;
    private CustomEditText mEtLicensenumber;
    private ImageView mIvid;
    private TextView mTvArea;
    private InputMethodManager imm = null;
    private ArrayList<RequestCodeInfo> mListArea;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_certify_id);

        initValues();
        initView();
        requestDriverLicenseArea();
    }

    /**
     * 변수 초기화
     */
    private void initValues() {
        mListArea = new ArrayList<RequestCodeInfo>();
        imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    /**
     * UI 초기화
     */
    private void initView() {
        mLayoutIdnumber = findViewById(R.id.ll_idnumber);
        mLayoutLicense = findViewById(R.id.ll_license);
        mBtnId = findViewById(R.id.btn_id);
        mBtnLicense = findViewById(R.id.btn_license);
        mBtnOk = findViewById(R.id.btn_confirm);
        mEtId = findViewById(R.id.et_befdate);
        mTvArea = findViewById(R.id.tv_areacode);
        mEtLicensenumber = findViewById(R.id.et_licensenumber);
        mIvid = findViewById(R.id.iv_id);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
        mBtnOk.setOnClickListener(this);
        mBtnId.setOnClickListener(this);
        mBtnLicense.setOnClickListener(this);
        mEtId.setOnBackPressListener(this);
        mEtId.setOnEditorActionListener(this);
        mEtId.addTextChangedListener(this);
        mEtLicensenumber.setOnBackPressListener(this);
        mEtLicensenumber.setOnEditorActionListener(this);
        mEtLicensenumber.addTextChangedListener(this);
        mTvArea.setOnClickListener(this);
        findViewById(R.id.iv_selecticon).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_id: {
                if (!mEtId.isFocused()) {
                    setIdLayout(true);
                }
                break;
            }
            case R.id.btn_license: {
                if (mEtId.isFocused()) {
                    setIdLayout(false);
                }
                break;
            }
            case R.id.btn_cancel: {
                Intent intent = new Intent(this, CustomerInfoReconfirmActivity.class);
                startActivity(intent);
                finish();
                break;
            }
            case R.id.btn_confirm: {
                if (checkDataValidation()) {
                    Logs.showToast(this, "본인확인 성공 시 내 정보수정 화면으로 이동");
                    requestVerifyIdentification();
                } else {
                    Logs.showToast(this, "정보입력 오류");
                    mBtnOk.setEnabled(false);
                }
                break;
            }
            case R.id.tv_areacode:
            case R.id.iv_selecticon: {
                showAreaCodeList();
                break;
            }

            default:
                break;
        }
    }

    /**
     * 주민번호/운전면허증 입력 화면 레이아웃 셋팅
     *
     * @param bIsIdFocused 주민번호 입력화면이면 true
     */
    private void setIdLayout(boolean bIsIdFocused) {
        if (bIsIdFocused) {
            mBtnId.setTextColor(getResources().getColor(R.color.black));
            mBtnId.setBackgroundResource(R.drawable.background_underline_focused);
            mBtnLicense.setTextColor(getResources().getColor(R.color.color888888));
            mBtnLicense.setBackgroundResource(R.drawable.background_underline);
            mLayoutIdnumber.setVisibility(View.VISIBLE);
            mLayoutLicense.setVisibility(View.GONE);
            mEtId.requestFocus();
        } else {
            mBtnLicense.setTextColor(getResources().getColor(R.color.black));
            mBtnLicense.setBackgroundResource(R.drawable.background_underline_focused);
            mBtnId.setTextColor(getResources().getColor(R.color.color888888));
            mBtnId.setBackgroundResource(R.drawable.background_underline);
            mLayoutIdnumber.setVisibility(View.GONE);
            mLayoutLicense.setVisibility(View.VISIBLE);
            mEtLicensenumber.requestFocus();
            if(mEtLicensenumber.getText().toString().length() < STR_LENGTH_LICENSE_NUMBER)
                setLicenseNumberNormal();
            else
                setLicenseNumberFormat();
        }
        mBtnOk.setEnabled(checkDataValidation());
    }

    /**
     * 생년월일 유효성 체크
     *
     * @param checkstr 생년월일
     * @return 8자리면 true
     */
    private boolean isValidStr(int strtype, String checkstr) {
        int validlength = 0;

        if(TextUtils.isEmpty(checkstr))
            return false;

        switch (strtype) {
            case STR_TYPE_BIRTH: {
                validlength = STR_LENGTH_ID_NUMBER;
                break;
            }

            case STR_TYPE_LICENSE_NUMBER: {
                validlength = STR_LENGTH_FORMAT_LICENSE_NUMBER;
                break;
            }

            default:
                break;
        }
        if (checkstr.length() > 0) {
            if (checkstr.length() == validlength) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    /**
     * 최종 확인 버튼 활성화 체크
     */
    private boolean checkDataValidation() {
        if (mEtId.isFocused()) {
            String birthstr = mEtId.getText().toString();
            if (isValidStr(STR_TYPE_BIRTH, birthstr) == false) {
                return false;
            }
        } else {
            String licenseNumberStr = mEtLicensenumber.getText().toString();
            String areaCodeStr = mTvArea.getText().toString();
            if ((isValidStr(STR_TYPE_LICENSE_NUMBER, licenseNumberStr) == false) || TextUtils.isEmpty(areaCodeStr)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public void onKeyboardBackPress() {
        mBtnOk.setEnabled(checkDataValidation());
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        Logs.i("onEditorAction");
        mBtnOk.setEnabled(checkDataValidation());
        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        Logs.i("beforeTextChanged");
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        Logs.i("onTextChanged");
    }

    @Override
    public void afterTextChanged(Editable s) {
        Logs.i("afterTextChanged");
        CustomEditText curet = (CustomEditText) getCurrentFocus();
        if (curet != null) {
            switch (curet.getId()) {
                case R.id.et_befdate: {
                    if (s.toString().length() == STR_LENGTH_ID_NUMBER) {
                        imm.hideSoftInputFromWindow(mEtId.getWindowToken(), 0);
                        mBtnOk.setEnabled(checkDataValidation());
                    }
                    break;
                }
                case R.id.et_licensenumber: {
                    if (s.toString().length() < STR_LENGTH_LICENSE_NUMBER) {
                       setLicenseNumberNormal();
                    } else if (s.toString().length() == STR_LENGTH_LICENSE_NUMBER) {
                        imm.hideSoftInputFromWindow(mEtId.getWindowToken(), 0);
                        setLicenseNumberFormat();
                    } else if (s.toString().length() < STR_LENGTH_FORMAT_LICENSE_NUMBER) {
                        setLicenseNumberNormal();
                    }
                    mBtnOk.setEnabled(checkDataValidation());
                    break;
                }

                default:
                    break;
            }
        }
    }

    /**
     * 운전면허 번호 입력 완료된 상태(포멧형식 xx-xxxxxx-xx)에서 삭제하면 일반형식으로 변경
     */
    private void setLicenseNumberNormal() {
        String lincensestr = mEtLicensenumber.getText().toString().replaceAll("[^0-9]", "");

        mEtLicensenumber.removeTextChangedListener(this);
        mEtLicensenumber.setText(lincensestr);
        mEtLicensenumber.setSelection(lincensestr.length());
        mEtLicensenumber.addTextChangedListener(this);

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(STR_LENGTH_LICENSE_NUMBER);
        mEtLicensenumber.setFilters(filterArray);
    }

    /**
     * 운전면허 번호 10자리 입력 시 xx-xxxxxx-xx 형식으로 변경
     */
    private void setLicenseNumberFormat() {
        String lincensestr = mEtLicensenumber.getText().toString();

        if (lincensestr.length() != STR_LENGTH_LICENSE_NUMBER)
            return;

        StringBuilder foramtstr = new StringBuilder();
        foramtstr.append(lincensestr.substring(0, 2));
        foramtstr.append("-");
        foramtstr.append(lincensestr.substring(2, 8));
        foramtstr.append("-");
        foramtstr.append(lincensestr.substring(8));

        InputFilter[] filterArray = new InputFilter[1];
        filterArray[0] = new InputFilter.LengthFilter(STR_LENGTH_FORMAT_LICENSE_NUMBER);
        mEtLicensenumber.setFilters(filterArray);

        mEtLicensenumber.removeTextChangedListener(this);
        mEtLicensenumber.setText(foramtstr);
        mEtLicensenumber.setSelection(foramtstr.length());
        mEtLicensenumber.addTextChangedListener(this);
    }

    /**
     * 운전면허증 지역코드 리스트 표시
     */
    private void showAreaCodeList() {
        if (this.isFinishing())
            return;

        SlidingDriverAreaCodeDialog dialog = new SlidingDriverAreaCodeDialog(this, mListArea, new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                RequestCodeInfo areaCodeInfo = mListArea.get(position);
                if (areaCodeInfo == null)
                    return;

                String code = areaCodeInfo.getCD_NM();
                mTvArea.setText(code);
                mBtnOk.setEnabled(checkDataValidation());
            }
        });
        dialog.show();
    }

    /**
     * 운전면허지역번호
     */
    private void requestDriverLicenseArea() {
        Map param = new HashMap();
        param.put("LCCD", "DRVN_LCNS_LCNO");
        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        Logs.e("error msg : " + msg + ", ret : " + ret);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) return;

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        RequestCodeInfo item = new RequestCodeInfo(obj);
                        mListArea.add(item);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void requestVerifyIdentification() {
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String username = loginUserInfo.getCUST_NM();
        String idtPrfDvcd = mEtId.isFocused() ? Const.ID_TYPE_JUMIM : Const.ID_TYPE_DRIVER;
        String jumin = loginUserInfo.getNRID();

        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(idtPrfDvcd) || TextUtils.isEmpty(jumin)) {
            return;
        }

        Map param = new HashMap();
        param.put("CUST_NM", username);
        param.put("IDT_PRF_DVCD", idtPrfDvcd);
        param.put("NRID", jumin);

        if (Const.ID_TYPE_JUMIM.equalsIgnoreCase(idtPrfDvcd)) {
            String isueDD = mEtId.getText().toString();

            param.put("ISUE_DD", isueDD);
        } else if (Const.ID_TYPE_DRIVER.equalsIgnoreCase(idtPrfDvcd)) {
            String areaCode = mTvArea.getText().toString();
            String licenseNum = mEtLicensenumber.getText().toString();

            licenseNum = licenseNum.replaceAll("-", "");
            param.put("DRVN_LCNS_LCNO", areaCode);
            param.put("DRVN_LCNS_SRNO", licenseNum);
        }

        param.put("DRVN_LCNS_EDMS_YN", Const.BRIDGE_RESULT_NO);

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010200A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (TextUtils.isEmpty(ret)) {
                    Logs.e(getResources().getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.e(getResources().getString(R.string.msg_debug_err_response));
                        Logs.e(ret);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    if (objectHead == null) {
                        showErrorMessage(getString(R.string.common_msg_no_reponse_value_was));
                        return;
                    }

                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        showErrorMessage(msg);
                        Logs.e(ret);
                        return;
                    }

                    String trtmStcd = object.optString("TRTM_STCD");
                    if (Const.REQUEST_COMMON_SUCCESS_CODE.equalsIgnoreCase(trtmStcd)) {
                        Logs.showToast(CertifyIdActivity.this, "신분증 진위 체크 성공");
                    } else {
                        String respCntn = object.optString("RESP_CNTN");
                        if (TextUtils.isEmpty(respCntn))
                            respCntn = getString(R.string.common_msg_no_reponse_value_was);

                        showErrorMessage(respCntn);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }
}