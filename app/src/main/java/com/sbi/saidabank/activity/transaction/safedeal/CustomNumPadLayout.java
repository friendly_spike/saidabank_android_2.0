package com.sbi.saidabank.activity.transaction.safedeal;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.sbi.saidabank.R;

public class CustomNumPadLayout extends LinearLayout implements View.OnClickListener{
    private OnNumPadClickListener mListener;

    public CustomNumPadLayout(Context context) {
        super(context);
        initUX(context);
    }

    public CustomNumPadLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_cust_num_pad, null);
        addView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));


        layout.findViewById(R.id.btn_1).setOnClickListener(this);
        layout.findViewById(R.id.btn_2).setOnClickListener(this);
        layout.findViewById(R.id.btn_3).setOnClickListener(this);
        layout.findViewById(R.id.btn_4).setOnClickListener(this);
        layout.findViewById(R.id.btn_5).setOnClickListener(this);
        layout.findViewById(R.id.btn_6).setOnClickListener(this);
        layout.findViewById(R.id.btn_7).setOnClickListener(this);
        layout.findViewById(R.id.btn_8).setOnClickListener(this);
        layout.findViewById(R.id.btn_9).setOnClickListener(this);
        layout.findViewById(R.id.btn_0).setOnClickListener(this);
        layout.findViewById(R.id.btn_cancel).setOnClickListener(this);
        layout.findViewById(R.id.btn_del).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mListener == null){
            Toast.makeText(getContext(),"OnNumPadClickListener를 등록해주세요.",Toast.LENGTH_LONG).show();
            return;
        }

        switch (v.getId()){
            case R.id.btn_cancel:
                mListener.onNumPadClick("cancel");
                break;
            case R.id.btn_del:
                mListener.onNumPadClick("del");
                break;
            default:
                String val = (String)v.getTag();
                mListener.onNumPadClick(val);
                break;
        }
    }

    public void setNumPadClickListener(OnNumPadClickListener listener){
        mListener = listener;
    }

    public interface OnNumPadClickListener{
        void onNumPadClick(String numStr);
    }
}
