package com.sbi.saidabank.activity.main2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptBridge;

import java.util.ArrayList;

public class Main2CoupleWebFragment extends BaseFragment {

    private Context mContext;
    private FrameLayout mContainer;
    private ProgressBarLayout mProgressLayout;
    private JavaScriptBridge mJsBridge;
    private String mServiceUrl;

    public static final Main2CoupleWebFragment newInstance() {
        return new Main2CoupleWebFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        this.mContext = getContext();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main2_web, container, false);
        mProgressLayout = rootView.findViewById(R.id.ll_progress);
        mContainer = rootView.findViewById(R.id.container_webview);
        WebView webView = getCurrentWebView();
        webView.setWebViewClient(new BaseWebClient((Activity) mContext, this));
        webView.setWebChromeClient(new BaseWebChromeClient((Activity) mContext, mContainer, mJsBridge));
        mJsBridge = new JavaScriptBridge((Activity) mContext, mContainer, this);
        webView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);
        return rootView;
    }

    @Override
    public void onResume() {
        MLog.d();
        super.onResume();
        requestMainInfoAfterAction();
    }

    @Override
    public void onPause() {
        MLog.d();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        MLog.d();
        super.onDestroyView();
        if (DataUtil.isNotNull(mJsBridge)) {
            mJsBridge.destroyJavaScriptBrigdge();
        }
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        if (DataUtil.isNotNull(getCurrentWebView()) && ((String) getCurrentWebView().getTag(R.id.TAG_URL)).contains(SaidaUrl.getBaseWebUrl()))
            mJsBridge.callJavascriptFunc("co.app.from.back", null);
        else {
            ((Main2Activity) mContext).showLogoutDialog();
        }
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        mProgressLayout.show();
    }

    @Override
    public void dismissProgress() {
        mProgressLayout.hide();
        if (DataUtil.isNotNull(mContainer) && mContainer.getVisibility() != View.VISIBLE) {
            mContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);
    }

    /**
     * Global menu를 유지한채 웹뷰를 뛰울때 웹에서 302번호 api호출시.Main에서만 주로 처리한다.
     *
     * @param url
     */
    @Override
    public void loadUrl(String url, String params) {
        int webViewCnt = mContainer.getChildCount();
        BaseWebView newWebView = new BaseWebView(mContext);
        newWebView.setTag(webViewCnt + 1);
        newWebView.setWebViewClient(new BaseWebClient(getActivity(), mJsBridge.getWebActionListener()));
        newWebView.setWebChromeClient(new BaseWebChromeClient(getActivity(), mContainer, mJsBridge));
        newWebView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);
        mContainer.addView(newWebView);
        if (DataUtil.isNull(params)) {
            newWebView.loadUrl(url);
        } else {
            newWebView.postUrl(url, params.getBytes());
        }
    }

    @Override
    public void refreshWebView() {
        MLog.d();
        requestMainInfoAfterAction();
    }

    @Override
    public void closeWebView() {
        MLog.d();
        int webViewCnt = mContainer.getChildCount();
        if (webViewCnt <= 1) {
            ((Main2Activity) mContext).showLogoutDialog();
        } else {
            mContainer.removeViewAt(webViewCnt - 1);
            webViewCnt = mContainer.getChildCount();
            if (webViewCnt == 1) {
                WebView webView = (WebView) mContainer.getChildAt(0);
                webView.loadUrl((String) webView.getTag(R.id.TAG_URL));
            }
        }
    }

    @Override
    public void setCurrentPage(String url) {
        MLog.d();
        if (DataUtil.isNotNull(url) && DataUtil.isNotNull(getCurrentWebView())) {
            getCurrentWebView().setTag(R.id.TAG_URL, url);
        }
    }

    @Override
    public void setFinishedPageLoading(boolean flag) {
        MLog.d();
        if (DataUtil.isNotNull(getCurrentWebView())) {
            getCurrentWebView().setTag(R.id.TAG_LOAD_STATE, flag);
        }
    }

    @Override
    public void onWebViewSSLError(String url, String errorCd, final SslErrorHandler handler) {
        DialogUtil.alert(
                mContext,
                R.string.common_msg_error_ssl_cert,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handler.proceed();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handler.cancel();
                    }
                }
        );
    }

// ==============================================================================================================
// VIEW
// ==============================================================================================================
    private WebView getCurrentWebView() {
        int webViewCnt = mContainer.getChildCount();
        if (webViewCnt < 1) return null;
        return (WebView) mContainer.getChildAt(webViewCnt - 1);
    }

// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    private void requestMainInfoAfterAction() {
        MLog.d();
        requestMainSimpleInfo(new OnRequestAfterActionListener() {
            @Override
            public void onRequestAfterAction() {
                moveWebPage();
            }
        });
    }

    /**
     * 상태별로 웹페이지 이동.
     *
     * 00 : 공유형통장 미가입
     * 01 : 공유형통장 초대취소 및 거절(초대전)
     * 02 : 공유형통장 수락대기(계좌주)
     * 03 : 공유형통장 수락 - 네이트브 화면이다. 여기에 들어올수 없다.
     * 04 : 공유향통장 수락대기(공유자)
     * 99 : 일지정지
     */
    private void moveWebPage() {
        MLog.d();

        String stcd = Main2DataInfo.getInstance().getSHRN_PRGS_STCD();
        MLog.i("stcd >> " + stcd);

        if (DataUtil.isNull(getCurrentWebView()))
            return;

        WebView webView = getCurrentWebView();

        switch (stcd) {

            // 커플통장 가입하면이 나와야 함.
            case "00":
                // 계좌가 없으면 입출금 계좌화면이 나와야 한다.
                if (Main2DataInfo.getInstance().getMain2Main2AccountInfo().size() == 0) {
                    mServiceUrl = WasServiceUrl.MAI0070200.getServiceUrl();
                    webView.setTag(R.id.TAG_URL, mServiceUrl);
                    webView.setTag(R.id.TAG_LOAD_STATE, false);
                    webView.loadUrl(mServiceUrl);
                } else {
                    mServiceUrl = WasServiceUrl.MAI0070300.getServiceUrl();
                    if (DataUtil.isNotNull(webView)) {
                        webView.loadUrl(mServiceUrl);
                    }
                }
                break;

            // 커플통장 초대,취소,거절
            case "01":
                mServiceUrl = WasServiceUrl.MAI0070400.getServiceUrl();
                if (DataUtil.isNotNull(webView)) {
                    webView.loadUrl(mServiceUrl);
                }
                break;

            // 커플통장 수락대기 - 공유자
            case "02":
                mServiceUrl = WasServiceUrl.UNT0470600.getServiceUrl();
                if (DataUtil.isNotNull(webView)) {
                    webView.loadUrl(mServiceUrl);
                }
                break;

            // 커플통장 초대요청
            case "04":
                mServiceUrl = WasServiceUrl.UNT0470500.getServiceUrl();
                ArrayList<Main2RelayInfo> arrayList = Main2DataInfo.getInstance().getMain2RelayArray();
                if (!arrayList.isEmpty()) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        Main2RelayInfo relayInfo = arrayList.get(i);
                        // 커플계좌 공유자 수락대기
                        String DSCT_CD = relayInfo.getDSCT_CD();
                        if (DSCT_CD.equals("312")) {
                            String param = "SHRN_DMND_KEY_VAL=" + relayInfo.getCANO();
                            if (DataUtil.isNotNull(webView)) {
                                webView.postUrl(mServiceUrl, param.getBytes());
                            }
                        }
                    }
                }
                break;

            // 일시정지상태
            case "99":
                // 계좌주이고 통장은 공유로 변경된후 출금정지 상태로 변경되면 여기로 들어오는데... 아직 공유자를 연결하지 않은 상태이면 친구 초대 화면이 나와야 한다.
                if (Main2DataInfo.getInstance().getSHRP_DVCD().equals("00") && DataUtil.isNull(Main2DataInfo.getInstance().getSHRP_NM())) {
                    mServiceUrl = WasServiceUrl.MAI0070400.getServiceUrl();
                    if (DataUtil.isNotNull(webView)) {
                        webView.loadUrl(mServiceUrl);
                    }
                    break;
                }

            default:
                if (DataUtil.isNotNull(((Main2Activity) getActivity())) && !((Main2Activity) getActivity()).isFinishing()) {
                    ((Main2Activity) getActivity()).viewPagerUpdate();
                }
                break;
        }
    }
}
