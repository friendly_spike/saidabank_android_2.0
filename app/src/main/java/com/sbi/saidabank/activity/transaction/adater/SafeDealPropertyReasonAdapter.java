package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: TransferAccountAdapter
 * Created by 950485 on 2019. 01. 02..
 * <p>
 * Description:아래에서 위로 올라오는 아래에서 위로 올라오는 최근/자주 이체한 계좌번호 다이얼로그 adapter
 */
public class SafeDealPropertyReasonAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<RequestCodeInfo> dataList;

    public SafeDealPropertyReasonAdapter(Context context, ArrayList<RequestCodeInfo> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        SafeDealPropertyReasonAdapter.AccountViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_transfer_safe_deal_property_list_item, null);
            viewHolder = new AccountViewHolder();
            viewHolder.textName = (TextView) convertView.findViewById(R.id.tv_name);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SafeDealPropertyReasonAdapter.AccountViewHolder) convertView.getTag();
        }

        viewHolder.textName.setText(dataList.get(position).getCD_NM());

        return convertView;
    }

    public void setArrayList(ArrayList<RequestCodeInfo> array){
        dataList = array;
        notifyDataSetChanged();
    }

    static class AccountViewHolder {
        TextView textName;
    }
}
