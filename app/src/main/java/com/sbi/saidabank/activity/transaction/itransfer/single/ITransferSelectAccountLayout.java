package com.sbi.saidabank.activity.transaction.itransfer.single;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendMultiActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSingleActionListener;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertInfoDialog;
import com.sbi.saidabank.common.dialog.SlidingAccountInputDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectRcverAccountDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectSenderAccountDialog;
import com.sbi.saidabank.common.dialog.TransferPhoneRealNameDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;
import com.sbi.saidabank.web.JavaScriptApi;

import java.util.ArrayList;
import java.util.Locale;

public class ITransferSelectAccountLayout extends ITransferBaseLayout implements View.OnClickListener{

    private Context mContext;

    //출금계좌부분
    private RelativeLayout mLayoutSendAccount;
    private ImageView mIvSendIcon;
    private TextView  mTvSendName;
    private TextView  mTvSendText;
    private TextView  mTvSendEmpty;
    private ImageView mIvCouple;
    private ImageView mIvDelayService;
    private ImageView mIvDsgtAccService;


    //입금계좌부분
    private RelativeLayout mLayoutRcvAccount;
    private ImageView mIvRcvIcon;
    private TextView mTvRcvName;
    private TextView mTvRcvText;
    private TextView mTvRcvEmpty;

    private OnITransferSingleActionListener mActionListener;

    private SlidingExpandSelectSenderAccountDialog mSelectSenderDialog;
    private SlidingExpandSelectRcverAccountDialog mSelectRcverDialog;
    public ITransferSelectAccountLayout(Context context) {
        super(context);
        initUX(context);
    }

    public ITransferSelectAccountLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_itransfer_single_select_account, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mContext = context;

        mLayoutSendAccount = layout.findViewById(R.id.layout_send_account);
        mLayoutSendAccount.setOnClickListener(this);
        mLayoutRcvAccount = layout.findViewById(R.id.layout_rcv_account);
        mLayoutRcvAccount.setOnClickListener(this);

        mIvSendIcon = layout.findViewById(R.id.iv_send_icon);
        mTvSendName = layout.findViewById(R.id.tv_send_name);
        mTvSendText = layout.findViewById(R.id.tv_send_text);
        mIvCouple = layout.findViewById(R.id.iv_couple_icon);
        mIvDelayService = layout.findViewById(R.id.iv_delay_service_icon);
        mIvDsgtAccService = layout.findViewById(R.id.iv_dsgt_acc_icon);
        mTvSendEmpty = layout.findViewById(R.id.tv_send_empty);

        mIvRcvIcon = layout.findViewById(R.id.iv_rcv_icon);
        mTvRcvName = layout.findViewById(R.id.tv_rcv_name);
        mTvRcvText = layout.findViewById(R.id.tv_rcv_text);
        mTvRcvEmpty= layout.findViewById(R.id.tv_rcv_empty);
        mTvRcvText.requestFocus();

        dispSendAccount();
        dispRcvAccount(true);

        //계좌목록 가져오기에서 들어왔는데 출금계좌가 선택되어 있지 않으면 다이얼로그 출력
        String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
        if(TextUtils.isEmpty(WTCH_BANK_CD) || TextUtils.isEmpty(WTCH_ACNO)
           ||ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() == ITransferDataMgr.ACCESS_TYPE_IMPORT) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showSelectSendAccountDialog();
                }
            },250);

            return;
        }

        //단건이체 들어왔는데 받는사람이 선택되어 있지 않으면...
        //다건에서 오픈뱅킹계좌로 변경시 발생할수 있는 케이스이다.
        if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_NONE
           ||ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() == ITransferDataMgr.ACCESS_TYPE_EXPORT){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showSelectRcvAccountDialog();
                }
            },250);

        }
    }

    //보내는계좌(출금계좌) 표시
    public void dispSendAccount(){
        mIvCouple.setVisibility(GONE);
        mIvDelayService.setVisibility(GONE);
        mIvDsgtAccService.setVisibility(GONE);

        //고객명 출력
        mTvSendName.setText(LoginUserInfo.getInstance().getCUST_NM());

        String bankCd = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        String detailBankCd = ITransferDataMgr.getInstance().getWTCH_DTLS_BANK_CD();
        String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();

        if(TextUtils.isEmpty(accountNo)){
            mTvSendEmpty.setVisibility(VISIBLE);
            mTvSendName.setVisibility(GONE);
            mTvSendText.setVisibility(GONE);
            mIvCouple.setVisibility(GONE);
            mIvDelayService.setVisibility(GONE);
            mIvDsgtAccService.setVisibility(View.GONE);
            return;
        }else{
            mTvSendEmpty.setVisibility(GONE);
            mTvSendText.setVisibility(VISIBLE);
            mTvSendName.setVisibility(VISIBLE);
        }

        //은행 CI표시
        Uri iconUri = ImgUtils.getIconUri(mContext,bankCd,detailBankCd);
        if(iconUri != null)
            mIvSendIcon.setImageURI(iconUri);
        else
            mIvSendIcon.setImageResource(R.drawable.img_logo_xxx);


        String accountName = "";
        if(bankCd.equalsIgnoreCase("028")){
            ITransferDataMgr.getInstance().setWTCH_BANK_NM(Const.BANK_FULL_NAME);


            //사이다뱅크계좌
            ArrayList<MyAccountInfo> accountInfoArrayList =  LoginUserInfo.getInstance().getMyAccountInfoArrayList();
            for(int i=0;i<accountInfoArrayList.size();i++){
                MyAccountInfo accountInfo = accountInfoArrayList.get(i);
                if(accountInfo.getACNO().equalsIgnoreCase(accountNo)){
                    String ACCO_ALS = accountInfo.getACCO_ALS();
                    if (TextUtils.isEmpty(ACCO_ALS)) {
                        ACCO_ALS = accountInfo.getPROD_NM();
                    }
                    accountName = ACCO_ALS;
                    ITransferDataMgr.getInstance().setWTCH_ACNO_NM(accountName);

                    //커플통장여부
                    if(accountInfo.getSHRN_ACCO_YN().equalsIgnoreCase("Y")){
                        mIvCouple.setVisibility(VISIBLE);
                    }else{
                        mIvCouple.setVisibility(GONE);
                    }

                    break;
                }
            }

            //지연이체서비스 가입여부표시
            if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                mIvDelayService.setVisibility(VISIBLE);
            }else{
                mIvDelayService.setVisibility(GONE);
            }

            //입금지정계좌서비스 가입여부
            if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                mIvDsgtAccService.setVisibility(View.VISIBLE);
            }else{
                mIvDsgtAccService.setVisibility(View.GONE);
            }
        }else{

            //오픈뱅킹계좌
            ArrayList<OpenBankAccountInfo> openBankAccountInfoArrayList = OpenBankDataMgr.getInstance().getOpenBankInOutAccountList();
            for(int i=0;i<openBankAccountInfoArrayList.size();i++){
                OpenBankAccountInfo accountInfo = openBankAccountInfoArrayList.get(i);
                if(accountInfo.ACNO.equalsIgnoreCase(accountNo)){
                    if(TextUtils.isEmpty(accountInfo.ACCO_ALS)){
                        accountName = accountInfo.PROD_NM;
                    }else{
                        accountName = accountInfo.ACCO_ALS;
                    }

                    ITransferDataMgr.getInstance().setWTCH_ACNO_NM(accountName);
                    ITransferDataMgr.getInstance().setWTCH_BANK_NM(accountInfo.BANK_NM);
                    break;
                }
            }

            mIvCouple.setVisibility(GONE);
            mIvDelayService.setVisibility(GONE);
            mIvDsgtAccService.setVisibility(GONE);
        }

        String shortAccNo = accountNo.substring(accountNo.length()-4,accountNo.length());

        if(!TextUtils.isEmpty(accountName) && accountName.length() > 8){
            accountName = accountName.substring(0,8) + Const.ELLIPSIS;
        }

        mTvSendText.setText(accountName + "(" + shortAccNo +")");

    }

    //받는분 계좌표시
    public void dispRcvAccount(boolean goInpuMoneyView){
        int transferType = ITransferDataMgr.getInstance().getTRANSFER_TYPE();

        if(transferType == ITransferDataMgr.TR_TYPE_NONE){
            mIvRcvIcon.setImageResource(R.drawable.img_logo_xxx);
            mTvRcvEmpty.setVisibility(VISIBLE);
            mTvRcvName.setText("");
            mTvRcvName.setVisibility(GONE);
            mTvRcvText.setText("");
            mTvRcvText.setVisibility(GONE);
            return;
        }else{
            mTvRcvEmpty.setVisibility(GONE);
            mTvRcvName.setVisibility(VISIBLE);
            mTvRcvText.setVisibility(VISIBLE);
        }


        if(transferType == ITransferDataMgr.TR_TYPE_PHONE){
            mIvRcvIcon.setImageResource(R.drawable.ico_cellphone);
            ContactsInfo contactsInfo = ITransferDataMgr.getInstance().getTransferContactsInfo();

            mTvRcvName.setText(contactsInfo.getFLNM());
            //mTvRcvName.setTextColor(Color.parseColor("#000000"));
            String phoneNo = contactsInfo.getTLNO();
            if(!TextUtils.isEmpty(phoneNo)){
                phoneNo = PhoneNumberUtils.formatNumber(phoneNo, Locale.getDefault().getCountry());
            }
            mTvRcvText.setText(phoneNo);
        }else{
            //수취인 정보가 없으면 절대 않된다.
            ArrayList<ITransferRemitteeInfo> remitteeInfoArrayList = ITransferDataMgr.getInstance().getRemitteInfoArrayList();
            if(remitteeInfoArrayList.size() == 0){
                return;
            }
            ITransferRemitteeInfo remitteeInfo = remitteeInfoArrayList.get(0);
            if(remitteeInfo == null){
                return;
            }

            //상대금융기관 코드
            Uri iconUri = ImgUtils.getIconUri(mContext,remitteeInfo.getCNTP_FIN_INST_CD(),remitteeInfo.getDTLS_FNLT_CD());
            if(iconUri != null)
                mIvRcvIcon.setImageURI(iconUri);
            else
                mIvRcvIcon.setImageResource(R.drawable.img_logo_xxx);

            //수취인명표시
            mTvRcvName.setText(remitteeInfo.getRECV_NM());
            //mTvRcvName.setTextColor(Color.parseColor("#000000"));

            String acno = remitteeInfo.getCNTP_BANK_ACNO();
            if(remitteeInfo.getCNTP_FIN_INST_CD().equalsIgnoreCase("000")||remitteeInfo.getCNTP_FIN_INST_CD().equalsIgnoreCase("028")){
                String account = acno.substring(0, 5) + "-" + acno.substring(5, 7) + "-" + acno.substring(7, acno.length());
                mTvRcvText.setText(remitteeInfo.getMNRC_BANK_ALS() + " " + account);
            }else{
                mTvRcvText.setText(remitteeInfo.getMNRC_BANK_ALS() + " " + acno);
            }
        }

        if(goInpuMoneyView){
            //화면이 어디에 있던 금액 입력화면으로 이동하도록 한다.
            goInputMoneyView();

            //출금계좌 정보가 없으면 한도조회 진행하지 않는다.
            String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
            String WTCH_ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
            if(!TextUtils.isEmpty(WTCH_BANK_CD) && !TextUtils.isEmpty(WTCH_ACNO) ) {
                //한도조회를 진행한다.
                TransferRequestUtils.requestAccountLimit((Activity) getContext(), new HttpSenderTask.HttpRequestListener() {
                    @Override
                    public void endHttpRequest(String ret) {
                        if(ret.equalsIgnoreCase("TRUE")){
                            //금액 입력 화면에 한도를 넣어주도록 하자.
                            View view = getSlideView(ITransferInputMoneyLayout.class);
                            if(view != null){
                                ((ITransferInputMoneyLayout)view).updateAccountLimit();
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_send_account: {
                /**
                 * 출금계좌 변경시 발생할수 있는 경우의 수.
                 * 1. 한도조회를 다시 한다.
                 * 2. 사이다 -> 오픈뱅킹으로 변경시 입금계좌가 휴대폰이면 입금계좌도 다시 설정해야 한다.
                 * 3. 금액은 초기화 한다.
                 * 4. 상세화면은 초기화 한다.
                 */
                showSelectSendAccountDialog();
                break;
            }
            case R.id.layout_rcv_account: {
                /**
                 * 1.수취확인을 한다.
                 * 2.휴대폰이면 받는분 성함을 확인한다.
                 * 3.금액은 초기화 한다.
                 * 4.상세화면은 초기화 한다.
                 */
                if(TextUtils.isEmpty(ITransferDataMgr.getInstance().getWTCH_ACNO())){
                    DialogUtil.alert(getContext(),"출금계좌를 선택해주세요.");
                    return ;
                }

                showSelectRcvAccountDialog();
                break;
            }
        }
    }


    public void showSelectSendAccountDialog(){
        if(mSelectSenderDialog != null && mSelectSenderDialog.isShowing()) return;
        mSelectSenderDialog = new SlidingExpandSelectSenderAccountDialog((Activity) getContext(), true,new SlidingExpandSelectSenderAccountDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(final String bankCd,final String accountNo,final Object objInfo) {

                if(!bankCd.equalsIgnoreCase("028")){
                    //장애은행인지 여부 체크
                    if(((OpenBankAccountInfo)objInfo).ACNO_REG_STCD.equalsIgnoreCase("3")){
                        TransferUtils.showDialogProblemBank((BaseActivity)getContext());
                        return;
                    }


                    //출금계좌가 오픈뱅킹 SBI이고 입금계좌가 사이다 계좌이면...
                    if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_ACCOUNT){
                        String CNTP_FIN_INST_CD = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getCNTP_FIN_INST_CD();
                        if(bankCd.equalsIgnoreCase("000") && CNTP_FIN_INST_CD.equalsIgnoreCase("028")){
                            DialogUtil.alert(mContext,mContext.getString(R.string.msg_transfer_if_sbi_savingbank),new View.OnClickListener(){

                                @Override
                                public void onClick(View v) {
                                    showSelectSendAccountDialog();
                                }
                            });

                            return;
                        }
                    }

                    //오픈뱅킹인데 받는 사람이 휴대폰 이면 받는사람을 다시 선택해야 한다.
                    if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_PHONE){


                        AlertInfoDialog alertInfoDialog = new AlertInfoDialog((Activity) getContext());
                        alertInfoDialog.mNListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                            }
                        };
                        alertInfoDialog.mPListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //화면 상태가 어떤지 모르지만 만약 상세 입력창에 가 있다면
                                //금액입력화면으로 이동시키도록 한다.
                                goInputMoneyView();



                                //보내는 사람 정보 변경표시
                                ITransferDataMgr.getInstance().setWTCH_BANK_CD(bankCd);
                                ITransferDataMgr.getInstance().setWTCH_ACNO(accountNo);
                                ITransferDataMgr.getInstance().setWTCH_BANK_NM(((OpenBankAccountInfo)objInfo).BANK_NM); //풀네임.
                                ITransferDataMgr.getInstance().setWTCH_BANK_ALS(((OpenBankAccountInfo)objInfo).MNRC_BANK_NM);//단축네임.
                                dispSendAccount();

                                //기존 한도조회 정보를 지워준다.
                                ITransferDataMgr.getInstance().setTransferOneTime(0d);
                                ITransferDataMgr.getInstance().setTransferOneDay(0d);
                                ITransferDataMgr.getInstance().setBalanceAmount(0d);
                                //금액 입력 화면에 한도를 넣어주도록 하자.
                                View inputMoneyView = getSlideView(ITransferInputMoneyLayout.class);
                                if(inputMoneyView != null){
                                    ((ITransferInputMoneyLayout)inputMoneyView).updateAccountLimit();
                                }

                                //기존 휴대폰 정보를 지원준다.
                                ITransferDataMgr.getInstance().setTransferContactsInfo(null);
                                ITransferDataMgr.getInstance().setTRANSFER_TYPE(ITransferDataMgr.TR_TYPE_NONE);

                                //받는분 정보를 지워준다.
                                dispRcvAccount(true);

                                showSelectRcvAccountDialog();
                            }
                        };

                        alertInfoDialog.mTopText = "출금계좌를 변경하시겠습니까?";
                        alertInfoDialog.mSubText = "다른계좌에서는 휴대폰으로 이체가 불가하며,  \n입력된 내용은 삭제됩니다.";
                        alertInfoDialog.show();
                        return;

                    }
                }else{
                    //사이다 거래정지 계좌인지 체크
                    int cnt = LoginUserInfo.getInstance().getMyAccountInfoArrayList().size();
                    for(int i=0;i<cnt;i++){
                        MyAccountInfo info = LoginUserInfo.getInstance().getMyAccountInfoArrayList().get(i);
                        if(info.getACNO().equalsIgnoreCase(accountNo) && info.getACCO_PRGS_STCD().equalsIgnoreCase("01")){
                            TransferUtils.showDialogSuspantionAccount((BaseActivity)getContext(),accountNo);
                            return;
                        }
                    }
                }

                //출금계좌와 입금계좌가 같은지 체크
                if(ITransferDataMgr.getInstance().getRemitteInfoArrayList().size() > 0){
                    ITransferRemitteeInfo info = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                    String rcvBankCd = info.getCNTP_FIN_INST_CD();
                    String rcvAccNo = info.getCNTP_BANK_ACNO();
                    if(bankCd.equalsIgnoreCase(rcvBankCd) && accountNo.equalsIgnoreCase(rcvAccNo)){
                        DialogUtil.alert(getContext(),getContext().getString(R.string.msg_transfer_error_same_acount));
                        return;
                    }
                }


                //화면 상태가 어떤지 모르지만 만약 상세 입력창에 가 있다면
                //금액입력화면으로 이동시키도록 한다.
                goInputMoneyView();

                //보내는 사람 정보 변경표시
                ITransferDataMgr.getInstance().setWTCH_BANK_CD(bankCd);
                //오픈뱅킹은 세부은행코드를 가지고 있어서 넣어줘야 한다.
                if(!bankCd.equalsIgnoreCase("028")){
                    String detailCd = ((OpenBankAccountInfo)objInfo).DTLS_FNLT_CD;
                    if(!TextUtils.isEmpty(detailCd))
                        ITransferDataMgr.getInstance().setWTCH_DTLS_BANK_CD(detailCd);
                }
                ITransferDataMgr.getInstance().setWTCH_ACNO(accountNo);
                dispSendAccount();
                mActionListener.clearDetailInfo();

                //한도조회를 진행한다.
                //한도조회는 조회중 에러가 발생하면 이체화면을 종료한다.
                if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() != ITransferDataMgr.TR_TYPE_NONE){
                    TransferRequestUtils.requestAccountLimit((Activity) getContext(), new HttpSenderTask.HttpRequestListener() {
                        @Override
                        public void endHttpRequest(String ret) {
                            if(ret.equalsIgnoreCase("TRUE")){
                                //금액 입력 화면에 한도를 넣어주도록 하자.
                                View view = getSlideView(ITransferInputMoneyLayout.class);
                                if(view != null){
                                    ((ITransferInputMoneyLayout)view).updateAccountLimit();
                                }
                            }
                        }
                    });
                }

            }
        });
        mSelectSenderDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSelectSenderDialog = null;
            }
        });
        mSelectSenderDialog.show();
    }

    //받는분 선택 다이얼로그를 보여준다.
    public void showSelectRcvAccountDialog(){

        if(mSelectRcverDialog != null && mSelectRcverDialog.isShowing()) return;

        //아이템을 선택했을때 처리방법
        SlidingExpandSelectRcverAccountDialog.OnSelectItemListener selectItemListener = new SlidingExpandSelectRcverAccountDialog.OnSelectItemListener() {
            @Override
            public void onSelectAccountItem(String bankCd,String detailBankCd, String accountNo, String recvNm) {
                //출금계좌와 입금계좌가 같은지 체크
                String sendBankCd = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
                String sendAccNo = ITransferDataMgr.getInstance().getWTCH_ACNO();
                if(bankCd.equalsIgnoreCase(sendBankCd) && accountNo.equalsIgnoreCase(sendAccNo)){
                    DialogUtil.alert(getContext(),getContext().getString(R.string.msg_transfer_error_same_acount));
                    return;
                }

                //수치인조회 *****
                checkRemitteeAccount(bankCd,detailBankCd,accountNo,recvNm);
            }

            @Override
            public void onSelectPhoneItem(String name, String phoneNo) {

                if(!TransferUtils.isSaidaAccount()){
                    DialogUtil.alert(getContext(),getContext().getString(R.string.itransfer_msg_phone_only_able_saida));
                    return;
                }

                //지연이체, 입금지정계좌서비스 체크
                if(!TransferUtils.checkAbleServiceCondition((BaseActivity)getContext(),"휴대폰이체")){
                    return;
                }

                TransferPhoneRealNameDialog dialog = new TransferPhoneRealNameDialog((Activity) getContext(), name, phoneNo, false);
                dialog.setOnConfirmListener(new TransferPhoneRealNameDialog.OnConfirmListener() {
                    @Override
                    public void onConfirmPress(String name, String TLNO, boolean isFromContactList) {
                        goInputMoneyView();

                        ContactsInfo contactsInfo = new ContactsInfo(name,TLNO);
                        ITransferDataMgr.getInstance().setTransferContactsInfo(contactsInfo);
                        dispRcvAccount(true);
                    }
                });
                dialog.show();
            }

            @Override
            public void onSelectGroupItem(String groupId,String favorite,String groupCntText) {
                if(!TransferUtils.isSaidaAccount()){
                    DialogUtil.alert(getContext(),getContext().getString(R.string.itransfer_msg_group_only_able_saida));
                    return;
                }

                //지연이체, 입금지정계좌서비스 체크
                if(!TransferUtils.checkAbleServiceCondition((BaseActivity)getContext(),"다건이체")){
                    return;
                }

                //그룹 등록 가능인원 체크
                if(!TextUtils.isEmpty(groupCntText)){
                    int goupCnt = Integer.parseInt(groupCntText);
                    if(goupCnt > 5){
                        DialogUtil.alert(getContext(),"최대 5건까지\n다건 이체 등록이 가능합니다.");
                        return;
                    }
                }

                TransferRequestUtils.requestRemitteeAccountByMulti((BaseActivity) getContext(),
                                                                    groupId,
                                                                    favorite,
                                                                    true,
                                                                    new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result, String ret) {
                        if(result){
                            //다건화면으로 이동한다.
                            Intent intent = new Intent(getContext(), ITransferSendMultiActivity.class);
                            ((BaseActivity)getContext()).startActivity(intent);
                            ((BaseActivity)getContext()).finish();
                        }
                    }
                });

            }

            @Override
            public void onClickDirectInput() {
                showBankStockAccountDialog();
            }

            @Override
            public void onShowErrorAlertMsg() {
                //출금계좌가 오픈뱅킹 계좌이면 보이지 않도록 한다.
                if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
                    DialogUtil.alert(getContext(),getContext().getString(R.string.itransfer_msg_phone_only_able_saida), new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            showSelectRcvAccountDialog();
                        }
                    });
                    return;
                }

                //지연이체, 입금지정계좌서비스 체크
                if(!TransferUtils.checkAbleServiceCondition((BaseActivity)getContext(),"휴대폰이체")){
                    return;
                }

            }

            @Override
            public void onAskPhonePermission() {
                String[] perList = new String[]{
                        Manifest.permission.READ_CONTACTS,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                };
                PermissionUtils.requestPermission((BaseActivity)getContext(),perList,Const.REQUEST_PERMISSION_SYNC_CONTACTS);
            }

            @Override
            public void onDismissDialog() {
//                if(ITransferDataMgr.getInstance().getTRANSFER_TYPE() == ITransferDataMgr.TR_TYPE_NONE) {
//                    DialogUtil.alert((Activity) getContext(),"받는분을 선택해주세요", new OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            //받는분을 선택하지 않으면 무한루프되도록 한다.
//                            showSelectRcvAccountDialog();
//                        }
//                    });
//                }
            }
        };

        //받는분 선택 다이얼로그 출력
        mSelectRcverDialog = new SlidingExpandSelectRcverAccountDialog((Activity) getContext(),selectItemListener);
        mSelectRcverDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSelectRcverDialog = null;
            }
        });
        mSelectRcverDialog.show();
    }

    /**
     * 은행 직접 선택 다이얼로그
     */
    private void showBankStockAccountDialog(){
        MLog.d();

        String bankListStr = Prefer.getBankList(mContext);
        String stockListStr = Prefer.getStockList(mContext);

        ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
        ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);

        final SlidingExpandBankStockDialog.OnSelectBankStockListener bankStockListener = new SlidingExpandBankStockDialog.OnSelectBankStockListener() {
            @Override
            public void onSelectListener(String bank, String code) {
                showAccountInputDialog(code,bank,"");
            };
        };


        SlidingExpandBankStockDialog bankStockDialog = new SlidingExpandBankStockDialog((Activity) getContext(), bankList, stockList, bankStockListener);
        bankStockDialog.show();
    }

    /**
     * 계좌번호 직접입력 다이얼로그
     *
     * @param bankCode
     * @param bankName
     */
    private String backUpBankCd,backUpBankNm;
    private void showAccountInputDialog(String bankCode, String bankName,String accountNo){
        MLog.d();
        backUpBankCd = bankCode;
        backUpBankNm = bankName;
        String newBankName = bankName;

        //메인 클립보드 이체에서 받은사람 은행코드만 물고들어오기에 여기서 은행 명을 찾아낸다.
        if(TextUtils.isEmpty(newBankName)){
            String bankListStr = Prefer.getBankList(mContext);
            String stockListStr = Prefer.getStockList(mContext);

            ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankListStr);
            ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockListStr);

            ArrayList<RequestCodeInfo> totalList = new ArrayList<RequestCodeInfo>();
            if (bankList != null) {
                totalList.addAll(bankList);
            }

            if (stockList != null) {
                totalList.addAll(stockList);
            }

            for(int i=0;i<totalList.size();i++){
                RequestCodeInfo info = totalList.get(i);
                if(info.getSCCD().equalsIgnoreCase(bankCode)){
                    newBankName = info.getCD_NM();
                    break;
                }
            }
        }

        final SlidingAccountInputDialog.OnFinishAccountInputListener accountInputListener = new SlidingAccountInputDialog.OnFinishAccountInputListener() {
            @Override
            public void onClickBack() {
                showBankStockAccountDialog();
            }

            @Override
            public void onEditFinishListener(String bankCode, String bankName, String accountNum) {
                String msg = "bankCdoe : " + bankCode + " , bankName : " + bankName + " , accountNum " + accountNum;
                if(BuildConfig.DEBUG)
                    Logs.showToast((Activity) getContext(),msg);

                //여기서 수치조회 해야한다.
                checkRemitteeAccount(bankCode, "", accountNum, new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result, String ret) {
                        if(result){
                            dispRcvAccount(true);
                        }else{
                            showAccountInputDialog(backUpBankCd,backUpBankNm,"");
                        }
                    }
                });
            }
        };

        SlidingAccountInputDialog accountInputDialog = new SlidingAccountInputDialog((Activity) getContext(), bankCode, newBankName,false,accountNo,accountInputListener);
        accountInputDialog.show();

    }

    //수취인을 확인한다.
    private void checkRemitteeAccount(String bankCode,String detailBankCd, String accountNo,String recvNm){
        goInputMoneyView();

        TransferRequestUtils.requestRemitteeAccountBySingle((Activity) getContext(),
                bankCode,
                detailBankCd,
                accountNo,
                recvNm,
                false,//단건이기에 무조건 false넣어준다.
                new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result,String ret) {
                        if(result){
                            dispRcvAccount(true);
                        }
                    }
                });
    }

    private void checkRemitteeAccount(String bankCode, String detailBankCd, String accountNo,HttpSenderTask.HttpRequestListener2 listener2){
        TransferRequestUtils.requestRemitteeAccountBySingle((Activity) getContext(),
                bankCode,
                detailBankCd,
                accountNo,
                false,
                listener2);
    }


    private void goInputMoneyView(){
        if(mActionListener != null) {
            mActionListener.openInputMoneyView();
        }
        //계좌를 변경해서 금액입력화면으로 이동할때는
        //상세화면의 데이타를 지워준다.
        clearTextDetailView();
    }

    private void clearTextDetailView(){
        View accountDetailView = getSlideView(ITransferInputDetailAccountLayout.class);
        if(accountDetailView != null){
            ((ITransferInputDetailAccountLayout)accountDetailView).clearInputText();
        }

        View phoneDetailView = getSlideView(ITransferInputDetailPhoneLayout.class);
        if(phoneDetailView != null){
            ((ITransferInputDetailPhoneLayout)phoneDetailView).clearInputText();
        }
    }

    public void setActionListener(OnITransferSingleActionListener listener){
        mActionListener = listener;
    }

}
