package com.sbi.saidabank.activity.main2.openbanking;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;


/**
 * Create by 20210119
 * 오픈뱅킹 리스트중 스티키 해더 부분을 출력
 */
public class ViewHolderHeader extends RecyclerView.ViewHolder{

    public TextView mTvGroupName;
    public TextView mTvGroupCnt;
    public TextView mTvGroupSumAmount;

    public static ViewHolderHeader newInstance(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main2_openbank_list_item_header, parent, false);
        return new ViewHolderHeader(parent.getContext(),itemView);
    }

    public ViewHolderHeader(Context context, @NonNull View itemView) {
        super(itemView);

        mTvGroupName    = itemView.findViewById(R.id.tv_group_name);
        mTvGroupCnt     = itemView.findViewById(R.id.tv_group_cnt);
        mTvGroupSumAmount = itemView.findViewById(R.id.tv_group_sum);

    }

    public void onBindView(OpenBankDataMgr.OpenBankGroupInfo groupInfo) {
        mTvGroupName.setText(groupInfo.getGroupName());
        mTvGroupCnt.setText(groupInfo.getGroupAccCnt()+"");
        mTvGroupSumAmount.setText(groupInfo.getGroupAmountSum()+" 원");
    }

}
