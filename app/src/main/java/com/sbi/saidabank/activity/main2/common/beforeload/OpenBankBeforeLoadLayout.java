package com.sbi.saidabank.activity.main2.common.beforeload;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.main2.common.banner.BannerLayout;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

public class OpenBankBeforeLoadLayout extends RelativeLayout {


    private RelativeLayout mLayoutTotal;
    private RelativeLayout mLayoutSalary;
    private RelativeLayout mLayoutItem;

    public OpenBankBeforeLoadLayout(Context context) {
        super(context);
        initUX(context);
    }

    public OpenBankBeforeLoadLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_main2_before_loaded_openbank, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mLayoutTotal = layout.findViewById(R.id.layout_total);
        mLayoutSalary = layout.findViewById(R.id.layout_salary);
        mLayoutItem = layout.findViewById(R.id.layout_item);


        int radius = (int) Utils.dpToPixel(getContext(),12);
        mLayoutTotal.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));

        radius = (int) Utils.dpToPixel(getContext(),20);
        mLayoutSalary.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mLayoutItem.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));

    }

    public void hideLayout(){

        animate()
                .alpha(0f)
                .setDuration(500)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(GONE);
                    }
                } )
                .start();

    }
}
