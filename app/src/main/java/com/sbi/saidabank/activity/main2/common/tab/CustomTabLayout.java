package com.sbi.saidabank.activity.main2.common.tab;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.push.FLKPushAgentReceiver;


public class CustomTabLayout extends LinearLayout implements View.OnClickListener{
    private static int ANIMATION_TIME = 250;//milisecond.

    int[][] mTabIcon = {
            {R.drawable.ico_home_on,R.drawable.ico_home_off},
            {R.drawable.ico_product_on,R.drawable.ico_product_off},
            {R.drawable.ico_my_on,R.drawable.ico_my_off},
            {R.drawable.ico_notice_on,R.drawable.ico_notice_off},
            {R.drawable.ico_menu_on,R.drawable.ico_menu_off},
    };

    String[] mTabText = {
            "홈",
            "상품",
            "MY",
            "알림",
            "전체"
    };

    private ViewPager mViewPager;
    private OnCustomTabLayoutListener mListener;

    private LinearLayout mHomeMenuLayout;
    private TabLayout mTabLayout;

    private int mSelectTabIdx;
    private int mSelectHomeMenuIdx;
    private ImageView mIvPointer;
    private View mViewLine;
    private LinearLayout mLayoutMy;
    private LinearLayout mLayoutCouple;
    private LinearLayout mLayoutOpen;

    private TextView mTvMyTab;
    private TextView mTvCoupleTab;
    private TextView mTvOpenTab;

    private float  mOriginHomeMenuPosY;
    private boolean isNowAnimating;


    public CustomTabLayout(Context context) {
        super(context);
        initUX(context);
    }

    public CustomTabLayout(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        mListener = (OnCustomTabLayoutListener) context;

        View layout = View.inflate(context, R.layout.layout_main2_tab, null);
        addView(layout, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        mHomeMenuLayout = layout.findViewById(R.id.layout_home_menu);

        mTabLayout      = layout.findViewById(R.id.layout_tab_main);
        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                MLog.d();

                if(mViewPager != null)
                    mViewPager.setCurrentItem(tab.getPosition(),false);

                mSelectTabIdx = tab.getPosition();

                RelativeLayout tabItem = (RelativeLayout)tab.getCustomView();

                ImageView tabIconView = ((ImageView)tabItem.findViewById(R.id.tab_icon));
                tabIconView.setImageResource(mTabIcon[mSelectTabIdx][0]);

                ((TextView)tabItem.findViewById(R.id.tab_name)).setTextColor(Color.parseColor("#1d1d1d"));

                if(tab.getPosition() == 0){
                    showHomeMenu();
                }else{
                    hideHomeMenu();
                }

                //알림탭 선택시 기존 신규 알림 아이콘이면 변경해준다.
                if(tab.getPosition() == 3){
                    if(Prefer.getFlagShowFirstNotiMsgBox(getContext())){
                        Prefer.setFlagShowFirstNotiMsgBox(getContext(),false);
                    }
                    FLKPushAgentReceiver.updateBadge(getContext(),0);
                }

                mListener.onClickTab(mSelectTabIdx);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                RelativeLayout tabItem = (RelativeLayout)tab.getCustomView();

                ((ImageView)tabItem.findViewById(R.id.tab_icon)).setImageResource(mTabIcon[tab.getPosition()][1]);
                ((TextView)tabItem.findViewById(R.id.tab_name)).setTextColor(Color.parseColor("#666666"));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }

        });


        addHomeMenu(context);
        addTabItem(context);

        //앱 시작전에 들어온 새 메세지가 있으면 알림탭 이미지를 변경해라.
        if(Prefer.getFlagShowFirstNotiMsgBox(getContext())){
            reachNewNotiMsg();
        }

        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        drawTabPointerLine(0);
                        //홈 메뉴 높이를 그림자를 뺀 높이로 잡아둔다. 홈메뉴가 아래로 내려 갔을때도 그림자는 보이게 하기 위해.
                        mOriginHomeMenuPosY = mHomeMenuLayout.getY();
                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });
    }

    private void addHomeMenu(Context context){
        RelativeLayout layout = (RelativeLayout) View.inflate(context, R.layout.layout_main2_tab_home_menu_item, null);

        mIvPointer = (ImageView) layout.findViewById(R.id.iv_tab_pointer);
        LinearLayout layout_body = (LinearLayout) layout.findViewById(R.id.layout_body);

        int radius = (int) Utils.dpToPixel(context,23);
        layout_body.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,0,0}));

        mViewLine = (View) layout.findViewById(R.id.v_line);


        mLayoutMy = (LinearLayout) layout.findViewById(R.id.layout_my_tab);
        mLayoutMy.setOnClickListener(this);
        mLayoutCouple = (LinearLayout) layout.findViewById(R.id.layout_couple_tab);
        mLayoutCouple.setOnClickListener(this);
        mLayoutOpen = (LinearLayout) layout.findViewById(R.id.layout_open_tab);
        mLayoutOpen.setOnClickListener(this);

        mTvMyTab = (TextView)layout.findViewById(R.id.tv_my_tab_name);
        mTvCoupleTab = (TextView)layout.findViewById(R.id.tv_couple_tab_name);
        mTvOpenTab = (TextView)layout.findViewById(R.id.tv_open_tab_name);

        mHomeMenuLayout.addView(layout);

    }


    private void addTabItem(Context context){
        for(int i=0;i<mTabText.length;i++){
            RelativeLayout layout = (RelativeLayout) View.inflate(context, R.layout.layout_main2_tab_item, null);

            ((ImageView)layout.findViewById(R.id.tab_icon)).setImageResource(mTabIcon[i][1]);
            ((TextView)layout.findViewById(R.id.tab_name)).setText(mTabText[i]);
            ((TextView)layout.findViewById(R.id.tab_name)).setTextColor(Color.parseColor("#666666"));

            TabLayout.Tab newTab = mTabLayout.newTab().setCustomView(layout);
            mTabLayout.addTab(newTab,i);
        }

    }

    public void showHomeMenu(){
        Logs.e("showHomeMenu");
        if(isNowAnimating){
            moveHomeMenu(mOriginHomeMenuPosY,ANIMATION_TIME);
        }else{
            moveHomeMenu(mOriginHomeMenuPosY,0);
        }
    }

    public void hideHomeMenu(){
        Logs.e("hideHomeMenu");
        //float translationY = mOriginHomeMenuPosY + mHomeMenuLayout.getHeight();
        float translationY = mOriginHomeMenuPosY + getResources().getDimension(R.dimen.main2_homemenu_height);

        if(isNowAnimating){
            moveHomeMenu(translationY,ANIMATION_TIME);
        }else{
            moveHomeMenu(translationY,0);
        }
    }

    private void moveHomeMenu(float transY,int delayTime){
        mHomeMenuLayout.animate()
                .translationY(transY)
                .setDuration(ANIMATION_TIME)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        isNowAnimating = true;
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        isNowAnimating = false;
                    }
                } )
                .setStartDelay(delayTime)
                .start();
    }


    public void setViewPager(ViewPager viewPager){
        mViewPager = viewPager;
    }

    public TabLayout getTabLayout(){
        return mTabLayout;
    }

    public int getSelectHomeMenuIdx(){
        return mSelectHomeMenuIdx;
    }

    public int getSelectTabIdx(){
        return mSelectTabIdx;
    }

    public void reachNewNotiMsg(){
        MLog.d();
        TabLayout.Tab tab = mTabLayout.getTabAt(3);
        RelativeLayout tabItem = (RelativeLayout)tab.getCustomView();

        ImageView tabIconView = ((ImageView)tabItem.findViewById(R.id.tab_icon));

        if(getSelectTabIdx() == 3){
            tabIconView.setImageResource(R.drawable.ico_notice_on_new);
        }else{
            tabIconView.setImageResource(R.drawable.ico_notice_off_new);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_my_tab:
                mSelectHomeMenuIdx = 0;
                break;
            case R.id.layout_couple_tab:
                mSelectHomeMenuIdx = 1;
                break;
            case R.id.layout_open_tab:

                //오픈뱅킹 계좌 만료후
                if(!TextUtils.isEmpty(LoginUserInfo.getInstance().getOBA_RESP_CD()) && LoginUserInfo.getInstance().getOBA_RESP_CD().equalsIgnoreCase("2")){

                    Intent intent = new Intent(getContext(), WebMainActivity.class);
                    intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0110101.getServiceUrl());
                    getContext().startActivity(intent);
                    return;
                }

                mSelectHomeMenuIdx = 2;
                break;
        }
        drawTabPointerLine(mSelectHomeMenuIdx);
        mListener.onClickHomeMenu(mSelectHomeMenuIdx);
    }

    private void drawTabPointerLine(int idx){
        int textWidth=0;
        int layoutXPos=0;
        int layoutWidth=0;
        mTvMyTab.setTextColor(Color.parseColor("#8c97ac"));
        mTvCoupleTab.setTextColor(Color.parseColor("#8c97ac"));
        mTvOpenTab.setTextColor(Color.parseColor("#8c97ac"));
        switch (idx){
            case 0:
                mTvMyTab.setTextColor(Color.parseColor("#293542"));
                textWidth = mTvMyTab.getWidth();
                layoutXPos = (int) mLayoutMy.getX();
                layoutWidth = (int) mLayoutMy.getWidth();
                break;
            case 1:
                mTvCoupleTab.setTextColor(Color.parseColor("#293542"));
                textWidth = mTvCoupleTab.getWidth();
                layoutXPos = (int) mLayoutCouple.getX();
                layoutWidth = (int) mLayoutCouple.getWidth();
                break;
            case 2:
                mTvOpenTab.setTextColor(Color.parseColor("#293542"));
                textWidth = mTvOpenTab.getWidth();
                layoutXPos = (int) mLayoutOpen.getX();
                layoutWidth = (int) mLayoutOpen.getWidth();
                break;
        }
        int offset = (int)Utils.dpToPixel(getContext(),23);
        int posX = (int)(layoutXPos + layoutWidth/2 + offset - mIvPointer.getWidth()/2);
        mIvPointer.animate()
                .translationX(posX)
                .setDuration(250)
                .start();


        posX = (int)(layoutXPos + layoutWidth/2 + offset - textWidth/2);
        mViewLine.animate()
                .translationX(posX)
                .setDuration(250)
                .start();

        //라인 길이 맞춤.
        ViewGroup.LayoutParams lineParam = mViewLine.getLayoutParams();
        lineParam.width = textWidth;
        mViewLine.setLayoutParams(lineParam);

    }
}
