package com.sbi.saidabank.activity.transaction.itransfer.single;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.ITransferCompleteActivity;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendSingleActivity;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSingleActionListener;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmSingleITransferDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingPreventAskDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.ahnlab.enginesdk.MetadataManager.getString;


public class ITransferInputDetailAccountLayout extends ITransferBaseLayout implements View.OnClickListener, View.OnFocusChangeListener {


    private ScrollView mScrollView;

    //상세정보
    private RelativeLayout mLayoutLine1Info;
    private RelativeLayout mLayoutLine2Info;
    private RelativeLayout mLayoutLine3Info;
    private RelativeLayout mLayoutLine4Info;

    private EditText        mEtDispRcvAccText;
    private EditText        mEtDispMyAccText;
    private EditText        mEtMemoText;

    private ImageView        mIvDispRcvAccErase;
    private ImageView        mIvDispMyAccErase;
    private ImageView        mIvMemoErase;

    private TextView         mTvSendDateText;
    private ImageView        mIvSendDateDropDown;
    private ImageView        mIvSendDateErase;

    //툴팁관련
    private ImageView      mIvOpenTooltip;
    private RelativeLayout mLayoutTooltip;
    private ImageView      mIvTooltipClose;

    //예약이체/메모확장
    private LinearLayout   mLayoutOpenMore;
    private ImageView      mIvOpenMore;

    //이체추가버튼
    private LinearLayout   mLayoutAddTransfer;
    private TextView       mTvAddTransfer;

    //지연이체 알림
    private LinearLayout   mLayoutDelayServiceNotify;

    private TextView       mTvTransferBtn;

    private OnITransferSingleActionListener mActionListener;

    private String mBackupTrnDvcd;

    public ITransferInputDetailAccountLayout(Context context) {
        super(context);
        initUX(context);
    }

    public ITransferInputDetailAccountLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_itransfer_input_detail_account_info, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mScrollView = layout.findViewById(R.id.scrollview);

        //상세정보 입력화면
        mLayoutLine1Info = layout.findViewById(R.id.layout_detail_line_1);
        mLayoutLine2Info = layout.findViewById(R.id.layout_detail_line_2);
        mLayoutLine3Info = layout.findViewById(R.id.layout_detail_line_3);
        mLayoutLine4Info = layout.findViewById(R.id.layout_detail_line_4);
        int radius = (int) Utils.dpToPixel(getContext(),6);
        mLayoutLine1Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));
        mLayoutLine2Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));
        mLayoutLine3Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));
        mLayoutLine4Info.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5"));



        //삭제 이미지.
        mIvDispRcvAccErase = findViewById(R.id.iv_line_1_erase);
        mIvDispRcvAccErase.setOnClickListener(this);
        mIvDispMyAccErase  = findViewById(R.id.iv_line_2_erase);
        mIvDispMyAccErase.setOnClickListener(this);
        mIvMemoErase  = findViewById(R.id.iv_line_4_erase);
        mIvMemoErase.setOnClickListener(this);

        //보낼일시 처리
        mTvSendDateText = findViewById(R.id.tv_senddate_text);
        mIvSendDateDropDown = findViewById(R.id.iv_senddate_dropdown);
        mIvSendDateErase = findViewById(R.id.iv_senddate_erase);
        mIvSendDateErase.setOnClickListener(this);


        //에디트텍스트 설정.
        initViewEditTexts();

        LinearLayout layoutDisplaySendDate = findViewById(R.id.layout_send_date);
        layoutDisplaySendDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mBackupTrnDvcd = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getTRNF_DVCD();
                mActionListener.showDatePicker();
            }
        });

        //툴팁관련
        mIvOpenTooltip = layout.findViewById(R.id.iv_open_tooltip);
        mIvOpenTooltip.setOnClickListener(this);

        //툴팁레이아웃
        mLayoutTooltip = layout.findViewById(R.id.layout_tooltip);
        radius = (int) Utils.dpToPixel(getContext(),3);
        mLayoutTooltip.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#000000"));
        mIvTooltipClose = layout.findViewById(R.id.iv_tooltip_close);
        mIvTooltipClose.setOnClickListener(this);

        //입력란 확장
        mLayoutOpenMore = layout.findViewById(R.id.layout_open_more_info);
        mLayoutOpenMore.setOnClickListener(this);
        mIvOpenMore = layout.findViewById(R.id.iv_open_more_info);

        //이체추가 버튼
        //지연이체 서비스 가입시에는 이체추가 버튼이 사라진다.
        mLayoutAddTransfer = layout.findViewById(R.id.layout_add_transfer);
        mTvAddTransfer = layout.findViewById(R.id.tv_add_transfer);
        //이체추가버튼 밑줄 그리기.
        mTvAddTransfer.setPaintFlags(mTvAddTransfer.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mTvAddTransfer.setOnClickListener(this);

        //이체버튼 연결
        mTvTransferBtn = findViewById(R.id.tv_tranfer_btn);
        mTvTransferBtn.setOnClickListener(this);


        //지연이체 알림
        mLayoutDelayServiceNotify = layout.findViewById(R.id.layout_delay_service_noti);

    }

    private void initViewEditTexts(){
        //에디트텍스트
        mEtDispRcvAccText = findViewById(R.id.et_line_1);
        mEtDispRcvAccText.setOnFocusChangeListener(this);
        mEtDispRcvAccText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Logs.e("onTextChanged : " + s);

                if(TextUtils.isEmpty(mEtDispRcvAccText.getText().toString())){
                    mIvDispRcvAccErase.setVisibility(GONE);
                }else{
                    mIvDispRcvAccErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEtDispRcvAccText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(getContext(),mEtDispRcvAccText);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });


        mEtDispMyAccText = findViewById(R.id.et_line_2);
        mEtDispMyAccText.setOnFocusChangeListener(this);
        mEtDispMyAccText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(mEtDispMyAccText.getText().toString())){
                    mIvDispMyAccErase.setVisibility(GONE);
                }else{
                    mIvDispMyAccErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mEtDispMyAccText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(getContext(),mEtDispMyAccText);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });

        mEtMemoText = findViewById(R.id.et_line_4);
        mEtMemoText.setOnFocusChangeListener(this);
        mEtMemoText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(TextUtils.isEmpty(mEtMemoText.getText().toString())){
                    mIvMemoErase.setVisibility(GONE);
                }else{
                    mIvMemoErase.setVisibility(VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEtMemoText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_DONE:
                        Utils.hideKeyboard(getContext(),mEtMemoText);
                        break;
                    default:
                        return false;
                }
                return true;
            }
        });
    }

    public void setInputMoney(String inputMoney){
        if(TextUtils.isEmpty(inputMoney)) return;

        //금액을 이체 정보에 넣어준다.
        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
        remitteeInfo.setTRN_AMT(inputMoney);

        //받는분,내통장 표기내용 출력.
        if(TextUtils.isEmpty(mEtDispRcvAccText.getText().toString())) {
            if(TextUtils.isEmpty(remitteeInfo.getDEPO_BNKB_MRK_NM())){
                mEtDispRcvAccText.setText(remitteeInfo.getCUST_NM());
                remitteeInfo.setDEPO_BNKB_MRK_NM(remitteeInfo.getCUST_NM());
            }else{
                mEtDispRcvAccText.setText(remitteeInfo.getDEPO_BNKB_MRK_NM());
                remitteeInfo.setDEPO_BNKB_MRK_NM(remitteeInfo.getDEPO_BNKB_MRK_NM());
            }
        }
        if(TextUtils.isEmpty(mEtDispMyAccText.getText().toString())) {
            if(TextUtils.isEmpty(remitteeInfo.getTRAN_BNKB_MRK_NM())){
                mEtDispMyAccText.setText(remitteeInfo.getRECV_NM());
                remitteeInfo.setTRAN_BNKB_MRK_NM(remitteeInfo.getRECV_NM());
            }else{
                mEtDispMyAccText.setText(remitteeInfo.getTRAN_BNKB_MRK_NM());
                remitteeInfo.setDEPO_BNKB_MRK_NM(remitteeInfo.getTRAN_BNKB_MRK_NM());
            }

        }
    }

    public void setActionListener(OnITransferSingleActionListener listener){
        mActionListener = listener;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {


        int radius = (int) Utils.dpToPixel(getContext(),6);
        Drawable background; 
        if(hasFocus){
            background = GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#01a2b4");
        }else{
            background = GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#e5e5e5");

        }

        switch (v.getId()){
            case R.id.et_line_1:
                mLayoutLine1Info.setBackground(background);
                if(hasFocus){
                    if(!TextUtils.isEmpty(mEtDispRcvAccText.getText().toString())){
                        mIvDispRcvAccErase.setVisibility(VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEtDispRcvAccText.setSelection(mEtDispRcvAccText.getText().toString().length());
                            }
                        },100);
                    }
                }else{
                    mIvDispRcvAccErase.setVisibility(GONE);
                }
                break;
            case R.id.et_line_2:
                mLayoutLine2Info.setBackground(background);
                if(hasFocus){
                    if(!TextUtils.isEmpty(mEtDispMyAccText.getText().toString())){
                        mIvDispMyAccErase.setVisibility(VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEtDispMyAccText.setSelection(mEtDispMyAccText.getText().toString().length());
                            }
                        },100);
                    }
                }else{
                    mIvDispMyAccErase.setVisibility(GONE);
                }
                break;
            case R.id.et_line_4:
                mLayoutLine4Info.setBackground(background);
                if(hasFocus){
                    if(!TextUtils.isEmpty(mEtMemoText.getText().toString())){
                        mIvMemoErase.setVisibility(VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mEtMemoText.setSelection(mEtMemoText.getText().toString().length());
                            }
                        },100);
                    }
                }else{
                    mIvMemoErase.setVisibility(GONE);
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_open_more_info:
                clearFocus();
                if(mLayoutLine3Info.getVisibility() != VISIBLE){
                    mLayoutLine3Info.setVisibility(VISIBLE);
                    mLayoutLine4Info.setVisibility(VISIBLE);
                    ObjectAnimator.ofFloat(mIvOpenMore, View.ROTATION, 0, 180f) .setDuration(100) .start();
                }else{
                    mLayoutLine3Info.setVisibility(GONE);
                    mLayoutLine4Info.setVisibility(GONE);
                    ObjectAnimator.ofFloat(mIvOpenMore, View.ROTATION, 180f, 360f) .setDuration(100) .start();
                }
                break;
            case R.id.iv_open_tooltip:
                if(mLayoutTooltip.getVisibility() != VISIBLE){
                    mLayoutTooltip.setVisibility(VISIBLE);
                    mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_on);
                }else{
                    mLayoutTooltip.setVisibility(GONE);
                    mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_off);
                }
                break;
            case R.id.iv_tooltip_close:
                mLayoutTooltip.setVisibility(GONE);
                mIvOpenTooltip.setImageResource(R.drawable.btn_tooltip_off);
                break;
            case R.id.iv_line_1_erase:
                mEtDispRcvAccText.setText("");
                break;
            case R.id.iv_line_2_erase:
                mEtDispMyAccText.setText("");
                break;
            case R.id.iv_line_4_erase:
                mEtMemoText.setText("");
                break;
            case R.id.iv_senddate_erase:
                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                remitteeInfo.setTRNF_DVCD(mBackupTrnDvcd);
                remitteeInfo.setTRNF_DMND_DT(Const.EMPTY);
                remitteeInfo.setTRNF_DMND_TM(Const.EMPTY);

                mIvSendDateDropDown.setVisibility(View.VISIBLE);
                mIvSendDateErase.setVisibility(View.GONE);

                if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                    if(mBackupTrnDvcd.equalsIgnoreCase("2")){
                        mTvSendDateText.setText(getContext().getString(R.string.msg_after_3_hour));
                        return;
                    }
                }
                mTvSendDateText.setText(getContext().getString(R.string.Immediately));
                mBackupTrnDvcd="1";
                break;
            case R.id.tv_add_transfer:
                DialogUtil.alert(getContext(),"이체를 추가하시겠습니까?",
                        new View.OnClickListener(){
                            @Override
                            public void onClick(View v) {
                                ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                                //입력한 값이 있을때 넣어준다. - 포커스가 사라질때 모델링에 넣도록 변경.
                                if(!TextUtils.isEmpty(mEtDispRcvAccText.getText().toString()))
                                    remitteeInfo.setDEPO_BNKB_MRK_NM(mEtDispRcvAccText.getText().toString());// 수신통장 표시명(입금계좌표시명)

                                if(!TextUtils.isEmpty(mEtDispMyAccText.getText().toString()))
                                    remitteeInfo.setTRAN_BNKB_MRK_NM(mEtDispMyAccText.getText().toString());// 송신통장 표시명(출금계좌표시명)

                                remitteeInfo.setTRN_MEMO_CNTN(mEtMemoText.getText().toString());// 메모

                                Intent intent = new Intent(getContext(), ITransferSelectReceiverActivity.class);
                                intent.putExtra(Const.INTENT_IS_ADD_TRANSFER,true);
                                ((BaseActivity)getContext()).startActivityForResult(intent, ITransferSendSingleActivity.REQUEST_SELECT_RCV);
                            }
                        },
                        new View.OnClickListener(){
                            @Override
                            public void onClick(View v) {

                            }
                        });

                break;
            case R.id.tv_tranfer_btn:
                mTvTransferBtn.setEnabled(false);
                requestCheckAmount();
                break;
        }
    }


    public void hideView(){
        if(mEtDispRcvAccText.hasFocus()){
            Utils.hideKeyboard(getContext(),mEtDispRcvAccText);
            mEtDispRcvAccText.clearFocus();
        }
        if(mEtDispMyAccText.hasFocus()){
            Utils.hideKeyboard(getContext(),mEtDispMyAccText);
            mEtDispMyAccText.clearFocus();
        }
        if(mEtMemoText.hasFocus()){
            Utils.hideKeyboard(getContext(),mEtMemoText);
            mEtMemoText.clearFocus();
        }

        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_out_transfer_view);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                //mTvAmount.setEnabled(true);
                setVisibility(GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(ani);
    }

    public void showView(){
        mIvDispRcvAccErase.setVisibility(GONE);
        mIvDispMyAccErase.setVisibility(GONE);
        mIvMemoErase.setVisibility(GONE);

        setVisibility(VISIBLE);
        if(mLayoutLine4Info.getVisibility() != VISIBLE)
            mEtMemoText.requestFocus();
        else
            mEtMemoText.clearFocus();

        if(TextUtils.isEmpty(mEtMemoText.getText().toString())){
            String TRN_MEMO_CNTN = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getTRN_MEMO_CNTN();
            if(!TextUtils.isEmpty(TRN_MEMO_CNTN)){
                mEtMemoText.setText(TRN_MEMO_CNTN);
            }
        }

        mLayoutDelayServiceNotify.setVisibility(GONE);
        if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
            //글자수 변경

            mEtDispRcvAccText.setHint("7자 이내 입력");
            mEtDispMyAccText.setHint("7자 이내 입력");

            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(8);
            mEtDispRcvAccText.setFilters(FilterArray);
            mEtDispMyAccText.setFilters(FilterArray);


            //오픈뱅킹이면 예약이체및 메모 보이지 않음.이체추가도 할수 없음.
            mLayoutOpenMore.setVisibility(GONE);
            mLayoutAddTransfer.setVisibility(GONE);

            if(mLayoutLine4Info.getVisibility() == VISIBLE){
                mLayoutLine3Info.setVisibility(GONE);
                mLayoutLine4Info.setVisibility(GONE);
                ObjectAnimator.ofFloat(mIvOpenMore, View.ROTATION, 180f, 360f) .setDuration(100) .start();
            }
        }else{

            mEtDispRcvAccText.setHint("10자 이내 입력");
            mEtDispMyAccText.setHint("10자 이내 입력");
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(10);
            mEtDispRcvAccText.setFilters(FilterArray);
            mEtDispMyAccText.setFilters(FilterArray);

            mLayoutOpenMore.setVisibility(VISIBLE);
            //지연이체서비스및 입금지정계좌서비스 가입했으면 이체추가 할수 없음.
            if( LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y") ||
                LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y") ){
                mLayoutAddTransfer.setVisibility(GONE);
                //지연이체서비스 가입되어 있으면
                if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                    mLayoutDelayServiceNotify.setVisibility(VISIBLE);
                    //입력금액과 지연이체 즉시이체가능금액과 비교하여 값을 추가한다.
                    if(ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0).getTRNF_DVCD().equalsIgnoreCase("2")){
                        mTvSendDateText.setText(getContext().getString(R.string.msg_after_3_hour));
                    }else{
                        mTvSendDateText.setText(getContext().getString(R.string.Immediately));
                    }
                }
            }else{
                mLayoutAddTransfer.setVisibility(VISIBLE);
            }
        }

        Animation ani = AnimationUtils.loadAnimation(getContext(), R.anim.sliding_fade_in_transfer_view);
        ani.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        startAnimation(ani);
    }

    /**
     * 이체화면에서 지연이체,지정입금 해제하고 왔을때 이체 추가버튼등.. 업데이트 필요.
     */
    public void updateDetailAccountView(){

        mLayoutDelayServiceNotify.setVisibility(GONE);
        if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){

            //오픈뱅킹이면 예약이체및 메모 보이지 않음.이체추가도 할수 없음.
            mLayoutOpenMore.setVisibility(GONE);
            mLayoutAddTransfer.setVisibility(GONE);

            if(mLayoutLine4Info.getVisibility() == VISIBLE){
                mLayoutLine3Info.setVisibility(GONE);
                mLayoutLine4Info.setVisibility(GONE);
                ObjectAnimator.ofFloat(mIvOpenMore, View.ROTATION, 180f, 360f) .setDuration(100) .start();
            }
        }else{
            mLayoutOpenMore.setVisibility(VISIBLE);
            //지연이체서비스및 입금지정계좌서비스 가입했으면 이체추가 할수 없음.
            if( LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y") ||
                    LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y") ){
                mLayoutAddTransfer.setVisibility(GONE);
            }else{
                mLayoutAddTransfer.setVisibility(VISIBLE);
            }
        }
    }


    public void clearInputText(){
        if(mIvSendDateErase.getVisibility() == VISIBLE){
            mIvSendDateErase.performClick();
        }

        if(ITransferDataMgr.getInstance().getRemitteInfoArrayList().size() > 0){
            ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
            mEtDispRcvAccText.setText(remitteeInfo.getCUST_NM());
            //선택된 계좌가 오픈뱅킹이면 받는 사람 표기 내용을 변경한다.
            if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
                mEtDispMyAccText.setText("사뱅오픈"+ remitteeInfo.getRECV_NM());
            }else{
                mEtDispMyAccText.setText(remitteeInfo.getRECV_NM());
            }

            mEtMemoText.setText("");
        }
    }


    //Activity에서 넣어준다.
    public void setSendDate(String inputDate){
        //취소할경우를 위해 백업해둔다.
        mTvSendDateText.setText(inputDate);
        mIvSendDateDropDown.setVisibility(View.GONE);
        mIvSendDateErase.setVisibility(View.VISIBLE);
    }

    /**
     * ============================================================
     * 이체버튼 클릭후 처리
     */

    /**
     * 1.이체전에 오늘 이체금액 내용을 체크해야 한다.
     */
    public void requestCheckAmount(){
        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);

        //입력한 값이 있을때 넣어준다. - 포커스가 사라질때 모델링에 넣도록 변경.
        if(!TextUtils.isEmpty(mEtDispRcvAccText.getText().toString()))
            remitteeInfo.setDEPO_BNKB_MRK_NM(mEtDispRcvAccText.getText().toString());// 수신통장 표시명(입금계좌표시명)

        if(!TextUtils.isEmpty(mEtDispMyAccText.getText().toString()))
            remitteeInfo.setTRAN_BNKB_MRK_NM(mEtDispMyAccText.getText().toString());// 송신통장 표시명(출금계좌표시명)

        remitteeInfo.setTRN_MEMO_CNTN(mEtMemoText.getText().toString());// 메모


        TransferRequestUtils.requestCheckAmountToday((BaseActivity)getContext(), new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {
                if(result){
                    showConfirmDialogSingleTransfer();
                }else{
                    ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);
                    if(!TextUtils.isEmpty(remitteeInfo.getEROR_MSG_CNTN())){
                        DialogUtil.alert(getContext(),remitteeInfo.getEROR_MSG_CNTN());
                    }
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 2.이체전 확인 다이얼로그를  띄운다.
     */

    private void showConfirmDialogSingleTransfer() {
        MLog.d();
        Logs.e("showConfirmDialogSingleTransfer");

        if(((BaseActivity)getContext()).isFinishing())
            return;

        SlidingConfirmSingleITransferDialog confirmDialog = new SlidingConfirmSingleITransferDialog(
            getContext(),
            new SlidingConfirmSingleITransferDialog.FinishListener() {
                @Override
                public void OnOKListener() {
                    requestUploadTransferInfo();
                }

                @Override
                public void OnCancelListener() {
                    mTvTransferBtn.setEnabled(true);
                }
            }
        );
        confirmDialog.setCancelable(false);
        confirmDialog.show();
    }


    /**
     * 3. 업무서버에 이체목록 등록
     *
     */
    private void requestUploadTransferInfo() {
        MLog.d();

        TransferRequestUtils.requestUploadTransferInfo((BaseActivity)getContext(), new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {
                if(result){
                    checkVoicePhishingState();
                }else{
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 4. 보이스피싱여부를 체크한다.
     */
    private void checkVoicePhishingState(){

        String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        if(WTCH_BANK_CD.equalsIgnoreCase("028")|| WTCH_BANK_CD.equalsIgnoreCase("000")){
            TransferRequestUtils.requestVoicePhising((BaseActivity) getContext(), new HttpSenderTask.HttpRequestListener2() {
                @Override
                public void endHttpRequest(boolean result,String ret) {

                    if(!result){
                        mTvTransferBtn.setEnabled(true);
                        return;
                    }

                    try {
                        JSONObject object = new JSONObject(ret);

                        if (DataUtil.isNotNull(Utils.getJsonString(object, "APRV_YN"))
                                && Utils.getJsonString(object, "APRV_YN").equals(Const.REQUEST_WAS_NO)) {
                            String saveDate = Prefer.getPrefVoicePhishingAskPopupDate(getContext());
                            String curDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
                            if (DataUtil.isNull(saveDate) || (Integer.parseInt(saveDate) < Integer.parseInt(curDate))) {
                                //showAdvanceVoicePhishing();
                                showVoicePhishing();
                            } else {
                                requestElectroSignData();
                            }
                        } else {
                            requestElectroSignData();
                        }
                    } catch (Exception e) {
                        MLog.e(e);
                    }
                }
            });
        }else{
            requestElectroSignData();
        }
    }


    /**
     * 20210408 - 알림팝업 띄우지 않도록 수정.
     * 4-1.보이스피싱 체크 일림 팝업을 띄어준다.
     */
//    private void showAdvanceVoicePhishing() {
//        if (((BaseActivity)getContext()).isFinishing())
//            return;
//        SlidingVoicePhishingDialog dialog = new SlidingVoicePhishingDialog(
//                getContext(),
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        // 이체정보 전자 서명값 요청
//                        requestElectroSignData();
//                    }
//                },
//                new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        showVoicePhishing();
//                    }
//                }
//        );
//        dialog.setCancelable(false);
//        dialog.show();
//    }

    /**
     * 4-2.보이스피싱 문진 팝업을 띄어준다.
     */
    private void showVoicePhishing() {
        if (((BaseActivity)getContext()).isFinishing())
            return;

        final SlidingVoicePhishingPreventAskDialog mVoicePhishingPreventAskDialog = new SlidingVoicePhishingPreventAskDialog(getContext());
        mVoicePhishingPreventAskDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mVoicePhishingPreventAskDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        mVoicePhishingPreventAskDialog.setCancelable(false);
        mVoicePhishingPreventAskDialog.setOnItemListener(new SlidingVoicePhishingPreventAskDialog.OnItemListener() {
            @Override
            public void onConfirm(boolean result) {
                mVoicePhishingPreventAskDialog.dismiss();
                if (result) {
                    // 문진 응답을 모두 "아니요"로 체크한 경우
                    requestElectroSignData();
//                    ITransferDataMgr.getInstance().clearTransferData();
//                    ((BaseActivity)getContext()).finish();
                }else {
                    // 문진 응답 중 하나라고 "예"라고 체크한 경우 팝업을 띄어준다.
                    DialogUtil.alert(getContext(),getContext().getString(R.string.msg_voice_phishing_05), new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ITransferDataMgr.getInstance().clearTransferData();
                            ((BaseActivity)getContext()).finish();
                        }
                    });
                }

            }
        });
        mVoicePhishingPreventAskDialog.show();
    }


    /**
     * 5. 이체 싸이데이타 생
     * 확인 다이얼로그에서 예 클릭시 웹에 올려두었던 이체 정보를 가지고
     * 싸인 데이타를 만들어 내려준다.
     */
    private void requestElectroSignData(){
        TransferRequestUtils.transferSignData((BaseActivity) getContext(), new HttpSenderTask.HttpRequestListener2() {
            @Override
            public void endHttpRequest(boolean result,String ret) {
                if(result){
                    showOTPActivity(ret);
                }else{
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }

    /**
     * 6.mOTP, 타행 OTP, 핀코드 입력창 표시
     *
     * @param signData 전자서명된 이체정보
     */
    private void showOTPActivity(String signData) {
        MLog.d();
        TransferUtils.showOTPActivity((BaseActivity)getContext(),signData);
    }
    /**
     * 인증 메소드가 취소되었을때 호출
     */
    public void cancelAuthMethod(){
        mTvTransferBtn.setEnabled(true);
    }

    public void showHideSoftKeyboard(boolean keyboardShowState){
        if(keyboardShowState){
            mTvTransferBtn.setVisibility(GONE);
        }else{
            mTvTransferBtn.setVisibility(VISIBLE);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mEtDispRcvAccText.clearFocus();
                    mEtDispMyAccText.clearFocus();
                    mEtMemoText.clearFocus();
                }
            },100);
        }
    }



    /**
     * 6.번은 ITransferSendSingleActivity에서 처리한다.
     * 7.이체 요청
     * @param elecSrno 전자서명된 이체정보
     */
    public void requestTransfer(String elecSrno) {
        Map param = new HashMap();
        param.put("ELEC_SGNR_SRNO", elecSrno);
        param.put("DMND_CCNT", ITransferDataMgr.getInstance().getRemitteInfoArraySize());

        ((BaseActivity)getContext()).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0190300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)getContext()).dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    TransferUtils.showFailTransfer((BaseActivity)getContext(),0, Const.EMPTY);
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        TransferUtils.showFailTransfer((BaseActivity)getContext(),0, Const.EMPTY);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);

                        if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
                            //출금계좌가 오픈뱅킹계좌일때 한도초과가 발생하면 금액 입력 화면으로 이동시킨다.
                            if(msg.contains("오픈뱅킹 이체한도가 초과")){
//                                CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
//                                    @Override
//                                    public void onConfirmPress() {
//                                        mActionListener.openInputMoneyView();
//                                    }
//                                };
//                                ((BaseActivity)getContext()).showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, okClick);


                                DialogUtil.alert(getContext(),msg, new OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        mActionListener.openInputMoneyView();
                                        mTvTransferBtn.setEnabled(true);

                                    }
                                });
                                return;
                            }
                        }
                        TransferUtils.showFailTransfer((BaseActivity)getContext(),0, Const.EMPTY);
                        return;
                    }

                    JSONArray array = object.optJSONArray("REC_OUT");
                    if (DataUtil.isNull(array))
                        return;

                    ArrayList<TransferReceiptInfo> listTransferReceiptInfo = new ArrayList<>();

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject jsonObject = array.getJSONObject(index);
                        if (jsonObject == null)
                            continue;
                        listTransferReceiptInfo.add(new TransferReceiptInfo(jsonObject));
                    }


                    String ACCO_ALS = object.optString("ACCO_ALS");
                    String mWithdrawName = Const.EMPTY;

                    if (DataUtil.isNull(ACCO_ALS)) {
                        ACCO_ALS = object.optString("PROD_NM");
                    }
                    String ACNO = ITransferDataMgr.getInstance().getWTCH_ACNO();
                    if (DataUtil.isNotNull(ACNO)) {
                        int lenACNO = ACNO.length();
                        if (lenACNO > 4) {
                            ACNO = ACNO.substring(lenACNO - 4, lenACNO);
                        }
                        ACNO = " [" + ACNO + "]";
                    }
                    if(!TextUtils.isEmpty(ACCO_ALS) && ACCO_ALS.length() > 14){
                        ACCO_ALS = ACCO_ALS.substring(0,14) + Const.ELLIPSIS;
                    }
                    mWithdrawName = ACCO_ALS + ACNO;


                    if (DataUtil.isNotNull(listTransferReceiptInfo) && listTransferReceiptInfo.size() == 1) {
                        String RESP_CD = listTransferReceiptInfo.get(0).getRESP_CD();
                        if ("XEEL0140".equalsIgnoreCase(RESP_CD) || "XEEL0141".equalsIgnoreCase(RESP_CD) ||
                                "XEEL0142".equalsIgnoreCase(RESP_CD) || "XEEL0143".equalsIgnoreCase(RESP_CD) ||
                                "XEKM0089".equalsIgnoreCase(RESP_CD) || "XEKM0091".equalsIgnoreCase(RESP_CD) ||
                                "XEKM0096".equalsIgnoreCase(RESP_CD)) {
                            TransferUtils.showFailTransfer((BaseActivity)getContext(),0, Const.EMPTY);
                            return;
                        }
                    }

                    // 이체완료 페이지로 이동
                    Intent intent = new Intent(getContext(), ITransferCompleteActivity.class);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO, listTransferReceiptInfo);
                    intent.putExtra(Const.INTENT_LIST_TRANSFER_NAME, LoginUserInfo.getInstance().getCUST_NM());
                    intent.putExtra(Const.INTENT_TRANSFER_BALANCE, object.optString("AFTR_BLNC"));
                    intent.putExtra(Const.INTENT_PROD_NAME, object.optString("PROD_NM"));
                    intent.putExtra(Const.INTENT_BANK_NM, DataUtil.isNotNull(object.optString("BANK_NM")) ? object.optString("BANK_NM") : Const.BANK_FULL_NAME);
                    intent.putExtra(Const.INTENT_WITHDRAWABLE_AMOUNT, object.optString("WTCH_TRN_POSB_AMT"));
                    intent.putExtra(Const.INTENT_WITHDRAW_NAME, mWithdrawName);
                    intent.putExtra(Const.INTENT_ACCO_ALS, ACCO_ALS);


                    getContext().startActivity(intent);
                    ((BaseActivity)getContext()).finish();

                } catch (JSONException e) {
                    MLog.e(e);
                    mTvTransferBtn.setEnabled(true);
                }
            }
        });
    }


}
