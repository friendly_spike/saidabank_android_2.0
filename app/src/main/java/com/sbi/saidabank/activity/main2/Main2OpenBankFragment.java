package com.sbi.saidabank.activity.main2;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.brandongogetap.stickyheaders.StickyLayoutManager;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.main2.common.beforeload.CoupleBeforeLoadLayout;
import com.sbi.saidabank.activity.main2.common.beforeload.OpenBankBeforeLoadLayout;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.activity.main2.openbanking.OnOpenBankActionListener;
import com.sbi.saidabank.activity.main2.openbanking.OpenBankAdapter;
import com.sbi.saidabank.activity.main2.openbanking.TopSnappedStickyLayoutManager;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandSelectSenderAccountDialog;
import com.sbi.saidabank.common.dialog.SlidingRevokeOpenBankDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomRecyclerView;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.openbank.CellType;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class Main2OpenBankFragment extends BaseFragment implements OnOpenBankActionListener {
    private Context mContext;

    private ProgressBarLayout mProgressLayout;
    private CustomRecyclerView mListView;
    private OpenBankAdapter mOpenbankAdapter;
    private RelativeLayout  mLayoutEmptyList;

    private OpenBankBeforeLoadLayout mBeforeLoadLayout;
    private int mAsyncRequestStartCnt;
    private int mAsyncRequestFinishCnt;

    //OnResume에서 오픈뱅크 계좌 조회할지 말지.
    private boolean isUpdatePrevent;
    private boolean isPreOneMenthExpand;

    private OpenBankAccountInfo mRevokeAccountInfo;

    public static final Main2OpenBankFragment newInstance() {
        MLog.d();
        return new Main2OpenBankFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        this.mContext = getContext();

    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main2_home_openbank, container, false);

        initLayoutEmpty(rootView);

        mProgressLayout = rootView.findViewById(R.id.ll_progress);

        mOpenbankAdapter = new OpenBankAdapter(this);

        mListView = rootView.findViewById(R.id.recyclerview);
        StickyLayoutManager layoutManager = new TopSnappedStickyLayoutManager(mContext, mOpenbankAdapter);
        //layoutManager.elevateHeaders(true); // Default elevation of 5dp
        // You can also specify a specific dp for elevation
//        layoutManager.elevateHeaders(10);
        mListView.setLayoutManager(layoutManager);
        mListView.setAdapter(mOpenbankAdapter);

        mListView.setOnScrollActionListener(new CustomRecyclerView.OnScrollActionListener() {
            @Override
            public void onScrollStart(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                ((Main2Activity) mContext).changeHomeFragmentScrollState(true);
            }

            @Override
            public void onScrollStop(boolean isBottom) {
                ((Main2Activity) mContext).changeHomeFragmentScrollState(false);
            }

            @Override
            public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

            }
        });

        mBeforeLoadLayout = rootView.findViewById(R.id.layout_before_loaded);

        return rootView;
    }

    private void initLayoutEmpty(View rootView){
        mLayoutEmptyList = rootView.findViewById(R.id.layout_empty_list);

        RelativeLayout layoutAdd = rootView.findViewById(R.id.layout_add);
        layoutAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, WebMainActivity.class);
                String url="";
                //미성년자여부
                String MNRT_YN = LoginUserInfo.getInstance().getMNRT_YN();
                if(MNRT_YN.equalsIgnoreCase("Y")){
                    url = WasServiceUrl.MAI0090300.getServiceUrl();
                }else{
                    //어카운트인포 가입일
                    String ACNF_USE_AGR_DT = LoginUserInfo.getInstance().getACNF_USE_AGR_DT();
                    //어카운트인포 1년 사용 경과여부
                    String ACNF_UUSE_YR1_ELPS_YN = LoginUserInfo.getInstance().getACNF_UUSE_YR1_ELPS_YN();

                    if(TextUtils.isEmpty(ACNF_USE_AGR_DT)||ACNF_UUSE_YR1_ELPS_YN.equalsIgnoreCase("Y")){
                        url = WasServiceUrl.MAI0090100.getServiceUrl();
                    }else{
                        url = WasServiceUrl.MAI0090300.getServiceUrl();
                    }
                }
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                mContext.startActivity(intent);
            }
        });
        int radius = (int) Utils.dpToPixel(getActivity(), 20);
        layoutAdd.setBackground(GraphicUtils.getRoundCornerDrawable("#0D000000", new int[]{ radius, radius, radius, radius }));



        LinearLayout layoutRevoke = rootView.findViewById(R.id.layout_revoke_service);
        layoutRevoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SlidingRevokeOpenBankDialog dialog = new SlidingRevokeOpenBankDialog(mContext, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestElectroSignData();
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public void onResume() {
        MLog.d();
        super.onResume();

        //업데이트 방지
        if(isUpdatePrevent){
            isUpdatePrevent = false;
            return;
        }

        requestOpenBankData();
    }

    @Override
    public void onPause() {
        MLog.d();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        MLog.d();
        super.onDestroy();
        isPreOneMenthExpand=false;
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        mProgressLayout.show();
    }

    @Override
    public void dismissProgress() {
        mProgressLayout.hide();
    }

    public void setPreOneMenthExpand(boolean flag){
        isPreOneMenthExpand = flag;
    }

    /**
     * 오픈뱅킹 계좌 조회
     */
    private void requestOpenBankData(){
        Map param = new HashMap();
        param.put("BLNC_INQ_YN", "N");//우선 잔액조회를 제외한 리스트만 조회해온다.

        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                if(getActivity() == null) return;

                if(((BaseActivity)getActivity()).onCheckHttpError(ret,false)){
                    ((BaseActivity)getActivity()).dismissProgressDialog();
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    String SLRY_TRNF_YN = object.optString("SLRY_TRNF_YN");
                    if(TextUtils.isEmpty(SLRY_TRNF_YN)) SLRY_TRNF_YN = "X";
                    OpenBankDataMgr.getInstance().setSLRY_TRNF_YN(SLRY_TRNF_YN);


                    int accCnt = OpenBankDataMgr.getInstance().parsorAccountList(object);
                    mOpenbankAdapter.makeOpenBankList();

                    if(accCnt > 0){
                        mListView.setVisibility(View.VISIBLE);
                        mListView.scrollTo(0,0);
                        mLayoutEmptyList.setVisibility(View.GONE);
                    }else{
                        mLayoutEmptyList.setVisibility(View.VISIBLE);
                        mListView.setVisibility(View.GONE);
                    }

                    //오픈뱅킹 계좌 만료 1개월 전 체크
                    if(!isPreOneMenthExpand && LoginUserInfo.getInstance().getOBA_RESP_CD().equalsIgnoreCase("1")){
                        Intent intent = new Intent(mContext,WebMainActivity.class);
                        intent.putExtra(Const.INTENT_MAINWEB_URL, WasServiceUrl.MAI0110100.getServiceUrl());
                        getActivity().startActivity(intent);
                        isPreOneMenthExpand = true;
                    }else{
                        mAsyncRequestStartCnt = 0;
                        mAsyncRequestFinishCnt = 0;
                        if(accCnt > 0){
                            ArrayList<CellType> listArray= (ArrayList<CellType>)mOpenbankAdapter.getAdapterData();
                            for(int i = 0;i < listArray.size();i++){
                                CellType cellType = listArray.get(i);
                                if(cellType.getType() == CellType.ITEM_ACC){
                                    requestOpenBankDataByAccount(i,cellType.getGroupPosition(),cellType.getAccountPos());
                                }
                            }
                        }
                    }

                    mBeforeLoadLayout.hideLayout();
                    
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 오픈뱅킹 계좌별로 잔액,출금가능금액 조회
     */
    private void requestOpenBankDataByAccount(final int listIdx,final int grpIndex,final int accIndex){
        mAsyncRequestStartCnt++;
        Map param = new HashMap();

        Logs.e("requestOpenBankDataByAccount list : " + listIdx + " , grpIdx :" + grpIndex + " , accIdx : " + accIndex);

        OpenBankAccountInfo info = mOpenbankAdapter.getAccountInfo(grpIndex,accIndex);
        if(info == null) return;

        param.put("RPRS_FNLT_CD", info.RPRS_FNLT_CD);
        param.put("ACNO", info.ACNO);
        param.put("BLNC_INQ_YN", "Y");

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0140200A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if (DataUtil.isNull(ret)) {
                    occureErrorrequestOpenBankDataByAccount(listIdx,grpIndex,accIndex);
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        occureErrorrequestOpenBankDataByAccount(listIdx,grpIndex,accIndex);
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        occureErrorrequestOpenBankDataByAccount(listIdx,grpIndex,accIndex);
                        return ;
                    }

                    OpenBankAccountInfo info = mOpenbankAdapter.getAccountInfo(grpIndex,accIndex);
                    if(info == null) return;

                    //계좌번호
                    String ACNO = object.optString("ACNO");
                    //계좌잔액
                    String ACCO_BLNC = object.optString("ACCO_BLNC");
                    //출금가능금액
                    String WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");

                    info.BLNC_AMT = ACCO_BLNC;
                    info.WTCH_POSB_AMT = WTCH_POSB_AMT;
                    info.isFinishSearchAmount = true;

                    //오픈뱅킹 데이터 모델에도 업데이트 해준다.
                    OpenBankDataMgr.getInstance().updateAccountAmount(grpIndex,ACNO,ACCO_BLNC,WTCH_POSB_AMT);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
                mAsyncRequestFinishCnt++;
                if(mAsyncRequestStartCnt == mAsyncRequestFinishCnt){
                    mOpenbankAdapter.notifyDataSetChanged();
                }else{
                    mOpenbankAdapter.notifyItemChanged(0);
                    mOpenbankAdapter.notifyItemChanged(listIdx);
                }
            }
        });
    }

    //개별 계좌 조회시 에러발생하면..
    private void occureErrorrequestOpenBankDataByAccount(int listIdx,int grpIndex,int accIndex){
        OpenBankAccountInfo info = mOpenbankAdapter.getAccountInfo(grpIndex,accIndex);
        if(info == null) return;

        if(info.ACNO_REG_STCD.equalsIgnoreCase("2"))
            info.ACNO_REG_STCD = "2";//연결실패는 그냥 연결실패로 나오도록한다.
        else
            info.ACNO_REG_STCD = "3";
        info.isFinishSearchAmount = true;
        info.BLNC_AMT="0";
        info.WTCH_POSB_AMT="0";

        mAsyncRequestFinishCnt++;
        if(mAsyncRequestStartCnt == mAsyncRequestFinishCnt){
            mOpenbankAdapter.notifyDataSetChanged();
        }else{
            mOpenbankAdapter.notifyItemChanged(0);
            mOpenbankAdapter.notifyItemChanged(listIdx);
        }
    }

    /**
     * 오픈뱅킹 서비스 해지
     */
    private void requestOpenBankRevokeService(){
        Map param = new HashMap();
        param.put("CUST_NO", LoginUserInfo.getInstance().getCUST_NO());
        param.put("SVC_DVCD", "");

        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                if(((BaseActivity)getActivity()).onCheckHttpError(ret,false)){
                    ((BaseActivity)getActivity()).dismissProgressDialog();
                    return;
                }

                DialogUtil.alert(mContext, "오픈뱅킹이 해지되었습니다.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //세션 한번 초기화 해주자.
                        requestSyncSession();
                    }
                });
            }
        });
    }

    //계좌 연결해지.
    private void requestOpenBankRevokeConnect(final Activity activity,String RPRS_FNLT_CD,String ACNO){

        Map param = new HashMap();
        param.put("DMND_CCNT",1);


        try {
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("RPRS_FNLT_CD",RPRS_FNLT_CD);
            jsonObject.put("ACNO",ACNO);
            jsonArray.put(jsonObject);
            param.put("REC_IN",jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        ((BaseActivity)activity).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.UNT0530101A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)activity).dismissProgressDialog();

                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
                    return;
                }

                DialogUtil.alert(activity, "오픈뱅킹 계좌 연결이 해지되었습니다.", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestOpenBankList();
                    }
                });
            }
        });

    }



    private void requestSyncSession(){
        showProgress();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010300A06.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                try {
                    if (DataUtil.isNotNull(ret)) {
                        JSONObject object = new JSONObject(ret);
                        if (DataUtil.isNull(object)) {
                            LoginUserInfo.getInstance().setOBA_SVC_ENTR_YN("N");
                            ((Main2Activity) getActivity()).viewPagerUpdate();
                            return;
                        }
                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            LoginUserInfo.getInstance().setOBA_SVC_ENTR_YN("N");
                            ((Main2Activity) getActivity()).viewPagerUpdate();
                            return;
                        }
                        LoginUserInfo.clearInstance();
                        LoginUserInfo.getInstance().syncLoginSession(ret);
                        LoginUserInfo.getInstance().setFakeFinderAllow(true);

                        ((Main2Activity) getActivity()).viewPagerUpdate();
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }

            }
        });
    }



    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            MLog.i("requestCode >> " + requestCode);
            switch (requestCode) {
                // 앨범에서 가져오기
                case Const.REQUEST_SSENSTONE_AUTH_REVOKE_SERVICE:
                    //OnResume업데이트 방지.
                    isUpdatePrevent=true;

                    requestOpenBankRevokeService();
                    break;
                case Const.REQUEST_SSENSTONE_AUTH_REVOKE_CONNECT:
                    //OnResume업데이트 방지.
                    isUpdatePrevent=true;

                    if(mRevokeAccountInfo != null)
                        requestOpenBankRevokeConnect(getActivity(),mRevokeAccountInfo.RPRS_FNLT_CD,mRevokeAccountInfo.ACNO);

                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void requestOpenBankList() {
        requestOpenBankData();
    }

    @Override
    public void revokeAccountInfo(OpenBankAccountInfo accountInfo) {
        mRevokeAccountInfo = accountInfo;
    }

    @Override
    public void requestElectroSignData(){

        Map param = new HashMap();

        ((BaseActivity)getActivity()).showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A05.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                ((BaseActivity)getActivity()).dismissProgressDialog();

                if(((BaseActivity)getActivity()).onCheckHttpError(ret,false)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    String ELEC_SGNR_VAL_CNTN = Utils.getJsonString(object, "ELEC_SGNR_VAL_CNTN");

                    if (DataUtil.isNotNull(ELEC_SGNR_VAL_CNTN)) {
                        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        commonUserInfo.setSignData(ELEC_SGNR_VAL_CNTN);
                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                        Intent intent = new Intent(getActivity(), PincodeAuthActivity.class);
                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "010");//오픈뱅킹 업무코드
                        intent.putExtra(Const.INTENT_TRN_CD, "EIF55016");
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        getActivity().startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH_REVOKE_SERVICE);
                        getActivity().overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);

                    }
                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });

    }
}
