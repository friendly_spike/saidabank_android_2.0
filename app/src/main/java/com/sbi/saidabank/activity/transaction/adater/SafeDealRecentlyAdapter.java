package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;

import java.util.ArrayList;

/**
 * TransferSafeDealRecentlyAdapter : 최근/자주 이체한 계좌번호 어댑터뷰
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class SafeDealRecentlyAdapter extends BaseAdapter {

    private ArrayList<ITransferRecentlyInfo> listAccountInfo;
    private int mType;   // 0 : 계좌, 1 : 휴대폰번호
    private Context mContext;
    private OnItemClickListener mListener;

    public interface OnItemClickListener {
        void OnFavoriteListener(int position);
    }

    public SafeDealRecentlyAdapter(Context context, int type, ArrayList<ITransferRecentlyInfo> listAccountInfo, OnItemClickListener listener) {
        this.mContext = context;
        this.mType = type;
        this.listAccountInfo = listAccountInfo;
        this.mListener = listener;
    }

    @Override
    public int getCount() {
        return (DataUtil.isNotNull(listAccountInfo) && !listAccountInfo.isEmpty()) ? listAccountInfo.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return listAccountInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        SafeDealRecentlyAdapter.AccountViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_transfer_safe_deal_recently_list_item, null);
            viewHolder = new SafeDealRecentlyAdapter.AccountViewHolder();
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textview_name_transfer_account);
            viewHolder.textAlias = (TextView) convertView.findViewById(R.id.textview_alias_transfer_account);
            viewHolder.textBank = (TextView) convertView.findViewById(R.id.textview_bank_transfer_account);
            viewHolder.textAccountNum = (TextView) convertView.findViewById(R.id.textview_account_num_transfer_account);
            viewHolder.layoutFavirte = (RelativeLayout) convertView.findViewById(R.id.layout_favorite_account);
            viewHolder.btnFavirte = (ImageView) convertView.findViewById(R.id.btn_favorite_account);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (SafeDealRecentlyAdapter.AccountViewHolder) convertView.getTag();
        }

        ITransferRecentlyInfo accountInfo = listAccountInfo.get(position);

        if (DataUtil.isNotNull(accountInfo)) {

            // 이름
            viewHolder.textName.setText(DataUtil.isNotNull(accountInfo.getMNRC_ACCO_DEPR_NM()) ? accountInfo.getMNRC_ACCO_DEPR_NM() : Const.EMPTY);

            if (mType == 0) {

                // 별명
                viewHolder.textAlias.setText(DataUtil.isNotNull(accountInfo.getMNRC_ACCO_ALS()) ? accountInfo.getMNRC_ACCO_ALS() : Const.EMPTY);

                // 은행명
                viewHolder.textBank.setText(DataUtil.isNotNull(accountInfo.getMNRC_BANK_NM()) ? accountInfo.getMNRC_BANK_NM() : Const.EMPTY);

                // 계좌번호
                String acno = accountInfo.getMNRC_ACNO();
                if (DataUtil.isNotNull(acno)) {
                    String MNRC_BANK_CD = accountInfo.getMNRC_BANK_CD();
                    String account;
                    if (!TextUtils.isEmpty(MNRC_BANK_CD) && "000".equalsIgnoreCase(MNRC_BANK_CD) || "028".equalsIgnoreCase(MNRC_BANK_CD)) {
                        account = acno.substring(0, 5) + "-" + acno.substring(5, 7) + "-" + acno.substring(7, acno.length());
                    } else {
                        account = acno;
                    }
                    viewHolder.textAccountNum.setText(account);
                }

            } else {
                viewHolder.textAlias.setVisibility(View.GONE);
                viewHolder.textBank.setVisibility(View.GONE);
                viewHolder.textAccountNum.setText(DataUtil.isNotNull(accountInfo.getMNRC_TLNO()) ? accountInfo.getMNRC_TLNO() : Const.EMPTY);
            }

            // 즐겨찾기 여부
            viewHolder.btnFavirte.setImageResource(
                    DataUtil.isNotNull(accountInfo.getFAVO_ACCO_YN())
                            && Const.REQUEST_WAS_YES.equalsIgnoreCase(accountInfo.getFAVO_ACCO_YN())
                            ? R.drawable.btn_favorite_on
                            : R.drawable.btn_favorite_off
            );
            final int pos = position;
            viewHolder.layoutFavirte.setOnClickListener(new OnSingleClickListener() {
                @Override
                public void onSingleClick(View v) {
                    mListener.OnFavoriteListener(pos);
                }
            });
        }

        return convertView;
    }

    public void updateDataList(ArrayList<ITransferRecentlyInfo> listAccountInfo) {
        this.listAccountInfo = listAccountInfo;
        this.notifyDataSetChanged();
    }

    class AccountViewHolder {
        TextView textName;
        TextView textAlias;
        TextView textBank;
        TextView textAccountNum;
        RelativeLayout layoutFavirte;
        ImageView btnFavirte;
    }
}
