package com.sbi.saidabank.activity.main2.openbanking;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.main2.ReArrangePosOpenBankActivity;
import com.sbi.saidabank.activity.setting.ManageAuthWaysActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.TransferSafeDealActivity;
import com.sbi.saidabank.common.dialog.SlidingRevokeOpenBankDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Create by 20210119
 * 오픈뱅킹 리스트중 추가버튼,정렬버튼, 서비스 해지 부분을 출력
 */
public class ViewHolderEtc extends RecyclerView.ViewHolder{

    private Context mContext;
    private RelativeLayout mLayoutAdd;
    private TextView mTvArrange;
    private LinearLayout mLayoutRevoke;

    public static ViewHolderEtc newInstance(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main2_openbank_list_item_etc, parent, false);
        return new ViewHolderEtc(parent.getContext(),itemView);
    }

    public ViewHolderEtc(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;

        mLayoutAdd = itemView.findViewById(R.id.layout_add);
        mTvArrange = itemView.findViewById(R.id.tv_arrange);
        mLayoutRevoke = itemView.findViewById(R.id.layout_revoke_service);

        //더하기 버튼 배경색및 스트록 그리기
        int radius = (int) Utils.dpToPixel(context, 20);
        mLayoutAdd.setBackground(GraphicUtils.getRoundCornerDrawable("#0D000000", new int[]{ radius, radius, radius, radius }));


        //순서변경버튼 배경 그리기
        radius = (int) Utils.dpToPixel(context, 18);
        mTvArrange.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff", new int[]{ radius, radius, radius, radius },1,"#dddddd"));

    }

    public void onBindView(final OnOpenBankActionListener listener) {

        mLayoutAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, WebMainActivity.class);
                String url="";
                //미성년자여부
                String MNRT_YN = LoginUserInfo.getInstance().getMNRT_YN();
                if(MNRT_YN.equalsIgnoreCase("Y")){
                    url = WasServiceUrl.MAI0090300.getServiceUrl();
                }else{
                    //어카운트인포 가입일
                    String ACNF_USE_AGR_DT = LoginUserInfo.getInstance().getACNF_USE_AGR_DT();
                    //어카운트인포 1년 사용 경과여부
                    String ACNF_UUSE_YR1_ELPS_YN = LoginUserInfo.getInstance().getACNF_UUSE_YR1_ELPS_YN();

                    if(TextUtils.isEmpty(ACNF_USE_AGR_DT)||ACNF_UUSE_YR1_ELPS_YN.equalsIgnoreCase("Y")){
                        url = WasServiceUrl.MAI0090100.getServiceUrl();
                    }else{
                        url = WasServiceUrl.MAI0090300.getServiceUrl();
                    }
                }
                intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                mContext.startActivity(intent);
            }
        });

        mTvArrange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ReArrangePosOpenBankActivity.class);
                mContext.startActivity(intent);
            }
        });

        mLayoutRevoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SlidingRevokeOpenBankDialog dialog = new SlidingRevokeOpenBankDialog(mContext, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.requestElectroSignData();
                    }
                });
                dialog.show();

            }
        });
    }

//    private void requestElectroSignData(final Activity activity){
//
//        Map param = new HashMap();
//
//        ((BaseActivity)activity).showProgressDialog();
//
//        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A05.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
//            @Override
//            public void endHttpRequest(String ret) {
//
//                ((BaseActivity)activity).dismissProgressDialog();
//
//                if(((BaseActivity)activity).onCheckHttpError(ret,false)){
//                    return;
//                }
//
//                try {
//                    JSONObject object = new JSONObject(ret);
//
//                    String ELEC_SGNR_VAL_CNTN = Utils.getJsonString(object, "ELEC_SGNR_VAL_CNTN");
//
//                    if (DataUtil.isNotNull(ELEC_SGNR_VAL_CNTN)) {
//                        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.WEB_CALL, EntryPoint.WEB_CALL);
//                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
//                        commonUserInfo.setSignData(ELEC_SGNR_VAL_CNTN);
//                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
//                        Intent intent = new Intent(activity, PincodeAuthActivity.class);
//                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
//                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "010");//오픈뱅킹 업무코드
//                        intent.putExtra(Const.INTENT_TRN_CD, "EIF55016");
//                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
//                        activity.startActivityForResult(intent, Const.REQUEST_SSENSTONE_AUTH_REVOKE_SERVICE);
//                        activity.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
//
//                    }
//                } catch (Exception e) {
//                    MLog.e(e);
//                }
//            }
//        });
//
//    }
}
