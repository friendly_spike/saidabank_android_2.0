package com.sbi.saidabank.activity.transaction.adater;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import java.util.ArrayList;

public class ITransferSelectRcvMyAccountAdapter extends BaseAdapter {
    private static final int ACC_TYPE_SAIDA     = 0;
    private static final int ACC_TYPE_OPENBANK  = 1;

    private Context mContext;
    private ArrayList<MyAccountInfo> mMyAccountArrayList;
    private ArrayList<OpenBankAccountInfo> mOpenBankAccountArrayList;//오픈뱅킹계좌목록
    private ArrayList<MyAccountDispInfo> mDisplayArrayList;

    public ITransferSelectRcvMyAccountAdapter(Context context){
        mContext = context;
        mMyAccountArrayList = new ArrayList<MyAccountInfo>();
        mOpenBankAccountArrayList = new ArrayList<OpenBankAccountInfo>();
        mDisplayArrayList = new ArrayList<MyAccountDispInfo>();
    }

    public ITransferSelectRcvMyAccountAdapter(Context context,ArrayList<MyAccountInfo> myAccountList,ArrayList<OpenBankAccountInfo> openBankList){
        mContext = context;
        mMyAccountArrayList = new ArrayList<MyAccountInfo>();
        mOpenBankAccountArrayList = new ArrayList<OpenBankAccountInfo>();
        mDisplayArrayList = new ArrayList<MyAccountDispInfo>();

        setMyAccOpenBankAccArray(myAccountList,openBankList);
    }

    @Override
    public int getCount() {
        return mDisplayArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDisplayArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mDisplayArrayList.get(position).position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.activity_itransfer_select_receiver_myaccount_list_item, null);
            viewHolder.ivIcon = convertView.findViewById(R.id.iv_icon);
            viewHolder.tvTitle = convertView.findViewById(R.id.tv_title);
            viewHolder.tvSubTitle = convertView.findViewById(R.id.tv_subtitle);
            viewHolder.ivCoupleIcon = convertView.findViewById(R.id.iv_couple_icon);
            viewHolder.ivDelayServiceIcon = convertView.findViewById(R.id.iv_delay_service_icon);
            viewHolder.ivDsgtAccIcon = convertView.findViewById(R.id.iv_dsgt_acc_icon);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        MyAccountDispInfo dispInfo = mDisplayArrayList.get(position);
        if(dispInfo.type == ACC_TYPE_SAIDA) {
            MyAccountInfo info = mMyAccountArrayList.get(dispInfo.position);
            String accoAls = info.getACCO_ALS();
            if (TextUtils.isEmpty(accoAls)) {
                accoAls = info.getPROD_NM();
            }
            viewHolder.tvTitle.setText(accoAls);

            String accountNum = info.getACNO();

            String bankNm = info.getBANK_NM();
            if (!TextUtils.isEmpty(accountNum)) {
                String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
                viewHolder.tvSubTitle.setText(bankNm + " " + account);
            }else{
                viewHolder.tvSubTitle.setText(bankNm + " " + accountNum);
            }

            //은행 아이콘 출력
            Uri iconUri = ImgUtils.getIconUri(mContext,"028");
            if (iconUri != null)
                viewHolder.ivIcon.setImageURI(iconUri);
            else
                viewHolder.ivIcon.setImageResource(R.drawable.img_logo_xxx);

            //커플통장여부
            if(info.getSHRN_ACCO_YN().equalsIgnoreCase("Y")){
                viewHolder.ivCoupleIcon.setVisibility(View.VISIBLE);
            }else{
                viewHolder.ivCoupleIcon.setVisibility(View.GONE);
            }

            //지연이체서비스 가입여부
            if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                viewHolder.ivDelayServiceIcon.setVisibility(View.VISIBLE);
            }else{
                viewHolder.ivDelayServiceIcon.setVisibility(View.GONE);
            }

            //입금지정계좌서비스 가입여부
            if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                viewHolder.ivDsgtAccIcon.setVisibility(View.VISIBLE);
            }else{
                viewHolder.ivDsgtAccIcon.setVisibility(View.GONE);
            }

        }else{
            OpenBankAccountInfo info = mOpenBankAccountArrayList.get(dispInfo.position);

            if(TextUtils.isEmpty(info.ACCO_ALS)){
                viewHolder.tvTitle.setText(info.PROD_NM);
            }else{
                viewHolder.tvTitle.setText(info.ACCO_ALS);
            }

            //오픈뱅킹이어도 SBI계좌일수 있다.
            if(info.RPRS_FNLT_CD.equalsIgnoreCase("000")||info.RPRS_FNLT_CD.equalsIgnoreCase("028")){
                String accountNum = info.ACNO;

                if (!TextUtils.isEmpty(accountNum)) {
                    String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
                    viewHolder.tvSubTitle.setText(info.BANK_NM + " " + account);
                }else{
                    viewHolder.tvSubTitle.setText(info.BANK_NM + " " + accountNum);
                }
            }else{
                viewHolder.tvSubTitle.setText(info.BANK_NM + " " + info.ACNO);
            }

            //은행 아이콘 출력
            Uri iconUri = ImgUtils.getIconUri(mContext,info.RPRS_FNLT_CD,info.DTLS_FNLT_CD);
            if (iconUri != null)
                viewHolder.ivIcon.setImageURI(iconUri);
            else
                viewHolder.ivIcon.setImageResource(R.drawable.img_logo_xxx);

            viewHolder.ivCoupleIcon.setVisibility(View.GONE);
            viewHolder.ivDelayServiceIcon.setVisibility(View.GONE);
            viewHolder.ivDsgtAccIcon.setVisibility(View.GONE);

        }



        return convertView;
    }

    public void setMyAccOpenBankAccArray(ArrayList<MyAccountInfo> myAccountArrayList,ArrayList<OpenBankAccountInfo> openBankAccountArrayList) {
        mMyAccountArrayList.clear();
        mOpenBankAccountArrayList.clear();
        mDisplayArrayList.clear();

        mMyAccountArrayList.addAll(myAccountArrayList);


        for(int i=0;i<mMyAccountArrayList.size();i++){
            MyAccountDispInfo dispInfo = new MyAccountDispInfo();
            dispInfo.type = ACC_TYPE_SAIDA;
            dispInfo.position = i;

            mDisplayArrayList.add(dispInfo);
        }

        //오픈뱅킹 서비스에 가입되어 있으면..
        if(LoginUserInfo.getInstance().getOBA_SVC_ENTR_YN().equalsIgnoreCase("Y")){
            mOpenBankAccountArrayList.addAll(openBankAccountArrayList);
            for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                MyAccountDispInfo dispInfo = new MyAccountDispInfo();
                dispInfo.type = ACC_TYPE_OPENBANK;
                dispInfo.position = i;

                mDisplayArrayList.add(dispInfo);
            }
        }

        notifyDataSetChanged();
    }

    public Object getAccountInfo(int position){
        MyAccountDispInfo dispInfo = mDisplayArrayList.get(position);

        if(dispInfo.type == ACC_TYPE_SAIDA){
            return mMyAccountArrayList.get(dispInfo.position);
        }else{
            return mOpenBankAccountArrayList.get(dispInfo.position);
        }
    }

    /**
     * 어댑터에 표시를 위한 클래스
     */
    private static class MyAccountDispInfo{
        public int type;
        public int position;
    }

    class ViewHolder {
        private ImageView ivIcon; //은행아이콘
        private TextView tvTitle;
        private TextView tvSubTitle;

        private ImageView ivCoupleIcon;      //커플계좌여부
        private ImageView ivDelayServiceIcon;//지연이체서비스
        private ImageView ivDsgtAccIcon;     //입금지정서비스
    }
}
