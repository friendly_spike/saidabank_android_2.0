package com.sbi.saidabank.activity.transaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.common.OtherOtpAuthActivity;
import com.sbi.saidabank.activity.common.OtpPwAuthActivity;
import com.sbi.saidabank.activity.login.IdentificationPrepareActivity;
import com.sbi.saidabank.activity.ssenstone.PincodeAuthActivity;
import com.sbi.saidabank.activity.transaction.safedeal.CustomTitleLayout;
import com.sbi.saidabank.activity.transaction.safedeal.OnSafeDealActionListener;
import com.sbi.saidabank.activity.transaction.safedeal.SlideDealReasonLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideInputMoneyLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideListBankStockLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideBaseLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideListPropertyReasonLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideMyAccountLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideReceiverAccountNumLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideReceiverPhoneNumLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideListContactLayout;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.CommonErrorDialog;
import com.sbi.saidabank.common.dialog.SlidingConfirmSafeDealTransferDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingDialog;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingPreventAskDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.EntryPoint;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.CommonUserInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TransferSafeDealActivity extends BaseActivity implements OnSafeDealActionListener {

    private static final int REQUEST_SEARCH_ADDRESS = 1000;
    private static final int REQUEST_SSENSTONE_AUTH = 2000;
    private static final int REQUEST_OTP_AUTH = 3000;

    // 0.Root view
    private RelativeLayout mLayoutRoot;

    // 1.타이틀하면
    private CustomTitleLayout mCustomTitleLayout;
    // 2.계좌번호 입력화면
    private SlideReceiverAccountNumLayout mSlideReceiverAccountNumLayout;
    // 3.은행,증권,최근/자주 리스트 화면
    private SlideListBankStockLayout mSlideBankStockRecentlyLayout;
    // 4.전화번호 입력화면
    private SlideReceiverPhoneNumLayout mSlideReceiverPhoneNumLayout;
    // 5.전화번호 검색 화면
    private SlideListContactLayout mSlideListContactLayout;
    // 6.나의 계좌 화면
    private SlideMyAccountLayout mSlideMyAccountLayout;
    // 7.보낼금액 화면
    private SlideInputMoneyLayout mSlideInputMoneyLayout;
    // 8.거래사유
    private SlideDealReasonLayout mSlideDealReasonLayout;
    // 9.부동산 거래일때 거래사유 리스트
    private SlideListPropertyReasonLayout mSlideListPropertyReasonLayout;
    // 애니메이션 도중에 터치되는 것을 막기위해 추가한 뷰
    private View mAniProtectView;

    private String[] perList = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private boolean mProtectDoubleClick;
    private boolean mIsGoIdCardVerify;
    private int mSafeDealKind;
    private String mIntentMyAccount;

    private boolean isCreate;
    private Handler mRequestCheckHandler;

    private BroadcastReceiver mNoContactAuthReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            mProtectDoubleClick = true;
            showOTPPinActivity(TransferSafeDealDataMgr.getInstance().getSIGN_DATA());
        }
    };

    /**
     * 타임체크로 시작시의 즐겨찾기와 은행/증권사 코드의 request상태를 확인한다.
     */
    private Runnable mRequestCheckRunnable = new Runnable() {
        @Override
        public void run() {

            Logs.e("TransferSafeDealActivity - Runnable - check request ~~");

            boolean loadRecently = mSlideReceiverAccountNumLayout.getRequestRecentlyFinishState();
            //boolean loadBankStock = mSlideBankStockRecentlyLayout.getRequestFinishState();

            //if (loadRecently && loadBankStock) {
            if (loadRecently) {
                dismissProgressDialog();
            } else {
                mRequestCheckHandler.postDelayed(mRequestCheckRunnable, 500);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MLog.d();
        getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorFFEA0A));
        setContentView(R.layout.activity_transfer_safe_deal);

        mLayoutRoot = findViewById(R.id.layout_root);
        mLayoutRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSlideDealReasonLayout.getVisibility() == View.VISIBLE) {
                    mSlideDealReasonLayout.hideKeypad();
                }

                if (mSlideListContactLayout.getVisibility() == View.VISIBLE) {
                    mSlideListContactLayout.hideKeypad();
                }
            }
        });

        mSafeDealKind = getIntent().getIntExtra("safe_deal_kind", Const.SAFEDEAL_TYPE_MOENY);
        mIntentMyAccount = getIntent().getStringExtra(Const.INTENT_MAIN_TRANSFER_MY_ACCOUNT);

        // 안심이체 종류를 미리 넣어둔다.
        TransferSafeDealDataMgr.getInstance().setSAFE_TRN_KNCD(mSafeDealKind);

        // 슬라이드의 높이에 따라 유동적으로 변경된다.
        mCustomTitleLayout = findViewById(R.id.layout_title);
        mCustomTitleLayout.setSafeDealKind(mSafeDealKind);

        // 계좌번호 입력화면
        mSlideReceiverAccountNumLayout = findViewById(R.id.layout_input_account_num);
        mSlideReceiverAccountNumLayout.setSafeDealKind(mSafeDealKind);

        // 은행,증권,최근/자주 리스트 화면
        mSlideBankStockRecentlyLayout = findViewById(R.id.layout_bank_stock_recently);

        // 전화번호 입력화면
        mSlideReceiverPhoneNumLayout = findViewById(R.id.layout_input_phone_num);

        // 전화번호 검색화면
        mSlideListContactLayout = findViewById(R.id.layout_select_phone_num);

        // 나의 계좌 리스트
        mSlideMyAccountLayout = findViewById(R.id.layout_my_account);

        // 이체금액 입력
        mSlideInputMoneyLayout = findViewById(R.id.layout_input_money);
        mSlideInputMoneyLayout.setSafeDealKind(mSafeDealKind);

        // 거래사유
        mSlideDealReasonLayout = findViewById(R.id.layout_deal_reason);
        mSlideDealReasonLayout.setSafeDealKind(mSafeDealKind);

        // 애니메이션중 다른 클릭을 막기위한 뷰.
        mAniProtectView = findViewById(R.id.v_ani_protect);

        if (mSafeDealKind == Const.SAFEDEAL_TYPE_PROPERTY) {
            // 부동산거래일 경우 거래사유 리스트
            mSlideListPropertyReasonLayout = findViewById(R.id.layout_property_reason);
        }

        isCreate = true;
        mRequestCheckHandler = new Handler();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Const.ACTION_SAFE_DEAL_NOCONTACT_COMPLETE);
        registerReceiver(mNoContactAuthReceiver, intentFilter);

        MLog.i("LCD Height >> " + GraphicUtils.getLCDHeight(this));
        MLog.i("getStatusBarHeight >> " + GraphicUtils.getStatusBarHeight(this));
        MLog.i("getNavigationBarHeight >> " + GraphicUtils.getNavigationBarHeight(this));
        MLog.i("getActivityHeight >> " + GraphicUtils.getActivityHeight(this));
        MLog.i("getNavigationBarHeight DP >> " + Utils.pixelToDp(this, GraphicUtils.getNavigationBarHeight(this)));
    }

    @Override
    protected void onResume() {
        super.onResume();

        //onCreate에서 request작업을 많이 하면 화면이 늦게 나타나는 폰들이 있다.
        //그래서 onResum에서 하도록 한다.한번만 실행된다.
        if (isCreate) {
            isCreate = false;

            showProgressDialog();

            //은행/증권 코드값을 미리 조회한다.-이미 받아둔 코드가 있으면 조회하지 않는다.
            mSlideBankStockRecentlyLayout.getBankStockList();

            //최근/자주 리스트를 조회한다.
            mSlideReceiverAccountNumLayout.requestRecentlyList();
            
            mSlideMyAccountLayout.makeMyAccountList(mSafeDealKind, mIntentMyAccount);

            if (mSafeDealKind == Const.SAFEDEAL_TYPE_PROPERTY) {
                mSlideListPropertyReasonLayout.requestPropertyReasonList();
            }
            //최근/자주 리스트의 조회가 끝난는지 체크하는 Runable이다.
            mRequestCheckHandler.postDelayed(mRequestCheckRunnable, 500);
        }

        if (mIsGoIdCardVerify) {
            mIsGoIdCardVerify = false;
            mProtectDoubleClick = false;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MLog.d();
        unregisterReceiver(mNoContactAuthReceiver);
    }

    @Override
    public void onBackPressed() {
        onActionClose();
    }

    @Override
    public void onActionClose() {
        DialogUtil.alert(TransferSafeDealActivity.this, "다음에 하기", "계속하기", "안심이체를 취소하시겠습니까?", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }, new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public void onActionOpenBank() {
        mSlideBankStockRecentlyLayout.slidingUp(250, 0);
    }

    @Override
    public void onActionSearchPhoneNum() {
        if (PermissionUtils.checkPermission(this, perList, Const.REQUEST_PERMISSION_SYNC_CONTACTS)) {
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS), Const.CONTACTS_INFO_PATH);
            if (file.exists())
                mSlideListContactLayout.getListContacts();
            else
                mSlideListContactLayout.showContactsList();
        }
    }

    @Override
    public void onSearchAddress() {
        Intent intent = new Intent(this, WebMainActivity.class);
        intent.putExtra("url", WasServiceUrl.CMM0040100.getServiceUrl());
        startActivityForResult(intent, REQUEST_SEARCH_ADDRESS);
    }

    @Override
    public void onActionNext(View v, Object obj) {
        if (v instanceof SlideReceiverAccountNumLayout) { //전화번호 화면 올리도록
            mSlideReceiverPhoneNumLayout.setRecieverName(((String) obj));
            mSlideReceiverPhoneNumLayout.slidingUp(250, 250);
        } else if (v instanceof SlideReceiverPhoneNumLayout) {
            String recieverName = mSlideReceiverPhoneNumLayout.getRecieverName();
            String phoneNum = mSlideReceiverPhoneNumLayout.getPhoneNum();
            mCustomTitleLayout.setReceiverInfo(recieverName, phoneNum);

            // 먼저 계좌를 올리고
            mSlideMyAccountLayout.slidingUp(0, 0);
            // 다음으로 전화번호 화면을 내린다.
            mSlideReceiverAccountNumLayout.slidingDown(0, 0);
            mSlideReceiverPhoneNumLayout.slidingDown(0, 0);

        } else if (v instanceof SlideDealReasonLayout) {
            if (mProtectDoubleClick) return;
            mProtectDoubleClick = true;

            // 이체 정보 확인 다이얼로그
            SlidingConfirmSafeDealTransferDialog singleTransferDialog = new SlidingConfirmSafeDealTransferDialog(
                    this,
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestVoicePhising();
//                            requestSignData();
                        }
                    },
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mProtectDoubleClick = false;
                        }
                    }
            );
            singleTransferDialog.show();
        }
    }

    @Override
    public void onActionBack(View v) {
        if (v instanceof SlideReceiverPhoneNumLayout) {
            mSlideReceiverAccountNumLayout.slidingUp(250, 0);
        }
    }

    @Override
    public void onAnimationEnd(View v, SlideBaseLayout.AnimationKind aniKind) {
        if (v instanceof SlideListBankStockLayout ||
                v instanceof SlideListContactLayout ||
                v instanceof SlideListPropertyReasonLayout) {
            return;
        }
        // 슬라이드 업일 경우만 타이틀의 위치를 계산한다.
        if (aniKind == SlideBaseLayout.AnimationKind.ANI_SLIDE_UP) {
            // Slide up에니가 끝났지만 아래 레이어는 타이틀 조절하지 못하도록 한다.
            if (v instanceof SlideInputMoneyLayout || v instanceof SlideDealReasonLayout) {
                return;
            }
            mCustomTitleLayout.changeTitlePosition();
        }
    }


    @SuppressWarnings("ConstantConditions")
    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        MLog.d();
        if (requestCode == Const.REQUEST_PERMISSION_SYNC_CONTACTS) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                mSlideListContactLayout.syncListContacts();
            } else {
                DialogUtil.alert(TransferSafeDealActivity.this,
                        getString(R.string.common_setting),
                        getString(R.string.common_cancel),
                        getString(R.string.msg_permission_read_contacts),
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                PermissionUtils.goAppSettingsActivity(TransferSafeDealActivity.this);
                                finish();
                            }
                        },
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                            }
                        }
                );
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case REQUEST_SEARCH_ADDRESS:
                if (resultCode == RESULT_OK) {
                    if (DataUtil.isNotNull(data)) {
                        String zipCode = data.getStringExtra(Const.INTENT_RESULT_ZIPCODE);
                        String bldgCode = data.getStringExtra(Const.INTENT_RESULT_BLDB_CODE);
                        String address = data.getStringExtra(Const.INTENT_RESULT_ADDRESS);
                        String addressDetail = data.getStringExtra(Const.INTENT_RESULT_ADDRESS_DETAIL);
                        TransferSafeDealDataMgr.getInstance().setLCTN_ZPCD(zipCode);
                        TransferSafeDealDataMgr.getInstance().setLCTN_BLDG_MNGMO(bldgCode);
                        TransferSafeDealDataMgr.getInstance().setLCTN_ADDR(address);
                        TransferSafeDealDataMgr.getInstance().setLCTN_DTL_ADDR(addressDetail);
                        mSlideDealReasonLayout.setPropertyAddress(address + " " + addressDetail);
                    }
                }
                break;

            // Pincode Auth
            case REQUEST_SSENSTONE_AUTH:
                if (resultCode == RESULT_OK) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    if (DataUtil.isNotNull(commonUserInfo)) {
                        if ("P000".equalsIgnoreCase(commonUserInfo.getResultMsg())) {
                            TransferSafeDealDataMgr.getInstance().setELEC_SGNR_SRNO(commonUserInfo.getSignData());
                            //if (Utils.isEnableClickEvent()) {
                            // 핀코드 인증완료 후 이체요청
                            requestSafeDealTransfer();
                            //}
                        } else {
                            Utils.showToast(TransferSafeDealActivity.this, "인증에 실패했습니다.");
//                            if (!TransferManager.getInstance().getIsNotRemove()) {
//                                //requestRemoveTransfer(true);
//                            }
                            mProtectDoubleClick = false;
                        }
                    }
                } else {
                    mProtectDoubleClick = false;
                }
                break;

            case REQUEST_OTP_AUTH:
                if (resultCode == RESULT_OK) {
                    CommonUserInfo commonUserInfo = data.getParcelableExtra(Const.INTENT_COMMON_INFO);
                    String otpAuthTools = data.getStringExtra(Const.INTENT_OTP_AUTH_TOOLS);
                    if (DataUtil.isNotNull(commonUserInfo)
                            && DataUtil.isNotNull(otpAuthTools)
                            && ("3".equalsIgnoreCase(otpAuthTools) || "2".equalsIgnoreCase(otpAuthTools))
                            && data.hasExtra(Const.INTENT_OTP_AUTH_SIGN)) {
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        commonUserInfo.setSignData(data.getStringExtra(Const.INTENT_OTP_AUTH_SIGN));
                        commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getMBR_NO());
                        Intent intent = new Intent(TransferSafeDealActivity.this, PincodeAuthActivity.class);
                        intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                    }
                } else {
                    mProtectDoubleClick = false;
                }
                break;

            default:
                break;
        }
    }

    /**
     * 보이스피싱 체크 일림 팝업을 띄어준다.
     */
    private void showAdvanceVoicePhishing() {
        if (isFinishing())
            return;
        SlidingVoicePhishingDialog dialog = new SlidingVoicePhishingDialog(
                TransferSafeDealActivity.this,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mSafeDealKind == Const.SAFEDEAL_TYPE_PROPERTY) {
                            Double tram = Double.parseDouble(TransferSafeDealDataMgr.getInstance().getTRAM());
                            //if (tram >= Const.SAFE_DEAL_PROPERTY_NOCONTACT_AMOUNT) {
                            if (tram > TransferSafeDealDataMgr.getInstance().getLIMIT_ONE_TIME()) {
                                mIsGoIdCardVerify = true;
                                CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER_SAFEDEAL, EntryPoint.TRANSFER_SAFEDEAL);
                                commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getCUST_NO());
                                Intent intent = new Intent(TransferSafeDealActivity.this, IdentificationPrepareActivity.class);
                                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                                startActivity(intent);
                                return;
                            }
                        }
                        requestSignData();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showVoicePhishing();
                    }
                }
        );
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * 보이스피싱 문진 팝업을 띄어준다.
     */
    private void showVoicePhishing() {
        if (isFinishing())
            return;
        final SlidingVoicePhishingPreventAskDialog dialog = new SlidingVoicePhishingPreventAskDialog(TransferSafeDealActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.setCancelable(false);
        dialog.setOnItemListener(new SlidingVoicePhishingPreventAskDialog.OnItemListener() {
            @Override
            public void onConfirm(boolean result) {
                // 문진 응답을 모두 "아니요"로 체크한 경우
                if (result) {
                    finish();
                }
                // 문진 응답 중 하나라고 "예"라고 체크한 경우 팝업을 띄어준다.
                else {
                    AlertDialog alertDialog = new AlertDialog(TransferSafeDealActivity.this);
                    alertDialog.mPListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            finish();
                        }
                    };
                    alertDialog.msg = getString(R.string.msg_voice_phishing_05);
                    alertDialog.show();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    /**
     * mOTP, 타행 OTP, 핀코드 입력창 표시
     *
     * @param signData 전자서명된 이체정보
     */
    private void showOTPPinActivity(String signData) {
        MLog.d();
        CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER, EntryPoint.TRANSFER);
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String SECU_MEDI_DVCD = loginUserInfo.getSECU_MEDI_DVCD();
        if (DataUtil.isNull(SECU_MEDI_DVCD)) {
            commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
            commonUserInfo.setSignData(signData);
            commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
            Intent intent = new Intent(TransferSafeDealActivity.this, PincodeAuthActivity.class);
            intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
            intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
            intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        } else {
            Intent intent;
            if ("2".equalsIgnoreCase(SECU_MEDI_DVCD)) {

                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = Double.parseDouble(TransferSafeDealDataMgr.getInstance().getTRAM());

                    if (totalAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(TransferSafeDealActivity.this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }

                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_OTP);

                intent = new Intent(TransferSafeDealActivity.this, OtherOtpAuthActivity.class);
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else if ("3".equalsIgnoreCase(SECU_MEDI_DVCD)) {
                String SECU_MEDI_USE_BZWR_DVCD = loginUserInfo.getSECU_MEDI_USE_BZWR_DVCD();
                if ("1".equalsIgnoreCase(SECU_MEDI_USE_BZWR_DVCD)) {
                    double totalAmount = Double.parseDouble(TransferSafeDealDataMgr.getInstance().getTRAM());
                    if (totalAmount <= Const.SHOW_MAX_OTP_AMOUNT) {
                        Intent intentPin = new Intent(TransferSafeDealActivity.this, PincodeAuthActivity.class);
                        commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                        //sign data, service id 추가
                        commonUserInfo.setSignData(signData);
                        intentPin.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                        intentPin.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                        intentPin.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                        commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                        intentPin.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                        startActivityForResult(intentPin, REQUEST_SSENSTONE_AUTH);
                        overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                        return;
                    }
                }


                intent = new Intent(TransferSafeDealActivity.this, OtpPwAuthActivity.class);
                commonUserInfo.setOtpPWEntryType(Const.OTP_PW_ENTRY_NATIVE);
                commonUserInfo.setOtpPWAccessType(Const.OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL);

                intent.putExtra(Const.INTENT_OTP_SERIAL_NUMBER, TransferSafeDealDataMgr.getInstance().getOTP_SERIAL_NO());
                intent.putExtra(Const.INTENT_OTP_TA_VERSION, TransferSafeDealDataMgr.getInstance().getOTP_TA_VERSION());
                intent.putExtra(Const.INTENT_OTP_AUTH_TOOLS, SECU_MEDI_DVCD);
                intent.putExtra(Const.INTENT_OTP_AUTH_SIGN, signData);
            } else {
                intent = new Intent(TransferSafeDealActivity.this, PincodeAuthActivity.class);
                commonUserInfo.setOperation(Const.SSENSTONE_AUTHENTICATE);
                //sign data, service id 추가
                commonUserInfo.setSignData(signData);
                intent.putExtra(Const.INTENT_SERVICE_ID, Const.EMPTY);
                intent.putExtra(Const.INTENT_BIZ_DV_CD, "003");
                intent.putExtra(Const.INTENT_TRN_CD, "EFN50042");
                commonUserInfo.setMBRnumber(loginUserInfo.getMBR_NO());
                intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                startActivityForResult(intent, REQUEST_SSENSTONE_AUTH);
                overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                return;
            }

            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
            startActivityForResult(intent, REQUEST_OTP_AUTH);
            overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
        }
    }

    /**
     * 보이스피싱 문진 요청
     */
    @SuppressLint("SimpleDateFormat")
    private void requestVoicePhising() {
        MLog.d();

        Map param = new HashMap();
        param.put("TRNF_DVCD", 2);  // (1=일반이체, 2=안심이체)
        param.put("DMND_CCNT", 1);
        param.put("REP_CNT", 1);

        try {
            JSONArray array = new JSONArray();
            JSONObject object = new JSONObject();
            object.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());                  // 출금 계좌번호
            object.put("TRN_AMT", TransferSafeDealDataMgr.getInstance().getTRAM());                         // 이체금액
            object.put("CNTP_FIN_INST_CD", TransferSafeDealDataMgr.getInstance().getMNRC_BANK_CD());        // 입금대상 금융기관코드
            object.put("CNTP_BANK_ACNO", TransferSafeDealDataMgr.getInstance().getMNRC_ACNO());             // 입금대상 계좌번호
            object.put("CNTP_ACCO_DEPR_NM", TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM());  // 입금대상 예금주명
            array.put(object);
            param.put("REC", array);
        } catch (JSONException e) { }

        MLog.i("param >> " + param.toString());

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A04.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response));
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);
                    if (DataUtil.isNull(object)) {
                        showErrorMessage(getString(R.string.msg_debug_err_response));
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (DataUtil.isNull(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true);
                        return;
                    }

                    if (DataUtil.isNotNull(Utils.getJsonString(object, "APRV_YN"))
                            && Utils.getJsonString(object, "APRV_YN").equals(Const.REQUEST_WAS_NO)) {
                        String saveDate = Prefer.getPrefVoicePhishingAskPopupDate(TransferSafeDealActivity.this);
                        String curDate = new SimpleDateFormat("yyyyMMdd").format(new Date());
                        if (DataUtil.isNull(saveDate) || (Integer.parseInt(saveDate) < Integer.parseInt(curDate))) {
                            showAdvanceVoicePhishing();
                        } else {
                            requestSignData();
                        }
                    } else {
                        requestSignData();
                    }

                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

    //========================================================================
    // 이체 최종 진입단계 함수들...
    //========================================================================
    public void requestSignData() {
        MLog.d();

        Map param = new HashMap();
        param.put("TRN_TYCD", "3");//3번이 안심이체이다.
        param.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());//출금계좌번호
        param.put("MNRC_TRGT_DEPR_NM", TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM());//입금대상예금주명
        param.put("MNRC_CPNO", TransferSafeDealDataMgr.getInstance().getMNRC_CPNO());//입금휴대전화번호
        param.put("TRAM", TransferSafeDealDataMgr.getInstance().getTRAM());//이체금액
        param.put("MNRC_ACCO_CNTN", LoginUserInfo.getInstance().getCUST_NM());//입금계좌표시내용(내이름 넣어준다)
        param.put("TRNF_FEE", TransferSafeDealDataMgr.getInstance().getTRNF_FEE());
        param.put("MNRC_BANK_CD", TransferSafeDealDataMgr.getInstance().getMNRC_BANK_CD());
        param.put("MNRC_ACNO", TransferSafeDealDataMgr.getInstance().getMNRC_ACNO());
        param.put("SAFE_TRN_KNCD", TransferSafeDealDataMgr.getInstance().getSAFE_TRN_KNCD());
        param.put("SAFE_TRN_RSCD", TransferSafeDealDataMgr.getInstance().getSAFE_TRN_RSCD());

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0019900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if (onCheckHttpError(ret, false)) {
                    mProtectDoubleClick = false;
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    String signData = object.optString("ELEC_SGNR_VAL_CNTN");
                    TransferSafeDealDataMgr.getInstance().setSIGN_DATA(signData);

                    int SAFE_TRN_KNCD = TransferSafeDealDataMgr.getInstance().getSAFE_TRN_KNCD();
                    Double tram = Double.parseDouble(TransferSafeDealDataMgr.getInstance().getTRAM());
                    Double transOneTime = TransferSafeDealDataMgr.getInstance().getLIMIT_ONE_TIME();

                    if (DataUtil.isNotNull(signData)) {
                        // 500만원보다 낮아서 보이스피싱은 안나오고 부동산거래이면서 1회이체금액을 초과할때 비대면 나타나도록.
                        if (SAFE_TRN_KNCD == 2 && tram > transOneTime) {
                            mIsGoIdCardVerify = true;
                            CommonUserInfo commonUserInfo = new CommonUserInfo(EntryPoint.TRANSFER_SAFEDEAL, EntryPoint.TRANSFER_SAFEDEAL);
                            commonUserInfo.setMBRnumber(LoginUserInfo.getInstance().getCUST_NO());
                            Intent intent = new Intent(TransferSafeDealActivity.this, IdentificationPrepareActivity.class);
                            intent.putExtra(Const.INTENT_COMMON_INFO, commonUserInfo);
                            startActivity(intent);
                        } else {
                            // mOTP, 타행 OTP, 핀코드 입력창 표시
                            showOTPPinActivity(signData);
                        }
                    }

                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 최종 이체 실행
     */
    private void requestSafeDealTransfer() {
        MLog.d();

        Map param = new HashMap();
        param.put("WTCH_ACNO", TransferSafeDealDataMgr.getInstance().getWTCH_ACNO());
        param.put("MNRC_ACCO_DEPR_NM", TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM());
        param.put("MNRC_CPNO", TransferSafeDealDataMgr.getInstance().getMNRC_CPNO());
        param.put("MNRC_BANK_CD", TransferSafeDealDataMgr.getInstance().getMNRC_BANK_CD());
        param.put("MNRC_ACNO", TransferSafeDealDataMgr.getInstance().getMNRC_ACNO());
        param.put("TRAM", TransferSafeDealDataMgr.getInstance().getTRAM());
        param.put("TRNF_FEE", TransferSafeDealDataMgr.getInstance().getTRNF_FEE());
        param.put("WTCH_ACCO_MRK_CNTN", TransferSafeDealDataMgr.getInstance().getWTCH_ACCO_MRK_CNTN());
        param.put("MNRC_ACCO_MRK_CNTN", TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_MRK_CNTN());
        param.put("SAFE_TRN_KNCD", TransferSafeDealDataMgr.getInstance().getSAFE_TRN_KNCD());
        param.put("SAFE_TRN_RSCD", TransferSafeDealDataMgr.getInstance().getSAFE_TRN_RSCD());
        param.put("SAFE_TRN_RSN_CNTN", TransferSafeDealDataMgr.getInstance().getSAFE_TRN_RSN_CNTN());
        param.put("LCTN_ZPCD", TransferSafeDealDataMgr.getInstance().getLCTN_ZPCD());
        param.put("LCTN_BLDG_MNGNO", TransferSafeDealDataMgr.getInstance().getLCTN_BLDG_MNGMO());
        param.put("LCTN_ADDR", TransferSafeDealDataMgr.getInstance().getLCTN_ADDR());
        param.put("LCTN_DTL_ADDR", TransferSafeDealDataMgr.getInstance().getLCTN_DTL_ADDR());
        param.put("ELEC_SGNR_SRNO", TransferSafeDealDataMgr.getInstance().getELEC_SGNR_SRNO());

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.TRA0090100A03.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if (DataUtil.isNull(ret)) {
                    showErrorMessage(getString(R.string.msg_debug_no_response_transfer), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showFailTransfer(0, Const.EMPTY);
                        }
                    });
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {

                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                        if (TextUtils.isEmpty(msg))
                            msg = getString(R.string.common_msg_no_reponse_value_was);
                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);

                        //이상금융거래 에러 차단.
                        if ("EEFN0306".equalsIgnoreCase(errCode) ||
                                "EEFN0307".equalsIgnoreCase(errCode) ||
                                "EEIF0516".equalsIgnoreCase(errCode)) {
                            Intent intent = new Intent(TransferSafeDealActivity.this, WebMainActivity.class);
                            String url = WasServiceUrl.ERR0030100.getServiceUrl();
                            intent.putExtra(Const.INTENT_MAINWEB_URL, url);

                            String param = Const.REQUEST_COMMON_RESP_CD + "=" + errCode +
                                    "&" + Const.REQUEST_COMMON_RESP_CNTN + "=" + msg;
                            intent.putExtra(Const.INTENT_PARAM, param);

                            startActivity(intent);
                            finish();
                            return;
                        }

                        final String retStr = ret;
                        CommonErrorDialog.OnConfirmListener okClick = new CommonErrorDialog.OnConfirmListener() {
                            @Override
                            public void onConfirmPress() {
                                showFailTransfer(0, retStr);
                            }
                        };
                        showCommonErrorDialog(msg, errCode, Const.EMPTY, objectHead, true, okClick);
                        return;
                    }

                    TransferSafeDealDataMgr.getInstance().setRET_COMPLET_STR(ret);


                    // 이체완료 페이지로 이동
                    Intent intent = new Intent(TransferSafeDealActivity.this, TransferSafeDealCompleteActivity.class);
                    startActivity(intent);
                    finish();


                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 이체실패에 따른 메세지 화면 표시
     *
     * @param type 오류 타입
     * @param ret  오류 메시지
     */
    private void showFailTransfer(int type, String ret) {
        MLog.d();
        Intent intent = new Intent(TransferSafeDealActivity.this, ITransferFailActivity.class);
        intent.putExtra(Const.INTENT_TRANSFER_ENTRY_TYPE, 3);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_TYPE, type);
        intent.putExtra(Const.INTENT_TRANSFER_FAIL_RET, ret);
        startActivity(intent);
        finish();
    }

    /**
     * 각종 애니메이션 중에 다른 뷰들을 클릭하게 되면 애니메이션이 꼬이게 되어 화면이 깨지는 문제가 발생한다.
     * 그래서 어떤 애니메이션이든 애니메이션 중에는 터치를 못하도록 방어코드를 추가한다.
     */
    private Handler mHandler;
    private ArrayList<String> mAniTimeSquency;

    public void showAniProtectView(boolean isShow, long keyMillis, String type) {
        if (mAniTimeSquency == null) {
            mAniTimeSquency = new ArrayList<String>();
        }

        if (mHandler == null) {
            mHandler = new Handler();
        }

        Logs.e("showAniProtectView - isShow : " + isShow + " , millis : " + keyMillis + " , type : " + type);
        if (isShow) {
            mAniTimeSquency.add(String.valueOf(keyMillis));
            if (mAniProtectView.getVisibility() != View.VISIBLE) {
                mAniProtectView.setVisibility(View.VISIBLE);
            }
        } else {
            mAniTimeSquency.remove(String.valueOf(keyMillis));
            if (mAniTimeSquency.size() == 0)
                mAniProtectView.setVisibility(View.GONE);
        }

        mHandler.removeCallbacks(mAnimationCheckRunnable);
        mHandler.postDelayed(mAnimationCheckRunnable, 500);
    }

    //애니메이션 보호 뷰 해제하기.
    private Runnable mAnimationCheckRunnable = new Runnable() {
        @Override
        public void run() {

            Logs.e("TransferSafeDealActivity - mAnimationCheckRunnable");
            mAniTimeSquency.clear();
            mAniProtectView.setVisibility(View.GONE);
        }
    };

    /*거래사유 입력에서 입력필드에 포커스가 생기면 전체 뷰를 위로 올린다.
     * 입력필드를 가리기 때문에...
     */
    public void moveUpRootView(boolean isUp) {
        if (isUp) {
            int pixel = (int) Utils.dpToPixel(this, 46f);
            mLayoutRoot.animate()
                    .translationY(-1 * pixel)
                    .setDuration(250)
                    .start();
        } else {
            mLayoutRoot.animate()
                    .translationY(0)
                    .setDuration(250)
                    .start();
        }
    }
}
