package com.sbi.saidabank.activity.main2.common.progress;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;

public class ProgressBarLayout extends RelativeLayout {

    private ImageView mLoadingImageView;

    public ProgressBarLayout(Context context) {
        super(context);
        initUX(context);
    }

    public ProgressBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return true;
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.fragment_main2_progress, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLoadingImageView = layout.findViewById(R.id.img_progress);
        setVisibility(GONE);
    }

    public void show() {
        if (getVisibility() == VISIBLE) return;
        setVisibility(VISIBLE);
        setAlpha(0);
        animate()
                .alpha(1)
                .setDuration(150)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        // TODO Auto-generated method stub
                    }
                });
        AnimationDrawable drawable = (AnimationDrawable) mLoadingImageView.getBackground();
        if (DataUtil.isNotNull(drawable)) {
            drawable.start();
        }
    }

    public void hide() {
        if (getVisibility() != VISIBLE) return;
        AnimationDrawable drawable = (AnimationDrawable) mLoadingImageView.getBackground();
        if (DataUtil.isNotNull(drawable)) {
            drawable.stop();
        }
        animate()
                .alpha(0)
                .setDuration(150)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        setVisibility(GONE);
                    }
                });
    }
}
