package com.sbi.saidabank.activity.certification;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.initech.xsafe.cert.CertInfo;
import com.initech.xsafe.cert.KeyPadCipher;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.ItemTouchHelperViewHolder;
import com.sbi.saidabank.common.dialog.SlidingCertDeleteDialog;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.common.CertItemInfo;
import com.sbi.saidabank.solution.cert.CertificateInfo;
import com.sbi.saidabank.solution.cert.CustomCertManager;
import com.sbi.saidabank.solution.cert.CustomRow;
import com.sbi.saidabank.solution.cert.NetResult;
import com.sbi.saidabank.solution.cert.NetServerConnector;
import com.softsecurity.transkey.TransKeyActivity;

import java.lang.ref.WeakReference;
import java.math.BigInteger;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Saidabank_android
 * Class: CertExportActivity
 * Created by 950469 on 2018. 9. 10..
 * <p>
 * Description:
 * 인증서 리스트 출력하는 함수.
 * 호출하는 목적에 따라 사용방법이 다르다.
 * CERT_TYPE_SCRAPING    스크래핑할때
 * CERT_TYPE_MANAGER     인증서 관리 할때
 */
public class CertListActivity extends BaseActivity implements View.OnClickListener{
    private static final int REQUEST_TRANSKEY    = 100;
    private static final int REQUEST_EXPORT      = 200;
    private static final int REQUEST_IMPORT      = 300;

    private static final long MIN_CLICK_INTERVAL = 800;    // ms

    private Context mContext;
    private long mLastClickTime;

    private ArrayList<CustomRow> mCertListArray;
    private CertListAdapter mCertListAdapter;
    private Const.CertJobType mJobType;
    private String mIdData;
    private String mUserName;
    private String mServiceData;
    private boolean mIsCertOnly;
    private int mCertIndex;
    private RelativeLayout mLayoutEmptyCert;
    private RecyclerView mListView;

    private CertListActivity.ExportCertTaskV12 exportTask;

    //필요권한 - 메모리접근권한
    private String[] perList = new String[]{
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cert_list);

        mContext = this;
        Intent intent = getIntent();
        mJobType = (Const.CertJobType) intent.getSerializableExtra(Const.INTENT_CERT_PURPOSE);
        mIdData = intent.getStringExtra(Const.INTENT_CERT_IDDATA);
        mServiceData = intent.getStringExtra(Const.INTENT_CERT_SERVICEDATA);
        mIsCertOnly = intent.getBooleanExtra(Const.INTENT_CERT_NO_SCRAPING, false);
        if(intent.hasExtra(Const.INTENT_CERT_USERNAME))
            mUserName = intent.getStringExtra(Const.INTENT_CERT_USERNAME);

        mLayoutEmptyCert = findViewById(R.id.ll_emptycert);

        mCertListArray = new ArrayList<CustomRow>();
        mCertListAdapter = new CertListAdapter();

        mListView = (RecyclerView) findViewById(R.id.recyclerview_cert_list);
        mListView.setAdapter(mCertListAdapter);
        mListView.setLayoutManager(new LinearLayoutManager(this));
        mListView.setVerticalScrollBarEnabled(true);

        findViewById(R.id.btn_cancel_cert_list).setOnClickListener(this);
        Button importBtn = findViewById(R.id.btn_import_cert);

        if (mJobType == Const.CertJobType.MANAGER || mJobType == Const.CertJobType.SCRAPING) {
            importBtn.setOnClickListener(this);
        } else {
            importBtn.setVisibility(View.GONE);
        }

        if (PermissionUtils.checkPermission(this, perList, Const.REQUEST_PERMISSION_EXTERNAL_STORAGE)) {
            // 인증서 목록 출력
            loadCertList();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        cancelExportTask();
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    public void onRequestPermissionsResult(int requestCode, @Nullable String[] permissions, @Nullable int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Logs.e("onRequestPermissionsResult");

        boolean isPerDeny = false;
        if (requestCode == Const.REQUEST_PERMISSION_EXTERNAL_STORAGE) {
            for (int i = 0; i < permissions.length; i++) {
                Logs.i("permissions." + i + "." + permissions[i] + " : " + grantResults[i]);
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    int msgId;

                    boolean state = ActivityCompat.shouldShowRequestPermissionRationale(this, permissions[i]);

                    if (state) {
                        msgId = R.string.cert_message_permission_deny;
                    } else {
                        msgId = R.string.cert_message_permission_deny_app_setting;
                    }

                    DialogUtil.alert(this, msgId, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            setResult(RESULT_CANCELED, null);
                            finish();
                        }
                    });
                    isPerDeny = true;
                    break;
                }
            }

            if (!isPerDeny) {
                // 인증서 목록 출력
                loadCertList();
            }
        }
    }

    /**
     * 인증서 리스트를 구성한다.
     */
    private void loadCertList() {
        mCertIndex = -1;
        mCertListArray.clear();

        String[] sCertList = CustomCertManager.getInstance().loadCertList();
        //Logs.i("sCertList : " + sCertList.length);

        if (sCertList == null) {
            mLayoutEmptyCert.setVisibility(View.VISIBLE);

        } else {
            for (String aSCertList : sCertList) {
                if(mUserName != null && mJobType == Const.CertJobType.SCRAPING) {
                    CertificateInfo mCertificateInfo = new CertificateInfo(aSCertList);
                    String certName = mCertificateInfo.getUser().replaceAll("[^가-힣]", "");
                    if(mUserName.equals(certName)){
                        mCertListArray.add(new CustomRow(mCertificateInfo));
                    }
                } else {
                    mCertListArray.add(new CustomRow(new CertificateInfo(aSCertList)));
                }
            }
            if( mCertListArray.size() <= 0 && mJobType == Const.CertJobType.SCRAPING){
                //mLayoutEmptyCert.setText(getResources().getText(R.string.cert_message_error_str_no_match_cert));
                mLayoutEmptyCert.setVisibility(View.VISIBLE);
            }
            Logs.i("mCertListArray : " + mCertListArray.size());
            mCertListAdapter.notifyDataSetChanged();
        }
    }

    /**
     * 인증서 선택하면 보안키패드 입력화면으로 이동한다.
     *
     * @param idx 선택한 인증서의 인덱스
     */
    private void onClickChoiceActivity(int idx) {
        if(idx < 0){
            DialogUtil.alert(this, "인증서 인덱스 오류입니다.\n잠시 후 다시 시도해주세요.", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            return;
        }

        int keyType = TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER;

        Intent intentKey = new Intent(this, CertAuthPWActivity.class);
        intentKey.putExtra(Const.INTENT_TRANSKEY_PADTYPE, keyType);
        intentKey.putExtra(Const.INTENT_TRANSKEY_TITLE, "");
        intentKey.putExtra(Const.INTENT_TRANSKEY_LABEL1, getResources().getString(R.string.txt_cert_input_password));
        intentKey.putExtra(Const.INTENT_TRANSKEY_MAXLENGTH, 30);
        intentKey.putExtra(Const.INTENT_TRANSKEY_MINLENGTH, 2);
        intentKey.putExtra(Const.INTENT_CERT_INDEX, idx);
        this.startActivityForResult(intentKey, REQUEST_TRANSKEY);
        this.overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_TRANSKEY: {
                if (resultCode == RESULT_OK) {
                    if (mJobType == Const.CertJobType.SCRAPING) {
                        String certPassword = data.getStringExtra(Const.INTENT_TRANSKEY_PLAIN_DATA1);

                        final CertInfo certInfo = (CertInfo) CustomCertManager.getInstance().getCertStorageInfo(mCertIndex);
                        //Firebase에러 - 인증서 인덱스 오류 수정
                        if (certInfo == null) {
                            return;
                        }

                        String certPath = certInfo.certPath;
                        Logs.i("certInfo.certPath:" + certPath);
                        if (certPath.contains(".der")) {
                            int pos = certPath.lastIndexOf("/");
                            if (pos > 0) {
                                certPath = certPath.substring(0, pos);
                            }
                        }

                        CustomRow customRow = null;
                        for (int index = 0; index < mCertListArray.size(); index++) {
                            CustomRow row = mCertListArray.get(index);
                            if (row == null)
                                continue;

                            if (mCertIndex == row.getIndex()) {
                                customRow = row;
                                break;
                            }
                        }

                        CertItemInfo certItemInfo = createCertItemInfo(customRow);
                        if (certItemInfo == null) {
                            return;
                        }

                        Logs.i("certInfo.certPath:" + certPath);

                        Intent intent = new Intent();
                        intent.putExtra(Const.INTENT_CERT_ISSUER_DN, certItemInfo.getIssuer());
                        intent.putExtra(Const.INTENT_CERT_SUBJECT_DN, certItemInfo.getCertCN());
                        if (mIsCertOnly) {
                            intent.putExtra(Const.INTENT_CERT_NO_SCRAPING, true);
                        } else {
                            intent.putExtra(Const.INTENT_CERT_NO_SCRAPING, false);
                            intent.putExtra(Const.INTENT_CERT_PATH, certPath);
                            intent.putExtra(Const.INTENT_CERT_PASSWORD, certPassword);
                            if (!TextUtils.isEmpty(mIdData))
                                intent.putExtra(Const.INTENT_CERT_IDDATA, mIdData);
                            else
                                intent.putExtra(Const.INTENT_CERT_IDDATA, "");
                            if (!TextUtils.isEmpty(mServiceData))
                                intent.putExtra(Const.INTENT_CERT_SERVICEDATA, mServiceData);
                            else
                                intent.putExtra(Const.INTENT_CERT_SERVICEDATA, "");
                        }

                        setResult(RESULT_OK, intent);
                        finish();
                    } else if (mJobType == Const.CertJobType.EXPORT ||
                            mJobType == Const.CertJobType.MANAGER) {
                        String passChiper = data.getStringExtra(Const.INTENT_TRANSKEY_CERT_CHIPER_DATA);
                        Logs.i("passChiper : " + passChiper);
                        startExportTask(passChiper);
                    }
                }
                break;
            }

            case REQUEST_EXPORT: {
                if (resultCode == RESULT_OK) {
                    finish();
                } else {
                    mCertIndex = -1;
                }
                break;
            }

            case REQUEST_IMPORT: {
                loadCertList();
                break;
            }

            default:
                break;
        }
    }

    private void listItemClick(int position) {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복클릭 방지
            return;
        }

        if (mJobType == Const.CertJobType.SCRAPING) {
            CustomRow row = mCertListArray.get(position);
            if (row.isExpired()) {
                return;
            }

            mCertIndex = row.getIndex();

            onClickChoiceActivity(mCertIndex);
        } else if (mJobType == Const.CertJobType.MANAGER) {
            Intent intent = new Intent(mContext, CertDetailActivity.class);
            if (mCertListArray == null && mCertListArray.size() <= 0)
                return;

            CustomRow row = mCertListArray.get(position);
            mCertIndex = row.getIndex();
            CertItemInfo certItemInfo = createCertItemInfo(row);
            if (certItemInfo == null)
                return;

            Bundle bundle = new Bundle();
            bundle.putParcelable(Const.INTENT_CERT_DATA, certItemInfo);
            intent.putExtras(bundle);
            mContext.startActivity(intent);

        } else if (mJobType == Const.CertJobType.EXPORT) {
            if (mCertListArray.get(position).isExpired()) {
                return;
            }

            CustomRow row = mCertListArray.get(position);
            mCertIndex = row.getIndex();
            onClickChoiceActivity(mCertIndex);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_cancel_cert_list) {
            finish();
        } else if (v.getId() == R.id.btn_import_cert) {
            Intent intent = new Intent(mContext, CertImportActivity.class);
            startActivityForResult(intent, REQUEST_IMPORT);
        }
    }

    private String getFormedUserNamne(String user, String issuer) {
        StringBuilder strbuilder = new StringBuilder();
        if (!TextUtils.isEmpty(user)) {
            int index = user.indexOf("(");
            if (index >= 0) {
                strbuilder.append(user.substring(0, index));
                if (!TextUtils.isEmpty(issuer)) {
                    strbuilder.append("(");
                    strbuilder.append(issuer);
                    strbuilder.append(")");
                }
            } else {
                strbuilder.append(user);
                if (!TextUtils.isEmpty(issuer)) {
                    strbuilder.append("(");
                    strbuilder.append(issuer);
                    strbuilder.append(")");
                }
            }
        } else {
            strbuilder.append("no name");
        }

        return strbuilder.toString();
    }

    class CertListAdapter extends RecyclerView.Adapter<CertListAdapter.ItemViewHolder> {

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_cert_list_item, parent, false);
            ItemViewHolder itemViewHolder = new ItemViewHolder(view);
            return itemViewHolder;
        }

        @Override
        public void onBindViewHolder(final ItemViewHolder holder, int position) {
            holder.txvUserNm.setText(getFormedUserNamne(mCertListArray.get(position).getUserName(), mCertListArray.get(position).getIssuerName()));
            holder.txvIssurerNm.setText(mCertListArray.get(position).getCertificateType());

            if (mCertListArray.get(position).isExpired()) {
                holder.ivArrow.setVisibility(View.GONE);
                holder.txvUserNm.setTextColor(getResources().getColor(R.color.color888888));
                holder.txvIssurerNm.setTextColor(getResources().getColor(R.color.color888888));
                String str = mCertListArray.get(position).getValidityDate() + " " + mContext.getString(R.string.txt_expire) + "(" + mContext.getString(R.string.cert_message_error_expired_cert) + ")";
                if(str.indexOf("(") > 0) {
                    SpannableStringBuilder ssb = new SpannableStringBuilder(str);
                    ssb.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorRed)), 0, str.indexOf("("), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                    holder.txvExpDate.setTextColor(getResources().getColor(R.color.color888888));
                    holder.txvExpDate.setText(ssb);
                } else {
                    holder.txvExpDate.setTextColor(getResources().getColor(R.color.colorRed));
                    holder.txvExpDate.setText(str);
                }
                holder.ivCertIcon.setImageDrawable(getResources().getDrawable(R.drawable.ico_expire_certificate));
            } else {
                holder.txvExpDate.setText(mCertListArray.get(position).getValidityDate() + " " + mContext.getString(R.string.txt_expire));
                String validityDate = mCertListArray.get(position).getValidityDate();
                if (!TextUtils.isEmpty(validityDate)) {
                    validityDate = validityDate.replaceAll("[^0-9]", "");
                    int days = getCountOfDays(validityDate);
                    if (days <= 30) {
                        String str = mCertListArray.get(position).getValidityDate() + " (" + mContext.getString(R.string.txt_expire_desc_01) +
                                             " " + days + mContext.getString(R.string.txt_expire_desc_02) + ")";
                        holder.txvExpDate.setText(str);
                    }
                }
            }

            if (mJobType == Const.CertJobType.MANAGER) {
                holder.btnLayout.setVisibility(View.VISIBLE);

                if (mCertListArray.get(position).isExpired()) {
                    holder.btnExport.setVisibility(View.INVISIBLE);
                    holder.btnPwChange.setVisibility(View.INVISIBLE);
                    holder.imageBar01.setVisibility(View.INVISIBLE);
                    holder.imageBar02.setVisibility(View.INVISIBLE);
                } else {
                    holder.btnExport.setVisibility(View.VISIBLE);
                    holder.btnPwChange.setVisibility(View.VISIBLE);
                    holder.imageBar01.setVisibility(View.VISIBLE);
                    holder.imageBar02.setVisibility(View.VISIBLE);
                }

                holder.btnExport.setTag(position);
                holder.btnExport.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCertIndex = (int) v.getTag();
                        onClickChoiceActivity(mCertIndex);
                    }
                });

                holder.btnPwChange.setTag(position);
                final Bundle curCert = new Bundle();
                curCert.putString(Const.INTENT_CERT_USERNAME, getFormedUserNamne(mCertListArray.get(position).getUserName(), mCertListArray.get(position).getIssuerName()));
                curCert.putString(Const.INTENT_CERT_TYPE, mCertListArray.get(position).getCertificateType());
                curCert.putString(Const.INTENT_CERT_ISSUERNAME, mCertListArray.get(position).getIssuerName());
                curCert.putString(Const.INTENT_CERT_EXPIREDATE, mCertListArray.get(position).getValidityDate());
                holder.btnPwChange.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        Intent intent = new Intent(mContext, CertChangePWActivity.class);
                        intent.putExtra(Const.INTENT_CERT_INDEX, pos);
                        intent.putExtra(Const.INTENT_CERT_BUNDLEDATA, curCert);
                        mContext.startActivity(intent);
                    }
                });

                holder.btnDelete.setTag(position);
                holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final int pos = (int) v.getTag();
                        String name = getFormedUserNamne(mCertListArray.get(pos).getUserName(), mCertListArray.get(pos).getIssuerName());
                        String type = mCertListArray.get(pos).getCertificateType();
                        String validityDate = mCertListArray.get(pos).getValidityDate();

                        SlidingCertDeleteDialog dilaog = new SlidingCertDeleteDialog(mContext, name, type, validityDate, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                boolean result = CustomCertManager.getInstance().delCert(pos);
                                if (result) {
                                    mCertListArray.remove(pos);
                                    notifyDataSetChanged();

                                    if (mCertListArray.size() == 0)
                                        mLayoutEmptyCert.setVisibility(View.VISIBLE);

                                    DialogUtil.alert(mContext, R.string.cert_message_done_delete_cert);
                                }
                            }
                        });
                        dilaog.show();
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mCertListArray.size();
        }

        public class ItemViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder, View.OnClickListener {
            RelativeLayout layoutItemRect;
            TextView       txvUserNm;
            TextView       txvIssurerNm;
            TextView       txvExpDate;
            ImageView      ivArrow;
            ImageView      ivCertIcon;
            LinearLayout   rootLayout;
            LinearLayout   btnLayout;
            TextView       btnExport;
            ImageView      imageBar01;
            ImageView      imageBar02;
            TextView       btnPwChange;
            TextView       btnDelete;

            public ItemViewHolder(View itemView) {
                super(itemView);
                layoutItemRect = (RelativeLayout) itemView.findViewById(R.id.layout_cert_item_rect);
                txvUserNm = (TextView) itemView.findViewById(R.id.txvUserNm);
                txvIssurerNm = (TextView) itemView.findViewById(R.id.txvIssurerNm);
                txvExpDate = (TextView) itemView.findViewById(R.id.txvExpDate);
                ivArrow = (ImageView) itemView.findViewById(R.id.iv_Arrow);
                ivCertIcon = (ImageView) itemView.findViewById(R.id.imgIcon);
                rootLayout = (LinearLayout) itemView.findViewById(R.id.ll_certitemroot);
                btnLayout = (LinearLayout) itemView.findViewById(R.id.btn_layout);
                btnExport = (TextView) itemView.findViewById(R.id.btn_exprt_cert);
                imageBar01 = (ImageView) itemView.findViewById(R.id.imageview_cert_bar_01);
                imageBar02 = (ImageView) itemView.findViewById(R.id.imageview_cert_bar_02);
                btnPwChange = (TextView) itemView.findViewById(R.id.btn_change_password);
                btnDelete = (TextView) itemView.findViewById(R.id.btn_delete_cert);

                layoutItemRect.setOnClickListener(this);
            }

            @Override
            public void onItemSelected() {
                Logs.e("ItemViewHolder - onItemSelected");
                itemView.setBackgroundColor(Color.LTGRAY);
            }

            @Override
            public void onItemClear() {
                itemView.setBackgroundColor(0);

            }

            @Override
            public void onClick(View view) {
                Logs.e("ItemViewHolder - onClick");
                listItemClick(getAdapterPosition());
            }
        }
    }

    private CertItemInfo createCertItemInfo(CustomRow customRow) {
        if (customRow == null)
            return null;

        CertItemInfo certItemInfo = new CertItemInfo();
        X509Certificate x509 = CustomCertManager.getInstance().getCertificateManager().viewCertificateInfo(mCertIndex);
        if (x509 == null)
            return null;

        String userName = customRow.getUserName();
        if (!TextUtils.isEmpty(userName))
            certItemInfo.setName(userName);

        String type =  customRow.getCertificateType();
        if (!TextUtils.isEmpty(type)) {
            certItemInfo.setType(type);
        }

        int ver = x509.getVersion();
        String stVer = String.valueOf(ver);
        if (!TextUtils.isEmpty(stVer)) {
            certItemInfo.setVer(stVer);
        }

        BigInteger no = x509.getSerialNumber();
        String stNo = String.valueOf(no);
        if (!TextUtils.isEmpty(stNo)) {
            certItemInfo.setNo(stNo);
        }

        String auth = customRow.getIssuerName();
        if (!TextUtils.isEmpty(auth)) {
            if (auth.contains("Yessign"))
                auth = "금융결제원";
            else if (auth.contains("SignKorea"))
                auth = "코스콤";
            else if (auth.contains("SignGate"))
                auth = "한국정보인증";
            else if (auth.contains("CrossCert"))
                auth = "한국전자인증";
            else if (auth.contains("TradeSign"))
                auth = "한국무역정보통신";
            else if (auth.contains("NCA"))
                auth = "이니텍";

            certItemInfo.setAuth(auth);
        }

        boolean isExpired = customRow.isExpired();
        if (isExpired)
            certItemInfo.setState("무효");
        else
            certItemInfo.setState("유효");

        String issueDate = customRow.getBeforeDate();
        if (!TextUtils.isEmpty(issueDate)) {
            certItemInfo.setIssueDate(issueDate);
        }

        String validityDate = customRow.getValidityDate();
        if (!TextUtils.isEmpty(validityDate)) {
            certItemInfo.setExpireDate(validityDate);
        }

        String sign = x509.getSigAlgName();
        if (!TextUtils.isEmpty(sign)) {
            certItemInfo.setSignAlog(sign);
        }

        String issuer = x509.getIssuerDN().toString();
        if (!TextUtils.isEmpty(issuer)) {
            certItemInfo.setIssuer(issuer);
        }

        String dn = x509.getSubjectDN().toString();
        if (!TextUtils.isEmpty(dn)) {
            certItemInfo.setCertCN(dn);
        }

        return certItemInfo;
    }

    public void cancelExportTask() {
        if (exportTask != null) {
            exportTask.cancel(true);
            exportTask = null;
        }
    }

    public void startExportTask(String passChiper) {
        cancelExportTask();

        if (mCertIndex < 0 || TextUtils.isEmpty(passChiper))
            return;

        exportTask = new ExportCertTaskV12(this, mCertIndex, passChiper);
        exportTask.execute(passChiper);
    }

    /**
     * v1.2 인증서 내보내기 Task
     * 진행상자를 표시하고 background에서 인증서 내보내기 수행
     */
    private static class ExportCertTaskV12 extends AsyncTask<String, Integer, Integer> {
        private WeakReference<BaseActivity> activityReference;
        private String  teskResMsg = null;
        private String  teskCertAuthCode = "";
        private int     teskCertIndex;
        private String  teskPassChiper;

        ExportCertTaskV12(BaseActivity activity, int certIdx, String passChiper) {
            activityReference = new WeakReference<>(activity);
            teskCertIndex = certIdx;
            teskPassChiper = passChiper;
        }

        @Override
        protected void onPreExecute() {
            BaseActivity activity = activityReference.get();
            activity.showProgressDialog();
        }

        @Override
        protected Integer doInBackground(String... params) {
            NetServerConnector exportHttps = null;
            NetResult netRes = new NetResult();
            String passwd = params[0];
            String pkcs12Data = null;

            // 복호화 모드(NONE : 암호화 되어있지 않음, NFILTER : NFilter 복호화, SPINPAD : SPinPad 복호화,
            // 					MTRANSKEY : Raon mTransKey 복호화, MODE_EXTRUS : Extrus 복호화)
            try {
                pkcs12Data = CustomCertManager.getInstance().getCertificateManager().exportCertificate_v12(teskCertIndex, passwd, KeyPadCipher.MODE_MTRANSKEY);

                // PKCS12 내보내고 인증코드 받기
                exportHttps = new NetServerConnector(SaidaUrl.ReqUrl.URL_CERT_EXPORT_IMPORT_URL.getReqUrl());
                exportHttps.setParameter("SVer", "1.2");
                exportHttps.setParameter("Size", "8");
                exportHttps.setParameter("Action", "EXPORT");
                exportHttps.setParameter("EncCert", pkcs12Data);
                netRes = exportHttps.getNetResponse();    // 인증코드

                teskResMsg = netRes.getFullResMessage();

                if (!netRes.isOk()) {
                    return -1;
                }

            } catch (Exception e) {
                Logs.printException(e);
                return -1;
            }
            teskCertAuthCode = teskResMsg.replaceAll("[^0-9]", "");    // SUCCESS 인증코드를 가지고

            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            final BaseActivity activity = activityReference.get();
            activity.dismissProgressDialog();

            if (result == 0) {    // 상대방의 가져오기를 대기한다
                Intent intent = new Intent(activity, CertExportActivity.class);
                intent.putExtra(Const.INTENT_CERT_INDEX, teskCertIndex);
                intent.putExtra(Const.INTENT_CERT_AUTHCODE, teskCertAuthCode);
                intent.putExtra(Const.INTENT_CERT_PASSWORD, teskPassChiper);
                intent.putExtra(Const.INTENT_CERT_PURPOSE, Const.CertJobType.EXPORT);
                activity.startActivityForResult(intent, REQUEST_EXPORT);

            } else {
                if (TextUtils.isEmpty(teskResMsg)) {
                    teskResMsg = activity.getString(R.string.cert_message_not_receive_auth_code);
                }

                DialogUtil.alert(activity, teskResMsg, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.finish();
                    }
                });
            }
        }
    }

    public int getCountOfDays(String expireDateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());

        Date expireDate = null;
        Date todayDate = null;

        try {
            expireDate = dateFormat.parse(expireDateString);
            Date today = new Date();
            todayDate = dateFormat.parse(dateFormat.format(today));
        } catch (ParseException e) {
            //e.printStackTrace();
        }

        int cYear = 0, cMonth = 0, cDay = 0;
        Calendar cCal = Calendar.getInstance();
        cCal.setTime(todayDate);
        cYear = cCal.get(Calendar.YEAR);
        cMonth = cCal.get(Calendar.MONTH);
        cDay = cCal.get(Calendar.DAY_OF_MONTH);

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

        long diff = date2.getTimeInMillis() - date1.getTimeInMillis();
        float dayCount = (float) diff / (24 * 60 * 60 * 1000);

        return (int)dayCount;
    }
}
