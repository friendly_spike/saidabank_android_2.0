package com.sbi.saidabank.activity.main2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.LogoutTimeChecker;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.main2.Main2DataInfo;
import com.sbi.saidabank.define.datatype.main2.Main2RelayInfo;
import com.sbi.saidabank.web.BaseWebChromeClient;
import com.sbi.saidabank.web.BaseWebClient;
import com.sbi.saidabank.web.JavaScriptBridge;

import java.util.ArrayList;

public class Main2OpenBankWebFragment extends BaseFragment {

    private Context mContext;
    private FrameLayout mContainer;
    private ProgressBarLayout mProgressLayout;
    private JavaScriptBridge mJsBridge;
    private String mServiceUrl;

    public static final Main2OpenBankWebFragment newInstance() {
        MLog.d();
        return new Main2OpenBankWebFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);
        this.mContext = getContext();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main2_web, container, false);
        mProgressLayout = rootView.findViewById(R.id.ll_progress);
        mContainer = rootView.findViewById(R.id.container_webview);
        WebView webView = getCurrentWebView();
        webView.setWebViewClient(new BaseWebClient((Activity) mContext, this));
        webView.setWebChromeClient(new BaseWebChromeClient((Activity) mContext, mContainer, mJsBridge));
        mJsBridge = new JavaScriptBridge((Activity) mContext, mContainer, this);
        webView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);

        mServiceUrl = WasServiceUrl.MAI0080100.getServiceUrl();
        webView.loadUrl(mServiceUrl);

        return rootView;
    }

    @Override
    public void onResume() {
        MLog.d();
        String OBA_SVC_ENTR_YN = LoginUserInfo.getInstance().getOBA_SVC_ENTR_YN();
        if(!TextUtils.isEmpty(OBA_SVC_ENTR_YN) && OBA_SVC_ENTR_YN.equalsIgnoreCase("Y")){
            showProgress();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissProgress();
                    ((Main2Activity) getActivity()).viewPagerUpdate();
                }
            },500);
        }

        super.onResume();
    }

    @Override
    public void onPause() {
        MLog.d();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        MLog.d();
        super.onDestroyView();
        if (DataUtil.isNotNull(mJsBridge)) {
            mJsBridge.destroyJavaScriptBrigdge();
        }
    }

    @Override
    public void onBackPressed() {
        MLog.d();
        if (DataUtil.isNotNull(getCurrentWebView()) && ((String) getCurrentWebView().getTag(R.id.TAG_URL)).contains(SaidaUrl.getBaseWebUrl()))
            mJsBridge.callJavascriptFunc("co.app.from.back", null);
        else {
            ((Main2Activity) mContext).showLogoutDialog();
        }
    }

    @Override
    public void showProgress() {
        LogoutTimeChecker.getInstance(mContext).autoLogoutUserInteraction(false);
        mProgressLayout.show();
    }

    @Override
    public void dismissProgress() {
        mProgressLayout.hide();
        if (DataUtil.isNotNull(mContainer) && mContainer.getVisibility() != View.VISIBLE) {
            mContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void fragmentForResult(int requestCode, int resultCode, Intent data) {
        super.fragmentForResult(requestCode, resultCode, data);
    }

    /**
     * Global menu를 유지한채 웹뷰를 뛰울때 웹에서 302번호 api호출시.Main에서만 주로 처리한다.
     *
     * @param url
     */
    @Override
    public void loadUrl(String url, String params) {
        int webViewCnt = mContainer.getChildCount();
        BaseWebView newWebView = new BaseWebView(mContext);
        newWebView.setTag(webViewCnt + 1);
        newWebView.setWebViewClient(new BaseWebClient(getActivity(), mJsBridge.getWebActionListener()));
        newWebView.setWebChromeClient(new BaseWebChromeClient(getActivity(), mContainer, mJsBridge));
        newWebView.addJavascriptInterface(mJsBridge, JavaScriptBridge.CALL_NAME);
        mContainer.addView(newWebView);
        if (DataUtil.isNull(params)) {
            newWebView.loadUrl(url);
        } else {
            newWebView.postUrl(url, params.getBytes());
        }
    }

    @Override
    public void refreshWebView() {
        MLog.d();
        WebView webView = getCurrentWebView();
        webView.reload();
    }

    @Override
    public void closeWebView() {
        MLog.d();
        int webViewCnt = mContainer.getChildCount();
        if (webViewCnt <= 1) {
            ((Main2Activity) mContext).showLogoutDialog();
        } else {
            mContainer.removeViewAt(webViewCnt - 1);
            webViewCnt = mContainer.getChildCount();
            if (webViewCnt == 1) {
                WebView webView = (WebView) mContainer.getChildAt(0);
                webView.loadUrl((String) webView.getTag(R.id.TAG_URL));
            }
        }
    }

    @Override
    public void setCurrentPage(String url) {
        MLog.d();
        if (DataUtil.isNotNull(url) && DataUtil.isNotNull(getCurrentWebView())) {
            getCurrentWebView().setTag(R.id.TAG_URL, url);
        }
    }

    @Override
    public void setFinishedPageLoading(boolean flag) {
        MLog.d();
        if (DataUtil.isNotNull(getCurrentWebView())) {
            getCurrentWebView().setTag(R.id.TAG_LOAD_STATE, flag);
        }
    }

    @Override
    public void onWebViewSSLError(String url, String errorCd, final SslErrorHandler handler) {
        DialogUtil.alert(
                mContext,
                R.string.common_msg_error_ssl_cert,
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handler.proceed();
                    }
                },
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handler.cancel();
                    }
                }
        );
    }

    // ==================================================
    // VIEW
    // ==================================================
    private WebView getCurrentWebView() {
        int webViewCnt = mContainer.getChildCount();
        if (webViewCnt < 1) return null;
        return (WebView) mContainer.getChildAt(webViewCnt - 1);
    }

}
