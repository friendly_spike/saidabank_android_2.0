package com.sbi.saidabank.activity.crop;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.crop.CropImagePreset;
import com.sbi.saidabank.common.crop.CropImageViewOptions;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Saidabanking_android
 * <p>
 * Class: CropImageActivity
 * Created by 950485 on 2018. 10. 15..
 * <p>
 * Description:화면 Crop을 위한 화면
 */
public class CropImageActivity extends BaseActivity {

    private CropImageFragment mCurrentFragment;
    private CropImageViewOptions mCropImageViewOptions = new CropImageViewOptions();
    private Uri mTargetImageUri;
    private Uri mCropImageUri;
    private int mCropType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MLog.d();
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_crop);
        Bundle bundle = getIntent().getExtras();
        if (DataUtil.isNotNull(bundle) && DataUtil.isNotNull(bundle.getString(Const.CROP_IMAGE_URI))) {
            mTargetImageUri = Uri.parse(bundle.getString(Const.CROP_IMAGE_URI));
        }
        initUX();
        if (DataUtil.isNull(savedInstanceState)) {
            if(bundle.getInt(Const.CROP_IMAGE_TYPE,Const.CROP_TYPE_CIRCLE) == Const.CROP_TYPE_CIRCLE){
                setMainFragmentByPreset(CropImagePreset.CIRCULAR);  // CropImagePreset.RECT
                mCropType = Const.CROP_TYPE_CIRCLE;
            }else{
                setMainFragmentByPreset(CropImagePreset.RECT);
                mCropType = Const.CROP_TYPE_RECT;
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mCurrentFragment.updateCurrentCropViewOptions();
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        MLog.d();
        super.onActivityResult(requestCode, resultCode, data);

        MLog.i("requestCode >> " + requestCode);

        if (resultCode == RESULT_OK) {
            if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE) {
                Uri imageUri = CropImage.getPickImageResultUri(this, data);
                if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                    mCropImageUri = imageUri;
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                } else {
                    mCurrentFragment.setImageUri(imageUri);
                }
            } else if (requestCode == Const.REQUEST_CROP_IMAGE) {
                if (DataUtil.isNotNull(CropImageResultActivity.mCropImage)) {
                    Uri uri = saveBitmapToJpeg(CropImageResultActivity.mCropImage);
                    if (DataUtil.isNotNull(uri)) {
                        Intent intent = new Intent();
                        intent.putExtra(Const.CROP_IMAGE_URI, uri.toString());
                        setResult(RESULT_OK, intent);
                    }
                    finish();
                }
                CropImageResultActivity.mCropImage = null;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        MLog.d();
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE
                && DataUtil.isNotNull(mCropImageUri)
                && (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
            mCurrentFragment.setImageUri(mCropImageUri);
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        Button btnCancel = (Button) findViewById(R.id.btn_crop_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Button btnOk = (Button) findViewById(R.id.btn_crop_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCurrentFragment.mCropImageView.getCroppedImageAsync();
            }
        });
    }

    private void setMainFragmentByPreset(CropImagePreset cropImagePreset) {
        getFragmentManager().beginTransaction().replace(R.id.container_crop, CropImageFragment.newInstance(cropImagePreset)).commit();
    }

    public void setCurrentFragment(CropImageFragment fragment) {
        mCurrentFragment = fragment;
    }

    public void setCurrentOptions(CropImageViewOptions options) {
        mCropImageViewOptions = options;
    }

    /**
     * 저장될 jpeg 파일 uri
     *
     * @return 저장될 jpeg 파일 uri
     */
    public Uri getTargetImageUri() {
        return mTargetImageUri;
    }

    /**
     * SDCard에 이미지 저장 uri 생성 (input Bitmap -> saved file JPEG)
     *
     * @param bitmap : bitmap file
     */
    public Uri saveBitmapToJpeg(Bitmap bitmap) {
        MLog.d();

        File file= ImgUtils.createNewImageFilePathName(this,"crop");

        try {
            FileOutputStream out = new FileOutputStream(file, false);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            MLog.e(e);
            return null;
        }
        Uri uri = Uri.fromFile(file);
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
        return uri;
    }


}
