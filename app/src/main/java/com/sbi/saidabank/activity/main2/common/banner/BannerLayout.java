package com.sbi.saidabank.activity.main2.common.banner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.viewpager2.widget.ViewPager2;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.main.MainAdsItemInfo;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class BannerLayout extends RelativeLayout {
    //관리자에 정의된 배너의 비율
    public static final int RECOM_BANNER_WIDHT = 360;
    public static final int RECOM_BANNER_HEIGHT = 77;

    private static final int CHANGE_TIME = 7*1000;

    private ArrayList<MainAdsItemInfo> mBannerArray;
    private ViewPager2 mViewPager;
    private LinearLayout mLayoutPosition;
    private TextView mTvPosition;
    private ImageView mIvPlayPause;

    private BannerAdapter mAdapter;
    private Timer mTimer;

    private OnLoadFinishListener mLoadFinishListener;

    public interface OnLoadFinishListener {
        void onLoadFinish();
    }

    public BannerLayout(Context context) {
        super(context);
        initUX(context);
    }

    public BannerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context) {
        View layout = View.inflate(context, R.layout.layout_main2_banner, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mBannerArray = new ArrayList<MainAdsItemInfo>();
        mAdapter = new BannerAdapter(context, new BannerAdapter.OnItemSelectListener() {
            @Override
            public void onSelectItem(String url) {
                if(TextUtils.isEmpty(url)) return;

                Intent intent = new Intent(getContext(), WebMainActivity.class);
                String newUrl = SaidaUrl.getBaseWebUrl() + "/" + url;
                if(!newUrl.contains(".act")){
                    newUrl = newUrl + ".act";
                }
                intent.putExtra(Const.INTENT_MAINWEB_URL, newUrl);
                if(getContext() != null && !((Activity)getContext()).isFinishing())
                    getContext().startActivity(intent);
            }

            @Override
            public void onLoadBitmapResult(ArrayList<MainAdsItemInfo> bannerArray) {
                if(bannerArray.size() == 0){
                    mBannerArray.clear();
                    setVisibility(GONE);
                }else{
                    if(mBannerArray.size() != bannerArray.size()){
                        mBannerArray.clear();
                        mBannerArray.addAll(bannerArray);
                    }

                    setVisibility(VISIBLE);
                    if(mBannerArray.size() > 1){
                        startTimer();
                    }else{
                        mIvPlayPause.setVisibility(GONE);
                    }

                    mLayoutPosition.setVisibility(VISIBLE);
                    mTvPosition.setText("1/"+mBannerArray.size());
                }

                if(mLoadFinishListener != null)
                    mLoadFinishListener.onLoadFinish();
            }
        });
        mViewPager = layout.findViewById(R.id.viewpager);
        mViewPager.setAdapter(mAdapter);
        mViewPager.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);

                int nextPageIdx = position + 1;
                int dispIdx = position%mBannerArray.size();
                mTvPosition.setText((dispIdx+1) + "/"+mBannerArray.size());
            }
        });

        mLayoutPosition = layout.findViewById(R.id.layout_position);
        mLayoutPosition.setVisibility(INVISIBLE);
        mLayoutPosition.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mTimer == null){
                    startTimer();
                }else{
                    stopTimer();
                }
            }
        });
        mTvPosition = layout.findViewById(R.id.tv_position);
        mIvPlayPause = layout.findViewById(R.id.iv_play_pause);


        getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        //Logs.e("BannerLayout - onGlobalLayout - height : " + getHeight());

                        int lcdWidth = GraphicUtils.getActivityWidth(getContext());

                        int widthDip = (int)Utils.pixelToDp(getContext(),lcdWidth);
                        //Logs.e("BannerLayout - onGlobalLayout - widthDip : " + widthDip);

                        int pagerHeightDip = RECOM_BANNER_HEIGHT;
                        if(RECOM_BANNER_WIDHT != widthDip){
                            pagerHeightDip = widthDip * RECOM_BANNER_HEIGHT / RECOM_BANNER_WIDHT;
                        }

                        //Logs.e("BannerLayout - onGlobalLayout - pagerHeightDip : " + pagerHeightDip);
                        ViewGroup.LayoutParams params = mViewPager.getLayoutParams();
                        params.height = (int)Utils.dpToPixel(getContext(),pagerHeightDip);
                        mViewPager.setLayoutParams(params);
                        mViewPager.invalidate();

                        getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                });


        setVisibility(INVISIBLE);
    }

    public void onResumeLayout(){
        //Logs.e("onResumeLayout");
        if(mBannerArray.size() > 1){
            startTimer();
        }

    }

    public void onPauseLayout(){
        //Logs.e("onPauseLayout");
        stopTimer();
        //mViewPager.setCurrentItem(0,false);
    }

    public void onDestroyLayout(){
        //Logs.e("onDestroyLayout");
        mAdapter.clearBitmap();
    }

    public void setBannerInfo(ArrayList<MainAdsItemInfo> arrayList){
        if(mBannerArray.size() == arrayList.size()){
//            int currentPageIdx = mViewPager.getCurrentItem();
//            int dispIdx = currentPageIdx%mBannerArray.size();
//            mViewPager.setCurrentItem(dispIdx,false);

            if(mLoadFinishListener != null)
                mLoadFinishListener.onLoadFinish();

        }else{
            mBannerArray.clear();
            mBannerArray.addAll(arrayList);

            mAdapter.setBannerArray(arrayList);
//            if(arrayList.size() > 0){
//                mViewPager.setCurrentItem(0,false);
//            }
        }
        mViewPager.setCurrentItem(0,false);
    }

    public int getBannerCount(){
        return mBannerArray.size();
    }

    private void stopTimer(){
        if(mTimer != null){
            mTimer.cancel();
            mTimer=null;
        }
        mIvPlayPause.setImageResource(R.drawable.btn_banner_play);
    }

    private void startTimer(){
        if(mBannerArray.size() < 2) return;

        if(mTimer != null){
            mTimer.cancel();
            mTimer=null;
        }

        mTimer = new Timer();
        mTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                ((Activity) getContext()).runOnUiThread(new Runnable() {
                    public void run() {
                        int currentPageIdx = mViewPager.getCurrentItem();
                        //Logs.e("startTimer - currentPageIdx :" + currentPageIdx);

                        int nextPageIdx = currentPageIdx + 1;
                        mViewPager.setCurrentItem(nextPageIdx, true);
                        int dispIdx = nextPageIdx%mBannerArray.size();
                        mTvPosition.setText((dispIdx+1) + "/" + mBannerArray.size());
                    }
                });
            }
        }, CHANGE_TIME-2000, CHANGE_TIME);

        mIvPlayPause.setImageResource(R.drawable.btn_banner_pause);
    }

    public void setLoadFinishListener(OnLoadFinishListener listener){
        mLoadFinishListener = listener;
    }
}
