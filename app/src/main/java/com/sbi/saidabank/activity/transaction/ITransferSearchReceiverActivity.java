package com.sbi.saidabank.activity.transaction;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.adater.ITransferRcvSearchAdapter;
import com.sbi.saidabank.activity.transaction.itransfer.OnITransferSearchActionListener;
import com.sbi.saidabank.activity.transaction.util.TransferRequestUtils;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.dialog.TransferPhoneRealNameDialog;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRcvSearchInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;

import java.util.ArrayList;



/**
 * Create 210125
 * 받는분 선택 화면
 * 최근/즐겨찾기,내계좌,연락처의 3개의 탭으로 구성
 * 각 탭별 통합 검색 지원.
 */
public class ITransferSearchReceiverActivity extends BaseActivity implements View.OnClickListener, OnITransferSearchActionListener {
    public static final int ITEM_TYPE_RECENTLY  = 0;
    public static final int ITEM_TYPE_MYACCOUNT = 1;
    public static final int ITEM_TYPE_OPENBANK  = 2;
    public static final int ITEM_TYPE_CONTACT   = 3;

    //검색영역
    private RelativeLayout mLayoutSearchEditArea;
    private EditText mEditTextSearch;
    private RecyclerView   mListSearchResult;
    private ArrayList<ITransferRcvSearchInfo> mSearchArrayList;
    private ITransferRcvSearchAdapter   mSearchResultAdapter;

    private ArrayList<ITransferRecentlyInfo> mRecentlyArrayList;
    private ArrayList<MyAccountInfo> mMyAccountArrayList;
    private ArrayList<OpenBankAccountInfo> mOpenBankAccountArrayList;
    private ArrayList<ContactsInfo> mContactArrayList;

    private ImageView mIconErase;

    private boolean isAddTransfer;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_itransfer_search_receiver);

        //이체추가로 진입한 경우
        isAddTransfer = getIntent().getBooleanExtra(Const.INTENT_IS_ADD_TRANSFER,false);

        mSearchArrayList  = new ArrayList<ITransferRcvSearchInfo>();

        mRecentlyArrayList = (ArrayList<ITransferRecentlyInfo>)getIntent().getSerializableExtra("ARRAY_RECENTLY");
        mMyAccountArrayList = (ArrayList<MyAccountInfo>)getIntent().getSerializableExtra("ARRAY_MYACCOUNT");
        mOpenBankAccountArrayList = (ArrayList<OpenBankAccountInfo>)getIntent().getSerializableExtra("ARRAY_OPENBANK");
        mContactArrayList = getIntent().getParcelableArrayListExtra("ARRAY_CONTACT");

        initView();

    }

    private void initView(){
        //검색버튼
        findViewById(R.id.iv_back).setOnClickListener(this);
        mIconErase = findViewById(R.id.iv_search_erase);
        mIconErase.setOnClickListener(this);

        //검색영역
        mLayoutSearchEditArea = findViewById(R.id.layout_search_edit_area);
        int radius = (int) Utils.dpToPixel(this,6);
        mLayoutSearchEditArea.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius},1,"#01a2b4"));
        mEditTextSearch = findViewById(R.id.et_search_text);
        mEditTextSearch.requestFocus();
        mEditTextSearch.addTextChangedListener(new TextWatcher(){

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
//                if(TextUtils.isEmpty(s.toString())){
//                    mSearchArrayList.clear();
//                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before,int count) {
                // TODO Auto-generated method stub
                String inputText = mEditTextSearch.getText().toString().trim();
                //한글 완성형만 체크하도록 한다.
                if(inputText.matches("^[A-Za-z0-9+가-힣]*$")){
                    makeSearchTextList(inputText);
                }

                if(TextUtils.isEmpty(inputText)){
                    mIconErase.setImageResource(R.drawable.btn_search_top);
                }else{
                    mIconErase.setImageResource(R.drawable.btn_erase);
                }
            }
        });
        //검색관련
        mListSearchResult = findViewById(R.id.recyclerview_search);
        mSearchResultAdapter = new ITransferRcvSearchAdapter(this,this);
        mListSearchResult.setAdapter(mSearchResultAdapter);
        mListSearchResult.setLayoutManager(new LinearLayoutManager(this));



    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.iv_search_erase:
                if(!TextUtils.isEmpty(mEditTextSearch.getText().toString())) {
                    mEditTextSearch.setText("");
                    Utils.showKeyboard(ITransferSearchReceiverActivity.this,mEditTextSearch);
                }
                break;
        }
    }

    /**
     * 검색어 입력시 해당 문자만 찾아낸다.
     *
     */
    private void makeSearchTextList(String inputText){
        mSearchArrayList.clear();
        if(!TextUtils.isEmpty(inputText)){
            for(int i=0;i<mRecentlyArrayList.size();i++){
                ITransferRecentlyInfo info  = mRecentlyArrayList.get(i);
                if(info.getTextForSearch().contains(inputText)){
                    mSearchArrayList.add(new ITransferRcvSearchInfo(ITEM_TYPE_RECENTLY,info));
                }
            }
            for(int i=0;i<mMyAccountArrayList.size();i++){
                MyAccountInfo info  = mMyAccountArrayList.get(i);
                if(info.getTextForSearch().contains(inputText)){
                    mSearchArrayList.add(new ITransferRcvSearchInfo(ITEM_TYPE_MYACCOUNT,info));
                }
            }
            for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                OpenBankAccountInfo info  = mOpenBankAccountArrayList.get(i);
                if(info.getTextForSearch().contains(inputText)){
                    mSearchArrayList.add(new ITransferRcvSearchInfo(ITEM_TYPE_OPENBANK,info));
                }
            }

            for(int i=0;i<mContactArrayList.size();i++){
                ContactsInfo info  = mContactArrayList.get(i);
                if(info.getTextForSearch().contains(inputText)){
                    mSearchArrayList.add(new ITransferRcvSearchInfo(ITEM_TYPE_CONTACT,info));
                }
            }
        }
        Logs.e("makeSearchTextList - Count : " + mSearchArrayList.size());
        mSearchResultAdapter.setSearchArray(mSearchArrayList,inputText);
    }

    @Override
    public void checkRemitteeAccount(String bankCd,String detailBankCd, String accountNo,String recvNm) {
        TransferRequestUtils.requestRemitteeAccountBySingle(ITransferSearchReceiverActivity.this,
                bankCd,
                detailBankCd,
                accountNo,
                recvNm,
                isAddTransfer,
                new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result, String ret) {
                        if(result){
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                });
    }

    @Override
    public void checkGroupAccount(String groupIdx,String favorite,String groupCntText) {
        //휴대폰 이체 조건검색
        //사이다이체가 아니면...
        if(!TransferUtils.isSaidaAccount()){
            //오픈뱅킹 계좌일경우엔 휴대폰 이체 사용불가
            DialogUtil.alert(this,getString(R.string.itransfer_msg_group_only_able_saida));
            return;
        }

        //지연이체, 입금지정계좌서비스 체크
        if(!TransferUtils.checkAbleServiceCondition(this,"휴대폰이체")){
            return;
        }

        //그룹 등록 가능인원 체크
        if(!TextUtils.isEmpty(groupCntText)){
            int goupCnt = Integer.parseInt(groupCntText);
            int saveRemitteeCnt = ITransferDataMgr.getInstance().getRemitteInfoArraySize();
            if(goupCnt + saveRemitteeCnt > 5){
                DialogUtil.alert(ITransferSearchReceiverActivity.this,"최대 5건까지\n다건 이체 등록이 가능합니다.");
                return;
            }
        }

        TransferRequestUtils.requestRemitteeAccountByMulti(ITransferSearchReceiverActivity.this,
                groupIdx,
                favorite,
                false,
                new HttpSenderTask.HttpRequestListener2() {
                    @Override
                    public void endHttpRequest(boolean result, String ret) {
                        if(result){
                            setResult(RESULT_OK);
                            finish();
                        }
                    }
                });
    }

    @Override
    public void showPhoneRealNameDialog(String name, String telNo) {
        //휴대폰 이체 조건검색
        //사이다이체가 아니면...
        if(!TransferUtils.isSaidaAccount()){
            //오픈뱅킹 계좌일경우엔 휴대폰 이체 사용불가
            DialogUtil.alert(this,getString(R.string.itransfer_msg_phone_only_able_saida));
            return;
        }

        //지연이체, 입금지정계좌서비스 체크
        if(!TransferUtils.checkAbleServiceCondition(this,"휴대폰이체")){
            return;
        }

        //이체 추가로 들어왔으면 휴대폰은 추가할수 없다.
        if(isAddTransfer){
            DialogUtil.alert(this,getString(R.string.itransfer_msg_able_add_transfer));
            return;
        }

        TransferPhoneRealNameDialog dialog = new TransferPhoneRealNameDialog(ITransferSearchReceiverActivity.this, name, telNo, false);
        dialog.setOnConfirmListener(new TransferPhoneRealNameDialog.OnConfirmListener() {
            @Override
            public void onConfirmPress(String name, String TLNO, boolean isFromContactList) {
                ContactsInfo cInfo = new ContactsInfo(name,TLNO);
                ITransferDataMgr.getInstance().setTransferContactsInfo(cInfo);


                setResult(RESULT_OK);
                finish();
            }
        });
        dialog.show();
    }
}
