package com.sbi.saidabank.activity.transaction.safedeal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.adater.SafeDealBankStockAdapter;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.util.ArrayList;

/**
 * 은행/증권사 리스트 슬라이드 화면
 */
public class SlideListBankStockLayout extends SlideBaseLayout implements View.OnClickListener,View.OnTouchListener{

    private ArrayList<TextView> mTabArray;
    private ArrayList<RequestCodeInfo> mArrayBank;                       // 은행 리스트값
    private ArrayList<RequestCodeInfo> mArrayStock;                      // 증권사 리스트값


    private SafeDealBankStockAdapter mBankStockAdapter;
    private ListView         mBankStockListView;


    private int mSelectTabIdx;
    //private boolean mRequestFinish;

    public SlideListBankStockLayout(Context context) {
        super(context);
        initUX(context);
    }

    public SlideListBankStockLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUX(Context context){
        setTag("SlideListBankStockLayout");
        View layout = View.inflate(context, R.layout.layout_safedeal_slide_list_bank_stock, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mLayoutBody = layout.findViewById(R.id.layout_body);

        layout.findViewById(R.id.layout_movebar).setOnTouchListener(this);


        mTabArray = new ArrayList<TextView>();
        mArrayBank = new ArrayList<>();
        mArrayStock = new ArrayList<>();

        TextView tvBank = layout.findViewById(R.id.tv_bank);
        tvBank.setTag(0);
        tvBank.setOnClickListener(this);
        mTabArray.add(tvBank);

        TextView tvStock = layout.findViewById(R.id.tv_stock);
        tvStock.setTag(1);
        tvStock.setOnClickListener(this);
        mTabArray.add(tvStock);

        mBankStockAdapter = new SafeDealBankStockAdapter(getContext(), mArrayBank, mArrayStock);
        mBankStockListView = layout.findViewById(R.id.listview_bank_stock);
        mBankStockListView.setAdapter(mBankStockAdapter);
        mBankStockListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Logs.e("position : " + position);
                View slideView = getSlideView(SlideReceiverAccountNumLayout.class);
                if(slideView != null){
                    RequestCodeInfo info;
                    if(mSelectTabIdx == 0){
                        info = mArrayBank.get(position);
                    }else{
                        info = mArrayStock.get(position);
                    }
                    ((SlideReceiverAccountNumLayout)slideView).setBankInfo(info.getSCCD(),info.getCD_NM());
                }
                slidingDown(250,0);
            }
        });

        selectTab(0);
        initLayoutSetBottom(true,false);
    }

    @Override
    public void onCancel() {
        //super.onCancel();
        //if(isScale) return;

        slidingDown(250,0);

    }

    @Override
    public void slidingUp(int aniTime,int delayTime) {
        selectTab(0);
        mBankStockListView.setSelection(0);

        super.slidingUp(aniTime,delayTime);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_bank:
            case R.id.tv_stock:
                int selectIdx = (int)v.getTag();
                selectTab(selectIdx);
                break;
        }
    }

    private void selectTab(int select_idx){
        for(int i=0;i<mTabArray.size();i++){
            Drawable backgoundDrawable;
            if(i == select_idx){
                backgoundDrawable = GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{0,0,0,0},2,"#000000");
                mTabArray.get(i).setBackground(backgoundDrawable);
                mTabArray.get(i).setTextColor(Color.parseColor("#000000"));
                mTabArray.get(i).setTypeface(Typeface.DEFAULT_BOLD);
            }else{
                backgoundDrawable = GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF0F0F0),new int[]{0,0,0,0},2,ContextCompat.getColor(getContext(),R.color.colorE5E5E5));
                mTabArray.get(i).setBackground(backgoundDrawable);
                mTabArray.get(i).setTextColor(ContextCompat.getColor(getContext(),R.color.color888888));
                mTabArray.get(i).setTypeface(Typeface.DEFAULT);
            }
        }

        mSelectTabIdx = select_idx;

        switch (select_idx){
            case 0:
                mBankStockAdapter.setBankStockMode(Const.BANK_STOCK_MODE.BANK_MODE);
                mBankStockAdapter.notifyDataSetChanged();
                break;
            case 1:
                mBankStockAdapter.setBankStockMode(Const.BANK_STOCK_MODE.STOCK_MODE);
                mBankStockAdapter.notifyDataSetChanged();
                break;
        }

        mBankStockListView.setSelection(0);
    }

    /**
     * Intro에서 은행 증권사 아이콘 리소스를 받을때 코드값도 같이 받는다.
     * 여기서는 Array로 변경만 하여 사용한다.
     * 안전거래 진입 시간을 줄일수 있다.
     */
    public void getBankStockList(){
        //20210113 - Intro에서 은행/증권사 리소스 다운로드 받을때 코드도 받아서 저장해 둔다.
        String bankArrayStr = Prefer.getBankList(getContext());
        String stockArrayStr = Prefer.getStockList(getContext());

        if (TextUtils.isEmpty(bankArrayStr) || TextUtils.isEmpty(stockArrayStr)){
            requestBankStockList();
            return;
        }

        mArrayBank = SaidaCodeUtil.getCodeArrayList(bankArrayStr);
        mArrayStock = SaidaCodeUtil.getCodeArrayList(stockArrayStr);

        if(DataUtil.isNull(mArrayBank) || DataUtil.isNull(mArrayStock)){
            requestBankStockList();
            return;
        }

        mBankStockAdapter.setBankStockList(mArrayBank,mArrayStock);
    }

    /**
     * 혹여 은행/증권사 코드값을 저장해 둔것이 없을 경우 다시 서버에서 조회한다.
     *
     */
    public void requestBankStockList(){
        mArrayBank.clear();
        mArrayStock.clear();
        SaidaCodeUtil.requestBankList(getContext(),new HttpSenderTask.HttpRequestListener(){

            @Override
            public void endHttpRequest(String ret) {
                mArrayBank = SaidaCodeUtil.getCodeArrayList(ret);
                if(DataUtil.isNotNull(mArrayBank) && DataUtil.isNotNull(mArrayStock)){
                    mBankStockAdapter.setBankStockList(mArrayBank,mArrayStock);
                }
            }
        });
        SaidaCodeUtil.requestStockList(getContext(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                mArrayStock = SaidaCodeUtil.getCodeArrayList(ret);
                if(DataUtil.isNotNull(mArrayBank) && DataUtil.isNotNull(mArrayStock)){
                    mBankStockAdapter.setBankStockList(mArrayBank,mArrayStock);
                }
            }
        });
    }

    //Move bar를 이용한 레이아웃 조절.
    private float startdY;
    private float moveY;
    private int   startViewHeight;
    private boolean isMove;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() != R.id.layout_movebar){
            return false;
        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_DOWN - y : " + event.getRawY());
                startdY = event.getRawY();
                isMove = false;
                startViewHeight = mLayoutBody.getHeight();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY =  startdY - event.getRawY();
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - moveY : " + moveY);

                if(moveY == 0){
                    return false;
                }

                //확장하려고 하지만 이미 모두 확장했당.
                if(moveY > 0 && mScreenHeight == startViewHeight){
                    return false;
                }

//                if(moveY < 0  && (mOriginViewHeight > startViewHeight + moveY) ){
//                    return false;
//                }

                isMove = true;
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - isMove : " + isMove);
                ViewGroup.LayoutParams params = mLayoutBody.getLayoutParams();
                params.height = startViewHeight + (int)moveY;
                mLayoutBody.setLayoutParams(params);
                mLayoutBody.requestLayout();

                break;

            case MotionEvent.ACTION_UP:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_UP - y : " + event.getRawY());
                if(event.getRawY() > mOriginViewYPos){
                    slidingDown(250,0);
                    return false;
                }

                if(isMove && moveY != 0){
                    int toSize = 0;
                    if(moveY > 0){ //확장
                        if(moveY > 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }else if(moveY < 0){ //복귀
                        moveY = moveY * -1;
                        if(moveY < 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }
                    AniUtils.changeViewHeightSizeAnimation(mLayoutBody,toSize,250);

                }
                isMove = false;
                break;

            default:
                return false;
        }


        return false;
    }

//    public boolean getRequestFinishState(){
//        return mRequestFinish;
//    }
}
