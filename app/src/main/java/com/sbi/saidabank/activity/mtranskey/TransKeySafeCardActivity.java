package com.sbi.saidabank.activity.mtranskey;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.solution.mtranskey.TransKeyUtils;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * Saidabanking_android
 *
 * Class: FingerprintActivity
 * Created by 950485 on 2018.09.12
 * <p>
 * Description:보안카드 입력
 */

public class TransKeySafeCardActivity extends BaseActivity implements View.OnClickListener, View.OnTouchListener, ITransKeyActionListener,ITransKeyActionListenerEx,ITransKeyCallbackListener {
    private static int INPUT_NUM = 2;

    final static int EDITTEXT_ID[] = {
            R.id.layout_input_01_safe_card, R.id.layout_input_02_safe_card
    };

    private TransKeyCtrl[] mTKMgrs = new TransKeyCtrl[2];

    private LinearLayout[] mLayouts = new LinearLayout[INPUT_NUM];
    private EditText[]     mEditTexts = new EditText[INPUT_NUM];
    private ViewGroup[]    mScrollViews = new ViewGroup[INPUT_NUM];
    private ViewGroup[]    mImgEditContainers = new ViewGroup[INPUT_NUM];

    private TextView       mTextInfo01;
    private TextView       mTextInfo02;

    private int            mKeypadType;
    private int            mMinLength;
    private String         mParameter;

    private String[]       mAlertMsgs =  new String[INPUT_NUM];
    private String[]       mCipherTextBuf = new String[INPUT_NUM];
    private String[]       mDummyTextBuf = new String[INPUT_NUM];

    private int           mCurViewIndex = 0;
    private int           mOnClickIndex = 0;
    private boolean      mIsViewCtrlKeypad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transkey_safe_card);

        Intent intent = getIntent();

        String firstNum = intent.getStringExtra(Const.SAFE_CARD_FIRST_NUM);
        String secondNum = intent.getStringExtra(Const.SAFE_CARD_SECOND_NUM);

        mKeypadType = intent.getIntExtra(Const.INTENT_TRANSKEY_PADTYPE, TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER);

        int mTextType = intent.getIntExtra(Const.INTENT_TRANSKEY_TEXTTYPE, TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE);
        String title = intent.getStringExtra(Const.INTENT_TRANSKEY_TITLE);
        String label = intent.getStringExtra(Const.INTENT_TRANSKEY_LABEL1);
        int max_length = intent.getIntExtra(Const.INTENT_TRANSKEY_MAXLENGTH,0);
        mMinLength = intent.getIntExtra(Const.INTENT_TRANSKEY_MINLENGTH,0);
        boolean precaution = intent.getBooleanExtra(Const.INTENT_TRANSKEY_PRECAUTION,false);
        mParameter = intent.getStringExtra(Const.INTENT_TRANSKEY_PARAMETER);
        if(TextUtils.isEmpty(mParameter))mParameter="";
        boolean mCationShow = intent.getBooleanExtra(Const.INTENT_TRANSKEY_CAPTION,false);

        mTextInfo01 = (TextView) findViewById(R.id.text_input_01_safe_card);
        mTextInfo02 = (TextView) findViewById(R.id.text_input_02_safe_card);

        String infoText01 = "[" + firstNum + "] 앞 2자리";
        String infoText02 = "[" + secondNum + "] 뒤 2자리";
        mAlertMsgs[0] = "보안카드 "+ firstNum + "번째 앞 2자리 숫자를 입력해 주십시오.";
        mAlertMsgs[1] = "보안카드 "+ secondNum + "번째 뒤 2자리 숫자를 입력해 주십시오.";

        final SpannableStringBuilder sp01 = new SpannableStringBuilder(infoText01);
        sp01.setSpan(new ForegroundColorSpan(Color.BLUE), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextInfo01.append(sp01);
        final SpannableStringBuilder sp02 = new SpannableStringBuilder(infoText02);
        sp02.setSpan(new ForegroundColorSpan(Color.RED), 0, 4, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextInfo02.append(sp02);

        for (int index = 0; index < mEditTexts.length; index++) {
            mLayouts[index] = (LinearLayout) findViewById(EDITTEXT_ID[index]);
            mEditTexts[index] = (EditText) mLayouts[index].findViewById(R.id.editText);
            mEditTexts[index].setOnTouchListener(this);
            mEditTexts[index].setOnClickListener(this);
            mEditTexts[index].setTag(index);

            mScrollViews[index] = (ViewGroup) mLayouts[index].findViewById(R.id.keyscroll);
            mScrollViews[index].setOnClickListener(this);
            mScrollViews[index].setTag(index);

            mImgEditContainers[index] = (ViewGroup) mLayouts[index].findViewById(R.id.keylayout);
            mImgEditContainers[index].setOnTouchListener(this);
            mImgEditContainers[index].setTag(index);

            mTKMgrs[index] = TransKeyUtils.initTransKeyPad(this,0, mKeypadType,
                    mTextType,
                    label,
                    "",
                    max_length,
                    "",
                    mMinLength,
                    mAlertMsgs[index],
                    0,
                    true,
                    (FrameLayout) findViewById(R.id.keypad_container_safe_card),
                    mEditTexts[index],
                    (HorizontalScrollView) mLayouts[index].findViewById(R.id.keyscroll),
                    (LinearLayout) mLayouts[index].findViewById(R.id.keylayout),
                    (ImageButton) mLayouts[index].findViewById(R.id.clearall),
                    (RelativeLayout) findViewById(R.id.keypad_ballon_safe_card),
                    null,
                    false,
                    false);
        }

        ImageButton btnCancel = (ImageButton) findViewById(R.id.btn_cancel_safe_card);
        ImageButton btnDone = (ImageButton) findViewById(R.id.btn_done_safe_card);
        btnCancel.setOnClickListener(this);
        btnDone.setOnClickListener(this);
        
        //((ImageView) findViewById(R.id.btn_back)).setOnClickListener(this);
        //((TextView)findViewById(R.id.tv_title)).setText(title);
        //((TextView)findViewById(R.id.tv_label)).setText(label);

        if (mCationShow) {
            //((LinearLayout) findViewById(R.id.tv_caption_title)).setVisibility(View.VISIBLE);
            //LinearLayout mCaptionExpLayout = ((LinearLayout) findViewById(R.id.tv_caption_explain));
            //mCaptionExpLayout.setVisibility(View.VISIBLE);
            //mImgArrow = ((ImageView) findViewById(R.id.img_arrow));
            //mImgArrow.setOnClickListener(this);
        }

        if (mTKMgrs[0] != null) {
            mTKMgrs[0].showKeypad(mKeypadType);
        }
    }

    @Override
    public void onBackPressed() {
        if (mCurViewIndex == 1) {
            mCurViewIndex = 0;
        } else {
            if (mIsViewCtrlKeypad == true) {
                mTKMgrs[mOnClickIndex].finishTransKey(true);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //cipherTextBuf = savedInstanceState.getStringArray("cipherTextBuf");
        mCipherTextBuf = savedInstanceState.getStringArray(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
        mDummyTextBuf = savedInstanceState.getStringArray("dummyTextBuf");
        mCurViewIndex = savedInstanceState.getInt("curViewIndex");

        for(int i = 0; i < mEditTexts.length; i++) {
            setData(i, mCipherTextBuf[i], mDummyTextBuf[i]);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putStringArray("cipherTextBuf", mCipherTextBuf);
        outState.putStringArray("dummyTextBuf", mDummyTextBuf);
        outState.putInt("curViewIndex", mCurViewIndex);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        int tagValue = 0;
        Object tag = v.getTag();
        if(tag != null)
            tagValue = (Integer)tag;

        if ((id == R.id.keyscroll) && tagValue == 0) {
            if (mOnClickIndex == 1)
                mTKMgrs[1].finishTransKey(true);

            mEditTexts[0].requestFocus();
            showTransKeyPad(0, TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER);
            mOnClickIndex = 0;
            mIsViewCtrlKeypad = true;

        } else if ((id == R.id.keyscroll) && tagValue == 1) {
            if (mOnClickIndex == 0)
                mTKMgrs[0].finishTransKey(true);

            mEditTexts[1].requestFocus();
            showTransKeyPad(1, TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER);
            mOnClickIndex = 1;
            mIsViewCtrlKeypad = true;

        } else if (id == R.id.btn_cancel_safe_card) {
            Intent intent = new Intent();
            setResult(Activity.RESULT_CANCELED, intent);
            finish();

        } else if (id == R.id.btn_done_safe_card) {
            if (mDummyTextBuf[0] == null || mDummyTextBuf[0].length() != INPUT_NUM) {
                showMsgDialog(mAlertMsgs[0]);
            } else if (mDummyTextBuf[1] == null ||  mDummyTextBuf[1].length() != INPUT_NUM) {
                showMsgDialog(mAlertMsgs[1]);
            } else {
                Intent intent = new Intent();
                intent.putExtra(Const.SAFE_CARD_FIRST_KEY, mCipherTextBuf[0]);
                intent.putExtra(Const.SAFE_CARD_SECOND_KEY, mCipherTextBuf[1]);
                intent.putExtra(Const.SAFE_CARD_FIRST_SECURE_KEY, mTKMgrs[0].getSecureKey());
                intent.putExtra(Const.SAFE_CARD_SECOND_SECURE_KEY, mTKMgrs[1].getSecureKey());
                intent.putExtra(Const.SAFE_CARD_FIRST_LENGTH, mTKMgrs[0].getInputLength());
                intent.putExtra(Const.SAFE_CARD_SECOND_LENGTH, mTKMgrs[1].getInputLength());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        int id = v.getId();
        int tagValue = 0;
        Object tag = v.getTag();
        if(tag != null)
            tagValue = (Integer)tag;

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if ((id == R.id.keylayout  && tagValue == 0) || (id == R.id.keyscroll && tagValue == 0) || (id == R.id.editText && tagValue == 0)) {
                if(mOnClickIndex == 1)
                    mTKMgrs[1].finishTransKey(true);

                mEditTexts[0].requestFocus();
                mCipherTextBuf[0] = "";
                mDummyTextBuf[0] = "";
                showTransKeyPad(0, TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER);
                mOnClickIndex = 0;
                mIsViewCtrlKeypad = true;
                return true;

            } else if((id == R.id.keylayout  && tagValue == 1) || (id == R.id.keyscroll && tagValue == 1) || (id == R.id.editText && tagValue == 1)) {
                if (mOnClickIndex == 0)
                    mTKMgrs[0].finishTransKey(true);

                mEditTexts[1].requestFocus();
                mCipherTextBuf[1] = "";
                mDummyTextBuf[1] = "";
                showTransKeyPad(1, TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER);
                mOnClickIndex = 1;
                mIsViewCtrlKeypad = true;
                return true;
            }
        }
        return false;
    }

    @Override
    public void cancel(Intent data) {
        mIsViewCtrlKeypad = false;

        mTKMgrs[mOnClickIndex].ClearAllData();
        setData(mOnClickIndex, "", "");
    }

    @Override
    public void done(Intent data) {
        mIsViewCtrlKeypad = false;

        if (data == null) {
            return;
        }

        String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
        String dummyData = data.getStringExtra(TransKeyActivity.mTK_PARAM_DUMMY_DATA);
        byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
        //String HexSecureKey = toHexString(secureKey);// 이렇게 변환하여 사용
        int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);
        if (iRealDataLength == 0) {
            return;
        }

        setData(mOnClickIndex, cipherData, dummyData);
    }

    private void setData(int index, String cipherText, String dummyText) {
        mCipherTextBuf[index] = cipherText;
        mDummyTextBuf[index] = dummyText;
    }

    public void showTransKeyPad(int index, int keyPadType) {
        mTKMgrs[index].showKeypad(keyPadType);
    }

    private void showMsgDialog(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).create().show();
    }

    @Override
    public void input(int type) {
        Logs.e("input");
    }

    @Override
    public void minTextSizeCallback(){
        Logs.e("minTextSizeCallback() call");
    }

    @Override
    public void maxTextSizeCallback(){
        Logs.e("maxTextSizeCallback() call");
    }
}
