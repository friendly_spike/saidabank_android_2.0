package com.sbi.saidabank.activity.main2.openbanking;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.activity.transaction.ITransferSalaryActivity;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;

/**
 * Create by 20210119
 * 오픈뱅킹 리스트중 첫번째 아이템인 총 계좌정보  부분을 출력
 */

public class ViewHolderBanner extends RecyclerView.ViewHolder{
    private Context mContext;
    private RelativeLayout mLayoutBody;   //전체몸통
    private TextView mTvText;
    public static ViewHolderBanner newInstance(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main2_openbank_list_item_banner, parent, false);
        return new ViewHolderBanner(parent.getContext(),itemView);
    }

    public ViewHolderBanner(Context context, @NonNull View itemView) {
        super(itemView);
        mContext = context;
        mLayoutBody = itemView.findViewById(R.id.layout_body);
        mTvText = itemView.findViewById(R.id.tv_text);

        int radius = (int) Utils.dpToPixel(context, 20);
        mLayoutBody.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff", new int[]{ radius, radius, radius, radius }));
    }

    public void onBindView() {

        if(OpenBankDataMgr.getInstance().getSLRY_TRNF_YN().equalsIgnoreCase("N")){
            String msg = "거래실적을 한 번에 해결하는\n사이다뱅크 급여이체";
            Utils.setTextWithSpan(mTvText,msg,"사이다뱅크 급여이체", Typeface.BOLD,"#000000");
        }else{
            String msg = "급여이체 하기";
            Utils.setTextWithSpan(mTvText,msg,msg, Typeface.BOLD,"#000000");
        }

        mLayoutBody.setClickable(true);
        mLayoutBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //더블클릭 방어.
                mLayoutBody.setClickable(false);
                if(OpenBankDataMgr.getInstance().getSLRY_TRNF_YN().equalsIgnoreCase("N")){
                    Intent intent = new Intent(mContext, WebMainActivity.class);
                    String url = WasServiceUrl.TRA0200100.getServiceUrl();
                    intent.putExtra(Const.INTENT_MAINWEB_URL, url);
                    mContext.startActivity(intent);
                }else{
                    TransferUtils.initAndSyncSessionAndCheckAuthMethod((BaseActivity) mContext, true, new TransferUtils.OnCheckFinishListener() {
                        @Override
                        public void onCheckFinish() {
                            ITransferSalayMgr.getInstance().clearSalaryInfo();
                            Intent intent = new Intent(mContext, ITransferSalaryActivity.class);
                            mContext.startActivity(intent);
                        }
                    });

                }
            }
        });
    }

}
