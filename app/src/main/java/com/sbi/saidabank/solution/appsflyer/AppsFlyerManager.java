package com.sbi.saidabank.solution.appsflyer;

import android.annotation.SuppressLint;
import android.content.Context;

import com.appsflyer.AppsFlyerLib;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.JsonUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

import java.util.Map;

/**
 * AppsFlyerManager : AppsFlyer 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class AppsFlyerManager {

    @SuppressLint("StaticFieldLeak")
    private static AppsFlyerManager instance = null;
    private Context mContext;

    /**
     * Singleton instance 생성
     *
     * @param context 컨테스트
     * @return instance
     */
    public static AppsFlyerManager getInstance(Context context) {
        if (instance == null) {
            synchronized (AppsFlyerManager.class) {
                if (instance == null) {
                    instance = new AppsFlyerManager(context);
                }
            }
        }
        return instance;
    }

    /**
     * 생성자
     *
     * @param context 생성자
     */
    private AppsFlyerManager(Context context) {
        this.mContext = context;
    }

    /**
     * AppsFlyer 에 원하는 이벤트를 전달시 호출되는 함수
     * 대시보드에 집계되기 위한 이벤트 정보를 전달할 때 호출되는 함수로 웹 컨텐츠로 부터 전달받은 이벤트명과 이벤트값을 셋팅하여 전달한다.
     *
     * @param eventName
     * @param eventValues
     */
    public void sendAppsFlyerTrackEvent(String eventName, String eventValues) {
        if (DataUtil.isNull(eventName))
            return;
        MLog.line();
        MLog.i("Track Event Name :: " + eventName);
        MLog.line();
        String mbrNo = LoginUserInfo.getInstance().getMBR_NO();
        if (DataUtil.isNotNull(mbrNo)) {
            AppsFlyerLib.getInstance().setCustomerUserId(mbrNo);
        }
        AppsFlyerLib.getInstance().trackEvent(mContext, eventName, null);
    }

    /**
     * 웹 컨텐츠로 부터 전달받은 jsonString 객체를 Map 객체로 변환하기 위한 컨버터
     *
     * @param eventValues
     * @return Map
     */
    private Map<String, Object> convertToEventValuesMap(String eventValues) {
        try {
            return JsonUtils.jsonStringToMap(eventValues);
        } catch (Exception e) {
            MLog.e(e);
            return null;
        }
    }
}
