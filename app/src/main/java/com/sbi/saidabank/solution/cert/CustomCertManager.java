package com.sbi.saidabank.solution.cert;

import android.content.Context;

import com.initech.xsafe.cert.CertInfo;
import com.initech.xsafe.cert.CertificateManager;
import com.initech.xsafe.cert.INIXSAFEException;
import com.initech.xsafe.cert.KeyPadCipher;


/**
 * Saidabank_android
 * Class: CustomCertManager
 * Created by 950469 on 2018. 11. 6.
 * <p>
 * Description:
 * 인증서를 관리 하는 Instance Manager
 */
public class CustomCertManager {
    private static CustomCertManager instance = null;
    private CertificateManager mCertManager;

    /**
     * Singleton instance 생성
     *
     * @return instance
     */
    public static CustomCertManager getInstance() {
        if (instance == null) {
            synchronized (CustomCertManager.class) {
                if (instance == null) {
                    instance = new CustomCertManager();
                }
            }
        }
        return instance;
    }

    public static void clearInstance(){
        instance = null;
    }

    /**
     * 생성자
     */
    private CustomCertManager() {
        //Context를 받는다./**/
        CertificateManager.setIssuerDnFilter("CN");
        refreshCertManager();
    }

    private void refreshCertManager(){
        CertificateManager.reloadCertificates();
        /*
         * 인증서가 삭제되거나 폴더가 지워지는 경우가 있기에
         * 리스트 요청시 CertificateManager는 다시 할당 하도록 한다.
         * */
        if(mCertManager != null)
            mCertManager = null;
        mCertManager = new CertificateManager();
    }

    public CertificateManager getCertificateManager(){
        return mCertManager;
    }

    /**
     * 인증서 리스트를 리턴한다.
     * @return
     * 인증서 리스트
     */
    public String[] loadCertList() {

        // SD 저장소
        refreshCertManager();
        String[] sCertList = mCertManager.getUserCertLists();
        if (sCertList != null && sCertList.length > 0) {
            return sCertList;
        }

        return null;
    }

    public boolean checkPassword(Context context, int index, String passChiper) throws INIXSAFEException{

        return mCertManager.checkPassword(index, passChiper, KeyPadCipher.MODE_MTRANSKEY);
    }

    public boolean changePassword(int index, String oldPwdChiper, String newPwdChiper) throws INIXSAFEException{

        // 복호화 모드(NONE : 암호화 되어있지 않음, NFILTER : NFilter 복호화, SPINPAD : SPinPad 복호화,
        // 					MTRANSKEY : Raon mTransKey 복호화, MODE_EXTRUS : Extrus 복호화)
        return mCertManager.changePassword(index, oldPwdChiper, newPwdChiper, KeyPadCipher.MODE_MTRANSKEY);

    }

    /**
     * 인증서 삭제
     * @param index
     * @return
     */
    public boolean delCert(int index) {
        return mCertManager.deleteUserCert(index);
    }


    public CertInfo getCertStorageInfo(int index){
        //Firebase에러 - 인증서 인덱스 오류 수정
        if(index < 0) return null;
        return (CertInfo)mCertManager.CertStorage.get(index);
    }
}
