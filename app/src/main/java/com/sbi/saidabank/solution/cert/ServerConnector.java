package com.sbi.saidabank.solution.cert;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ServerConnector {
	private String mUrl;
	private boolean mIsHttps = false;
	private List<NameValuePair> mParameters = null;
	private static int sTimeout = 0;
	private static final int DEFAULT_TIMEOUT = 10000;	// default timeout = 10s
	private DefaultHttpClient httpClient;
	
	public ServerConnector(String url) throws MalformedURLException {
		this(url, DEFAULT_TIMEOUT);
	}
	
	public ServerConnector(String url, int timeoutMs) throws MalformedURLException {
		// Initialize fields
		mParameters = new ArrayList<NameValuePair>();
		mUrl = url;
		sTimeout = timeoutMs;
		URL rUrl = new URL(url);
		mIsHttps = (rUrl.getProtocol().equals("https")) ? true : false;
	}
	
	public void setURL(String url) throws MalformedURLException {
		// Initialize fields
		mParameters = new ArrayList<NameValuePair>();
		mUrl = url;
		URL rUrl = new URL(url);
		mIsHttps = (rUrl.getProtocol().equals("https")) ? true : false;
	}
	
	public void setParameter(String key, String value) {
		mParameters.add(new BasicNameValuePair(key, value));
	}
	
	public void setConnectionTimeout(int ms) {
		sTimeout = ms;
	}
	
	public String getResponse() throws ProtocolException {
		String retStr = null;
		StringBuffer sb = null;
		BufferedReader buf = null;
		try {
			// Now you can access an https URL without having the certificate in the truststore
			if (httpClient == null) {
				httpClient = getThreadSafeClient();
			}
			
			if (mIsHttps) {	// HTTPS
				// HTTPS 신뢰되지 않은 인증서 처리
				httpClient.getConnectionManager().getSchemeRegistry().unregister("https");
				KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
				trustStore.load(null, null);
				httpClient.getConnectionManager().getSchemeRegistry().register(new Scheme("https", new UnsafeSSLSocketFactory(trustStore), 443));
			}
			
			HttpPost post = new HttpPost(mUrl);
			
			// 파라미터 전송
		    UrlEncodedFormEntity ent = new UrlEncodedFormEntity(mParameters, HTTP.UTF_8);
		    post.setEntity(ent);
				
			// 응답
			HttpResponse response = httpClient.execute(post);
			HttpEntity entity = response.getEntity();
			
			if (entity.getContentType().toString().toUpperCase().indexOf("UTF-8") != -1) {
				buf = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), HTTP.UTF_8));
			} else {
				buf = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "EUC-KR"));
			}
			
			sb = new StringBuffer();
			
			for (int i = 0; (i = buf.read()) != -1; ) {
				sb.append((char)i);
			}

			retStr = sb.toString();
			
		} catch(Exception e) {
			System.err.println(e);
			throw new ProtocolException(e.toString());
			
		} finally {
			try { buf.close(); } catch(Exception ignored) {}
		}

		return retStr;
	}
	
	public static DefaultHttpClient getThreadSafeClient()  {

		DefaultHttpClient client = new DefaultHttpClient();
		ClientConnectionManager mgr = client.getConnectionManager();
		HttpParams params = client.getParams();
		HttpConnectionParams.setConnectionTimeout(params, sTimeout);
		HttpConnectionParams.setSoTimeout(params, sTimeout);
		client = new DefaultHttpClient(new ThreadSafeClientConnManager(params, mgr.getSchemeRegistry()), params);
		return client;
	}
	
	class UnsafeSSLSocketFactory extends SSLSocketFactory {
		SSLContext sslContext = SSLContext.getInstance("TLS");

		public UnsafeSSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
			super(truststore);
			TrustManager tm = new X509TrustManager() {

				public X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}

				public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
				}
			};
			sslContext.init(null, new TrustManager[] { tm }, null);
		}

		public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
			return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
		}

		public Socket createSocket() throws IOException {
			return sslContext.getSocketFactory().createSocket();
		}
		
	}
}
