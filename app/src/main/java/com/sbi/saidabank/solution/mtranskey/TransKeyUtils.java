package com.sbi.saidabank.solution.mtranskey;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.initech.xsafe.util.MTransKeyXSafeRandomKey;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.softsecurity.transkey.ITransKeyActionListener;
import com.softsecurity.transkey.ITransKeyActionListenerEx;
import com.softsecurity.transkey.ITransKeyCallbackListener;
import com.softsecurity.transkey.TransKeyActivity;
import com.softsecurity.transkey.TransKeyCipher;
import com.softsecurity.transkey.TransKeyCtrl;

/**
 * TransKeyUtils : TransKey Utils
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class TransKeyUtils {

    public static TransKeyCtrl initTransKeyPad(
            Context context, int index, int keyPadType, int textType, String label, String hint,
            int maxLength, String maxLengthMessage, int minLength, String minLengthMessage, int line3Padding, boolean bReArrange,
            FrameLayout keyPadView, EditText editView, HorizontalScrollView scrollView,
            LinearLayout inputView, ImageButton clearView, RelativeLayout ballonView, ImageView lastInput, boolean bUseAtmMode, boolean isUseAutoFocusing) {
        try {

            TransKeyCtrl mTkMngr = new TransKeyCtrl(context);

            //Activity 로 처리할 때 처럼 파라미터를 인텐트에 넣어서 처리.
            Intent newIntent = getIntentParam(context, keyPadType, textType, label, hint, maxLength, maxLengthMessage, minLength, minLengthMessage, line3Padding, 20, bUseAtmMode, isUseAutoFocusing);

            if (lastInput != null) {
                mTkMngr.init(newIntent, keyPadView, editView, scrollView, inputView, clearView, ballonView, lastInput);
            } else {
                mTkMngr.init(newIntent, keyPadView, editView, scrollView, inputView, clearView, ballonView);
            }

            mTkMngr.setReArrangeKeapad(bReArrange);
            mTkMngr.setTransKeyListener((ITransKeyActionListener) context);
            mTkMngr.setTransKeyListenerEx((ITransKeyActionListenerEx) context);
            mTkMngr.setTransKeyListenerCallback((ITransKeyCallbackListener) context);

            return mTkMngr;

        } catch (Exception e) {
            MLog.e(e);
        }
        return null;
    }


    public static Intent getIntentParam(
            Context context, int keyPadType, int textType, String label, String hint,
            int maxLength, String maxLengthMessage, int minLength, String minLengthMessage,
            int line3Padding, int reduceRate, boolean useAtmMode, boolean isUseAutoFocusing) {

        Intent newIntent = new Intent(context.getApplicationContext(), TransKeyActivity.class);

        // 빈자리에 아이콘 출력여부
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_CUSTOM_DUMMY, true);

        /**
         * 키패드 타입
         * TransKeyActivity.mTK_TYPE_KEYPAD_NUMBER			:	숫자전용
         * TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_LOWER	:	소문자 쿼티
         * TransKeyActivity.mTK_TYPE_KEYPAD_QWERTY_UPPER	:	대문자 쿼티
         * TransKeyActivity.mTK_TYPE_KEYPAD_ABCD_LOWER		:	소문자 순열자판
         * TransKeyActivity.mTK_TYPE_KEYPAD_ABCD_UPPER		:	대문자 순열자판
         * TransKeyActivity.mTK_TYPE_KEYPAD_SYMBOL			:	심벌자판
         */
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_TYPE, keyPadType);

        /**
         * 키보드가 입력되는 형태
         * TransKeyActivity.mTK_TYPE_TEXT_IMAGE 				:	보안 텍스트 입력
         * TransKeyActivity.mTK_TYPE_TEXT_PASSWORD 			    :	패스워드 입력
         * TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_EX 			:	마지막 글자 보여주는 패스워드 입력
         * mTK_TYPE_TEXT_PASSWORD_IMAGE                         :   Text Area에 이미지 * 표시
         * TransKeyActivity.mTK_TYPE_TEXT_PASSWORD_LAST_IMAGE	:	마지막 글자를 제외한 나머지를 *표시.
         */
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_INPUT_TYPE, textType);

        /**
         * securekey 설정 옵션
         * 	타입	:	TransKeyActivity.mTK_TYPE_CRYPT_SERVER	:	생성한 키를 할당하도록 설정 (서버와 키를 미리 정의하여 복호화)
         * 			TransKeyActivity.mTK_TYPE_CRYPT_LOCAL	:	로컬에서 키패드를 호출할때마다 내부적으로 키를 자동으로 생성하여 할당되도록 설정.
         * @code
         *        1. 생성한 키 설정.
         * 			byte[] secureKey = { 'M', 'O', 'B', 'I', 'L', 'E', 'T', 'R', 'A', 'N', 'S', 'K', 'E', 'Y', '1', '0' };
         * 			newIntent.putExtra(TransKeyActivity.mTK_PARAM_CRYPT_TYPE, TransKeyActivity.mTK_TYPE_CRYPT_SERVER);
         * 			newIntent.putExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY, secureKey);
         *		2. 내부적으로 키 자동생성
         *			newIntent.putExtra(TransKeyActivity.mTK_PARAM_CRYPT_TYPE, TransKeyActivity.mTK_TYPE_CRYPT_LOCAL);
         */
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_CRYPT_TYPE, TransKeyActivity.mTK_TYPE_CRYPT_SERVER);

        // 키패드입력화면의 입력필드 라벨
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_NAME_LABEL, label);

        // 입력필드에 스페이스바 입력을 무시
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_SPACE, false);

        // 라이센스 적용
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_LICENSE_FILE_NAME, "license_TouchEnmTranskeyCS_sbi");

        /**
         * 최대 입력 길이 설정 ( Default : 설정이 없을 경우 16글자까지 입력가능하도록 설정된다.)
         * 암호화한 데이터의 경우 보안성 강화 적용된 데이터의 경우 입력 글자수에 상관없이 최대 입력 길이 설정값만큼
         * 항상 데이터가 채워져서 암호화 데이터가 적용되므로 사용목적에 따라 적당한 값을 사용하는 것을 권장.
         */
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_INPUT_MAXLENGTH, maxLength);                  // Maxlength 설정
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_MAX_LENGTH_MESSAGE, maxLengthMessage);        // Maxlength 설정된 값보다 길이가 초과할 경우 보여줄 메세지 설정.
        if (DataUtil.isNotNull(minLengthMessage)) {
            newIntent.putExtra(TransKeyActivity.mTK_PARAM_INPUT_MINLENGTH, minLength);              // Minlength 설정
            newIntent.putExtra(TransKeyActivity.mTK_PARAM_MIN_LENGTH_MESSAGE, minLengthMessage);    // Minlength 설정된 값보다 길이가 미만일 경우 보여줄 메세지 설정.
        }

        // 해당 Hint 메시지를 보여준다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SET_HINT, hint);

        // Hint 테스트 사이즈를 설정한다.(단위 dip, 0이면 디폴트 크기로 보여준다.)
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SET_HINT_TEXT_SIZE, 0);

        // 커서를 보여준다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SHOW_CURSOR, true);

        // 에디트 박스안의 글자 크기를 조절한다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_EDIT_CHAR_REDUCE_RATE, reduceRate);

        // 심볼 변환 버튼을 비활성화 시킨다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_SYMBOL, false);

        // 심볼 변환 버튼을 비활성화 시킬 경우 팝업 메시지를 설정한다.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_SYMBOL_MESSAGE, "심볼키는 사용할 수 없습니다.");
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_MARGIN, line3Padding);

        // TalkBack 설정 시 음성이 나올 수 있도록 설정.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_TALKBACK, true);
        //newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_HIGHEST_TOP_MARGIN, 2);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_TALKBACK, true);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_SUPPORT_ACCESSIBILITY_SPEAK_PASSWORD, true);

        // 캡쳐방지 기능 활성/비활성을 설정해 주며,옵션을 사용하지않을때는 캡쳐방지가 비홗성상태로 설정됨.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_PREVENT_CAPTURE, true);

        // 마지막 글자가 *로 바뀌는 딜레이 시갂을 설정.
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_HIDE_TIMER_DELAY, 3);

        // 컨트롤분리키패드 사용시 애니메이션 사용여부를 설정
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_KEYPAD_ANIMATION, false);

        // 컨트롤분리키패드 사용시 ATM(키오스크)모드 사용여부
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_ATM_MODE, useAtmMode);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_SHIFT_OPTION, false);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_CLEAR_BUTTON, false);
        //newIntent.putExtra(TransKeyActivity.mTK_PARAM_SAMEKEY_ENCRYPT_ENABLE, true);?

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_LANGUAGE, TransKeyActivity.mTK_Language_Korean);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_NUMPAD_USE_CANCEL_BUTTON, false);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_QWERTY_USE_CANCEL_BUTTON, false);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_ALERTDIALOG_TITLE, "알림");
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_BUTTON_EFFECT, false);//풍선효과 제어

        //newIntent.putExtra(TransKeyActivity.mTK_PARAM_QWERTY_BUTTON_MARGIN, 1f);
        //newIntent.putExtra(TransKeyActivity.mTK_PARAM_NUMBER_BUTTON_MARGIN, 0.44f);

        // 드래그기능을 막고 최초 터치값으로 입력되도록 설정
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_DISABLE_DRAG_EVENT, true);

        // 순차적인 더미 이미지 다르게 표현하기위한 옵션
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_CUSTOM_DUMMY, true);

        // 커서를 이미지로 사용하는 옵션
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_CUSTOM_CURSOR, true);

        // 비대칭키 사용시 공개키 설정하는 옵션
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_RSA_PUBLICK_KEY, publicKey);

        // 쿼티 자판 높이 설정
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_QWERTY_HEIGHT, (float)1.5);

        // 넘버 자판 높이 설정
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_NUMBER_HEIGHT, (float)0.5);

        // 오토포커싱 설정
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_AUTO_FOCUSING, isUseAutoFocusing);

        // 시프트옵션
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_USE_SHIFT_OPTION, false);

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_KEYPAD_HIGHEST_TOP_MARGIN, 4);

        // 심볼키패드에서 불필요한 키 더미화
        // newIntent.putExtra(TransKeyActivity.mTK_PARAM_CUSTOM_DUMMY_STRING, "',\",\\,|");

        byte[] pbkdfKey = setRandomPbkdfKey().getBytes();//mTransRandomKey.getBytes();
        byte[] PBKDF2_SALT = {1, 2, 1, 6, 0, 1, 0, 1, 8, 1, 3, 7, 0, 6, 4, 2, 3, 1, 0, 1};  //랜덤 솔트값
        int PBKDF2_IT = 1024;

        newIntent.putExtra(TransKeyActivity.mTK_PARAM_PBKDF2_RANDKEY, pbkdfKey);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_PBKDF2_SALT, PBKDF2_SALT);
        newIntent.putExtra(TransKeyActivity.mTK_PARAM_PBKDF2_IT, PBKDF2_IT);

        return newIntent;
    }

    private static String setRandomPbkdfKey() {
        java.security.SecureRandom secureRandom = new java.security.SecureRandom();
        byte[] secureKey = new byte[10];
        secureRandom.nextBytes(secureKey);
        String transRandomKey = toHexString(secureKey);
        MTransKeyXSafeRandomKey.setSecureRandomKey(transRandomKey);
        return transRandomKey;
    }

    /**
     * byte[]를 hex문자열로 변환하는 함수
     *
     * @return hexstring
     */
    public static String toHexString(byte[] buf) {
        StringBuilder sb = new StringBuilder();
        for (byte val : buf) {
            sb.append(Integer.toHexString(0x0100 + (val & 0x00FF)).substring(1));
        }
        return sb.toString();
    }

    public static String decryptPlainDataFromCipherData(Intent data) {
        StringBuilder plainData = null;
        String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
        byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
        int iRealDataLength = data.getIntExtra(TransKeyActivity.mTK_PARAM_DATA_LENGTH, 0);
        try {
            TransKeyCipher tkc = new TransKeyCipher("SEED");
            tkc.setSecureKey(secureKey);
            byte[] pbPlainData = new byte[iRealDataLength];
            if (tkc.getDecryptCipherData(cipherData, pbPlainData)) {
                plainData = new StringBuilder(new String(pbPlainData));
                for (int i = 0; i < pbPlainData.length; i++)
                    pbPlainData[i] = 0x01;
            } else {
                // 복호화 실패
                plainData = new StringBuilder("plainData decode fail...");
            }
        } catch (Exception e) {
            MLog.e(e);
            plainData = new StringBuilder("plainData decode fail...");
        }
        return plainData.toString();
    }

    public static String getTransKeyPassCipherData(Intent data) {
        String passChiperData = Const.EMPTY;
        String cipherData = data.getStringExtra(TransKeyActivity.mTK_PARAM_CIPHER_DATA);
        byte[] secureKey = data.getByteArrayExtra(TransKeyActivity.mTK_PARAM_SECURE_KEY);
        try {
            TransKeyCipher tkc = new TransKeyCipher("SEED");
            tkc.setSecureKey(secureKey);
            passChiperData = tkc.getPBKDF2DataEncryptCipherData(cipherData);
        } catch (Exception e) {
            MLog.e(e);
        }
        return passChiperData;
    }
}
