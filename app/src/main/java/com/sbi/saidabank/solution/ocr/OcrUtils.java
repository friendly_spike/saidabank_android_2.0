package com.sbi.saidabank.solution.ocr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.JsonUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.selvasai.selvyocr.idcard.recognition.ImageRecognition;
import com.selvasai.selvyocr.idcard.recognizer.SelvyIDCardRecognizer;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * OcrUtils : 신분증 촬영 Utils
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class OcrUtils {

    public static String parsorIdcardData(ArrayList<String> resultText) {

        HashMap<String, Object> map = new HashMap<>();

        // 신분증 종류
        String typeString = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.TYPE.ordinal());
        map.put("idcard_type", typeString);

        // 면허번호
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String licenseNumber = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.LICENSE_NUMBER.ordinal());
            map.put("license_number", licenseNumber);
        }

        // 이름
        String name = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.NAME.ordinal());
        map.put("name", name);

        // 주민등록번호
        if (!typeString.equalsIgnoreCase(ImageRecognition.TYPE_ALIEN_CARD)) {
            String rrn = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.REGISTRATION_NUMBER.ordinal());
            map.put("RRN", rrn);
        }

        // 발급일
        String date = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.ISSUE_DATE.ordinal());
        map.put("issue_date", date);

        // 발급처
        if (!typeString.equalsIgnoreCase(ImageRecognition.TYPE_ALIEN_CARD)) {
            String oranization = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.ORGANIZATION.ordinal());
            map.put("oranization", oranization);
        }

        // Driver license ACFN
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String license_acfn = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.ACFN.ordinal());
            map.put("license_acfn", license_acfn);
        }

        // Driver license aptitude date
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String license_apitude_date = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.LICENSE_APTITUDE_DATE.ordinal());
            map.put("license_apitude_date", license_apitude_date);
        }

        // Driver license type
        if (typeString.equalsIgnoreCase(ImageRecognition.TYPE_DRIVER_LICENSE)) {
            String license_type = resultText.get(ImageRecognition.RECOG_FIELD_IDCARD.LICENSE_TYPE.ordinal());
            map.put("license_type", license_type);
        }

        String jsonStr = JsonUtils.mapToJson(map).toString();

        MLog.i("jsonStr >> " + jsonStr);

        return jsonStr;
    }

    public static Bitmap makeMaskImg(Intent data) {

        Bitmap idCardBitmap = null;

        // 신분증 이미지 영역
        byte[] encryptImage = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
        encryptImage =  SelvyIDCardRecognizer.decrypt(encryptImage, Const.ENCRYPT_KEY); // 이미지 복호화 추가


        // 주민번호 마스킹 영역
        Rect rrnRect = data.getParcelableExtra(CameraActivity.DATA_RRN_RECT);

        // 면허번호 마스킹 영역
        Rect licenseNumberRect = data.getParcelableExtra(CameraActivity.DATA_LICENSE_NUMBER_RECT);

        // 신분증 증명사진 영역
        Rect photoRect = data.getParcelableExtra(CameraActivity.DATA_PHOTO_RECT);

        if (DataUtil.isNull(encryptImage) || DataUtil.isNull(rrnRect) || DataUtil.isNull(licenseNumberRect)) {
            return null;
        } else {

            // 마스킹이미지
            // byte[] maskingImage;

            // 사진영역 이미지
            // byte[] faceImage;

            idCardBitmap = ImgUtils.byteArrayToBitmap(encryptImage);

            if (DataUtil.isNotNull(idCardBitmap) && DataUtil.isNotNull(rrnRect) && DataUtil.isNotNull(licenseNumberRect)) {

                Canvas c = new Canvas(idCardBitmap);
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(Color.BLACK);

                //주민번호 영역 마스킹
                c.drawRect(rrnRect, paint);

                // 면허번호 영역 마스킹
                double shiftLength = (licenseNumberRect.right - licenseNumberRect.left) * 0.5;
                licenseNumberRect.left = (int) (licenseNumberRect.left + shiftLength);
                licenseNumberRect.right = (int) (licenseNumberRect.right + shiftLength);
                c.drawRect(licenseNumberRect, paint);

                // 증명사진 영역 마스킹
                paint.setStyle(Paint.Style.STROKE);
                paint.setColor(Color.RED);
                c.drawRect(photoRect, paint);

                // 신분증 이미지에서 얼굴이미지를 잘라냄
                // Bitmap bitmapFace = ImgUtils.cropBitmap(idCardBitmap, photoRect);

                // maskingImage = OcrUtils.bitmapToByteArray(mIDCardBitmap);
                // faceImage = OcrUtils.bitmapToByteArray(bitmapFace);

                return idCardBitmap;
            }
        }
        return null;
    }
}
