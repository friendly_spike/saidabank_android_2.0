package com.sbi.saidabank.solution.ssenstone;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.util.Base64;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * StonePassUtils : 스톤패스 Utils
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class StonePassUtils {

    /**
     * 지문인식 지원 여부를 확인한다
     *
     * @param context 생성자
     * @return 지문인식 지원여부 값
     */
    public static int isUsableFingerprint(Context context) {
        // 지문 인식 지원 OS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            // 지원기기 검사오류
            if (fingerprintManager == null)
                return -4;
            // 지문 인식 하드웨어 존재 여부
            if (!fingerprintManager.isHardwareDetected())
                return -2;
            // 인증 지문이 등록되어 있는지 여부
            if (!fingerprintManager.hasEnrolledFingerprints())
                return -3;
        } else {
            // 지원버전아님(SDK 6.0이상 지원)
            return -1;
        }
        return 0;
    }

    /**
     * 지문인식 디바이스 존재 유무 확인
     *
     * @param context
     * @return 0이 아니면 지원 불가
     */
    public static int hasFingerprintDevice(Context context) {
        // 지문 인식 지원 OS
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            FingerprintManager fingerprintManager = (FingerprintManager) context.getSystemService(Context.FINGERPRINT_SERVICE);
            if (fingerprintManager == null)
                return -4;
            // 지문 인식 하드웨어 존재 여부
            if (!fingerprintManager.isHardwareDetected())
                return -2;
        } else {
            return -1;
        }
        return 0;
    }

    /**
     * 서버에 패턴, 지문 인식 값 등록, 인증, 해제 전문 생성
     *
     * @param op
     * @param userName 사용자 아이디
     * @param systemID 서버 아이디
     * @param bioType  패턴,
     * @param deviceID 디바이스 아이디
     * @return 서버에 전송될 전문
     */
    public static String genFidoRequestMsg(String op, String userName, String systemID, String token, String signData, String bioType, String deviceID) {
        try {
            JSONObject jsonContextMsg = new JSONObject();
            jsonContextMsg.put("MODEL", Build.MODEL);
            jsonContextMsg.put("userName", userName);
            jsonContextMsg.put("SYSTEMID", systemID);
            if (DataUtil.isNotNull(bioType)) {
                jsonContextMsg.put("BIOTYPE", bioType);
            }
            if (DataUtil.isNotNull(deviceID)) {
                if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
                    jsonContextMsg.put("DEVICE", deviceID);
                } else {
                    jsonContextMsg.put("DEVICEHASH", getSHA256(deviceID).toUpperCase());
                }
            }
            if (DataUtil.isNotNull(signData)) {
                String encodedStr = Base64.encodeToString(signData.getBytes(), Base64.NO_WRAP);
                jsonContextMsg.put("transaction", encodedStr);
            }
            if (DataUtil.isNotNull(token)) {
                jsonContextMsg.put("token", token);
            }

            JSONObject jsonRequestMsg = new JSONObject();
            jsonRequestMsg.put("op", op);
            jsonRequestMsg.put("context", jsonContextMsg.toString());

            return jsonRequestMsg.toString();

        } catch (Exception e) {
            MLog.e(e);
            return Const.EMPTY;
        }
    }

    /**
     * 문자를 암호화 한다
     *
     * @param str
     * @return 암호화된 문자
     */
    private static String getSHA256(String str) {
        try {
            MessageDigest sh = MessageDigest.getInstance("SHA-256");
            sh.update(str.getBytes());
            byte[] byteData = sh.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            MLog.e(e);
            return Const.EMPTY;
        }
    }

    /**
     * 스트링 base64 encoding
     *
     * @param secureKey
     * @return base64 encoding된 스트링
     */
    public static String getBase64Key(String secureKey) {
        return Base64.encodeToString(hexToBytes(secureKey), Base64.URL_SAFE);
    }

    /**
     * hex string -> byte
     *
     * @param str
     * @return byte값
     */
    public static byte[] hexToBytes(String str) {
        if (DataUtil.isNull(str) || str.length() < 2) {
            return null;
        }
        int len = str.length() / 2;
        byte[] buffer = new byte[len];
        for (int i = 0; i < len; i++) {
            buffer[i] = (byte) Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16);
        }
        return buffer;
    }

    /**
     * 서버에 패턴, 지문 인식 값 등록, 인증, 해제 전문 생성
     *
     * @param op
     * @param userName 사용자 아이디
     * @param systemID 서버 아이디
     * @return 서버에 전송될 전문
     */
    /**
     public static String genPinRequestMsg(String op, String userName,String pin, String systemID,String ssid, String token) {
     String ret = Const.EMPTY;
     try {
     JSONObject jsonRequest = new JSONObject();
     jsonRequest.put("ACMD", "op");
     jsonRequest.put("USERID", userName);
     jsonRequest.put("PIN", pin);
     jsonRequest.put("SYSTEMID", systemID);
     jsonRequest.put("AUTHTYPE", 0x1);
     jsonRequest.put("MULTIAUTH", 0x0);
     jsonRequest.put("SSID", ssid);
     jsonRequest.put("TOKEN", token);
     if (DataUtil.isNotNull(secureKey)) {
     jsonRequest.put("PINTYPE", "RAON");
     jsonRequest.put("PINDATATYPE", "R01");
     String strSecureKey = Base64.encodeToString(secureKey, Base64.URL_SAFE);
     jsonRequest.put("BASE64PINSECKEY", strSecureKey);
     }
     ret = jsonRequest.toString();
     } catch (Exception e) {
     MLog.e(e);
     }
     return ret;
     }
     */
}
