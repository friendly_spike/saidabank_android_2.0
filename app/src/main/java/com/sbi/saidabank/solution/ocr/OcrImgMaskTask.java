package com.sbi.saidabank.solution.ocr;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.AsyncTask;

import com.rosisit.idcardcapture.CameraActivity;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.define.Const;
import com.selvasai.selvyocr.idcard.recognizer.SelvyIDCardRecognizer;

/**
 * OcrImgMaskTask : 신분증 이미지의 마스킹 처리를 위한 TASK.
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class OcrImgMaskTask extends AsyncTask<Intent, Void, Boolean> {

    private Bitmap mIDCardBitmap;
    private OcrImgTaskListener mListener;

    public interface OcrImgTaskListener {
        void onEndOcrMasking(boolean result, Bitmap bmp);
    }

    public OcrImgMaskTask(OcrImgTaskListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        mListener.onEndOcrMasking(aBoolean, mIDCardBitmap);
    }

    @Override
    protected Boolean doInBackground(Intent... params) {

        Intent data = params[0];

        // 신분증 이미지 영역
        byte[] encryptImage = data.getByteArrayExtra(CameraActivity.DATA_ENCRYT_IMAGE_BYTE_ARRAY);
        encryptImage =  SelvyIDCardRecognizer.decrypt(encryptImage, Const.ENCRYPT_KEY); // 이미지 복호화 추가

        // 주민번호 마스킹 영역
        Rect rrnRect = data.getParcelableExtra(CameraActivity.DATA_RRN_RECT);

        // 면허번호 마스킹 영역
        Rect licenseNumberRect = data.getParcelableExtra(CameraActivity.DATA_LICENSE_NUMBER_RECT);

        // 신분증 증명사진 영역
        // Rect photoRect = data.getParcelableExtra(CameraActivity.DATA_PHOTO_RECT);

        if (DataUtil.isNull(encryptImage) || DataUtil.isNull(rrnRect) || DataUtil.isNull(licenseNumberRect)) {
            return false;
        } else {

            // 마스킹이미지
            // byte[] maskingImage;

            // 사진영역 이미지
            // byte[] faceImage;

            if (DataUtil.isNotNull(mIDCardBitmap) && !mIDCardBitmap.isRecycled()) {
                mIDCardBitmap.recycle();
            }

            mIDCardBitmap = ImgUtils.byteArrayToBitmap(encryptImage);

            if (DataUtil.isNotNull(mIDCardBitmap) && DataUtil.isNotNull(rrnRect) && DataUtil.isNotNull(licenseNumberRect)) {

                Canvas c = new Canvas(mIDCardBitmap);
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                paint.setColor(Color.BLACK);

                // 주민번호 영역 마스킹
                c.drawRect(rrnRect, paint);

                // 면허번호 영역 마스킹
                double shiftLength = (licenseNumberRect.right - licenseNumberRect.left) * 0.5;
                licenseNumberRect.left = (int) (licenseNumberRect.left + shiftLength);
                licenseNumberRect.right = (int) (licenseNumberRect.right + shiftLength);
                c.drawRect(licenseNumberRect, paint);

                // 증명사진 영역 마스킹
                // paint.setStyle(Paint.Style.STROKE);
                // paint.setColor(Color.RED);
                // c.drawRect(photoRect, paint);

                // 신분증 이미지에서 얼굴이미지를 잘라냄
                // Bitmap bitmapFace = ImgUtils.cropBitmap(mIDCardBitmap, photoRect);

                // maskingImage = OcrUtils.bitmapToByteArray(mIDCardBitmap);
                // faceImage = OcrUtils.bitmapToByteArray(bitmapFace);

                return true;
            }
        }
        return false;
    }
}
