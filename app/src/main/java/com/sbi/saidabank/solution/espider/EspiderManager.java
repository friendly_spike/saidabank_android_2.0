package com.sbi.saidabank.solution.espider;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Handler;
import android.os.Looper;

import com.heenam.espider.Engine;
import com.heenam.espider.EngineInterface;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.LicenseKey;
import com.sbi.saidabank.web.JavaScriptApi;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FilenameFilter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * EspiderManager : Espider 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class EspiderManager implements EngineInterface {

    @SuppressLint("StaticFieldLeak")
    private static EspiderManager instance = null;
    private Engine mEngine = null;
    private CertFileFilter mCertFileFilter;
    private ArrayList<HashMap<String, HashMap<String, String>>> mJobList; // 스크래핑 될 jobList
    private ArrayList<EspiderData> mJobResultList; //모듈조회시 결과 데이터
    private String mCertPath = "";
    private String mCertPassword = "";
    private String mIdentifyNo = "";
    private String mCn;
    private String mDn;
    private String mJavaCallBackName;
    private Activity mContext;
    private EspiderEventListener mListner = null;

    /**
     * 조회 요청 결과를 전달받을 리스너
     * 2Way 방식을 진행 할 때, 사용자 입력이 필요한 Alert을 생성한다
     * CapTcha(보안문자)를 입력받는 화면 구성이 필요하다
     * void onShowSecureNoDialog(final String url, final int threadCnt, final int jobIndex, final JSONObject paramEx);
     * 2Way 방식을 진행 할 때, 사용자 입력이 필요한 Alert을 생성한다
     * SMS 인증문자를 입력받는 화면 구성이 필요하다
     * void onShowSmsNoDialog(final String phoneNo, final int threadCnt, final int jobIndex, final JSONObject paramEx);
     */
    public interface EspiderEventListener {
        void onFinishEspider(String callbackName, JSONObject object);

        void onStatusEspider(String callbackName, int status, String msg);
    }

    public static EspiderManager getInstance(Activity context) {
        if (instance == null) {
            synchronized (EspiderManager.class) {
                if (instance == null) {
                    instance = new EspiderManager(context);
                }
            }
        }
        return instance;
    }

    public static void clearInstance() {
        if (instance == null)
            return;
        instance.clearEngine();
        instance.removeEspiderEventListener();
        instance = null;
    }

    private void clearEngine() {
        if (mEngine == null)
            return;
        mEngine.stopEngine();
        mEngine = null;
    }

    private EspiderManager(Activity context) {
        this.mContext = context;
        initList();
    }

    public void setEspiderEventListener(EspiderEventListener listner) {
        this.mListner = listner;
    }

    /**
     * 엔진에 실행중인 모듈List의 갯수를 엔진에 전달한다
     *
     * @param engine Espider Engine
     */
    @Override
    public int numberOfJobInEngine(Engine engine) {
        return mJobList.size();
    }

    /**
     * 실행할 로그인정보(loginInfo) 및 파라메터(paramInfo)를 엔진에 전달하는 Callback 함수
     *
     * @param engine   Espider Engine
     * @param jobIndex
     */
    @Override
    public HashMap<String, String> engineGetJob(Engine engine, int jobIndex) {
        MLog.d();
        if (mJobList.size() <= jobIndex)
            return null;
        HashMap<String, HashMap<String, String>> element = mJobList.get(jobIndex);
        if (element.containsKey(Engine.ENGINE_JOB_MODULE_KEY)) {
            return element.get(Engine.ENGINE_JOB_MODULE_KEY);
        }
        return null;
    }

    /**
     * 실행할 로그인정보(loginInfo) 및 파라메터(paramInfo)를 엔진에 전달하는 Callback 함수
     */
    @Override
    public String engineGetParam(Engine engine, int threadIndex, int jobIndex, String requireJSONString, boolean bSynchronous) {
        MLog.d();
        try {
            JSONObject requireJson = new JSONObject(requireJSONString);
            if (bSynchronous) {
                // 로그인정보(loginInfo) 엔진에 전달
                if (requireJson.has(Engine.ENGINE_JOB_PARAM_LOGIN_KEY)) {
                    JSONObject reqJobItem = (JSONObject) requireJson.get(Engine.ENGINE_JOB_PARAM_LOGIN_KEY);
                    HashMap<String, String> jobSourceItem = mJobList.get(jobIndex)
                            .get(Engine.ENGINE_JOB_PARAM_LOGIN_KEY);

                    Iterator<String> itr = jobSourceItem.keySet().iterator();
                    while (itr.hasNext()) {
                        String key = itr.next();
                        reqJobItem.put(key, jobSourceItem.get(key));
                    }
                }
                // 파라메터(paramInfo) 엔진에 전달
                if (requireJson.has(Engine.ENGINE_JOB_PARAM_INFO_KEY)) {
                    JSONObject reqJobItem = (JSONObject) requireJson.get(Engine.ENGINE_JOB_PARAM_INFO_KEY);
                    HashMap<String, String> jobSourceItem = mJobList.get(jobIndex)
                            .get(Engine.ENGINE_JOB_PARAM_INFO_KEY);
                    Iterator<String> itr = jobSourceItem.keySet().iterator();
                    while (itr.hasNext()) {
                        String key = itr.next();
                        reqJobItem.put(key, jobSourceItem.get(key));
                    }
                }
                if (requireJson.has(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY)) {
                    JSONObject reqJobItem = (JSONObject) requireJson.get(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY);
                    HashMap<String, String> jobSourceItem = mJobList.get(jobIndex)
                            .get(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY);
                    Iterator<String> itr = jobSourceItem.keySet().iterator();
                    while (itr.hasNext()) {
                        String key = itr.next();
                        reqJobItem.put(key, jobSourceItem.get(key));
                    }
                }
                MLog.i("requireJson >> " + requireJson.toString());
                return requireJson.toString();
            }
//            else {
//                if (requireJson.has(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY)) {
//                    JSONObject reqJobItem = requireJson.getJSONObject(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY);
//                    // 2Way로 처리 해야 하는 파라미터를 추가적으로 진행합니다. (Alert형태로 진행하였음.)
//                    // getParam2Way(reqJobItem, threadIndex, jobIndex);
//                } else {
//                    return null;
//                }
//            }
            return null;
        } catch (Exception e) {
            MLog.e(e);
        }
        return null;
    }

    /**
     * 엔진이 모듈을 실행 후 결과를 가져올 때 호출된다 (각 조회된 데이터를 처리한다)
     */
    @Override
    public void engineResult(Engine engine, int threadIndex, final int jobIndex, int error, String userError, final String errorMessage, String resultJsonString) {
        MLog.d();
        synchronized (EspiderManager.class) {
            EspiderData espiderData = mJobResultList.get(jobIndex);
            String code = String.valueOf(error & 0xFFFF);
            espiderData.setStatus(code);
            if (error != 0) {
                String result = DataUtil.isNotNull(errorMessage) ? errorMessage : code;
                espiderData.setResult(result);
            } else {
                espiderData.setResult(resultJsonString);
            }
            mJobResultList.set(jobIndex, espiderData);
        }
    }

    /**
     * 엔진시스템의 Error가 발생시 호출된다
     */
    @Override
    public void engineSystemError(Engine engine, int error, String errorMessage) {
        MLog.d();
        if (DataUtil.isNotNull(mListner))
            mListner.onStatusEspider(mJavaCallBackName, error, errorMessage);
    }

    /**
     * 엔진의 상태를 가져온다. (status 값이 0 이면 모든 작업이 완료상태)
     */
    @Override
    public void engineStatus(Engine engine, int status) {
        MLog.d();
        if (status == 0) {
            // 모듈리스트의 업무조회 완료 후 에러가 있을 경우 Error를 출력하는 alertDialog를 실행합니다.
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (mEngine != null)
                        mEngine.stopEngine();
                    if (mListner != null && !mContext.isFinishing()) {
                        //스크래핑은 쓰레드의 연속인걸로 알고 있는데.. 결과값 리턴할때는 UI쓰레드로 변경해줘야...
                        //웹에서 에러메세지 출력할때 Fatal에러가 발생하지 않을것이란 말이다.
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mListner.onFinishEspider(mJavaCallBackName, getResultJsonObject());
                                mListner.onStatusEspider(mJavaCallBackName, 0, Const.EMPTY);
                            }
                        });
                    }
                }
            });
        }
    }

    /**
     * 실행중인 모듈의 진행중인 상태를 status로 확인할 수 있습니다.(시작, 진행중, 완료 등등)
     */
    @Override
    public void engineJobStatus(Engine engine, int threadIndex, final int jobIndex, final int status) {
        MLog.d();
    }

    /**
     * 실행중인 모듈의 진행상태를 percent로 확인할 수 있습니다.(Progress를 처리 가능.)
     */
    @Override
    public void engineJobPercent(Engine engine, int threadIndex, int jobIndex, final int percent) {
        MLog.d();
    }

    /**
     * 사용 리스트를 초기화 한다
     */
    public void initList() {
        if (mJobList == null)
            mJobList = new ArrayList<>();
        else
            mJobList.clear();
        if (mJobResultList == null)
            mJobResultList = new ArrayList<>();
        else
            mJobResultList.clear();
    }

    /**
     * Engine 사용가능한 상태인지 확인
     *
     * @return 사용가능하면 true
     */
    public static boolean isEngineEnabled() {
        if (instance != null) {
            return (instance.mEngine != null);
        } else {
            return false;
        }
    }

    /**
     * Espider 이벤트 리스너를 해제한다.
     *
     * @param
     */
    public void removeEspiderEventListener() {
        mListner = null;
    }

    public List<EspiderData> getResultList() {
        return mJobResultList;
    }

    public void startEspider() {
        try {
            mEngine = Engine.getInstatnce(mContext);    // 엔진의 Instatnce를 가져옵니다.
            mEngine.setInterface(this);                 // 엔진 실행시 콜백 받을 Class
            mEngine.setLicenseKey(LicenseKey.ESPIDER_LICENSE_KEY); // 엔진 라이센스키
            MLog.line();
            MLog.i("ENGINE_VERSION >> " + mEngine.getVersion());
            MLog.i("ENGINE_DEVICE_APP_ID >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_APP_ID));
            MLog.i("ENGINE_DEVICE_APP_VERSION >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_APP_VERSION));
            MLog.i("ENGINE_DEVICE_UNIQUE_ID >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_UNIQUE_ID));
            MLog.i("ENGINE_DEVICE_UUID >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_UUID));
            MLog.i("ENGINE_DEVICE_MANUFACTURER >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_MANUFACTURER));
            MLog.i("ENGINE_DEVICE_MODEL >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_MODEL));
            MLog.i("ENGINE_DEVICE_OS_NAME >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_OS_NAME));
            MLog.i("ENGINE_DEVICE_OS_VERSION  >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_OS_VERSION));
            MLog.i("ENGINE_DEVICE_PLATFORM  >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_PLATFORM));
            MLog.i("ENGINE_DEVICE_PLATFORM_NAME  >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_PLATFORM_NAME));
            MLog.i("ENGINE_DEVICE_USER_NAME  >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_USER_NAME));
            MLog.i("ENGINE_DEVICE_LOCALE_NAME  >> " + mEngine.getDeviceInfo(Engine.ENGINE_DEVICE_LOCALE_NAME));
            MLog.line();
            mEngine.stopEngine();       // 기존 엔진이 실행중이면, 중단
            mEngine.setThreadCount(8);  // 스레드의 개수 설정
            mEngine.setAutoStop(true);  // 엔진 자동중단 실행 여부
            mEngine.startEngine();      // 엔진 기동
            mEngine.startJob();         // 추가된 모듈리스트 조회
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 건강보험 보험료납부확인
     *
     * @param startDate    조회 시작 년월 (yyyyMM)
     * @param endDate      사용용도 (yyyyMM)
     * @param isShowOrigin 원문 DATA 포함 여부 (default: 미포함)
     */
    public void setHealthInsurancePay(String startDate, String endDate, boolean isShowOrigin) {
        try {

            // 서비스 명세서를 확인 하여 실행 할 모듈정보를 세팅한다
            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0002");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010020");

            // 서비스 명세서를 확인하여 필요한 로그인 정보를 세팅한다
            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der")); // 조회할 인증서 signCert.der파일경로
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));   // 조회할 인증서 signPri.key파일경로
            loginInfo.put("reqCertPass", mCertPassword); // 인증서 패스워드
            loginInfo.put("reqIdentity", mIdentifyNo);   // 주민등록번호

            // 서비스 명세서를 확인하여 업무에 필요한 추가정보 파라미터 정보를 세팅한다
            HashMap<String, String> paramInfo = new HashMap<>();
            paramInfo.put("reqUseType", "00");
            paramInfo.put("reqUsePurposes", "02");
            paramInfo.put("commStartDate", startDate);
            paramInfo.put("commEndDate", endDate);
            paramInfo.put("reqOriginDataYN", isShowOrigin ? "1" : "0");

            // 모듈 실행될 전체 데이터
            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            // 모듈 실행리스트에 추가
            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_HEALTH_PAY);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 건강보험 자격득실확인
     *
     * @param isShowJumin  주민번호 뒷자리 공개여부
     * @param isShowOrigin 원문 DATA 포함 여부 (default: 미포함)
     */
    public void setHealthInsuranceQualification(boolean isShowJumin, boolean isShowOrigin) {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0002");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010010");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            HashMap<String, String> paramInfo = new HashMap<>();
            paramInfo.put("reqUseType", "0");
            paramInfo.put("reqIsIdentityViewYn", isShowJumin ? "1" : "0");
            paramInfo.put("reqOriginDataYN", isShowOrigin ? "1" : "0");

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_HEALTH_QUALIFICATION);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 국민연금 가입자증명 조회
     */
    public void setNationalPensionSubscribers() {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "000050");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, new HashMap<String, String>());
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_NATIONAL_SUBSCRIBERS);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 국민연금 가입증명서 조회
     */
    public void setNationalPensionJoin() {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010040");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, new HashMap<String, String>());
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_NATIONAL_PAY);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 국민연금 연금산정용 가입내역확인서 조회
     */
    public void setNationalPensionJoinHistory() {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "PP");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010060");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            loginInfo.put("reqIdentity", mIdentifyNo);

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, new HashMap<String, String>());
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 국세청 사업자 등록증명
     *
     * @param identity      사업자번호
     * @param isShowJumin   주민번호 뒷자리 공개여부
     * @param isShowAddress 주소 공개여부
     * @param isShowOrigin  원문 DATA 포함 여부 (default: 포함)
     */
    public void setRevenueBusinessRegister(String identity, boolean isShowJumin, boolean isShowAddress, boolean isShowOrigin) {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010030");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);

            HashMap<String, String> paramInfo = new HashMap<>();
            if (DataUtil.isNotNull(identity))
                paramInfo.put("reqIdentity", identity);
            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");
            paramInfo.put("reqIsIdentityViewYn", isShowJumin ? "1" : "0");
            paramInfo.put("reqIsAddrViewYn", isShowAddress ? "1" : "0");
            paramInfo.put("reqOriginDataYN", isShowOrigin ? "1" : "0");

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_REVENUE_REGISTER);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 국세청 소득금액 증명
     *
     * @param startDate     조회 시작 년월 - yyyy (작년기준 5년 이전)
     * @param endDate       조회 종료 년월 - yyyy (작년기준 5년 이전)
     * @param isShowJumin   주민번호 뒷자리 공개여부
     * @param isShowAddress 주소 공개여부
     * @param isShowOrigin  원문 DATA 포함 여부 (default: 포함)
     */
    public void setQualificationIncome(String username, String usertype, String startDate, String endDate, boolean isShowJumin, boolean isShowAddress, boolean isShowOrigin) {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "012000");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);
            // 비회원인증 시 주민번호, 이름 필수
            loginInfo.put("reqIdentity", mIdentifyNo);
            loginInfo.put("reqUserName", username);

            // 근로소득자용
            HashMap<String, String> paramInfo = new HashMap<>();
            paramInfo.put("reqProofType", usertype);
            paramInfo.put("reqSearchStartYear", startDate);
            paramInfo.put("reqSearchEndYear", endDate);
            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");
            paramInfo.put("reqIsIdentityViewYn", isShowJumin ? "1" : "0");
            paramInfo.put("reqIsAddrViewYn", isShowAddress ? "1" : "0");
            paramInfo.put("reqOriginDataYN", isShowOrigin ? "1" : "0");

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_INCOME_QUALIFICATION);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 국세청 부가세과세표준증명
     *
     * @param identity      사업자번호
     * @param startDate     조회 시작 년월 - yyyyMM ([yyyy] 1분기: 5년전 ~ 작년, 그외: 4년전 ~ 금년, [MM] 1기:"01", 2기:"07")
     * @param endDate       조회 종료 년월 - yyyyMM ([yyyy] 1분기: 5년전 ~ 작년, 그외: 4년전 ~ 금년, [MM] 1기:"01", 2기:"07")
     *                      2013년 이후분에 대한 간이과세자의 과세표준증명원은 과세기간을 "1기"로 선택
     * @param isShowJumin   주민번호 뒷자리 공개여부
     * @param isShowAddress 주소 공개여부
     * @param isShowOrigin  원문 DATA 포함 여부 (default: 포함)
     */
    public void setRevenueTax(String identity, String startDate, String endDate, boolean isShowJumin, boolean isShowAddress, boolean isShowOrigin) {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010040");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);

            HashMap<String, String> paramInfo = new HashMap<>();
            paramInfo.put("commStartDate", startDate);
            paramInfo.put("commEndDate", endDate);
            if (DataUtil.isNotNull(identity))
                paramInfo.put("reqIdentity", identity);
            paramInfo.put("reqIsIdentityViewYn", isShowJumin ? "1" : "0");
            paramInfo.put("reqIsAddrViewYn", isShowAddress ? "1" : "0");
            paramInfo.put("reqOriginDataYN", isShowOrigin ? "1" : "0");
            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_REVENUE_TAX);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 부가가치세 면세사업자 수입금액증명
     *
     * @param identity    사업자번호
     * @param date        조회년도
     * @param isShowJumin 주민번호 뒷자리 공개여부
     */
    public void setRevenueTaxIncome(String identity, String date, boolean isShowJumin) {
        try {

            HashMap<String, String> moduleInfo = new HashMap<>();
            moduleInfo.put(Engine.ENGINE_JOB_COUNTRY_KEY, "KR");
            moduleInfo.put(Engine.ENGINE_JOB_ORGANIZATION_KEY, "NT");
            moduleInfo.put(Engine.ENGINE_JOB_SUBORGANIZATION_KEY, "0001");
            moduleInfo.put(Engine.ENGINE_JOB_MODULECODE_KEY, "010050");

            HashMap<String, String> loginInfo = new HashMap<>();
            loginInfo.put("reqCertFile", getCertFileFullPath(mCertPath, "signCert.der"));
            loginInfo.put("reqKeyFile", getCertFileFullPath(mCertPath, "signPri.key"));
            loginInfo.put("reqCertPass", mCertPassword);

            HashMap<String, String> paramInfo = new HashMap<>();
            paramInfo.put("reqSearchStartYear", date);
            paramInfo.put("reqSearchEndYear", date);
            if (DataUtil.isNotNull(identity))
                paramInfo.put("reqIdentity", identity);
            paramInfo.put("reqIsIdentityViewYn", isShowJumin ? "1" : "0");
            paramInfo.put("reqUsePurposes", "07");
            paramInfo.put("reqSubmitTargets", "01");

            HashMap<String, HashMap<String, String>> executeModule = new HashMap<>();
            executeModule.put(Engine.ENGINE_JOB_MODULE_KEY, moduleInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_LOGIN_KEY, loginInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAM_INFO_KEY, paramInfo);
            executeModule.put(Engine.ENGINE_JOB_PARAMEXT_INFO_KEY, new HashMap<String, String>());

            mJobList.add(executeModule);

            EspiderData espiderData = new EspiderData();
            espiderData.setType(Const.ESPIDER_TYPE_REVENUE_TAX);
            mJobResultList.add(espiderData);

        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 요청 타입에 따른 코드값 return
     *
     * @param type 요청 타입
     * @return 요청타입에 따른 코드
     */
    private String getEspiderDataCode(int type) {
        switch (type) {
            // 건강보험 납부
            case Const.ESPIDER_TYPE_HEALTH_PAY:
                return Const.ESPIDER_TYPE_HEALTH_PAY_CODE;
            // 건강보험 자격득실
            case Const.ESPIDER_TYPE_HEALTH_QUALIFICATION:
                return Const.ESPIDER_TYPE_HEALTH_QUALIFICATION_CODE;
            // 국민연금 연금정산용 가입내역
            case Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY:
                return Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY_CODE;
            // 국세청 부가세과세표준증명
            case Const.ESPIDER_TYPE_REVENUE_TAX:
                return Const.ESPIDER_TYPE_REVENUE_TAX_CODE;
            // 국세청 소득금액 증명
            case Const.ESPIDER_TYPE_INCOME_QUALIFICATION:
                return Const.ESPIDER_TYPE_INCOME_QUALIFICATION_CODE;
            default:
                return Const.EMPTY;
        }
    }

    /**
     * 조회결과를 JsonObject로 정리하여 return
     *
     * @return 조회결과
     */
    public JSONObject getResultJsonObject() {
        JSONObject jsonobj = new JSONObject();
        JSONArray jsonarray = new JSONArray();
        try {
            jsonobj.put(Const.ESPIDER_STATUS, Const.ESPIDER_STATUS_COMPLETE);
            if (DataUtil.isNotNull(mCn) && DataUtil.isNotNull(mDn)) {
                jsonobj.put("cn", mCn);
                jsonobj.put("dn", mDn);
            }
            if (DataUtil.isNotNull(mJobResultList) && !mJobResultList.isEmpty()) {
                for (Object obj : mJobResultList) {
                    JSONObject tmpjosobj = new JSONObject();
                    EspiderData espiderdata = (EspiderData) obj;
                    // error인 경우 errorCode 항목을 추가
                    if (!"0".equals(espiderdata.getStatus())) {
                        tmpjosobj.put("errorCode", espiderdata.getStatus());
                    }
                    tmpjosobj.put("svc_id", getEspiderDataCode(espiderdata.getType()));
                    tmpjosobj.put("data", espiderdata.getResult());
                    jsonarray.put(tmpjosobj);
                }
            } else {
                // resultdata가 없을 경우 공백으로 처리
                JSONObject tmpjosobj = new JSONObject();
                tmpjosobj.put("svc_id", Const.EMPTY);
                tmpjosobj.put("data", Const.EMPTY);
                jsonarray.put(tmpjosobj);
            }
            jsonobj.put("svc_list", jsonarray);
        } catch (JSONException e) {
            MLog.e(e);
        }
        return jsonobj;
    }

    public int startEspider(int javaApiNum, String javaCallBack, String certpass, String certpath, String id, String svcdata, String cn, String dn) {
        mJavaCallBackName = javaCallBack;
        mCn = cn;
        mDn = dn;
        switch (javaApiNum) {
            case JavaScriptApi.API_600:
                return startEspider_API600(certpass, certpath, id, svcdata);
            case JavaScriptApi.API_601:
                return startEspider_API601(certpass, certpath, id, svcdata);
            default:
                break;
        }
        return Const.ESPIDER_TYPE_RETURN_FAIL;
    }

    /**
     * 자바스크립트 인터페이스 API 600번 요청 처리
     *
     * @param certpass 인증서 암호
     * @param certpath 인증서 경로
     * @param id       주민번호
     * @param svcdata  서버로부터 요청받은 jsondata
     * @return ESPIDER_TYPE_RETURN_SUCCESS : 조회요청 성공, ESPIDER_TYPE_RETURN_FAIL : 조회 요청 실패
     */
    private int startEspider_API600(String certpass, String certpath, String id, String svcdata) {
        MLog.d();

        boolean hasHQC = false;
        JSONObject jsonobj;
        JSONArray jsonarray;
        ArrayList<String> startDate = new ArrayList<>();
        ArrayList<String> endDate = new ArrayList<>();

        initList();
        setPassword(certpass);
        setCertPath(certpath);
        setIdentifyNo(id);

        try {
            if (DataUtil.isNotNull(svcdata)) {
                jsonobj = new JSONObject(svcdata);
                jsonarray = jsonobj.optJSONArray("svc_list");
                if (DataUtil.isNotNull(jsonarray)) {
                    MLog.i("Data >> " + jsonarray.toString());
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject obj = jsonarray.getJSONObject(i);
                        if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_HEALTH_PAY_CODE)) {
                            startDate.add(obj.optString("start_date"));
                            endDate.add(obj.optString("end_date"));
                        } else if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_HEALTH_QUALIFICATION_CODE)) {
                            hasHQC = true;
                        }
                    }
                } else {
                    return Const.ESPIDER_TYPE_RETURN_FAIL;
                }
            } else {
                return Const.ESPIDER_TYPE_RETURN_FAIL;
            }
        } catch (JSONException e) {
            MLog.e(e);
            return Const.ESPIDER_TYPE_RETURN_FAIL;
        }

        for (int i = 0; i < startDate.size(); i++) {
            setHealthInsurancePay(startDate.get(i), endDate.get(i), false);
        }

        // 건강보험 자격득실
        if (hasHQC)
            setHealthInsuranceQualification(true, false);

        // espider start
        startEspider();

        return Const.ESPIDER_TYPE_RETURN_SUCCESS;
    }

    /**
     * 자바스크립트 인터페이스 API 601번 요청 처리
     *
     * @param certpass 인증서 암호
     * @param certpath 인증서 경로
     * @param id       주민번호
     * @param svcdata  서버로부터 요청받은 jsondata
     * @return ESPIDER_TYPE_RETURN_SUCCESS : 조회요청 성공, ESPIDER_TYPE_RETURN_FAIL : 조회 요청 실패
     */
    private int startEspider_API601(String certpass, String certpath, String id, String svcdata) {
        MLog.d();

        boolean hasNPHC = false;
        JSONObject jsonobj;
        JSONArray jsonarray;
        ArrayList<String> alKrppStartdate = new ArrayList<>();
        ArrayList<String> alKrppEnddate = new ArrayList<>();
        ArrayList<String> alKrntStartdate = new ArrayList<>();
        ArrayList<String> alKrntEnddate = new ArrayList<>();
        ArrayList<String> alKrntUserType = new ArrayList<>();
        String userName;

        initList();
        setPassword(certpass);
        setCertPath(certpath);
        setIdentifyNo(id);

        try {
            if (DataUtil.isNotNull(svcdata)) {
                jsonobj = new JSONObject(svcdata);
                userName = jsonobj.optString("user_name");
                jsonarray = jsonobj.optJSONArray("svc_list");
                if (DataUtil.isNotNull(jsonarray)) {
                    MLog.i("Data >> " + jsonarray.toString());
                    for (int i = 0; i < jsonarray.length(); i++) {
                        JSONObject obj = jsonarray.getJSONObject(i);
                        if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_HEALTH_PAY_CODE)) {
                            alKrppStartdate.add(obj.optString("start_date"));
                            alKrppEnddate.add(obj.optString("end_date"));
                        } else if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_INCOME_QUALIFICATION_CODE)) {
                            alKrntStartdate.add(obj.optString("start_date"));
                            alKrntEnddate.add(obj.optString("end_date"));
                            alKrntUserType.add(obj.optString("reqProofType"));
                        } else if (obj.optString("svc_id").equals(Const.ESPIDER_TYPE_NATIONAL_PAY_HISTORY_CODE)) {
                            hasNPHC = true;
                        }
                    }
                } else {
                    return Const.ESPIDER_TYPE_RETURN_FAIL;
                }
            } else {
                return Const.ESPIDER_TYPE_RETURN_FAIL;
            }
        } catch (JSONException e) {
            MLog.e(e);
            return Const.ESPIDER_TYPE_RETURN_FAIL;
        }

        for (int i = 0; i < alKrppStartdate.size(); i++) {
            setHealthInsurancePay(alKrppStartdate.get(i), alKrppEnddate.get(i), false);
        }

        // 국민연금 연금정산용 가입내역 확인
        if (hasNPHC)
            setNationalPensionJoinHistory();

        // 국세청 소득금액 증명 2년
        for (int i = 0; i < alKrntStartdate.size(); i++) {
            setQualificationIncome(userName, alKrntUserType.get(i), alKrntStartdate.get(i), alKrntEnddate.get(i), true, true, false);
        }

        // espider start
        startEspider();

        return Const.ESPIDER_TYPE_RETURN_SUCCESS;
    }

    /**
     * 특정 파일이 특정경로에 존재하는지
     *
     * @param filePath 파일 경로
     * @param fileName 파일 이름
     * @return 파일 경로
     */
    private String getCertFileFullPath(String filePath, String fileName) {
        String result = Const.EMPTY;
        try {
            File file = new File(filePath);
            if (mCertFileFilter == null) {
                mCertFileFilter = new CertFileFilter();
            }
            mCertFileFilter.setSearchFileName(fileName);
            File[] dirFiles = file.listFiles(mCertFileFilter);
            if (dirFiles != null && dirFiles.length > 0) {
                result = dirFiles[0].getAbsolutePath();
            }
        } catch (Exception e) {
            MLog.e(e);
            return result;
        }
        return result;
    }

    private class CertFileFilter implements FilenameFilter {

        private String searchFileName = Const.EMPTY;

        public void setSearchFileName(String name) {
            searchFileName = name;
        }

        @Override
        public boolean accept(File dir, String name) {
            boolean isAccept = false;
            if (DataUtil.isNotNull(searchFileName)) {
                isAccept = searchFileName.equalsIgnoreCase(name);
            }
            return isAccept;
        }
    }

    public String getCertPath() {
        return mCertPath;
    }

    public void setCertPath(String certPath) {
        mCertPath = certPath;
    }

    public void setPassword(String pw) {
        mCertPassword = pw;
    }

    public void setIdentifyNo(String num) {
        mIdentifyNo = num;
    }

    @SuppressLint("SimpleDateFormat")
    public static boolean validateDateFormat(String dateToValdate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMM");
        formatter.setLenient(false);
        try {
            Date parsedDate = formatter.parse(dateToValdate);
            MLog.i("Validate Format Date >> " + formatter.format(parsedDate));
        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
