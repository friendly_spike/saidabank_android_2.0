package com.sbi.saidabank.solution.jex;

import android.text.TextUtils;

import com.webcash.sws.secure.AESUtils;
import com.webcash.sws.secure.HexUtils;

/**
 * JexAESEncrypt : Jex 암/복호화
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class JexAESEncrypt {

    private static final byte[] keyValue = new byte[]{
            0x64, 0x70, 0x74, 0x6D, 0x71, 0x6C, 0x64, 0x6B, 0x64, 0x6C, 0x74,
            0x6B, 0x64, 0x6C, 0x65, 0x6B, 0x71, 0x6F, 0x64, 0x7A, 0x6D
    };

    private static String getKeyStr() {
        return new String(keyValue);
    }

    public static String encrypt(String plainText) throws Exception {
        //아래 코드 지우지 말것! 공백값이 들어올때 이상한 값으로 암호화 하는데..
        //웹 젝스 라이브러리에서 에러를 토해냄.
        if(TextUtils.isEmpty(plainText)) return "";

        String cipherKey = SecureString.encrypt(getKeyStr()).toLowerCase().substring(10, 26);
        cipherKey = HexUtils.toHex(cipherKey.getBytes());
        return AESUtils.encryptTypePKCS5Padding(cipherKey, plainText);
    }

    public static String decrypt(String encText) throws Exception {
        //아래 코드 지우지 말것! 공백값이 들어올때 이상한 값으로 암호화 하는데..
        //웹 젝스 라이브러리에서 에러를 토해냄.
        if(TextUtils.isEmpty(encText)) return "";

        String cipherKey = SecureString.encrypt(getKeyStr()).toLowerCase().substring(10, 26);
        cipherKey = HexUtils.toHex(cipherKey.getBytes());
        return AESUtils.decryptTypePKCS5Padding(cipherKey, encText);
    }
}
