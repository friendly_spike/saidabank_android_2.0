package com.sbi.saidabank.solution.jex;

import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;

import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * SecureString : 암호화 String Util
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class SecureString {

    // 암호화 키 byte[]
    private byte[] mCryptKey = new byte[64];

    // 암호화 데이터 byte[]
    private byte[] mBuff = null;

    /**
     * 생성자
     *
     * @param s 암호화할 평문 String
     */
    public SecureString(String s) {
        initKey();
        mBuff = change(s.getBytes());
    }

    /**
     * 암호화 키 초기화 (생성)
     */
    private final void initKey() {
        SecureRandom random = new SecureRandom();
        random.nextBytes(mCryptKey);
    }

    /**
     * 암호화 byte[] -> String (복호화)
     *
     * @return 평문 String
     */
    public String toString() {
        return new String(change(mBuff));
    }

    /**
     * 데이터 변환 (암/복호화)
     *
     * @param buff 변환할 데이터 byte[]
     * @return 변환 데이터 반환
     */
    private final byte[] change(byte[] buff) {
        byte[] result = new byte[buff.length];
        int nKey = 0;
        int nSize = buff.length;
        for (int i = 0; i < nSize; i++) {
            result[i] = (byte) (buff[i] ^ mCryptKey[nKey++]);
            if (nKey >= mCryptKey.length) {
                nKey = 0;
            }
        }
        return result;
    }

    /**
     * SHA256 패턴 암호화
     *
     * @param planText
     * @return
     */
    public static String encrypt(String planText) {
        try {
            MessageDigest md = MessageDigest.getInstance(Const.SHA_256);
            md.update(planText.getBytes());
            byte[] byteData = md.digest();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            StringBuilder hexString = new StringBuilder();
            for (int i = 0; i < byteData.length; i++) {
                String hex = Integer.toHexString(0xff & byteData[i]);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (Exception e) {
            MLog.e(e);
            return Const.EMPTY;
        }
    }
}
