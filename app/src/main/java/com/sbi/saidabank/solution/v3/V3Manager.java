package com.sbi.saidabank.solution.v3;

import android.annotation.SuppressLint;
import android.content.Context;

import com.ahnlab.v3mobileplus.interfaces.V3MobilePlusCtl;
import com.ahnlab.v3mobileplus.interfaces.V3MobilePlusResultListener;
import com.sbi.saidabank.common.util.DataUtil;

/**
 * V3Manager : V3 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class V3Manager implements V3MobilePlusResultListener {

    @SuppressLint("StaticFieldLeak")
    private static V3Manager instance = null;
    private V3MobilePlusCtl mV3MPlusCtl = null;
    private String mLicenseKey = null;
    private boolean mIsNotInstall = false;
    private boolean mV3Started = false;
    private Context mContext;
    private V3CompleteListener mListner = null;

    public interface V3CompleteListener {
        void onCompleteV3();
    }

    public static V3Manager getInstance(Context context) {
        if (instance == null) {
            synchronized (V3Manager.class) {
                if (instance == null) {
                    instance = new V3Manager(context);
                }
            }
        }
        return instance;
    }

    private V3Manager(Context context) {
        this.mContext = context;
        mV3MPlusCtl = V3MobilePlusCtl.getInstance(mContext);
        mV3MPlusCtl.setIntroType(V3MobilePlusCtl.INTRO_TYPE_TOAST);
    }

    public void setV3CompleteListener(V3CompleteListener listener) {
        this.mListner = listener;
    }

    /**
     * V3 Mobile plus 2.0 설치가 완료 되면 호출된다
     */
    @Override
    public void OnInstallCompleted() {
        mIsNotInstall = false;
        if (DataUtil.isNotNull(mV3MPlusCtl)) {
            mV3MPlusCtl.RmCtlV3MobilePlus(V3MobilePlusCtl.RMCTLV3_CMD_START, mLicenseKey);
        }
    }

    /**
     * V3 Mobile Plus 2.0 제품이 실행되면 호출된다
     */
    @Override
    public void OnV3MobilePlusStarted() {
        // 제품 실행된 이후에 필요한 코드가 있으면 이곳에 선언한다.
        if (DataUtil.isNotNull(mListner) && !mV3Started) {
            mListner.onCompleteV3();
            mV3Started = true;
        }
    }

    @Override
    public void OnPatchState(boolean aPatchState) {
        // 패치가 존재할 경우
        if (aPatchState) {
            goProductMarket();
        }
    }

    /**
     * V3 Mobile plus 2.0 을 시작한다
     *
     * @param aLicenseKey
     * @return int v3 함수 리턴값
     */
    public int startV3MobilePlus(String aLicenseKey) {
        mV3Started = false;
        int ret = V3MobilePlusCtl.ERR_FAILURE;
        if (DataUtil.isNull(mV3MPlusCtl)) {
            return ret;
        }
        mLicenseKey = aLicenseKey;
        mV3MPlusCtl.registerResultListener(instance);
        ret = mV3MPlusCtl.RmCtlV3MobilePlus(V3MobilePlusCtl.RMCTLV3_CMD_START, mLicenseKey);
        if (V3MobilePlusCtl.ERR_NOT_INSTALLED == ret) {
            mIsNotInstall = true;
        }
        return ret;
    }

    /**
     * V3 Mobile plus 2.0 을 종료한다
     *
     * @return int  v3 함수 리턴값
     */
    public int stopV3MobilePlus() {
        int ret = -1;
        if (DataUtil.isNotNull(mV3MPlusCtl)) {
            ret = mV3MPlusCtl.RmCtlV3MobilePlus(V3MobilePlusCtl.RMCTLV3_CMD_STOP, mLicenseKey);
            mV3MPlusCtl.unRegisterResultListener(this);
        }
        return ret;
    }

    /**
     * 구글 플레이스토어 마켓 V3 다운로드 페이지로 이동
     * */
    public void goProductMarket() {
        if (mIsNotInstall) {
            mV3MPlusCtl.startProductMarketInstaller();
        }
    }

    /**
     * 버전 체크를 요청한다.
     * OnPatchState() 함수를 통해 V3 Mobile Plus 2.0 패치 유무를 확인할 수 있다.
     */
    public void checkVersion() {
        // 버전 확인 실행 여부 판단 필요
        mV3MPlusCtl.checkV3MPatchVersion();
    }

    /**
     * V3 Mobile plus 2.0 동작을 확인한다.
     *
     * @return boolean
     */
    public boolean isRunV3MobilePlus() {
        boolean ret = false;
        if (null == mV3MPlusCtl) {
            return false;
        }
        ret = mV3MPlusCtl.isV3MobileRunning(mLicenseKey);
        return ret;
    }
}
