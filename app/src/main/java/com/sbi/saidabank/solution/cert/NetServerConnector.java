package com.sbi.saidabank.solution.cert;

import android.util.Log;

import com.sbi.saidabank.common.util.Logs;

import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.util.StringTokenizer;

public class NetServerConnector extends ServerConnector {

	public NetServerConnector(String url) throws MalformedURLException {
		super(url);
	}

	public NetServerConnector(String url, int timeoutMs) throws MalformedURLException {
		super(url, timeoutMs);
	}

	public NetResult getNetResponse() throws ProtocolException {
		String resCode = null;
		NetResult netRes = new NetResult();
		
		String res = super.getResponse();
		res = res.replace("\n", "");
		Logs.d("res:" + res);
		
		StringTokenizer token = new StringTokenizer(res, "$");
		
		resCode = token.nextToken();

		// resCode가 OK, 000, ERROR 이 아니면 알려지지 않은 Error
    	if ((resCode.equals("OK") || resCode.equals("ERROR") || token.hasMoreTokens()) == false) {		
    		netRes.setResCode("UNDEFINED");
    		
			netRes.setResMessage(resCode);
			
    		return netRes;
    	}
    	
    	netRes.setResCode(resCode);
    	while (token.hasMoreTokens()) {
    		netRes.setResMessage(token.nextToken());
    	}
    	
    	return netRes;
	}
}
