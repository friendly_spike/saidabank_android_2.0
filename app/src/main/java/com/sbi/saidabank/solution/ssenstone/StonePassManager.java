package com.sbi.saidabank.solution.ssenstone;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.util.Base64;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.AES256Utils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.ssenstone.stonepass.libstonepass_sdk.SSUserManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;

/**
 * StonePassManager : 스톤패스 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class StonePassManager {

    @SuppressLint("StaticFieldLeak")
    private static StonePassManager instance;
    private SSUserManager mSSUserManager;
    private String mOp;
    private String mUserName;
    private String mSystemId;
    private String mSsId;
    private String mToken;
    private String mSignData;
    private String mBioType;
    private String mDeviceId;
    private String mFcode;
    private String mErrDesc;
    private String mErrIP;
    private Activity mActivity;
    private Thread mFidoListenerThread;
    private Context mContext;
    private StonePassInitListener mSpInitListener;
    private StonePassListener mSpListener;
    private FingerPrintDlgListener mFingerPrintDlgListener;

    public interface StonePassInitListener {
        void stonePassInitResult();
    }

    public interface StonePassListener {
        void stonePassResult(String op, int errorCode, String errorMsg);
    }

    public interface FingerPrintDlgListener {
        void showFingerPrintDialog();
    }

    public static StonePassManager getInstance(Context context) {
        if (instance == null) {
            synchronized (StonePassManager.class) {
                if (instance == null) {
                    instance = new StonePassManager(context);
                }
            }
        }
        return instance;
    }

    public static void clearInstance() {
        if (instance == null)
            return;
        instance = null;
    }

    private StonePassManager(Context context) {
        this.mContext = context;
        mSSUserManager = new SSUserManager();
    }

    public void stonePassInit(StonePassInitListener listener) {
        MLog.d();
        this.mSpInitListener = listener;
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    mSSUserManager.SSInit(mContext, null, SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/userRequest.jsp");
                }catch (UnsatisfiedLinkError e){
                    Logs.printException(e.getMessage());
                    System.exit(0);
                    return;
                }
                mDeviceId = Const.EMPTY;    // mDeviceId = mSSUserManager.SSGetDUID()
                mSsId = Const.EMPTY;
                mSystemId = "3";
                mToken = "SBISaidaBank";
                mUserName = Prefer.getAuthPhoneCI(mContext);

                Logs.e("stonePassInit - mUserName : " + mUserName);
                if (DataUtil.isNotNull(mUserName)) {
                    byte[] data = mUserName.getBytes(StandardCharsets.UTF_8);
                    mUserName = Base64.encodeToString(data, Base64.NO_WRAP);
                }
                mSpInitListener.stonePassInitResult();
            }
        });
        thread.start();
    }

    public void cancelFingerPrint() {
        MLog.d();
        mSSUserManager.SSCancelFingerprint();
    }

    /**
     * 사용자 지문/패턴 인증 전문 송신 - 숏타입
     *
     * @param op 등록/인증/해제
     */
    public void ssenstoneFIDO(Activity activity, String op, String signData, String bioType, String fcode) {
        FingerPrintDlgListener fpdListener = null;
        if (bioType.equalsIgnoreCase("FINGERPRINT") && activity instanceof FingerPrintDlgListener) {
            fpdListener = (FingerPrintDlgListener) activity;
        }
        ssenstoneFIDO(  activity,
                (StonePassListener) activity,
                fpdListener,
                op,
                signData,
                bioType,
                fcode);

    }

    /**
     * 사용자 지문/패턴 인증 전문 송신 - 롱타입
     *
     * @param op 등록/인증/해제
     */
    public void ssenstoneFIDO(Activity activity, StonePassListener spListener, FingerPrintDlgListener fingerPrintDlgListener, String op, String signData, String bioType, String fcode) {
        mActivity = activity;
        mOp = op;
        mSignData = signData;
        mBioType = bioType;
        mFcode = fcode;
        mSpListener = spListener;
        if (bioType.equalsIgnoreCase("FINGERPRINT")) {
            mFingerPrintDlgListener = fingerPrintDlgListener;
        }
        String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Get.jsp";
        // FIDO 지문인식 사용 가능 프로세스 진행
        if (op.equalsIgnoreCase(Const.SSENSTONE_REGISTER)) {
            MLog.i("********** SSENSTONE_REGISTER **********");
            String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
            HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_AUTHENTICATE)) {
            MLog.i("********** SSENSTONE_AUTHENTICATE **********");
            String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
            HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
        } else if (op.equalsIgnoreCase(Const.SSENSTONE_DEREGISTER)) {
            MLog.i("********** SSENSTONE_DEREGISTER **********");
            if (bioType.equalsIgnoreCase("FINGERPRINT")) {
                // 해지 시에는 통신 없이 FIDO 프로세스 호출
                if (DataUtil.isNotNull(mFingerPrintDlgListener)) {
                    mFingerPrintDlgListener.showFingerPrintDialog();
                }
                fidoProcess(mBioType, mFcode);
            } else {
                String requestMsg = StonePassUtils.genFidoRequestMsg(op, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
                HttpUtils.sendHttpTask(url, requestMsg, mFidoHttpListener);
            }
        }
    }

    private HttpSenderTask.HttpRequestListener mFidoHttpListener = new HttpSenderTask.HttpRequestListener() {
        @Override
        public void endHttpRequest(String ret) {
            if (DataUtil.isNull(ret)) {
                mSpListener.stonePassResult(mOp, 9998, Const.EMPTY);
                return;
            }
            try {
                JSONObject jsonObject = new JSONObject(ret);
                int statusCode = jsonObject.getInt("statusCode");
                if (statusCode == 1200) {
                    String serverRequestMsg = jsonObject.optString("uafRequest");
                    if (DataUtil.isNotNull(serverRequestMsg)) {
                        if (mBioType.equalsIgnoreCase("FINGERPRINT") && mFingerPrintDlgListener != null) {
                            mFingerPrintDlgListener.showFingerPrintDialog();
                        }
                        fidoProcess(serverRequestMsg, mFcode);
                    }
                } else {
                    // 사용자가 등록되어 있지 않는 경우
                    mSpListener.stonePassResult(mOp, statusCode, Const.EMPTY);
                }
            } catch (Exception e) {
                MLog.e(e);
                mSpListener.stonePassResult(mOp, 9999, e.getMessage());
            }
        }
    };

    /**
     * @author 950546
     * @Descrition: 등록된 lisetner로 처리결과를 전달할 때 UI쓰레드에서 처리
     * @since 2018-10-31 오후 3:49
     **/
    private void ssenstonePassResult(final String op, final int errocode, final String errstr) {
        if (DataUtil.isNotNull(mActivity) && DataUtil.isNotNull(mSpListener)) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mSpListener.stonePassResult(op, errocode, errstr);
                }
            });
        }
    }

    private void fidoProcess(final String message, final String fcode) {
        if (mBioType.equalsIgnoreCase("FINGERPRINT")) {
            // 아래 함수를 실행하지 않으면 fingerprint device가 초기화 되지 않아 에러가 발생한다.
            mSSUserManager.SSCheckDevice();
        }
        final SSUserManager.SSFidoListener mFidoListener = new SSUserManager.SSFidoListener() {
            @Override
            public void authenticationFailed(int errorCode, String errString) {
                MLog.d();
                if (DataUtil.isNotNull(mFidoListenerThread)) {
                    mFidoListenerThread.interrupt();
                    mFidoListenerThread = null;
                }
                MLog.line();
                MLog.i("errorCode : " + errorCode + " | errorMsg : " + errString);
                MLog.line();
                ssenstonePassResult(mOp, errorCode, errString);
            }

            @Override
            public void authenticationSucceeded(String responseMsg) {
                MLog.d();
                if (DataUtil.isNotNull(mFidoListenerThread)) {
                    mFidoListenerThread.interrupt();
                    mFidoListenerThread = null;
                }
                if (!mOp.equalsIgnoreCase(Const.SSENSTONE_DEREGISTER)) {
                    // 등록, 인증
                    String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Send" + mOp + ".jsp";
                    HttpUtils.sendHttpTask(url, responseMsg, new HttpSenderTask.HttpRequestListener() {
                        @Override
                        public void endHttpRequest(String ret) {
                            if (DataUtil.isNull(ret)) {
                                mSpListener.stonePassResult(mOp, 9998, Const.EMPTY);
                                return;
                            }
                            try {
                                JSONObject jsonObject = new JSONObject(ret);
                                int statusCode = jsonObject.getInt("statusCode");
                                mSpListener.stonePassResult(mOp, statusCode, Const.EMPTY);
                            } catch (JSONException e) {
                                MLog.e(e);
                                mSpListener.stonePassResult(mOp, 9999, Const.EMPTY);
                            }
                        }
                    });
                } else {
                    // 해지
                    try {
                        JSONObject jsonObject = new JSONObject(responseMsg);
                        final int statusCode = jsonObject.getInt("statusCode");
                        if (statusCode == 99) {
                            // 지문인식 성공
                            String requestMsg = StonePassUtils.genFidoRequestMsg(mOp, mUserName, mSystemId, mToken, mSignData, mBioType, mDeviceId);
                            String url = SaidaUrl.ReqUrl.URL_SSENSTONE.getReqUrl() + "/stonePass/Get.jsp";
                            HttpUtils.sendHttpTask(url, requestMsg, new HttpSenderTask.HttpRequestListener() {
                                @Override
                                public void endHttpRequest(String ret) {
                                    if (DataUtil.isNull(ret)) {
                                        mSpListener.stonePassResult(mOp, 9998, Const.EMPTY);
                                        return;
                                    }
                                    try {
                                        JSONObject jsonObject = new JSONObject(ret);
                                        int statusCode = jsonObject.getInt("statusCode");
                                        mSpListener.stonePassResult(mOp, statusCode, Const.EMPTY);
                                        if (statusCode == 1200 && mBioType.equalsIgnoreCase("PATTERN"))
                                            Prefer.setPatternRegStatus(mContext, true);
                                    } catch (Exception e) {
                                        mSpListener.stonePassResult(mOp, 9999, Const.EMPTY);
                                    }
                                }
                            });
                        } else if (statusCode == 0) {
                            // FIDO 삭제 성공
                            ssenstonePassResult(mOp, statusCode, Const.EMPTY);
                        } else {
                            ssenstonePassResult(mOp, statusCode, Const.EMPTY);
                        }
                    } catch (JSONException e) {
                        MLog.e(e);
                        ssenstonePassResult(mOp, 9999, Const.EMPTY);
                    }
                }
            }
        };
        mFidoListenerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                mSSUserManager.setFidoListener(mFidoListener, mActivity, mUserName, message, fcode, mBioType, true);
            }
        });
        mFidoListenerThread.start();
    }

    /**
     * PIN을 등록한다
     *
     * @param pin       입력 핀값
     * @param secureKey nullable
     * @return 리턴 코드값 (P000 : success | P001 : network error | P003 : input values error | P004 : ocure exeption)
     */
    public String registPin(String pin, byte[] secureKey) {
        MLog.d();
        try {
            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "USERADD");
            jsonServerRequest.put("PIN", pin);
            jsonServerRequest.put("USERID", mUserName);
            if (DataUtil.isNotNull(secureKey)) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                jsonServerRequest.put("BASE64PINSECKEY", Base64.encodeToString(secureKey, Base64.URL_SAFE));
            }
            String userName = LoginUserInfo.getInstance().getCUST_NM();
            userName = DataUtil.isNotNull(userName) ? Base64.encodeToString(userName.getBytes(), Base64.NO_WRAP) : Const.EMPTY;
            jsonServerRequest.put("USERNAME", userName);
            jsonServerRequest.put("SSID", mSsId);
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            jsonServerRequest.put("AUTHTYPE", 0x1);
            jsonServerRequest.put("MULTIAUTH", 0x0);
            jsonServerRequest.put("ARSPIN", AES256Utils.encodeAES(pin));
            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            if (DataUtil.isNotNull(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    return jsonObject.optString("ERRORCODE");
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
        return "P004";
    }

    /**
     * PIN 인증한다 (APP 인증)
     *
     * @param pin       입력 핀값
     * @param secureKey nullable
     * @param signData  전자서명값
     * @return 리턴 코드값 (P000 : success | P001 : network error | P003 : input values error | P004 : ocure exeption)
     */
    public String authenticatePin(String pin, byte[] secureKey, String signData, String signDocData, String ssid, long startTime) {
        MLog.d();
        try {
            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "AUTH1");
            jsonServerRequest.put("PIN", pin);
            jsonServerRequest.put("USERID", mUserName);
            if (DataUtil.isNotNull(secureKey)) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                jsonServerRequest.put("BASE64PINSECKEY", Base64.encodeToString(secureKey, Base64.URL_SAFE));
            }
            if (DataUtil.isNotNull(signData)) {
                String encodedStr = Base64.encodeToString(signData.getBytes(), Base64.NO_WRAP);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(encodedStr);
                jsonServerRequest.put("SIGNDATATOPKCS7", jsonArray); // 서명할 데이터(계좌이체시 필요함)
            }
            if (DataUtil.isNotNull(signDocData)) {
                String encodedStr = Base64.encodeToString(signDocData.getBytes(), Base64.NO_WRAP);
                JSONArray jsonArray = new JSONArray();
                jsonArray.put(encodedStr);
                jsonServerRequest.put("SIGNDATATOPKCS7DETACHED", jsonArray); // 서명할 문서데이터
            }
            long gapTime = System.currentTimeMillis() - startTime;      // PIN 입력 시간 (사용자 행동 값)
            jsonServerRequest.put("GT", String.valueOf(gapTime));
            jsonServerRequest.put("SSID", ssid);
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            jsonServerRequest.put("AUTHTYPE", 0x1);
            jsonServerRequest.put("AUTHPURPOSE", 0x1);
            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            if (DataUtil.isNotNull(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    JSONObject jsonServerRequest2 = new JSONObject();
                    jsonServerRequest2.put("ACMD", "SENDSSOTP");
                    strRet = mSSUserManager.StonePASS(jsonServerRequest2.toString());
                    if (DataUtil.isNotNull(strRet)) {
                        JSONObject jsonObject2 = new JSONObject(strRet);
                        String strResult2 = jsonObject2.getString("RESULT");
                        if (strResult2.equalsIgnoreCase("SUCCESS")) {
                            return "P000";
                        } else if (strResult2.equalsIgnoreCase("ERROR")) {
                            String errCode = jsonObject.optString("ERRORCODE");
                            if ("P002".equals(errCode)) {
                                mErrIP = jsonObject2.optString("client_ip");
                                mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_p002);
                            } else if ("O130".equals(errCode)) {
                                mErrIP = jsonObject2.optString("client_ip");
                                mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_o130);
                            } else if ("G301".equals(errCode)) {
                                mErrIP = jsonObject2.optString("client_ip");
                                mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_g301);
                            } else if ("P004".equals(errCode)) {
                                mErrIP = jsonObject2.optString("client_ip");
                                mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_p002);
                            }
                            return errCode;
                        }
                    }
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    String errCode = jsonObject.optString("ERRORCODE");
                    if ("P002".equals(errCode)) {
                        mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_p002);
                    } else if ("O130".equals(errCode)) {
                        mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_o130);
                    } else if ("G301".equals(errCode)) {
                        mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_g301);
                    } else if ("P004".equals(errCode)) {
                        mErrDesc = mContext.getResources().getString(R.string.msg_ssenstone_error_p002);
                    }
                    return errCode;
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
        return "P004";
    }

    /**
     * 등록한 PIN 해지한다
     *
     * @param pin       입력핀값
     * @param secureKey nullable
     * @return 리턴 코드값 (P000 : success | P001 : network error | P003 : input values error | P004 : ocure exeption)
     */
    public String deregistPin(String pin, byte[] secureKey) {
        MLog.d();
        try {
            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "DELUSER");
            jsonServerRequest.put("USERID", mUserName);
            jsonServerRequest.put("PIN", pin);
            if (DataUtil.isNotNull(secureKey)) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                jsonServerRequest.put("BASE64PINSECKEY", Base64.encodeToString(secureKey, Base64.URL_SAFE));
            }
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            if (DataUtil.isNotNull(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    return jsonObject.optString("ERRORCODE");
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
        return "P004";
    }

    /**
     * PIN 변경한다 (APP 인증)
     *
     * @param pin          입력핀코드
     * @param secureKey    nullable
     * @param newPin       신규핀코드
     * @param newSecureKey nullalbe
     * @return
     */
    public String changePin(String pin, String secureKey, String newPin, String newSecureKey) {
        MLog.d();
        try {
            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "CHANGEPIN");
            jsonServerRequest.put("PIN", pin);
            jsonServerRequest.put("USERID", mUserName);
            if (DataUtil.isNotNull(secureKey)) {
                jsonServerRequest.put("PINTYPE", "RAON");
                jsonServerRequest.put("PINDATATYPE", "R01");
                jsonServerRequest.put("BASE64PINSECKEY", StonePassUtils.getBase64Key(secureKey));
            }
            if (DataUtil.isNotNull(newSecureKey)) {
                jsonServerRequest.put("NEWBASE64PINSECKEY", StonePassUtils.getBase64Key(secureKey));
            }
            jsonServerRequest.put("NEWPIN", newPin);
            jsonServerRequest.put("TOKEN", mToken);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            if (DataUtil.isNotNull(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    return jsonObject.optString("ERRORCODE");
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
        return "P004";
    }

    /*
     * 등록된 사용자 상태를 체크한다
     *
     * @param userName 사용자 아이디
     * @param systemID 시스템 아이디
     */
    public synchronized String checkUserStatus() {
        MLog.d();
        try {
            JSONObject jsonServerRequest = new JSONObject();
            jsonServerRequest.put("ACMD", "CHECKUSERSTATUS");
            jsonServerRequest.put("USERID", mUserName);
            jsonServerRequest.put("SYSTEMID", mSystemId);
            jsonServerRequest.put("TOKEN", mToken);
            String strRet = mSSUserManager.StonePASS(jsonServerRequest.toString());
            if (DataUtil.isNotNull(strRet)) {
                JSONObject jsonObject = new JSONObject(strRet);
                String strResult = jsonObject.optString("RESULT");
                if (strResult.equalsIgnoreCase("SUCCESS")) {
                    return "P000";
                } else if (strResult.equalsIgnoreCase("ERROR")) {
                    return jsonObject.optString("ERRORCODE");
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
        return "P004";
    }

    public String getErrorDesc() {
        return mErrDesc;
    }

    public String getErrorIP() {
        return mErrIP;
    }
}
