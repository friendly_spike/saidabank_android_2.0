package com.sbi.saidabank.solution.motp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;

import com.atsolutions.secure.util.ByteUtils;
import com.atsolutions.tapagent.TAPAgent;
import com.atsolutions.tapagent.a2a.data.A2AData;
import com.atsolutions.tapagent.a2a.define.A2ADefine;
import com.atsolutions.tapagent.callback.TAPResultCallback;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

/**
 * MOTPManager : OTP 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class MOTPManager implements TAPResultCallback {

    @SuppressLint("StaticFieldLeak")
    private static MOTPManager instance;
    private TAPAgent mAgent;
    private String mSiteCode = "02051";
    private MOTPEventListener mOtpEventListener = null;

    public interface MOTPEventListener {
        void mOtpEventListener(String resultCode, String resultMsg, String reqData1, String reqData2, String reqData3, String reqData4);
    }

    public static MOTPManager getInstance(Context context) {
        if (instance == null) {
            synchronized (MOTPManager.class) {
                if (instance == null) {
                    instance = new MOTPManager(context);
                }
            }
        }
        return instance;
    }

    public static void clearInstance() {
        instance = null;
    }

    private MOTPManager(Context context) {
        this.mAgent = new TAPAgent(context, LoginUserInfo.getInstance().getSMPH_DEVICE_ID());
    }

    @Override
    public void onCommandResult(int apiCode, A2AData result) {
        MLog.d();

        String resultCode = result.getResultCode();
        String resultMsg = result.getResultMsg();
        Bundle resultData = result.getData();
        String reqData1 = Const.EMPTY;
        String reqData2 = Const.EMPTY;
        String reqData3 = Const.EMPTY;
        String reqData4 = Const.EMPTY;

        if (resultCode.equals(A2ADefine.A2aResultCode.A2A_RESULT_CODE_SUCCESS)) {

            switch (apiCode) {

                case A2ADefine.ApiCode.API_GET_TA_INFO:
                    String taVersion = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_TA_VERSION);
                    reqData1 = DataUtil.isNotNull(taVersion) ? taVersion : Const.EMPTY;
                    break;

                case A2ADefine.ApiCode.API_ISSUE_OTP:
                case A2ADefine.ApiCode.API_DISSUE_OTP:
                    break;

                case A2ADefine.ApiCode.API_GET_SERIAL_NUM:
                    String serialNumber = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_SERIAL_NUMBER);
                    reqData1 = DataUtil.isNotNull(serialNumber) ? serialNumber : Const.EMPTY;
                    break;

                case A2ADefine.ApiCode.API_GENERATE_DS_OTP:
                    String otp = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_OTP);
                    reqData1 = DataUtil.isNotNull(otp) ? otp : Const.EMPTY;
                    break;

                case A2ADefine.ApiCode.API_GET_ALL_INFO:
                    serialNumber = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_SERIAL_NUMBER);
                    taVersion = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_TA_VERSION);
                    String issueDate = resultData.getString(A2ADefine.A2aExtraKey.A2A_EXTRA_ISSUE_DATE);
                    if (DataUtil.isNotNull(serialNumber)) {
                        reqData1 = serialNumber;
                        reqData2 = serialNumber.substring(0, 3);
                    } else {
                        reqData1 = Const.EMPTY;
                        reqData2 = Const.EMPTY;
                        resultCode = "A2106A82";
                        resultMsg = "모바일 OTP정보가 없습니다. 모바일OTP를 발급/재발급 받으시기 바랍니다.";
                    }
                    reqData3 = DataUtil.isNotNull(issueDate) ? issueDate : Const.EMPTY;
                    reqData4 = DataUtil.isNotNull(taVersion) ? taVersion : Const.EMPTY;
                    break;

                default:
                    break;
            }
        }
        if (DataUtil.isNotNull(mOtpEventListener)) {
            mOtpEventListener.mOtpEventListener(resultCode, resultMsg, reqData1, reqData2, reqData3, reqData4);
        }
    }

    public void removeListener() {
        mOtpEventListener = null;
    }

    public void getTaVersion(MOTPEventListener listener) {
        MLog.d();
        this.mOtpEventListener = listener;
        mAgent.getTaInfo(this);
    }

    public void saveIssueInfo(MOTPEventListener listener, String optKey1, String optKey2, final String serialNum, final String issueDate) {
        MLog.d();
        this.mOtpEventListener = listener;
        final byte[] otpKey1Hex = ByteUtils.HexStringToBytes(optKey1);
        final byte[] otpKey2Hex = ByteUtils.HexStringToBytes(optKey2);
        mAgent.issue(serialNum, issueDate, otpKey1Hex, otpKey2Hex, this);
    }

    public void getSerialNum(MOTPEventListener listener) {
        MLog.d();
        this.mOtpEventListener = listener;
        mAgent.getAllInfo(mSiteCode, this);
    }

    public void reqOTPCode(MOTPEventListener listener, String otpTime, String serialNum, String hti) {
        MLog.d();
        this.mOtpEventListener = listener;
        byte[] para = ByteUtils.HexStringToBytes(hti);
        mAgent.sign(serialNum, otpTime, para, mSiteCode, true, this);
    }

    public void reqDissue(MOTPEventListener listener, String serialNum) {
        MLog.d();
        this.mOtpEventListener = listener;
        mAgent.dissue(serialNum, this);
    }
}
