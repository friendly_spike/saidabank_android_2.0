package com.sbi.saidabank.solution.fds;

import android.content.Context;

import com.interezen.mobile.android.I3GAsyncResponse;
import com.interezen.mobile.android.I3GServiceProc;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

/**
 * FDSManager : FDS 매니져
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class FDSManager {

    private static FDSManager instance;
    private I3GServiceProc i3GServiceProc;

    public static FDSManager getInstance() {
        if (instance == null) {
            synchronized (FDSManager.class) {
                if (instance == null) {
                    instance = new FDSManager();
                }
            }
        }
        return instance;
    }

    public static void clearInstance() {
        instance = null;
    }

    private FDSManager() {
        this.i3GServiceProc = new I3GServiceProc();
    }

    /**
     * 단말기의 IMEI값을 반환한다.
     *
     * @param context
     * @return String IMEI문자열
     */
    public String getEncIMEI(Context context) {
        // 20200109 Android 10 이상 IMEI 값 추출 불가로 함수 변경 (i3GServiceProc.GetMDUInfo(context, 400, 1000, 21);)
        return i3GServiceProc.GetMDUInfo(context, 400, 1000, 41);
    }

    /**
     * 단말기의 IMEI값을 반환한다.
     *
     * @param context
     * @return String IMEI문자열
     */
    public String getEncIMEI121(Context context) {
        return i3GServiceProc.GetMDUInfo(context, 400, 1000, 121);
    }

    /**
     * 솔루션에서 만들어진 UUID를 반환한다
     *
     * @param context
     * @return String UUID문자열
     */
    public String getEncUUID(Context context) {
        return i3GServiceProc.GetMDUInfo(context, 400, 1000, 22);
    }

    /**
     * FDS내용을 해당 공인 IP로 전달한다.
     *
     * @param context
     * @param response
     */
    public void getFDSInfo(Context context, I3GAsyncResponse response) {
        i3GServiceProc.delegate = response;
        i3GServiceProc.i3GGetEXData(
                context,
                Const.EMPTY,                          // 수집서버 IP (SaidaUrl.ReqUrl.URL_FDS.getReqUrl())
                80,                                // 포트번호
                100,                              // 소켓타임아웃시간 기본3000
                "Android",                        // 사용자ID  --- 아래 이하 값은 FDS이전에 수집했던 내용으로 최근 FDS에서는 사용하지 않음. 그냥 값 박아 넣으면 됨.
                1000,                             // 고객사구분코드 - 고객사의 정보
                Utils.getVersionName(context),        // 앱 버전 정보
                Const.EMPTY,                          // 서비스 구분코드(10자리숫자)
                "1",                             // 로그인 성공 실패 여부
                Const.EMPTY                           // 부가데이타
        );
    }
}
