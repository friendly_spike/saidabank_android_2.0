package com.sbi.saidabank.define;

/**
 * LicenseKey : 솔루션 라이센스키를 정의
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class LicenseKey {

    /** V3 */
    public static final String V3_LICENSE_KEY = "40040020-17087142";

    /** Espider */
    public static final String ESPIDER_LICENSE_KEY = "98215963-aa7a-11e8-8a14-80c16e782f98";

    /** AppsFlyer */
    public static final String AF_DEV_KEY = "xhHM9BoZyKGsQdDyswhJV6";

    /** FakeFinder */
    public static final String FAKE_FINDER_ID = "sbibank";
    public static final String FAKE_FINDER_LICENSE_KEY = "d2dbeaa133c31bf37a188f5f3b0c06a0765c34b7";
}
