package com.sbi.saidabank.define.datatype.transfer;

import android.text.TextUtils;

import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import java.util.ArrayList;

/**
 * Create 210125.
 * 통합이체 개발로 추가
 * 기존 계좌로보내기,휴대폰이체를 새로추가된 오픈뱅킹이체 까지 통합하여
 * 하나의 이체화면으로 개발함.
 *
 */
public class ITransferDataMgr {
    //단건이체에서 받는사람이 누구인지 알고 싶어서...
    public static int TR_TYPE_NONE     = 0; //받는분이 선택되지 않았을때
    public static int TR_TYPE_ACCOUNT  = 1; //받는분이 계좌이체일때
    public static int TR_TYPE_PHONE    = 2; //받는분이 휴대폰이체일때

    //웹의 가져오기나 내보내기에서 진입했을 경우
    public static int ACCESS_TYPE_NORMAL    = 0; //웹의 통합이체
    public static int ACCESS_TYPE_EXPORT    = 1; //웹의 내보내기로 부터 진입
    public static int ACCESS_TYPE_IMPORT    = 2; //웹의 가져오기로 부터 진입

    private static ITransferDataMgr instance = null;

    //멤버 변수정의 ============================================
    private String OTP_SERIAL_NO;       //OTP 시리얼번호
    private String OTP_TA_VERSION;      //OTP버전

    //출금계좌정보 =====
    private String WTCH_BANK_CD;        //출금은행코드
    private String WTCH_DTLS_BANK_CD;   //세부출금은행코드 - 저축은행별 코드
    private String WTCH_BANK_NM;        //출금은행이름
    private String WTCH_BANK_ALS;       //출금은행이름 - 단축명
    private String WTCH_ACNO;           //출금계좌번호
    private String WTCH_ACNO_NM;        //출금계좌이름

    //출금계좌한도
    private Double transferOneTime;   //1회이체한도금액
    private Double transferOneDay;    //1일이체한도금액
    private Double balanceAmount;     //출금가능금액

    //지연이체서비스
    private String DLY_POSB_LMIT_AMT;//지연이체시 즉시 입금 가능 금액.

    //입금계좌정보 ======
    //수신자 타입(0:수신자 없음, 1:계좌이체,2:휴대폰이체)
    private int TRANSFER_TYPE = TR_TYPE_NONE;

    //거래내역에서 가져오기 내보내기 할때 처리 변수
    //웹 브리지에서 가져오기, 내보내기시 값을 넣어준다.
    private int TRANSFER_ACCESS_TYPE = ACCESS_TYPE_NORMAL;
    private String MNRC_AMT; //이체금액

    //휴대폰이체시 사용할경우
    private ContactsInfo transferContactsInfo;

    //계좌이체시 사용할 리스트
    private ArrayList<ITransferRemitteeInfo> remitteInfoArrayList;

    //통합이체용 자주/즐겨찾기 - 이체중 다이얼로그에서 사용하기 위해 담아둔다.
    private ArrayList<ITransferRecentlyInfo> recentlyArrayList;


    //인스턴스 함수 정의 ===============================================
    public static ITransferDataMgr getInstance() {
        if (instance == null) {
            synchronized (ITransferDataMgr.class) {
                if (instance == null) {
                    instance = new ITransferDataMgr();
                }
            }
        }
        return instance;
    }

    public ITransferDataMgr(){
        this.recentlyArrayList = new ArrayList<ITransferRecentlyInfo>();
        this.remitteInfoArrayList = new ArrayList<ITransferRemitteeInfo>();

        WTCH_BANK_CD="";        //출금은행코드
        WTCH_DTLS_BANK_CD="";   //세부출금은행코드 - 저축은행별 코드
        WTCH_BANK_NM="";        //출금은행이름
        WTCH_BANK_ALS="";       //출금은행이름 - 단축명
        WTCH_ACNO="";           //출금계좌번호
        WTCH_ACNO_NM="";


        transferOneTime = 0d;   //1회이체한도금액
        transferOneDay = 0d;    //1일이체한도금액
        balanceAmount = 0d;     //잔액
    }

    /**
     * 인스턴스 자체를 초기화 시키는 것임.
     * 이체 화면으로 처음 진입시 호출해준다.
     */
    public static void clear(){
        instance = null;
    }

    /**
     * 인스턴스는 초기화 하지 않음.
     * OTP정보,출금계좌정보 초기화 하지 않음.
     *
     * 이체완료 화면에서 이체계속하기 버튼 클릭시 기존 계좌정보 물고 들어와야 해서
     * 이전 이체 정보만 지워주는 것이다.
     */
    public void clearTransferData(){
        transferOneTime = 0d;   //1회이체한도금액
        transferOneDay = 0d;    //1일이체한도금액
        balanceAmount = 0d;     //출금가능금액

        TRANSFER_TYPE = TR_TYPE_NONE;
        TRANSFER_ACCESS_TYPE = ACCESS_TYPE_NORMAL;

        transferContactsInfo = null;

        remitteInfoArrayList.clear();

        recentlyArrayList.clear();
    }


    //멤버 함수 정의 ===============================================
    public String getOTP_SERIAL_NO() {
        return OTP_SERIAL_NO;
    }

    public void setOTP_SERIAL_NO(String OTP_SERIAL_NO) {
        this.OTP_SERIAL_NO = OTP_SERIAL_NO;
    }

    public String getOTP_TA_VERSION() {
        return OTP_TA_VERSION;
    }

    public void setOTP_TA_VERSION(String OTP_TA_VERSION) {
        this.OTP_TA_VERSION = OTP_TA_VERSION;
    }

    public ArrayList<ITransferRecentlyInfo> getRecentlyArrayList() {
        return recentlyArrayList;
    }

    public void setRecentlyArrayList(ArrayList<ITransferRecentlyInfo> recentlyArrayList) {
        if(this.recentlyArrayList == null) {
            this.recentlyArrayList = new ArrayList<ITransferRecentlyInfo>();
        }
        this.recentlyArrayList.clear();
        this.recentlyArrayList.addAll(recentlyArrayList);
    }

    //출금계좌정보
    public String getWTCH_BANK_CD() {
        return WTCH_BANK_CD;
    }

    public void setWTCH_BANK_CD(String WTCH_BANK_CD) {
        this.WTCH_BANK_CD = WTCH_BANK_CD;
    }

    public String getWTCH_DTLS_BANK_CD() {
        return WTCH_DTLS_BANK_CD;
    }

    public String getWTCH_BANK_ALS() {
        return WTCH_BANK_ALS;
    }

    public void setWTCH_BANK_ALS(String WTCH_BANK_ALS) {
        this.WTCH_BANK_ALS = WTCH_BANK_ALS;
    }

    public void setWTCH_DTLS_BANK_CD(String WTCH_DTLS_BANK_CD) {
        this.WTCH_DTLS_BANK_CD = WTCH_DTLS_BANK_CD;
    }

    public String getWTCH_BANK_NM() {
        return WTCH_BANK_NM;
    }

    public void setWTCH_BANK_NM(String WTCH_BANK_NM) {
        this.WTCH_BANK_NM = WTCH_BANK_NM;
    }

    public String getWTCH_ACNO() {
        return WTCH_ACNO;
    }

    public void setWTCH_ACNO(String WTCH_ACNO) {
        this.WTCH_ACNO = WTCH_ACNO;
    }

    public String getWTCH_ACNO_NM() {
        return WTCH_ACNO_NM;
    }

    public void setWTCH_ACNO_NM(String WTCH_ACNO_NM) {
        this.WTCH_ACNO_NM = WTCH_ACNO_NM;
    }

    public Double getTransferOneTime() {
        return transferOneTime;
    }

    public void setTransferOneTime(Double transferOneTime) {
        this.transferOneTime = transferOneTime;
    }

    public Double getTransferOneDay() {
        return transferOneDay;
    }

    public void setTransferOneDay(Double transferOneDay) {
        this.transferOneDay = transferOneDay;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getDLY_POSB_LMIT_AMT() {
        return DLY_POSB_LMIT_AMT;
    }

    public void setDLY_POSB_LMIT_AMT(String DLY_POSB_LMIT_AMT) {
        this.DLY_POSB_LMIT_AMT = DLY_POSB_LMIT_AMT;
    }

    //입금계좌정보 함수...#######
    //이체 타입
     public int getTRANSFER_TYPE() {
        return TRANSFER_TYPE;
    }

    public void setTRANSFER_TYPE(int TRANSFER_TYPE) {
        this.TRANSFER_TYPE = TRANSFER_TYPE;
    }

    public int getTRANSFER_ACCESS_TYPE() {
        return TRANSFER_ACCESS_TYPE;
    }

    public void setTRANSFER_ACCESS_TYPE(int TRANSFER_ACCESS_TYPE) {
        this.TRANSFER_ACCESS_TYPE = TRANSFER_ACCESS_TYPE;
    }

    public String getMNRC_AMT() {
        return MNRC_AMT;
    }

    public void setMNRC_AMT(String MNRC_AMT) {
        this.MNRC_AMT = MNRC_AMT;
    }

    //수취인정보 확인관련
    public ArrayList<ITransferRemitteeInfo> getRemitteInfoArrayList() {
        return remitteInfoArrayList;
    }

    public void setRemitteInfoArrayList(ArrayList<ITransferRemitteeInfo> remitteInfoArrayList) {
        this.remitteInfoArrayList = remitteInfoArrayList;
    }

    /**
     * 다건이체시의 수취인 조회는 계속 추가하면 된다.
     * @param remitteInfo
     */
    public void addRemitteInfoIntoArrayList(ITransferRemitteeInfo remitteInfo){
        TRANSFER_TYPE = TR_TYPE_ACCOUNT;
        this.remitteInfoArrayList.add(remitteInfo);

        this.transferContactsInfo = null;
    }

    public void removeRemitteInfoFromArrayList(int position){
        this.remitteInfoArrayList.remove(position);
    }

    public int getRemitteInfoArraySize(){
        return this.remitteInfoArrayList.size();
    }

    //전체 이체내역의 금액을 리턴한다.
    public Double getTotalTranSumAmount(){
        double sumAmount=0;
        for(int i=0;i<remitteInfoArrayList.size();i++){
            String tranAmountStr = remitteInfoArrayList.get(i).getTRN_AMT();
            if(TextUtils.isEmpty(tranAmountStr)) tranAmountStr = "0";
            sumAmount += Double.valueOf(tranAmountStr);
        }

        return sumAmount;
    }

    /**
     * 수취인 리스트 0에서 부터 untile postion까지의 금액을 합산하여 리턴한다.
     * @param untilePosition
     * @return
     */
    public Double getUntilTranSumAmount(int untilePosition){
        double sumAmount=0;
        for(int i=0;i<untilePosition;i++){
            String tranAmountStr = remitteInfoArrayList.get(i).getTRN_AMT();
            if(TextUtils.isEmpty(tranAmountStr)) tranAmountStr = "0";
            sumAmount += Double.valueOf(tranAmountStr);
        }

        return sumAmount;
    }

    /**
     * 수취인 리스트에서 입력된  postion을 제외한 합산 금액을 합산하여 리턴한다.
     * @param exeptPosition
     * @return
     */
    public Double getExeptPositionTranSumAmount(int exeptPosition){
        double sumAmount=0;
        for(int i=0;i<remitteInfoArrayList.size();i++){
            if(i != exeptPosition){
                String tranAmountStr = remitteInfoArrayList.get(i).getTRN_AMT();
                if(TextUtils.isEmpty(tranAmountStr)) tranAmountStr = "0";
                sumAmount += Double.valueOf(tranAmountStr);
            }
        }

        return sumAmount;
    }

    //휴대폰이체 정보 입력
    public ContactsInfo getTransferContactsInfo() {
        return transferContactsInfo;
    }

    public void setTransferContactsInfo(ContactsInfo transferContactsInfo) {
        if(transferContactsInfo != null){
            TRANSFER_TYPE = TR_TYPE_PHONE;
        }else{
            TRANSFER_TYPE = TR_TYPE_NONE;
        }
        this.transferContactsInfo = transferContactsInfo;


        //혹시 수취인정보가 있으면 지워준다.
        this.remitteInfoArrayList.clear();
    }

    public void initRemiteeInfo(){
        for(int i=0;i<remitteInfoArrayList.size();i++){
            ITransferRemitteeInfo info = remitteInfoArrayList.get(i);

            info.setTRNF_DVCD("1");
            info.setTRNF_DMND_TM("");
            info.setTRNF_DMND_DT("");
            info.setDEPO_BNKB_MRK_NM(info.getRECV_NM());
            info.setTRAN_BNKB_MRK_NM(info.getCUST_NM());
            info.setTRN_MEMO_CNTN("");
        }
    }
}
