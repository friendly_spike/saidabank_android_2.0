package com.sbi.saidabank.define.datatype.common;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.EntryPoint;

/**
 * CommonUserInfo : 고객정보 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class CommonUserInfo implements Parcelable {

    // 사용자 정보
    private EntryPoint entryPoint;
    private EntryPoint entryStart;
    private String name;
    private String idName;
    private String birth;
    private String sexCode;
    private String phoneNumber;
    private String bankName;
    private String bankAccount;
    private boolean hasAccount;
    private boolean isFullIdnumber;
    private boolean useSession;

    // 가입정보
    private String CInumber;
    private String DInumber;
    private String emailAddress;
    private String MBRnumber;
    private boolean isNewUser;

    // 선택약관 동의여부
    private boolean optionalPolicyPush;
    private boolean optionalPolicySMS;

    // 보안서비스알림관 동의여부
    private boolean securityPolicyPush;
    private boolean securityPolicySMS;

    // 기기정보 801
    private String wData;
    private String nData;

    // 기기정보 802
    private String deviceOS;
    private String deviceInfo01;
    private String deviceInfo02;

    // 센스톤
    private String operation;
    private String patternData;
    private String signData;
    private String signDocData;
    private String resultMsg;
    private int pinLength;

    // 로그인을 패턴으로 진행여부(true : 패턴 / false : 지문)
    private boolean isLoginPattern;
    private boolean isChangedFinger;

    // OTP
    private int otpPWEntryType;
    private int otpPWAccessType;
    private int otpPWLength;
    private String otpPWAuthData;
    private String otpTitle;
    private boolean otpForgetAuthNo;
    private boolean otpUseForgetAuth;

    // Transkey Data
    private String secureData;
    private String cipherData;
    private String dummyData;
    private String plainData;

    // 신분증 인증
    private String idnumber;
    private String issueDate;
    private String IDT_PRF_DVCD;
    private String DRVN_LCNS_NO;
    private String DRVN_LCNS_LCNO;
    //private String DRVN_LCNS_SRNO;
    private String DRVN_LCNS_ISS_YR;
    private String DRVN_LCNS_ETC_NO;


    // 가대출 여부
    private String CRDT_LOAN_YN; // 대출 가조회 여부

    public CommonUserInfo() {

    }

    public CommonUserInfo(EntryPoint entryStart, EntryPoint entryPoint) {
        this.entryStart = entryStart;
        this.entryPoint = entryPoint;
    }

    protected CommonUserInfo(Parcel in) {
        entryPoint = (EntryPoint) in.readSerializable();
        entryStart = (EntryPoint) in.readSerializable();
        name = in.readString();
        idName = in.readString();
        birth = in.readString();
        sexCode = in.readString();
        phoneNumber = in.readString();
        bankName = in.readString();
        bankAccount = in.readString();
        hasAccount = in.readByte() != 0;
        isFullIdnumber = in.readByte() != 0;
        useSession = in.readByte() != 0;
        CInumber = in.readString();
        DInumber = in.readString();
        emailAddress = in.readString();
        MBRnumber = in.readString();
        isNewUser = in.readByte() != 0;
        optionalPolicyPush = in.readByte() != 0;
        optionalPolicySMS = in.readByte() != 0;
        securityPolicyPush = in.readByte() != 0;
        securityPolicySMS = in.readByte() != 0;
        wData = in.readString();
        nData = in.readString();
        deviceOS = in.readString();
        deviceInfo01 = in.readString();
        deviceInfo02 = in.readString();
        operation = in.readString();
        patternData = in.readString();
        signData = in.readString();
        signDocData = in.readString();
        resultMsg = in.readString();
        pinLength = in.readInt();
        isLoginPattern = in.readByte() != 0;
        isChangedFinger = in.readByte() != 0;
        otpPWEntryType = in.readInt();
        otpPWAccessType = in.readInt();
        otpPWLength = in.readInt();
        otpPWAuthData = in.readString();
        otpTitle = in.readString();
        otpForgetAuthNo = in.readByte() != 0;
        otpUseForgetAuth = in.readByte() != 0;
        secureData = in.readString();
        cipherData = in.readString();
        dummyData = in.readString();
        plainData = in.readString();
        idnumber = in.readString();
        issueDate = in.readString();
        IDT_PRF_DVCD = in.readString();
        DRVN_LCNS_NO = in.readString();
        DRVN_LCNS_LCNO = in.readString();
        //DRVN_LCNS_SRNO = in.readString();
        DRVN_LCNS_ISS_YR = in.readString();
        DRVN_LCNS_ETC_NO = in.readString();
        CRDT_LOAN_YN = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(entryPoint);
        dest.writeSerializable(entryStart);
        dest.writeString(name);
        dest.writeString(idName);
        dest.writeString(birth);
        dest.writeString(sexCode);
        dest.writeString(phoneNumber);
        dest.writeString(bankName);
        dest.writeString(bankAccount);
        dest.writeByte((byte) (hasAccount ? 1 : 0));
        dest.writeByte((byte) (isFullIdnumber ? 1 : 0));
        dest.writeByte((byte) (useSession ? 1 : 0));
        dest.writeString(CInumber);
        dest.writeString(DInumber);
        dest.writeString(emailAddress);
        dest.writeString(MBRnumber);
        dest.writeByte((byte) (isNewUser ? 1 : 0));
        dest.writeByte((byte) (optionalPolicyPush ? 1 : 0));
        dest.writeByte((byte) (optionalPolicySMS ? 1 : 0));
        dest.writeByte((byte) (securityPolicyPush ? 1 : 0));
        dest.writeByte((byte) (securityPolicySMS ? 1 : 0));
        dest.writeString(wData);
        dest.writeString(nData);
        dest.writeString(deviceOS);
        dest.writeString(deviceInfo01);
        dest.writeString(deviceInfo02);
        dest.writeString(operation);
        dest.writeString(patternData);
        dest.writeString(signData);
        dest.writeString(signDocData);
        dest.writeString(resultMsg);
        dest.writeInt(pinLength);
        dest.writeByte((byte) (isLoginPattern ? 1 : 0));
        dest.writeByte((byte) (isChangedFinger ? 1 : 0));
        dest.writeInt(otpPWEntryType);
        dest.writeInt(otpPWAccessType);
        dest.writeInt(otpPWLength);
        dest.writeString(otpPWAuthData);
        dest.writeString(otpTitle);
        dest.writeByte((byte) (otpForgetAuthNo ? 1 : 0));
        dest.writeByte((byte) (otpUseForgetAuth ? 1 : 0));
        dest.writeString(secureData);
        dest.writeString(cipherData);
        dest.writeString(dummyData);
        dest.writeString(plainData);
        dest.writeString(idnumber);
        dest.writeString(issueDate);
        dest.writeString(IDT_PRF_DVCD);
        dest.writeString(DRVN_LCNS_NO);
        dest.writeString(DRVN_LCNS_LCNO);

        dest.writeString(DRVN_LCNS_ISS_YR);
        dest.writeString(DRVN_LCNS_ETC_NO);

        dest.writeString(CRDT_LOAN_YN);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<CommonUserInfo> CREATOR = new Creator<CommonUserInfo>() {
        @Override
        public CommonUserInfo createFromParcel(Parcel in) {
            return new CommonUserInfo(in);
        }

        @Override
        public CommonUserInfo[] newArray(int size) {
            return new CommonUserInfo[size];
        }
    };

    public EntryPoint getEntryPoint() {
        return entryPoint;
    }

    public void setEntryPoint(EntryPoint entryType) {
        this.entryPoint = entryType;
    }

    public EntryPoint getEntryStart() {
        return entryStart;
    }

    public void setEntryStart(EntryPoint entryStart) {
        this.entryStart = entryStart;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        Logs.e("CommonUserInfo - setName : " + name);
        if(!TextUtils.isEmpty(name)){
            name = name.trim();
        }
        this.name = name;
    }

    public String getIdName() {
        return idName;
    }

    public void setIdName(String idName) {
        this.idName = idName;
    }

    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public String getSexCode() {
        return sexCode;
    }

    public void setSexCode(String sexCode) {
        this.sexCode = sexCode;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCInumber() {
        return CInumber;
    }

    public void setCInumber(String CInumber) {
        this.CInumber = CInumber;
    }

    public String getDInumber() {
        return DInumber;
    }

    public void setDInumber(String DInumber) {
        this.DInumber = DInumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getSignData() {
        return signData;
    }

    public void setSignData(String signData) {
        this.signData = signData;
    }

    public String getSignDocData() {
        return signDocData;
    }

    public void setSignDocData(String signDocData) {
        this.signDocData = signDocData;
    }

    public int getPinLength() {
        return pinLength;
    }

    public void setPinLength(int pinLength) {
        this.pinLength = pinLength;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public String getwData() {
        return wData;
    }

    public void setwData(String wData) {
        this.wData = wData;
    }

    public String getnData() {
        return nData;
    }

    public void setnData(String nData) {
        this.nData = nData;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getDeviceInfo01() {
        return deviceInfo01;
    }

    public void setDeviceInfo01(String deviceInfo01) {
        this.deviceInfo01 = deviceInfo01;
    }

    public String getDeviceInfo02() {
        return deviceInfo02;
    }

    public void setDeviceInfo02(String deviceInfo02) {
        this.deviceInfo02 = deviceInfo02;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getIDT_PRF_DVCD() {
        return IDT_PRF_DVCD;
    }

    public void setIDT_PRF_DVCD(String IDT_PRF_DVCD) {
        this.IDT_PRF_DVCD = IDT_PRF_DVCD;
    }

    public String getDRVN_LCNS_NO() {
        return DRVN_LCNS_NO;
    }

    public void setDRVN_LCNS_NO(String DRVN_LCNS_NO) {
        this.DRVN_LCNS_NO = DRVN_LCNS_NO;
    }

    public String getDRVN_LCNS_LCNO() {
        return DRVN_LCNS_LCNO;
    }

    public void setDRVN_LCNS_LCNO(String DRVN_LCNS_LCNO) {
        this.DRVN_LCNS_LCNO = DRVN_LCNS_LCNO;
    }

//    public String getDRVN_LCNS_SRNO() {
//        return DRVN_LCNS_SRNO;
//    }
//
//    public void setDRVN_LCNS_SRNO(String DRVN_LCNS_SRNO) {
//        this.DRVN_LCNS_SRNO = DRVN_LCNS_SRNO;
//    }


    public String getDRVN_LCNS_ISS_YR() {
        return DRVN_LCNS_ISS_YR;
    }

    public void setDRVN_LCNS_ISS_YR(String DRVN_LCNS_ISS_YR) {
        this.DRVN_LCNS_ISS_YR = DRVN_LCNS_ISS_YR;
    }

    public String getDRVN_LCNS_ETC_NO() {
        return DRVN_LCNS_ETC_NO;
    }

    public void setDRVN_LCNS_ETC_NO(String DRVN_LCNS_ETC_NO) {
        this.DRVN_LCNS_ETC_NO = DRVN_LCNS_ETC_NO;
    }

    public String getCRDT_LOAN_YN() {
        return CRDT_LOAN_YN;
    }

    public void setCRDT_LOAN_YN(String CRDT_LOAN_YN) {
        this.CRDT_LOAN_YN = CRDT_LOAN_YN;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public int getOtpPWEntryType() {
        return otpPWEntryType;
    }

    public void setOtpPWEntryType(int otpPWEntryType) {
        this.otpPWEntryType = otpPWEntryType;
    }

    public int getOtpPWAccessType() {
        return otpPWAccessType;
    }

    public void setOtpPWAccessType(int otpPWAccessType) {
        this.otpPWAccessType = otpPWAccessType;
    }


    public int getOtpPWLength() {
        return otpPWLength;
    }

    public void setOtpPWLength(int otpPWLength) {
        this.otpPWLength = otpPWLength;
    }

    public String getOtpPWAuthData() {
        return otpPWAuthData;
    }

    public void setOtpPWAuthData(String otpPWAuthData) {
        this.otpPWAuthData = otpPWAuthData;
    }

    public boolean isOtpForgetAuthNo() {
        return otpForgetAuthNo;
    }

    public void setOtpForgetAuthNo(boolean otpForgetAuthNo) {
        this.otpForgetAuthNo = otpForgetAuthNo;
    }

    public boolean isOtpUseForgetAuth() {
        return otpUseForgetAuth;
    }

    public void setOtpUseForgetAuth(boolean otpUseForgetAuth) {
        this.otpUseForgetAuth = otpUseForgetAuth;
    }

    public String getOtpTitle() {
        return otpTitle;
    }

    public void setOtpTitle(String otpTitle) {
        this.otpTitle = otpTitle;
    }

    public boolean hasAccount() {
        return hasAccount;
    }

    public void setHasAccount(boolean hasAccount) {
        this.hasAccount = hasAccount;
    }

    public boolean isNewUser() {
        return isNewUser;
    }

    public void setNewUser(boolean newUser) {
        isNewUser = newUser;
    }

    public String getMBRnumber() {
        return MBRnumber;
    }

    public void setMBRnumber(String MBRnumber) {
        this.MBRnumber = MBRnumber;
    }

    public boolean isLoginPattern() {
        return isLoginPattern;
    }

    public void setLoginPattern(boolean loginPattern) {
        isLoginPattern = loginPattern;
    }

    public boolean isFullIdnumber() {
        return isFullIdnumber;
    }

    public void setFullIdnumber(boolean fullIdnumber) {
        isFullIdnumber = fullIdnumber;
    }

    public boolean isOptionalPolicyPush() {
        return optionalPolicyPush;
    }

    public void setOptionalPolicyPush(boolean optionalPolicyPush) {
        this.optionalPolicyPush = optionalPolicyPush;
    }

    public boolean isOptionalPolicySMS() {
        return optionalPolicySMS;
    }

    public void setOptionalPolicySMS(boolean optionalPolicySMS) {
        this.optionalPolicySMS = optionalPolicySMS;
    }

    public String getSecureData() {
        return secureData;
    }

    public void setSecureData(String secureData) {
        this.secureData = secureData;
    }

    public String getCipherData() {
        return cipherData;
    }

    public void setCipherData(String cipherData) {
        this.cipherData = cipherData;
    }

    public String getDummyData() {
        return dummyData;
    }

    public void setDummyData(String dummyData) {
        this.dummyData = dummyData;
    }

    public String getPlainData() {
        return plainData;
    }

    public void setPlainData(String plainData) {
        this.plainData = plainData;
    }

    public boolean isUseSession() {
        return useSession;
    }

    public void setUseSession(boolean useSession) {
        this.useSession = useSession;
    }

    public String getPatternData() {
        return patternData;
    }

    public void setPatternData(String patternData) {
        this.patternData = patternData;
    }

    public boolean isChangedFinger() {
        return isChangedFinger;
    }

    public void setChangedFinger(boolean changedFinger) {
        isChangedFinger = changedFinger;
    }

    public boolean isSecurityPolicyPush() {
        return securityPolicyPush;
    }

    public void setSecurityPolicyPush(boolean securityPolicyPush) {
        this.securityPolicyPush = securityPolicyPush;
    }

    public boolean isSecurityPolicySMS() {
        return securityPolicySMS;
    }

    public void setSecurityPolicySMS(boolean securityPolicySMS) {
        this.securityPolicySMS = securityPolicySMS;
    }


}
