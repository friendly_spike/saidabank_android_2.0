package com.sbi.saidabank.define.datatype.main2;

import org.json.JSONObject;

public class Main2CoupleExpenceAmount {
    private  String SETT_YM;	    //결제년월
    private  String SETT_TYCD;	    //결제유형코드
    private  String SETT_ITEM_NM;	//결제항목명
    private  String SETT_AMT;	    //결제금액
    private  String FNTR_DVCD_NM;	//금융거래구분코드명(결제유형명)

    public Main2CoupleExpenceAmount(JSONObject object){
        SETT_YM = object.optString("SETT_YM");
        SETT_TYCD = object.optString("SETT_TYCD");
        SETT_ITEM_NM = object.optString("SETT_ITEM_NM");
        SETT_AMT = object.optString("SETT_AMT");
        FNTR_DVCD_NM = object.optString("FNTR_DVCD_NM");
    }

    public String getSETT_YM() {
        return SETT_YM;
    }

    public void setSETT_YM(String SETT_YM) {
        this.SETT_YM = SETT_YM;
    }

    public String getSETT_TYCD() {
        return SETT_TYCD;
    }

    public void setSETT_TYCD(String SETT_TYCD) {
        this.SETT_TYCD = SETT_TYCD;
    }

    public String getSETT_ITEM_NM() {
        return SETT_ITEM_NM;
    }

    public void setSETT_ITEM_NM(String SETT_ITEM_NM) {
        this.SETT_ITEM_NM = SETT_ITEM_NM;
    }

    public String getSETT_AMT() {
        return SETT_AMT;
    }

    public void setSETT_AMT(String SETT_AMT) {
        this.SETT_AMT = SETT_AMT;
    }

    public String getFNTR_DVCD_NM() {
        return FNTR_DVCD_NM;
    }

    public void setFNTR_DVCD_NM(String FNTR_DVCD_NM) {
        this.FNTR_DVCD_NM = FNTR_DVCD_NM;
    }
}
