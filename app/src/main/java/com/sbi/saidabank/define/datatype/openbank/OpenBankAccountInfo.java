package com.sbi.saidabank.define.datatype.openbank;

import android.text.TextUtils;

import org.json.JSONObject;

import java.io.Serializable;

public class OpenBankAccountInfo implements Serializable {
    public String INST_DVCD;	    //  기관구분코드
    public String RPRS_FNLT_CD;	    //  대표금융기관코드 - 은행코드
    public String DTLS_FNLT_CD;	    //  세부금융기관코드 - 저축은행코드
    public String OBA_ACCO_KNCD;	//  오픈뱅킹계좌종류코드( 1: 입출금, 2: 예적금, 6: 수익증권, T: 종합계좌)
    public String OBA_ACCO_KNCD_NM; //  오픈뱅킹계좌종류명
    public String ACNO_REG_STCD;	//	계좌등록상태코드(1:정상등록, 2:등록실패, 3:기관장애)
    public String ACNO;	            //	계좌번호
    public String PROD_NM;	        //	상품명
    public String ACCO_ALS;	        //	계좌별칭
    public String ACNO_SQNO;	    //	계좌번호순번
    public String BLNC_AMT;	        //	잔액금액
    public String WTCH_POSB_AMT;	//	출금가능금액
    public String ACCO_STCD;	    //	계좌상태코드
    public String INQ_AGR_YN;	    //	조회동의여부
    public String WTCH_AGR_YN;	    //	출금동의여부
    public String NEW_DT;	        //	신규일자
    public String EXPT_DT;	        //	만기일자
    public String TM_CLR_VAL;	    //  테마색값  - 오픈뱅킹 메인 화면에서만 사용되는 변수.
    public String BANK_NM;          //  은행명 - 풀네임.
    public String MNRC_BANK_NM;     //  은행명 - 약어

    public boolean IS_CMA_ACCOUNT;  //  종합계좌여부

    public boolean isFinishSearchAmount;

    public OpenBankAccountInfo(JSONObject object){

        INST_DVCD = object.optString("INST_DVCD");
        RPRS_FNLT_CD = object.optString("RPRS_FNLT_CD");
        DTLS_FNLT_CD = object.optString("DTLS_FNLT_CD");
        OBA_ACCO_KNCD = object.optString("OBA_ACCO_KNCD");
        if(OBA_ACCO_KNCD.equalsIgnoreCase("T")){
            OBA_ACCO_KNCD = "6";
            //종합계좌이면 - 그룹 종류는 수익증권(6)으로 변경하고 종합계좌 여부만 가지고 있도록 한다.
            IS_CMA_ACCOUNT = true;
            OBA_ACCO_KNCD_NM = "수익증권";
        }else{
            OBA_ACCO_KNCD_NM = object.optString("OBA_ACCO_KNCD_NM");
        }

        ACNO_REG_STCD = object.optString("ACNO_REG_STCD");
        ACNO = object.optString("ACNO");
        PROD_NM = object.optString("PROD_NM");
        ACCO_ALS = object.optString("ACCO_ALS");
        ACNO_SQNO = object.optString("ACNO_SQNO");
        BLNC_AMT = object.optString("BLNC_AMT");
        WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");
        ACCO_STCD = object.optString("ACCO_STCD");
        INQ_AGR_YN = object.optString("INQ_AGR_YN");
        WTCH_AGR_YN = object.optString("WTCH_AGR_YN");
        NEW_DT = object.optString("NEW_DT");
        EXPT_DT = object.optString("EXPT_DT");

        TM_CLR_VAL = object.optString("TM_CLR_VAL");
        if(TextUtils.isEmpty(TM_CLR_VAL)){
            TM_CLR_VAL = "#858e95";//디폴트 색상
        }

        BANK_NM = object.optString("BANK_NM");
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");
    }

    public String getTextForSearch(){
        return PROD_NM + "#" + ACCO_ALS + "#" + ACNO;
    }
}
