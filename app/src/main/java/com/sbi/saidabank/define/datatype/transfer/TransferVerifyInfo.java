package com.sbi.saidabank.define.datatype.transfer;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.define.Const;

import java.io.Serializable;

/**
 * VerifyTransferInfo : 수취인 정보 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TransferVerifyInfo implements Serializable {

    private String CNTP_FIN_INST_CD;    // 상대 금융기관코드
    private String CNTP_BANK_ACNO;      // 상대 은행계좌번호
    private String TRN_AMT;             // 거래금액
    private String RECV_NM;             // 수취인명
    private String FEE;                 // 수수료
    private String CUST_NM;             // 고객명
    private String DEPO_BNKB_MRK_NM;    // 수신통장 표시명
    private String TRAN_BNKB_MRK_NM;    // 송신통장 표시명
    private String TRNF_DMND_DT;        // 이체 요청일자
    private String TRNF_DMND_TM;        // 이체 요청시각
    private String TRN_MEMO_CNTN;       // 메모
    private String TRNF_DVCD;           // 이체 종류코드 (1 : 즉시이체, 2 : 지연이체, 3 : 예약이체, 5:입금지정)
    private String MNRC_BANK_NM;        // 입금은행명
    private String BANK_NM;             // 은행명
    private String UUID_NO;             // UUID번호
    private String SVC_DVCD;            // 서비스 구분코드 (0 : 보통(서비스 가입한게 없음) 1 : 즉시, 2 : 지연3시간이후)
    private String TRN_UNQ_NO;          // 거래 시퀀스 번호

    public TransferVerifyInfo() {

    }

    public String getCNTP_FIN_INST_CD() {
        return CNTP_FIN_INST_CD;
    }

    public void setCNTP_FIN_INST_CD(String CNTP_FIN_INST_CD) {
        this.CNTP_FIN_INST_CD = CNTP_FIN_INST_CD;
    }

    public String getCNTP_BANK_ACNO() {
        return CNTP_BANK_ACNO;
    }

    public void setCNTP_BANK_ACNO(String CNTP_BANK_ACNO) {
        this.CNTP_BANK_ACNO = CNTP_BANK_ACNO;
    }

    public String getTRN_AMT() {
        return TRN_AMT;
    }

    public void setTRN_AMT(String TRN_AMT) {
        this.TRN_AMT = TRN_AMT;
    }

    public String getRECV_NM() {
        return RECV_NM;
    }

    public void setRECV_NM(String RECV_NM) {
        this.RECV_NM = RECV_NM;
    }

    public String getFEE() {
        return FEE;
    }

    public void setFEE(String FEE) {
        this.FEE = FEE;
    }

    public String getCUST_NM() {
        return CUST_NM;
    }

    public void setCUST_NM(String CUST_NM) {
        this.CUST_NM = CUST_NM;
    }

    public String getDEPO_BNKB_MRK_NM() {
        return DEPO_BNKB_MRK_NM;
    }

    public void setDEPO_BNKB_MRK_NM(String DEPO_BNKB_MRK_NM) {
        this.DEPO_BNKB_MRK_NM = DEPO_BNKB_MRK_NM;
    }

    public String getTRAN_BNKB_MRK_NM() {
        return TRAN_BNKB_MRK_NM;
    }

    public void setTRAN_BNKB_MRK_NM(String TRAN_BNKB_MRK_NM) {
        this.TRAN_BNKB_MRK_NM = TRAN_BNKB_MRK_NM;
    }

    public String getTRNF_DMND_DT() {
        return TRNF_DMND_DT;
    }

    public void setTRNF_DMND_DT(String TRNF_DMND_DT) {
        this.TRNF_DMND_DT = TRNF_DMND_DT;
    }

    public String getTRNF_DMND_TM() {
        return TRNF_DMND_TM;
    }

    public void setTRNF_DMND_TM(String TRNF_DMND_TM) {
        this.TRNF_DMND_TM = TRNF_DMND_TM;
    }

    public String getTRN_MEMO_CNTN() {
        return TRN_MEMO_CNTN;
    }

    public void setTRN_MEMO_CNTN(String TRN_MEMO_CNTN) {
        this.TRN_MEMO_CNTN = TRN_MEMO_CNTN;
    }

    public String getTRNF_DVCD() {
        return DataUtil.isNotNull(TRNF_DVCD) ? TRNF_DVCD : Const.EMPTY;
    }

    public void setTRNF_DVCD(String TRNF_DVCD) {
        this.TRNF_DVCD = TRNF_DVCD;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    public String getBANK_NM() {
        return BANK_NM;
    }

    public void setBANK_NM(String BANK_NM) {
        this.BANK_NM = BANK_NM;
    }

    public String getUUID_NO() {
        return UUID_NO;
    }

    public void setUUID_NO(String UUID_NO) {
        this.UUID_NO = UUID_NO;
    }

    public String getSVC_DVCD() {
        return SVC_DVCD;
    }

    public void setSVC_DVCD(String SVC_DVCD) {
        this.SVC_DVCD = SVC_DVCD;
    }

    public String getTRN_UNQ_NO() {
        return TRN_UNQ_NO;
    }

    public void setTRN_UNQ_NO(String TRN_UNQ_NO) {
        this.TRN_UNQ_NO = TRN_UNQ_NO;
    }
}
