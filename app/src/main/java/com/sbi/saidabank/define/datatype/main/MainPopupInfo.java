package com.sbi.saidabank.define.datatype.main;

import android.graphics.Bitmap;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * MainPopupInfo : 메인화면 팝업 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.2
 * @since 2020-08-14
 */
public class MainPopupInfo implements Serializable {

    private String SITE_ID;         // 사이트ID
    private String PUP_SRNO;        // 팝업일련번호
    private String PUP_NM;          // 팝업명
    private String PUP_CNTN_DVCD;   // 팝업내용구분코드 (I: 이미지, T:텍스트)
    private String PUP_CNTN;        // 팝업내용 ("텍스트" 일 경우 내용)
    private String LOC_DVCD;        // 위치구분코드
    private String LINK_URL_ADDR;   // 링크URL주소
    private String BTN_EPOS_YN;     // 버튼노출여부
    private String BTN_NM;          // 버튼명
    private String ATFL_UNNO;       // 첨부파일고유번호
    private String ATFL_SRNO;       // 첨부파일일련번호
    private String IMG_URL_ADDR;    // 이미지URL주소 ("이미지" 일 경우 이미지 주소)

    private Bitmap  LOAD_BITMAP;
    private boolean LOAD_COMPLETE;  // 이미지 로드 상태
	public MainPopupInfo() {

    }
	
    public MainPopupInfo(JSONObject object) {
        SITE_ID = object.optString("SITE_ID");
        PUP_SRNO = object.optString("PUP_SRNO");
        PUP_NM = object.optString("PUP_NM");
        PUP_CNTN_DVCD = object.optString("PUP_CNTN_DVCD");
        PUP_CNTN = object.optString("PUP_CNTN");
        LOC_DVCD = object.optString("LOC_DVCD");
        LINK_URL_ADDR = object.optString("LINK_URL_ADDR");
        BTN_EPOS_YN = object.optString("BTN_EPOS_YN");
        BTN_NM = object.optString("BTN_NM");
        ATFL_UNNO = object.optString("ATFL_UNNO");
        ATFL_SRNO = object.optString("ATFL_SRNO");
        IMG_URL_ADDR = object.optString("IMG_URL_ADDR");
    }

    public String getSITE_ID() {
        return SITE_ID;
    }

    public void setSITE_ID(String SITE_ID) {
        this.SITE_ID = SITE_ID;
    }

    public String getPUP_SRNO() {
        return PUP_SRNO;
    }

    public void setPUP_SRNO(String PUP_SRNO) {
        this.PUP_SRNO = PUP_SRNO;
    }

    public String getPUP_NM() {
        return PUP_NM;
    }

    public void setPUP_NM(String PUP_NM) {
        this.PUP_NM = PUP_NM;
    }

    public String getPUP_CNTN_DVCD() {
        return PUP_CNTN_DVCD;
    }

    public void setPUP_CNTN_DVCD(String PUP_CNTN_DVCD) {
        this.PUP_CNTN_DVCD = PUP_CNTN_DVCD;
    }

    public String getPUP_CNTN() {
        return PUP_CNTN;
    }

    public void setPUP_CNTN(String PUP_CNTN) {
        this.PUP_CNTN = PUP_CNTN;
    }

    public String getLOC_DVCD() {
        return LOC_DVCD;
    }

    public void setLOC_DVCD(String LOC_DVCD) {
        this.LOC_DVCD = LOC_DVCD;
    }

    public String getLINK_URL_ADDR() {
        return LINK_URL_ADDR;
    }

    public void setLINK_URL_ADDR(String LINK_URL_ADDR) {
        this.LINK_URL_ADDR = LINK_URL_ADDR;
    }

    public String getBTN_EPOS_YN() {
        return BTN_EPOS_YN;
    }

    public void setBTN_EPOS_YN(String BTN_EPOS_YN) {
        this.BTN_EPOS_YN = BTN_EPOS_YN;
    }

    public String getBTN_NM() {
        return BTN_NM;
    }

    public void setBTN_NM(String BTN_NM) {
        this.BTN_NM = BTN_NM;
    }

    public String getATFL_UNNO() {
        return ATFL_UNNO;
    }

    public void setATFL_UNNO(String ATFL_UNNO) {
        this.ATFL_UNNO = ATFL_UNNO;
    }

    public String getATFL_SRNO() {
        return ATFL_SRNO;
    }

    public void setATFL_SRNO(String ATFL_SRNO) {
        this.ATFL_SRNO = ATFL_SRNO;
    }

    public String getIMG_URL_ADDR() {
        return IMG_URL_ADDR;
    }

    public void setIMG_URL_ADDR(String IMG_URL_ADDR) {
        this.IMG_URL_ADDR = IMG_URL_ADDR;
    }

    public Bitmap getLOAD_BITMAP() {
        return LOAD_BITMAP;
    }

    public void setLOAD_BITMAP(Bitmap LOAD_BITMAP) {
        this.LOAD_BITMAP = LOAD_BITMAP;
    }

    public boolean isLOAD_COMPLETE() {
        return LOAD_COMPLETE;
    }

    public void setLOAD_COMPLETE(boolean LOAD_COMPLETE) {
        this.LOAD_COMPLETE = LOAD_COMPLETE;
    }
}
