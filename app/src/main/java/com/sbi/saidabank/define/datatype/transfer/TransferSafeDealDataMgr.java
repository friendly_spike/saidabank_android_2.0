package com.sbi.saidabank.define.datatype.transfer;

public class TransferSafeDealDataMgr {
    private static TransferSafeDealDataMgr instance = null;

    private String WTCH_ACNO;           //출금계좌번호
    private String MNRC_ACCO_DEPR_NM;   //입금계좌 예금주명
    private String MNRC_CPNO;           //입금대상 전화번호
    private String MNRC_BANK_CD;        //입금은행코드
    private String MNRC_ACNO;           //입금계좌번호
    private String TRAM;                //이체금액
    private String TRNF_FEE;            //이체수수료
    private String WTCH_ACCO_MRK_CNTN;  //출금하는 계좌표시내용(입금받는 예금주 명)
    private String MNRC_ACCO_MRK_CNTN;  //입금받는 계좌표시내용(출금하는 예금주 명,나)
    private int SAFE_TRN_KNCD;          //안심이체 종류 코드 (1:금전거래, 2:부동산거래 3:물품거래)
    private String SAFE_TRN_RSCD;       //안전거래사유코드   (01:개인거래간증빙, 02:계약금, 03:중도금, 04:잔금, 99:직접입력)
    private String SAFE_TRN_RSN_CNTN;   //안전거래사유내용

    private String LMIT_LMT_ACCO_YN;    //한도계좌여부
    private Double LIMIT_ONE_TIME;      //1회이체 한도금액
    private Double LIMIT_ONE_DAY;       //1일이체 한도금액

    private String LCTN_ZPCD;           //소재지 우편번호
    private String LCTN_BLDG_MNGMO;     //소재지 건물 관리번호
    private String LCTN_ADDR;           //소재지 주소
    private String LCTN_DTL_ADDR;       //소재지 소재지 상세주소

    private String ELEC_SGNR_SRNO;      //전자서명 일련번호

    //아래 변수는 안심이체 진행하면서 필요한 값이어서 넣어둔다.
    private String SIGN_DATA;           //전자서명 데이타
    private String ACCO_ALS;            //출금계좌별명
    private String MNRC_BANK_NAME;      //입금은행
    private String OTP_SERIAL_NO;       //OTP 시리얼번호
    private String OTP_TA_VERSION;      //OTP버전

    private String RET_COMPLET_STR;      //이체 완료후 전달받는 문자열


    public static TransferSafeDealDataMgr getInstance() {
        if (instance == null) {
            synchronized (TransferSafeDealDataMgr.class) {
                if (instance == null) {
                    instance = new TransferSafeDealDataMgr();
                }
            }
        }
        return instance;
    }

    public static void clear(){
        instance = null;
    }

    public String getWTCH_ACNO() {
        return WTCH_ACNO;
    }

    public void setWTCH_ACNO(String WTCH_ACNO) {
        this.WTCH_ACNO = WTCH_ACNO;
    }

    public String getMNRC_ACCO_DEPR_NM() {
        return MNRC_ACCO_DEPR_NM;
    }

    public void setMNRC_ACCO_DEPR_NM(String MNRC_ACCO_DEPR_NM) {
        this.MNRC_ACCO_DEPR_NM = MNRC_ACCO_DEPR_NM;
    }
    public String getMNRC_CPNO() {
        return MNRC_CPNO;
    }

    public void setMNRC_CPNO(String MNRC_CPNO) {
        this.MNRC_CPNO = MNRC_CPNO;
    }
    public String getMNRC_BANK_CD() {
        return MNRC_BANK_CD;
    }

    public void setMNRC_BANK_CD(String MNRC_BANK_CD) {
        this.MNRC_BANK_CD = MNRC_BANK_CD;
    }

    public String getMNRC_ACNO() {
        return MNRC_ACNO;
    }

    public void setMNRC_ACNO(String MNRC_ACNO) {
        this.MNRC_ACNO = MNRC_ACNO;
    }

    public String getTRAM() {
        return TRAM;
    }

    public void setTRAM(String TRAM) {
        this.TRAM = TRAM;
    }

    public String getTRNF_FEE() {
        return TRNF_FEE;
    }

    public void setTRNF_FEE(String TRNF_FEE) {
        this.TRNF_FEE = TRNF_FEE;
    }

    public String getWTCH_ACCO_MRK_CNTN() {
        return WTCH_ACCO_MRK_CNTN;
    }

    public void setWTCH_ACCO_MRK_CNTN(String WTCH_ACCO_MRK_CNTN) {
        this.WTCH_ACCO_MRK_CNTN = WTCH_ACCO_MRK_CNTN;
    }

    public String getMNRC_ACCO_MRK_CNTN() {
        return MNRC_ACCO_MRK_CNTN;
    }

    public void setMNRC_ACCO_MRK_CNTN(String MNRC_ACCO_MRK_CNTN) {
        this.MNRC_ACCO_MRK_CNTN = MNRC_ACCO_MRK_CNTN;
    }

    public int getSAFE_TRN_KNCD() {
        return SAFE_TRN_KNCD;
    }

    public void setSAFE_TRN_KNCD(int SAFE_TRN_KNCD) {
        this.SAFE_TRN_KNCD = SAFE_TRN_KNCD;
    }

    public String getSAFE_TRN_RSCD() {
        return SAFE_TRN_RSCD;
    }

    public void setSAFE_TRN_RSCD(String SAFE_TRN_RSCD) {
        this.SAFE_TRN_RSCD = SAFE_TRN_RSCD;
    }

    public String getSAFE_TRN_RSN_CNTN() {
        return SAFE_TRN_RSN_CNTN;
    }

    public void setSAFE_TRN_RSN_CNTN(String SAFE_TRN_RSN_CNTN) {
        this.SAFE_TRN_RSN_CNTN = SAFE_TRN_RSN_CNTN;
    }

    public String getLMIT_LMT_ACCO_YN() {
        return LMIT_LMT_ACCO_YN;
    }

    public void setLMIT_LMT_ACCO_YN(String LMIT_LMT_ACCO_YN) {
        this.LMIT_LMT_ACCO_YN = LMIT_LMT_ACCO_YN;
    }

    public Double getLIMIT_ONE_TIME() {
        return LIMIT_ONE_TIME;
    }

    public void setLIMIT_ONE_TIME(Double LIMIT_ONE_TIME) {
        this.LIMIT_ONE_TIME = LIMIT_ONE_TIME;
    }

    public Double getLIMIT_ONE_DAY() {
        return LIMIT_ONE_DAY;
    }

    public void setLIMIT_ONE_DAY(Double LIMIT_ONE_DAY) {
        this.LIMIT_ONE_DAY = LIMIT_ONE_DAY;
    }

    public String getLCTN_ZPCD() {
        return LCTN_ZPCD;
    }

    public void setLCTN_ZPCD(String LCTN_ZPCD) {
        this.LCTN_ZPCD = LCTN_ZPCD;
    }

    public String getLCTN_BLDG_MNGMO() {
        return LCTN_BLDG_MNGMO;
    }

    public void setLCTN_BLDG_MNGMO(String LCTN_BLDG_MNGMO) {
        this.LCTN_BLDG_MNGMO = LCTN_BLDG_MNGMO;
    }

    public String getLCTN_ADDR() {
        return LCTN_ADDR;
    }

    public void setLCTN_ADDR(String LCTN_ADDR) {
        this.LCTN_ADDR = LCTN_ADDR;
    }

    public String getLCTN_DTL_ADDR() {
        return LCTN_DTL_ADDR;
    }

    public void setLCTN_DTL_ADDR(String LCTN_DTL_ADDR) {
        this.LCTN_DTL_ADDR = LCTN_DTL_ADDR;
    }

    public String getELEC_SGNR_SRNO() {
        return ELEC_SGNR_SRNO;
    }

    public void setELEC_SGNR_SRNO(String ELEC_SGNR_SRNO) {
        this.ELEC_SGNR_SRNO = ELEC_SGNR_SRNO;
    }

    public String getSIGN_DATA() {
        return SIGN_DATA;
    }

    public void setSIGN_DATA(String SIGN_DATA) {
        this.SIGN_DATA = SIGN_DATA;
    }

    public String getACCO_ALS() {
        return ACCO_ALS;
    }

    public void setACCO_ALS(String ACCO_ALS) {
        this.ACCO_ALS = ACCO_ALS;
    }

    public String getMNRC_BANK_NAME() {
        return MNRC_BANK_NAME;
    }

    public void setMNRC_BANK_NAME(String MNRC_BANK_NAME) {
        this.MNRC_BANK_NAME = MNRC_BANK_NAME;
    }

    public String getOTP_SERIAL_NO() {
        return OTP_SERIAL_NO;
    }

    public void setOTP_SERIAL_NO(String OTP_SERIAL_NO) {
        this.OTP_SERIAL_NO = OTP_SERIAL_NO;
    }

    public String getOTP_TA_VERSION() {
        return OTP_TA_VERSION;
    }

    public void setOTP_TA_VERSION(String OTP_TA_VERSION) {
        this.OTP_TA_VERSION = OTP_TA_VERSION;
    }

    public String getRET_COMPLET_STR() {
        return RET_COMPLET_STR;
    }

    public void setRET_COMPLET_STR(String RET_COMPLET_STR) {
        this.RET_COMPLET_STR = RET_COMPLET_STR;
    }
}
