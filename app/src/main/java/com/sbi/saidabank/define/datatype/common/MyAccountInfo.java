package com.sbi.saidabank.define.datatype.common;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * MyAccountInfo : 본인 계좌 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class MyAccountInfo implements Serializable {

    private String ACNO;                 // 계좌번호
    private String ACCO_INDO;            // 계좌식별번호
    private String ACCO_STCD;            // 계좌상태코드
    private String ACCO_PRGS_STCD;       // 계좌진행상태코드 (00: 정상, 01: 거래중지계좌(잡좌))
    private String NEW_DT;               // 신규일자
    //private String EXPT_DT;              // 만기일자
    //private String TRMT_DT;              // 해지일자
    //private String UPRN_PROD_CLCD;       // 상위상품분류코드
    private String PROD_CD;              // 상품코드 (상품코드 앞2자리 - 과목코드)
    private String PROD_NM;              // 상품명
    private String ACCO_ALS;             // 계좌별칭
    private String ACCO_BLNC;            // 계좌잔액
    private String WTCH_POSB_AMT;        // 출금가능금액
    //private String APL_INRT;             // 적용이율
    //private String CHCR_YN;              // 체크카드연결계좌여부

//    private String SMRT_WTCH_REG_YN;     // 스마트출금등록여부
//    private String IMMD_WTCH_ACCO_YN;    // 즉시출금계좌여부
//    private String ATM_WTCH_LMT_ACCO_YN; // 자동화기기출금제한계좌여부(장기미거래)
//    private String ATTR_NEW_PROP_YN;     // 자동이체등록계좌여부
//    private String SECU_ACCO_SETP_YN;    // 보안계좌설정여부
    private String RPRS_ACCO_YN;         // 대표계좌여부
    private String SHRN_ACCO_YN;         // 공유형통장여부
    private String ACDT_DCL_CD;          // 사고신고여부
    private String LMIT_LMT_ACCO_YN;     // 한도제한계좌여부

    private String BANK_NM;              //은행 풀네임.
    private String MNRC_BANK_NM;         //은행 단축명.

    public MyAccountInfo() {

    }

    public MyAccountInfo(JSONObject object) {
        ACNO = object.optString("ACNO");
        ACCO_INDO = object.optString("ACCO_INDO");
        ACCO_STCD = object.optString("ACCO_STCD");
        ACCO_PRGS_STCD = object.optString("ACCO_PRGS_STCD");
        NEW_DT = object.optString("NEW_DT");
        //EXPT_DT = object.optString("EXPT_DT");
        //TRMT_DT = object.optString("TRMT_DT");
        //UPRN_PROD_CLCD = object.optString("UPRN_PROD_CLCD");
        PROD_CD = object.optString("PROD_CD");
        PROD_NM = object.optString("PROD_NM");
        ACCO_ALS = object.optString("ACCO_ALS");
        ACCO_BLNC = object.optString("ACCO_BLNC");
        WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");
        //APL_INRT = object.optString("APL_INRT");
        //CHCR_YN = object.optString("CHCR_YN");

        //SMRT_WTCH_REG_YN = object.optString("SMRT_WTCH_REG_YN");
        //IMMD_WTCH_ACCO_YN = object.optString("IMMD_WTCH_ACCO_YN");
        //ATM_WTCH_LMT_ACCO_YN = object.optString("ATM_WTCH_LMT_ACCO_YN");
        //ATTR_NEW_PROP_YN = object.optString("ATTR_NEW_PROP_YN");
        //SECU_ACCO_SETP_YN = object.optString("SECU_ACCO_SETP_YN");
        RPRS_ACCO_YN = object.optString("RPRS_ACCO_YN");
        SHRN_ACCO_YN = object.optString("SHRN_ACCO_YN");
        ACDT_DCL_CD = object.optString("ACDT_DCL_CD");
        LMIT_LMT_ACCO_YN = object.optString("LMIT_LMT_ACCO_YN");

        BANK_NM = object.optString("BANK_NM");
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");
    }

    public String getACNO() {
        return ACNO;
    }

    public void setACN0(String ACNO) {
        this.ACNO = ACNO;
    }

    public String getACCO_INDO() {
        return ACCO_INDO;
    }

    public void setACCO_INDO(String ACCO_INDO) {
        this.ACCO_INDO = ACCO_INDO;
    }

    public String getACCO_STCD() {
        return ACCO_STCD;
    }

    public void setACCO_STCD(String ACCO_STCD) {
        this.ACCO_STCD = ACCO_STCD;
    }

    public String getACCO_PRGS_STCD() {
        return ACCO_PRGS_STCD;
    }

    public void setACCO_PRGS_STCD(String ACCO_PRGS_STCD) {
        this.ACCO_PRGS_STCD = ACCO_PRGS_STCD;
    }

    public String getNEW_DT() {
        return NEW_DT;
    }

    public void setNEW_DT(String NEW_DT) {
        this.NEW_DT = NEW_DT;
    }

//    public String getEXPT_DT() {
//        return EXPT_DT;
//    }
//
//    public void setEXPT_DT(String EXPT_DT) {
//        this.EXPT_DT = EXPT_DT;
//    }
//
//    public String getTRMT_DT() {
//        return TRMT_DT;
//    }
//
//    public void setTRMT_DT(String TRMT_DT) {
//        this.TRMT_DT = TRMT_DT;
//    }
//
//    public String getUPRN_PROD_CLCD() {
//        return UPRN_PROD_CLCD;
//    }
//
//    public void setUPRN_PROD_CLCD(String UPRN_PROD_CLCD) {
//        this.UPRN_PROD_CLCD = UPRN_PROD_CLCD;
//    }

    public String getPROD_CD() {
        return PROD_CD;
    }

    public void setPROD_CD(String PROD_CD) {
        this.PROD_CD = PROD_CD;
    }

    public String getPROD_NM() {
        return PROD_NM;
    }

    public void setPROD_NM(String PROD_NM) {
        this.PROD_NM = PROD_NM;
    }

    public String getACCO_ALS() {
        return ACCO_ALS;
    }

    public void setACCO_ALS(String ACCO_ALS) {
        this.ACCO_ALS = ACCO_ALS;
    }

    public String getACCO_BLNC() {
        return ACCO_BLNC;
    }

    public void setACCO_BLNC(String ACCO_BLNC) {
        this.ACCO_BLNC = ACCO_BLNC;
    }

    public String getWTCH_POSB_AMT() {
        return WTCH_POSB_AMT;
    }

    public void setWTCH_POSB_AMT(String WTCH_POSB_AMT) {
        this.WTCH_POSB_AMT = WTCH_POSB_AMT;
    }

//    public String getAPL_INRT() {
//        return APL_INRT;
//    }
//
//    public void setAPL_INRT(String APL_INRT) {
//        this.APL_INRT = APL_INRT;
//    }
//
//    public String getCHCR_YN() {
//        return CHCR_YN;
//    }
//
//    public void setCHCR_YN(String CHCR_YN) {
//        this.CHCR_YN = CHCR_YN;
//    }

    public String getLMIT_LMT_ACCO_YN() {
        return LMIT_LMT_ACCO_YN;
    }

    public void setLMIT_LMT_ACCO_YN(String LMIT_LMT_ACCO_YN) {
        this.LMIT_LMT_ACCO_YN = LMIT_LMT_ACCO_YN;
    }

    public String getBANK_NM() {
        return BANK_NM;
    }

    public void setBANK_NM(String BANK_NM) {
        this.BANK_NM = BANK_NM;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    //    public String getSMRT_WTCH_REG_YN() {
//        return SMRT_WTCH_REG_YN;
//    }
//
//    public void setSMRT_WTCH_REG_YN(String SMRT_WTCH_REG_YN) {
//        this.SMRT_WTCH_REG_YN = SMRT_WTCH_REG_YN;
//    }
//
//    public String getIMMD_WTCH_ACCO_YN() {
//        return IMMD_WTCH_ACCO_YN;
//    }
//
//    public void setIMMD_WTCH_ACCO_YN(String IMMD_WTCH_ACCO_YN) {
//        this.IMMD_WTCH_ACCO_YN = IMMD_WTCH_ACCO_YN;
//    }
//
//    public String getATM_WTCH_LMT_ACCO_YN() {
//        return ATM_WTCH_LMT_ACCO_YN;
//    }
//
//    public void setATM_WTCH_LMT_ACCO_YN(String ATM_WTCH_LMT_ACCO_YN) {
//        this.ATM_WTCH_LMT_ACCO_YN = ATM_WTCH_LMT_ACCO_YN;
//    }
//
//    public String getATTR_NEW_PROP_YN() {
//        return ATTR_NEW_PROP_YN;
//    }
//
//    public void setATTR_NEW_PROP_YN(String ATTR_NEW_PROP_YN) {
//        this.ATTR_NEW_PROP_YN = ATTR_NEW_PROP_YN;
//    }
//
//    public String getSECU_ACCO_SETP_YN() {
//        return SECU_ACCO_SETP_YN;
//    }
//
//    public void setSECU_ACCO_SETP_YN(String SECU_ACCO_SETP_YN) {
//        this.SECU_ACCO_SETP_YN = SECU_ACCO_SETP_YN;
//    }

    public String getRPRS_ACCO_YN() {
        return RPRS_ACCO_YN;
    }

    public void setRPRS_ACCO_YN(String RPRS_ACCO_YN) {
        this.RPRS_ACCO_YN = RPRS_ACCO_YN;
    }

    public String getACDT_DCL_CD() {
        return ACDT_DCL_CD;
    }

    public void setACDT_DCL_CD(String ACDT_DCL_CD) {
        this.ACDT_DCL_CD = ACDT_DCL_CD;
    }

    public String getSHRN_ACCO_YN() {
        return SHRN_ACCO_YN;
    }

    public void setSHRN_ACCO_YN(String SHRN_ACCO_YN) {
        this.SHRN_ACCO_YN = SHRN_ACCO_YN;
    }

    public String getTextForSearch(){
        return PROD_NM + "#" + ACCO_ALS + "#" + ACNO;
    }
}
