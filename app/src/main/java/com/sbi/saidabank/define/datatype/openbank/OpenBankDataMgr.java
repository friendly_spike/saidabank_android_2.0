package com.sbi.saidabank.define.datatype.openbank;

import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OpenBankDataMgr {
    private static OpenBankDataMgr instance = null;
    private int    mAccountCnt=0;


    private String SLRY_TRNF_YN;//급여이체 가입여부

    //출금계좌 다이얼로그에서 오픈뱅킹 잔액 조회가 필요할 경우를 위해 추가.
    private boolean IS_NEED_SEARCH_AMOUNT;

    //그룹정보
    private ArrayList<OpenBankGroupInfo> mGroupArrayList;

    //Instance 생성자
    public static OpenBankDataMgr getInstance() {
        if (instance == null) {
            synchronized ( OpenBankDataMgr.class) {
                if (instance == null) {
                    instance = new OpenBankDataMgr();
                }
            }
        }
        return instance;
    }

    public OpenBankDataMgr(){
        mGroupArrayList = new ArrayList<OpenBankGroupInfo>();
        IS_NEED_SEARCH_AMOUNT = false;
    }

    public String getSLRY_TRNF_YN() {
        return SLRY_TRNF_YN;
    }

    public void setSLRY_TRNF_YN(String SLRY_TRNF_YN) {
        this.SLRY_TRNF_YN = SLRY_TRNF_YN;
    }

    public boolean isIS_NEED_SEARCH_AMOUNT() {
        return IS_NEED_SEARCH_AMOUNT;
    }

    public void setIS_NEED_SEARCH_AMOUNT(boolean IS_NEED_SEARCH_AMOUNT) {
        this.IS_NEED_SEARCH_AMOUNT = IS_NEED_SEARCH_AMOUNT;
    }

    /**
     * 오픈뱅킹 계좌 리스트로 그룹과 각 그룹별 계좌를 정리한다.
     * @param object
     */
    public int parsorAccountList(JSONObject object){
        if(mGroupArrayList.size() > 0){
            for (int i=0;i<mGroupArrayList.size();i++){
                mGroupArrayList.get(i).getAccountList().clear();
            }
            mGroupArrayList.clear();
        }

        String groupCd="";
        String groupName="";
        ArrayList<OpenBankAccountInfo> accountArrayList = new ArrayList<OpenBankAccountInfo>();

        try{
            JSONArray jsonArray = object.optJSONArray("REC");

            if (jsonArray != null && jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {

                    JSONObject obj = jsonArray.getJSONObject(i);
                    OpenBankAccountInfo obAccInfo = new OpenBankAccountInfo(obj);
                    Logs.e(i + ".accountInfo :" + obj.toString());

                    //그룹 코드가 달라지면 그룹리스트에 담아둔다.
                    if(i>0 && !groupCd.equalsIgnoreCase(obAccInfo.OBA_ACCO_KNCD)){
                        mGroupArrayList.add(new OpenBankGroupInfo(groupCd,groupName,accountArrayList));
                        accountArrayList.clear();
                    }

                    accountArrayList.add(obAccInfo);

                    if(!groupCd.equalsIgnoreCase(obAccInfo.OBA_ACCO_KNCD)){
                        groupCd = obAccInfo.OBA_ACCO_KNCD;
//                        if(groupCd.equalsIgnoreCase("T")) {
//                            //종합계좌이면 - 그룹 종류는 수익증권(6)으로 변경하고 종합계좌 여부만 가지고 있도록 한다.
//                            groupCd = "6";
//                        }
                        groupName = obAccInfo.OBA_ACCO_KNCD_NM;
                    }
                }
                mAccountCnt = jsonArray.length();
                //마지막 그룹정보를 넣어준다.
                if(accountArrayList.size() > 0)
                    mGroupArrayList.add(new OpenBankGroupInfo(groupCd,groupName,accountArrayList));

                return mAccountCnt;
            }
        } catch (JSONException e) {
            MLog.e(e);
        }

        return 0;
    }

    public int getGroupCnt(){
        return mGroupArrayList.size();
    }

    public int getAccountCnt(){
        return mAccountCnt;
    }

    public String getTotalAmountString(){
        return Utils.moneyFormatToWon(getTotalAmount());
    }

    public Double getTotalAmount(){
        Double totalAmount = 0d;
        for(int grpIdx=0;grpIdx<mGroupArrayList.size();grpIdx++){
            ArrayList<OpenBankAccountInfo> accList = mGroupArrayList.get(grpIdx).getAccountList();
            for(int accIdx = 0;accIdx<accList.size();accIdx++){
                OpenBankAccountInfo accInfo = accList.get(accIdx);
                if(!TextUtils.isEmpty(accInfo.BLNC_AMT)){
                    //BLNC = Utils.moneyFormatToWon(Double.valueOf(BLNC));
                    totalAmount = totalAmount + Double.valueOf(accInfo.BLNC_AMT);
                }

            }
        }

        return totalAmount;
    }

    public String getDetailBankCd(String accountNo){

        for(int grpIdx=0;grpIdx<mGroupArrayList.size();grpIdx++){
            ArrayList<OpenBankAccountInfo> accList = mGroupArrayList.get(grpIdx).getAccountList();
            for(int accIdx = 0;accIdx<accList.size();accIdx++){
                OpenBankAccountInfo accInfo = accList.get(accIdx);
                if(accInfo.ACNO.equalsIgnoreCase(accountNo)){
                    return accInfo.DTLS_FNLT_CD;
                }
            }
        }

        return "";
    }

    public ArrayList<OpenBankGroupInfo> getOpenBankGroupList(){
        return mGroupArrayList;
    }

    /**
     * 오픈뱅킹 입출금 계좌리스트를 가져온다.
     * @return
     */
    public ArrayList<OpenBankAccountInfo> getOpenBankInOutAccountList(){
        ArrayList<OpenBankAccountInfo> accountInfoArrayList = new ArrayList<OpenBankAccountInfo>();

        for(int i=0;i<mGroupArrayList.size();i++){
            if(mGroupArrayList.get(i).groupCd.equalsIgnoreCase("1")){
                accountInfoArrayList.addAll(mGroupArrayList.get(i).accountArrayList);
            }
        }

        //정상계좌만 처리하기 위해 한번더...
        ArrayList<OpenBankAccountInfo> realAccountInfoArrayList = new ArrayList<OpenBankAccountInfo>();
        for(int i=0;i<accountInfoArrayList.size();i++){
            OpenBankAccountInfo info = accountInfoArrayList.get(i);
            if(info.ACNO_REG_STCD.equalsIgnoreCase("1") || info.ACNO_REG_STCD.equalsIgnoreCase("3")){
                realAccountInfoArrayList.add(info);
            }
        }


        return realAccountInfoArrayList;
    }

    /**
     * 오픈뱅킹 계좌리스트중  입금이 가능한 계좌 가져온다.
     * 6번 수익증권만 제외하면 된다.
     * @return
     */
    public ArrayList<OpenBankAccountInfo> getOpenBankDepositPosibleAccountList(){
        ArrayList<OpenBankAccountInfo> accountInfoArrayList = new ArrayList<OpenBankAccountInfo>();

        for(int i=0;i<mGroupArrayList.size();i++){
            //수익증권만 제외.
            if(!mGroupArrayList.get(i).groupCd.equalsIgnoreCase("6")){
                accountInfoArrayList.addAll(mGroupArrayList.get(i).accountArrayList);
            }
        }

        //정상계좌만 처리하기 위해 한번더...
        ArrayList<OpenBankAccountInfo> realAccountInfoArrayList = new ArrayList<OpenBankAccountInfo>();
        for(int i=0;i<accountInfoArrayList.size();i++){
            OpenBankAccountInfo info = accountInfoArrayList.get(i);
            if(info.ACNO_REG_STCD.equalsIgnoreCase("1")){
                realAccountInfoArrayList.add(info);
            }
        }


        return realAccountInfoArrayList;
    }

    public OpenBankAccountInfo getOpenBankAccountInfo(int grpIndex,int accIndex){
        return mGroupArrayList.get(grpIndex).getAccountList().get(accIndex);
    }

    /**
     * 입출금계좌중 원하는 계좌의 출금가능금액을 리턴한다.
     */
    public String getWTCH_POSB_AMT(String bankCd,String accountNo){
        ArrayList<OpenBankAccountInfo> accountInfoArrayList = getOpenBankInOutAccountList();

        for(int i=0;i<accountInfoArrayList.size();i++){
            OpenBankAccountInfo info = accountInfoArrayList.get(i);
            if(info.RPRS_FNLT_CD.equalsIgnoreCase(bankCd) && info.ACNO.equalsIgnoreCase(accountNo)){
                return info.WTCH_POSB_AMT;
            }
        }

        return "0";
    }



    /**
     * GroupInfoList를 addAll로 복사할경우
     * GroupInfo는 2차원 배열이므로 GroupInfo내부의 accountInfo List는 주소값이 복사된다.
     * 만일 addAll로 복사할경우 원본데이타도 변경되므로 카피데이타 함수를 따로 만들어 사용하도록 한다.
     * @return
     */
    public ArrayList< OpenBankGroupInfo> getCopyGroupList() {
        ArrayList<OpenBankGroupInfo> groupList = new ArrayList<OpenBankGroupInfo>();

        for (int i = 0; i < mGroupArrayList.size(); i++) {
            String groupCd = mGroupArrayList.get(i).groupCd;
            String groupName = mGroupArrayList.get(i).groupName;

            OpenBankGroupInfo groupInfo = new OpenBankGroupInfo(groupCd, groupName);
            groupInfo.accountArrayList.addAll(mGroupArrayList.get(i).accountArrayList);
            groupList.add(groupInfo);
        }
        return groupList;
    }

    /**
     * 이체시 조회된 잔액과 출금가능금액을 업데이트 해둔다.
     *
     * @param accountNo    계좌번호
     * @param accAmount    잔액
     * @param WtchPosbAmount 출금가능금액
     */
    public void updateAccountAmount(String bankCd,String accountNo,String accAmount,String WtchPosbAmount){
        Logs.e("updateAccountAmount accountNo : " + accountNo);
        boolean isInputEnd = false;
        for (int grpIdx = 0; grpIdx < mGroupArrayList.size(); grpIdx++) {
            ArrayList<OpenBankAccountInfo> accountInfoArrayList = mGroupArrayList.get(grpIdx).accountArrayList;
            for(int accIdx=0;accIdx<accountInfoArrayList.size();accIdx++){

                if( accountInfoArrayList.get(accIdx).RPRS_FNLT_CD.equalsIgnoreCase(bankCd) &&
                    accountInfoArrayList.get(accIdx).ACNO.equalsIgnoreCase(accountNo)){

                    accountInfoArrayList.get(accIdx).BLNC_AMT = accAmount;
                    accountInfoArrayList.get(accIdx).WTCH_POSB_AMT = WtchPosbAmount;
                    isInputEnd = true;

                    Logs.e("PRODNM : " + accountInfoArrayList.get(accIdx).PROD_NM);
                    Logs.e("BLNC_AMT : " + accountInfoArrayList.get(accIdx).BLNC_AMT);
                    Logs.e("WTCH_POSB_AMT : " + accountInfoArrayList.get(accIdx).WTCH_POSB_AMT);
                    break;
                }
            }

            if(isInputEnd) break;
        }
    }

    /**
     * 이체시 조회된 잔액과 출금가능금액을 업데이트 해둔다.
     *
     * @param accountNo    계좌번호
     * @param accAmount    잔액
     * @param WtchPosbAmount 출금가능금액
     */
    public void updateAccountAmount(int realGroupIdx,String accountNo,String accAmount,String WtchPosbAmount){
        Logs.e("updateAccountAmount accountNo : " + accountNo);

        ArrayList<OpenBankAccountInfo> accountInfoArrayList = mGroupArrayList.get(realGroupIdx).accountArrayList;
        for(int accIdx=0;accIdx<accountInfoArrayList.size();accIdx++){
            if(accountInfoArrayList.get(accIdx).ACNO.equalsIgnoreCase(accountNo)){
                accountInfoArrayList.get(accIdx).BLNC_AMT = accAmount;
                accountInfoArrayList.get(accIdx).WTCH_POSB_AMT = WtchPosbAmount;

                Logs.e("PRODNM : " + accountInfoArrayList.get(accIdx).PROD_NM);
                Logs.e("BLNC_AMT : " + accountInfoArrayList.get(accIdx).BLNC_AMT);
                Logs.e("WTCH_POSB_AMT : " + accountInfoArrayList.get(accIdx).WTCH_POSB_AMT);
                break;
            }
        }
    }


     /**
     * 그룹 정보
     */
    public static class OpenBankGroupInfo{
        private String groupCd;
        private String groupName;

        //계좌리스트
        private ArrayList<OpenBankAccountInfo> accountArrayList;

        public OpenBankGroupInfo(String groupCd,String groupName,ArrayList<OpenBankAccountInfo> arrayList){
            this.groupCd = groupCd;
            this.groupName = groupName;

            accountArrayList = new ArrayList<OpenBankAccountInfo>();
            accountArrayList.addAll(arrayList);
        }

        public OpenBankGroupInfo(String groupCd,String groupName){
            this.groupCd = groupCd;
            this.groupName = groupName;

            accountArrayList = new ArrayList<OpenBankAccountInfo>();
        }

        public ArrayList<OpenBankAccountInfo> getAccountList(){
            return accountArrayList;
        }

        public String getGroupCd() {
            return groupCd;
        }

        public String getGroupName() {
            return groupName;
        }

        public int getGroupAccCnt() {
            return accountArrayList.size();
        }

        public String getGroupAmountSum(){
            Double sumAmount=0d;
            for(int i=0;i<accountArrayList.size();i++){
                String amountStr = accountArrayList.get(i).BLNC_AMT;
                if(!TextUtils.isEmpty(amountStr)){
                    sumAmount += Double.parseDouble(amountStr);
                }
            }

            return Utils.moneyFormatToWon(sumAmount);
        }
    }
}
