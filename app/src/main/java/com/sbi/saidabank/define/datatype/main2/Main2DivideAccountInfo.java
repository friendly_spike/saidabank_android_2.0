package com.sbi.saidabank.define.datatype.main2;

import com.sbi.saidabank.common.util.Logs;

import org.json.JSONObject;

public class Main2DivideAccountInfo {
    private String ACNO;	            //계좌번호
    private String ACDIV_USG_CD;	    //계좌분리용도
    private String USE_CLS_ACCO_ALS;	//용도분류계좌별칭
    private String ENTR_LMIT_AMT;	    //가입한도금액
    private String ACCO_BLNC;           //계좌잔액
    private String ATMT_SCHD_AMT;       //자동이체예정금액



    public Main2DivideAccountInfo(JSONObject object) {
        ACNO = object.optString("ACNO");
        ACDIV_USG_CD = object.optString("ACDIV_USG_CD");
        USE_CLS_ACCO_ALS = object.optString("USE_CLS_ACCO_ALS");
        ENTR_LMIT_AMT = object.optString("ENTR_LMIT_AMT");
        ACCO_BLNC = object.optString("ACCO_BLNC");
        ATMT_SCHD_AMT = object.optString("ATMT_SCHD_AMT");
    }

    public String getACNO() {
        return ACNO;
    }

    public void setACNO(String ACNO) {
        this.ACNO = ACNO;
    }

    public String getACDIV_USG_CD() {
        return ACDIV_USG_CD;
    }

    public void setACDIV_USG_CD(String ACDIV_USG_CD) {
        this.ACDIV_USG_CD = ACDIV_USG_CD;
    }

    public String getUSE_CLS_ACCO_ALS() {
        return USE_CLS_ACCO_ALS;
    }

    public void setUSE_CLS_ACCO_ALS(String USG_CLS_ACCO_ALS) {
        this.USE_CLS_ACCO_ALS = USG_CLS_ACCO_ALS;
    }

    public String getENTR_LMIT_AMT() {
        return ENTR_LMIT_AMT;
    }

    public void setENTR_LMIT_AMT(String ENTR_LIMIT_AMT) {
        this.ENTR_LMIT_AMT = ENTR_LIMIT_AMT;
    }

    public String getACCO_BLNC() {
        return ACCO_BLNC;
    }

    public void setACCO_BLNC(String ACCO_BLNC) {
        this.ACCO_BLNC = ACCO_BLNC;
    }

    public String getATMT_SCHD_AMT() {
        return ATMT_SCHD_AMT;
    }

    public void setATMT_SCHD_AMT(String ATMT_SCHD_AMT) {
        this.ATMT_SCHD_AMT = ATMT_SCHD_AMT;
    }
}
