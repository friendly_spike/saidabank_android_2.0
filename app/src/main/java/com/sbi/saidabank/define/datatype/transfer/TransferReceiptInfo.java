package com.sbi.saidabank.define.datatype.transfer;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * TransferReceiptInfo : 계좌 이체 결과 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TransferReceiptInfo implements Serializable {

    private String WTCH_ACNO;           // 출금계좌번호
    private String EROR_CNTA_TLNO;      // 오류연락전화번호
    private String CNTP_FIN_INST_CD;    // 상대 금융기관코드
    private String CNTP_BANK_ACNO;      // 상대 은행계좌번호
    private String CNTP_ACCO_DEPR_NM;   // 상대 계좌예금주명
    private String MNRC_ACCO_MRK_CNTN;  // 입금 계좌 표시내용
    private String WTCH_ACCO_MRK_CNTN;  // 출금 계좌 표시내용
    private String TRN_AMT;             // 거래금액
    private String TRN_FEE;             // 거래수수료
    private String REDU_FEE;            // 감면수수료
    private String FEE_REDU_RSCD;       // 수수료 감면 사유코드

    private String FAVO_ACCO_YN;        // 즐겨찾기 계좌여부
    private String ACCO_ALS;            // 출금계좌별칭

    private String TRNF_DT;             // 이체일자
    private String TRNF_TM;             // 이체시각
    private String TRTM_STS_DVCD;       // 처리상태구분코드
    private String RESP_CD;             // 응답코드
    private String RESP_CNTN;           // 응답내용
    private String TRN_MEMO_CNTN;       // 거래 메모내용
    private String TRN_UNQ_NO;          // 거래 고유번호

    private String MNRC_BANK_NM;        // 상대은행명

    private String SVC_DVCD;            // 서비스 구분 코드
    private String BANK_ACCO_NM;        // 출금되는 은행계좌명
    private String TRNF_DVCD;           // 이체구분 코드 (1:즉시, 2:지연, 3:예약, 4:간편, 5:입금지정)


    private String PROD_NM;             //받는계좌명
    private String GRP_SRNO;            //그룹번호
    private String MNRC_ACCO_ALS;       //그룹 별명 - 다건이체에서 사용.

    private String WTCH_LMIT_REMN_AMT; //출금한도 잔여금액

    public TransferReceiptInfo(JSONObject object) {
        WTCH_ACNO = object.optString("WTCH_ACNO");
        EROR_CNTA_TLNO = object.optString("EROR_CNTA_TLNO");
        CNTP_FIN_INST_CD = object.optString("CNTP_FIN_INST_CD");
        CNTP_BANK_ACNO = object.optString("CNTP_BANK_ACNO");
        CNTP_ACCO_DEPR_NM = object.optString("CNTP_ACCO_DEPR_NM");

        MNRC_ACCO_MRK_CNTN = object.optString("MNRC_ACCO_MRK_CNTN");
        WTCH_ACCO_MRK_CNTN = object.optString("WTCH_ACCO_MRK_CNTN");
        TRN_AMT = object.optString("TRN_AMT");
        TRN_FEE = object.optString("TRN_FEE");
        REDU_FEE = object.optString("REDU_FEE");
        FEE_REDU_RSCD = object.optString("FEE_REDU_RSCD");
        FAVO_ACCO_YN = object.optString("FAVO_ACCO_YN");
        ACCO_ALS =  object.optString("ACCO_ALS");
        TRNF_DT = object.optString("TRNF_DT");
        TRNF_TM = object.optString("TRNF_TM");
        TRTM_STS_DVCD = object.optString("TRTM_STS_DVCD");
        RESP_CD = object.optString("RESP_CD");
        RESP_CNTN = object.optString("RESP_CNTN");
        TRN_MEMO_CNTN = object.optString("TRN_MEMO_CNTN");
        TRN_UNQ_NO = object.optString("TRN_UNQ_NO");
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");
        SVC_DVCD = object.optString("SVC_DVCD");
        BANK_ACCO_NM = object.optString("BANK_ACCO_NM");
        TRNF_DVCD = object.optString("TRNF_DVCD");

        //통합이체에서 추가.
        PROD_NM = object.optString("PROD_NM");
        GRP_SRNO = object.optString("GRP_SRNO");
        MNRC_ACCO_ALS = object.optString("MNRC_ACCO_ALS");

        //오픈뱅킹때문에 추가
        WTCH_LMIT_REMN_AMT = object.optString("WTCH_LMIT_REMN_AMT");
    }

    public String getWTCH_ACNO() {
        return WTCH_ACNO;
    }

    public void setWTCH_ACNO(String WTCH_ACNO) {
        this.WTCH_ACNO = WTCH_ACNO;
    }

    public String getEROR_CNTA_TLNO() {
        return EROR_CNTA_TLNO;
    }

    public void setEROR_CNTA_TLNO(String EROR_CNTA_TLNO) {
        this.EROR_CNTA_TLNO = EROR_CNTA_TLNO;
    }

    public String getCNTP_FIN_INST_CD() {
        return CNTP_FIN_INST_CD;
    }

    public void setCNTP_FIN_INST_CD(String CNTP_FIN_INST_CD) {
        this.CNTP_FIN_INST_CD = CNTP_FIN_INST_CD;
    }

    public String getCNTP_BANK_ACNO() {
        return CNTP_BANK_ACNO;
    }

    public void setCNTP_BANK_ACNO(String CNTP_BANK_ACNO) {
        this.CNTP_BANK_ACNO = CNTP_BANK_ACNO;
    }

    public String getCNTP_ACCO_DEPR_NM() {
        return CNTP_ACCO_DEPR_NM;
    }

    public void setCNTP_ACCO_DEPR_NM(String CNTP_ACCO_DEPR_NM) {
        this.CNTP_ACCO_DEPR_NM = CNTP_ACCO_DEPR_NM;
    }

    public String getMNRC_ACCO_MRK_CNTN() {
        return MNRC_ACCO_MRK_CNTN;
    }

    public void setMNRC_ACCO_MRK_CNTN(String MNRC_ACCO_MRK_CNTN) {
        this.MNRC_ACCO_MRK_CNTN = MNRC_ACCO_MRK_CNTN;
    }

    public String getWTCH_ACCO_MRK_CNTN() {
        return WTCH_ACCO_MRK_CNTN;
    }

    public void setWTCH_ACCO_MRK_CNTN(String WTCH_ACCO_MRK_CNTN) {
        this.WTCH_ACCO_MRK_CNTN = WTCH_ACCO_MRK_CNTN;
    }

    public String getTRN_AMT() {
        return TRN_AMT;
    }

    public void setTRN_AMT(String TRN_AMT) {
        this.TRN_AMT = TRN_AMT;
    }

    public String getTRN_FEE() {
        return TRN_FEE;
    }

    public void setTRN_FEE(String TRN_FEE) {
        this.TRN_FEE = TRN_FEE;
    }

    public String getREDU_FEE() {
        return REDU_FEE;
    }

    public void setREDU_FEE(String REDU_FEE) {
        this.REDU_FEE = REDU_FEE;
    }

    public String getFEE_REDU_RSCD() {
        return FEE_REDU_RSCD;
    }

    public void setFEE_REDU_RSCD(String FEE_REDU_RSCD) {
        this.FEE_REDU_RSCD = FEE_REDU_RSCD;
    }

    public String getFAVO_ACCO_YN() {
        return FAVO_ACCO_YN;
    }

    public void setFAVO_ACCO_YN(String FAVO_ACCO_YN) {
        this.FAVO_ACCO_YN = FAVO_ACCO_YN;
    }

    public String getACCO_ALS() {
        return ACCO_ALS;
    }

    public void setACCO_ALS(String ACCO_ALS) {
        this.ACCO_ALS = ACCO_ALS;
    }

    public String getTRNF_DT() {
        return TRNF_DT;
    }

    public void setTRNF_DT(String TRNF_DT) {
        this.TRNF_DT = TRNF_DT;
    }

    public String getTRNF_TM() {
        return TRNF_TM;
    }

    public void setTRNF_TM(String TRNF_TM) {
        this.TRNF_TM = TRNF_TM;
    }

    public String getTRTM_STS_DVCD() {
        return TRTM_STS_DVCD;
    }

    public void setTRTM_STS_DVCD(String TRTM_STS_DVCD) {
        this.TRTM_STS_DVCD = TRTM_STS_DVCD;
    }

    public String getRESP_CD() {
        return RESP_CD;
    }

    public void setRESP_CD(String RESP_CD) {
        this.RESP_CD = RESP_CD;
    }

    public String getRESP_CNTN() {
        return RESP_CNTN;
    }

    public void setRESP_CNTN(String RESP_CNTN) {
        this.RESP_CNTN = RESP_CNTN;
    }

    public String getTRN_MEMO_CNTN() {
        return TRN_MEMO_CNTN;
    }

    public void setTRN_MEMO_CNTN(String TRN_MEMO_CNTN) {
        this.TRN_MEMO_CNTN = TRN_MEMO_CNTN;
    }

    public String getTRN_UNQ_NO() {
        return TRN_UNQ_NO;
    }

    public void setTRN_UNQ_NO(String TRN_UNQ_NO) {
        this.TRN_UNQ_NO = TRN_UNQ_NO;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    public String getSVC_DVCD() {
        return SVC_DVCD;
    }

    public void setSVC_DVCD(String SVC_DVCD) {
        this.SVC_DVCD = SVC_DVCD;
    }

    public String getBANK_ACCO_NM() {
        return BANK_ACCO_NM;
    }

    public void setBANK_ACCO_NM(String BANK_ACCO_NM) {
        this.BANK_ACCO_NM = BANK_ACCO_NM;
    }

    public String getTRNF_DVCD() {
        return TRNF_DVCD;
    }

    public void setTRNF_DVCD(String TRNF_DVCD) {
        this.TRNF_DVCD = TRNF_DVCD;
    }

    public String getPROD_NM() {
        return PROD_NM;
    }

    public void setPROD_NM(String PROD_NM) {
        this.PROD_NM = PROD_NM;
    }

    public String getGRP_SRNO() {
        return GRP_SRNO;
    }

    public void setGRP_SRNO(String GRP_SRNO) {
        this.GRP_SRNO = GRP_SRNO;
    }

    public String getMNRC_ACCO_ALS() {
        return MNRC_ACCO_ALS;
    }

    public void setMNRC_ACCO_ALS(String MNRC_ACCO_ALS) {
        this.MNRC_ACCO_ALS = MNRC_ACCO_ALS;
    }

    public String getWTCH_LMIT_REMN_AMT() {
        return WTCH_LMIT_REMN_AMT;
    }

    public void setWTCH_LMIT_REMN_AMT(String WTCH_LMIT_REMN_AMT) {
        this.WTCH_LMIT_REMN_AMT = WTCH_LMIT_REMN_AMT;
    }
}
