package com.sbi.saidabank.define.datatype.main2;

import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Main2DataInfo {
    private static Main2DataInfo instance = null;

    //*********** 나의계좌 탭 에서 서비스 조회후 넘겨 받는값. ************************
    private String MBR_NO;	                //회원번호
    private String MAIN_EVNT_PRTP_POSB_YN;	//메인이벤트참여가능여부
    private String EVNT_ID;	                //이벤트ID
    private String USER_PROFL_IMG_CNTN;	    //사용자프로필이미지내용
    private String SHRP_PROFL_IMG_CNTN;	    //공유자프로필이미지내용
    private String USER_NM;	                //사용자명
    private String SHRP_NM;	                //공유자명
    private String PROFL_IMG_CNTN;	        //공유진행상태코드
    private String SHRP_TRN_DD;	            //공유자이메일

    /*
    00 : 공유형통장 미가입
    01 : 공유형통장 초대취소 및 거절(초대전)
    02 : 공유형통장 수락대기(계좌주)
    03 : 공유형통장 수락
    04 : 공유향통장 수락대기(공유자)
    99 : 일지정지                                                       */
    private String SHRN_PRGS_STCD;      	//공유진행상태코드
    private String SHRP_DVCD;	            //공유자구분코드 00:계좌주, 01:공유자


    private String ACCOUNT_CHARGE_SET_YN;	//계좌충전 설정여부 - 서버에서 내려주는 데이타가 아님. 서버데이타 체크하여 직접 값 넣음.

    //커플통장에서 초대수락대기 이어하기 체크해야 되어서 변수값 가지고 있자.
    private ArrayList<Main2RelayInfo> main2RelayArray;

    //계좌분리 정보 리스트
    private ArrayList<Main2DivideAccountInfo> main2DivideAccountArray;

    //필요시 마다 계좌 리스트 복사용. - 계좌 순서변경화면에서 사용.
    private ArrayList<Main2AccountInfo> main2Main2AccountInfo;


    //*********** 커플계좌 탭 에서 서비스 조회후 넘겨 받는값. ************************

    //private String SHRP_DVCD;	    //위에 있음.. 같이 사용.
    private String SHRN_PROP_STCD;	//공유신청상태코드
    private String SHRP_SETP_DT;	//공유자설정일자
    private String SHRP_SETP_TM;	//공유자설정시각
    private String SHRN_INVT_SRNO;	//공유초대일련번호
    private String SHRP_MBR_NO;	    //공유자회원번호
    private String SHRP_ACCO_CCNT;	//공유형계좌건수
    private String SHRP_ACCO_SUM_AMT;	//공유형계좌합계금액
    private String PVMN2_BSMN;	    //전전월기준월
    private String PVMN2_USE_AMT;	//전전월사용금액
    private String PVMN_BSMN;	    //전월기준월
    private String PVMN_USE_AMT;	//전월사용금액
    private String SETT_HSTR_YN;	//결제이력여부

    //결젝금액별 리스트
    private ArrayList<Main2CoupleExpenceAmount> main2CoupleExpenceAmountArray;

    //결제 건수별 리스트
    private ArrayList<Main2CoupleExpenceCount> main2CoupleExpenceCountArray;

    public static Main2DataInfo getInstance() {
        if (instance == null) {
            synchronized (Main2DataInfo.class) {
                if (instance == null) {
                    instance = new Main2DataInfo();
                }
            }
        }
        return instance;
    }

    public static void clearInstance() {
        if (instance == null)
            return;

        instance = null;
    }

    public Main2DataInfo() {
        //커플통장에서 초대수락대기 이어하기 체크해야 되어서 변수값 가지고 있자.
        main2RelayArray = new ArrayList<Main2RelayInfo>();

        //계좌분리 정보 리스트
        main2DivideAccountArray = new ArrayList<Main2DivideAccountInfo>();

        //필요시 마다 계좌 리스트 복사용. - 계좌 순서변경화면에서 사용.
        main2Main2AccountInfo = new ArrayList<Main2AccountInfo>();

        //결젝금액별 리스트
        main2CoupleExpenceAmountArray = new ArrayList<Main2CoupleExpenceAmount>();

        //결제 건수별 리스트
        main2CoupleExpenceCountArray = new ArrayList<Main2CoupleExpenceCount>();
    }

    /**
     * ================================================================================================
     * 나의 계좌 에서 설정하는 값과 함수들

     * =================================================================================================
     */
    public void  setMain2DataInfoFromMyAccount(JSONObject object){
        MBR_NO = object.optString("MBR_NO");
        MAIN_EVNT_PRTP_POSB_YN = object.optString("MAIN_EVNT_PRTP_POSB_YN");
        EVNT_ID = object.optString("EVNT_ID");
        USER_PROFL_IMG_CNTN = object.optString("USER_PROFL_IMG_CNTN");
        SHRP_PROFL_IMG_CNTN = object.optString("SHRP_PROFL_IMG_CNTN");
        USER_NM = object.optString("USER_NM");
        SHRP_NM = object.optString("SHRP_NM");
        PROFL_IMG_CNTN = object.optString("PROFL_IMG_CNTN");
        SHRP_TRN_DD = object.optString("SHRP_TRN_DD");
        SHRN_PRGS_STCD = object.optString("SHRN_PRGS_STCD");
        SHRP_DVCD = object.optString("SHRP_DVCD","00");
        if(TextUtils.isEmpty(SHRP_DVCD)) SHRP_DVCD = "00";
        ACCOUNT_CHARGE_SET_YN = "N";

        Logs.i("Start Main2 Data ==============================================");
        Logs.i("MBR_NO : " + MBR_NO);
        Logs.i("MAIN_EVNT_PRTP_POSB_YN : " + MAIN_EVNT_PRTP_POSB_YN);
        Logs.i("EVNT_ID : " + EVNT_ID);
        Logs.i("USER_PROFL_IMG_CNTN : " + USER_PROFL_IMG_CNTN);
        Logs.i("SHRP_PROFL_IMG_CNTN : " + SHRP_PROFL_IMG_CNTN);
        Logs.i("USER_NM : " + USER_NM);
        Logs.i("SHRP_NM : " + SHRP_NM);
        Logs.i("PROFL_IMG_CNTN : " + PROFL_IMG_CNTN);
        Logs.i("SHRP_TRN_DD : " + SHRP_TRN_DD);
        Logs.i("SHRACCOUNT_CHARGE_SET_YNP_NM : " + ACCOUNT_CHARGE_SET_YN);


        //계좌분리 정보 조회
        if(main2DivideAccountArray == null)
            main2DivideAccountArray = new ArrayList<>();
        else
            main2DivideAccountArray.clear();
        try {
            JSONArray divide_array = object.optJSONArray("REC_ACDIVS");
            if (divide_array != null) {
                Logs.e("divide_array : " + divide_array.toString());
                for (int i = 0; i < divide_array.length(); i++) {
                    JSONObject jsonObject = divide_array.getJSONObject(i);
                    if (jsonObject == null)
                        continue;
//                        try {
//                            MLog.i(" ############### Start Main2 Divide Account ###############");
//                            ParseUtil.parseJSONObject(jsonObject);
//                            MLog.i(" ############### End Main2 Divide Account ###############");
//                        } catch (Exception e) {
//                            MLog.e(e);
//                        }
                    Main2DivideAccountInfo divideInfo = new Main2DivideAccountInfo(jsonObject);
                    main2DivideAccountArray.add(divideInfo);
                }
            }
        } catch (JSONException e) {
            Logs.printException(e);
        }

        //메인 이어하기 리스트 백업
        if(main2RelayArray == null)
            main2RelayArray = new ArrayList<>();

        //메인 계좌 리스트 백업을 위해 추가 - 계좌 순서 정렬 화면에서 사용
        if(main2Main2AccountInfo == null)
            main2Main2AccountInfo = new ArrayList<>();

        Logs.e("End Main2 Data ==============================================");
    }

    public String getMBR_NO() {
        return MBR_NO;
    }

    public void setMBR_NO(String MBR_NO) {
        this.MBR_NO = MBR_NO;
    }

    public String getMAIN_EVNT_PRTP_POSB_YN() {
        return MAIN_EVNT_PRTP_POSB_YN;
    }

    public void setMAIN_EVNT_PRTP_POSB_YN(String MAIN_EVNT_PRTP_POSB_YN) {
        this.MAIN_EVNT_PRTP_POSB_YN = MAIN_EVNT_PRTP_POSB_YN;
    }

    public String getEVNT_ID() {
        return EVNT_ID;
    }

    public void setEVNT_ID(String EVNT_ID) {
        this.EVNT_ID = EVNT_ID;
    }

    public String getUSER_PROFL_IMG_CNTN() {
        return USER_PROFL_IMG_CNTN;
    }

    public void setUSER_PROFL_IMG_CNTN(String USER_PROFL_IMG_CNTN) {
        this.USER_PROFL_IMG_CNTN = USER_PROFL_IMG_CNTN;
    }

    public String getSHRP_PROFL_IMG_CNTN() {
        return SHRP_PROFL_IMG_CNTN;
    }

    public void setSHRP_PROFL_IMG_CNTN(String SHRP_PROFL_IMG_CNTN) {
        this.SHRP_PROFL_IMG_CNTN = SHRP_PROFL_IMG_CNTN;
    }

    public String getUSER_NM() {
        return USER_NM;
    }

    public void setUSER_NM(String USER_NM) {
        this.USER_NM = USER_NM;
    }

    public String getSHRP_NM() {
        return SHRP_NM;
    }

    public void setSHRP_NM(String SHRP_NM) {
        this.SHRP_NM = SHRP_NM;
    }

    public String getPROFL_IMG_CNTN() {
        return PROFL_IMG_CNTN;
    }

    public void setPROFL_IMG_CNTN(String PROFL_IMG_CNTN) {
        this.PROFL_IMG_CNTN = PROFL_IMG_CNTN;
    }

    public String getSHRP_TRN_DD() {
        return SHRP_TRN_DD;
    }

    public void setSHRP_TRN_DD(String SHRP_TRN_DD) {
        this.SHRP_TRN_DD = SHRP_TRN_DD;
    }

    public String getSHRN_PRGS_STCD() {
        return SHRN_PRGS_STCD;
    }

    public void setSHRN_PRGS_STCD(String SHRN_PRGS_STCD) {
        this.SHRN_PRGS_STCD = SHRN_PRGS_STCD;
    }

    public String getSHRP_DVCD() {
        return SHRP_DVCD;
    }

    public void setSHRP_DVCD(String SHRP_DVCD) {
        this.SHRP_DVCD = SHRP_DVCD;
    }

    public String getACCOUNT_CHARGE_SET_YN() {
        return ACCOUNT_CHARGE_SET_YN;
    }

    public void setACCOUNT_CHARGE_SET_YN(String SET_ACCOUNT_CHARGE_YN) {
        this.ACCOUNT_CHARGE_SET_YN = SET_ACCOUNT_CHARGE_YN;
    }

    public ArrayList<Main2DivideAccountInfo> getMain2DivideAccountArray() {
        return main2DivideAccountArray;
    }

    public void setMain2DivideAccountArray(ArrayList<Main2DivideAccountInfo> divideAccountArray) {
        this.main2DivideAccountArray = divideAccountArray;
    }

    public ArrayList<Main2RelayInfo> getMain2RelayArray() {
        return main2RelayArray;
    }

    public void setMain2RelayArray(ArrayList<Main2RelayInfo> arrayList) {
        this.main2RelayArray.clear();
        if(arrayList.size() > 0)
            this.main2RelayArray.addAll(arrayList);
    }

    public ArrayList<Main2AccountInfo> getMain2Main2AccountInfo() {
        return main2Main2AccountInfo;
    }

    public void setMain2Main2AccountInfo(ArrayList<Main2AccountInfo> arrayList) {
        this.main2Main2AccountInfo.clear();
        if(arrayList != null && arrayList.size() > 0)
            this.main2Main2AccountInfo.addAll(arrayList);
    }

    /**
     * ================================================================================================
     * 커플 계좌 에서 설정하는 값과 함수들
     *
     * =================================================================================================
     */
    public void  setMain2DataInfoFromCoupleAccount(JSONObject object){
        SHRP_DVCD = object.optString("SHRP_DVCD");
        SHRN_PROP_STCD = object.optString("SHRN_PROP_STCD");
        SHRP_SETP_DT = object.optString("SHRP_SETP_DT");
        SHRP_SETP_TM = object.optString("SHRP_SETP_TM");
        SHRN_INVT_SRNO = object.optString("SHRN_INVT_SRNO");
        SHRP_MBR_NO = object.optString("SHRP_MBR_NO");
        SHRP_ACCO_CCNT = object.optString("SHRP_ACCO_CCNT","0");
        SHRP_ACCO_SUM_AMT = object.optString("SHRP_ACCO_SUM_AMT","0");
        PVMN2_BSMN = object.optString("PVMN2_BSMN");
        PVMN2_USE_AMT = object.optString("PVMN2_USE_AMT");
        PVMN_BSMN = object.optString("PVMN_BSMN");
        PVMN_USE_AMT = object.optString("PVMN_USE_AMT");
        SETT_HSTR_YN = object.optString("SETT_HSTR_YN");

        // 계좌분리 정보 조회
        if(main2DivideAccountArray == null)
            main2DivideAccountArray = new ArrayList<>();
        else
            main2DivideAccountArray.clear();

        try {
            JSONArray divide_array = object.optJSONArray("REC_ACDIVS");
            if (divide_array != null) {
                for (int i = 0; i < divide_array.length(); i++) {
                    JSONObject jsonObject = divide_array.getJSONObject(i);
                    if (jsonObject == null)
                        continue;
                    Main2DivideAccountInfo divideInfo = new Main2DivideAccountInfo(jsonObject);
                    main2DivideAccountArray.add(divideInfo);
                }
            }
        } catch (JSONException e) {
            Logs.printException(e);
        }
    }

    public String getSHRN_PROP_STCD() {
        return SHRN_PROP_STCD;
    }

    public void setSHRN_PROP_STCD(String SHRN_PROP_STCD) {
        this.SHRN_PROP_STCD = SHRN_PROP_STCD;
    }

    public String getSHRP_SETP_DT() {
        return SHRP_SETP_DT;
    }

    public void setSHRP_SETP_DT(String SHRN_SETP_DT) {
        this.SHRP_SETP_DT = SHRN_SETP_DT;
    }

    public String getSHRP_SETP_TM() {
        return SHRP_SETP_TM;
    }

    public void setSHRP_SETP_TM(String SHRN_SETP_TM) {
        this.SHRP_SETP_TM = SHRN_SETP_TM;
    }

    public String getSHRN_INVT_SRNO() {
        return SHRN_INVT_SRNO;
    }

    public void setSHRN_INVT_SRNO(String SHRN_INVT_SRNO) {
        this.SHRN_INVT_SRNO = SHRN_INVT_SRNO;
    }

    public String getSHRP_MBR_NO() {
        return SHRP_MBR_NO;
    }

    public void setSHRP_MBR_NO(String SHRN_MBR_NO) {
        this.SHRP_MBR_NO = SHRN_MBR_NO;
    }

    public String getSHRP_ACCO_CCNT() {
        return SHRP_ACCO_CCNT;
    }

    public void setSHRP_ACCO_CCNT(String SHRN_ACCO_CCNT) {
        this.SHRP_ACCO_CCNT = SHRN_ACCO_CCNT;
    }

    public String getSHRP_ACCO_SUM_AMT() {
        return SHRP_ACCO_SUM_AMT;
    }

    public void setSHRP_ACCO_SUM_AMT(String SHRN_ACCO_SUM_AMT) {
        this.SHRP_ACCO_SUM_AMT = SHRN_ACCO_SUM_AMT;
    }

    public String getPVMN2_BSMN() {
        return PVMN2_BSMN;
    }

    public void setPVMN2_BSMN(String PVMN2_BSMN) {
        this.PVMN2_BSMN = PVMN2_BSMN;
    }

    public String getPVMN2_USE_AMT() {
        return PVMN2_USE_AMT;
    }

    public void setPVMN2_USE_AMT(String PVMN2_USE_AMT) {
        this.PVMN2_USE_AMT = PVMN2_USE_AMT;
    }

    public String getPVMN_BSMN() {
        return PVMN_BSMN;
    }

    public void setPVMN_BSMN(String PVMN_BSMN) {
        this.PVMN_BSMN = PVMN_BSMN;
    }

    public String getPVMN_USE_AMT() {
        return PVMN_USE_AMT;
    }

    public void setPVMN_USE_AMT(String PVMN_USE_AMT) {
        this.PVMN_USE_AMT = PVMN_USE_AMT;
    }

    public String getSETT_HSTR_YN() {
        return SETT_HSTR_YN;
    }

    public void setSETT_HSTR_YN(String SETT_HSTR_YN) {
        this.SETT_HSTR_YN = SETT_HSTR_YN;
    }

    public ArrayList<Main2CoupleExpenceAmount> getMain2CoupleExpenceAmountArray() {
        return main2CoupleExpenceAmountArray;
    }

    public void setMain2CoupleExpenceAmountArray(ArrayList<Main2CoupleExpenceAmount> main2CoupleExpenceAmountArray) {
        this.main2CoupleExpenceAmountArray = main2CoupleExpenceAmountArray;
    }

    public ArrayList<Main2CoupleExpenceCount> getMain2CoupleExpenceCountArray() {
        return main2CoupleExpenceCountArray;
    }

    public void setMain2CoupleExpenceCountArray(ArrayList<Main2CoupleExpenceCount> main2CoupleExpenceCountArray) {
        this.main2CoupleExpenceCountArray = main2CoupleExpenceCountArray;
    }
}
