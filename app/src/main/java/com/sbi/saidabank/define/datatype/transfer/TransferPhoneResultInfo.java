package com.sbi.saidabank.define.datatype.transfer;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * TransferPhoneResultInfo : 휴대폰 이체 결과 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TransferPhoneResultInfo implements Serializable {

    private String TRNF_KEY_VAL;       // 이체키값
    private String FAVO_ACCO_YN;       // 즐겨찾기 계좌여부
    private String ACCO_IDNO;          // 계좌 식별번호
    private String TRN_SRNO;           // 거래 일련번호
    private String WTCH_ACNO;          // 출금 계좌번호
    private String MNRC_TRGT_DEPR_NM;  // 입금 대상예금주명
    private String MNRC_CPNO;          // 입금 휴대전화번호
    private String MNRC_ACNO;          // 입금 계좌번호
    private String TRAM;               // 이체금액
    private String TRNF_FEE;           // 이체수수료
    private String MNRC_ACCO_MRK_CNTN; // 입금 계좌 표시내용
    private String ACCO_ALS;           // 계좌별칭
    private String PROD_NM;            // 상품명

    public TransferPhoneResultInfo() {
    }

    public TransferPhoneResultInfo(JSONObject object) {
        TRNF_KEY_VAL = object.optString("TRNF_KEY_VAL");
        FAVO_ACCO_YN = object.optString("FAVO_ACCO_YN");
        ACCO_IDNO = object.optString("ACCO_IDNO");
        TRN_SRNO = object.optString("TRN_SRNO");
        WTCH_ACNO = object.optString("WTCH_ACNO");
        MNRC_TRGT_DEPR_NM = object.optString("MNRC_TRGT_DEPR_NM");
        MNRC_ACCO_MRK_CNTN = object.optString("MNRC_ACCO_MRK_CNTN");
        MNRC_CPNO = object.optString("MNRC_CPNO");
        MNRC_ACNO = object.optString("MNRC_ACNO");
        TRAM = object.optString("TRAM");
        TRNF_FEE = object.optString("TRNF_FEE");
        MNRC_ACCO_MRK_CNTN = object.optString("MNRC_ACCO_MRK_CNTN");
        ACCO_ALS = object.optString("ACCO_ALS");
        PROD_NM = object.optString("PROD_NM");
    }

    public String getTRNF_KEY_VAL() {
        return TRNF_KEY_VAL;
    }

    public void setTRNF_KEY_VAL(String TRNF_KEY_VAL) {
        this.TRNF_KEY_VAL = TRNF_KEY_VAL;
    }

    public String getFAVO_ACCO_YN() {
        return FAVO_ACCO_YN;
    }

    public void setFAVO_ACCO_YN(String FAVO_ACCO_YN) {
        this.FAVO_ACCO_YN = FAVO_ACCO_YN;
    }

    public String getACCO_IDNO() {
        return ACCO_IDNO;
    }

    public void setACCO_IDNO(String ACCO_IDNO) {
        this.ACCO_IDNO = ACCO_IDNO;
    }

    public String getTRN_SRNO() {
        return TRN_SRNO;
    }

    public void setTRN_SRNO(String TRN_SRNO) {
        this.TRN_SRNO = TRN_SRNO;
    }

    public String getWTCH_ACNO() {
        return WTCH_ACNO;
    }

    public void setWTCH_ACNO(String WTCH_ACNO) {
        this.WTCH_ACNO = WTCH_ACNO;
    }

    public String getMNRC_TRGT_DEPR_NM() {
        return MNRC_TRGT_DEPR_NM;
    }

    public void setMNRC_TRGT_DEPR_NM(String MNRC_TRGT_DEPR_NM) {
        this.MNRC_TRGT_DEPR_NM = MNRC_TRGT_DEPR_NM;
    }

    public String getMNRC_CPNO() {
        return MNRC_CPNO;
    }

    public void setMNRC_CPNO(String MNRC_CPNO) {
        this.MNRC_CPNO = MNRC_CPNO;
    }

    public String getMNRC_ACNO() {
        return MNRC_ACNO;
    }

    public void setMNRC_ACNO(String MNRC_ACNO) {
        this.MNRC_ACNO = MNRC_ACNO;
    }

    public String getTRAM() {
        return TRAM;
    }

    public void setTRAM(String TRAM) {
        this.TRAM = TRAM;
    }

    public String getTRNF_FEE() {
        return TRNF_FEE;
    }

    public void setTRNF_FEE(String TRNF_FEE) {
        this.TRNF_FEE = TRNF_FEE;
    }

    public String getMNRC_ACCO_MRK_CNTN() {
        return MNRC_ACCO_MRK_CNTN;
    }

    public void setMNRC_ACCO_MRK_CNTN(String MNRC_ACCO_MRK_CNTN) {
        this.MNRC_ACCO_MRK_CNTN = MNRC_ACCO_MRK_CNTN;
    }

    public String getACCO_ALS() {
        return ACCO_ALS;
    }

    public void setACCO_ALS(String ACCO_ALS) {
        this.ACCO_ALS = ACCO_ALS;
    }

    public String getPROD_NM() {
        return PROD_NM;
    }

    public void setPROD_NM(String PROD_NM) {
        this.PROD_NM = PROD_NM;
    }
}
