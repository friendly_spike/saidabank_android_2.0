package com.sbi.saidabank.define.datatype.main;

import android.graphics.Bitmap;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * AdsItemInfo : 메인화면 광고 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class MainAdsItemInfo implements Serializable {

    private String IMG_URL_ADDR;    // 이미지 url
    private String LINK_URL_ADDR;   // 링크 url

    private boolean IS_LOAD_BITMAP; //가져온 비트맵 이미지

    public MainAdsItemInfo(JSONObject object) {
        IMG_URL_ADDR = object.optString("IMG_URL_ADDR");
        LINK_URL_ADDR = object.optString("LINK_URL_ADDR");
    }

    public String getIMG_URL_ADDR() {
        return IMG_URL_ADDR;
    }

    public void setIMG_URL_ADDR(String IMG_URL_ADDR) {
        this.IMG_URL_ADDR = IMG_URL_ADDR;
    }

    public String getLINK_URL_ADDR() {
        return LINK_URL_ADDR;
    }

    public void setLINK_URL_ADDR(String LINK_URL_ADDR) {
        this.LINK_URL_ADDR = LINK_URL_ADDR;
    }

    public boolean getIS_LOAD_BITMAP() {
        return IS_LOAD_BITMAP;
    }

    public void setIS_LOAD_BITMAP(boolean flag) {
        this.IS_LOAD_BITMAP = flag;
    }
}
