package com.sbi.saidabank.define.datatype.main2;

import org.json.JSONObject;

public class Main2CoupleExpenceCount {
    private  String SETT_YM;	    //결제년월
    private  String SETT_TYCD;	    //결제유형코드
    private  String SETT_ITEM_NM;	//결제항목명
    private  String SETT_CCNT;  	//결제건수
    private  String FNTR_DVCD_NM;	//금융거래구분코드명(결제유형명)

    public Main2CoupleExpenceCount(JSONObject object){
        SETT_YM = object.optString("SETT_YM");
        SETT_TYCD = object.optString("SETT_TYCD");
        SETT_ITEM_NM = object.optString("SETT_ITEM_NM");
        SETT_CCNT = object.optString("SETT_CCNT");
        FNTR_DVCD_NM = object.optString("FNTR_DVCD_NM");
    }

    public String getSETT_YM() {
        return SETT_YM;
    }

    public void setSETT_YM(String SETT_YM) {
        this.SETT_YM = SETT_YM;
    }

    public String getSETT_TYCD() {
        return SETT_TYCD;
    }

    public void setSETT_TYCD(String SETT_TYCD) {
        this.SETT_TYCD = SETT_TYCD;
    }

    public String getSETT_ITEM_NM() {
        return SETT_ITEM_NM;
    }

    public void setSETT_ITEM_NM(String SETT_ITEM_NM) {
        this.SETT_ITEM_NM = SETT_ITEM_NM;
    }

    public String getSETT_CCNT() {
        return SETT_CCNT;
    }

    public void setSETT_CCNT(String SETT_CCNT) {
        this.SETT_CCNT = SETT_CCNT;
    }

    public String getFNTR_DVCD_NM() {
        return FNTR_DVCD_NM;
    }

    public void setFNTR_DVCD_NM(String FNTR_DVCD_NM) {
        this.FNTR_DVCD_NM = FNTR_DVCD_NM;
    }
}
