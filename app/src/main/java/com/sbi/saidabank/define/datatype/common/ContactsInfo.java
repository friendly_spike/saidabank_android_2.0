package com.sbi.saidabank.define.datatype.common;

import android.os.Parcel;
import android.os.Parcelable;

import com.sbi.saidabank.common.util.DataUtil;

import org.json.JSONObject;

/**
 * ContactsInfo : 주소록 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class ContactsInfo implements Parcelable {

	//연락처 기본정보
	private String TLNO;               // 전화번호
	private String FLNM;               // 성명
	private String OWBK_CUST_YN;       // 당행고객여부

	private String ACCO_IDNO;          // 계좌식별번호
	private String ACNO;               // 계좌번호
	private String CUST_NO;            // 고객번호

	//휴대폰 이체중 이제 정보 저장
	private String TRN_AMT;            // 이체금액
	private String TRNF_FEE;           // 이체수수료
	private String DEPO_BNKB_MRK_NM;   // 입금계좌표시내용 - 보낼메세지
//	private String TRNF_KEY_VAL;       // 이체키값
//	private String FAVO_ACCO_YN;       // 즐겨찾기계좌여부
//	private String TRN_SRNO;           // 거래일련번호
//	private String WTCH_ACNO;          // 출금계좌번호

	public ContactsInfo() {
	}

	public ContactsInfo(String FLNM) {
		this.FLNM = FLNM;
	}

	public ContactsInfo(String FLNM,String TLNO) {
		this.TLNO = TLNO;
		this.FLNM = FLNM;
	}

	public ContactsInfo(JSONObject object) {
		this.TLNO = object.optString("TLNO");
		this.ACCO_IDNO = object.optString("ACCO_IDNO");
		this.ACNO = object.optString("ACNO");
		this.CUST_NO = object.optString("CUST_NO");
		this.FLNM= object.optString("FLNM");
		this.OWBK_CUST_YN = object.optString("OWBK_CUST_YN");
	}

	//보내는사람 검색에서 사용.
	public String getTextForSearch(){
		return TLNO + " " + FLNM;
	}

	public ContactsInfo(Parcel src) {
		TLNO = src.readString();
		ACCO_IDNO = src.readString();
		ACNO = src.readString();
		CUST_NO = src.readString();
		FLNM = src.readString();
		OWBK_CUST_YN = src.readString();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(TLNO);
		dest.writeString(ACCO_IDNO);
		dest.writeString(ACNO);
		dest.writeString(CUST_NO);
		dest.writeString(FLNM);
		dest.writeString(OWBK_CUST_YN);
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		ContactsInfo contactsInfo = (ContactsInfo) o;

		if (DataUtil.isNotNull(FLNM) ? !FLNM.equals(contactsInfo.FLNM) : contactsInfo.FLNM != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		return FLNM != null ? FLNM.hashCode() : 0;
	}

	public static final Creator<ContactsInfo> CREATOR = new Creator<ContactsInfo>() {
		@Override
		public ContactsInfo createFromParcel(Parcel src) {
			return new ContactsInfo(src);
		}

		@Override
		public ContactsInfo[] newArray(int size) {
			return new ContactsInfo[size];
		}
	};

	public String getTLNO() {
		return TLNO;
	}

	public void setTLNO(String TLNO) {
		this.TLNO = TLNO;
	}

	public String getACCO_IDNO() {
		return ACCO_IDNO;
	}

	public void setACCO_IDNO(String ACCO_IDNO) {
		this.ACCO_IDNO = ACCO_IDNO;
	}

	public String getACNO() {
		return ACNO;
	}

	public void setACNO(String ACNO) {
		this.ACNO = ACNO;
	}

	public String getCUST_NO() {
		return CUST_NO;
	}

	public void setCUST_NO(String CUST_NO) {
		this.CUST_NO = CUST_NO;
	}

	public String getFLNM() {
		return FLNM;
	}

	public void setFLNM(String FLNM) {
		this.FLNM = FLNM;
	}

	public String getOWBK_CUST_YN() {
		return OWBK_CUST_YN;
	}

	public void setOWBK_CUST_YN(String OWBK_CUST_YN) {
		this.OWBK_CUST_YN = OWBK_CUST_YN;
	}

	public String getTRN_AMT() {
		return TRN_AMT;
	}

	public void setTRN_AMT(String TRN_AMT) {
		this.TRN_AMT = TRN_AMT;
	}

	public String getTRNF_FEE() {
		return TRNF_FEE;
	}

	public void setTRNF_FEE(String TRNF_FEE) {
		this.TRNF_FEE = TRNF_FEE;
	}

	public String getDEPO_BNKB_MRK_NM() {
		return DEPO_BNKB_MRK_NM;
	}

	public void setDEPO_BNKB_MRK_NM(String DEPO_BNKB_MRK_NM) {
		this.DEPO_BNKB_MRK_NM = DEPO_BNKB_MRK_NM;
	}

//	public String getTRNF_KEY_VAL() {
//		return TRNF_KEY_VAL;
//	}
//
//	public void setTRNF_KEY_VAL(String TRNF_KEY_VAL) {
//		this.TRNF_KEY_VAL = TRNF_KEY_VAL;
//	}
//
//	public String getFAVO_ACCO_YN() {
//		return FAVO_ACCO_YN;
//	}
//
//	public void setFAVO_ACCO_YN(String FAVO_ACCO_YN) {
//		this.FAVO_ACCO_YN = FAVO_ACCO_YN;
//	}
//
//	public String getTRN_SRNO() {
//		return TRN_SRNO;
//	}
//
//	public void setTRN_SRNO(String TRN_SRNO) {
//		this.TRN_SRNO = TRN_SRNO;
//	}
//
//	public String getWTCH_ACNO() {
//		return WTCH_ACNO;
//	}
//
//	public void setWTCH_ACNO(String WTCH_ACNO) {
//		this.WTCH_ACNO = WTCH_ACNO;
//	}

}
