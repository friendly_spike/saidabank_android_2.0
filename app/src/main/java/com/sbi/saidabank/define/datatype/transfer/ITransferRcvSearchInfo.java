package com.sbi.saidabank.define.datatype.transfer;

import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;

public class ITransferRcvSearchInfo {
    private int tabType;
    private Object objectInfo;


    public ITransferRcvSearchInfo(int tabType,Object info){
        this.tabType = tabType;
        objectInfo = info;
    }


    public int getTabType() {
        return tabType;
    }

    public void setTabType(int tabType) {
        this.tabType = tabType;
    }

    public Object getObjectInfo() {
        return objectInfo;
    }

    public void setObjectInfo(Object objectInfo) {
        this.objectInfo = objectInfo;
    }
}
