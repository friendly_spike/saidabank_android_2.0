package com.sbi.saidabank.define.datatype.main2;

import android.text.TextUtils;

import org.json.JSONObject;

import java.io.Serializable;

public class Main2AccountInfo implements Serializable {

    private String DSCT_CD;             // 구분코드
    private String ACNO;                // 계좌번호
    private String CMPH_BNKB_LOAN_YN;   // 종합통장대출여부 (마이너스통장여부(Y/N))
    private String PROD_CD;             // 상품코드
    private String ACCO_IDNO;           // 계좌식별번호
    private String TRNF_BANK_CD;        // 이체은행코드
    private String TRNF_ACNO;           // 이체계좌번호
    private String TRNF_DEPR_NM;        // 이체예금주명
    private String CNTR_AMT;            // 계약금액
    private String BLNC;                // 잔액
    private String MM_INSA;             // 월부금
    private String CNTR_DT;             // 계약일자
    private String EXPT_DT;             // 만기일자
    private String TOT_DCNT;            // 총일수
    private String REMN_DCNT;           // 잔여일수
    private String PI_RT;               // 납입율
    private String PROP_STEP_CD;        // 신청단계코드
    private String WTCH_POSB_AMT;       // 출금가능금액
    private String ACDT_DCL_CD;         // 사고신고여부
    private String LMIT_LMT_ACCO_YN;    // 한도제한계좌여부
    private String CHCR_PROP_PRGS_STCD; // 체크카드신청진행상태코드 (00:정상, 01:발급요청, 04:일시정지, 05:사고등록, 10:해지, 11:재발급해지, 12:갱신해지, 13:발급취소, 19:서손, 25:서손)
    private String ATMT_EXPT_EXTS_CD;   // 자동만기연장코드 (0:직접해지, 1:원금재예치(자동연장), 3:원래금재예치(자동연장), 4:자동만기해지)
    private String OVRD_DVCD;           // 연체구분코드 (0:정상, 1:연체, 2:기한이익상실)
    private String LOAN_EXTS_POSB_YN;   // 대출연장가능여부
    private String MM_PI_DD;            // 월납입일
    private String FAX_SEND_PRGS_CNTN;  // 팩스발송진행상태코드
    private String CANO;                // 카드번호
    private String CHCR_PROP_POSB_YN;   // 체크카드신청가능여부
    private String SHRN_ACCO_YN;        // 공유계좌여부
    private String ACDIV_ACCO_YN;       // 계좌분리여부
    private String SCRN_MRK_SQNC;        // 정렬순서
    private String IMMD_WTCH_ACCO_YN;    // 즉시출금계좌여부 / 충전설정여부
    private String SHRN_DMND_KEY_VAL;    // 공유요청키값
    private String SHRN_TRNF_DD;        // 공유이체일

    private String SUBJ_CD;             // 과목코드(이체은행코드)
    private String LOAN_PROP_NO;        // 대출신청번호(카드번호)
    private String ELPS_DCNT;           // 경과일수(잔여일수)
    private String PROD_NM;             // 상품명(이체예금주명)
    private String ACPT_CNTN;           // 접수내용(팩스발송진행내용)

    private String EXPT_YN;             //예금,적금 만기여부

    public Main2AccountInfo() {
        // TODO Auto-generated method stub
    }

    public Main2AccountInfo(JSONObject object) {
        DSCT_CD = object.optString("DSCT_CD");
        ACNO = object.optString("ACNO");
        CMPH_BNKB_LOAN_YN = object.optString("CMPH_BNKB_LOAN_YN");
        PROD_CD = object.optString("PROD_CD");
        ACCO_IDNO = object.optString("ACCO_IDNO");
        TRNF_BANK_CD = object.optString("TRNF_BANK_CD");
        TRNF_ACNO = object.optString("TRNF_ACNO");
        TRNF_DEPR_NM = object.optString("TRNF_DEPR_NM");
        CNTR_AMT = object.optString("CNTR_AMT");
        BLNC = object.optString("BLNC");
        MM_INSA = object.optString("MM_INSA");
        CNTR_DT = object.optString("CNTR_DT");
        EXPT_DT = object.optString("EXPT_DT");
        TOT_DCNT = object.optString("TOT_DCNT");
        REMN_DCNT = object.optString("REMN_DCNT");
        PI_RT = object.optString("PI_RT");
        PROP_STEP_CD = object.optString("PROP_STEP_CD");
        WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");
        ACDT_DCL_CD = object.optString("ACDT_DCL_CD");
        LMIT_LMT_ACCO_YN = object.optString("LMIT_LMT_ACCO_YN");
        CHCR_PROP_PRGS_STCD = object.optString("CHCR_PROP_PRGS_STCD");
        ATMT_EXPT_EXTS_CD = object.optString("ATMT_EXPT_EXTS_CD");
        OVRD_DVCD = object.optString("OVRD_DVCD", "");
        if (TextUtils.isEmpty(OVRD_DVCD)) OVRD_DVCD = "0";
        LOAN_EXTS_POSB_YN = object.optString("LOAN_EXTS_POSB_YN");
        MM_PI_DD = object.optString("MM_PI_DD");
        FAX_SEND_PRGS_CNTN = object.optString("FAX_SEND_PRGS_CNTN");
        CANO = object.optString("CANO");
        CHCR_PROP_POSB_YN = object.optString("CHCR_PROP_POSB_YN");
        SHRN_ACCO_YN = object.optString("SHRN_ACCO_YN");
        ACDIV_ACCO_YN = object.optString("ACDIV_ACCO_YN");
        SCRN_MRK_SQNC = object.optString("SCRN_MRK_SQNC");
        IMMD_WTCH_ACCO_YN = object.optString("IMMD_WTCH_ACCO_YN");
        SHRN_DMND_KEY_VAL = object.optString("SHRN_DMND_KEY_VAL");
        SHRN_TRNF_DD = object.optString("SHRN_TRNF_DD");
        SUBJ_CD = object.optString("SUBJ_CD");
        LOAN_PROP_NO = object.optString("LOAN_PROP_NO");
        ELPS_DCNT = object.optString("ELPS_DCNT");
        PROD_NM = object.optString("PROD_NM");
        ACPT_CNTN = object.optString("ACPT_CNTN");
        EXPT_YN = object.optString("EXPT_YN");
    }

    public String getDSCT_CD() {
        return DSCT_CD;
    }

    public void setDSCT_CD(String DSCT_CD) {
        this.DSCT_CD = DSCT_CD;
    }

    public String getACNO() {
        return ACNO;
    }

    public void setACNO(String ACNO) {
        this.ACNO = ACNO;
    }

    public String getCMPH_BNKB_LOAN_YN() {
        return CMPH_BNKB_LOAN_YN;
    }

    public void setCMPH_BNKB_LOAN_YN(String CMPH_BNKB_LOAN_YN) {
        this.CMPH_BNKB_LOAN_YN = CMPH_BNKB_LOAN_YN;
    }

    public String getPROD_CD() {
        return PROD_CD;
    }

    public void setPROD_CD(String PROD_CD) {
        this.PROD_CD = PROD_CD;
    }

    public String getACCO_IDNO() {
        return ACCO_IDNO;
    }

    public void setACCO_IDNO(String ACCO_IDNO) {
        this.ACCO_IDNO = ACCO_IDNO;
    }

    public String getTRNF_BANK_CD() {
        return TRNF_BANK_CD;
    }

    public void setTRNF_BANK_CD(String TRNF_BANK_CD) {
        this.TRNF_BANK_CD = TRNF_BANK_CD;
    }

    public String getTRNF_ACNO() {
        return TRNF_ACNO;
    }

    public void setTRNF_ACNO(String TRNF_ACNO) {
        this.TRNF_ACNO = TRNF_ACNO;
    }

    public String getTRNF_DEPR_NM() {
        return TRNF_DEPR_NM;
    }

    public void setTRNF_DEPR_NM(String TRNF_DEPR_NM) {
        this.TRNF_DEPR_NM = TRNF_DEPR_NM;
    }

    public String getCNTR_AMT() {
        return CNTR_AMT;
    }

    public void setCNTR_AMT(String CNTR_AMT) {
        this.CNTR_AMT = CNTR_AMT;
    }

    public String getBLNC() {
        return BLNC;
    }

    public void setBLNC(String BLNC) {
        this.BLNC = BLNC;
    }

    public String getMM_INSA() {
        return MM_INSA;
    }

    public void setMM_INSA(String MM_INSA) {
        this.MM_INSA = MM_INSA;
    }

    public String getCNTR_DT() {
        return CNTR_DT;
    }

    public void setCNTR_DT(String CNTR_DT) {
        this.CNTR_DT = CNTR_DT;
    }

    public String getEXPT_DT() {
        return EXPT_DT;
    }

    public void setEXPT_DT(String EXPT_DT) {
        this.EXPT_DT = EXPT_DT;
    }

    public String getTOT_DCNT() {
        return TOT_DCNT;
    }

    public void setTOT_DCNT(String TOT_DCNT) {
        this.TOT_DCNT = TOT_DCNT;
    }

    public String getREMN_DCNT() {
        return REMN_DCNT;
    }

    public void setREMN_DCNT(String REMN_DCNT) {
        this.REMN_DCNT = REMN_DCNT;
    }

    public String getPI_RT() {
        return PI_RT;
    }

    public void setPI_RT(String PI_RT) {
        this.PI_RT = PI_RT;
    }

    public String getPROP_STEP_CD() {
        return PROP_STEP_CD;
    }

    public void setPROP_STEP_CD(String PROP_STEP_CD) {
        this.PROP_STEP_CD = PROP_STEP_CD;
    }

    public String getWTCH_POSB_AMT() {
        return WTCH_POSB_AMT;
    }

    public void setWTCH_POSB_AMT(String WTCH_POSB_AMT) {
        this.WTCH_POSB_AMT = WTCH_POSB_AMT;
    }

    public String getACDT_DCL_CD() {
        return ACDT_DCL_CD;
    }

    public void setACDT_DCL_CD(String ACDT_DCL_CD) {
        this.ACDT_DCL_CD = ACDT_DCL_CD;
    }

    public String getLMIT_LMT_ACCO_YN() {
        return LMIT_LMT_ACCO_YN;
    }

    public void setLMIT_LMT_ACCO_YN(String LMIT_LMT_ACCO_YN) {
        this.LMIT_LMT_ACCO_YN = LMIT_LMT_ACCO_YN;
    }

    public String getCHCR_PROP_PRGS_STCD() {
        return CHCR_PROP_PRGS_STCD;
    }

    public void setCHCR_PROP_PRGS_STCD(String CHCR_PROP_PRGS_STCD) {
        this.CHCR_PROP_PRGS_STCD = CHCR_PROP_PRGS_STCD;
    }

    public String getATMT_EXPT_EXTS_CD() {
        return ATMT_EXPT_EXTS_CD;
    }

    public void setATMT_EXPT_EXTS_CD(String ATMT_EXPT_EXTS_CD) {
        this.ATMT_EXPT_EXTS_CD = ATMT_EXPT_EXTS_CD;
    }

    public String getOVRD_DVCD() {
        return OVRD_DVCD;
    }

    public void setOVRD_DVCD(String OVRD_DVCD) {
        this.OVRD_DVCD = OVRD_DVCD;
    }

    public String getLOAN_EXTS_POSB_YN() {
        return LOAN_EXTS_POSB_YN;
    }

    public void setLOAN_EXTS_POSB_YN(String LOAN_EXTS_POSB_YN) {
        this.LOAN_EXTS_POSB_YN = LOAN_EXTS_POSB_YN;
    }

    public String getMM_PI_DD() {
        return MM_PI_DD;
    }

    public void setMM_PI_DD(String MM_PI_DD) {
        this.MM_PI_DD = MM_PI_DD;
    }

    public String getFAX_SEND_PRGS_CNTN() {
        return FAX_SEND_PRGS_CNTN;
    }

    public void setFAX_SEND_PRGS_CNTN(String FAX_SEND_PRGS_CNTN) {
        this.FAX_SEND_PRGS_CNTN = FAX_SEND_PRGS_CNTN;
    }

    public String getCANO() {
        return CANO;
    }

    public void setCANO(String CANO) {
        this.CANO = CANO;
    }

    public String getCHCR_PROP_POSB_YN() {
        return CHCR_PROP_POSB_YN;
    }

    public void setCHCR_PROP_POSB_YN(String CHCR_PROP_POSB_YN) {
        this.CHCR_PROP_POSB_YN = CHCR_PROP_POSB_YN;
    }

    public String getSHRN_ACCO_YN() {
        return SHRN_ACCO_YN;
    }

    public void setSHRN_ACCO_YN(String SHRN_ACCO_YN) {
        this.SHRN_ACCO_YN = SHRN_ACCO_YN;
    }

    public String getACDIV_ACCO_YN() {
        return ACDIV_ACCO_YN;
    }

    public void setACDIV_ACCO_YN(String ACDIV_ACCO_YN) {
        this.ACDIV_ACCO_YN = ACDIV_ACCO_YN;
    }

    public String getSCRN_MRK_SQNC() {
        return SCRN_MRK_SQNC;
    }

    public void setSCRN_MRK_SQNC(String SCRN_MRK_SQNC) {
        this.SCRN_MRK_SQNC = SCRN_MRK_SQNC;
    }

    public String getIMMD_WTCH_ACCO_YN() {
        return IMMD_WTCH_ACCO_YN;
    }

    public void setIMMD_WTCH_ACCO_YN(String IMMD_WTCH_ACCO_YN) {
        this.IMMD_WTCH_ACCO_YN = IMMD_WTCH_ACCO_YN;
    }

    public String getSHRN_DMND_KEY_VAL() {
        return SHRN_DMND_KEY_VAL;
    }

    public void setSHRN_DMND_KEY_VAL(String SHRN_DMND_KEY_VAL) {
        this.SHRN_DMND_KEY_VAL = SHRN_DMND_KEY_VAL;
    }

    public String getSHRN_TRNF_DD() {
        return SHRN_TRNF_DD;
    }

    public void setSHRN_TRNF_DD(String SHRN_TRNF_DD) {
        this.SHRN_TRNF_DD = SHRN_TRNF_DD;
    }

    public String getSUBJ_CD() {
        return SUBJ_CD;
    }

    public void setSUBJ_CD(String SUBJ_CD) {
        this.SUBJ_CD = SUBJ_CD;
    }

    public String getLOAN_PROP_NO() {
        return LOAN_PROP_NO;
    }

    public void setLOAN_PROP_NO(String LOAN_PROP_NO) {
        this.LOAN_PROP_NO = LOAN_PROP_NO;
    }

    public String getELPS_DCNT() {
        return ELPS_DCNT;
    }

    public void setELPS_DCNT(String ELPS_DCNT) {
        this.ELPS_DCNT = ELPS_DCNT;
    }

    public String getPROD_NM() {
        return PROD_NM;
    }

    public void setPROD_NM(String PROD_NM) {
        this.PROD_NM = PROD_NM;
    }

    public String getACPT_CNTN() {
        return ACPT_CNTN;
    }

    public void setACPT_CNTN(String ACPT_CNTN) {
        this.ACPT_CNTN = ACPT_CNTN;
    }

    public String getEXPT_YN() {
        return EXPT_YN;
    }

    public void setEXPT_YN(String EXPT_YN) {
        this.EXPT_YN = EXPT_YN;
    }
}
