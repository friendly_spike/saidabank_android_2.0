package com.sbi.saidabank.define.datatype.transfer;

import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * 급여이체 관리
 */
public class ITransferSalayMgr {
    public static final int TRANSFER_STATE_NONE     = 0;
    public static final int TRANSFER_STATE_PLAY     = 1;
    public static final int TRANSFER_STATE_PAUSE    = 2;
    public static final int TRANSFER_STATE_STOP     = 3;
    public static final int TRANSFER_STATE_COMPLETE = 4;

    private static ITransferSalayMgr instance = null;

    //인스턴스 함수 정의 ===============================================
    public static ITransferSalayMgr getInstance() {
        if (instance == null) {
            synchronized (ITransferSalayMgr.class) {
                if (instance == null) {
                    instance = new ITransferSalayMgr();
                }
            }
        }
        return instance;
    }

    public ITransferSalayMgr(){
        mSalayArrayList = new ArrayList<ITransferSalayInfo>();
        mCurrentPlayIndex = 0;
        mTransferPlayState = TRANSFER_STATE_NONE;
    }

    public void clearSalaryInfo(){
        mSalayArrayList.clear();
        ACCO_BLNC = "0";
        WTCH_POSB_AMT = "0";
        WTCH_LMIT_REMN_AMT = "0";
        mCurrentPlayIndex = 0;
        mTransferPlayState = TRANSFER_STATE_NONE;
    }

    //변수정의
    private String ACCO_BLNC;           //계좌잔액
    private String WTCH_POSB_AMT;       //출금가능금액
    private String WTCH_LMIT_REMN_AMT;  //출금한도-잔여금액

    private int    mTransferPlayState;
    private int    mCurrentPlayIndex;
    private ArrayList<ITransferSalayInfo> mSalayArrayList;

    public String getACCO_BLNC() {
        return ACCO_BLNC;
    }

    public void setACCO_BLNC(String ACCO_BLNC) {
        this.ACCO_BLNC = ACCO_BLNC;
    }

    public String getWTCH_POSB_AMT() {
        return WTCH_POSB_AMT;
    }

    public void setWTCH_POSB_AMT(String WTCH_POSB_AMT) {
        this.WTCH_POSB_AMT = WTCH_POSB_AMT;
    }

    public String getWTCH_LMIT_REMN_AMT() {
        return WTCH_LMIT_REMN_AMT;
    }

    public void setWTCH_LMIT_REMN_AMT(String WTCH_LMIT_REMN_AMT) {
        this.WTCH_LMIT_REMN_AMT = WTCH_LMIT_REMN_AMT;
    }

    public int getTransferPlayState() {
        return mTransferPlayState;
    }

    public void setTransferPlayState(int playState) {
        this.mTransferPlayState = playState;
    }

    public int getCurrentPlayIndex() {
        return mCurrentPlayIndex;
    }

    public void setCurrentPlayIndex(int currentPlayIndex) {
        this.mCurrentPlayIndex = currentPlayIndex;
    }

    public int getSalayArrayListCount() {
        return mSalayArrayList.size();
    }

    public ArrayList<ITransferSalayInfo> getSalayArrayList() {
        return mSalayArrayList;
    }

    public void setSalayArrayList(ArrayList<ITransferSalayInfo> mSalayArrayList) {
        this.mSalayArrayList = mSalayArrayList;
    }

    /**
     * 급여이체 완료된 항목의 갯수를 넘겨준다.
     * @return
     */
    public int getCompleteCount(){
        int count = 0;
        for(int i=0;i<mSalayArrayList.size();i++){
            if(mSalayArrayList.get(i).getIS_TRANSFER_COMPLETE()){
                count++;
            }
        }

        return count;
    }

    //총 이체 금액 합계
    public boolean checkTotalTransferSumAmountLimit(){
        double sumAmount=0;
        for(int i=0;i<mSalayArrayList.size()-1;i++){
            String tranAmountStr = mSalayArrayList.get(i).getTRTM_AMT();
            if(TextUtils.isEmpty(tranAmountStr)) tranAmountStr = "0";
            sumAmount += Double.valueOf(tranAmountStr);
        }

        //총합계와 1일 이체 한도 조화 비교.
        String wtchLmitRemnAmt = WTCH_LMIT_REMN_AMT;
        if(TextUtils.isEmpty(wtchLmitRemnAmt)) wtchLmitRemnAmt = "0";
        if(sumAmount > Double.valueOf(wtchLmitRemnAmt)){
            return false;
        }

        return true;
    }

    //총 이체 금액 합계
    public double getTotalTransferAmount(){
        double sumAmount=0;
        for(int i=0;i<mSalayArrayList.size()-1;i++){
            String tranAmountStr = mSalayArrayList.get(i).getTRTM_AMT();
            if(TextUtils.isEmpty(tranAmountStr)) tranAmountStr = "0";
            sumAmount += Double.valueOf(tranAmountStr);
        }

        return sumAmount;
    }

    /**
     * 0번 아이템에서 금액을 입력할때
     * @param position
     * @param amount
     */
    public void setTransStartAmount(int position,String amount){
        if(position != 0) return;
        String transAmount = amount.replaceAll("[^0-9]", Const.EMPTY);
        mSalayArrayList.get(position).setTRTM_AMT(transAmount);
        mSalayArrayList.get(position).setREMAIN_AMT("");
        reCalTransAmount();
    }

    /**
     * 급여이체의 Row에서 1~부터는 입력값이 남길금액이다.
     * 그래서 텍스트를 입력할때마다 이체 금액을 계산해 두도록 하자.
     *
     * @param position
     * @param remainAmount
     */
    public void setRemainAmount(int position,String remainAmount){
        if(position == 0) return;//0번째는 시작계좌로 남은 금액을 입력하지 않는다.

        //이전아이템 이체금액
        String preTransAmount = mSalayArrayList.get(position-1).getTRTM_AMT();

        //현재아이템 남길금액
        String amount = remainAmount.replaceAll("[^0-9]", Const.EMPTY);
        mSalayArrayList.get(position).setREMAIN_AMT(amount);

        int transAmount = (int)(Double.parseDouble(preTransAmount) - Double.parseDouble(amount));

        mSalayArrayList.get(position).setTRTM_AMT(String.valueOf(transAmount));

        reCalTransAmount();
        Logs.e("setRemainAmount   #################");
        Logs.e("index : " + position);
        Logs.e("preTransAmount : " + preTransAmount);
        Logs.e("remainAmount : " + amount);
        Logs.e("transAmount : " + transAmount);
        Logs.e("setRemainAmount   #################");

    }

    public void reCalTransAmount(){
        //어느걸 삭제한지는 모르지만 마지막것을 삭제했다면 남길금액은 0으로 변경
        mSalayArrayList.get(mSalayArrayList.size()-1).setREMAIN_AMT("0");

        for(int i=1;i<mSalayArrayList.size();i++){
            String preTransAmount = mSalayArrayList.get(i-1).getTRTM_AMT();
            String remainAmount = mSalayArrayList.get(i).getREMAIN_AMT();

            int transAmount = (int)(Double.parseDouble(preTransAmount) - Double.parseDouble(remainAmount));
            mSalayArrayList.get(i).setTRTM_AMT(String.valueOf(transAmount));
        }
    }

    //급여이체 각 계좌정보
    public static class ITransferSalayInfo{
        private String SLRY_TRNF_SQNO;	    //급여이체순번
        private String RPRS_FNLT_CD;	    //대표금융기관코드
        private String DTLS_FNLT_CD;	    //세부금융기관코드
        private String ACNO;	            //계좌번호
        private String PROD_NM;	            //상품명
        private String ACCO_ALS;	        //계좌별칭
        private String TRTM_AMT;	        //이체금액
        private String REMAIN_AMT;          //남길금액


        private String MNRC_BANK_NM;	    //입금은행명
        private String CNTP_ACCO_DEPR_NM;	//상대계좌예금주명

        //각 계좌의 에러 여부및 에러 메세지를 가지고 있는다.
        private Boolean IS_TOTAL_LIMIT_ERROR;//이체하기 누를때 전체 이체 한도가 넘으면 true로 변경시켜주는데. 0번째 아이템에만 적용된다.
        private Boolean IS_NOW_ERROR;
        private String  MSG_TITLE_ERROR;
        private String  MSG_SUBTITLE_ERROR;

        //실이체 할때 화면 갱신을 위해 추가.
        private Boolean IS_TRANSFER_COMPLETE;
        private Boolean IS_TRANSFER_ERROR;

        private Boolean IS_NEED_FOCUS;

        public ITransferSalayInfo(int totalCnt,int position,JSONObject object){
            SLRY_TRNF_SQNO = object.optString("SLRY_TRNF_SQNO");	    //급여이체순번
            RPRS_FNLT_CD = object.optString("RPRS_FNLT_CD");	        //대표금융기관코드
            DTLS_FNLT_CD = object.optString("DTLS_FNLT_CD");	        //세부금융기관코드
            ACNO = object.optString("ACNO");	                        //계좌번호
            PROD_NM = object.optString("PROD_NM");	                //상품명
            ACCO_ALS = object.optString("ACCO_ALS");	                //계좌별칭

            if(position == 0){
                //이체금액
                TRTM_AMT = object.optString("TRTM_AMT");	            //처리금액(노출금액)
                if(TextUtils.isEmpty(TRTM_AMT)) TRTM_AMT = "0";
                REMAIN_AMT = "0";                                           //남길금액
            }else if(position+1 == totalCnt){
                //이체금액
                TRTM_AMT = object.optString("TRTM_AMT");	            //처리금액(노출금액)
                if(TextUtils.isEmpty(TRTM_AMT)) TRTM_AMT = "0";
                REMAIN_AMT = "0";                                           //남길금액
            }else{
                /*
                중요!!!
                웹 에서 내려주는 값과 방식이 다르다.
                0,과 마지막 아이템은 이체금액을 표시하고
                중간은 남길금액만 가지고 있도록 한다.
                Request가 끝나면 다시 이체금액을 계산하도록 한다.
                */
                //남길금액
                TRTM_AMT = "0";                              	            //처리금액(노출금액)
                REMAIN_AMT = object.optString("TRTM_AMT");           //남길금액 - 0번과 마지막번호가 아니면 이체금액이 아니라 남길금액이다.
            }


            MNRC_BANK_NM = object.optString("MNRC_BANK_NM");	        //입금은행명
            CNTP_ACCO_DEPR_NM = object.optString("CNTP_ACCO_DEPR_NM");//상대계좌예금주명

            IS_TOTAL_LIMIT_ERROR = false;
            IS_NOW_ERROR = false;
            MSG_TITLE_ERROR = "";
            MSG_SUBTITLE_ERROR = "";

            IS_TRANSFER_COMPLETE = false;
            IS_TRANSFER_ERROR = false;
            IS_NEED_FOCUS = false;
        }


        public String getSLRY_TRNF_SQNO() {
            return SLRY_TRNF_SQNO;
        }

        public void setSLRY_TRNF_SQNO(String SLRY_TRNF_SQNO) {
            this.SLRY_TRNF_SQNO = SLRY_TRNF_SQNO;
        }

        public String getRPRS_FNLT_CD() {
            return RPRS_FNLT_CD;
        }

        public void setRPRS_FNLT_CD(String RPRS_FNLT_CD) {
            this.RPRS_FNLT_CD = RPRS_FNLT_CD;
        }

        public String getDTLS_FNLT_CD() {
            return DTLS_FNLT_CD;
        }

        public void setDTLS_FNLT_CD(String DTLS_FNLT_CD) {
            this.DTLS_FNLT_CD = DTLS_FNLT_CD;
        }

        public String getACNO() {
            return ACNO;
        }

        public void setACNO(String ACNO) {
            this.ACNO = ACNO;
        }

        public String getPROD_NM() {
            return PROD_NM;
        }

        public void setPROD_NM(String PROD_NM) {
            this.PROD_NM = PROD_NM;
        }

        public String getACCO_ALS() {
            return ACCO_ALS;
        }

        public void setACCO_ALS(String ACCO_ALS) {
            this.ACCO_ALS = ACCO_ALS;
        }

        //이체금액
        public String getTRTM_AMT() {
            return TRTM_AMT;
        }

        public void setTRTM_AMT(String TRTM_AMT) {
            this.TRTM_AMT = TRTM_AMT;
        }

        //남길금액
        public String getREMAIN_AMT() {
            return REMAIN_AMT;
        }

        public void setREMAIN_AMT(String REMAIN_AMT) {
            this.REMAIN_AMT = REMAIN_AMT;
        }

        public String getMNRC_BANK_NM() {
            return MNRC_BANK_NM;
        }

        public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
            this.MNRC_BANK_NM = MNRC_BANK_NM;
        }

        public String getCNTP_ACCO_DEPR_NM() {
            return CNTP_ACCO_DEPR_NM;
        }

        public void setCNTP_ACCO_DEPR_NM(String CNTP_ACCO_DEPR_NM) {
            this.CNTP_ACCO_DEPR_NM = CNTP_ACCO_DEPR_NM;
        }

        //이체하기 누를때 전체 이체 한도가 넘으면 true로 변경시켜주는데. 0번째 아이템에만 적용된다.
        public Boolean getIS_TOTAL_LIMIT_ERROR() {
            return IS_TOTAL_LIMIT_ERROR;
        }

        public void setIS_TOTAL_LIMIT_ERROR(Boolean IS_TOTAL_LIMIT_ERROR) {
            this.IS_TOTAL_LIMIT_ERROR = IS_TOTAL_LIMIT_ERROR;
        }

        public Boolean getIS_NOW_ERROR() {
            return IS_NOW_ERROR;
        }

        public void setIS_NOW_ERROR(Boolean IS_NOW_ERROR) {
            this.IS_NOW_ERROR = IS_NOW_ERROR;
        }

        public String getMSG_TITLE_ERROR() {
            return MSG_TITLE_ERROR;
        }

        public void setMSG_TITLE_ERROR(String MSG_TITLE_ERROR) {
            this.MSG_TITLE_ERROR = MSG_TITLE_ERROR;
        }

        public String getMSG_SUBTITLE_ERROR() {
            return MSG_SUBTITLE_ERROR;
        }

        public void setMSG_SUBTITLE_ERROR(String MSG_ERROR) {
            this.MSG_SUBTITLE_ERROR = MSG_ERROR;
        }


        public Boolean getIS_TRANSFER_COMPLETE() {
            return IS_TRANSFER_COMPLETE;
        }

        public void setIS_TRANSFER_COMPLETE(Boolean IS_TRANSFER_COMPLETE) {
            this.IS_TRANSFER_COMPLETE = IS_TRANSFER_COMPLETE;
        }

        public Boolean getIS_TRANSFER_ERROR() {
            return IS_TRANSFER_ERROR;
        }

        public void setIS_TRANSFER_ERROR(Boolean IS_TRANSFER_ERROR) {
            this.IS_TRANSFER_ERROR = IS_TRANSFER_ERROR;
        }

        public Boolean getIS_NEED_FOCUS() {
            return IS_NEED_FOCUS;
        }

        public void setIS_NEED_FOCUS(Boolean IS_NEED_FOCUS) {
            this.IS_NEED_FOCUS = IS_NEED_FOCUS;
        }
    }
}
