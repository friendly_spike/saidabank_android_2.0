package com.sbi.saidabank.define.datatype.common;

import java.io.Serializable;

/**
 * CheatUseAccountInfo : 사기이용계좌 명의인 등록고객 거래제한 아이템 VO
 */
public class CheatUseAccountInfo implements Serializable {

    private static CheatUseAccountInfo instance;
    private String FRD_ACCO_REG_YN ="";            // 사기계좌 등록여부
    private String FRD_ACCO_REG_FIN_INST_NM="";   // 사기계좌 등록 금융기관명

    public CheatUseAccountInfo() {

    }

    public static CheatUseAccountInfo getInstance() {
        if (instance == null) {
            synchronized (CheatUseAccountInfo.class) {
                if (instance == null) {
                    instance = new CheatUseAccountInfo();
                }
            }
        }
        return instance;
    }

    public static void clearInstance() {
        if (instance == null)
            return;
        instance = null;
    }

    public String getFRD_ACCO_REG_YN() {
        return FRD_ACCO_REG_YN;
    }

    public void setFRD_ACCO_REG_YN(String FRD_ACCO_REG_YN) {
        this.FRD_ACCO_REG_YN = FRD_ACCO_REG_YN;
    }

    public String getFRD_ACCO_REG_FIN_INST_NM() {
        return FRD_ACCO_REG_FIN_INST_NM;
    }

    public void setFRD_ACCO_REG_FIN_INST_NM(String FRD_ACCO_REG_FIN_INST_NM) {
        this.FRD_ACCO_REG_FIN_INST_NM = FRD_ACCO_REG_FIN_INST_NM;
    }
}
