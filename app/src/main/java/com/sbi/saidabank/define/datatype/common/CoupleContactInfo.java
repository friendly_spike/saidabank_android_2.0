package com.sbi.saidabank.define.datatype.common;

import org.json.JSONObject;

import java.io.Serializable;

public class CoupleContactInfo {

    private String TLNO;            //전화번호
    private String MBR_NO;          //회원번호
    private String SHRN_SVC_ENTR_YN;//공유서비스 가입여부
    private String FLNM;            //이름
    private String OWBK_CUST_YN;    //사이다회원여부

    public CoupleContactInfo(JSONObject object,boolean isMember) {
        TLNO = object.optString("TLNO");
        MBR_NO = object.optString("MBR_NO");
        SHRN_SVC_ENTR_YN = object.optString("SHRN_SVC_ENTR_YN");
        FLNM = object.optString("FLNM");
        if(isMember)
            OWBK_CUST_YN = "Y";
        else
            OWBK_CUST_YN = "N";

    }

    public String getTLNO() {
        return TLNO;
    }

    public void setTLNO(String TLNO) {
        this.TLNO = TLNO;
    }

    public String getMBR_NO() {
        return MBR_NO;
    }

    public void setMBR_NO(String MBR_NO) {
        this.MBR_NO = MBR_NO;
    }

    public String getSHRN_SVC_ENTR_YN() {
        return SHRN_SVC_ENTR_YN;
    }

    public void setSHRN_SVC_ENTR_YN(String SHRN_SVC_ENTR_YN) {
        this.SHRN_SVC_ENTR_YN = SHRN_SVC_ENTR_YN;
    }

    public String getFLNM() {
        return FLNM;
    }

    public void setFLNM(String FLNM) {
        this.FLNM = FLNM;
    }

    public String getOWBK_CUST_YN() {
        return OWBK_CUST_YN;
    }

    public void setOWBK_CUST_YN(String OWBK_CUST_YN) {
        this.OWBK_CUST_YN = OWBK_CUST_YN;
    }
}
