package com.sbi.saidabank.define;

import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

/**
 * Const : 공통으로 사용하는 상수를 정의
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class Const {
    //갤럭시 폴드때문에 레이아웃의 최대값을 정해준다.
    public static final int MAX_WIDTH_DIP   = 420;



    public static final String BANK_FULL_NAME     = "사이다뱅크(SBI)";
    public static final String BANK_ALS_NAME      = "사이다";

    /** 공통 */
    public static final String HTTP     = "http://";
    public static final String HTTPS    = "https://";
    public static final String MARKET   = "market://";
    public static final String UTF_8    = "UTF-8";
    public static final String EUC_KR   = "EUC-KR";
    public static final String SHA_256  = "SHA-256";
    public static final String GET      = "GET";
    public static final String POST     = "POST";
    public static final String DOT      = ".";
    public static final String COMMA    = ",";
    public static final String EMPTY    = "";
    public static final String SLASH    = "/";
    public static final String UNDER    = "_";
    public static final String DASH     = "-";
    public static final String EQUAL    = "=";
    public static final String AT       = "@";
    public static final String AND      = "&";
    public static final String ZERO     = "0";
    public static final String ELLIPSIS = "∙∙∙";


    /** 메인 웹뷰 인텐트 */
    public static final String INTENT_MAINWEB_URL              = "url";

    /** 메인 인텐트 */
    public static final String INTENT_MAIN_TRANSFER_MY_ACCOUNT    = "intent_main_transfer_my_account";

    /** 보안키패드 인텐트 정의 */
    public static final String INTENT_TRANSKEY_PADTYPE            = "keypad_type";
    public static final String INTENT_TRANSKEY_TEXTTYPE           = "text_type";
    public static final String INTENT_TRANSKEY_TITLE              = "title";
    public static final String INTENT_TRANSKEY_LABEL1             = "label1";
    public static final String INTENT_TRANSKEY_LABEL2             = "label2";
    public static final String INTENT_TRANSKEY_HINT               = "hint";
    public static final String INTENT_TRANSKEY_MAXLENGTH          = "max_length";
    public static final String INTENT_TRANSKEY_MINLENGTH          = "min_length";
    public static final String INTENT_TRANSKEY_PRECAUTION         = "precaution";
    public static final String INTENT_TRANSKEY_INPUTID1           = "input_id1";                // 보안키패드 호출한 컨트롤 아이디1
    public static final String INTENT_TRANSKEY_INPUTID2           = "input_id2";                // 보안키패드 호출한 컨트롤 아이디2
    public static final String INTENT_TRANSKEY_PARAMETER          = "parameter";                // 임시저장을 위한 파라미터값
    public static final String INTENT_TRANSKEY_CAPTION            = "caption_show";             // 말풍선
    public static final String INTENT_TRANSKEY_DUMMY_DATA1        = "dummy_data1";              // 가짜비밀번호
    public static final String INTENT_TRANSKEY_DUMMY_DATA2        = "dummy_data2";              // 가짜비밀번호
    public static final String INTENT_TRANSKEY_PLAIN_DATA1        = "plain_data1";              // 비밀번호
    public static final String INTENT_TRANSKEY_PLAIN_DATA2        = "plain_data2";              // 비밀번호
    public static final String INTENT_TRANSKEY_CERT_CHIPER_DATA   = "cert_pass_chiper_data";    // 이니텍 인증서 비밀번호를 위한 암호화 데이타
    public static final String INTENT_TRANSKEY_SECURE_DATA1       = "secure_data1";             // 암호화데이타
    public static final String INTENT_TRANSKEY_SECURE_DATA2       = "secure_data2";             // 암호화데이타

    /** 인증서 관련 인텐트 */
    public static final String INTENT_CERT_PURPOSE         = "intent_cert_purpose";
    public static final String INTENT_CERT_IDDATA          = "intent_cert_iddata";//주민번호나 기타 저장에 필요한 데이타 담아둘때.
    public static final String INTENT_CERT_PATH            = "intent_cert_path";
    public static final String INTENT_CERT_PASSWORD        = "intent_cert_password";
    public static final String INTENT_CERT_INDEX           = "intent_cert_index";
    public static final String INTENT_CERT_AUTHCODE        = "intent_cert_authcode";
    public static final String INTENT_CERT_SERVICEDATA     = "intent_cert_servicedata";
    public static final String INTENT_CERT_BUNDLEDATA      = "intent_cert_bundledata";
    public static final String INTENT_CERT_USERNAME        = "intent_cert_username";
    public static final String INTENT_CERT_TYPE            = "intent_cert_certtype";
    public static final String INTENT_CERT_ISSUERNAME      = "intent_cert_issuername";
    public static final String INTENT_CERT_EXPIREDATE      = "intent_cert_expiredate";
    public static final String INTENT_CERT_DATA            = "intent_cert_data";
    public static final String INTENT_CERT_NO_SCRAPING     = "intent_cert_no_scraping";
    public static final String INTENT_CERT_ISSUER_DN       = "intent_cert_issuer_dn";
    public static final String INTENT_CERT_SUBJECT_DN      = "intent_cert_subject_dn";

    /** Was 요청 파리미터값 */
    public static final String REQUEST_WAS_YES             = "Y";
    public static final String REQUEST_WAS_NO              = "N";

    /** java script 기본 결과값 */
    public static final String BRIDGE_RESULT_KEY           = "result";
    public static final String BRIDGE_RESULT_TRUE          = "true";
    public static final String BRIDGE_RESULT_FALSE         = "false";
    public static final String BRIDGE_RESULT_YES           = "Y";
    public static final String BRIDGE_RESULT_NO            = "N";

    /** 코드 조회 결과값 공통 */
    public static final String REQUEST_COMMON_HEAD          = "COMMON_HEAD";
    public static final String REQUEST_COMMON_ERROR         = "ERROR";
    public static final String REQUEST_COMMON_MESSAGE       = "MESSAGE";
    public static final String REQUEST_COMMON_CODE          = "CODE";
    public static final String REQUEST_COMMON_RESP_CD       = "RESP_CD";
    public static final String REQUEST_COMMON_RESP_CNTN     = "RESP_CNTN";
    public static final String REQUEST_COMMON_TRUE          = "true";
    public static final String REQUEST_COMMON_SUCCESS_CODE  = "01";
    public static final String REQUEST_COMMON_FAIL_CODE     = "02";

    public static final String ESPIDER_STATUS               = "status";
    public static final String ESPIDER_STATUS_START         = "start";
    public static final String ESPIDER_STATUS_CANCEL        = "cancel";
    public static final String ESPIDER_STATUS_COMPLETE      = "complete";

    public static final String ESPIDER_TYPE_HEALTH_PAY_CODE             = "KRPP0002010020";
    public static final String ESPIDER_TYPE_HEALTH_QUALIFICATION_CODE   = "KRPP0002010010";
    public static final String ESPIDER_TYPE_NATIONAL_PAY_HISTORY_CODE   = "KRPP0001010060";
    public static final String ESPIDER_TYPE_REVENUE_TAX_CODE            = "KRNT0001010040";
    public static final String ESPIDER_TYPE_INCOME_QUALIFICATION_CODE   = "KRNT0001012000";

    /** Ssenstone Operation */
    public static final String SSENSTONE_REGISTER           = "Reg";
    public static final String SSENSTONE_AUTHENTICATE       = "Auth";
    public static final String SSENSTONE_DEREGISTER         = "Dereg";
    public static final String SSENSTONE_CHANGE_PIN         = "changePin";

    /** Safe card keypad 관련 인텐트 */
    public static final String SAFE_CARD_FIRST_NUM          = "safe_key_first_num";
    public static final String SAFE_CARD_SECOND_NUM         = "safe_key_second_num";
    public static final String SAFE_CARD_FIRST_KEY          = "safe_key_first_key";
    public static final String SAFE_CARD_SECOND_KEY         = "safe_key_second_key";
    public static final String SAFE_CARD_FIRST_SECURE_KEY   = "safe_key_first_secure_key";
    public static final String SAFE_CARD_SECOND_SECURE_KEY  = "safe_key_second_secure_key";
    public static final String SAFE_CARD_FIRST_LENGTH       = "safe_key_first_length";
    public static final String SAFE_CARD_SECOND_LENGTH      = "safe_key_v_length";

    /** 메인 관련 */
    public static final String ACCOUNT_TOP                  = "999";
    public static final String ACCOUNT_NO                   = "998";
    public static final String SLIDING_EXTEND               = "100";
    public static final String SLIDING_SMALL_EXTEND         = "101";
    public static final String SLIDING_ADD_LIMIT            = "102";
    public static final String SLIDING_DOWN_INTEREST        = "103";
    public static final String ACCOUNT_TAG                  = "201";
    public static final String ACCOUNT_NOMAL_REP            = "300";
    public static final String ACCOUNT_GO_ON_NOMAL          = "310";
    public static final String ACCOUNT_GO_ON_LOAN           = "320";
    public static final String ACCOUNT_GO_ON_MINUS          = "321";
    public static final String ACCOUNT_GO_ON_SMALL_MINUS    = "322";
    public static final String ACCOUNT_GO_ON_SAIDOL         = "323";
    public static final String ACCOUNT_LOAN_PRICE_CHECK     = "324";
    public static final String ACCOUNT_MINUS_PRICE_CHECK    = "325";
    public static final String ACCOUNT_GO_ON_PREM_LOAN      = "326";
    public static final String ACCOUNT_PREM_LOAN_PRICE_CHECK= "327";
    public static final String ACCOUNT_GO_ON_KEEP_EMER_FUND = "328";
    public static final String ACCOUNT_NOMAL                = "330";
    public static final String ACCOUNT_SAVINGS              = "400";
    public static final String ACCOUNT_INSTALL_SAVINGS      = "500";
    public static final String ACCOUNT_LOAN                 = "600";
    public static final String ACCOUNT_REPORT               = "997";
    public static final String ACCOUNT_ADS                  = "996";

    public static final int NICE_PREV                    = 1100;
    public static final int NICE_NEXT                    = 1101;
    public static final int VERIFY_ANOTHER_ACCOUNT       = 1102;

    /** Appsflyer 회원가입 */
    public static final String APPSFLYER_MEMBER             = "join membership_";

    /** 점검 시간 알림 전문 에러 코드 */
    public static final String ERRORCODE_EFWK0002           = "EFWK0002";

    /** 푸시 인텐트 */
    public static final String ACTION_PUSH_MESSAGE          = ".ACTION_PUSH_MESSAGE";
    public static final String ACTION_REFRESH_FCM_TOKEN     = ".ACTION_REFRESH_FCM_TOKEN";
    public static final String ACTION_PUSH_RECEIVE          = ".ACTION_PUSH_RECEIVE";
    public static final String INTENT_PUSH_DATA             = "push_data";
    public static final String INTENT_PUSH_REGID            = "push_reg_id";

    /** 공통 전달 인텐트 (로그인, SsenStone, OTP, OCR) */
    public static final String INTENT_COMMON_INFO           = "intent_common_info";
    public static final String INTENT_SERVICE_ID            = "intent_service_id";
    public static final String INTENT_BIZ_DV_CD             = "intent_biz_dv_cd";
    public static final String INTENT_TRN_CD                = "intent_trn_cd";

    //20200919 - 도데체 Intent 파라미터를 웹 인터페이스 파라미터와 같이 쓰는 경우가 있나!
    //고칠려고 했으나 어디서 버그가 날지 몰라 그냥 둔다. 이러지 말자..
    public static final String INTENT_PARAM                 = "param";

    /** OTP 인증 시 인텐트 */
    public static final String INTENT_OTP_AUTH_TOOLS        = "intent_otp_auth_tools";
    public static final String INTENT_OTP_AUTH_SIGN         = "intent_otp_auth_sign";
    public static final String INTENT_OTP_TA_VERSION        = "intent_otp_ta_version";
    public static final String INTENT_OTP_SERIAL_NUMBER     = "intent_otp_serial_number";

    /** 신분증 인증 인텐트 */
    public static final String INTENT_IDENTIFICATION_IMG    = "intent_id_img";

    /** 타행 계좌 인증 인텐트 */
    public static final String INTENT_VERIFY_ACCOUNT_SQNO         = "intent_verify_account_sqno";
    public static final String INTENT_VERIFY_ACCOUNT_NUMBER       = "intent_verify_account_number";
    public static final String INTENT_VERIFY_ACCOUNT_BANK_CODE    = "intent_verify_account_bank_code";
    public static final String INTENT_VERIFY_ACCOUNT_CUST_NAME    = "intent_verify_account_cust_name";
    public static final String INTENT_VERIFY_ACCOUNT_DVCD         = "intent_verify_account_dvcd";
    public static final String INTENT_VERIFY_ACCOUNT_PROPNO       = "intent_verify_account_propno";

    /** 계좌번호 이체 인텐트 */
    public static final String INTENT_LIST_TRANSFER_RECEIPT_INFO  = "intent_list_transfer_receipt_info";
    public static final String INTENT_LIST_TRANSFER_NAME          = "intent_transfer_name";
    public static final String INTENT_TRANSFER_BALANCE            = "intent_transfer_balance";
    public static final String INTENT_WITHDRAW_NAME               = "intent_withdraw_name";
    public static final String INTENT_WITHDRAWABLE_AMOUNT         = "intent_withdrawable_amount";
    public static final String INTENT_PROD_NAME                   = "intent_prod_name";
    public static final String INTENT_ACCO_ALS                    = "intent_acco_als";
    public static final String INTENT_BANK_NM                     = "intent_bank_name";
    public static final String INTENT_RCV_BANK_CODE               = "intent_rcv_bank_code";
    public static final String INTENT_RCV_ACCOUNT                 = "intent_rcv_account";
    public static final String INTENT_IS_ADD_TRANSFER             = "intent_is_add_transfer";//이체추가로 해서 받는분 선택 진입한 경우.
    public static final String INTENT_GRP_SERIAL_NO               = "intent_grp_serial_no";
    public static final String INTENT_GRP_REVS_YN                 = "intent_grp_revs_yn";

    /** 휴대전화번호 이체 인텐트 */
    public static final String INTENT_TRANSFER_PHONE_INFO         = "intent_transfer_phone_info";
    public static final String INTENT_TRANSFER_PHONE_RET          = "intent_transfer_phone_ret";
    public static final String CONTACTS_INFO_PATH                 = "contactsInfo.dat";
    public static final String INTENT_TRANSFER_PHONE_KAKAO_MSG    = "intent_transfer_phone_kakao_msg";

    /** 이체 실패 인텐트 */
    public static final String INTENT_TRANSFER_ENTRY_TYPE         = "intent_transfer_entry_type";  // 0 : 계좌이체, 1 : 휴대폰번호이체, 3 : 안심이체
    public static final String INTENT_TRANSFER_FAIL_TYPE          = "intent_transfer_fail_type";   // 0 : 일반실패, 1 : 잔액부족
    public static final String INTENT_TRANSFER_FAIL_RET           = "intent_transfer_fail_ret";

    /** 주소록 인텐트 */
    public static final String INTENT_CONTACTS_LIST               = "intent_contacts_list";
    public static final String INTENT_CONTACTS_PHONE_INFO         = "intent_contacts_phone_info";

    /** 로그아웃 인텐트 */
    public static final String INTENT_LOGOUT_TYPE                 = "intent_logout_type";

    /** Crop 관련 인텐트 */
    public static final String CROP_IMAGE_URI                     = "crop_image_uri";
    public static final String CROP_IMAGE_TYPE                    = "crop_image_type";

    public static final int CROP_TYPE_CIRCLE                    = 0;
    public static final int CROP_TYPE_RECT                      = 1;


    /** 주소록 검색 결과 인텐트*/
    public static final String INTENT_RESULT_ZIPCODE              = "intent_result_zipcode";//우편번호
    public static final String INTENT_RESULT_BLDB_CODE            = "intent_result_bldg_code";//빌딩코드
    public static final String INTENT_RESULT_ADDRESS              = "intent_result_address";//소재지주소
    public static final String INTENT_RESULT_ADDRESS_DETAIL       = "intent_result_address_detail";//상세주소

    /** ARS 인증 call 수신 여부 (불필요시 삭제 예정) */
    public static boolean isIncomingARS;

    public static final String OTP_PW_TITLE_JUMIN                 = "주민번호뒷자리";

    /** OCR Type */
    public static final String IDENTIFICATION_TYPE_ID             = "ID";
    public static final String IDENTIFICATION_TYPE_DOC            = "DOC";

    /** 신분증 타입 */
    public static final String ID_TYPE_JUMIM                      = "01";
    public static final String ID_TYPE_DRIVER                     = "02";

    /** 인증서 리스트 호출 목적 */
    public enum CertJobType {
        SCRAPING,
        MANAGER,
        EXPORT
    }

    /** 은행/증권사 단계 */
    public enum BANK_STOCK_MODE {
        BANK_MODE,
        STOCK_MODE
    }

    /** 계좌 개설 */
    public enum ADD_ACCOUNT_MODE {
        NOMAL_MODE,     // 입출금
        SAVINGS_MODE    // 예적금
    }

    /** 카드 개설 */
    public enum ADD_CARD_MODE {
        APPLY_MODE,      // 카드신청
        APPLYING_MODE,   // 카드신청중
        REGISTER_MODE,   // 카드등록
        INQUIRY_MODE     // 카드내역
    }

    /** 퍼미션 요청코드 */
    public static final int REQUEST_PERMISSION_READ_PHONE_STATE    = 1001;
    public static final int REQUEST_PERMISSION_CALL                = 1002;
    public static final int REQUEST_PERMISSION_CAMERA              = 1003;
    public static final int REQUEST_PERMISSION_EXTERNAL_STORAGE    = 1004;
    public static final int REQUEST_PERMISSION_SMS                 = 1005;
    public static final int REQUEST_PERMISSION_READ_CONTACTS       = 1006;
    public static final int REQUEST_PERMISSION_SYNC_CONTACTS       = 1007;

    /** 디버깅 서버 선택 */
    public static final int DEBUGING_SERVER_NONE             = -1;
    public static final int DEBUGING_SERVER_DEV              = 0;
    public static final int DEBUGING_SERVER_TEST             = 1;
    public static final int DEBUGING_SERVER_OPERATION        = 2;

    /** 메인 메뉴 Fragment Type */
    public static final int MainMenu_TYPE_Main               = 0;
    public static final int MainMenu_TYPE_Goods              = 1;
    public static final int MainMenu_TYPE_My                 = 2;
    public static final int MainMenu_TYPE_Menu               = 3;

    //신규메뉴 탭 인덱
    public static final int MAIN2_INDEX_HOME 	= 0;
    public static final int MAIN2_INDEX_GOODS   = 1;
    public static final int MAIN2_INDEX_MY 	    = 2;
    public static final int MAIN2_INDEX_NOTI    = 3;
    public static final int MAIN2_INDEX_MENU    = 4;
    public static final int MAIN2_INDEX_MAX     = 5;


    /** Espider */
    public static final int ESPIDER_TYPE_HEALTH_PAY            = 1;
    public static final int ESPIDER_TYPE_HEALTH_QUALIFICATION  = 2;
    public static final int ESPIDER_TYPE_NATIONAL_SUBSCRIBERS  = 3;
    public static final int ESPIDER_TYPE_NATIONAL_PAY          = 4;
    public static final int ESPIDER_TYPE_NATIONAL_PAY_HISTORY  = 5;
    public static final int ESPIDER_TYPE_REVENUE_REGISTER      = 6;
    public static final int ESPIDER_TYPE_REVENUE_TAX           = 7;
    public static final int ESPIDER_TYPE_REVENUE_TAX_INCOME    = 8;
    public static final int ESPIDER_TYPE_INCOME_QUALIFICATION  = 9;

    public static final int ESPIDER_TYPE_RETURN_SUCCESS        = 0;
    public static final int ESPIDER_TYPE_RETURN_FAIL           = 1;

    /** Ssenstone */
    public static final int SSENSTONE_MIN_PATTERN            = 6;
    public static final int SSENSTONE_PINCODE_LENGTH         = 6;
    public static final int SSENSTONE_PIN_DUPLICATE          = 3;
    public static final int SSENSTONE_AUTH_COUNT_FAIL        = 5;


    /** 입력창 한글 유효성 */
    public static final int INDEX_EDITBOX_NAME               = 0;
    public static final int INDEX_EDITBOX_BIRTH              = 1;
    public static final int INDEX_EDITBOX_EMAIL              = 2;
    public static final int STATE_EDITBOX_NORMAL             = 0;
    public static final int STATE_EDITBOX_FOCUSED            = 1;
    public static final int STATE_EDITBOX_ERROR              = 2;
    public static final int STATE_VALID_STR_NORMAL           = 0;
    public static final int STATE_VALID_STR_LENGTH_ERR       = 1;
    public static final int STATE_VALID_STR_CHAR_ERR         = 2;

    /** Picker type */
    public static final int PICKER_TYPE_DATE                 = 1;
    public static final int PICKER_TYPE_TIME                 = 2;
    public static final int PICKER_TYPE_DATE_WEB_YMD         = 3;
    public static final int PICKER_TYPE_DATE_WEB_YM          = 4;
    public static final int PICKER_TYPE_DATE_CYCLE_WEB_YM    = 5;
    public static final int PICKER_TYPE_CUSTOM_DATA_WEB      = 6;

    /** 프로필 편집 */
    public static final int REQUEST_PROFILE_ALBUM            = 2000;
    public static final int REQUEST_PROFILE_IMAGE            = 2001;
    public static final int REQUEST_PROFILE_CAMERA           = 2002;
    public static final int REQUEST_COUPLE_BG_ALBUM          = 2003;
    public static final int REQUEST_COUPLE_BG_CAMERA         = 2004;
    public static final int REQUEST_CROP_TO_IMAGE            = 2005;
    public static final int REQUEST_CROP_IMAGE               = 20000;

    /** OTP PW 등록, 변경, 인증 입력화면 진입 타잎 */
    public static final int OTP_PW_ENTRY_WEB                 = 1;
    public static final int OTP_PW_ENTRY_NATIVE              = 2;

    public static final int OTP_PW_ACCESS_REG_OTP               = 1;
    public static final int OTP_PW_ACCESS_REG_MOTP              = 2;
    public static final int OTP_PW_ACCESS_REG_PW                = 3;
    public static final int OTP_PW_ACCESS_CHNG_OTP              = 4;
    public static final int OTP_PW_ACCESS_CHNG_MOTP             = 5;
    public static final int OTP_PW_ACCESS_CHNG_PW               = 6;
    public static final int OTP_PW_ACCESS_AUTH_OTP              = 7;
    public static final int OTP_PW_ACCESS_AUTH_MOTP             = 8;
    public static final int OTP_PW_ACCESS_AUTH_MOTP_SAFE_DEAL   = 9;
    public static final int OTP_PW_ACCESS_AUTH_MOTP_SALARY      = 10;
    public static final int OTP_PW_ACCESS_AUTH_PW               = 11;
    public static final int OTP_PW_ACCESS_AUTH_LOSTPIN          = 12;

    public static final int OTP_PW_NUM_LENGTH                = 6;
    public static final int OTP_PW_JUMIN_LENGTH              = 7;
    public static final int OTP_PW_NUM_DUPLICATE             = 3;
    public static final int OTP_PW_OTP_AUTH_COUNT_FAIL       = 10;
    public static final int OTP_PW_PW_AUTH_COUNT_FAIL        = 5;


    //인증관련 리퀘스트 값 정의 - 이체등 여러군데서 사용. 값 중복되지 않도록 주의요망!!!!
    public static final int REQUEST_SSENSTONE_AUTH                  = 10000;
    public static final int REQUEST_OTP_AUTH                        = 10001;
    public static final int REQUEST_SSENSTONE_AUTH_REVOKE_SERVICE   = 10002;
    public static final int REQUEST_SSENSTONE_AUTH_REVOKE_CONNECT   = 10003;

    /** 이체시 보이스 피싱 노출 액수 */
    public static final Double SHOW_VOICE_PHISHING_AMOUNT    = 5000000.;

    /** 다건 이체 최대 갯수 */
    public static final int MAX_TRANSFER_AT_ONCE             = 5;

    /** 야간모드 시간 */
    public static final int NIGHTTIME_END                    = 5;
    public static final int NIGHTTIME_START                  = 21;

    /** 이체화면에서 계좌번호 입력 시, 입력창 활성화 최저값 */
    public static final int MAX_TRANSFER_ACOOUNT_LENGTH      = 6;
    public static final int MAX_TRANSFER_PHONE_LENGTH        = 11;

    /** 신분증 촬영 실패 타입 */
    public static final int ID_SHOT_FAIL_SHOT                = 0;
    public static final int ID_SHOT_FAIL_CHECK               = 1;

    /** 원격지원 실행 여부 */
    public static final int REMOTE_ACTIVITY                  = 90000;

    /** 로그아웃 타입 */
    public static final int LOGOUT_TYPE_USER                 = 0;
    public static final int LOGOUT_TYPE_TIMEOUT              = 1;
    public static final int LOGOUT_TYPE_FORCE                = 2;

    /** 안심이체 타입 */
    public static final int SAFEDEAL_TYPE_MOENY              = 1;
    public static final int SAFEDEAL_TYPE_PROPERTY           = 2;
    public static final int SAFEDEAL_TYPE_GOODS              = 3;

    //안심이체 사유 타입
    public static final String SAFEDEAL_REASON_DEPOSIT            = "01";//계약금
    public static final String SAFEDEAL_REASON_MIDDLE_PAYMENT     = "02";//중도금
    public static final String SAFEDEAL_REASON_BALANCE            = "03";//잔금
    public static final String SAFEDEAL_REASON_WRITE_DIRECT       = "99";//직접입력

    //public static final Double SAFE_DEAL_PROPERTY_NOCONTACT_AMOUNT   = 100000000.; //부동산거래 비대면 실행하기 위한 최소 가격 (1억원)
    public static final Double SAFE_DEAL_PROPERTY_MAX_AMOUNT         = 500000000.; //부동산거래 최대 입력 가능 금액         (5억원)
    // 안심이체 Intent
    public static final String ACTION_SAFE_DEAL_NOCONTACT_COMPLETE     = "com.sbi.saidabank.intent.ACTION_SAFE_DEAL_NOCONTACT_COMPLETE";

    /** 이체시 OTP 체크 액수 */
    public static final Double SHOW_MAX_OTP_AMOUNT    = 5000000.;

    //통합이체 자주/즐겨찾기 종류
    public static final int RECENTLY_KIND_ACCOUNT              = 0;
    public static final int RECENTLY_KIND_CONTACT              = 1;
    public static final int RECENTLY_KIND_GROUP                = 2;

    public static final String ENCRYPT_KEY = "rosisEncryptkey";
}
