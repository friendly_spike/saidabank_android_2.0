package com.sbi.saidabank.define.datatype.transfer;

import android.text.TextUtils;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.define.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * ITransferRemitteInfo : 수취인 정보 VO
 *
 */
public class ITransferRemitteeInfo implements Serializable {

    //받는분 선택에서 수취조회하면 넣는정보들
    private String GRP_SRNO;                //그룹일련번호
    private String CNTP_FIN_INST_CD;        //상대 금융기관코드
    private String DTLS_FNLT_CD;            //상대 세부금융기관코드
    private String CNTP_BANK_ACNO;          //상대 은행계좌번호
    private String CUST_NM;	                //고객명
    private String RECV_NM;	                //수취인명
    //private String SVC_DVCD;	            //서비스 구분코드 (0 : 보통(서비스 가입한게 없음) 1 : 즉시, 2 : 지연3시간이후)
    private String MNRC_BANK_NM;            //입금은행명
    private String MNRC_BANK_ALS;           //입금은행 단축명

    //이체전 금액체크 하면 받는 정보들.
    private String FEE;	                    //수수료
    private String SAME_AMT_TRNF_YN;	    //동일금액이체여부
    private String OWBK_SAME_CUST_ACCO_YN;	//당행동일고객계좌여부
    private String TRN_UNQ_NO;	            //거래고유번호
    private String EROR_MSG_CNTN;	        //오류메세지내용 - 다건에서 리스트에 출력함. 단건은 바로 메세지박스 출력.


    //#### 위까지가 수취조회하면 넣어주는 부분이다.
    private String TRN_AMT;                 //거래금액

    //상세입력화면에서 입력되는 내용
    private String DEPO_BNKB_MRK_NM;        //수신통장 표시명(입금계좌표시명)
    private String TRAN_BNKB_MRK_NM;        //송신통장 표시명(출금계좌표시명)
    private String TRNF_DVCD;               //이체 종류코드 (1 : 즉시이체, 2 : 지연이체, 3 : 예약이체, 5:입금지정)
    private String TRNF_DMND_DT;            //이체 요청일자
    private String TRNF_DMND_TM;            //이체 요청시각
    private String TRN_MEMO_CNTN;           //메모

    //그룹일때 받는 사람이름.
    private String MNRC_ACCO_DEPR_NM;

    //마지막 웹에 이체등록하면 입력되는 내용
    private String WTCH_FNLT_CD;            //출금은행코드
    private String WTCH_ACNO;               //출금계좌번호
    private String WTCH_ACCO_DEPR_NM;       //출금계좌예금주명
    private String EROR_CNTA_TLNO;          //오류연락전화번호
    private String CNTP_ACCO_DEPR_NM;       //상대계좌예금주명
    private String BANK_NM;                 //은행명
    private String UUID_NO;                 //UUID번호

    private String VERTUAL_ACCOUNT_YN;

    //다건이체에서 리스트 금액 입력에 포커스 주기 위해.
    private boolean IS_NEED_FOCUS;

    /**
     * 이체전 금액 확인할때 내려준 Json문자열을 담아두었다가
     * 확인 다이얼로그에서 예를 누르면 그대로 다시 웹에 올려주어 세션에 담아두도록 한다.
     */
    private String REQ_OBJ_DATA;              //리퀘스트된 Json문자열

    //다건이체에서는 수취인 정보를 직접 출력해주는데 추가정보 레이아웃 열렸는지와
    //보낼일시 툴팁이 열려있는지 확인해야 해서 넣어준다.
    private boolean IS_OPEN_MORE_INFO;
    private boolean IS_OPEN_TOOLTIP_SENDDATE;

    public ITransferRemitteeInfo(){}

    public ITransferRemitteeInfo(JSONObject object) throws JSONException {

        //SVC_DVCD = object.optString("SVC_DVCD");//        서비스구분코드
        CNTP_FIN_INST_CD = object.optString("CNTP_FIN_INST_CD");//        상대금융기관코드
        DTLS_FNLT_CD = object.optString("DTLS_FNLT_CD");//        세부금융기관코드
        CNTP_BANK_ACNO = object.optString("CNTP_BANK_ACNO");//        상대은행계좌번호
        TRN_AMT = object.optString("MNRC_AMT");//        입금금액
        DEPO_BNKB_MRK_NM = object.optString("MNRC_ACCO_MRK_CNTN");//        입금계좌표시내용
        TRAN_BNKB_MRK_NM = object.optString("WTCH_ACCO_MRK_CNTN");//        출금계좌표시내용
        MNRC_BANK_ALS = object.optString("MNRC_ACCO_ALS");//        입금계좌별칭(은행단축명)
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");//        입금은행명
        EROR_MSG_CNTN = object.optString("EROR_MSG_CNTN");//        오류메시지내용
        GRP_SRNO = object.optString("GRP_SRNO");//        그룹일련번호
        RECV_NM = object.optString("RECV_NM");//        수취인명
        TRN_MEMO_CNTN = object.optString("TRN_MEMO_CNTN");//   메모
        MNRC_ACCO_DEPR_NM = object.optString("MNRC_ACCO_DEPR_NM");//   메모
        VERTUAL_ACCOUNT_YN = "N";
    }


    //아래 함수 ====================================================

    public String getGRP_SRNO() {
        return GRP_SRNO;
    }

    public void setGRP_SRNO(String GRP_SRNO) {
        this.GRP_SRNO = GRP_SRNO;
    }

    public String getRECV_NM() {
        return RECV_NM;
    }

    public void setRECV_NM(String RECV_NM) {
        this.RECV_NM = RECV_NM;
    }

    public String getFEE() {
        return FEE;
    }

    public void setFEE(String FEE) {
        this.FEE = FEE;
    }

    public String getSAME_AMT_TRNF_YN() {
        return SAME_AMT_TRNF_YN;
    }

    public void setSAME_AMT_TRNF_YN(String SAME_AMT_TRNF_YN) {
        this.SAME_AMT_TRNF_YN = SAME_AMT_TRNF_YN;
    }

    public String getCUST_NM() {
        return CUST_NM;
    }

    public void setCUST_NM(String CUST_NM) {
        this.CUST_NM = CUST_NM;
    }

//    public String getSVC_DVCD() {
//        return SVC_DVCD;
//    }
//
//    public void setSVC_DVCD(String SVC_DVCD) {
//        this.SVC_DVCD = SVC_DVCD;
//    }

    public String getOWBK_SAME_CUST_ACCO_YN() {
        return OWBK_SAME_CUST_ACCO_YN;
    }

    public void setOWBK_SAME_CUST_ACCO_YN(String OWBK_SAME_CUST_ACCO_YN) {
        this.OWBK_SAME_CUST_ACCO_YN = OWBK_SAME_CUST_ACCO_YN;
    }

    public String getTRN_UNQ_NO() {
        return TRN_UNQ_NO;
    }

    public void setTRN_UNQ_NO(String TRN_UNQ_NO) {
        this.TRN_UNQ_NO = TRN_UNQ_NO;
    }

    public String getEROR_MSG_CNTN() {
        return EROR_MSG_CNTN;
    }

    public void setEROR_MSG_CNTN(String EROR_MSG_CNTN) {
        this.EROR_MSG_CNTN = EROR_MSG_CNTN;
    }

    public String getCNTP_FIN_INST_CD() {
        return CNTP_FIN_INST_CD;
    }

    public void setCNTP_FIN_INST_CD(String CNTP_FIN_INST_CD) {
        this.CNTP_FIN_INST_CD = CNTP_FIN_INST_CD;
    }

    public String getDTLS_FNLT_CD() {
        return DTLS_FNLT_CD;
    }

    public void setDTLS_FNLT_CD(String DTLS_FNLT_CD) {
        this.DTLS_FNLT_CD = DTLS_FNLT_CD;
    }

    public String getCNTP_BANK_ACNO() {
        return CNTP_BANK_ACNO;
    }

    public void setCNTP_BANK_ACNO(String CNTP_BANK_ACNO) {
        this.CNTP_BANK_ACNO = CNTP_BANK_ACNO;
    }

    public String getTRN_AMT() {
        return TRN_AMT;
    }

    public void setTRN_AMT(String TRN_AMT) {
        this.TRN_AMT = TRN_AMT;
    }

    public String getDEPO_BNKB_MRK_NM() {
        return DEPO_BNKB_MRK_NM;
    }

    public void setDEPO_BNKB_MRK_NM(String DEPO_BNKB_MRK_NM) {
        this.DEPO_BNKB_MRK_NM = DEPO_BNKB_MRK_NM;
    }

    public String getTRAN_BNKB_MRK_NM() {
        return TRAN_BNKB_MRK_NM;
    }

    public void setTRAN_BNKB_MRK_NM(String TRAN_BNKB_MRK_NM) {
        this.TRAN_BNKB_MRK_NM = TRAN_BNKB_MRK_NM;
    }

    public String getTRNF_DVCD() {
        return TRNF_DVCD;
    }

    public void setTRNF_DVCD(String TRNF_DVCD) {
        this.TRNF_DVCD = TRNF_DVCD;
    }

    public String getTRNF_DMND_DT() {
        return TRNF_DMND_DT;
    }

    public void setTRNF_DMND_DT(String TRNF_DMND_DT) {
        this.TRNF_DMND_DT = TRNF_DMND_DT;
    }

    public String getTRNF_DMND_TM() {
        return TRNF_DMND_TM;
    }

    public void setTRNF_DMND_TM(String TRNF_DMND_TM) {
        this.TRNF_DMND_TM = TRNF_DMND_TM;
    }

    public String getTRN_MEMO_CNTN() {
        return TRN_MEMO_CNTN;
    }

    public void setTRN_MEMO_CNTN(String TRN_MEMO_CNTN) {
        this.TRN_MEMO_CNTN = TRN_MEMO_CNTN;
    }

    public String getMNRC_ACCO_DEPR_NM() {
        return MNRC_ACCO_DEPR_NM;
    }

    public void setMNRC_ACCO_DEPR_NM(String MNRC_ACCO_DEPR_NM) {
        this.MNRC_ACCO_DEPR_NM = MNRC_ACCO_DEPR_NM;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    public String getMNRC_BANK_ALS() {
        return MNRC_BANK_ALS;
    }

    public void setMNRC_BANK_ALS(String MNRC_BANK_ALS) {
        this.MNRC_BANK_ALS = MNRC_BANK_ALS;
    }

    public String getREQ_OBJ_DATA() {
        return REQ_OBJ_DATA;
    }

    public void setREQ_OBJ_DATA(String REQ_OBJ_DATA) {
        this.REQ_OBJ_DATA = REQ_OBJ_DATA;
    }

    public boolean isIS_OPEN_MORE_INFO() {
        return IS_OPEN_MORE_INFO;
    }

    public void setIS_OPEN_MORE_INFO(boolean IS_OPEN_MORE_INFO) {
        this.IS_OPEN_MORE_INFO = IS_OPEN_MORE_INFO;
    }

    public boolean isIS_OPEN_TOOLTIP_SENDDATE() {
        return IS_OPEN_TOOLTIP_SENDDATE;
    }

    public void setIS_OPEN_TOOLTIP_SENDDATE(boolean IS_OPEN_TOOLTIP_SENDDATE) {
        this.IS_OPEN_TOOLTIP_SENDDATE = IS_OPEN_TOOLTIP_SENDDATE;
    }

    public String getVERTUAL_ACCOUNT_YN() {
        return VERTUAL_ACCOUNT_YN;
    }

    public void setVERTUAL_ACCOUNT_YN(String VERTUAL_ACCOUNT_YN) {
        this.VERTUAL_ACCOUNT_YN = VERTUAL_ACCOUNT_YN;
    }

    public boolean isIS_NEED_FOCUS() {
        return IS_NEED_FOCUS;
    }

    public void setIS_NEED_FOCUS(boolean IS_NEED_FOCUS) {
        this.IS_NEED_FOCUS = IS_NEED_FOCUS;
    }

    /**
     * 웹에 업로드 후 내려받는 결과로 최종값을 정리한다. 굳이 않해도 될거 같은데.. 그냥 받아두자.. 어디서 사용하지 모르니..
     * @param object
     * @throws JSONException
     */
    public void setUploadRemitteeInfo(JSONObject object) throws JSONException {
        if(!TextUtils.isEmpty(object.optString("UUID_NO")))
            UUID_NO = object.optString("UUID_NO");

        if(!TextUtils.isEmpty(object.optString("WTCH_FNLT_CD")))
            WTCH_FNLT_CD = object.optString("WTCH_FNLT_CD");

        if(!TextUtils.isEmpty(object.optString("WTCH_ACNO")))
            WTCH_ACNO = object.optString("WTCH_ACNO");

        if(!TextUtils.isEmpty(object.optString("WTCH_ACCO_DEPR_NM")))
            WTCH_ACCO_DEPR_NM = object.optString("WTCH_ACCO_DEPR_NM");

        if(!TextUtils.isEmpty(object.optString("EROR_CNTA_TLNO")))
            EROR_CNTA_TLNO = object.optString("EROR_CNTA_TLNO");

        if(!TextUtils.isEmpty(object.optString("CNTP_FIN_INST_CD")))
            CNTP_FIN_INST_CD = object.optString("CNTP_FIN_INST_CD");

        if(!TextUtils.isEmpty(object.optString("CNTP_BANK_ACNO")))
            CNTP_BANK_ACNO = object.optString("CNTP_BANK_ACNO");

        if(!TextUtils.isEmpty(object.optString("CNTP_ACCO_DEPR_NM")))
            CNTP_ACCO_DEPR_NM = object.optString("CNTP_ACCO_DEPR_NM");

        if(!TextUtils.isEmpty(object.optString("DEPO_BNKB_MRK_NM")))
            DEPO_BNKB_MRK_NM = object.optString("DEPO_BNKB_MRK_NM");

        if(!TextUtils.isEmpty(object.optString("TRAN_BNKB_MRK_NM")))
            TRAN_BNKB_MRK_NM = object.optString("TRAN_BNKB_MRK_NM");

        if(!TextUtils.isEmpty(object.optString("TRN_AMT")))
            TRN_AMT = object.optString("TRN_AMT");

        if(!TextUtils.isEmpty(object.optString("FEE")))
            FEE = object.optString("FEE");

        if(!TextUtils.isEmpty(object.optString("TRNF_DMND_DT")))
            TRNF_DMND_DT = object.optString("TRNF_DMND_DT");

        if(!TextUtils.isEmpty(object.optString("TRNF_DMND_TM")))
            TRNF_DMND_TM = object.optString("TRNF_DMND_TM");

        if(!TextUtils.isEmpty(object.optString("TRN_MEMO_CNTN")))
            TRN_MEMO_CNTN = object.optString("TRN_MEMO_CNTN");

        if(!TextUtils.isEmpty(object.optString("TRNF_DVCD")))
            TRNF_DVCD = object.optString("TRNF_DVCD");

        if(!TextUtils.isEmpty(object.optString("MNRC_BANK_NM")))
            MNRC_BANK_NM = object.optString("MNRC_BANK_NM");

        if(!TextUtils.isEmpty(object.optString("BANK_NM")))
            BANK_NM = object.optString("BANK_NM");

        if(!TextUtils.isEmpty(object.optString("GRP_SRNO")))
            GRP_SRNO = object.optString("GRP_SRNO");

//        if(!TextUtils.isEmpty(object.optString("SVC_DVCD")))
//            SVC_DVCD = object.optString("SVC_DVCD");

        if(!TextUtils.isEmpty(object.optString("TRN_UNQ_NO")))
            TRN_UNQ_NO = object.optString("TRN_UNQ_NO");
    }
}
