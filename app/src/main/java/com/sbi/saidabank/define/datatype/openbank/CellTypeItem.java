package com.sbi.saidabank.define.datatype.openbank;

public class CellTypeItem extends CellType {



    public CellTypeItem(int type, int groupPosition) {
        super(type, groupPosition,-1);
    }

    public CellTypeItem(int type, int groupPosition,int accountPos) {
        super(type, groupPosition,accountPos);
    }

}
