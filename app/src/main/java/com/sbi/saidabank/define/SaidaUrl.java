package com.sbi.saidabank.define;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.common.util.DataUtil;

/**
 * SaidaUrl : 서비스 URL을 정의
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class SaidaUrl {

    /** 개발계 */
    public static final String DEV_SERVER = Const.HTTPS + "devapp.saidabank.co.kr";               // 개발 서버 (http://192.30.1.97)
    public static final String DEV_SERVER_M = Const.HTTPS + "devm.saidabank.co.kr";               // 개발 서버
    private static final String DEV_AUTH_SERVER = Const.HTTP + "121.254.187.167:80";              // 개발 인증서버
    private static final String DEV_CHATBOT_SERVER = Const.HTTP + "dev.chat2.sbisb.co.kr:8080";   // 개발 챗봇서버
    private static final String DEV_PUSH_SERVER = "10.100.1.211";                                 // 개발 푸쉬서버

    /** 테스트계 */
    public static final String TEST_SERVER = Const.HTTPS + "testapp.saidabank.co.kr";             // 테스트 서버 (http://192.30.1.97) -> (http://121.254.187.187)
    public static final String TEST_SERVER_M = Const.HTTPS + "testm.saidabank.co.kr";             // 테스트 서버
    private static final String TEST_AUTH_SERVER = Const.HTTPS + "auth.sbisb.co.kr";              // 테스트 인증서버
    private static final String TEST_PUSH_SERVER = "121.254.187.172";                             // 테스트 푸쉬서버

    /** 운영계 */
    public static final String OPER_SERVER = Const.HTTPS + "app.saidabank.co.kr";                 // 운영 서버 (http://192.30.1.97)
    private static final String OPER_SERVER_M = Const.HTTPS + "m.saidabank.co.kr";                // 운영 서버
    private static final String OPER_AUTH_SERVER = Const.HTTPS + "auth.sbisb.co.kr";              // 운영 인증서버
    private static final String OPER_CHATBOT_SERVER = Const.HTTPS + "chat2.sbisb.co.kr";          // 운영 챗봇서버
    private static final String OPER_PUSH_SERVER = "push.saidabank.co.kr";                        // 운영 푸쉬서버

    //기본값을 푸시 때문에 TEST한다.
    public static int serverIndex = Const.DEBUGING_SERVER_DEV;

    public static String getBaseWebUrl() {
        if (BuildConfig.DEBUG) {
            switch (serverIndex) {
                case Const.DEBUGING_SERVER_DEV:
                    return DEV_SERVER;
                case Const.DEBUGING_SERVER_TEST:
                    return TEST_SERVER;
                case Const.DEBUGING_SERVER_OPERATION:
                    return OPER_SERVER;
                default:
                    return TEST_SERVER;
            }
        } else {
            serverIndex = Const.DEBUGING_SERVER_OPERATION;
            return OPER_SERVER;
        }
    }

    public static String getBaseMWebUrl() {
        if (BuildConfig.DEBUG) {
            switch (serverIndex) {
                case Const.DEBUGING_SERVER_DEV:
                    return DEV_SERVER_M;
                case Const.DEBUGING_SERVER_TEST:
                    return TEST_SERVER_M;
                case Const.DEBUGING_SERVER_OPERATION:
                default:
                    return TEST_SERVER_M;
            }
        } else {
            return OPER_SERVER_M;
        }
    }

    public enum ReqUrl {

        // 인증
        URL_CERT_EXPORT_IMPORT_URL(
                DEV_SERVER,
                TEST_SERVER,
                OPER_SERVER,
                "/sw/initech/GetCertificate_v12.jsp"
        ),

        // FDS서 수집서버 주소
        URL_FDS(
                DEV_SERVER,
                TEST_SERVER,
                OPER_SERVER,
                "/sp"
        ),

        // 패턴,지문,얼굴 서버주소
        URL_SSENSTONE(
                DEV_SERVER,
                TEST_SERVER,
                OPER_SERVER,
                "/sw"),
/*
//스톤패스(쎈스톤)인증서버는 하나이다. 하나의 서버에 개발, 테스트, 운영이 붙어 있다.
//그래서 개발 서버에 회원가입되어 있다면 다른 서버로 접속시 다시 회원가입을 해야 한다. 패턴,지문이 다르다고 에러가 발생하기 때문이다.

        URL_SSENSTONE_AUTH(
                DEV_AUTH_SERVER,
                DEV_AUTH_SERVER,
                DEV_AUTH_SERVER,
                "/sp"
        ),
*/
        // Push
        URL_FLKPUSH(
                //20201027 - 푸시는 개발서버에서는 테스트 불가! 테스트 서버에서만 푸시 받을수 있기에 기본값을 TEST로 하도록 한다.
                TEST_PUSH_SERVER,//DEV_PUSH_SERVER,
                TEST_PUSH_SERVER,
                OPER_PUSH_SERVER,
                ""),

        // 챗봇
        URL_CHATBOT(
                DEV_CHATBOT_SERVER,
                DEV_CHATBOT_SERVER,
                OPER_CHATBOT_SERVER,
                "/client-page/saida/index.html"
        );

        private String[] mSaidaUrl;

        ReqUrl(String dev, String test, String oper, String common) {
            if (DataUtil.isNull(mSaidaUrl))
                mSaidaUrl = new String[3];
            mSaidaUrl[0] = new StringBuilder().append(dev).append(common).toString();
            mSaidaUrl[1] = new StringBuilder().append(test).append(common).toString();
            mSaidaUrl[2] = new StringBuilder().append(oper).append(common).toString();
        }

        public String getReqUrl() {
            if (BuildConfig.DEBUG) {
                switch (serverIndex) {
                    case Const.DEBUGING_SERVER_DEV:
                        return mSaidaUrl[0];
                    case Const.DEBUGING_SERVER_TEST:
                        return mSaidaUrl[1];
                    case Const.DEBUGING_SERVER_OPERATION:
                        return mSaidaUrl[2];
                    default:
                        return mSaidaUrl[0];
                }
            } else {
                return mSaidaUrl[2];
            }
        }
    }
}
