package com.sbi.saidabank.define.datatype.main;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * SlidingItemInfo : 메인화면 슬라이딩 아이템 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class SlidingItemInfo implements Serializable {

    private String DSCT_CD;             // 구분코드
    private String ACNO;                // 계좌번호
    private String ACCO_IDNO;           // 신청번호
    private String TRNF_BANK_CD;        // 과목코드
    private String REMN_DCNT;           // 이어하기잔여일수
    private String PROP_STEP_CD;        // 신청단계코드
    private String FAX_SEND_PRGS_CNTN;  // 팩스발송진행내용
    private String CANO;                // 이어하기신청번호

    public SlidingItemInfo() {

    }

    public SlidingItemInfo(JSONObject object) {
        DSCT_CD = object.optString("DSCT_CD");
        ACNO = object.optString("ACNO");
        ACCO_IDNO = object.optString("ACCO_IDNO");
        TRNF_BANK_CD = object.optString("TRNF_BANK_CD");
        REMN_DCNT = object.optString("REMN_DCNT");
        PROP_STEP_CD = object.optString("PROP_STEP_CD");
        FAX_SEND_PRGS_CNTN = object.optString("FAX_SEND_PRGS_CNTN");
        CANO = object.optString("CANO");
    }

    public String getDSCT_CD() {
        return DSCT_CD;
    }

    public void setDSCT_CD(String DSCT_CD) {
        this.DSCT_CD = DSCT_CD;
    }

    public String getACNO() {
        return ACNO;
    }

    public void setACNO(String ACNO) {
        this.ACNO = ACNO;
    }

    public String getACCO_IDNO() {
        return ACCO_IDNO;
    }

    public void setACCO_IDNO(String ACCO_IDNO) {
        this.ACCO_IDNO = ACCO_IDNO;
    }

    public String getTRNF_BANK_CD() {
        return TRNF_BANK_CD;
    }

    public void setTRNF_BANK_CD(String TRNF_BANK_CD) {
        this.TRNF_BANK_CD = TRNF_BANK_CD;
    }

    public String getREMN_DCNT() {
        return REMN_DCNT;
    }

    public void setREMN_DCNT(String REMN_DCNT) {
        this.REMN_DCNT = REMN_DCNT;
    }

    public String getPROP_STEP_CD() {
        return PROP_STEP_CD;
    }

    public void setPROP_STEP_CD(String PROP_STEP_CD) {
        this.PROP_STEP_CD = PROP_STEP_CD;
    }

    public String getFAX_SEND_PRGS_CNTN() {
        return FAX_SEND_PRGS_CNTN;
    }

    public void setFAX_SEND_PRGS_CNTN(String FAX_SEND_PRGS_CNTN) {
        this.FAX_SEND_PRGS_CNTN = FAX_SEND_PRGS_CNTN;
    }

    public String getCANO() {
        return CANO;
    }

    public void setCANO(String CANO) {
        this.CANO = CANO;
    }
}
