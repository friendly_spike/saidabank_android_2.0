package com.sbi.saidabank.define;

/**
 * EntryPoint : DeepLink 시작포인트
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public enum EntryPoint {
    NONE,
    LOGIN_PATTERN,
    LOGIN_BIO,
    SERVICE_JOIN,
    DEVICE_CHANGE,
    AUTH_TOOL_MANAGE_PINCODE,
    AUTH_TOOL_MANAGE_PINCODE_RESET,
    AUTH_TOOL_MANAGE_PATTERN,
    AUTH_TOOL_MANAGE_BIO,
    WEB_CALL,
    TRANSFER,
    TRANSFER_SAFEDEAL,
    PATTERN_FORGET,
    PINCODE_FORGET
}
/**
 * 0.로그인(LOGIN)
 *    패턴인증(패턴분실은 아래로 분기),생체인증 ->메인
 *    생체가 변경되었을 경우 5번 생체 변경 프로세스(지문인증 -> 핀코드인증(핀코드분실은 아래 분기) -> 패턴인증)
 *
 * 1.서비스 가입(SERVICE_JOIN)
 *    기본정보입력(A),휴대폰인증,패턴등록,생체등록,핀코드등록,패턴인증(패턴분실은 아래로 분기)(생체인증) ->메인
 *
 * 2.기기변경(DEVICE_CHANGE)
 *   기본정보입력(A),휴대폰인증,(신분증촬영,타행계좌확인),패턴등록,핀코드등록,패턴인증(패턴분실은 아래로 분기) ->메인
 *
 * 3.인증센터 - 패턴변경(AUTH_TOOL_MANAGE)
 *   인증수단관리 -> 패턴인증(패턴분식은 아래로 분기) -> 패턴등록 -> 핀코드인증(핀코드분실은 아래로 분기)
 *
 * 4.인증센터 - 패턴 재등록(AUTH_TOOL_MANAGE)
 *    인증수단관리 -> 패턴인증 -> 아래 패턴분실 처리와 같다.
 *
 * 5. 인증센터 - 생체인증사용(AUTH_TOOL_MANAGE)
 *    인증수단관리 -> 지문인증 -> 핀코드인증 (핀코드분실은 아래로 분기)->인증수단관리
 *
 * 6.인증센터. - 핀코드변경(AUTH_TOOL_MANAGE)
 *   인증수단관리 -> 핀코드인증(핀코드분실은 아래로 분기) -> 핀코드등록 ->인증수단관리
 *
 * 7.인증센터 - 핀코드 재등록(AUTH_TOOL_NAMAGE)
 *   인증수단관리 -> 핀코드인증 -> 아래 핀코드 분실 처리와 같다.
 *
 * 8.웹호출(WEB_CALL)
 *    핀코드인증,
 *    지문인증,
 *    휴대폰본인인증,
 *    타행계좌인증
 *
 * 9.이체(TRANSFER)
 *    핀코드인증
 *    생체인증
 *
 * ========================분실 분기 처리===================================
 *
 * *. 패턴 인증화면 - 패턴 분실(PATTERN_FORGET)
 *   패턴인증(분실클릭) ->패턴분실안내->기본정보입력(생년월일)->휴대폰인증 -> 패턴등록 -> 핀코드 인증(핀코드분실은 아래로 분기) ->
 *   패턴인증
 *
 * *.핀코드 인증화면 - 핀코드 분실(PINCODE_FORGET)
 *   핀코드인증(분실클릭) ->핀코드분실안내->기본정보입력-> 휴대폰인증 -> (신분증확인,타행계좌확인) -> 핀코드등록 -> 핀코드인증
 * */