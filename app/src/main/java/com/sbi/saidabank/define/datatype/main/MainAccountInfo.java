package com.sbi.saidabank.define.datatype.main;

import com.sbi.saidabank.define.Const;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * AccountInfo : 계좌 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class MainAccountInfo implements Serializable {

    private String DSCT_CD;             // 구분코드
    private String ACNO;                // 계좌번호
    private String CMPH_BNKB_LOAN_YN;   // 종합통장대출여부 (마이너스통장여부(Y/N))
    private String PROD_CD;             // 상품코드
    private String ACCO_IDNO;           // 계좌식별번호
    private String TRNF_BANK_CD;        // 이체은행코드
    private String TRNF_ACNO;           // 이체계좌번호
    private String TRNF_DEPR_NM;        // 이체예금주명
    private String CNTR_AMT;            // 계약금액
    private String BLNC;                // 잔액
    private String MM_INSA;             // 월부금
    private String CNTR_DT;             // 계약일자
    private String EXPT_DT;             // 만기일자
    private String TOT_DCNT;            // 총일수
    private String REMN_DCNT;           // 잔여일수
    private String PI_RT;               // 납입율
    private String PROP_STEP_CD;        // 신청단계코드
    private String WTCH_POSB_AMT;       // 출금가능금액
    private String ACDT_DCL_CD;         // 사고신고여부
    private String LMIT_LMT_ACCO_YN;    // 한도제한계좌여부
    private String CHCR_PROP_PRGS_STCD; // 체크카드신청진행상태코드 (00:정상, 01:발급요청, 04:일시정지, 05:사고등록, 10:해지, 11:재발급해지, 12:갱신해지, 13:발급취소, 19:서손, 25:서손)
    private String ATMT_EXPT_EXTS_CD;   // 자동만기연장코드 (0:직접해지, 1:원금재예치(자동연장), 3:원래금재예치(자동연장), 4:자동만기해지)
    private String OVRD_DVCD;           // 연체구분코드 (0:정상, 1:연체, 2:기한이익상실)
    private String LOAN_EXTS_POSB_YN;   // 대출연장가능여부
    private String MM_PI_DD;            // 월납입일
    private String FAX_SEND_PRGS_CNTN;  // 팩스발송진행상태코드
    private String CANO;                // 카드번호
    private String CHCR_PROP_POSB_YN;   // 체크카드신청가능여부
    private String SHRN_ACCO_YN;	    // 공유계좌여부
    private String LNP_SQNC;	        // 정렬순서


    private ArrayList<SlidingItemInfo> listSlidingItem;     // 슬라이딩 항목
    private ArrayList<TagItemInfo> listTags;                // 최근 이체 내역
    private ArrayList<MainAdsItemInfo> listAds;             // 광고 배너 리스트
    private Const.ADD_ACCOUNT_MODE ADD_ACCOUNT_MODE;        // 계좌 발급 요청 타잎

    public MainAccountInfo() {

    }

    public MainAccountInfo(JSONObject object) {
        DSCT_CD = object.optString("DSCT_CD");
        ACNO = object.optString("ACNO");
        CMPH_BNKB_LOAN_YN = object.optString("CMPH_BNKB_LOAN_YN");
        PROD_CD = object.optString("PROD_CD");
        ACCO_IDNO = object.optString("ACCO_IDNO");
        TRNF_BANK_CD = object.optString("TRNF_BANK_CD");
        TRNF_ACNO = object.optString("TRNF_ACNO");
        TRNF_DEPR_NM = object.optString("TRNF_DEPR_NM");
        CNTR_AMT = object.optString("CNTR_AMT");
        BLNC = object.optString("BLNC");
        MM_INSA = object.optString("MM_INSA");
        CNTR_DT = object.optString("CNTR_DT");
        EXPT_DT = object.optString("EXPT_DT");
        TOT_DCNT = object.optString("TOT_DCNT");
        REMN_DCNT = object.optString("REMN_DCNT");
        PI_RT = object.optString("PI_RT");
        PROP_STEP_CD = object.optString("PROP_STEP_CD");
        WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");
        ACDT_DCL_CD = object.optString("ACDT_DCL_CD");
        LMIT_LMT_ACCO_YN = object.optString("LMIT_LMT_ACCO_YN");
        CHCR_PROP_PRGS_STCD = object.optString("CHCR_PROP_PRGS_STCD");
        ATMT_EXPT_EXTS_CD = object.optString("ATMT_EXPT_EXTS_CD");
        OVRD_DVCD = object.optString("OVRD_DVCD");
        LOAN_EXTS_POSB_YN = object.optString("LOAN_EXTS_POSB_YN");
        MM_PI_DD = object.optString("MM_PI_DD");
        FAX_SEND_PRGS_CNTN = object.optString("FAX_SEND_PRGS_CNTN");
        CANO = object.optString("CANO");
        CHCR_PROP_POSB_YN = object.optString("CHCR_PROP_POSB_YN");
        SHRN_ACCO_YN = object.optString("SHRN_ACCO_YN");
        LNP_SQNC = object.optString("LNP_SQNC");
    }

    public String getDSCT_CD() {
        return DSCT_CD;
    }

    public void setDSCT_CD(String DSCT_CD) {
        this.DSCT_CD = DSCT_CD;
    }

    public String getACNO() {
        return ACNO;
    }

    public void setACNO(String ACNO) {
        this.ACNO = ACNO;
    }

    public String getCMPH_BNKB_LOAN_YN() {
        return CMPH_BNKB_LOAN_YN;
    }

    public void setCMPH_BNKB_LOAN_YN(String CMPH_BNKB_LOAN_YN) {
        this.CMPH_BNKB_LOAN_YN = CMPH_BNKB_LOAN_YN;
    }

    public String getPROD_CD() {
        return PROD_CD;
    }

    public void setPROD_CD(String PROD_CD) {
        this.PROD_CD = PROD_CD;
    }

    public String getACCO_IDNO() {
        return ACCO_IDNO;
    }

    public void setACCO_IDNO(String ACCO_IDNO) {
        this.ACCO_IDNO = ACCO_IDNO;
    }

    public String getTRNF_BANK_CD() {
        return TRNF_BANK_CD;
    }

    public void setTRNF_BANK_CD(String TRNF_BANK_CD) {
        this.TRNF_BANK_CD = TRNF_BANK_CD;
    }

    public String getTRNF_ACNO() {
        return TRNF_ACNO;
    }

    public void setTRNF_ACNO(String TRNF_ACNO) {
        this.TRNF_ACNO = TRNF_ACNO;
    }

    public String getTRNF_DEPR_NM() {
        return TRNF_DEPR_NM;
    }

    public void setTRNF_DEPR_NM(String TRNF_DEPR_NM) {
        this.TRNF_DEPR_NM = TRNF_DEPR_NM;
    }

    public String getCNTR_AMT() {
        return CNTR_AMT;
    }

    public void setCNTR_AMT(String CNTR_AMT) {
        this.CNTR_AMT = CNTR_AMT;
    }

    public String getBLNC() {
        return BLNC;
    }

    public void setBLNC(String BLNC) {
        this.BLNC = BLNC;
    }

    public String getMM_INSA() {
        return MM_INSA;
    }

    public void setMM_INSA(String MM_INSA) {
        this.MM_INSA = MM_INSA;
    }

    public String getCNTR_DT() {
        return CNTR_DT;
    }

    public void setCNTR_DT(String CNTR_DT) {
        this.CNTR_DT = CNTR_DT;
    }

    public String getEXPT_DT() {
        return EXPT_DT;
    }

    public void setEXPT_DT(String EXPT_DT) {
        this.EXPT_DT = EXPT_DT;
    }

    public String getTOT_DCNT() {
        return TOT_DCNT;
    }

    public void setTOT_DCNT(String TOT_DCNT) {
        this.TOT_DCNT = TOT_DCNT;
    }

    public String getREMN_DCNT() {
        return REMN_DCNT;
    }

    public void setREMN_DCNT(String REMN_DCNT) {
        this.REMN_DCNT = REMN_DCNT;
    }

    public String getPI_RT() {
        return PI_RT;
    }

    public void setPI_RT(String PI_RT) {
        this.PI_RT = PI_RT;
    }

    public String getPROP_STEP_CD() {
        return PROP_STEP_CD;
    }

    public void setPROP_STEP_CD(String PROP_STEP_CD) {
        this.PROP_STEP_CD = PROP_STEP_CD;
    }

    public String getWTCH_POSB_AMT() {
        return WTCH_POSB_AMT;
    }

    public void setWTCH_POSB_AMT(String WTCH_POSB_AMT) {
        this.WTCH_POSB_AMT = WTCH_POSB_AMT;
    }

    public String getACDT_DCL_CD() {
        return ACDT_DCL_CD;
    }

    public void setACDT_DCL_CD(String ACDT_DCL_CD) {
        this.ACDT_DCL_CD = ACDT_DCL_CD;
    }

    public String getLMIT_LMT_ACCO_YN() {
        return LMIT_LMT_ACCO_YN;
    }

    public void setLMIT_LMT_ACCO_YN(String LMIT_LMT_ACCO_YN) {
        this.LMIT_LMT_ACCO_YN = LMIT_LMT_ACCO_YN;
    }

    public String getCHCR_PROP_PRGS_STCD() {
        return CHCR_PROP_PRGS_STCD;
    }

    public void setCHCR_PROP_PRGS_STCD(String CHCR_PROP_PRGS_STCD) {
        this.CHCR_PROP_PRGS_STCD = CHCR_PROP_PRGS_STCD;
    }

    public String getATMT_EXPT_EXTS_CD() {
        return ATMT_EXPT_EXTS_CD;
    }

    public void setATMT_EXPT_EXTS_CD(String ATMT_EXPT_EXTS_CD) {
        this.ATMT_EXPT_EXTS_CD = ATMT_EXPT_EXTS_CD;
    }

    public String getOVRD_DVCD() {
        return OVRD_DVCD;
    }

    public void setOVRD_DVCD(String OVRD_DVCD) {
        this.OVRD_DVCD = OVRD_DVCD;
    }

    public String getLOAN_EXTS_POSB_YN() {
        return LOAN_EXTS_POSB_YN;
    }

    public void setLOAN_EXTS_POSB_YN(String LOAN_EXTS_POSB_YN) {
        this.LOAN_EXTS_POSB_YN = LOAN_EXTS_POSB_YN;
    }

    public String getMM_PI_DD() {
        return MM_PI_DD;
    }

    public void setMM_PI_DD(String MM_PI_DD) {
        this.MM_PI_DD = MM_PI_DD;
    }

    public String getFAX_SEND_PRGS_CNTN() {
        return FAX_SEND_PRGS_CNTN;
    }

    public void setFAX_SEND_PRGS_CNTN(String FAX_SEND_PRGS_CNTN) {
        this.FAX_SEND_PRGS_CNTN = FAX_SEND_PRGS_CNTN;
    }

    public String getCANO() {
        return CANO;
    }

    public void setCANO(String CANO) {
        this.CANO = CANO;
    }

    public String getCHCR_PROP_POSB_YN() {
        return CHCR_PROP_POSB_YN;
    }

    public void setCHCR_PROP_POSB_YN(String CHCR_PROP_POSB_YN) {
        this.CHCR_PROP_POSB_YN = CHCR_PROP_POSB_YN;
    }

    public String getSHRN_ACCO_YN() {
        return SHRN_ACCO_YN;
    }

    public void setSHRN_ACCO_YN(String SHRN_ACCO_YN) {
        this.SHRN_ACCO_YN = SHRN_ACCO_YN;
    }

    public String getLNP_SQNC() {
        return LNP_SQNC;
    }

    public void setLNP_SQNC(String LNP_SQNC) {
        this.LNP_SQNC = LNP_SQNC;
    }

    public Const.ADD_ACCOUNT_MODE getADD_ACCOUNT_MODE() {
        return ADD_ACCOUNT_MODE;
    }

    public void setADD_ACCOUNT_MODE(Const.ADD_ACCOUNT_MODE ADD_ACCOUNT_MODE) {
        this.ADD_ACCOUNT_MODE = ADD_ACCOUNT_MODE;
    }

    public ArrayList<SlidingItemInfo> getListSlidings() {
        return listSlidingItem;
    }

    public void setListSlidings(ArrayList<SlidingItemInfo> listSlidingItem) {
        this.listSlidingItem = listSlidingItem;
    }

    public ArrayList<TagItemInfo> getListTags() {
        return listTags;
    }

    public void setListTags(ArrayList<TagItemInfo> listTags) {
        this.listTags = listTags;
    }

    public ArrayList<MainAdsItemInfo> getListAds() {
        return listAds;
    }

    public void setListAds(ArrayList<MainAdsItemInfo> listAds) {
        this.listAds = listAds;
    }
}
