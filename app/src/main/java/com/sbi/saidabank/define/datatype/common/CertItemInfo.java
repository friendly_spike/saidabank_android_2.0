package com.sbi.saidabank.define.datatype.common;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * CertItemInfo : 인증서 아이템 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class CertItemInfo implements Parcelable {

	private String name;
	private String type;
	private String ver;
	private String no;
	private String auth;
	private String issueAgency;
	private String state;
	private String issueDate;
	private String expireDate;
	private String signAlog;
	private String issuer;
	private String certCN;

	public CertItemInfo(String name, String type, String ver, String no, String auth,
						String issueAgency, String state, String issueDate, String expireDate,
						String signAlog, String issuer, String certCN) {
		this.name = name;
		this.type = type;
		this.ver = ver;
		this.no = no;
		this.auth = auth;
		this.issueAgency = issueAgency;
		this.state = state;
		this.issueDate = issueDate;
		this.expireDate = expireDate;
		this.signAlog = signAlog;
		this.issuer = issuer;
		this.certCN = certCN;
	}

	public CertItemInfo() {
	}

	public CertItemInfo(Parcel src) {
		name = src.readString();
		type = src.readString();
		ver = src.readString();
		no = src.readString();
		auth = src.readString();
		issueAgency = src.readString();
		state = src.readString();
		issueDate = src.readString();
		expireDate = src.readString();
		signAlog = src.readString();
		issuer = src.readString();
		certCN = src.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name);
		dest.writeString(type);
		dest.writeString(ver);
		dest.writeString(no);
		dest.writeString(auth);
		dest.writeString(issueAgency);
		dest.writeString(state);
		dest.writeString(issueDate);
		dest.writeString(expireDate);
		dest.writeString(signAlog);
		dest.writeString(issuer);
		dest.writeString(certCN);
	}

	public static final Creator<CertItemInfo> CREATOR = new Creator<CertItemInfo>() {
		@Override
		public CertItemInfo createFromParcel(Parcel src) {
			return new CertItemInfo(src);
		}

		@Override
		public CertItemInfo[] newArray(int size) {
			return new CertItemInfo[size];
		}
	};

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getVer() {
		return ver;
	}

	public void setVer(String ver) {
		this.ver = ver;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getIssueAgency() {
		return issueAgency;
	}

	public void setIssueAgency(String issueAgency) {
		this.issueAgency = issueAgency;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}

	public String getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(String expireDate) {
		this.expireDate = expireDate;
	}

	public String getSignAlog() {
		return signAlog;
	}

	public void setSignAlog(String signAlog) {
		this.signAlog = signAlog;
	}

	public String getIssuer() {
		return issuer;
	}

	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}

	public String getCertCN() {
		return certCN;
	}

	public void setCertCN(String certCN) {
		this.certCN = certCN;
	}
}
