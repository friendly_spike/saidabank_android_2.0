package com.sbi.saidabank.define.datatype.openbank;

import com.brandongogetap.stickyheaders.exposed.StickyHeader;

public class CellTypeHeader extends CellType implements StickyHeader {

    public CellTypeHeader(int groupPos){
        super(CellType.HEADER,groupPos,-1);
    }
}
