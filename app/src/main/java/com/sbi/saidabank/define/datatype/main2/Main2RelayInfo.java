package com.sbi.saidabank.define.datatype.main2;

import org.json.JSONObject;

public class Main2RelayInfo {
    private String  DSCT_CD;	        //구분코드
    private String  ACNO;	            //계좌번호
    private String  PROD_CD;	        //상품코드
    private String  ACCO_IDNO;	        //계좌식별번호
    private String  TRNF_BANK_CD;	    //이체은행코드
    private String  TRNF_DEPR_NM;	    //이어하기 상품명
    private int     REMN_DCNT;	        //잔여일수
    private String  PROP_STEP_CD;	    //신청단계코드
    private String  FAX_SEND_PRGS_CNTN;	//팩스발송진행내용
    private String  CANO;	            //카드번호
    private String  TRAM;       	    //이체금액
    private String  FAM_CARD_PRGS_STEP_CD;	//가족카드진행단계코드
    private String  SHRN_ACCO_YN;	        //가족카드 계좌주/공유자

    public Main2RelayInfo(JSONObject object) {
        DSCT_CD = object.optString("DSCT_CD");
        ACNO = object.optString("ACNO");
        PROD_CD = object.optString("PROD_CD");
        ACCO_IDNO = object.optString("ACCO_IDNO");
        TRNF_BANK_CD = object.optString("TRNF_BANK_CD");
        TRNF_DEPR_NM = object.optString("TRNF_DEPR_NM");
        REMN_DCNT = object.optInt("REMN_DCNT");
        PROP_STEP_CD = object.optString("PROP_STEP_CD");
        FAX_SEND_PRGS_CNTN = object.optString("FAX_SEND_PRGS_CNTN");
        CANO = object.optString("CANO");
        TRAM = object.optString("TRAM");
        FAM_CARD_PRGS_STEP_CD = object.optString("FAM_CARD_PRGS_STEP_CD");
        SHRN_ACCO_YN = object.optString("SHRN_ACCO_YN","N");
    }

    public String getDSCT_CD() {
        return DSCT_CD;
    }

    public void setDSCT_CD(String DSCT_CD) {
        this.DSCT_CD = DSCT_CD;
    }

    public String getACNO() {
        return ACNO;
    }

    public void setACNO(String ACNO) {
        this.ACNO = ACNO;
    }

    public String getPROD_CD() {
        return PROD_CD;
    }

    public void setPROD_CD(String PROD_CD) {
        this.PROD_CD = PROD_CD;
    }

    public String getACCO_IDNO() {
        return ACCO_IDNO;
    }

    public void setACCO_IDNO(String ACCO_IDNO) {
        this.ACCO_IDNO = ACCO_IDNO;
    }

    public String getTRNF_BANK_CD() {
        return TRNF_BANK_CD;
    }

    public void setTRNF_BANK_CD(String TRNF_BANK_CD) {
        this.TRNF_BANK_CD = TRNF_BANK_CD;
    }

    public String getTRNF_DEPR_NM() {
        return TRNF_DEPR_NM;
    }

    public void setTRNF_DEPR_NM(String TRNF_DEPR_NM) {
        this.TRNF_DEPR_NM = TRNF_DEPR_NM;
    }

    public int getREMN_DCNT() {
        return REMN_DCNT;
    }

    public void setREMN_DCNT(int REMN_DCNT) {
        this.REMN_DCNT = REMN_DCNT;
    }

    public String getPROP_STEP_CD() {
        return PROP_STEP_CD;
    }

    public void setPROP_STEP_CD(String PROP_STEP_CD) {
        this.PROP_STEP_CD = PROP_STEP_CD;
    }

    public String getFAX_SEND_PRGS_CNTN() {
        return FAX_SEND_PRGS_CNTN;
    }

    public void setFAX_SEND_PRGS_CNTN(String FAX_SEND_PRGS_CNTN) {
        this.FAX_SEND_PRGS_CNTN = FAX_SEND_PRGS_CNTN;
    }

    public String getCANO() {
        return CANO;
    }

    public void setCANO(String CANO) {
        this.CANO = CANO;
    }

    public String getTRAM() {
        return TRAM;
    }

    public void setTRAM(String TRAM) {
        this.TRAM = TRAM;
    }

    public String getFAM_CARD_PRGS_STEP_CD() {
        return FAM_CARD_PRGS_STEP_CD;
    }

    public void setFAM_CARD_PRGS_STEP_CD(String FAM_CARD_PRGS_STEP_CD) {
        this.FAM_CARD_PRGS_STEP_CD = FAM_CARD_PRGS_STEP_CD;
    }

    public String getSHRN_ACCO_YN() {
        return SHRN_ACCO_YN;
    }

    public void setSHRN_ACCO_YN(String SHRN_ACCO_YN) {
        this.SHRN_ACCO_YN = SHRN_ACCO_YN;
    }
}
