package com.sbi.saidabank.define.datatype.transfer;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * TransferRecentlyInfo : 자주/즐겨찾기 리스트
 */
public class ITransferRecentlyInfo implements Serializable {

    private String DTA_DVCD;            // 자료 구분코드
    private String FAVO_ACCO_YN;        // 즐겨찾기 계좌여부
    private String MNRC_BANK_CD;        // 입금 은행코드
    private String DTLS_FNLT_CD;        // 세부금융기관코드(저축은행코드)
    private String MNRC_ACNO;           // 입금 계좌번호
    private String MNRC_ACCO_DEPR_NM;   // 입금 계좌예금주명
    private String MNRC_ACCO_ALS;       // 입금 계좌별칭
    private String MNRC_TLNO;           // 입금 전화번호
    private String INQ_RNKN;            // 조회 순위
    private String GRP_YN;              // 그룹여부
    private String GRP_SRNO;            // 그룹일련번호
    private String GRP_CCNT;            // 그룹건수
    private String GRP_DTLS_SQNC;       // 그룹상세순서
    private String MNRC_BANK_NM;        // 입금은행명
    private String CLAS_NM;             // 클래스이름
    private String OWBK_CUST_YN;	    //당행고객여부


    public ITransferRecentlyInfo() {
    }

    public ITransferRecentlyInfo(JSONObject object) {
        DTA_DVCD = object.optString("DTA_DVCD");
        FAVO_ACCO_YN = object.optString("FAVO_ACCO_YN");
        MNRC_BANK_CD = object.optString("MNRC_BANK_CD");
        DTLS_FNLT_CD = object.optString("DTLS_FNLT_CD");
        MNRC_ACNO = object.optString("MNRC_ACNO");
        MNRC_ACCO_DEPR_NM = object.optString("MNRC_ACCO_DEPR_NM");
        MNRC_ACCO_ALS = object.optString("MNRC_ACCO_ALS");
        MNRC_TLNO = object.optString("MNRC_TLNO");
        INQ_RNKN = object.optString("INQ_RNKN");
        GRP_YN = object.optString("GRP_YN");
        GRP_SRNO = object.optString("GRP_SRNO");
        GRP_CCNT = object.optString("GRP_CCNT");
        GRP_DTLS_SQNC = object.optString("GRP_DTLS_SQNC");
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");
        OWBK_CUST_YN = object.optString("OWBK_CUST_YN");
    }

    /**
     * 보내는 사람 검색에서 사용하는 함수.
     * @return
     */
    public String getTextForSearch(){
        return MNRC_ACNO + " " + MNRC_ACCO_DEPR_NM + " " + MNRC_ACCO_ALS + " " + MNRC_TLNO;
    }

    public String getDTA_DVCD() {
        return DTA_DVCD;
    }

    public void setDTA_DVCD(String DTA_DVCD) {
        this.DTA_DVCD = DTA_DVCD;
    }

    public String getFAVO_ACCO_YN() {
        return FAVO_ACCO_YN;
    }

    public void setFAVO_ACCO_YN(String FAVO_ACCO_YN) {
        this.FAVO_ACCO_YN = FAVO_ACCO_YN;
    }

    public String getMNRC_BANK_CD() {
        return MNRC_BANK_CD;
    }

    public void setMNRC_BANK_CD(String MNRC_BANK_CD) {
        this.MNRC_BANK_CD = MNRC_BANK_CD;
    }

    public String getDTLS_FNLT_CD() {
        return DTLS_FNLT_CD;
    }

    public void setDTLS_FNLT_CD(String DTLS_FNLT_CD) {
        this.DTLS_FNLT_CD = DTLS_FNLT_CD;
    }

    public String getMNRC_ACNO() {
        return MNRC_ACNO;
    }

    public void setMNRC_ACNO(String MNRC_ACNO) {
        this.MNRC_ACNO = MNRC_ACNO;
    }

    public String getMNRC_ACCO_DEPR_NM() {
        return MNRC_ACCO_DEPR_NM;
    }

    public void setMNRC_ACCO_DEPR_NM(String MNRC_ACCO_DEPR_NM) {
        this.MNRC_ACCO_DEPR_NM = MNRC_ACCO_DEPR_NM;
    }

    public String getMNRC_ACCO_ALS() {
        return MNRC_ACCO_ALS;
    }

    public void setMNRC_ACCO_ALS(String MNRC_ACCO_ALS) {
        this.MNRC_ACCO_ALS = MNRC_ACCO_ALS;
    }

    public String getMNRC_TLNO() {
        return MNRC_TLNO;
    }

    public void setMNRC_TLNO(String MNRC_TLNO) {
        this.MNRC_TLNO = MNRC_TLNO;
    }

    public String getINQ_RNKN() {
        return INQ_RNKN;
    }

    public void setINQ_RNKN(String INQ_RNKN) {
        this.INQ_RNKN = INQ_RNKN;
    }

    public String getGRP_YN() {
        return GRP_YN;
    }

    public void setGRP_YN(String GRP_YN) {
        this.GRP_YN = GRP_YN;
    }

    public String getGRP_SRNO() {
        return GRP_SRNO;
    }

    public void setGRP_SRNO(String GRP_SRNO) {
        this.GRP_SRNO = GRP_SRNO;
    }

    public String getGRP_CCNT() {
        return GRP_CCNT;
    }

    public void setGRP_CCNT(String GRP_CCNT) {
        this.GRP_CCNT = GRP_CCNT;
    }

    public String getGRP_DTLS_SQNC() {
        return GRP_DTLS_SQNC;
    }

    public void setGRP_DTLS_SQNC(String GRP_DTLS_SQNC) {
        this.GRP_DTLS_SQNC = GRP_DTLS_SQNC;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    public String getCLAS_NM() {
        return CLAS_NM;
    }

    public void setCLAS_NM(String CLAS_NM) {
        this.CLAS_NM = CLAS_NM;
    }

    public String getOWBK_CUST_YN() {
        return OWBK_CUST_YN;
    }

    public void setOWBK_CUST_YN(String OWBK_CUST_YN) {
        this.OWBK_CUST_YN = OWBK_CUST_YN;
    }
}
