package com.sbi.saidabank.define.datatype.transfer;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * TransferPhoneInfo : 휴대폰 이체 VO
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class TransferPhoneInfo implements Serializable {

    private String TLNO;                // 자료 구분코드
    private String FAVO_ACCO_YN;        // 즐겨찾기 계좌여부
    private String MNRC_BANK_CD;        // 입금 은행코드
    private String MNRC_ACNO;           // 입금 계좌번호
    private String MNRC_ACCO_DEPR_NM;   // 입금 계좌예금주명
    private String MNRC_ACCO_ALS;       // 입금 계좌별칭
    private String MNRC_TLNO;           // 입금 전화번호
    private String INQ_RNKN;            // 조회순위
    private String MNRC_BANK_NM;        // 입금 은행명
    private String CLAS_NM;             // 클래스명

    public TransferPhoneInfo() {
    }

    public TransferPhoneInfo(JSONObject object) {
        TLNO = object.optString("TLNO");
        FAVO_ACCO_YN = object.optString("FAVO_ACCO_YN");
        MNRC_BANK_CD = object.optString("MNRC_BANK_CD");
        MNRC_ACNO = object.optString("MNRC_ACNO");
        MNRC_ACCO_DEPR_NM = object.optString("MNRC_ACCO_DEPR_NM");
        MNRC_ACCO_ALS = object.optString("MNRC_ACCO_ALS");
        MNRC_TLNO = object.optString("MNRC_ACNO");
        INQ_RNKN = object.optString("INQ_RNKN");
        MNRC_BANK_NM = object.optString("MNRC_BANK_NM");
        CLAS_NM = object.optString("CLAS_NM");
    }

    public String getTLNO() {
        return TLNO;
    }

    public void setTLNO(String TLNO) {
        this.TLNO = TLNO;
    }

    public String getFAVO_ACCO_YN() {
        return FAVO_ACCO_YN;
    }

    public void setFAVO_ACCO_YN(String FAVO_ACCO_YN) {
        this.FAVO_ACCO_YN = FAVO_ACCO_YN;
    }

    public String getMNRC_BANK_CD() {
        return MNRC_BANK_CD;
    }

    public void setMNRC_BANK_CD(String MNRC_BANK_CD) {
        this.MNRC_BANK_CD = MNRC_BANK_CD;
    }

    public String getMNRC_ACNO() {
        return MNRC_ACNO;
    }

    public void setMNRC_ACNO(String MNRC_ACNO) {
        this.MNRC_ACNO = MNRC_ACNO;
    }

    public String getMNRC_ACCO_DEPR_NM() {
        return MNRC_ACCO_DEPR_NM;
    }

    public void setMNRC_ACCO_DEPR_NM(String MNRC_ACCO_DEPR_NM) {
        this.MNRC_ACCO_DEPR_NM = MNRC_ACCO_DEPR_NM;
    }

    public String getMNRC_ACCO_ALS() {
        return MNRC_ACCO_ALS;
    }

    public void setMNRC_ACCO_ALS(String MNRC_ACCO_ALS) {
        this.MNRC_ACCO_ALS = MNRC_ACCO_ALS;
    }

    public String getMNRC_TLNO() {
        return MNRC_TLNO;
    }

    public void setMNRC_TLNO(String MNRC_TLNO) {
        this.MNRC_TLNO = MNRC_TLNO;
    }

    public String getINQ_RNKN() {
        return INQ_RNKN;
    }

    public void setINQ_RNKN(String INQ_RNKN) {
        this.INQ_RNKN = INQ_RNKN;
    }

    public String getMNRC_BANK_NM() {
        return MNRC_BANK_NM;
    }

    public void setMNRC_BANK_NM(String MNRC_BANK_NM) {
        this.MNRC_BANK_NM = MNRC_BANK_NM;
    }

    public String getCLAS_NM() {
        return CLAS_NM;
    }

    public void setCLAS_NM(String CLAS_NM) {
        this.CLAS_NM = CLAS_NM;
    }
}
