package com.sbi.saidabank.define.datatype.openbank;

public class CellType {
    public static final int HEADER      = 0;
    public static final int ITEM_BANNER = 1;
    public static final int ITEM_ACC    = 2;
    public static final int ITEM_TOTAL  = 3;
    public static final int ITEM_ETC    = 4;


    private int type;
    private int groupPosition;
    private int accountPos;

    public CellType(int type, int groupPosition,int accountPos){
        this.type = type;
        this.groupPosition = groupPosition;
        this.accountPos = accountPos;
    }

    public int getType() {
        return type;
    }

    public int getGroupPosition() {
        return groupPosition;
    }

    public int getAccountPos() {
        return accountPos;
    }
}
