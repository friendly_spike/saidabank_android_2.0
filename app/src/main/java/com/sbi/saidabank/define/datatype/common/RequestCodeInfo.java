package com.sbi.saidabank.define.datatype.common;

import org.json.JSONObject;

/**
 * RequestCodeInfo : 사이다뱅크 코드값 데이터타입
 * 은행,증권사,통신사 등등
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class RequestCodeInfo {

    private String CD_ABRV_NM;  // 코드 대분류
    private String CD_NM;       // 코드 이름
    private String SCCD;        // 코드값

    public RequestCodeInfo(JSONObject object) {
        this.CD_ABRV_NM = object.optString("CD_ABRV_NM");
        this.CD_NM = object.optString("CD_NM");
        this.SCCD = object.optString("SCCD");
    }

    public String getCD_ABRV_NM() {
        return CD_ABRV_NM;
    }

    public void setCD_ABRV_NM(String CD_ABRV_NM) {
        this.CD_ABRV_NM = CD_ABRV_NM;
    }

    public String getCD_NM() {
        return CD_NM;
    }

    public void setCD_NM(String CD_NM) {
        this.CD_NM = CD_NM;
    }

    public String getSCCD() {
        return SCCD;
    }

    public void setSCCD(String SCCD) {
        this.SCCD = SCCD;
    }
}
