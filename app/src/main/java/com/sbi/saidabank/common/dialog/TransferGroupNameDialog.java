package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;


public class TransferGroupNameDialog extends BaseDialog implements View.OnClickListener{//, TextWatcher {
    private Context mContext;
    private EditText mEtName;
    private InputMethodManager mImm;
    private TransferGroupNameDialog.OnConfirmListener _listener;
    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;
    private String mGrpName;
    public TransferGroupNameDialog(Context context,String grpName,TransferGroupNameDialog.OnConfirmListener listener) {
        super(context);
        mContext = context;
        _listener = listener;
        mGrpName = grpName;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        setContentView(R.layout.dialog_transfer_group_name);
        setDialogWidth();

        initUX();
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mEtName = findViewById(R.id.et_name);
        if(!TextUtils.isEmpty(mGrpName)){
            mEtName.setText(mGrpName);
        }
        mEtName.requestFocus();
        Utils.showKeyboard(getContext(),mEtName);
        findViewById(R.id.ibtn_erase).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);

        mImm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibtn_erase: {
                mEtName.setText("");
                break;
            }
            case R.id.btn_ok: {
                String name = mEtName.getText().toString();
                if(TextUtils.isEmpty(name)){
                    Logs.showToast(getContext(),"등록/변경할 이름을 입력하세요.");
                    return;
                }

                //if ((Utils.isKorean(name) != Const.STATE_VALID_STR_CHAR_ERR)) {
                    if (_listener != null) {
                        _listener.onConfirmPress(mEtName.getText().toString());
                    }
                    mImm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
                    dismiss();
                //}
                break;
            }

            case R.id.btn_cancel: {
                mImm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
                dismiss();
                break;
            }

            default:
                break;
        }
    }

//    @Override
//    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//    }
//
//    @Override
//    public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//    }
//
//    @Override
//    public void afterTextChanged(Editable s) {
//        String name = s.toString();
//        if (!name.matches("^[ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]*$")) {
//            mEtName.removeTextChangedListener(this);
//            mEtName.setText(name.replaceAll("[^ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]", ""));
//            mEtName.setSelection(mEtName.getText().length());
//            mEtName.addTextChangedListener(this);
//            name = mEtName.getText().toString();
//        }
//    }

    public interface OnConfirmListener {
        void onConfirmPress(String name);
    }
}
