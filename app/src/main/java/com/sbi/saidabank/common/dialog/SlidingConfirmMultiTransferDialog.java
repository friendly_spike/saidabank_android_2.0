package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.adater.TransferMultiListAdapter;
import com.sbi.saidabank.activity.transaction.TransferManager;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.transfer.TransferVerifyInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * SlidingConfirmMultiTransferDialog : 다건이체 확인 다이얼로그
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class SlidingConfirmMultiTransferDialog extends Dialog {

    private TextView mTvTransferCnt;
    private TextView mTvTransferTotAmount;
    private TextView mTvTransferWithdrawAccount;
    private String mWithdrawAccount;
    private Double dLimitOneDay = -1.0;
    private TransferManager mTransferManager;
    private TransferMultiListAdapter multiTransferListAdapter = null;
    private boolean isLastRemove = false;   // 마지막 리스트 삭제 여부 - '아니요' 선택 시, 마지막 항목 삭제하면 안됨
    private boolean isRemove = false;       // 아이템 삭제 여부
    private Context mContext;
    private FinishListener mFinishListener;

    public interface FinishListener {
        void OnCancelListener(boolean isLastRemove, Double dLimitOneDay, boolean isAllRemove);
        void OnOKListener(boolean isLastRemove);
        void OnRemoveItemListener();
        void OnRemoveAllListener();
        void OnAddTransfer();
        void OnChangeLimit(double dLimitOneDay);
    }

    public SlidingConfirmMultiTransferDialog(@NonNull Context context, String withdrawAccount, FinishListener finishListener) {
        super(context, R.style.SlideDialog);
        this.mContext = context;
        this.mWithdrawAccount = withdrawAccount;
        this.mFinishListener = finishListener;
        this.mTransferManager = TransferManager.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        setContentView(R.layout.dialog_confirm_multi_transfer);
        initLayout();
    }
// ==============================================================================================================
// UI
// ==============================================================================================================
    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {
        mTvTransferCnt = (TextView) findViewById(R.id.textview_transfer_num_multi);
        mTvTransferTotAmount = (TextView) findViewById(R.id.textview_transfer_amount_multi);
        mTvTransferWithdrawAccount = (TextView) findViewById(R.id.textview_withdraw_account_multi);

        Button mBtnAddTransfer = (Button) findViewById(R.id.btn_add_confirm_multi_transfer);
        mBtnAddTransfer.setOnClickListener(onAddTransferClickListener);

        Button mBtnCancel = (Button) findViewById(R.id.btn_cancel_confirm_multi_transfer);
        mBtnCancel.setOnClickListener(onCancelClickListener);

        Button mBtnOK = (Button) findViewById(R.id.btn_ok_confirm_multi_transfer);
        mBtnOK.setOnClickListener(onOkClickListener);

        RecyclerView recyclerViewReceipt = (RecyclerView) findViewById(R.id.recyclerview_receiptInfo);
        if (DataUtil.isNull(multiTransferListAdapter)) {
            multiTransferListAdapter = new TransferMultiListAdapter(mContext, onRemoveItemListener);
            recyclerViewReceipt.setAdapter(multiTransferListAdapter);
            recyclerViewReceipt.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            recyclerViewReceipt.setHasFixedSize(true);
        }
        updateTitleInfo();
    }

    /**
     * 이체 타이틀 정보를 업데이트 한다.
     * */
    private void updateTitleInfo() {
        mTvTransferCnt.setText(String.valueOf(mTransferManager.size()));
        mTvTransferTotAmount.setText(String.format(mContext.getString(R.string.msg_ask_comfirm_transfer), Utils.moneyFormatToWon(mTransferManager.getTotalTransferAmount())));
        mTvTransferWithdrawAccount.setText(DataUtil.isNotNull(mWithdrawAccount) ? mWithdrawAccount : Const.EMPTY);
    }
// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    /**
     * 출금 정보를 업데이트
     */
    private void updateTransferInfo() {
        if (DataUtil.isNotNull(mFinishListener)) {
            if ((DataUtil.isNotNull(mTransferManager) && mTransferManager.size() < 1)) {
                mFinishListener.OnRemoveAllListener();
                dismiss();
                return;
            }
            mFinishListener.OnRemoveItemListener();
        }
        updateTitleInfo();
    }

    /**
     * 에러 메시지를 출력
     *
     * @param msg 에러내용
     */
    private void showErrorMsgDialog(String msg) {
        if (DataUtil.isNull(msg)) {
            return;
        }
        AlertDialog alertDialog = new AlertDialog(mContext);
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };
        alertDialog.msg = msg.replaceAll("\\\\n", "\n");
        alertDialog.show();
    }

    /**
     * key가 존재하는지 체크한다.
     *
     * @param json
     * @param key
     * @return
     */
    public String getJsonString(JSONObject json, String key) {
        String outValue = null;
        if (json.has(key)) {
            try {
                outValue = json.getString(key);
            } catch (JSONException e) {
                MLog.e(e);
            }
        }
        return outValue;
    }
// ==============================================================================================================
// LISTENER
// ==============================================================================================================
    /**
     * 다건이체 목록 아이템 삭제 이벤트 리스너
     */
    private TransferMultiListAdapter.RemoveListener onRemoveItemListener = new TransferMultiListAdapter.RemoveListener() {
        @Override
        public void OnRemoveListener(final int position) {
            if (DataUtil.isNotNull(mTransferManager) && mTransferManager.size() >= position) {
                AlertDialog alertDialog = new AlertDialog(mContext);
                alertDialog.mNListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                };
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // 선택한 아이템을 이체목록 제외처리 요청
                        requestRemoveTransfer(position, false, false);
                    }
                };
                alertDialog.msg = String.format(mContext.getString(R.string.msg_remove_transfer), mTransferManager.get(position).getRECV_NM());
                alertDialog.show();
            }
        }
    };

    /**
     * 추가이체 아이템 클릭 이벤트 리스너
     */
    private View.OnClickListener onAddTransferClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View v) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            // 다건이체 개수 유효성 체크
            if (DataUtil.isNotNull(multiTransferListAdapter) && multiTransferListAdapter.getItemCount() >= Const.MAX_TRANSFER_AT_ONCE) {
                AlertDialog alertDialog = new AlertDialog(mContext);
                alertDialog.mPListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    }
                };
                alertDialog.msg = mContext.getString(R.string.msg_max_transfer_at_once);
                alertDialog.show();
                return;
            }
            if (DataUtil.isNotNull(mFinishListener)) {
                mFinishListener.OnAddTransfer();
            }
        }
    };

    /**
     * 취소 클릭 이벤트 리스너
     */
    private View.OnClickListener onCancelClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View v) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            // 삭제이력이 있을경우
            if (isRemove) {
                // 2건미만일 경우
                if (DataUtil.isNotNull(mTransferManager) && mTransferManager.size() < 2) {
                    requestRemoveTransfer((mTransferManager.size()-1), true, false);
                } else {
                    for (int i = 0; i < (mTransferManager.size()-1); i++) {
                        requestRemoveTransfer(i, true, true);
                    }
                    if (DataUtil.isNotNull(mFinishListener)) {
                        mFinishListener.OnCancelListener(false, dLimitOneDay, true);
                    }
                    dismiss();
                }
            } else {
                if (DataUtil.isNotNull(mFinishListener)) {
                    mFinishListener.OnCancelListener(isLastRemove, dLimitOneDay, false);
                }
                dismiss();
            }
        }
    };

    /**
     * 이체 클릭 이벤트 리스너
     */
    private View.OnClickListener onOkClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View v) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            if (DataUtil.isNotNull(mFinishListener)) {
                mFinishListener.OnOKListener(isLastRemove);
            }
            dismiss();
        }
    };

// ==============================================================================================================
// API
// ==============================================================================================================
    /**
     * 선택된 이체목록 제외처리 요청
     *
     * @param position 삭제할 아이템의 포지션 번호
     * @param cancel
     */
    private void requestRemoveTransfer(final int position, final boolean cancel, final boolean isFor) {

        MLog.d();

        if (DataUtil.isNotNull(mTransferManager) && mTransferManager.size() >= position) {

            TransferVerifyInfo transferInfo = mTransferManager.get(position);

            if (DataUtil.isNull(transferInfo) || DataUtil.isNull(transferInfo.getUUID_NO())) {
                return;
            }

            Map param = new HashMap();
            param.put("UUID_NO", transferInfo.getUUID_NO());

            HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
                @Override
                public void endHttpRequest(String ret) {

                    if (DataUtil.isNull(ret)) {
                        showErrorMsgDialog(mContext.getString(R.string.msg_debug_no_response));
                        return;
                    }

                    try {

                        JSONObject object = new JSONObject(ret);
                        if (DataUtil.isNull(object)) {
                            showErrorMsgDialog(mContext.getString(R.string.msg_debug_err_response));
                            return;
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        if (objectHead == null) {
                            showErrorMsgDialog(mContext.getString(R.string.common_msg_no_reponse_value_was));
                            return;
                        }

                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = mContext.getString(R.string.common_msg_no_reponse_value_was);
                            String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
                            ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
                            return;
                        }

                        // 마지막 아이템 삭제여부 체크
                        isLastRemove = (position == (mTransferManager.size() - 1));

                        // 이체한도 유효성 체크
                        String D1_UZ_LMIT_AMT = object.optString("D1_UZ_LMIT_AMT");
                        if (!TextUtils.isEmpty(D1_UZ_LMIT_AMT)) {
                            dLimitOneDay = Double.valueOf(D1_UZ_LMIT_AMT);
                            mFinishListener.OnChangeLimit(dLimitOneDay);
                        }

                        // 이체목록 초기화
                        mTransferManager.clear();

                        JSONArray array = object.optJSONArray("REC");
                        if (array == null) return;
                        for (int index = 0; index < array.length(); index++) {
                            JSONObject jsonObject = array.getJSONObject(index);
                            if (jsonObject == null)
                                continue;

                            TransferVerifyInfo item = new TransferVerifyInfo();

                            String CNTP_FIN_INST_CD = jsonObject.optString("CNTP_FIN_INST_CD");
                            if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                                CNTP_FIN_INST_CD = "028";
                            }
                            item.setCNTP_FIN_INST_CD(CNTP_FIN_INST_CD);

                            String CNTP_BANK_ACNO = jsonObject.optString("CNTP_BANK_ACNO");
                            item.setCNTP_BANK_ACNO(CNTP_BANK_ACNO);

                            String CNTP_ACCO_DEPR_NM = jsonObject.optString("CNTP_ACCO_DEPR_NM");
                            item.setRECV_NM(CNTP_ACCO_DEPR_NM);

                            String DEPO_BNKB_MRK_NM = jsonObject.optString("DEPO_BNKB_MRK_NM");
                            item.setDEPO_BNKB_MRK_NM(DEPO_BNKB_MRK_NM);

                            String TRAN_BNKB_MRK_NM = jsonObject.optString("TRAN_BNKB_MRK_NM");
                            item.setTRAN_BNKB_MRK_NM(TRAN_BNKB_MRK_NM);

                            String TRN_AMT = jsonObject.optString("TRN_AMT");
                            item.setTRN_AMT(TRN_AMT);

                            String FEE = jsonObject.optString("FEE");
                            item.setFEE(FEE);

                            String TRNF_DMND_DT = getJsonString(jsonObject, "TRNF_DMND_DT");
                            item.setTRNF_DMND_DT(DataUtil.isNotNull(TRNF_DMND_DT) ? TRNF_DMND_DT : Const.EMPTY);

                            String TRNF_DMND_TM = getJsonString(jsonObject, "TRNF_DMND_TM");
                            item.setTRNF_DMND_TM(DataUtil.isNotNull(TRNF_DMND_TM) ? TRNF_DMND_TM : Const.EMPTY);

                            String TRN_MEMO_CNTN = getJsonString(jsonObject, "TRN_MEMO_CNTN");
                            if (DataUtil.isNotNull(TRN_MEMO_CNTN)) {
                                item.setTRN_MEMO_CNTN(TRN_MEMO_CNTN);
                            }

                            String TRNF_DVCD = jsonObject.optString("TRNF_DVCD");
                            item.setTRNF_DVCD(TRNF_DVCD);

                            String MNRC_BANK_NM = jsonObject.optString("MNRC_BANK_NM");
                            item.setMNRC_BANK_NM(MNRC_BANK_NM);

                            String BANK_NM = jsonObject.optString("BANK_NM");
                            item.setBANK_NM(BANK_NM);

                            String UUID_NO = jsonObject.optString("UUID_NO");
                            item.setUUID_NO(UUID_NO);

                            // 이체목록을 추가
                            mTransferManager.add(index, item);
                        }

                        // 어댑터뷰를 갱신처리
                        if (DataUtil.isNotNull(multiTransferListAdapter)) {
                            multiTransferListAdapter.notifyDataSetChanged();
                        }

                        // 취소일경우
                        if (cancel) {
                            // 단건 삭제일 경우
                            if (!isFor) {
                                if (DataUtil.isNotNull(mFinishListener)) {
                                    mFinishListener.OnCancelListener(isLastRemove, dLimitOneDay, false);
                                }
                                isRemove = false;
                                dismiss();
                            }
                        }
                        // 아이템 삭제요청일 경우
                        else {
                            updateTransferInfo();
                            isRemove = true;
                        }

                    } catch (JSONException e) {
                        MLog.e(e);
                    }
                }
            });
        }
    }
}
