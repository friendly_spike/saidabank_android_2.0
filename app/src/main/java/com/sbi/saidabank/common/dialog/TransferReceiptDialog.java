package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.TransferReceiptSaveActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.CustomViewPager;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.TransferReceiptInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: TransferConfirmationDialog
 * Created by 950546
 * Date: 2019-01-30
 * Time: 오후 2:41
 * Description: 이체확인증 팝업
 */
public class TransferReceiptDialog extends Dialog implements View.OnClickListener {
    private final int MAX_TRANSFER_COUNT = 5;
    private String mName;
    private String mBankName;
    private ImageView[] mIvIndi = new ImageView[MAX_TRANSFER_COUNT];
    private TextView mTvCurIndex;
    private TextView mTvTotalCount;
    private CustomViewPager mViewPager;
    private Button mBtPositive;
    private ImageButton mBtNegative;
    private Context mContext;
    private int mPagerCount = 0;
    private int mCurrentPos = 0;
    private final ArrayList<TransferReceiptInfo> mTransferReceiptInfos;

    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;

    public TransferReceiptDialog(Context context, ArrayList<TransferReceiptInfo> transferReceiptInfos, String name, String bankName) {
        //super(context);
        super(context, R.style.AppThemeNoActionBar);
        mContext = context;
        mTransferReceiptInfos = transferReceiptInfos;
        mPagerCount = mTransferReceiptInfos.size();
        mName = name;
        mBankName = bankName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        setContentView(R.layout.dialog_transfer_receipt);

        if (mTransferReceiptInfos == null || (mTransferReceiptInfos.size() == 0 || mTransferReceiptInfos.size() > MAX_TRANSFER_COUNT)) {
            Logs.showToast(mContext, "이체 정보 오류");
            return;
        }

        initUX();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_confirm: {
                if (mPListener != null) {
                    mPListener.onClick(view);
                }
                dismiss();
                break;
            }

            case R.id.btn_download: {
                Intent intent = new Intent(mContext, TransferReceiptSaveActivity.class);
                intent.putExtra(Const.INTENT_LIST_TRANSFER_RECEIPT_INFO, mTransferReceiptInfos.get(mCurrentPos));
                intent.putExtra("name", mName);
                intent.putExtra(Const.INTENT_BANK_NM, mBankName);
                mContext.startActivity(intent);
                ((Activity) mContext).overridePendingTransition(R.anim.sliding_dialog_up, R.anim.stay);
                break;
            }

            default:
                break;
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mIvIndi[0] = findViewById(R.id.iv_indi01);
        mIvIndi[1] = findViewById(R.id.iv_indi02);
        mIvIndi[2] = findViewById(R.id.iv_indi03);
        mIvIndi[3] = findViewById(R.id.iv_indi04);
        mIvIndi[4] = findViewById(R.id.iv_indi05);
        mIvIndi[0].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_on));
        mIvIndi[1].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_off));
        mIvIndi[2].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_off));
        mIvIndi[3].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_off));
        mIvIndi[4].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_off));

        for (int i = 0; i < MAX_TRANSFER_COUNT - mPagerCount; i++) {
            mIvIndi[MAX_TRANSFER_COUNT - 1 - i].setVisibility(View.GONE);
        }

        mTvCurIndex = (TextView) findViewById(R.id.tv_transfer_count);
        mTvTotalCount = (TextView) findViewById(R.id.tv_total_transfercount);

        if (mPagerCount > 1) {
            mTvCurIndex.setText("1");
            mTvTotalCount.setText("/" + mPagerCount + ")");
        } else {
            LinearLayout layoutIndi = (LinearLayout) findViewById(R.id.ll_indi);
            TextView textCountBracket = (TextView) findViewById(R.id.tv_transfer_count_bracket);
            layoutIndi.setVisibility(View.INVISIBLE);
            textCountBracket.setVisibility(View.GONE);
            mTvCurIndex.setVisibility(View.GONE);
            mTvTotalCount.setVisibility(View.GONE);
        }

        mViewPager = (CustomViewPager) findViewById(R.id.viewpager);
        mViewPager.setOffscreenPageLimit(mPagerCount);

        TransferPagerAdapter adapter = new TransferPagerAdapter();
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }

            @Override
            public void onPageSelected(int position) {
                Logs.e("curpos : " + mCurrentPos);
                mCurrentPos = position;
                setTransferConfirmation();
            }
        });
        mBtPositive = (Button) findViewById(R.id.btn_confirm);
        mBtPositive.setOnClickListener(this);
        mBtNegative = (ImageButton) findViewById(R.id.btn_download);
        mBtNegative.setOnClickListener(this);
    }

    /**
     * swipe 화면 전환 시 상단 표시 내용 업데이트
     */
    private void setTransferConfirmation() {
        for (int i = 0; i < mPagerCount; i++) {
            if (i == mCurrentPos) {
                mIvIndi[i].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_on));
            } else {
                mIvIndi[i].setImageDrawable(mContext.getResources().getDrawable(R.drawable.indi_off));
            }
        }
        mTvCurIndex.setText(String.valueOf(mCurrentPos + 1));
    }

    /**
     * 이체확인 페이저 어댑터
     */
    private class TransferPagerAdapter extends PagerAdapter {

        @Override
        public int getCount() {
            return mPagerCount;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_transfer_receipt_item, container, false);

            TextView tvTransferAmount = view.findViewById(R.id.tv_transferamount);
            TextView tvFee = view.findViewById(R.id.tv_fee);
            TextView tvTransferDate = view.findViewById(R.id.tv_transferdate);
            TextView tvAccountName = view.findViewById(R.id.tv_accountname);
            TextView tvWithdrawAccount = view.findViewById(R.id.tv_withdrawaccount);
            TextView tvWithdrawName = view.findViewById(R.id.tv_withdrawname);
            TextView tvDepositAccountName = view.findViewById(R.id.tv_depositccountname);
            TextView tvDepositAccount = view.findViewById(R.id.tv_depositccount);
            TextView tvDepositName = view.findViewById(R.id.tv_depositname);
            TextView tvDisplayRecipient = view.findViewById(R.id.tv_displayrecipient);
            TextView tvDisplaySending = view.findViewById(R.id.tv_displaysending);

            TransferReceiptInfo listTransferReceiptInfo = mTransferReceiptInfos.get(position);

            String strValue = listTransferReceiptInfo.getTRN_AMT();
            strValue = Utils.moneyFormatToWon(Double.valueOf(strValue));
            if (!TextUtils.isEmpty(strValue))
                tvTransferAmount.setText(strValue);

            strValue = listTransferReceiptInfo.getTRN_FEE();
            strValue = Utils.moneyFormatToWon(Double.valueOf(strValue));
            tvFee.setText(strValue + " " + mContext.getString(R.string.won));

            String TRNF_DT = listTransferReceiptInfo.getTRNF_DT();
            String TRNF_MT = listTransferReceiptInfo.getTRNF_TM();
            if (!TextUtils.isEmpty(TRNF_DT) && !TextUtils.isEmpty(TRNF_MT) ) {
                String year = TRNF_DT.substring(0, 4);
                String month = TRNF_DT.substring(4, 6);
                String day = TRNF_DT.substring(6, 8);
                String time = TRNF_MT.substring(0, 2);
                String min = TRNF_MT.substring(2, 4);
                String sec = TRNF_MT.substring(4, 6);

                String date = year + "." + month + "." + day + " " + time + ":" + min + ":" + sec;
                tvTransferDate.setText(date);
            }

            tvAccountName.setText(mBankName);

            strValue = listTransferReceiptInfo.getWTCH_ACNO();
            if (!TextUtils.isEmpty(strValue)) {
                String account = strValue.substring(0, 5) + "-" + strValue.substring(5, 7) + "-" + strValue.substring(7, strValue.length());
                tvWithdrawAccount.setText(account);
            }
            if (!TextUtils.isEmpty(mName))
                tvWithdrawName.setText(mName);
            String bankname = listTransferReceiptInfo.getMNRC_BANK_NM();
            if (!TextUtils.isEmpty(bankname)) {
                tvDepositAccountName.setText(bankname);
            }
            String account = listTransferReceiptInfo.getCNTP_BANK_ACNO();
            if (!TextUtils.isEmpty(account)) {
                String CNTP_FIN_INST_CD = listTransferReceiptInfo.getCNTP_FIN_INST_CD();
                if ("000".equalsIgnoreCase(CNTP_FIN_INST_CD) || "028".equalsIgnoreCase(CNTP_FIN_INST_CD)) {
                    account = account.substring(0, 5) + "-" + account.substring(5, 7) + "-" + account.substring(7, account.length());
                }
                tvDepositAccount.setText(account);
            }
            strValue = listTransferReceiptInfo.getCNTP_ACCO_DEPR_NM();
            if (!TextUtils.isEmpty(strValue))
                tvDepositName.setText(strValue);
            strValue = listTransferReceiptInfo.getWTCH_ACCO_MRK_CNTN();
            if (!TextUtils.isEmpty(strValue))
                tvDisplayRecipient.setText(strValue);
            strValue = listTransferReceiptInfo.getMNRC_ACCO_MRK_CNTN();
            if (!TextUtils.isEmpty(strValue))
                tvDisplaySending.setText(strValue);

            container.addView(view);
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }
    }
}
