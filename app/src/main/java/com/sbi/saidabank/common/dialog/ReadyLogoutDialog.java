package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.LogoutTimeChecker;

public class ReadyLogoutDialog extends BaseDialog {

    private Context mContext;
    private TextView mTextReadyLogoutTime;

    public ReadyLogoutDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_ready_logout_time);
        setDialogWidth();
        mTextReadyLogoutTime = (TextView) findViewById(R.id.textview_ready_logout);
        Button btnReset = (Button) findViewById(R.id.btn_reset_logout);
        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LogoutTimeChecker.getInstance(mContext).autoLogoutRestart();
                dismiss();
            }
        });
    }

    public void updateTime(long remainTime) {
        mTextReadyLogoutTime.setText(String.valueOf(remainTime));
    }
}
