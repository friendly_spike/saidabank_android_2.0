package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.core.content.res.ResourcesCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

/**
 * 수취인확인 후 출력되는 확인 다이얼로그
 */

public class AlertRemitteeDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;

    public String msg;

    public String mNBtText = "";
    public String mPBtText = "";
    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;

    public boolean mIsSafeDeal;

    public AlertRemitteeDialog(Context context) {
        super(context);
        mContext = context;
        mIsSafeDeal = false;
    }

    public AlertRemitteeDialog(Context context, boolean isSafeDeal) {
        super(context);
        mContext = context;
        mIsSafeDeal = isSafeDeal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_transfer_alert);

        setDialogWidth();

        initView();
    }

    private void initView(){
        setCancelable(false);

        if (msg == null || "".equals(msg)) {
            msg = mContext.getString(R.string.common_no_msg);
        }

        ((TextView)findViewById(R.id.tv_msg)).setText(msg);
        
        Button mBtNegative = (Button)findViewById(R.id.bt_negative);
        Button mBtPositive = (Button)findViewById(R.id.bt_positive);
        mBtNegative.setOnClickListener(this);
        mBtPositive.setOnClickListener(this);

        if (mNBtText != null && !"".equals(mNBtText)) {
            mBtNegative.setText(mNBtText);
        }

        if (mPBtText != null && !"".equals(mPBtText)) {
            mBtPositive.setText(mPBtText);
        }

        if (mNListener == null) {
            mBtNegative.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        } else {
            mBtNegative.setBackgroundResource(R.drawable.selector_radius_left_cancelbtn);
            if(mIsSafeDeal){
                int radius = (int) Utils.dpToPixel(getContext(),5f);
                mBtPositive.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{0,0,radius,0}));
                mBtPositive.setTextColor(getContext().getResources().getColor(R.color.color00EBFF));
            }else{
                mBtPositive.setBackgroundResource(R.drawable.selector_radius_right_okbtn);
            }

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_negative:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }
                if(mNListener != null) {
                    mNListener.onClick(view);
                }
                break;

            case R.id.bt_positive:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                break;

            default:
                break;
        }
    }

}
