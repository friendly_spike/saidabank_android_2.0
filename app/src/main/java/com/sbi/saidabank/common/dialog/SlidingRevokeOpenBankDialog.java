package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;


public class SlidingRevokeOpenBankDialog extends SlidingBaseDialog implements View.OnClickListener{
    private Context  context;

    public View.OnClickListener listener;

    public SlidingRevokeOpenBankDialog(@NonNull Context context, View.OnClickListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.dialog_revoke_openbank);
        setDialogWidth();

        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancel:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }
                break;

            case R.id.btn_ok:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }

                if (listener != null) {
                    listener.onClick(v);
                }
                break;

            default:
                break;
        }
    }
}
