package com.sbi.saidabank.common;

import android.app.Activity;
import android.content.Context;

import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.WasServiceUrl;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * LogoutTimeChecker : 로그인 후에 일정 시간후 로그아웃 팝업을 출력하기 위한 타임 체커
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class LogoutTimeChecker {

    private static final int AUTO_LOGOUT_TIME = 10 * 60;
    private static final int READY_LOGOUT_TIME = 60;
    private static LogoutTimeChecker instance;
    private WeakReference<BaseActivity> mWeakActivity;
    private Timer autoLogoutTimer = null;
    private long interactionTime = 0;
    private boolean mIsBackground;
    private Context mContext;
    private LogoutTimeCheckerListener mListener;

    public interface LogoutTimeCheckerListener {
        void onReadyLogoutTime(long remainTime);
        void onEndLogoutTime(boolean isRestart);
    }

    public static LogoutTimeChecker getInstance(Context context) {
        if (instance == null) {
            synchronized (LogoutTimeChecker.class) {
                if (instance == null) {
                    instance = new LogoutTimeChecker(context);
                }
            }
        } else {
            instance.updateContext(context);
        }
        return instance;
    }

    public static void clearInstance() {
        if (instance == null)
            return;
        instance.autoLogoutStop();
        instance = null;
    }

    private LogoutTimeChecker(Context context) {
        this.mWeakActivity = new WeakReference<>((BaseActivity) context);
        this.mContext = context;
        this.mListener = (LogoutTimeCheckerListener) context;
        this.mIsBackground = false;
    }

    private void updateContext(Context context) {
        this.mWeakActivity = new WeakReference<>((BaseActivity) context);
        this.mContext = context;
        this.mListener = (LogoutTimeCheckerListener) context;
    }

    public void autoLogoutStart() {
        MLog.d();
        interactionTime = System.currentTimeMillis();
        MLog.i("interactionTime >> " + interactionTime);
        if (autoLogoutTimer == null) {
            autoLogoutTimer = new Timer();
            autoLogoutTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    final long duration = (System.currentTimeMillis() - interactionTime) / 1000;
                    BaseActivity weakActivity = mWeakActivity.get();
                    if (weakActivity != null) {
                        if (duration >= AUTO_LOGOUT_TIME) {
                            autoLogoutStop();
                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                public void run() {
                                    mListener.onEndLogoutTime(false);
                                }
                            });

                        } else if ((AUTO_LOGOUT_TIME - duration) < READY_LOGOUT_TIME) {
                            ((Activity) mContext).runOnUiThread(new Runnable() {
                                public void run() {
                                    mListener.onReadyLogoutTime(AUTO_LOGOUT_TIME - duration);
                                }
                            });
                        }
                    }

                }
            }, 0, 1000);
        }
    }

    public void autoLogoutRestart() {
        MLog.d();
        interactionTime = System.currentTimeMillis();
        mListener.onEndLogoutTime(true);
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), new HashMap(), new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                MLog.i("HttpRequest >> " + ret);
            }
        });
    }

    public void autoLogoutUserInteraction(boolean isShownLogoutPopup) {
        if (DataUtil.isNotNull(autoLogoutTimer) && !isShownLogoutPopup) {
            interactionTime = System.currentTimeMillis();
        }
    }

    public void autoLogoutStop() {
        MLog.d();
        try {
            interactionTime = 0;
            if (DataUtil.isNotNull(autoLogoutTimer)) {
                autoLogoutTimer.cancel();
                autoLogoutTimer = null;
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    public boolean isBackground() {
        return mIsBackground;
    }

    public void setBackground(boolean mIsBackground) {
        this.mIsBackground = mIsBackground;
    }
}
