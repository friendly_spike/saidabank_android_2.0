package com.sbi.saidabank.common.util;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;

import java.io.File;

/**
 * SingleMediaScanner : 갤러리 파일 스캔
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-08-05
 */
public class SingleMediaScanner implements MediaScannerConnection.MediaScannerConnectionClient {

    private MediaScannerConnection mConnection;

    private File mTargetFile;

    private onScannerProcess mListener;

    public interface onScannerProcess {
        void onScanCompleted(Uri uri);
    }

    public SingleMediaScanner(Context context, File targetFile) {
        this.mTargetFile = targetFile;
        mConnection = new MediaScannerConnection(context, this);
        mConnection.connect();
    }

    @Override
    public void onMediaScannerConnected() {
        mConnection.scanFile(mTargetFile.getAbsolutePath(), null);
    }

    @Override
    public void onScanCompleted(String path, Uri uri) {
        if (mListener != null)
            mListener.onScanCompleted(uri);
        mConnection.disconnect();
    }

    public void setListener(onScannerProcess listener) {
        mListener = listener;
    }

}
