package com.sbi.saidabank.common.download;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.text.TextUtils;


import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.test.NewApkDownloadActivity;
import com.sbi.saidabank.common.DialogUtil;
import com.sbi.saidabank.common.util.Files;
import com.sbi.saidabank.common.util.Logs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFileAsync extends AsyncTask<String, Integer, Boolean> {

	protected WeakReference<Activity> activityReference;
	protected String mSaveFilePath;
	protected String mSaveFileName;

	protected DownloadFileCallback mListener;

    public DownloadFileAsync(Activity context, DownloadFileCallback l){
		activityReference = new WeakReference<Activity>((Activity) context);
    	mListener = l;
    }

	public DownloadFileAsync(Activity context,int type, DownloadFileCallback l){
		activityReference = new WeakReference<Activity>((Activity) context);
		mListener = l;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}
	
	@Override
	protected Boolean doInBackground(String... param) {
		return startDownloadFile(param);
	}
	
	@Override
	protected void onCancelled(Boolean result) {
		Logs.e("onCancelled : " + result);
		super.onCancelled(result);
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		Activity context = activityReference.get();
		if(mListener != null && !context.isFinishing()){
			Logs.e("onProgressUpdate : " + values[0]);
			mListener.updateProgress(values[0]);
		}
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {

		Logs.e("onPostExecute : " + result);
		//다운로드가 실패하면 다운받던 파일을 지운다.
		if(!result){
			Files.deleteFile(mSaveFilePath + "/" + mSaveFileName);
		}

		Activity context = activityReference.get();
		if(context.isFinishing()) return;

		if(mListener != null)
			mListener.onDownloadComplete(result,mSaveFilePath,mSaveFileName);

		super.onPostExecute(result);
	}

	protected boolean startDownloadFile(String... param){
		Activity context = activityReference.get();
		try {
			String downUrl = param[0];
			String newFolder = Environment.DIRECTORY_DOWNLOADS;
			if(param.length > 1){
				newFolder = param[1];//다운로드할 폴더
			}
			Logs.e("Download url :" + downUrl);

			mSaveFilePath = context.getExternalFilesDir(newFolder).getPath();
			Logs.e("mFolderPath :" + mSaveFilePath);

			int pos = downUrl.lastIndexOf("/");
			mSaveFileName = downUrl.substring(pos + 1, downUrl.length());
			Logs.e("SaveFileName :" + mSaveFileName);

			File saveFile = new File(mSaveFilePath, mSaveFileName);
			Files.deleteFile(mSaveFilePath + "/" + mSaveFileName);

			URL url = new URL(downUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();

			if (conn != null) {
				conn.setRequestMethod("GET");
				conn.setDoInput(true);
				conn.setConnectTimeout(60 * 1000);

				if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
					/*
					 * 파일 다운로드를 시작한다.
					 */
					int mContentSize = conn.getContentLength();
					Logs.e("mContentSize :" + mContentSize);
					if(mContentSize <= 0) return false;

					InputStream mInputStream = new BufferedInputStream(url.openStream());
					OutputStream mOutputStream = new FileOutputStream(saveFile);

					int mReadCount=0;
					byte[] mDownloadBuffer = new byte[1024*20];
					int mTotalDownloadSize = 0;
					int mPercent=0;
					while ((mReadCount = mInputStream.read(mDownloadBuffer)) != -1) {

						mOutputStream.write(mDownloadBuffer, 0, mReadCount);

						mTotalDownloadSize = mTotalDownloadSize + mReadCount;
						int diff = (int) ((float) mTotalDownloadSize * 100 / (float) mContentSize);
						if (diff != mPercent) {
							mPercent = diff;
							publishProgress(mPercent);
						}
					}

					conn.disconnect();

					if(mTotalDownloadSize > 0)
						return true;
				} else {
					Logs.e("error - conn.getResponseCode : " + conn.getResponseCode());
				}
			}
		} catch (Exception e){
			Logs.printException(e);
		}
		return false;
	}
}
