package com.sbi.saidabank.common.util;

import android.content.Context;
import android.text.TextUtils;

import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Create on 20210113.
 * 여러군데서 자주 사용하는 사이다뱅크의 코드값을 조회하기 위한 클래스
 */
public class SaidaCodeUtil {

    /**
     * Http에서 리턴받은 코드문자열을 넣어주면 ArrayList형식으로 리턴한다.
     * @param arrayStr
     * @return
     */
    public static ArrayList<RequestCodeInfo> getCodeArrayList(String arrayStr){
        JSONObject object = null;
        ArrayList<RequestCodeInfo> mCodeArray = new ArrayList<RequestCodeInfo>();
        try {
            object = new JSONObject(arrayStr);
            JSONArray array = object.optJSONArray("REC");
            if (array == null) return null;

            for (int index = 0; index < array.length(); index++) {
                JSONObject jsonObject = array.getJSONObject(index);
                if (jsonObject == null)
                    continue;

                RequestCodeInfo item = new RequestCodeInfo(jsonObject);

                mCodeArray.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return mCodeArray;
    }

    /**
     * 코드 request 결과값에 에러가 있는지 체크한다.
     * @param ret
     * @return
     */
    public static boolean checkReqCodeError(String ret){
        if (TextUtils.isEmpty(ret)) {
            return true;
        }

        try {
            JSONObject object = new JSONObject(ret);
            if (object == null) {
                Logs.e(ret);
                return true;
            }

            JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
            if (objectHead == null) {
                return true;
            }

            String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
            if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                Logs.e("error msg : " + msg + ", ret : " + ret);
                return true;
            }
        } catch (JSONException e) {
            Logs.printException(e);
            return true;
        }

        return false;
    }

    /**
     * 사이다 코드 Http조회
     * @param param
     * @param listener
     */
    public static void requestCode(Map<String, Object> param,final HttpSenderTask.HttpRequestListener listener){
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010100A00.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                Logs.i("====== Saidabank code reqeust ======================");
                Logs.i("RequestCode - ret : " + ret);
                Logs.i("====================================================");
                if(listener != null){
                    listener.endHttpRequest(ret);
                }
            }
        });
    }



    /**
     * 은행 코드 조회
     */
    public static void requestBankList(final Context context) {
        requestBankList(context,null);
    }
    public static void requestBankList(final Context context,final HttpSenderTask.HttpRequestListener listener) {
        Map param = new HashMap();
        param.put("LCCD", "BANK_CD");

        requestCode(param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if(checkReqCodeError(ret)) return;

                Prefer.setBankList(context, ret);

                if(listener != null){
                    listener.endHttpRequest(ret);
                }
            }
        });
    }

    /**
     * 증권사 코드 조회
     */
    public static void requestStockList(final Context context) {
        requestStockList(context,null);
    }
    public static void requestStockList(final Context context, final HttpSenderTask.HttpRequestListener listener) {
        Map param = new HashMap();
        param.put("LCCD", "SCCM_CD");

        requestCode(param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                if(checkReqCodeError(ret)) return;

                Prefer.setStockList(context, ret);

                if(listener != null){
                    listener.endHttpRequest(ret);
                }
            }
        });
    }
}
