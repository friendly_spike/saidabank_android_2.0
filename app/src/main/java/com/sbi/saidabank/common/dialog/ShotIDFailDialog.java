package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.define.Const;

/**
 * Saidabank_android
 * Class: ShotIDFailDialog
 * Created by 950546 on 2019. 04. 10..
 * <p>
 * Description: 신분증 촬영 실패 다이얼로그
 */
public class ShotIDFailDialog extends BaseDialog {

    private Context mContext;
    private int mIdFailType;
    private ShotIDFailDialog.OnConfirmListener mListener;

    public interface OnConfirmListener {
        void onConfirmPress();
    }

    public void setOnCofirmListener(OnConfirmListener listener) {
        this.mListener = listener;
    }

    public ShotIDFailDialog(@NonNull Context context, int idFailType) {
        super(context);
        this.mContext = context;
        this.mIdFailType = idFailType;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((mIdFailType == Const.ID_SHOT_FAIL_SHOT) ? R.layout.dialog_id_shot_fail : R.layout.dialog_id_check_fail);
        setDialogWidth();
        findViewById(R.id.btn_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onConfirmPress();
                }
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
