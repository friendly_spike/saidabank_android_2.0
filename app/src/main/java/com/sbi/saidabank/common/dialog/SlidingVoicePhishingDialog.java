package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: SlidingCertDeleteDialog
 * Created by 950485 on 2018. 11. 30
 * <p>
 * Description:
 * 아래에서 위로 올라오는 보이싱피싱 안내 다이얼로그
 */
public class SlidingVoicePhishingDialog extends SlidingBaseDialog implements View.OnClickListener {
    private Context context;

    public View.OnClickListener nListener;
    public View.OnClickListener pListener;

    public SlidingVoicePhishingDialog(@NonNull Context context, View.OnClickListener nListener, View.OnClickListener pListener) {
        super(context);

        this.context = context;
        this.nListener = nListener;
        this.pListener = pListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_voice_phishing);
        setDialogWidth();

        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                if (nListener != null) {
                    nListener.onClick(v);
                }

                dismiss();

                break;

            case R.id.btn_ok:
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                if (pListener != null) {
                    pListener.onClick(v);
                }

                dismiss();

                break;

            default:
                break;
        }
    }
}
