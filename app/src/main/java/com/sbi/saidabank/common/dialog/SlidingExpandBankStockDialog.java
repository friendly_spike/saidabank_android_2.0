package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: SlidingExpandBankStockDialog
 * Created by 950485 on 2018. 11. 14..
 * <p>
 * Description:아래에서 위로 올라오는 은행/증권회사 다이얼로그.화면 사이즈를 늘릴수 있다.
 */

public class SlidingExpandBankStockDialog extends SlidingExpandBaseDialog{
    private static final int SHOW_LIST_BANK  = 0;
    private static final int SHOW_LIST_STOCK = 1;
    private static final int SHOW_LIST_CARD  = 2;

    private TextView         mTextTabBank;
    private TextView         mTextTabStock;
    private TextView         mTextTabCard;

    private ListView         mListView;
    private ListAdapter      mListAdapter;

    private Context      mContext;
    private int          mSelectTab;
    private ArrayList<RequestCodeInfo> mBankArray;
    private ArrayList<RequestCodeInfo> mStockArray;
    private ArrayList<RequestCodeInfo> mCardArray;
    private OnSelectBankStockListener  mBankStockSelectListener;



    public SlidingExpandBankStockDialog(@NonNull Context context,
                                        ArrayList<RequestCodeInfo> listBankArray,
                                        ArrayList<RequestCodeInfo> listStockArray,
                                        OnSelectBankStockListener bankClickListener) {
        super(context);
        this.mContext = context;

        mBankArray = new ArrayList<RequestCodeInfo>();
        mStockArray = new ArrayList<RequestCodeInfo>();
        mCardArray = new ArrayList<RequestCodeInfo>();

        this.mBankArray.addAll(listBankArray);
        this.mStockArray.addAll(listStockArray);
        this.mBankStockSelectListener = bankClickListener;
        mSelectTab = SHOW_LIST_BANK;
    }

    public SlidingExpandBankStockDialog(@NonNull Context context,
                                        ArrayList<RequestCodeInfo> listBankArray,
                                        ArrayList<RequestCodeInfo> listStockArray,
                                        ArrayList<RequestCodeInfo> listCardArray,
                                        OnSelectBankStockListener bankClickListener) {
        super(context);
        this.mContext = context;

        mBankArray = new ArrayList<RequestCodeInfo>();
        mStockArray = new ArrayList<RequestCodeInfo>();
        mCardArray = new ArrayList<RequestCodeInfo>();

        this.mBankArray.addAll(listBankArray);
        this.mStockArray.addAll(listStockArray);
        this.mCardArray.addAll(listCardArray);
        this.mBankStockSelectListener = bankClickListener;
        mSelectTab = SHOW_LIST_BANK;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_sliding_expand_bank_stock);

        setDialogWidth();

        //화면을 늘리기위한 기본 뷰를 셋팅한다.
        setDialogBody((LinearLayout) findViewById(R.id.layout_body));
        setExpandMoveBar(findViewById(R.id.layout_movebar));

        initUX();
    }

    private int mListBackupIndex=-1;
    private void initUX() {

        //백그라운드 터치시 다이얼로그 종료
        findViewById(R.id.v_background).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        mListAdapter = new ListAdapter();
        mListView= (ListView) findViewById(R.id.listview);
        mListView.setAdapter(mListAdapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mListView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Logs.e("onScrollChange - getFirstVisiblePosition : " + mListView.getFirstVisiblePosition());
                    Logs.e("onScrollChange - getLastVisiblePosition : " + mListView.getLastVisiblePosition());
                    Logs.e("onScrollChange - mListBackupIndex : " + mListBackupIndex);
                    if(mListBackupIndex < 0 || mListBackupIndex == mListView.getFirstVisiblePosition()){
                        mListBackupIndex = mListView.getFirstVisiblePosition();
                        return;
                    }
                    if(mListBackupIndex < mListView.getFirstVisiblePosition()){
                        Logs.e("draw MAX");
                        if(mListView.getLastVisiblePosition() > 13) {
                            resizeDialogMax();
                        }
                    }else{
                        Logs.e("draw ORIGIN");
                        if(mListView.getFirstVisiblePosition() < 3) {
                            resizeDialogOrigin();
                        }
                    }
                    mListBackupIndex = mListView.getFirstVisiblePosition();
                }
            });
        }


        mTextTabBank = (TextView) findViewById(R.id.textview_tab_bank);
        mTextTabStock = (TextView) findViewById(R.id.textview_tab_stock);
        mTextTabCard = (TextView) findViewById(R.id.textview_tab_card);
        if(DataUtil.isNotNull(mCardArray)&& mCardArray.size() > 0){
            mTextTabCard.setVisibility(View.VISIBLE);
        }


        mTextTabBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectTab = SHOW_LIST_BANK;
                clickTabText();
            }
        });

        mTextTabStock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectTab = SHOW_LIST_STOCK;
                clickTabText();
            }
        });

        mTextTabCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectTab = SHOW_LIST_CARD;
                clickTabText();
            }
        });
    }

    private void clickTabText(){
        mTextTabBank.setBackgroundResource(R.drawable.background_tab_unselect_box);
        mTextTabBank.setTextColor(mContext.getResources().getColor(R.color.color666666));

        mTextTabStock.setBackgroundResource(R.drawable.background_tab_unselect_box);
        mTextTabStock.setTextColor(mContext.getResources().getColor(R.color.color666666));

        mTextTabCard.setBackgroundResource(R.drawable.background_tab_unselect_box);
        mTextTabCard.setTextColor(mContext.getResources().getColor(R.color.color666666));

        switch (mSelectTab){
            case SHOW_LIST_BANK:
                mTextTabBank.setBackgroundResource(R.drawable.background_tab_select_box);
                mTextTabBank.setTextColor(mContext.getResources().getColor(R.color.black));
                break;
            case SHOW_LIST_STOCK:
                mTextTabStock.setBackgroundResource(R.drawable.background_tab_select_box);
                mTextTabStock.setTextColor(mContext.getResources().getColor(R.color.black));
                break;
            case SHOW_LIST_CARD:
                mTextTabCard.setBackgroundResource(R.drawable.background_tab_select_box);
                mTextTabCard.setTextColor(mContext.getResources().getColor(R.color.black));
                break;
        }
        mListAdapter.notifyDataSetChanged();
        mListView.setSelection(0);
    }

    class ListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            switch (mSelectTab){
                case SHOW_LIST_BANK:
                    return mBankArray.size();
                case SHOW_LIST_STOCK:
                    return mStockArray.size();
                case SHOW_LIST_CARD:
                    return mCardArray.size();
            }
            return 0;
        }

        @Override
        public Object getItem(int position) {

            switch (mSelectTab){
                case SHOW_LIST_BANK:
                    return mBankArray.get(position);
                case SHOW_LIST_STOCK:
                    return mStockArray.get(position);
                case SHOW_LIST_CARD:
                    return mCardArray.get(position);
            }
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewBankHolder;

            if (convertView == null) {
                viewBankHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.dialog_sliding_expand_bankstock_list_item, null);
                viewBankHolder.layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_bankstock_item);
                viewBankHolder.imageIcon = (ImageView) convertView.findViewById(R.id.imageview_bankstock);
                viewBankHolder.textName = (TextView) convertView.findViewById(R.id.textview_bankstock);
                convertView.setTag(viewBankHolder);
            } else {
                viewBankHolder = (ViewHolder) convertView.getTag();
            }

            RequestCodeInfo itemInfo=null;
            switch (mSelectTab){
                case SHOW_LIST_BANK:
                    itemInfo = mBankArray.get(position);
                    break;
                case SHOW_LIST_STOCK:
                    itemInfo = mStockArray.get(position);
                    break;
                case SHOW_LIST_CARD:
                    itemInfo = mCardArray.get(position);
                    break;
            }

            final String name = itemInfo.getCD_NM();
            final String code = itemInfo.getSCCD();

            if (itemInfo != null) {
                if (!TextUtils.isEmpty(name))
                    viewBankHolder.textName.setText(name);

                Uri iconUri = ImgUtils.getIconUri(mContext,code);
                if(iconUri != null)
                    viewBankHolder.imageIcon.setImageURI(iconUri);
                else
                    viewBankHolder.imageIcon.setImageResource(R.drawable.img_logo_xxx);

            }

            viewBankHolder.layoutItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mContext instanceof Activity && ((Activity) mContext).isFinishing())
                        return;

                    mBankStockSelectListener.onSelectListener(name, code);
                    dismiss();
                }
            });

            return convertView;
        }

        class ViewHolder {
            LinearLayout layoutItem;
            ImageView    imageIcon;
            TextView     textName;
        }
    }

    public interface OnSelectBankStockListener {
        void onSelectListener(String bank, String code);
    }
}
