package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;

public class FidoDialog extends BaseDialog {

    public View.OnClickListener mPListener;
    public Dialog.OnDismissListener mDismissListener;
    public Dialog.OnDismissListener mBackKeyListener;
    private Context mContext;
    private LinearLayout mCautionMsg;
    private boolean mIsCancelBtn;
    private boolean mIsComplete;

    public FidoDialog(Context context, boolean isCancelBtn) {
        super(context);
        this.mContext = context;
        this.mIsCancelBtn = isCancelBtn;
        this.mIsComplete = false;
    }

    public FidoDialog(Context context, boolean isCancelBtn,boolean isComplete) {
        super(context);
        this.mContext = context;
        this.mIsCancelBtn = isCancelBtn;
        this.mIsComplete = isComplete;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_fido);

        mCautionMsg = findViewById(R.id.layout_fingerprint_msg);
        Button btnPositive = (Button) findViewById(R.id.btn_finger_ok);
        Button btnNegative = (Button) findViewById(R.id.btn_finger_cancel);

        if (mIsCancelBtn) {
            btnNegative.setVisibility(View.VISIBLE);
            btnNegative.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickEvent(v);
                }
            });
            btnPositive.setVisibility(View.GONE);
        } else {
            btnNegative.setVisibility(View.GONE);
            btnPositive.setVisibility(View.VISIBLE);
            btnPositive.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickEvent(v);
                }
            });
        }


        if(mIsComplete){
            findViewById(R.id.iv_icon).setVisibility(View.VISIBLE);
            findViewById(R.id.lottie_icon).setVisibility(View.GONE);
            ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.ico_bio_check_l);
        }else{
            //1분간 지문인식을 하지 않으면 다이얼로그 닫도록 한다.
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if(FidoDialog.this.isShowing()){
//                        if(DataUtil.isNotNull(mBackKeyListener))
//                            mBackKeyListener.onDismiss(FidoDialog.this);
//
//                        dismiss();
//                    }
//                }
//            }, 60*1000);
//            findViewById(R.id.iv_icon).setVisibility(View.GONE);
//            findViewById(R.id.lottie_icon).setVisibility(View.VISIBLE);
        }

        setDialogWidth();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (DataUtil.isNotNull(mBackKeyListener)) {
            mBackKeyListener.onDismiss(this);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (DataUtil.isNotNull(mDismissListener)) {
            mDismissListener.onDismiss(this);
        }
    }

    public void showFidoCautionMsg(boolean show) {
        mCautionMsg.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void onClickEvent(View v) {
        if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
            dismiss();
        }
        if (DataUtil.isNotNull(mPListener)) {
            mPListener.onClick(v);
        }
    }
}
