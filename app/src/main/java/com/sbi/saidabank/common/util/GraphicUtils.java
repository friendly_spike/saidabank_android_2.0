package com.sbi.saidabank.common.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.DisplayMetrics;

public class GraphicUtils {
    /**
     * 뷰의 둥근 배경 화면을 만든다.
     *
     * @param bgColor           #FFFFFF
     * @param cornerRadius      int[]
     *
     *     1                  2
     *       ----------------
     *      |                |
     *      |                |
     *       ----------------
     *     4                  3
     *
     * strokeWidth              int
     * strokeColor              #FFFFFF
     * @return
     */


    public static Drawable getRoundCornerDrawable(String bgColor, int[] cornerRadius){
        int bg_color = Color.parseColor(bgColor);
        return getRoundCornerDrawable(bg_color,cornerRadius,0,0);
    }


    public static Drawable getRoundCornerDrawable(int bgColor, int[] cornerRadius){
        return getRoundCornerDrawable(bgColor,cornerRadius,0,0);
    }

    public static Drawable getRoundCornerDrawable(String bgColor, int[] cornerRadius, int strokeWidth, String strokeColor){
        int bg_color = Color.parseColor(bgColor);
        int storke_color=0;
        if(!TextUtils.isEmpty(strokeColor)){
            storke_color = Color.parseColor(strokeColor);
        }

        return getRoundCornerDrawable(bg_color,cornerRadius,strokeWidth,storke_color);
    }

    public static Drawable getRoundCornerDrawable(int bgColor, int[] cornerRadius, int strokeWidth, int strokeColor){
        GradientDrawable gdDefault = new GradientDrawable();

        gdDefault.setColor(bgColor);


        //뷰의 각 코너를 시계방향으로 이동하면서 둥글게 처리한다.
        if(cornerRadius != null && cornerRadius.length > 0){
            float[] arrayCornerRadius= {cornerRadius[0],cornerRadius[0],cornerRadius[1],cornerRadius[1],cornerRadius[2],cornerRadius[2],cornerRadius[3],cornerRadius[3]};
            gdDefault.setCornerRadii(arrayCornerRadius);
        }

        if(strokeWidth > 0)
            gdDefault.setStroke(strokeWidth, strokeColor);

        return gdDefault;
    }

    /**
     * 네비게이션 키가 포함된 화면 전체 높이를 가져온다.
     * @param context
     * @return
     */
    public static int getLCDHeight(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((Activity)context).getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        return metrics.heightPixels;
    }

    /**
     * Statusbar가 포함된 액티비티의 화면 높이를 가져온다.
     * @param context
     * @return
     */
    public static int getActivityHeight(Context context) {
//        DisplayMetrics metrics = new DisplayMetrics();
//        ((Activity)context).getWindowManager().getDefaultDisplay().getMetrics(metrics);
//        return metrics.heightPixels;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        return metrics.heightPixels;

    }

    public static int getActivityWidth(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();

        return metrics.widthPixels;
    }

    public static int getStatusBarHeight(Context context) {
        int	result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }

        return result;
    }

    public static int getNavigationBarHeight(Context context) {
        int usableHeight = getActivityHeight(context);
        int realHeight = getLCDHeight(context);

        if (realHeight > usableHeight)
            return realHeight - usableHeight;
        else
            return 0;
    }
}
