package com.sbi.saidabank.common.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.sbi.saidabank.R;

public class TutorialAdapter extends PagerAdapter {
    private Context    context;

    private final int[] drawableImgs = new int[] {
            R.drawable.img_tutorial_01,
            R.drawable.img_tutorial_02,
            R.drawable.img_tutorial_03,
            R.drawable.img_tutorial_04,
            R.drawable.img_tutorial_05,
            R.drawable.img_tutorial_06,
            R.drawable.img_tutorial_07,
            R.drawable.img_tutorial_08
    };
/*
    private final int[] contentDescription = new int[]{

            R.string.tutorial_accessibility_str1,
            R.string.tutorial_accessibility_str2,
            R.string.tutorial_accessibility_str3,
            R.string.tutorial_accessibility_str4,
            R.string.tutorial_accessibility_str5,
            R.string.tutorial_accessibility_str6,
            R.string.tutorial_accessibility_str7

    };
*/
    public TutorialAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return drawableImgs.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.tutorial_item, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageview_tutorial);

        WindowManager wm = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);
        //imageView.setContentDescription(context.getString(contentDescription[position]));

        Bitmap drawImg = BitmapFactory.decodeResource(context.getResources(), drawableImgs[position]);
        imageView.setImageBitmap(drawImg);

        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
