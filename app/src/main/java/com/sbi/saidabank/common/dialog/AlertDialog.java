package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;


public class AlertDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;

    public String msg;

    public String mNBtText = "";
    public String mPBtText = "";
    public String mSubText = "";
    public String mTopText = "";

    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;
    private OnDismissListener   _dismissListener;

    public AlertDialog(Context context) {
        super(context);
        mContext = context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_alert);
        setCancelable(false);

        setDialogWidth();

        initView();
    }

    private void initView(){


        if (DataUtil.isNull(msg)) {
            msg = mContext.getString(R.string.common_no_msg);
        }

        if (msg.contains("font color")||msg.contains("<br>") || msg.contains("<b>")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ((TextView) findViewById(R.id.tv_msg)).setText(Html.fromHtml(msg, Html.FROM_HTML_MODE_COMPACT));
            } else {
                ((TextView) findViewById(R.id.tv_msg)).setText(Html.fromHtml(msg));
            }
        } else {
            ((TextView) findViewById(R.id.tv_msg)).setText(msg);
        }

        if (!TextUtils.isEmpty(mTopText)) {
            ((TextView) findViewById(R.id.tv_topmsg)).setText(mTopText);
            findViewById(R.id.tv_topmsg).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.tv_topmsg).setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(mSubText)) {
            ((TextView) findViewById(R.id.tv_submsg)).setText(mSubText);
            findViewById(R.id.tv_submsg).setVisibility(View.VISIBLE);
        }else{
            findViewById(R.id.tv_submsg).setVisibility(View.GONE);
        }

        Button mBtNegative = (Button) findViewById(R.id.bt_negative);
        mBtNegative.setOnClickListener(this);

        Button mBtPositive = (Button) findViewById(R.id.bt_positive);
        mBtPositive.setOnClickListener(this);

        if (DataUtil.isNotNull(mNBtText)) {
            mBtNegative.setText(mNBtText);
        }

        if (DataUtil.isNotNull(mPBtText)) {
            mBtPositive.setText(mPBtText);
        }

        if (DataUtil.isNull(mNListener)) {
            mBtNegative.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        } else {
            mBtNegative.setBackgroundResource(R.drawable.selector_radius_left_cancelbtn);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_right_okbtn);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_negative:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (_dismissListener != null)
                        _dismissListener.onDismiss();
                    dismiss();
                }
                if(mNListener != null) {
                    mNListener.onClick(view);
                }
                break;
            case R.id.bt_positive:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (_dismissListener != null)
                        _dismissListener.onDismiss();
                    dismiss();
                }
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (_dismissListener != null)
            _dismissListener.onDismiss();
    }

    public void setOnDismissListener(OnDismissListener listener) {
        _dismissListener = listener;
    }

    public interface OnDismissListener {
        void onDismiss();
    }

}
