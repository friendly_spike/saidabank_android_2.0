package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: SlidingCompleteChangeDialog
 * Created by 950485 on 2019. 01. 10..
 * <p>
 * Description:아래에서 위로 올라오는 수정 확인 다이얼로그
 */

public class SlidingCompleteChangeDialog extends SlidingBaseDialog {
    private Context   context;
    private String    description;
    private FinishListener finishListener;

    public SlidingCompleteChangeDialog(@NonNull Context context, String description, FinishListener finishListener) {
        super(context);
        this.context = context;
        this.description = description;
        this.finishListener = finishListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_complete_change);
        setDialogWidth();

        initUX();
    }

    private void initUX() {
        TextView textDesc = (TextView) findViewById(R.id.textview_desc_complete_change);
        textDesc.setText(description);

        Button btnCancel = (Button) findViewById(R.id.btn_cancel_complete_change);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnCancelListener();
                dismiss();
            }
        });

        Button btnOK = (Button) findViewById(R.id.btn_ok_complete_change);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnOKListener();
                dismiss();
            }
        });
    }

    public interface FinishListener {
        void OnCancelListener();
        void OnOKListener();
    }
}
