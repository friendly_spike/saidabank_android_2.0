package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.NumberPicker;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.customview.DatePicker;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabank_android
 * Class: SlidingDateTimerPickerDialog
 * Created by 950546 on 2018. 11. 26..
 * <p>
 * Description: 아래에서 위로 올라오는 날짜/시간 선택 다이얼로그
 */
public class SlidingDateTimerPickerDialog extends Dialog implements View.OnClickListener, NumberPicker.OnValueChangeListener {

    private Context mContext;
    private SlidingDateTimerPickerDialog.OnConfirmListener _listener;
    private SlidingDateTimerPickerDialog.OnWebCallConfirmListener _webListener;
    private NumberPicker mNpTime;
    private NumberPicker mNpCycle;
    private NumberPicker mNpCycleData;
    private NumberPicker mNpCustomData;

    private String[] mCycle = {"매월", "매주", "매일"};
    private String[] mCycleMonth = new String[31];
    private String[] mCycleWeek = {"일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"};
    private String[] mCycleDaily = {" "};

    private int mPickertype;
    private boolean mIstoday;
    private boolean mHasDaily;
    private boolean mHasWeek;
    private boolean mIsDelayService;
    private JSONArray mCustomData;
    private JSONArray mCustomDataArray;
    private String mStartDate;
    private String mEndDate;
    private String mSelectedDate;
    private String mSelectedData;
    private String mSelectedCycle;
    private String mSelectedCode;
    private int mCurYear;
    private int mCurMon;
    private int mCurDay;
    private static int mCurSelectYear;
    private static int mCurSelectMon;
    private static int mCurSelectDay;

    /**
     * 네이티브 호출. 이체 예약 날짜, 시간 선택
     *
     * @param context    context
     * @param pickerType PICKER_TYPE_DATE, PICKER_TYPE_TIME
     * @param isToday    PICKER_TYPE_TIME인 경우 true면 오늘, false면 그외 날짜
     */
    public SlidingDateTimerPickerDialog(@NonNull Context context, int pickerType, boolean isToday, boolean delayService) {
        super(context);
        mContext = context;
        mPickertype = pickerType;
        mIsDelayService = delayService;
        mIstoday = isToday;
    }

    /**
     * 웹 호출. 이체 주기 선택
     *
     * @param context    context
     * @param pickerType PICKER_TYPE_DATE_CYCLE_WEB_YM
     * @param hasDaily   매일 주기 포함 여부
     * @param hasWeek    매주 주기 포함 여부
     */
    public SlidingDateTimerPickerDialog(@NonNull Context context, int pickerType, boolean hasMon, boolean hasDaily, boolean hasWeek, JSONObject selectedCycle) {
        super(context);
        mContext = context;
        mPickertype = pickerType;
        mHasDaily = hasDaily;
        mHasWeek = hasWeek;

        if (selectedCycle != null) {
            mSelectedCycle = selectedCycle.optString("cycle");
            mSelectedCode = selectedCycle.optString("code");
        }
    }

    /**
     * 웹 호출. custom data 선택
     *
     * @param context    context
     * @param pickerType PICKER_TYPE_CUSTOM_DATA_WEB
     * @param customData custom data
     */
    public SlidingDateTimerPickerDialog(@NonNull Context context, int pickerType, JSONArray customData, String selectedData) {
        super(context);
        mContext = context;
        mPickertype = pickerType;
        mCustomData = customData;
        mSelectedData = selectedData;
    }

    /**
     * 웹 호출. 년월일, 년월 선택
     *
     * @param context    context
     * @param pickertype PICKER_TYPE_DATE_WEB_YMD, PICKER_TYPE_DATE_WEB_YM
     * @param startDate  시작날짜
     * @param endDate    종료날짜
     */
    public SlidingDateTimerPickerDialog(@NonNull Context context, int pickertype, String startDate, String endDate, String seletedDate) {
        super(context);
        mContext = context;
        mPickertype = pickertype;
        mStartDate = startDate;
        mEndDate = endDate;
        mSelectedDate = seletedDate;
    }

    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setGravity(Gravity.BOTTOM);

        setContentView(R.layout.dialog_datetimepicker);

        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        getWindow().setAttributes(lp);
        lp.width = size.x;

        initSetting();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_cancel: {
                if (mNListener != null) {
                    mNListener.onClick(v);
                }
                dismiss();
                break;
            }

            case R.id.btn_confirm: {
                if (mPickertype == Const.PICKER_TYPE_DATE_WEB_YMD) {
                    Logs.e("mCurSelectYear : " + mCurSelectYear);
                    Logs.e("mCurSelectMon : " + (mCurSelectMon + 1));
                    Logs.e("mCurSelectDay : " + mCurSelectDay);
                    if (_webListener != null)
                        _webListener.onConfirmPress(mCurSelectYear, mCurSelectMon + 1, mCurSelectDay, 0, 0, null, null);
                } else if (mPickertype == Const.PICKER_TYPE_DATE_WEB_YM) {
                    Logs.e("mCurSelectYear : " + mCurSelectYear);
                    Logs.e("mCurSelectMon : " + (mCurSelectMon + 1));
                    if (_webListener != null)
                        _webListener.onConfirmPress(mCurSelectYear, mCurSelectMon + 1, 0, 0, 0, null, null);
                } else if (mPickertype == Const.PICKER_TYPE_DATE) {
                    if (mIstoday) {
                        if (mCurYear != mCurSelectYear || mCurMon != mCurSelectMon || mCurDay != mCurSelectDay)
                            mIstoday = false;
                    }
                    if (_listener != null)
                        _listener.onConfirmPress(0, 0, 0, 0, true, mIstoday, mIsDelayService);
                } else if (mPickertype == Const.PICKER_TYPE_DATE_CYCLE_WEB_YM) {
                    Logs.e("이체주기 : " + mNpCycle.getDisplayedValues()[mNpCycle.getValue()]);
                    Logs.e("이체주기일 : " + mNpCycleData.getDisplayedValues()[mNpCycleData.getValue()]);
                    int curIndex = (mNpCycle.getValue() == 1) ? mNpCycleData.getValue() : mNpCycleData.getValue() + 1;
                    if (_webListener != null)
                        _webListener.onConfirmPress(mCurSelectYear, mCurSelectMon + 1, 0, mNpCycle.getValue(), curIndex, null, null);
                } else if (mPickertype == Const.PICKER_TYPE_TIME) {
                    String strCurtime = mNpTime.getDisplayedValues()[mNpTime.getValue()];
                    int time = Integer.parseInt(strCurtime.substring(0, strCurtime.length() - 1));
                    if (_listener != null)
                        _listener.onConfirmPress(mCurSelectYear, mCurSelectMon + 1, mCurSelectDay, time, false, false, false);
                } else if (mPickertype == Const.PICKER_TYPE_CUSTOM_DATA_WEB) {
                    if (_webListener != null) {
                        try {
                            JSONObject obj = mCustomDataArray.getJSONObject(mNpCustomData.getValue());
                            String code = obj.optString("code");
                            String name = obj.optString("name");
                            _webListener.onConfirmPress(0, 0, 0, 0, 0, code, name);
                        } catch (JSONException e) {
                            //e.printStackTrace();
                        }
                    }
                }

                dismiss();
                break;
            }

            default:
                break;
        }
    }

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

        if (newVal == 0) {        // 매월
            mNpCycle.setValue(0);
            mNpCycleData.setDisplayedValues(null);
            mNpCycleData.setMaxValue(30);
            mNpCycleData.setDisplayedValues(mCycleMonth);
            if ("01".equals(mSelectedCycle) && !TextUtils.isEmpty(mSelectedCode)) {
                int selectedday = Integer.parseInt(mSelectedCode) - 1;
                if (selectedday <= 0)
                    selectedday = 0;
                mNpCycleData.setValue(selectedday);
            } else {
                Calendar calendar = Calendar.getInstance();
                int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
                mNpCycleData.setValue(dayOfMonth - 1);
            }
        } else if (newVal == 1) { // 매주
            mNpCycle.setValue(1);
            mNpCycleData.setDisplayedValues(null);
            mNpCycleData.setMaxValue(6);
            mNpCycleData.setDisplayedValues(mCycleWeek);
            if ("07".equals(mSelectedCycle) && !TextUtils.isEmpty(mSelectedCode)) {
                int selectedday = Integer.parseInt(mSelectedCode);
                if (selectedday <= 0)
                    selectedday = 0;
                mNpCycleData.setValue(selectedday);
            } else {
                Calendar calendar = Calendar.getInstance();
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                mNpCycleData.setValue(dayOfWeek - 1);
            }
        } else {                  // 매일
            mNpCycle.setValue(2);
            mNpCycleData.setDisplayedValues(null);
            mNpCycleData.setMaxValue(0);
            mNpCycleData.setDisplayedValues(mCycleDaily);
            mNpCycleData.setValue(0);
        }
    }

    /**
     * 변수, UI 초기화
     */
    private void initSetting() {
        Calendar calendar = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();

        findViewById(R.id.btn_confirm).setOnClickListener(this);

        switch (mPickertype) {
            case Const.PICKER_TYPE_DATE_WEB_YMD: {
                findViewById(R.id.ll_date).setVisibility(View.VISIBLE);
                findViewById(R.id.ll_time).setVisibility(View.GONE);
                findViewById(R.id.ll_cycle).setVisibility(View.GONE);
                FrameLayout layoutDatePicker = findViewById(R.id.ll_datepicker);
                DatePicker datePicker = new DatePicker(layoutDatePicker, R.style.NumberPicker, true, getWindowWidth());

                if (!TextUtils.isEmpty(mStartDate)) {
                    mStartDate = mStartDate.replaceAll("[^0-9]", "");
                    int startYear = Integer.parseInt(mStartDate.substring(0, 4));
                    int startMon = Integer.parseInt(mStartDate.substring(4, 6));
                    int startDay = Integer.parseInt(mStartDate.substring(6, 8));
                    minDate.set(startYear, startMon - 1, startDay);
                } else {
                    minDate.set(1950, 0, 1);
                }

                if (!TextUtils.isEmpty(mEndDate)) {
                    mEndDate = mEndDate.replaceAll("[^0-9]", "");
                    int endYear = Integer.parseInt(mEndDate.substring(0, 4));
                    int endMon = Integer.parseInt(mEndDate.substring(4, 6));
                    int endDay = Integer.parseInt(mEndDate.substring(6, 8));
                    maxDate.set(endYear, endMon - 1, endDay);
                } else {
                    maxDate.set(2100, 11, 31);
                }
                datePicker.setMaxDate(maxDate.getTimeInMillis());
                datePicker.setMinDate(minDate.getTimeInMillis());

                if (TextUtils.isEmpty(mSelectedDate)) {
                    mCurSelectYear = datePicker.getYear();
                    mCurSelectMon = datePicker.getMonth();
                    mCurSelectDay = datePicker.getDayOfMonth();
                } else {
                    mSelectedDate = mSelectedDate.replaceAll("[^0-9]", "");
                    mCurSelectYear = Integer.parseInt(mSelectedDate.substring(0, 4));
                    mCurSelectMon = Integer.parseInt(mSelectedDate.substring(4, 6)) - 1;
                    mCurSelectDay = Integer.parseInt(mSelectedDate.substring(6, 8));
                }

                datePicker.init(mCurSelectYear, mCurSelectMon, mCurSelectDay, new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Logs.e("year : " + year);
                        Logs.e("monthOfYear : " + monthOfYear);
                        Logs.e("dayOfMonth : " + dayOfMonth);
                        mCurSelectYear = year;
                        mCurSelectMon = monthOfYear;
                        mCurSelectDay = dayOfMonth;
                    }
                });
                break;
            }

            case Const.PICKER_TYPE_DATE_WEB_YM: {
                findViewById(R.id.ll_date).setVisibility(View.VISIBLE);
                findViewById(R.id.ll_time).setVisibility(View.GONE);
                findViewById(R.id.ll_cycle).setVisibility(View.GONE);
                FrameLayout layoutDatePicker = findViewById(R.id.ll_datepicker);
                DatePicker datePicker = new DatePicker(layoutDatePicker, R.style.NumberPicker, false, getWindowWidth());

                if (!TextUtils.isEmpty(mStartDate)) {
                    mStartDate = mStartDate.replaceAll("[^0-9]", "");
                    int startYear = Integer.parseInt(mStartDate.substring(0, 4));
                    int startMon = Integer.parseInt(mStartDate.substring(4, 6));
                    int startDay = 1;
                    minDate.set(startYear, startMon - 1, startDay);
                } else {
                    minDate.set(1950, 0, 1);
                }
                if (!TextUtils.isEmpty(mEndDate)) {
                    mEndDate = mEndDate.replaceAll("[^0-9]", "");
                    int endYear = Integer.parseInt(mEndDate.substring(0, 4));
                    int endtMon = Integer.parseInt(mEndDate.substring(4, 6));
                    int startDay = 1;
                    maxDate.set(endYear, endtMon - 1, startDay);
                } else {
                    maxDate.set(2100, 11, 31);
                }
                datePicker.setMaxDate(maxDate.getTimeInMillis());
                datePicker.setMinDate(minDate.getTimeInMillis());

                if (TextUtils.isEmpty(mSelectedDate)) {
                    mCurSelectYear = datePicker.getYear();
                    mCurSelectMon = datePicker.getMonth();
                    mCurSelectDay = 1;
                } else {
                    mSelectedDate = mSelectedDate.replaceAll("[^0-9]", "");
                    mCurSelectYear = Integer.parseInt(mSelectedDate.substring(0, 4));
                    mCurSelectMon = Integer.parseInt(mSelectedDate.substring(4, 6)) - 1;
                    mCurSelectDay = 1;
                }

                datePicker.init(mCurSelectYear, mCurSelectMon, mCurSelectDay, new DatePicker.OnDateChangedListener() {
                    @Override
                    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Logs.e("year1 : " + year);
                        Logs.e("monthOfYear1 : " + monthOfYear);
                        mCurSelectYear = year;
                        mCurSelectMon = monthOfYear;
                    }
                });
                break;
            }

            case Const.PICKER_TYPE_CUSTOM_DATA_WEB: {
                findViewById(R.id.ll_date).setVisibility(View.GONE);
                findViewById(R.id.ll_time).setVisibility(View.VISIBLE);
                findViewById(R.id.ll_cycle).setVisibility(View.GONE);
                mNpCustomData = findViewById(R.id.np_time);
                setSelectionDivider(mContext, mNpCustomData);
                mNpCustomData.setMinValue(0);
                mCustomDataArray = new JSONArray();
                for (int i = 0; i < mCustomData.length(); i++) {
                    try {
                        JSONObject obj = mCustomData.getJSONObject(i);
                        //if (!TextUtils.isEmpty(obj.optString("code"))) {
                            JSONObject tmpObj = new JSONObject();
                            tmpObj.put("code", obj.optString("code"));
                            tmpObj.put("name", obj.optString("name"));
                            mCustomDataArray.put(tmpObj);
                        //}
                    } catch (JSONException e) {
                        //e.printStackTrace();
                    }
                }
                mNpCustomData.setMaxValue(mCustomDataArray.length() - 1);
                String[] customData = new String[mCustomDataArray.length()];
                int selectedIndex = 0;
                for (int i = 0; i < mCustomDataArray.length(); i++) {
                    try {
                        JSONObject obj = mCustomDataArray.getJSONObject(i);
                        customData[i] = obj.optString("name");
                        if (!TextUtils.isEmpty(mSelectedData) && mSelectedData.equals(obj.optString("code")))
                            selectedIndex = i;

                    } catch (JSONException e) {
                        //e.printStackTrace();
                    }
                }

                mNpCustomData.setDisplayedValues(customData);
                mNpCustomData.setValue(selectedIndex);
                break;
            }

            case Const.PICKER_TYPE_DATE: {
                findViewById(R.id.ll_date).setVisibility(View.VISIBLE);
                findViewById(R.id.ll_time).setVisibility(View.GONE);
                findViewById(R.id.ll_cycle).setVisibility(View.GONE);

                getServerTime();
                break;
            }

            case Const.PICKER_TYPE_DATE_CYCLE_WEB_YM: {
                findViewById(R.id.ll_date).setVisibility(View.GONE);
                findViewById(R.id.ll_time).setVisibility(View.GONE);
                findViewById(R.id.ll_cycle).setVisibility(View.VISIBLE);

                mNpCycle = findViewById(R.id.np_cycle);
                setSelectionDivider(mContext, mNpCycle);
                mNpCycleData = findViewById(R.id.np_cycledata);
                setSelectionDivider(mContext, mNpCycleData);

                mNpCycle.setMinValue(0);
                mNpCycleData.setMinValue(0);

                int i = 0;

                for (i = 0; i < 31; i++)
                    mCycleMonth[i] = i + 1 + "일";

                if (mHasDaily) {
                    mNpCycle.setMaxValue(2);
                } else if (!mHasWeek){
                    mNpCycle.setMaxValue(0);
                } else {
                    mNpCycle.setMaxValue(1);
                }

                mNpCycle.setDisplayedValues(mCycle);
                mNpCycle.setOnValueChangedListener(this);
                mNpCycleData.setDisplayedValues(mCycleMonth);
                mNpCycleData.setMaxValue(30);

                if (!TextUtils.isEmpty(mSelectedCycle) && !TextUtils.isEmpty(mSelectedCode)) {
                    if ("01".equals(mSelectedCycle)) {
                        int selectedday = Integer.parseInt(mSelectedCode) - 1;
                        if (selectedday <= 0)
                            selectedday = 0;
                        mNpCycleData.setValue(selectedday);
                    } else if ("07".equals(mSelectedCycle)) {
                        mNpCycle.setValue(1);
                        mNpCycleData.setDisplayedValues(null);
                        mNpCycleData.setMaxValue(6);
                        mNpCycleData.setDisplayedValues(mCycleWeek);
                        int selectedday = Integer.parseInt(mSelectedCode);
                        if (selectedday <= 0)
                            selectedday = 0;
                        mNpCycleData.setValue(selectedday);
                    } else if ("99".equals(mSelectedCycle)) {
                        mNpCycle.setValue(2);
                        mNpCycleData.setDisplayedValues(null);
                        mNpCycleData.setMaxValue(0);
                        mNpCycleData.setDisplayedValues(mCycleDaily);
                        mNpCycleData.setValue(0);
                    }
                } else if (!TextUtils.isEmpty(mSelectedCycle)) {
                   if ("99".equals(mSelectedCycle)) {
                        mNpCycle.setValue(2);
                        mNpCycleData.setDisplayedValues(null);
                        mNpCycleData.setMaxValue(0);
                        mNpCycleData.setDisplayedValues(mCycleDaily);
                        mNpCycleData.setValue(0);
                    }
                }
                break;
            }

            case Const.PICKER_TYPE_TIME: {
                findViewById(R.id.ll_date).setVisibility(View.GONE);
                findViewById(R.id.ll_time).setVisibility(View.VISIBLE);
                findViewById(R.id.ll_cycle).setVisibility(View.GONE);
                mNpTime = findViewById(R.id.np_time);
                setSelectionDivider(mContext, mNpTime);
                mNpTime.setMinValue(0);
                String[] mTime;

                // 당일이체면 현재 시간부터 설정
                if (mIstoday) {
                    calendar.getTimeInMillis();
                    // 매시 30분 초과 체크
                    int timelistcount = 0;
                    int firttime = 0;

                    if (mIsDelayService) {
                        if ((calendar.get(Calendar.HOUR_OF_DAY) + 3) >= 23) {
                            Logs.showToast(mContext, "시간 설정 오류입니다.");
                            dismiss();
                            return;
                        } else {
                            timelistcount = 23 - calendar.get(Calendar.HOUR_OF_DAY) + 3;
                            firttime = calendar.get(Calendar.HOUR_OF_DAY) + 4;
                        }
                    } else {
                        if (calendar.get(Calendar.MINUTE) > 30) {
                            // 이체시간 불가 시간(23시 ~ 0시) 체크
                            if (calendar.get(Calendar.HOUR_OF_DAY) >= 22) {
                                Logs.showToast(mContext, "시간 설정 오류입니다.");
                                dismiss();
                                return;
                            }

                            timelistcount = 22 - calendar.get(Calendar.HOUR_OF_DAY);
                            firttime = calendar.get(Calendar.HOUR_OF_DAY) + 2;
                        } else {
                            // 이체시간 불가 시간(23시 ~ 0시) 체크
                            if (calendar.get(Calendar.HOUR_OF_DAY) >= 23) {
                                Logs.showToast(mContext, "시간 설정 오류입니다.");
                                dismiss();
                                return;
                            }

                            Logs.e("calendar.get(Calendar.HOUR_OF_DAY) : " + calendar.get(Calendar.HOUR_OF_DAY));

                            timelistcount = 23 - calendar.get(Calendar.HOUR_OF_DAY);
                            firttime = calendar.get(Calendar.HOUR_OF_DAY) + 1;
                        }
                    }

                    mTime = new String[timelistcount];
                    mNpTime.setMaxValue(timelistcount - 1);
                    for (int i = 0; i < timelistcount; i++) {
                        mTime[i] = (i + firttime) + "시";
                    }
                }
                // 당일이 아니면 1시 ~ 23시로 설정
                else {
                    mNpTime.setMaxValue(22);
                    mTime = new String[23];
                    for (int i = 0; i < 23; i++) {
                        mTime[i] = i + 1 + "시";
                    }
                }

                mNpTime.setDisplayedValues(mTime);
                break;
            }

            default:
                break;
        }
    }

    /**
     * divider의 컬러와 간격, 두께 조정
     *
     * @param context
     * @param numberpicker year, month, day picker
     */
    private void setSelectionDivider(Context context, NumberPicker numberpicker) {
        Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        int dividercolor;
        int distance = (int) context.getResources().getDimension(R.dimen.numberpicker_divider_distance);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dividercolor = context.getColor(R.color.colorE5E5E5);
        } else {
            dividercolor = ContextCompat.getColor(context, R.color.colorE5E5E5);
        }

        Logs.e("distance : " + (int) context.getResources().getDimension(R.dimen.numberpicker_divider_distance));

        for (Field pf : pickerFields) {
            // divider color
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(dividercolor);
                    pf.set(numberpicker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    Logs.printException(e);
                } catch (Resources.NotFoundException e) {
                    Logs.printException(e);
                } catch (IllegalAccessException e) {
                    Logs.printException(e);
                }
            }
            // divider distance
            if (pf.getName().equals("mSelectionDividersDistance")) {
                pf.setAccessible(true);
                try {
                    Logs.i("deviders distance : " + pf.getInt(numberpicker));
                    //pf.setInt(numberpicker, distance);
                } catch (IllegalArgumentException e) {
                    Logs.printException(e);
                } catch (Resources.NotFoundException e) {
                    Logs.printException(e);
                } catch (IllegalAccessException e) {
                    Logs.printException(e);
                }
            }
            // divider height
            if (pf.getName().equals("mSelectionDividerHeight")) {
                pf.setAccessible(true);
                try {
                    Logs.i("deviders height : " + pf.getInt(numberpicker));
                    //pf.setInt(numberpicker, 1);
                } catch (IllegalArgumentException e) {
                    Logs.printException(e);
                } catch (Resources.NotFoundException e) {
                    Logs.printException(e);
                } catch (IllegalAccessException e) {
                    Logs.printException(e);
                }
            }
        }
    }

    private void getServerTime() {
        Map param = new HashMap();
        ((BaseActivity) mContext).showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011500A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                ((BaseActivity) mContext).dismissProgressDialog();
                Logs.i("CMM0011500A01 : " + ret);
                if (TextUtils.isEmpty(ret)) {
                    Logs.e(mContext.getResources().getString(R.string.msg_debug_no_response));
                }

                try {
                    Calendar calendar = Calendar.getInstance();
                    Calendar minDate = Calendar.getInstance();
                    if (!TextUtils.isEmpty(ret)) {
                        JSONObject object = new JSONObject(ret);
                        if (object == null) {
                            Logs.e(mContext.getResources().getString(R.string.msg_debug_err_response));
                            Logs.e(ret);
                        }

                        JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                        if (objectHead == null) {
                              return;
                        }

                        String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                        if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                            String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
                            if (TextUtils.isEmpty(msg))
                                msg = mContext.getResources().getString(R.string.common_msg_no_reponse_value_was);

                            Logs.e("error msg : " + msg + ", ret : " + ret);
                        }

                        String result = object.optString("SYS_DTTM");
                        Logs.i("result : " + result);

                        if (!TextUtils.isEmpty(result)) {
                            calendar.set(Integer.parseInt(result.substring(0, 4)), Integer.parseInt(result.substring(4, 6)) - 1, Integer.parseInt(result.substring(6, 8)));
                        }
                    }

                    // 22시 30분이 넘으면 당일 예약 이체가 불가하므로 다음날부터 이체되도록 함
                    if (!mIsDelayService) {
                        if (calendar.get(Calendar.HOUR_OF_DAY) >= 22 && calendar.get(Calendar.MINUTE) > 29) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    } else {
                        if (calendar.get(Calendar.HOUR_OF_DAY) + 3 >= 23) {
                            calendar.setTimeInMillis(calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 1000.0));
                            mIstoday = false;
                        } else {
                            mIstoday = true;
                        }
                    }

                    FrameLayout layoutDatePicker = findViewById(R.id.ll_datepicker);
                    DatePicker datePicker = new DatePicker(layoutDatePicker, R.style.NumberPicker, true, getWindowWidth());
                    minDate.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                    long tmp = calendar.getTimeInMillis() + (long) (60 * 60 * 24 * 90 * 1000.0);
                    datePicker.setMinDate(minDate.getTimeInMillis());
                    datePicker.setMaxDate(tmp);
                    mCurYear = datePicker.getYear();
                    mCurMon = datePicker.getMonth();
                    mCurDay = datePicker.getDayOfMonth();
                    mCurSelectYear = mCurYear;
                    mCurSelectMon = mCurMon;
                    mCurSelectDay = mCurDay;

                    datePicker.init(mCurSelectYear, mCurSelectMon, mCurSelectDay, new DatePicker.OnDateChangedListener() {
                        @Override
                        public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                            Logs.e("year1 : " + year);
                            Logs.e("monthOfYear1 : " + monthOfYear);
                            Logs.e("dayOfMonth1 : " + dayOfMonth);
                            mCurSelectYear = year;
                            mCurSelectMon = monthOfYear;
                            mCurSelectDay = dayOfMonth;
                        }
                    });
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 화면 가로사이즈 가져오기
     *
     * @return 화면 가로사이즈
     */
    private int getWindowWidth() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        return size.x;
    }

    //확인버튼 누를 때 호출한 액티비티로 날짜, 시간값 리턴
    public void setOnConfirmListener(SlidingDateTimerPickerDialog.OnConfirmListener listener) {
        _listener = listener;
    }

    public void setOnWebCallConfirmListener(SlidingDateTimerPickerDialog.OnWebCallConfirmListener listener) {
        _webListener = listener;
    }

    public interface OnConfirmListener {
        void onConfirmPress(int year, int month, int day, int time, boolean needTimeSelect, boolean isToday, boolean isDelayService);
    }

    public interface OnWebCallConfirmListener {
        void onConfirmPress(int year, int month, int day, int selectedType, int selectedIndex, String code, String name);
    }
}
