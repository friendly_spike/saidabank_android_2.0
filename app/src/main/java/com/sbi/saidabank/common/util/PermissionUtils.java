package com.sbi.saidabank.common.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 * PermissionUtils : 퍼미션 체크 관련 유틸
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class PermissionUtils {

    public static boolean checkPermission(Activity activity, String[] pList, int requestCode) {
        for (int i = 0; i < pList.length; i++) {
            int perStatus = ContextCompat.checkSelfPermission(activity, pList[i]);
            if (perStatus == PackageManager.PERMISSION_DENIED) {
                requestPermission(activity, pList, requestCode);
                return false;
            }
        }
        return true;
    }

    public static boolean checkPermission(Activity activity, String[] pList) {
        for (int i = 0; i < pList.length; i++) {
            int perStatus = ContextCompat.checkSelfPermission(activity, pList[i]);
            if (perStatus == PackageManager.PERMISSION_DENIED) {
                return false;
            }
        }
        return true;
    }

    public static void requestPermission(Activity activity, String[] pList, int requestCode) {
        MLog.d();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(activity, pList, requestCode);
        }
    }

    public static void goAppSettingsActivity(Context context) {
        MLog.d();
        if (DataUtil.isNotNull(context)) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(Uri.parse("package:" + context.getPackageName()));
            context.startActivity(intent);
        }
    }
}
