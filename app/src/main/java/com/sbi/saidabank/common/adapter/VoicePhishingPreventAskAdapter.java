package com.sbi.saidabank.common.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.dialog.SlidingVoicePhishingPreventAskDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * VoicePhishingPreventAskAdapter : 보이스피싱 문진 팝업 어댑터뷰
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-11-04
 */
public class VoicePhishingPreventAskAdapter extends RecyclerView.Adapter {

    /** 뷰타입 */
    static final int HEADER = 0;
    static final int LIST = HEADER + 1;
    static final int BOTTOM = LIST + 2;

    /** 문진 개수 (추후 항목 추가시 mTitleList 개수와 맞춤) */
    static final int ASK_CNT = 4;

    /** 생성자 */
    private Context mContext;

    private SlidingVoicePhishingPreventAskDialog.OnItemListener mListener;

    /** 문진 제목 */
    private ArrayList<String> mTitleList;

    /** 문진 체크 값 */
    private ArrayList<String> mChkList;

    public VoicePhishingPreventAskAdapter(Context context, SlidingVoicePhishingPreventAskDialog.OnItemListener listener) {
        this.mContext = context;
        this.mListener = listener;
        this.mTitleList = Utils.getStringArray(mContext.getResources().getStringArray(R.array.voice_phishing_prevent_ask));
        this.mChkList = new ArrayList<>();
        for (int i=0; i < mTitleList.size(); i++) {
            mChkList.add(Const.EMPTY);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = null;
        switch (viewType) {
            case HEADER:
                view = LayoutInflater.from(mContext).inflate(R.layout.row_voice_phishing_prevent_ask_header_item, viewGroup, false);
                return new HeaderViewHolder(view);
            case BOTTOM:
                view = LayoutInflater.from(mContext).inflate(R.layout.row_voice_phishing_prevent_ask_bottom_item, viewGroup, false);
                return new BottomViewHolder(view);
            case LIST:
            default:
                view = LayoutInflater.from(mContext).inflate(R.layout.row_voice_phishing_prevent_ask_list_item, viewGroup, false);
                return new ListViewHolder(view);
        }
    }

    @Override
    public int getItemCount() {
        return ASK_CNT + 2;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case HEADER:
                return HEADER;
            case (ASK_CNT + 1):
                return BOTTOM;
            default:
                return LIST;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ListViewHolder) {
            ListViewHolder holder = (ListViewHolder) viewHolder;
            holder.setAskItem(position - 1);
        } else if (viewHolder instanceof BottomViewHolder) {
            BottomViewHolder holder = (BottomViewHolder) viewHolder;
            holder.setBtnConfirm();
        }
    }
// ==============================================================================================================
// VIEW HOLDER
// ==============================================================================================================
    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        public HeaderViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {

        TextView mTvTitle;
        ImageView mIvYes;
        ImageView mIvNo;
        LinearLayout mLlYesArea;
        LinearLayout mLlNoArea;
        int mPos;

        public ListViewHolder(View itemView) {
            super(itemView);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_ask_title);
            mLlYesArea = (LinearLayout) itemView.findViewById(R.id.ll_ask_yes_area);
            mIvYes = (ImageView) itemView.findViewById(R.id.iv_ask_radio_yes);
            mLlNoArea = (LinearLayout) itemView.findViewById(R.id.ll_ask_no_area);
            mIvNo = (ImageView) itemView.findViewById(R.id.iv_ask_radio_no);
        }

        private void setAskItem(int pos) {
            mTvTitle.setText(mTitleList.get(pos));
            mLlYesArea.setTag(pos);
            mLlYesArea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPos = (Integer) v.getTag();
                    setToggleBtn(true);
                    mChkList.set(mPos, Const.REQUEST_WAS_YES);
                    notifyDataSetChanged();
                }
            });
            mLlNoArea.setTag(pos);
            mLlNoArea.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mPos = (Integer) v.getTag();
                    setToggleBtn(false);
                    mChkList.set(mPos, Const.REQUEST_WAS_NO);
                    notifyDataSetChanged();
                }
            });
        }

        private void setToggleBtn(boolean chk) {
            mIvYes.setBackgroundResource(chk ? R.drawable.btn_radio_on : R.drawable.btn_radio_off);
            mIvNo.setBackgroundResource(chk ? R.drawable.btn_radio_off : R.drawable.btn_radio_on);
        }
    }

    public class BottomViewHolder extends RecyclerView.ViewHolder {

        TextView mTvCaution;
        TextView mTvCall;
        Button mBtnConfirm;

        public BottomViewHolder(View itemView) {
            super(itemView);
            mTvCaution = (TextView) itemView.findViewById(R.id.tv_voice_phishing_03);
            setCautionText(mContext.getResources().getString(R.string.msg_voice_phishing_03));
            mTvCall = (TextView) itemView.findViewById(R.id.tv_call_voice_phishing);
            mTvCall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Utils.makeCall((Activity) mContext, "16445277");
                }
            });
            mBtnConfirm = (Button) itemView.findViewById(R.id.btn_ok_alert_voice_phishing);
            mBtnConfirm.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SimpleDateFormat")
                @Override
                public void onClick(View v) {
                    if (DataUtil.isNotNull(mListener)) {
                        // 문진 팝업 확인날짜를 로컬 스토리지에 저장 후 이체하기 화면으로 이동
                        Prefer.setPrefVoicePhishingAskPopupDate(mContext, new SimpleDateFormat("yyyyMMdd").format(new Date()));
                        mListener.onConfirm(isValidate(false));
                    }
                }
            });
        }

        /**
         * 주의 문구를 설정
         *
         * @param str
         * */
        private void setCautionText(String str) {
            int index = str.lastIndexOf(mContext.getResources().getString(R.string.msg_voice_phishing_03_1));
            SpannableStringBuilder sb = new SpannableStringBuilder(str);
            sb.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.color009BEB)), index, str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            sb.setSpan(new StyleSpan(Typeface.NORMAL), index, str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            mTvCaution.setText(sb);
        }

        /**
         * 확인 버튼 설정
         * */
        private void setBtnConfirm() {
            mBtnConfirm.setEnabled(isValidate(true));
        }

        /**
         * 유효성 체크
         *
         * @return
         * */
        private boolean isValidate(boolean type) {
            boolean validate = true;
            for (String s : mChkList) {
                if (type ? s.equals(Const.EMPTY) : s.equals(Const.REQUEST_WAS_YES)) {
                    validate = false;
                    break;
                }
            }
            return validate;
        }
    }
}
