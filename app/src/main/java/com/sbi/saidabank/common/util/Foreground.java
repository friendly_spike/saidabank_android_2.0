package com.sbi.saidabank.common.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Foreground : 포그라운드 화면 감지
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-02-27
 */
public class Foreground implements Application.ActivityLifecycleCallbacks {

    private static final long CHECK_DELAY = 500;
    private boolean mForeground = false;
    private boolean mPaused = true;
    private static Foreground instance;
    private Handler handler = new Handler();
    private static Map<String, Activity> allActivity;
    private Runnable check;
    private List<Listener> listeners = new CopyOnWriteArrayList<>();

    public interface Listener {
        void onForeground();
        void onBackground();
    }

    public static Foreground init(Application application) {
        if (DataUtil.isNull(instance)) {
            instance = new Foreground();
            application.registerActivityLifecycleCallbacks(instance);
            allActivity = new HashMap<>();
        }
        return instance;
    }

    public static Foreground get(Application application) {
        if (DataUtil.isNull(instance))
            init(application);
        return instance;
    }

    public static Foreground get(Context context) {
        if (DataUtil.isNull(instance)) {
            Context mContext = context.getApplicationContext();
            if (mContext instanceof Application) {
                init((Application) mContext);
            }
            throw new IllegalStateException("Foreground is not initialised and " + "cannot obtain the Application object");
        }
        return instance;
    }

    public static Foreground get() {
        if (DataUtil.isNull(instance))
            throw new IllegalStateException("Foreground is not initialised - invoke " + "at least once with parameterised init/get");
        return instance;
    }

    public boolean isForeground() {
        return mForeground;
    }

    public boolean isBackground() {
        return !mForeground;
    }

    public void addListener(Listener listener) {
        if (DataUtil.isNotNull(listeners)) {
            listeners.add(listener);
        }
    }

    public void removeListener(Listener listener) {
        if (DataUtil.isNotNull(listeners)) {
            listeners.remove(listener);
        }
    }

    @Override
    public void onActivityResumed(Activity activity) {
        mPaused = false;
        boolean wasBackground = !mForeground;
        mForeground = true;
        if (check != null)
            handler.removeCallbacks(check);
        if (wasBackground) {
            for (Listener l : listeners) {
                try {
                    l.onForeground();
                } catch (Exception e) {
                    MLog.e(e);
                }
            }
        }
    }

    @Override
    public void onActivityPaused(Activity activity) {
        mPaused = true;
        if (check != null)
            handler.removeCallbacks(check);
        check = new Runnable() {
            @Override
            public void run() {
                if (mForeground && mPaused) {
                    mForeground = false;
                    for (Listener l : listeners) {
                        try {
                            l.onBackground();
                        } catch (Exception e) {
                            MLog.e(e);
                        }
                    }
                }
            }
        };
        handler.postDelayed(check, CHECK_DELAY);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        if (DataUtil.isNotNull(allActivity)) {
            allActivity.put(activity.toString(), activity);
        }
    }

    @Override
    public void onActivityStarted(Activity activity) { }

    @Override
    public void onActivityStopped(Activity activity) { }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) { }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (DataUtil.isNotNull(allActivity)) {
            allActivity.remove(activity.toString());
        }
    }

    public void unregisterActivity(Application application) {
        if (DataUtil.isNull(instance)) {
            application.unregisterActivityLifecycleCallbacks(instance);
        }
    }

    public Map<String, Activity> getAllActivity() {
        return allActivity;
    }
}
