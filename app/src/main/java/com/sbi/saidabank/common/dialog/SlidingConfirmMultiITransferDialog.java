package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.transaction.TransferManager;
import com.sbi.saidabank.activity.transaction.adater.ITransferComfirmDialogMultiListAdapter;
import com.sbi.saidabank.activity.transaction.adater.TransferMultiListAdapter;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.TransferVerifyInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class SlidingConfirmMultiITransferDialog extends Dialog {

    private TextView mTvTransferCnt;
    private TextView mTvTransferTotAmount;
    private TextView mTvTransferWithdrawAccount;
    private String mWithdrawAccount;
    private Double dLimitOneDay = -1.0;
    private TransferManager mTransferManager;
    private ITransferComfirmDialogMultiListAdapter multiTransferListAdapter = null;
    private boolean isLastRemove = false;   // 마지막 리스트 삭제 여부 - '아니요' 선택 시, 마지막 항목 삭제하면 안됨
    private boolean isRemove = false;       // 아이템 삭제 여부
    private Context mContext;
    private FinishListener mFinishListener;

    public interface FinishListener {
        void OnCancelListener(boolean isLastRemove, Double dLimitOneDay, boolean isAllRemove);
        void OnOKListener(boolean isLastRemove);
    }

    public SlidingConfirmMultiITransferDialog(@NonNull Context context, String withdrawAccount, FinishListener finishListener) {
        super(context, R.style.SlideDialog);
        this.mContext = context;
        this.mWithdrawAccount = withdrawAccount;
        this.mFinishListener = finishListener;
        this.mTransferManager = TransferManager.getInstance();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        setContentView(R.layout.dialog_confirm_multi_itransfer);
        initLayout();
    }
// ==============================================================================================================
// UI
// ==============================================================================================================
    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {
        mTvTransferCnt = (TextView) findViewById(R.id.textview_transfer_num_multi);
        mTvTransferTotAmount = (TextView) findViewById(R.id.textview_transfer_amount_multi);
        mTvTransferWithdrawAccount = (TextView) findViewById(R.id.textview_withdraw_account_multi);

        Button mBtnCancel = (Button) findViewById(R.id.btn_cancel_confirm_multi_transfer);
        mBtnCancel.setOnClickListener(onCancelClickListener);

        Button mBtnOK = (Button) findViewById(R.id.btn_ok_confirm_multi_transfer);
        mBtnOK.setOnClickListener(onOkClickListener);

        RecyclerView recyclerViewReceipt = (RecyclerView) findViewById(R.id.recyclerview_receiptInfo);
        if (DataUtil.isNull(multiTransferListAdapter)) {
            multiTransferListAdapter = new ITransferComfirmDialogMultiListAdapter(mContext);
            recyclerViewReceipt.setAdapter(multiTransferListAdapter);
            recyclerViewReceipt.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
            recyclerViewReceipt.setHasFixedSize(true);
        }
        updateTitleInfo();

    }

    /**
     * 이체 타이틀 정보를 업데이트 한다.
     * */
    private void updateTitleInfo() {
        mTvTransferCnt.setText(String.valueOf(ITransferDataMgr.getInstance().getRemitteInfoArraySize()));
        mTvTransferTotAmount.setText(String.format(mContext.getString(R.string.msg_ask_comfirm_transfer), Utils.moneyFormatToWon(ITransferDataMgr.getInstance().getTotalTranSumAmount())));
        mTvTransferWithdrawAccount.setText(DataUtil.isNotNull(mWithdrawAccount) ? mWithdrawAccount : Const.EMPTY);
    }

// ==============================================================================================================
// LISTENER
// ==============================================================================================================
    /**
     * 취소 클릭 이벤트 리스너
     */
    private View.OnClickListener onCancelClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View v) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            if (DataUtil.isNotNull(mFinishListener)) {
                mFinishListener.OnCancelListener(isLastRemove, dLimitOneDay, false);
            }
            dismiss();
        }
    };

    /**
     * 이체 클릭 이벤트 리스너
     */
    private View.OnClickListener onOkClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View v) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            if (DataUtil.isNotNull(mFinishListener)) {
                mFinishListener.OnOKListener(isLastRemove);
            }
            dismiss();
        }
    };

}
