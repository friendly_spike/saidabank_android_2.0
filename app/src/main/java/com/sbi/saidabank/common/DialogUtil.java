package com.sbi.saidabank.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.dialog.AlertDialog;
import com.sbi.saidabank.common.dialog.ErrorDialog;
import com.sbi.saidabank.common.dialog.FidoDialog;
import com.sbi.saidabank.common.dialog.SlidingExpandBankStockDialog;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.util.ArrayList;


public class DialogUtil {

	/**
	 * =====================================
	 * Alert, Confirm Dialog
	 * =====================================
	 */
	private static AlertDialog mAlertDialog;

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param msgId   메세지 문자열ID
	 */
	public static void alert(Context context, int msgId) {
		alert(context, context.getString(msgId));
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param msg     메세지 문자열
	 */
	public static void alert(Context context, String msg) {
		alert(context, null, msg, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title   타이틀 문자열
	 * @param msg     메세지 문자열
	 */
	public static void alert(Context context, String title, String msg) {
		alert(context, title, msg, null);
	}

	/**
	 * @param context
	 * @param titleId 타이틀 문자열ID
	 * @param msgId   메세지 문자열ID
	 */
	public static void alert(Context context, int titleId, int msgId) {
		alert(context, context.getString(titleId), context.getString(msgId));
	}

	/**
	 * @param context
	 * @param msgId     메세지 문자열ID
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, int msgId, View.OnClickListener pListener) {
		alert(context, context.getString(R.string.common_confirm), "", context.getString(msgId), pListener, null);
	}

	/**
	 * @param context
	 * @param pId       버튼 문자열ID
	 * @param msg       메세지 문자열
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, int pId, String msg, View.OnClickListener pListener) {
		alert(context, context.getString(pId), "", msg, pListener, null);
	}

	/**
	 * @param context
	 * @param msg       메세지 문자열
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, String msg, View.OnClickListener pListener) {
		alert(context, context.getString(R.string.common_confirm), "", msg, pListener, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title     타이틀 문자열
	 * @param msg       메세지 문자열
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg, View.OnClickListener pListener) {
		alert(context, title, context.getString(R.string.common_confirm), "", msg, pListener, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title     타이틀 문자열
	 * @param msg       메세지 문자열
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg, String pBtn, View.OnClickListener pListener) {
		alert(context, title, pBtn, "", msg, pListener, null);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param msg       메세지 문자열ID
	 * @param pListener 확인버튼 콜백 리스너
	 * @param nListener 취소버튼 콜백 리스너
	 */
	public static void alert(Context context, int msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, context.getString(R.string.common_confirm), context.getString(R.string.common_cancel), context.getString(msg), pListener, nListener);
	}

	/**
	 * @param context
	 * @param msg
	 * @param pListener
	 * @param nListener
	 */
	public static void alert(Context context, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, context.getString(R.string.common_confirm), context.getString(R.string.common_cancel), msg, pListener, nListener);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param title     타이틀 문자열
	 * @param msg       메세지 문자열
	 * @param pListener 확인버튼 콜백 리스너
	 * @param nListener 취소버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, title, context.getString(R.string.common_confirm), context.getString(R.string.common_cancel), msg, pListener, nListener);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param pBtn      positive 버튼 문자열
	 * @param nBtn      nagative 버튼 문자열
	 * @param msg       메세지 문자열
	 * @param pListener positive 버튼 콜백 리스너
	 * @param nListener nagative 버튼 콜백 리스터
	 */
	public static void alert(Context context, String pBtn, String nBtn, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, null, pBtn, nBtn, msg, pListener, nListener);
	}

	/**
	 * Alert, Confirm Dialog 구성함수
	 *
	 * @param context
	 * @param title     타이틀 문자열
	 * @param pBtn      Positive 버튼 문자열
	 * @param nBtn      nagative 버튼 문자열
	 * @param msg       메세지 문자열
	 * @param pListener positive 버튼 콜백 리스너
	 * @param nListener nagative 버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String pBtn, String nBtn, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {

		//액티비티가 종료되었으면 여기서 중지 한다.
		if (context instanceof Activity && ((Activity) context).isFinishing()) return;

		mAlertDialog = new AlertDialog(context);

		if (!TextUtils.isEmpty(msg)) {
			mAlertDialog.msg = msg;
		}

		if (!TextUtils.isEmpty(nBtn)) {
			mAlertDialog.mNBtText = nBtn;
		}

		if (!TextUtils.isEmpty(pBtn)) {
			mAlertDialog.mPBtText = pBtn;
		}

		if (nListener != null) {
			mAlertDialog.mNListener = nListener;
		}

		if (pListener != null) {
			mAlertDialog.mPListener = pListener;
		}

		if (context instanceof Activity && !((Activity) context).isFinishing()) {
			//201230-간혹 Thread내에서 알럿메세지 박스를 출력하려고 할때가 있어서 여기서 한번에 처리하도록 하자.
			((Activity) context).runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mAlertDialog.show();
				}
			});
		}
	}

	/**
	 * =====================================
	 * Error Dialog
	 * =====================================
	 */
	private static ErrorDialog mErrorDialog;

	/**
	 * Error Dialog 구성 함수
	 *
	 * @param context
	 * @param title
	 * @param msg
	 * @param pBtn
	 * @param pListener
	 */
	public static void error(Context context, String title, String msg, String pBtn, View.OnClickListener pListener) {

		mErrorDialog = new ErrorDialog(context);
/*
		if(title != null && !"".equals(title)){
			mErrorDialog.title = title;
		}
*/
		if (msg != null && !"".equals(msg)) {
			mErrorDialog.msg = msg;
		}

		if (pBtn != null && !"".equals(pBtn)) {
			mErrorDialog.mPBtText = pBtn;
		}

		if (pListener != null) {
			mErrorDialog.mPListener = pListener;
		}

		if (context instanceof Activity && !((Activity) context).isFinishing()) {
			mErrorDialog.show();
		}
	}

	/**
	 * Fido Dialog 구성 함수
	 *
	 * @param context
	 * @param isCancelBtn
	 * @param pListener
	 */
	public static FidoDialog fido(Context context, boolean isCancelBtn, View.OnClickListener pListener, Dialog.OnDismissListener dListener) {
		return fido(context,isCancelBtn,false,pListener,dListener);
	}

	public static FidoDialog fido(Context context, boolean isCancelBtn, boolean isComplete, View.OnClickListener pListener, Dialog.OnDismissListener dListener) {

		FidoDialog fidoDialog = new FidoDialog(context, isCancelBtn,isComplete);


		if (pListener != null) {
			fidoDialog.mPListener = pListener;
		}

		if (dListener != null) {
			fidoDialog.mDismissListener = dListener;
		}

		if (context instanceof Activity && !((Activity) context).isFinishing()) {
			fidoDialog.show();
		}

		return fidoDialog;
	}

	/**
	 * 은행 코드 출력하는 다이얼로그
	 *
	 * @param context
	 * @param listener
	 * @return
	 */
	public static SlidingExpandBankStockDialog bankStock(Context context, SlidingExpandBankStockDialog.OnSelectBankStockListener listener){
		return bankStock(context,false,listener);
	}
	/**
	 * 은행 코드 출력하는 다이얼로그
	 *
	 * @param context
	 * @param exeptSbi   SBI저축은행 리스트에서 제외여여부
	 * @param listener
	 * @return
	 */
	public static SlidingExpandBankStockDialog bankStock(Context context, boolean exeptSbi, SlidingExpandBankStockDialog.OnSelectBankStockListener listener){
		if (context instanceof Activity && ((Activity) context).isFinishing()) return null;

		//20210113 - Intro에서 은행/증권사 리소스 다운로드 받을때 코드도 받아서 저장해 둔다.
		String bankArrayStr = Prefer.getBankList(context);
		String stockArrayStr = Prefer.getStockList(context);

		if (TextUtils.isEmpty(bankArrayStr) || TextUtils.isEmpty(stockArrayStr)){
			alert(context,"은행/증권사 코드를 가져올 수 없습니다.");
			return null;
		}

		ArrayList<RequestCodeInfo> bankList = SaidaCodeUtil.getCodeArrayList(bankArrayStr);
		ArrayList<RequestCodeInfo> stockList = SaidaCodeUtil.getCodeArrayList(stockArrayStr);

		if(DataUtil.isNull(bankList) || DataUtil.isNull(stockList)){
			alert(context,"은행/증권사 코드를 가져올 수 없습니다.");
			return null;
		}

		//타행이체확인시 자사가 리스트에 나오면 않되기에 제외하는 코드 추가.
		if(exeptSbi){
			for(int i=bankList.size()-1;i>=0;i--){
				RequestCodeInfo item = bankList.get(i);
				//타행이체인 경우 SBI은행은 리스트에서 제외
				if ("028".equals(item.getSCCD()) || "000".equals(item.getSCCD())){
					bankList.remove(i);
				}
			}
		}

		SlidingExpandBankStockDialog dialog = new SlidingExpandBankStockDialog(context, bankList, stockList,listener);
		dialog.show();

		return dialog;
	}

}
