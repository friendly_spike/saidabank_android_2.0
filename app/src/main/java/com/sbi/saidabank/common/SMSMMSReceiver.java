package com.sbi.saidabank.common;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.common.util.Logs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

public class SMSMMSReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, Intent intent) {
		Logs.i("SMSMMSReceiver - intent.getAction() : " + intent.getAction());

		//Registration ID를 받음.
		if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {

			Bundle bundle = intent.getExtras();
			Object messages[] = (Object[]) bundle.get("pdus");
			SmsMessage smsMessage[] = new SmsMessage[messages.length];

			for (int i = 0; i < messages.length; i++) {
				// PDU 포맷으로 되어 있는 메시지를 복원합니다.
				smsMessage[i] = SmsMessage.createFromPdu((byte[]) messages[i]);
			}

			// SMS 수신 시간 확인
			Date curDate = new Date(smsMessage[0].getTimestampMillis());
			Logs.i("문자 수신 시간 : " + curDate.toString());

			// SMS 발신 번호 확인
			String origNumber = smsMessage[0].getOriginatingAddress();
			Logs.i("문자 발신 번호 : " + origNumber);
			if (TextUtils.isEmpty(origNumber))
				return;

			//SBI저축은행 콜센터 전화번호만 체크한다.
			if (!origNumber.contains("16700042"))
				return;

			// SMS 메시지 확인
			String message = smsMessage[0].getMessageBody().toString();
			Logs.e("문자 내용", "발신자 : " + origNumber + ", 내용 : " + message);


			String authCode = getSMSAuthCode(message);
			Logs.i("authCode : " + authCode);
			putIntoEditText(context, authCode);

		}else if(intent.getAction().equals("android.provider.Telephony.WAP_PUSH_RECEIVED")){

			try {
				//삼성폰이 DB에 저장하는데 약 2초가 걸림. 그래서 3초 딜레이줌.
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				Logs.printException(e);
			}

			ContentResolver contentResolver = context.getContentResolver();
			final String[] projection = new String[] { "_id" };
			Uri uri = Uri.parse("content://mms");

			try {
				Cursor cursor = contentResolver.query(uri, projection, null, null, "_id desc limit 1");
				if (cursor.getCount() == 0) {
					cursor.close();
					return;
				}

				cursor.moveToFirst();
				String id = cursor.getString(cursor.getColumnIndex("_id"));
				cursor.close();
				//아이디 값이 없으면 리턴.
				if (TextUtils.isEmpty(id)) {
					return;
				}

				String message = parseMessage(context, id);

				//사이다 뱅크 관련 메세지 체크 하도록.
				if (TextUtils.isEmpty(message) || !message.contains("사이다뱅크")) {
					return;
				}

				String authCode = getSMSAuthCode(message);
				if (!TextUtils.isEmpty(authCode)) {
					Logs.i("authCode : " + authCode);
					if(BuildConfig.DEBUG)
						Logs.showToast(context,authCode);
				}

				putIntoEditText(context, authCode);
			}catch(SecurityException e){
				Logs.printException(e);
			}
		}
	}

	public static String getSMSAuthCode(String msg){
		for(int i=0;i<msg.length();i++){
			if(msg.charAt(i) >= 0x30 && msg.charAt(i) <= 0x39 ){
				if(i+6 < msg.length()){
					String authCode = msg.substring(i,i+6);
					Logs.i("getSMSAuthCode - authCode : " + authCode);
					if(TextUtils.isDigitsOnly(authCode)){
						return authCode;
					}
				}
			}
		}
		return "";
	}

	private void putIntoEditText(Context context, final String authCode){
		if(TextUtils.isEmpty(authCode)) {
			return;
		}

		ActivityManager actMng = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> list = actMng.getRunningAppProcesses();
		String packageName = "";
		for(ActivityManager.RunningAppProcessInfo rap : list) {

			packageName = rap.processName;
			Logs.e("packageName = " + packageName + ", importance = " + rap.importance);
			if (packageName.equals(context.getPackageName())) {
				//현재 앱이 실행중이다.
				SaidaApplication mApplicationClass = (SaidaApplication) context.getApplicationContext();

				if (mApplicationClass.getActivitySize() > 0) {
					Activity activity = mApplicationClass.getOpenActivity("CertifyPhoneActivity");
					if (activity != null) {
						final Activity webActivity = activity;
						((CertifyPhoneActivity) activity).runOnUiThread(
								new Runnable() {
									@Override
									public void run() {
										((CertifyPhoneActivity) webActivity).autoInsertAuthCode(authCode);
									}
								});
					}
				}
				return;
			}//=>if
		}//=>for
	}

	/*===========================================================================================
	* MMS 번호, 메세지 분리하는 함수
	* ===========================================================================================*/

	private static String parseNumber(Context context, String id)
	{
		String result = null;

		Uri uri = Uri.parse(MessageFormat.format("content://mms/{0}/addr", id));
		String[] projection = new String[] { "address" };
		String selection = "msg_id = ? and type = 137";// type=137은 발신자
		String[] selectionArgs = new String[] { id };

		Cursor cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, "_id asc limit 1");

		if (cursor.getCount() == 0)
		{
			cursor.close();
			return result;
		}

		cursor.moveToFirst();
		result = cursor.getString(cursor.getColumnIndex("address"));
		cursor.close();

		return result;
	}

	private static String parseMessage(Context context, String mmsID)
	{
		String result = null;

		// 조회에 조건을 넣게되면 가장 마지막 한두개의 mms를 가져오지 않는다.
		Cursor cursor = context.getContentResolver().query(Uri.parse("content://mms/part"), new String[] { "mid", "_id", "ct", "_data", "text" }, null, null, null);

		Logs.e("MMSReceiver.java | parseMessage", "|mms 메시지 갯수 : " + cursor.getCount() + "|");
		if (cursor.getCount() == 0)
		{
			cursor.close();
			return result;
		}

		cursor.moveToFirst();
		while (!cursor.isAfterLast())
		{
			String mid = cursor.getString(cursor.getColumnIndex("mid"));
			if (mmsID.equals(mid))
			{
				String partId = cursor.getString(cursor.getColumnIndex("_id"));
				String type = cursor.getString(cursor.getColumnIndex("ct"));
				if ("text/plain".equals(type))
				{
					String data = cursor.getString(cursor.getColumnIndex("_data"));

					if (TextUtils.isEmpty(data))
						result = cursor.getString(cursor.getColumnIndex("text"));
					else
						result = parseMessageWithPartId(context,partId);
				}
				break;
			}else{
				cursor.moveToNext();
			}
		}
		cursor.close();

		return result;
	}


	private static String parseMessageWithPartId(Context context, String id)
	{
		Uri partURI = Uri.parse("content://mms/part/" + id);
		InputStream is = null;
		StringBuilder sb = new StringBuilder();
		try
		{
			is = context.getContentResolver().openInputStream(partURI);
			if (is != null)
			{
				InputStreamReader isr = new InputStreamReader(is, "UTF-8");
				BufferedReader reader = new BufferedReader(isr);
				String temp = reader.readLine();
				while (!TextUtils.isEmpty(temp))
				{
					sb.append(temp);
					temp = reader.readLine();
				}
			}
		}
		catch (IOException e)
		{
			Logs.printException(e);
		}
		finally
		{
			if (is != null)
			{
				try
				{
					is.close();
				}
				catch (IOException e)
				{
				}
			}
		}
		return sb.toString();
	}
}

