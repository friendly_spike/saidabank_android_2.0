package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: SlidingMobileCarrierDialog
 * Created by 950485 on 2011. 11. 02..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 통신사 다이얼로그
 */
public class SlidingMobileCarrierDialog extends SlidingBaseDialog { //implements AdapterView.OnItemClickListener{

    private Context mContext;
    private ArrayList<RequestCodeInfo> mDataList;

    private NumberPicker mPickerCarrier;
    private SelectItemListener mListener;

    public SlidingMobileCarrierDialog(@NonNull Context context, ArrayList<RequestCodeInfo> dataList, SelectItemListener listener) {
        super(context);
        mContext = context;
        mDataList = dataList;
        mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_mobile_carrier) ;
        setDialogWidth();

        mPickerCarrier = (NumberPicker) findViewById(R.id.numberpicker_carrier);
        setSelectionDivider(mContext, mPickerCarrier);
        final String[] arrayString= new String[mDataList.size()];
        for (int index = 0; index < mDataList.size(); index++) {
            RequestCodeInfo info = mDataList.get(index);
            if (info == null)
                continue;

            String name = info.getCD_NM();
            if (TextUtils.isEmpty(name))
                continue;

            arrayString[index] = name;
        }

        mPickerCarrier.setMinValue(0);
        mPickerCarrier.setMaxValue(arrayString.length - 1);
        mPickerCarrier.setWrapSelectorWheel(false);
        mPickerCarrier.setDisplayedValues(arrayString);

        TextView btnConfirm = (TextView) findViewById(R.id.textview_confirm_mobile_carrier);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = mPickerCarrier.getValue();
                String name = arrayString[position];
                for (int index = 0; index < mDataList.size(); index++) {
                    RequestCodeInfo info = mDataList.get(index);
                    String nameOrg = info.getCD_NM();
                    if (name.equalsIgnoreCase(nameOrg)) {
                        mListener.OnSelectItemListener(index);
                        break;
                    }
                }
                dismiss();
            }
        });
    }

    /**
     * divider의 컬러와 간격 조정
     *
     * @param context
     * @param numberpicker year, month, day picker
     */
    private void setSelectionDivider(Context context, NumberPicker numberpicker) {
        Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        int dividercolor;
        int distance = (int) context.getResources().getDimension(R.dimen.numberpicker_divider_distance);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            dividercolor = context.getColor(R.color.colorE5E5E5);
        } else {
            dividercolor = ContextCompat.getColor(context, R.color.colorE5E5E5);
        }

        Logs.e("distance : " + (int) context.getResources().getDimension(R.dimen.numberpicker_divider_distance));

        for (Field pf : pickerFields) {
            // divider color
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(dividercolor);
                    pf.set(numberpicker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    Logs.printException(e);
                } catch (Resources.NotFoundException e) {
                    Logs.printException(e);
                } catch (IllegalAccessException e) {
                    Logs.printException(e);
                }
            }

            // divider distance
            if (pf.getName().equals("mSelectionDividersDistance")) {
                pf.setAccessible(true);
                try {
                    Logs.i("deviders distance : " + pf.getInt(numberpicker));
                    //pf.setInt(numberpicker, distance);
                } catch (IllegalArgumentException e) {
                    Logs.printException(e);
                } catch (Resources.NotFoundException e) {
                    Logs.printException(e);
                } catch (IllegalAccessException e) {
                    Logs.printException(e);
                }
            }
        }
    }

    public interface SelectItemListener {
        void OnSelectItemListener(int position);
    }
}
