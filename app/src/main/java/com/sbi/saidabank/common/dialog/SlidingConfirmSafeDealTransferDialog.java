package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.transfer.TransferSafeDealDataMgr;

/**
 * SlidingConfirmSafeDealTransferDialog : 안심이체 확인 다이얼로그
 *
 */
public class SlidingConfirmSafeDealTransferDialog extends SlidingBaseDialog {

    private Context mContext;
    private View.OnClickListener mPlistener;
    private View.OnClickListener mLlistener;
    public SlidingConfirmSafeDealTransferDialog(@NonNull Context context, View.OnClickListener plistener, View.OnClickListener llistener) {
        super(context);
        this.mContext = context;
        this.mPlistener = plistener;
        this.mLlistener = llistener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm_safe_deal_transfer);
        setDialogWidth();
        setCanceledOnTouchOutside(false);
        initLayout();
    }
// ==============================================================================================================
// UI
// ==============================================================================================================
    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {

        Button mBtnCancel = (Button) findViewById(R.id.btn_cancel);
        mBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mLlistener.onClick(v);
            }
        });

        Button mBtnOK = (Button) findViewById(R.id.btn_ok);
        mBtnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
                mPlistener.onClick(v);
            }
        });

        TextView mTvName = (TextView) findViewById(R.id.tv_rcv_name);
        mTvName.setText(DataUtil.isNotNull(TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM()) ? TransferSafeDealDataMgr.getInstance().getMNRC_ACCO_DEPR_NM() : Const.EMPTY);

        TextView mTvAccount = (TextView) findViewById(R.id.tv_rcv_account);
        mTvAccount.setText(DataUtil.isNotNull(getAccountNm()) ? getAccountNm() : Const.EMPTY);

        TextView textAmount = (TextView) findViewById(R.id.tv_amount);
        String amountStr = (DataUtil.isNotNull(TransferSafeDealDataMgr.getInstance().getTRAM()) ? Utils.moneyFormatToWon(Double.valueOf(TransferSafeDealDataMgr.getInstance().getTRAM())) : Const.ZERO) + " 원" ;
        textAmount.setText(amountStr);

        TextView textFee = (TextView) findViewById(R.id.tv_fee);

        String feeStr = "";
        if(TextUtils.isEmpty(TransferSafeDealDataMgr.getInstance().getTRNF_FEE()) || TransferSafeDealDataMgr.getInstance().getTRNF_FEE().equals("0")){
            feeStr = "2,000 원(면제)";
        }else{
            String TRNF_FEE = TransferSafeDealDataMgr.getInstance().getTRNF_FEE();
            if (!TextUtils.isEmpty(TRNF_FEE)) {
                TRNF_FEE = Utils.moneyFormatToWon(Double.valueOf(TRNF_FEE));
            }
            feeStr = TRNF_FEE;
        }
        textFee.setText(feeStr);

        TextView textWithdrawAccountName = (TextView) findViewById(R.id.tv_withdraw_account_name);


        String ACCO_ALS = TransferSafeDealDataMgr.getInstance().getACCO_ALS();
        ACCO_ALS = DataUtil.isNotNull(ACCO_ALS) ? ACCO_ALS : "SBI저축(사이다뱅크)";

        String ACNO =  TransferSafeDealDataMgr.getInstance().getWTCH_ACNO();
        if (!TextUtils.isEmpty(ACNO)) {
            int lenACNO = ACNO.length();
            if (lenACNO > 4)
                ACNO = ACNO.substring(lenACNO - 4, lenACNO);
        }

        textWithdrawAccountName.setText(ACCO_ALS + " [" + ACNO + "]");

    }

    /**
     * 계좌명, 계좌번호를 얻는다.
     *
     * @return 계좌명 && 계좌번호
     */
    private String getAccountNm() {

        String accNo;
        String mnrcBankCd = TransferSafeDealDataMgr.getInstance().getMNRC_BANK_CD();
        String mnrcAcno = TransferSafeDealDataMgr.getInstance().getMNRC_ACNO();
        if (DataUtil.isNotNull(mnrcBankCd) && ("000".equalsIgnoreCase(mnrcBankCd) || "028".equalsIgnoreCase(mnrcBankCd))) {
            accNo = mnrcAcno.substring(0, 5) + Const.DASH + mnrcAcno.substring(5, 7) + Const.DASH + mnrcAcno.substring(7, mnrcAcno.length());
        } else {
            accNo = mnrcAcno;
        }
        return TransferSafeDealDataMgr.getInstance().getMNRC_BANK_NAME() + " " + accNo + "으로";
    }
}
