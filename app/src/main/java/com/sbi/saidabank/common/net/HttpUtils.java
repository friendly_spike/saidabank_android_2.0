package com.sbi.saidabank.common.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.JsonUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class HttpUtils {

    public static final String JEX_SVC_EXT = ".jct";    // JEX프레임에서 사용하는 Action 확장자 파일
    public static String mUserAgent;

    public enum NetState {
        NET_STATE_MOBILE,
        NET_STATE_WIFI,
        NET_STATE_ETHERNET,
        NET_STATE_OFFLINE,
        NET_STATE_NOT_SUPPORT,
    }

    /**
     * Get방식으로 웹에 접속하여 데이타 조회시 사용
     *
     * @param url      접속주소
     * @param listener 리턴받을 리스너
     */
    public static void sendHttpTask(String url, HttpSenderTask.HttpRequestListener listener) {
        new HttpSenderTask(url, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * POST방식으로 웹에 접속하여 데이타 조회시 사용
     *
     * @param url      접속주소
     * @param param    서버에 보낼 데이타  String 타입
     * @param listener 리턴받을 리스너
     */
    public static void sendHttpTask(String url, String param, HttpSenderTask.HttpRequestListener listener) {
        new HttpSenderTask(url, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,param);
    }

    /**
     * POST방식으로 웹에 접속하여 데이타 조회시 사용
     *
     * @param url      접속주소
     * @param param    서버에 보낼 데이타  Map타입
     * @param listener 리턴받을 리스너
     */
    public static void sendHttpTask(String url, Map<String, Object> param, HttpSenderTask.HttpRequestListener listener) {
        if (DataUtil.isNull(url))
            return;
        String strParam = Const.EMPTY;

        if(param != null && param.size() > 0){
            try {
                if (url.contains(SaidaUrl.DEV_SERVER) || url.contains(SaidaUrl.OPER_SERVER) || url.contains(SaidaUrl.TEST_SERVER)) {
                    strParam = JsonUtils.mapToJson(param).toString();
                } else {
                    strParam = mapToParamString(param);
                }
            } catch (UnsupportedEncodingException e) {
                MLog.e(e);
            }
        }

        //Logs.e("sendHttpTask - url : " + url);
        //Logs.e("sendHttpTask - param : " + strParam);

        new HttpSenderTask(url, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, strParam);
    }



    /**
     * 앱에서 사용되는 User-Agent 값을 생성한다. <br>
     * baseUserAgent 값이 있는 경우 baseUserAgent에 앱전용 User-Agent값이 append된다.
     *
     * @param context
     * @param string
     * @return User-Agent 문자열
     */
    public static String makeUserAgentString(Context context, String string) {
        StringBuilder sb = new StringBuilder();
        if (DataUtil.isNotNull(string)) {
            sb.append(string).append(";");
        }
        sb.append("COMPANY=").append("sbisavingsbank").append(";");
        sb.append("DEVICE_APP_VER=").append(Utils.getVersionName(context)).append(";");
        sb.append("DEVICE_OS=").append("Android").append(";");
        sb.append("OS_VERSION=").append(Build.VERSION.RELEASE).append(";");
        sb.append("DEVICE_TEL_NO=").append(DataUtil.isNotNull(Utils.getPhoneNumber(context)) ? Utils.getPhoneNumber(context).replace("-", Const.EMPTY).replace("+82", "0") : "unknown").append(";");
        sb.append("DEVICE_MODEL=").append(Build.MODEL);
        mUserAgent = sb.toString();
        return mUserAgent;
    }

    /**
     * Http연결시 Header에 쿠키정보를 넣어준다.
     *
     * @param url  접속주소
     * @param conn Connection정보
     */
    public static void setCooKie(String url, HttpURLConnection conn) {
        try {
            String cookieString = CookieManager.getInstance().getCookie(url);
            if (DataUtil.isNotNull(cookieString)) {
                conn.setRequestProperty("Cookie", cookieString);
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * Http접속후 넘어오는 Cookie정보를 저장해 둔다.
     *
     * @param conn
     */
    public static void saveCookie(URLConnection conn) {
        try {
            String cookie = conn.getHeaderField("Set-Cookie");

            if (DataUtil.isNotNull(cookie)) {
                String url = conn.getURL().toString();
                CookieManager cookieManager = CookieManager.getInstance();
                cookieManager.setCookie(url, cookie);
                cookieManager.flush();
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 쿠키정보를 Clear한다.
     */
    public static void removeCookie() {
        try {
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeSessionCookies(new ValueCallback<Boolean>() {
                @Override
                public void onReceiveValue(Boolean aBoolean) {
                    MLog.i("remove Session Cookie >> " + aBoolean);
                }
            });
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * Web Browser에서 앱 호출시 파라미터 자름
     * Saidasb://smartbanking?json={"MENU_ID":"MPDT0110101000M","KEY1":"한글 한글 asdfasf","KEY2":"abcd 한글 가나다"}
     *
     * @param url
     * @return String 자른 파라미터 부분 문자열
     */
    public static String getMobileWebParam(String url) {
        String DIV_STR = "json=";
        if (DataUtil.isNull(url)) return Const.EMPTY;
        int pos = url.indexOf(DIV_STR) + DIV_STR.length();
        if (pos < 0) return Const.EMPTY;
        if (pos + 1 == url.length()) return Const.EMPTY;
        return url.substring(pos, url.length());
    }

    /**
     * 모바일 단말기의 인터넷 접속 상태를 체크한다.
     *
     * @param context
     * @return 상태값
     */
    public static NetState checkNetworkState(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (DataUtil.isNotNull(activeNetwork)) {
            if (activeNetwork.isConnectedOrConnecting()) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                    return NetState.NET_STATE_WIFI;     // WIFI 네트워크 연결중
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                    return NetState.NET_STATE_MOBILE;   // 모바일 네트워크 연결중
                } else if (activeNetwork.getType() == ConnectivityManager.TYPE_ETHERNET) {
                    return NetState.NET_STATE_ETHERNET; // 모바일 네트워크 연결중
                }
            } else {
                return NetState.NET_STATE_OFFLINE;      // 네트워크 오프라인 상태.
            }
        }
        return NetState.NET_STATE_NOT_SUPPORT;          // 네트워크 null.. 모뎀이 없는 경우??
    }

    /**
     * Http Header 문자열을 가져온다
     *
     * @param type
     * @param conn
     * @return Header 문자열
     */
    public static String printHeader(String type, HttpURLConnection conn) {
        JSONObject jsonHeader = new JSONObject();
        try {
            if (DataUtil.isNotNull(conn)) {
                Map<String, List<String>> headers = conn.getHeaderFields();
                Iterator<String> it = headers.keySet().iterator();
                while (it.hasNext()) {
                    String key = it.next();
                    List<String> values = headers.get(key);
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < values.size(); i++) {
                        sb.append(";" + values.get(i));
                    }
                    if (key != null && key.length() > 0) {
                        jsonHeader.put(key, sb.toString().substring(1));
                    }
                }
                jsonHeader.put("Location", conn.getURL().toString());
            }
        } catch (JSONException e) {
            MLog.e(e);
        }
        return jsonHeader.toString();
    }

    /**
     * Map을 파라미터 문자열 형태로 변환한다.
     *
     * @param paramMap 파라미터 Map
     * @return 파라미터 문자열 (key1=value1&key2=value2&...)
     * @throws UnsupportedEncodingException
     */
    public static String mapToParamString(Map<String, Object> paramMap) throws UnsupportedEncodingException {
        if (DataUtil.isNull(paramMap))
            return null;
        StringBuilder sb = new StringBuilder();
        int paramCnt = paramMap.size();
        int index = 0;
        for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
            sb.append(entry.getKey()).append(Const.EQUAL).append(URLEncoder.encode((String) entry.getValue(), Const.EUC_KR));
            if (index < paramCnt - 1) {
                sb.append(Const.AND);
            }
            index++;
        }
        return sb.toString();
    }

    /**
     * 사이다 뱅크서버에 접속하는 URL인지 체크한다.
     * JEX 프레임에 접근하는 특정 방식이 있어 이를 체크하여 처리한다.
     *
     * @param url
     * @return
     */
    public static boolean isSaidabankUrl(String url) {
        return url.contains(JEX_SVC_EXT)
                && (url.contains(SaidaUrl.OPER_SERVER)
                || url.contains(SaidaUrl.DEV_SERVER)
                || url.contains(SaidaUrl.TEST_SERVER));
    }

    /**
     * JEX 프레임의 서비스 명을 뽑아낸다.
     *
     * @param url
     * @return
     */
    public static String getJexSeviceId(String url) {
        if (isSaidabankUrl(url)) {
            int pos = url.lastIndexOf(JEX_SVC_EXT);
            String subStr = url.substring(0, pos);
            pos = subStr.lastIndexOf(Const.SLASH);
            if (pos >= 0) {
                return subStr.substring(pos);
            }
        }
        return Const.EMPTY;
    }
}
