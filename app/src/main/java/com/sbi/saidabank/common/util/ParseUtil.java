package com.sbi.saidabank.common.util;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;

/**
 * ParseUtil : Server로 부터 응답받은 데이터를 확인
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-02-24
 */
public class ParseUtil {

    /**
     * 사용자가 server에 요청하는 API 호출
     *
     * @param url : 요청 API
     */
    public static void parseUrl(String url) {
        try {
            MLog.i(" ############### Client URL Start ###############");
            MLog.i(" ## Request Url ##");
            MLog.i(" " + url);
            MLog.i(" ############### Client URL End ###############");
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 사용자가 server로 보내는 값을 확인.
     *
     * @param url   요청 Url
     * @param query 요청 파라미터
     */
    public static void parseClientCall(String url, @Nullable String query) {
        try {
            MLog.i(" ############### Client Call Start ###############");
            MLog.i(" ## Request Url ##");
            MLog.i(" " + url);
            if (DataUtil.isNotNull(query)) {
                MLog.i(" ## Request Params ##");
                parseJSONObject(new JSONObject(query));
            }
            MLog.i(" ############### Client Call End ###############");
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 사용자가 server로 보내는 값을 확인.
     *
     * @param url   요청 Url
     * @param query 요청 파라미터
     */
    public static void parseClientCall(String url, Map<String, Object> query) {
        try {
            MLog.i(" ############### Client Call Start ###############");
            MLog.i(" ## Request Url ##");
            MLog.i(" " + url);
            if (DataUtil.isNotNull(query)) {
                MLog.i(" ## Request Parameter ##");
                Iterator<String> iterator = query.keySet().iterator();
                while (iterator.hasNext()) {
                    String key = iterator.next();
                    MLog.i(" ## " + key + ": " + query.get(key));
                }
            }
            MLog.i(" ############### Client Call End ###############");
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 전송받은 데이타의 전체 내용을 보여준다. debug 용이다.
     *
     * @param jsonObj 전송 데이타의 JSON Object
     */
    public static void parseServerData(String url, @Nullable String query, JSONObject jsonObj) {
        try {
            MLog.i(" ############### Client Call Start ###############");
            MLog.i(" ## Request Url ##");
            MLog.i(" " + url);
            if (DataUtil.isNotNull(query)) {
                MLog.i(" ## Request Params ##");
                parseJSONObject(new JSONObject(query));
            }
            MLog.i(" ############### Client Call End ###############");
            MLog.i(" ############### Response Start ###############");
            parseJSONObject(jsonObj);
            MLog.i(" ############### Response End  ###############");
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * 전송받은 데이타의 전체 내용을 보여준다. debug 용이다.
     *
     * @param jsonObj 전송 데이타의 JSON Object
     */
    public static void parseServerData(JSONObject jsonObj) {
        /**
         * JSONObject jsonObj = new JSONObject();
         * 		try {
         * 			jsonObj.put("str1", "String 1입니다");
         * 			jsonObj.put("data",
         * 					new JSONArray(""
         * 					+ "["
         * 						+ "{\"dog\":\"arr1 value 1\", \"cat\":\"arr1 value 2\", \"dolpin\":\"arr1 value 3\"}, "
         * 						+ "{\"arr2-1\":\"arr2 value 1\", \"arr2-2\":\"arr2 value 2\"}"
         * 					+ "]"));
         * 			jsonObj.put("str2", "String 2입니다");
         *
         * 			ParseData.parseServerData(jsonObj);
         *         } catch (Exception e) {}
         * */
        MLog.i(" ############### Response Start ###############");
        parseJSONObject(jsonObj);
        MLog.i(" ############### Response End  ###############");
    }

    /**
     * server에서 받은 데이타 값을 확인.
     *
     * @param jsonObj 서버에서 받은 데이타 값
     */
    public static void parseJSONObject(JSONObject jsonObj) {
        try {
            if (jsonObj == null)
                return;

            Iterator<String> keys = jsonObj.keys();

            while (keys.hasNext()) {
                String nextKey = keys.next();
                Object obj = jsonObj.get(nextKey);

                if (obj instanceof JSONObject) {
                    MLog.i(" ## \"" + nextKey + "\" : {");
                    parseJSONObject((JSONObject) obj);

                    if (keys.hasNext())
                        MLog.i("## }, ");
                    else
                        MLog.i("## }");

                } else if (obj instanceof JSONArray) {
                    MLog.i(" ## \"" + nextKey + "\" : [");
                    JSONArray array = (JSONArray) obj;
                    parseArr(array);

                    if (keys.hasNext())
                        MLog.i("## ], ");
                    else
                        MLog.i("## ]");
                } else if (obj instanceof String) {
                    if (keys.hasNext()) {
                        MLog.i(" ##    \"" + nextKey + "\" : \"" + obj + "\",");
                    } else {
                        MLog.i(" ##    \"" + nextKey + "\" : \"" + obj + "\"");
                    }
                } else {
                    if (keys.hasNext()) {
                        MLog.i(" ##    \"" + nextKey + "\" : " + obj + ",");
                    } else {
                        MLog.i(" ##    \"" + nextKey + "\" : " + obj + "");
                    }
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    private static void parseArr(JSONArray array) {
        try {
            Object obj;

            for (int i = 0; i < array.length(); i++) {
                obj = array.get(i);

                if (obj instanceof JSONObject) {
                    MLog.i(" ## {");
                    parseJSONObject((JSONObject) obj);

                    if (i + 1 == array.length())
                        MLog.i(" ## }");
                    else
                        MLog.i("## },");
                } else if (obj instanceof JSONArray) {
                    parseArr((JSONArray) obj);
                } else {
                    MLog.i(" ##     " + array.getString(i));
                }
            }
        } catch (Exception e) {
            MLog.e(e);
        }
    }

    /**
     * JsonObject 'Null' 데이터 삭제
     *
     * @param jsonObj : 데이터
     */
    public static JSONObject removeNullKey(JSONObject jsonObj) {
        try {
            Iterator<String> keyIt = jsonObj.keys();
            HashSet<String> keySet = new HashSet<String>();
            while (keyIt.hasNext()) {
                keySet.add(keyIt.next());
            }
            Iterator<String> keys = keySet.iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                if (jsonObj.isNull(key)) {
                    jsonObj.remove(key);
                    continue;
                }
                Object value;
                value = jsonObj.get(key);
                if (value instanceof JSONObject) {
                    removeNullKey((JSONObject) value);
                } else if (value instanceof JSONArray) {
                    removeNullKey((JSONArray) value);
                } else {
                    // do nothing
                }
            }
        } catch (JSONException e) {
            MLog.e(e);
        }
        return jsonObj;
    }

    /**
     * JSONArray 'Null' 데이터 삭제
     *
     * @param jsonArr : 배열 데이터
     */
    public static JSONArray removeNullKey(JSONArray jsonArr) {
        try {
            for (int i = 0; i < jsonArr.length(); i++) {
                Object obj = jsonArr.get(i);
                if (obj instanceof JSONObject) {
                    removeNullKey((JSONObject) obj);
                } else if (obj instanceof JSONArray) {
                    removeNullKey((JSONArray) obj);
                } else {
                    // do nothing
                }
            }
        } catch (JSONException e) {
            MLog.e(e);
        }
        return jsonArr;
    }
}
