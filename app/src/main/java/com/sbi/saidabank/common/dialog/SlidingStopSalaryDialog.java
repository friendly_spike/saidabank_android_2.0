package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;


public class SlidingStopSalaryDialog extends SlidingBaseDialog implements View.OnClickListener{
    private Context  context;

    public View.OnClickListener listener;

    public SlidingStopSalaryDialog(@NonNull Context context, View.OnClickListener listener) {
        super(context);
        this.context = context;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.dialog_slide_stop_salary);
        setDialogWidth();

        TextView tvSubTitle = findViewById(R.id.tv_subtitle);

        int completeCnt = ITransferSalayMgr.getInstance().getCompleteCount();

        String msg = "총 " + completeCnt + "개의 계좌에 급여이체로 입금되었습니다.\n급여이체중단 시, 처음부터 다시 계좌가 이체되오니\n이용에 참고하세요.";
        Utils.setTextWithSpan(tvSubTitle, msg, String.valueOf(completeCnt), Typeface.BOLD, "#009beb");

        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancel:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }
                break;

            case R.id.btn_ok:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }

                if (listener != null) {
                    listener.onClick(v);
                }
                break;

            default:
                break;
        }
    }
}
