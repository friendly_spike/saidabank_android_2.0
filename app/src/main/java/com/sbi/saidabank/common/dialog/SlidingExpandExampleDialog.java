package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: SlidingExpandExampleDialog
 *
 * <p>
 * Description: 화면 사이즈를 늘릴수 있는 다이얼로그의 템플릿이다. 복사해서 새로운 다이얼로그 만들때 사용하라.
 */

public class SlidingExpandExampleDialog extends SlidingExpandBaseDialog{

    private Context      mContext;


    public SlidingExpandExampleDialog(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_sliding_expand_bank_stock);

        setDialogWidth();

        //화면을 늘리기위한 기본 뷰를 셋팅한다.
        setDialogBody((LinearLayout) findViewById(R.id.layout_body));
        setExpandMoveBar(findViewById(R.id.layout_movebar));

        initUX();
    }

    private void initUX() {

        //백그라운드 터치시 다이얼로그 종료
        findViewById(R.id.v_background).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }


    public interface OnSelectItemListener {
        void onSelectListener(String bank, String code);
    }
}
