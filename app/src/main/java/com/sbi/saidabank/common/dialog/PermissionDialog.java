package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;

/**
 * Saidabank_android
 * Class: PermissionDialog
 * Created by 950546
 * Date: 2018-10-17
 * Time: 오후 2:41
 * Description: 접근권한 팝업
 */
public class PermissionDialog extends Dialog {

    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;
    private Context mContext;

    public PermissionDialog(Context context) {
        //super(context);
        super(context, R.style.AppThemeNoActionBar);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        setContentView(R.layout.dialog_permission);
        Button mBtPositive = (Button) findViewById(R.id.btn_confirm);
        mBtPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mPListener)) {
                    mPListener.onClick(v);
                }
                dismiss();
            }
        });
    }
}
