package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;


public class CommonErrorDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;
    private String mMsg;
    private String mErrCode;
    private String mScrnId;
    private JSONObject mObjentHead;
    private boolean mIsShownRetryText;
    private OnConfirmListener _listener;
    private OnDismissListener _dismissListener;

    private LinearLayout        mLayoutSendErr;
    private ImageView           mLoadingImageView;
    private Button              mBtChatbot;
    private Button              mBtPositive;

    public CommonErrorDialog(Context context, String msg, String errCode, String scrnId, JSONObject objectHead) {
        super(context);
        mContext = context;
        mMsg = msg;
        mErrCode = errCode;
        mScrnId = scrnId;
        mObjentHead = objectHead;
        mIsShownRetryText = true;
    }

    public CommonErrorDialog(Context context, String msg, String errCode, String scrnId, JSONObject objectHead, boolean isShownWaitText) {
        super(context);
        mContext = context;
        mMsg = msg;
        mErrCode = errCode;
        mScrnId = scrnId;
        mObjentHead = objectHead;
        mIsShownRetryText = isShownWaitText;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setContentView(R.layout.dialog_common_error);

        setDialogWidth();

        initView();
    }

    private void initView() {
        setCancelable(false);

        if (TextUtils.isEmpty(mMsg)) {
            mMsg = mContext.getString(R.string.msg_communication_fail_err_popup);
        }

        if (TextUtils.isEmpty(mErrCode)) {
            mErrCode = "에러코드 없음";
        }

        mLoadingImageView = (ImageView) findViewById(R.id.img_progress_webview);

        if (mMsg.contains("font color")||mMsg.contains("<br>") || mMsg.contains("<b>")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mMsg, Html.FROM_HTML_MODE_COMPACT));
            } else {
                ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mMsg));
            }
        }else{
            ((TextView) findViewById(R.id.tv_title)).setText(mMsg);
        }

        ((TextView) findViewById(R.id.tv_errcode)).setText(mErrCode);

        boolean isHideBtnChatbot = true;
        // 점검시간 에러 코드일 때 메세지만 보이도록 함
        if (Const.ERRORCODE_EFWK0002.equals(mErrCode)) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
        } else if (mMsg.contains("출금계좌와 입금계좌가 동일")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_same_acount)));
        } else if (mMsg.contains("유효하지 않은 계좌")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_invalid_acount)));
        }  else if (mMsg.contains("입금이 불가능한 계좌")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_impossible_acount)));
        } else if (mMsg.contains("해당 계좌는 휴면계좌")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(mContext.getString(R.string.msg_transfer_dormant_account));
        } else if (mMsg.contains("출금계좌를 변경해 주세요")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_if_sbi_savingbank)));
        }else if (mMsg.contains("오픈뱅킹 서비스가 해지")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_not_join_openbank)));
        }else if (mMsg.contains("수취인 조회 중 일시적인 오류")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_not_check_remitte)));
        }else if (mMsg.contains("즐겨찾기는 최대 10")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_albe_favorite_max_10)));
        }else if (mMsg.contains("급여이체 불가능")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_salary_not_able_time)));
        }else if (mMsg.contains("오늘 발급된 증명서")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_today_certificate)));
        }else if (mMsg.contains("오픈뱅킹으로 이체가 불가한 기관")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText(Html.fromHtml(mContext.getString(R.string.msg_transfer_error_not_able_openbank)));
        }else if (mMsg.contains("일시적으로 계좌의 정보")) {
            findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            findViewById(R.id.ll_sendimage).setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_title)).setText("일시적으로 계좌의 정보를\n확인할 수 없습니다.\n잠시 후 다시 이용해주시기 바랍니다.");
        }else {
            mLayoutSendErr = (LinearLayout) findViewById(R.id.ll_sendimage);
            mLayoutSendErr.setOnClickListener(this);
            isHideBtnChatbot = false;
        }

        mBtChatbot = (Button) findViewById(R.id.btn_chatbot);
        mBtPositive = (Button) findViewById(R.id.btn_confirm);
        if (!isHideBtnChatbot) {
            mBtChatbot.setOnClickListener(this);
        }
        else {
            mBtChatbot.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        }

        if (!LoginUserInfo.getInstance().isLogin() || mErrCode.startsWith("MAI")) {
            if (mLayoutSendErr != null)
                mLayoutSendErr.setVisibility(View.GONE);
            mBtChatbot.setVisibility(View.GONE);
            if (mErrCode.startsWith("MAI"))
                findViewById(R.id.tv_errcode).setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        }

        mBtPositive.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_chatbot: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_TEST) {
                        Logs.showToast(mContext, "테스트 서버에서는 챗봇 사용 불가입니다.");
                    } else {
                        dismiss();

                        String TRN_CD = "";
                        if (mObjentHead != null && mObjentHead.has("DATA")) {
                            JSONObject data = mObjentHead.optJSONObject("DATA");

                            if (data.has("__INFO__")) {
                                JSONObject info;
                                info = data.optJSONObject("__INFO__");
                                TRN_CD = info.optString("TRN_CD");
                            }
                        }
                        StringBuilder param = new StringBuilder();

                        try {
                            param.append("TRN_CD" + "=" + URLEncoder.encode(TRN_CD, "EUC-KR"));
                            param.append("&");
                            param.append("EROR_CD" + "=" + URLEncoder.encode(mErrCode, "EUC-KR"));
                            param.append("&");
                            param.append("ERROR_MESSAGE" + "=" + URLEncoder.encode(mMsg));
                        } catch (UnsupportedEncodingException e) {
                            //e.printStackTrace();
                        }
                        String url = SaidaUrl.ReqUrl.URL_CHATBOT.getReqUrl() + "?" + param.toString();
                        Intent intent = new Intent(mContext, WebMainActivity.class);
                        intent.putExtra("url", url);
                        mContext.startActivity(intent);
                    }
                }
                break;
            }
            case R.id.btn_confirm: {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    dismiss();

                    if (_dismissListener != null)
                        _dismissListener.onDismiss();

                    if (_listener != null)
                        _listener.onConfirmPress();

                }
                break;
            }

            case R.id.ll_sendimage: {
                captureScreen();
                break;
            }

            default:
                break;
        }
    }

    public void setOnConfirmListener(OnConfirmListener listener) {
        _listener = listener;
    }

    public void setOnDismissListener(OnDismissListener listener) {
        _dismissListener = listener;
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /**
     * 에러 다이얼로그가 호출되면 화면을 캡쳐하여 WAS로 전달한다.
     */
    private void captureScreen() {

        Map param = new HashMap();

        //채널 was와 오류났을 경우
        if (mObjentHead != null && mObjentHead.has("DATA")) {
            JSONObject data = mObjentHead.optJSONObject("DATA");

            if (data != null) {
                if (data.has("__INFO__")) {
                    JSONObject info = data.optJSONObject("__INFO__");
                    if (info != null) {
                        String CH_GLOBAL_ID = info.optString("CH_GLOBAL_ID");
                        String TRN_DT = info.optString("TRN_DT");
                        String TRN_DTTM = info.optString("TRN_DTTM");
                        String GLOBAL_ID = info.optString("GLOBAL_ID");

                        param.put("CH_GLOBAL_ID", CH_GLOBAL_ID);
                        param.put("TRN_DT", TRN_DT);
                        param.put("TRN_DTTM", TRN_DTTM);
                        param.put("GLOBAL_ID", GLOBAL_ID);
                    }
                }
            }
        }

        param.put("SCRN_ID", mScrnId);
        param.put("ERO_CD", mErrCode);

        View activityView = ((Activity) mContext).getWindow().getDecorView().getRootView();
        View dialogView = getWindow().getDecorView().getRootView();
        byte[] imgData = captureSave(activityView, dialogView);

        param.put("IMG_CNTN", Base64.encodeToString(imgData, Base64.DEFAULT));

        showProgress();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgress();
                if (TextUtils.isEmpty(ret)) {
                    Logs.showToast(mContext, "이미지 전송 실패하였습니다.");
                    return;
                }
                try {
                    JSONObject object = new JSONObject(ret);
                    if (object == null) {
                        Logs.showToast(mContext, "이미지 전송 실패하였습니다.");
                        return;
                    }
                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        Logs.showToast(mContext, "이미지 전송 실패하였습니다.");
                        return;
                    }
                    Logs.showToast(mContext, "이미지 전송 완료하였습니다.");
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    public static byte[] captureSave(View view, View view2) {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();

        view2.setDrawingCacheEnabled(true);
        view2.buildDrawingCache();
        Bitmap viewBmp2 = view2.getDrawingCache();

        if (viewBmp != null && viewBmp2 != null) {
            Bitmap combineBmp = ImgUtils.combineBitmap(viewBmp, viewBmp2);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            combineBmp.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            view2.setDrawingCacheEnabled(false);
            view2.destroyDrawingCache();
            viewBmp2.recycle();
            combineBmp.recycle();
            return byteArrayOutputStream.toByteArray();
        }

        return null;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (_dismissListener != null)
            _dismissListener.onDismiss();
    }

    public interface OnConfirmListener {
        void onConfirmPress();
    }

    public interface OnDismissListener {
        void onDismiss();
    }

    private void showProgress() {
        if (mLayoutSendErr != null)
            mLayoutSendErr.setClickable(false);
        if (mBtChatbot != null)
            mBtChatbot.setClickable(false);
        if (mBtPositive != null)
            mBtPositive.setClickable(false);
        if (mLoadingImageView != null && mLoadingImageView.getVisibility() != View.VISIBLE) {
            mLoadingImageView.setVisibility(View.VISIBLE);
            ((AnimationDrawable) mLoadingImageView.getBackground()).start();
        }
    }

    private void dismissProgress() {
        if (mLayoutSendErr != null)
        mLayoutSendErr.setClickable(true);
        if (mBtChatbot != null)
        mBtChatbot.setClickable(true);
        if (mBtPositive != null)
        mBtPositive.setClickable(true);
        if (mLoadingImageView != null) {
            mLoadingImageView.setVisibility(View.GONE);
        }
    }
}

