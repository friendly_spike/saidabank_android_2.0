package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.ahnlab.enginesdk.MetadataManager.getString;

/**
 * Saidabank_android
 * Class: SlidingExpandBankStockDialog
 * Created by 950485 on 2018. 11. 14..
 * <p>
 * Description:아래에서 위로 올라오는 은행/증권회사 다이얼로그.화면 사이즈를 늘릴수 있다.
 */

public class SlidingExpandSelectSenderAccountDialog extends SlidingExpandBaseDialog {
    private static final int SHOW_LIST_MYACCOUNT = 0;
    private static final int SHOW_LIST_OPENBANK = 1;

    private Activity mContext;

    private ProgressBarLayout mProgressBarLayout;

    //탭관련
    private LinearLayout     mLayoutTab;
    private TextView         mTextTabMyaccount;
    private TextView         mTextTabOpenBank;

    //내계좌관련
    private RelativeLayout          mLayoutMyAccount;
    private ListView                mListViewMyAccount;
    private ListAdapterMyAccount    mListAdapterMyAccount;
    private LinearLayout            mLayoutEmptyMyAccount;

    //오픈뱅크관련
    private RelativeLayout          mLayoutOpenBank;
    private ListView                mListViewOpenBank;
    private ListAdapterOpenBank     mListAdapterOpenBank;
    private LinearLayout            mLayoutEmptyOpenBank;

    private LinearLayout            mLayoutEmptyAllAccount;

    private ArrayList<MyAccountInfo> mMyAccountArrayList;
    private ArrayList<OpenBankAccountInfo> mOpenBankAccountArrayList;

    private OnSelectBankStockListener  mBankStockSelectListener;

    //이체화면에서 사용하는지.
    private boolean isUseTransfer;

    private int mMyAccountListBackupIndex = -1;
    private int mOpenBankListBackupIndex = -1;

    public SlidingExpandSelectSenderAccountDialog(@NonNull Activity context, boolean useTransfer,
                                                  OnSelectBankStockListener bankClickListener) {
        super(context);
        this.mContext = context;
        this.isUseTransfer = useTransfer;


        mMyAccountArrayList = new ArrayList<MyAccountInfo>();
        mOpenBankAccountArrayList = new ArrayList<OpenBankAccountInfo>();

        mMyAccountArrayList.addAll(LoginUserInfo.getInstance().getMyAccountInfoArrayList());
        mOpenBankAccountArrayList.addAll(OpenBankDataMgr.getInstance().getOpenBankInOutAccountList());

        this.mBankStockSelectListener = bankClickListener;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_sliding_expand_select_sender_account);

        setDialogWidth();

        //화면을 늘리기위한 기본 뷰를 셋팅한다.
        setDialogBody((LinearLayout) findViewById(R.id.layout_body));
        setExpandMoveBar(findViewById(R.id.layout_movebar));

        initUX();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    private void initUX() {

        //백그라운드 터치시 다이얼로그 종료
        findViewById(R.id.v_background).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        //전체화면 프로그레스바 레이아웃
        //프로그레스 다이얼로그를 출력할수 없기에...
        mProgressBarLayout = findViewById(R.id.ll_progress);

        mLayoutTab = findViewById(R.id.layout_tab);

        mLayoutEmptyMyAccount = findViewById(R.id.layout_empty_myaccount);
        mLayoutEmptyOpenBank = findViewById(R.id.layout_empty_openbank);
        mLayoutEmptyAllAccount = findViewById(R.id.layout_empty_allaccount);

        /**
         * 주의!!!
         *
         * 내계좌와 오픈뱅크 리스트를 두개로 나눈 이유는
         * 오픈뱅킹에서 개별 조회를 해야 하는데.. 에니메이션이 동작이 않되는 경우가 간혹 발생하여
         * 어댑터를 따로 두어 해결하였다.
         */

        //내계좌
        mLayoutMyAccount = findViewById(R.id.layout_myaccount);
        mListAdapterMyAccount = new ListAdapterMyAccount();
        mListViewMyAccount = (ListView) findViewById(R.id.listview_myaccount);
        mListViewMyAccount.setAdapter(mListAdapterMyAccount);
        mListViewMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int realPos = position;
                boolean hasHeader = (boolean)mListViewMyAccount.getTag(R.id.HAS_LIST_HEADER);
                if(hasHeader){
                    realPos = realPos - 1;
                }

                //Header와 Footer를 추가했더니.. Header는 0번, Footer는 마지막번으로 넘어오는 문제가 발생한다.
                if(realPos < 0 || realPos >= mMyAccountArrayList.size()) return;

                MyAccountInfo info = mMyAccountArrayList.get(realPos);
                mBankStockSelectListener.onSelectListener("028",info.getACNO(),info);

                dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mListViewMyAccount.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Logs.e("onScrollChange - getFirstVisiblePosition : " + mListViewMyAccount.getFirstVisiblePosition());
                    Logs.e("onScrollChange - getLastVisiblePosition : " + mListViewMyAccount.getLastVisiblePosition());
                    Logs.e("onScrollChange - mListBackupIndex : " + mMyAccountListBackupIndex);
                    if(mMyAccountListBackupIndex < 0 || mMyAccountListBackupIndex == mListViewMyAccount.getFirstVisiblePosition()){
                        mMyAccountListBackupIndex = mListViewMyAccount.getFirstVisiblePosition();
                        return;
                    }
                    if(mMyAccountListBackupIndex < mListViewMyAccount.getFirstVisiblePosition()){
                        Logs.e("draw MAX");
                        if(mListViewMyAccount.getLastVisiblePosition() > 7) {
                            resizeDialogMax();
                        }
                    }else{
                        Logs.e("draw ORIGIN");
                        if(mListViewMyAccount.getFirstVisiblePosition() < 1) {
                            resizeDialogOrigin();
                        }
                    }
                    mMyAccountListBackupIndex = mListViewMyAccount.getFirstVisiblePosition();
                }
            });
        }

        //오픈뱅크계좌
        mLayoutOpenBank = findViewById(R.id.layout_openbank);
        mListAdapterOpenBank = new ListAdapterOpenBank();
        mListViewOpenBank= (ListView) findViewById(R.id.listview_openbank);
        mListViewOpenBank.setAdapter(mListAdapterOpenBank);
        mListViewOpenBank.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int realPos = position;
                boolean hasHeader = (boolean)mListViewOpenBank.getTag(R.id.HAS_LIST_HEADER);
                if(hasHeader){
                    realPos = realPos - 1;
                }

                //Header와 Footer를 추가했더니.. Header는 0번, Footer는 마지막번으로 넘어오는 문제가 발생한다.
                if(realPos < 0 || realPos >= mOpenBankAccountArrayList.size()) return;

                OpenBankAccountInfo info = mOpenBankAccountArrayList.get(realPos);

                if(info.isFinishSearchAmount){
                    mBankStockSelectListener.onSelectListener(info.RPRS_FNLT_CD,info.ACNO,info);
                    dismiss();
                }else{
                    Logs.showToast(mContext,"잔액 및 출금가능 금액 조회중입니다.\n잠시만 기다리세요.");
                }
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mListViewOpenBank.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Logs.e("onScrollChange - getFirstVisiblePosition : " + mListViewOpenBank.getFirstVisiblePosition());
                    Logs.e("onScrollChange - getLastVisiblePosition : " + mListViewOpenBank.getLastVisiblePosition());
                    Logs.e("onScrollChange - mListBackupIndex : " + mOpenBankListBackupIndex);
                    if(mOpenBankListBackupIndex < 0 || mOpenBankListBackupIndex == mListViewOpenBank.getFirstVisiblePosition()){
                        mOpenBankListBackupIndex = mListViewOpenBank.getFirstVisiblePosition();
                        return;
                    }
                    if(mOpenBankListBackupIndex < mListViewOpenBank.getFirstVisiblePosition()){
                        Logs.e("draw MAX");
                        if(mListViewOpenBank.getLastVisiblePosition() > 7) {
                            resizeDialogMax();
                        }
                    }else{
                        Logs.e("draw ORIGIN");
                        if(mListViewOpenBank.getFirstVisiblePosition() < 1) {
                            resizeDialogOrigin();
                        }
                    }
                    mOpenBankListBackupIndex = mListViewOpenBank.getFirstVisiblePosition();
                }
            });
        }


        mTextTabMyaccount = (TextView) findViewById(R.id.textview_tab_myaccount);
        mTextTabOpenBank = (TextView) findViewById(R.id.textview_tab_openbank);

        mTextTabMyaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickTabText(SHOW_LIST_MYACCOUNT);
            }
        });

        mTextTabOpenBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickTabText(SHOW_LIST_OPENBANK);
            }
        });


        mListViewMyAccount.setTag(R.id.HAS_LIST_HEADER,false);
        mListViewOpenBank.setTag(R.id.HAS_LIST_HEADER,false);
        //이체화면에서 들어온경우이면 물고들어온 계좌는 보이지 않도록 한다.
        if(isUseTransfer){
            //물고들어온 출금계좌가 있으면 리스트에서 삭제해준다.
            String BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
            String ACNO    = ITransferDataMgr.getInstance().getWTCH_ACNO();

            if(!TextUtils.isEmpty(BANK_CD) && BANK_CD.equalsIgnoreCase("028")){
                for(int i=0;i<mMyAccountArrayList.size();i++){
                    MyAccountInfo accountInfo = mMyAccountArrayList.get(i);
                    if(accountInfo.getACNO().equalsIgnoreCase(ACNO)){
                        mMyAccountArrayList.remove(i);
                    }
                }

                //제거한 리스트에 아이템이 있으면
                if(mMyAccountArrayList.size() > 0){
                    View myAccountHeader = getLayoutInflater().inflate(R.layout.layout_listview_header_myaccount_send_dialog, null, false) ;
                    mListViewMyAccount.addHeaderView(myAccountHeader);
                    mListViewMyAccount.setTag(R.id.HAS_LIST_HEADER,true);
                }

                mListAdapterMyAccount.notifyDataSetChanged();
            }else{
                for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                    OpenBankAccountInfo accountInfo = mOpenBankAccountArrayList.get(i);
                    if(accountInfo.RPRS_FNLT_CD.equalsIgnoreCase(BANK_CD)
                            &&accountInfo.ACNO.equalsIgnoreCase(ACNO)){
                        mOpenBankAccountArrayList.remove(i);
                    }
                }

                //제거한 리스트에 아이템이 있으면
                if(mOpenBankAccountArrayList.size() > 0){
                    View myOpenBankHeader = getLayoutInflater().inflate(R.layout.layout_listview_header_myaccount_send_dialog, null, false) ;
                    mListViewOpenBank.addHeaderView(myOpenBankHeader);
                    mListViewOpenBank.setTag(R.id.HAS_LIST_HEADER,true);
                }
            }
        }

        //웹에서 호출하는 경우가 있는데.. .
        //이때는 mOpenBankAccountArrayList이 조회 전일 수가 있다.
        //그래서 로그인정보에 있는 오픈뱅킹 계좌 정보를 사용하도록 하자.
        String haveOpenBankAccountYN = "N";
        String OBA_ACCO_HOLD_YN = LoginUserInfo.getInstance().getOBA_ACCO_HOLD_YN();
        if(!TextUtils.isEmpty(OBA_ACCO_HOLD_YN) && OBA_ACCO_HOLD_YN.equalsIgnoreCase("Y")){
            haveOpenBankAccountYN = "Y";
        }

        //시작화면을 표시한다.
        if(mMyAccountArrayList.size() == 0 && !haveOpenBankAccountYN.equalsIgnoreCase("Y")){
            mLayoutTab.setVisibility(View.GONE);
            mListViewMyAccount.setVisibility(View.GONE);
            mListViewOpenBank.setVisibility(View.GONE);
            mLayoutEmptyAllAccount.setVisibility(View.VISIBLE);
        }else if(mMyAccountArrayList.size() == 0 && haveOpenBankAccountYN.equalsIgnoreCase("Y")){
            mLayoutEmptyMyAccount.setVisibility(View.VISIBLE);
        }else if(mMyAccountArrayList.size() > 0 && !haveOpenBankAccountYN.equalsIgnoreCase("Y")){
            mLayoutTab.setVisibility(View.GONE);
            mListViewMyAccount.setVisibility(View.VISIBLE);
        }

        mLayoutOpenBank.setVisibility(View.GONE);

        if(ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() != (ITransferDataMgr.ACCESS_TYPE_NORMAL)){
            resizeDialogMax();
        }
    }

    private void clickTabText(int selectTab){
        mTextTabMyaccount.setBackgroundResource(R.drawable.background_tab_unselect_box);
        mTextTabMyaccount.setTextColor(mContext.getResources().getColor(R.color.color666666));

        mTextTabOpenBank.setBackgroundResource(R.drawable.background_tab_unselect_box);
        mTextTabOpenBank.setTextColor(mContext.getResources().getColor(R.color.color666666));


        switch (selectTab){
            case SHOW_LIST_MYACCOUNT:
                mTextTabMyaccount.setBackgroundResource(R.drawable.background_tab_select_box);
                mTextTabMyaccount.setTextColor(mContext.getResources().getColor(R.color.black));

                mLayoutMyAccount.setVisibility(View.VISIBLE);
                mLayoutOpenBank.setVisibility(View.GONE);
                break;
            case SHOW_LIST_OPENBANK:
                mTextTabOpenBank.setBackgroundResource(R.drawable.background_tab_select_box);
                mTextTabOpenBank.setTextColor(mContext.getResources().getColor(R.color.black));

                mLayoutMyAccount.setVisibility(View.GONE);
                mLayoutOpenBank.setVisibility(View.VISIBLE);

                if(mOpenBankAccountArrayList.size()==0){
                    requestOpenBankList();
                }else{
                    if(OpenBankDataMgr.getInstance().isIS_NEED_SEARCH_AMOUNT()){
                        for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                            requestOpenBankDataByAccount(i);
                        }
                        OpenBankDataMgr.getInstance().setIS_NEED_SEARCH_AMOUNT(false);
                    }
                }

                break;
        }
    }

    /**
     * 오픈뱅킹 계좌 조회
     */
    private void requestOpenBankList(){
        Map param = new HashMap();
        param.put("BLNC_INQ_YN", "N");

        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    //한번도 오픈뱅킹 계좌를 조회한 적이 없기에 모델에 데이타를 넣어준다.
                    int accCnt = OpenBankDataMgr.getInstance().parsorAccountList(object);
                    if(accCnt > 0){
                        mOpenBankAccountArrayList.addAll(OpenBankDataMgr.getInstance().getOpenBankInOutAccountList());

                        String BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
                        String ACNO    = ITransferDataMgr.getInstance().getWTCH_ACNO();

                        if(!BANK_CD.equalsIgnoreCase("028")){
                            //물고들어온 오픈뱅킹 계좌 정보가 있으면 리스트에서 삭제해준다.
                            for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                                OpenBankAccountInfo accountInfo = mOpenBankAccountArrayList.get(i);
                                if(accountInfo.RPRS_FNLT_CD.equalsIgnoreCase(BANK_CD)
                                        &&accountInfo.ACNO.equalsIgnoreCase(ACNO)){
                                    mOpenBankAccountArrayList.remove(i);
                                }
                            }
                        }

                        mListAdapterOpenBank.notifyDataSetChanged();

                        if(mOpenBankAccountArrayList.size() > 0){
                            mLayoutEmptyOpenBank.setVisibility(View.GONE);
                            //리스트 출력 후 약간의 딜레이를 주도록...
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                                        requestOpenBankDataByAccount(i);
                                    }
                                    OpenBankDataMgr.getInstance().setIS_NEED_SEARCH_AMOUNT(false);
                                }
                            },500);
                        }else{
                            mLayoutEmptyOpenBank.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    /**
     * 오픈뱅킹 계좌별로 잔액,출금가능금액 조회
     */
    private void requestOpenBankDataByAccount(final int index){
        Map param = new HashMap();

        OpenBankAccountInfo info = mOpenBankAccountArrayList.get(index);

        param.put("RPRS_FNLT_CD", info.RPRS_FNLT_CD);
        param.put("ACNO", info.ACNO);
        param.put("BLNC_INQ_YN", "Y");


        HttpUtils.sendHttpTask(WasServiceUrl.CMM0140200A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                //다이얼로그가 닫힌상태면 업데이트 하지 않도록 하자.
                if(!SlidingExpandSelectSenderAccountDialog.this.isShowing()) return;

                if(onCheckHttpError(ret,false)){
                    mOpenBankAccountArrayList.get(index).BLNC_AMT="0";
                    mOpenBankAccountArrayList.get(index).WTCH_POSB_AMT="0";
                    mOpenBankAccountArrayList.get(index).ACNO_REG_STCD = "3";
                    mOpenBankAccountArrayList.get(index).isFinishSearchAmount = true;
                    mListAdapterOpenBank.notifyDataSetChanged();
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    //계좌번호
                    String ACNO = object.optString("ACCO");
                    //계좌잔액
                    String ACCO_BLNC = object.optString("ACCO_BLNC");
                    //출금가능금액
                    String WTCH_POSB_AMT = object.optString("WTCH_POSB_AMT");

                    mOpenBankAccountArrayList.get(index).BLNC_AMT = ACCO_BLNC;
                    mOpenBankAccountArrayList.get(index).WTCH_POSB_AMT = WTCH_POSB_AMT;
                    mOpenBankAccountArrayList.get(index).isFinishSearchAmount = true;
                    mOpenBankAccountArrayList.get(index).ACNO_REG_STCD = "1";
                    mListAdapterOpenBank.notifyDataSetChanged();

                    //오픈뱅킹 데이터 모델에도 업데이트 해준다.
                    String BANK_CD = mOpenBankAccountArrayList.get(index).RPRS_FNLT_CD;
                    OpenBankDataMgr.getInstance().updateAccountAmount(BANK_CD,ACNO,ACCO_BLNC,WTCH_POSB_AMT);

                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    class ListAdapterMyAccount extends BaseAdapter {
        @Override
        public int getCount() {
            return mMyAccountArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return mMyAccountArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewBankHolder;

            if (convertView == null) {
                viewBankHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.dialog_sliding_expand_select_sender_account_list_item, null);

                viewBankHolder.layoutItem = convertView.findViewById(R.id.layout_item);
                viewBankHolder.imageIcon  = convertView.findViewById(R.id.iv_icon);
                viewBankHolder.textAccountName = convertView.findViewById(R.id.tv_account_name);
                viewBankHolder.imageIconCouple  = convertView.findViewById(R.id.iv_couple_icon);
                viewBankHolder.imageIconDelay  = convertView.findViewById(R.id.iv_delay_service_icon);
                viewBankHolder.imageIconDsgt  = convertView.findViewById(R.id.iv_dsgt_acc_icon);
                viewBankHolder.textWithdrawPosibleAmount = convertView.findViewById(R.id.tv_withdraw_posible_amount);
                viewBankHolder.textAccountNum = convertView.findViewById(R.id.tv_account_num);
                viewBankHolder.textAmount = convertView.findViewById(R.id.tv_amount);
                viewBankHolder.viewLine = convertView.findViewById(R.id.v_line);

                convertView.setTag(viewBankHolder);
            } else {
                viewBankHolder = (ViewHolder) convertView.getTag();
            }

            viewBankHolder.imageIcon.setVisibility(View.GONE);

            MyAccountInfo info = mMyAccountArrayList.get(position);

            String ACCO_ALS = info.getACCO_ALS();
            if (TextUtils.isEmpty(ACCO_ALS)) {
                ACCO_ALS = info.getPROD_NM();
            }
            if(!TextUtils.isEmpty(ACCO_ALS) && ACCO_ALS.length() > 8){
                ACCO_ALS = ACCO_ALS.substring(0,8) + Const.ELLIPSIS;
            }

            viewBankHolder.textAccountName.setText(ACCO_ALS);


            //커플통장여부
            if(info.getSHRN_ACCO_YN().equalsIgnoreCase("Y")){
                viewBankHolder.imageIconCouple.setVisibility(View.VISIBLE);
            }else{
                viewBankHolder.imageIconCouple.setVisibility(View.GONE);
            }

            //지연이체서비스 가입여부
            if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                viewBankHolder.imageIconDelay.setVisibility(View.VISIBLE);
            }else{
                viewBankHolder.imageIconDelay.setVisibility(View.GONE);
            }

            //입금지정계좌서비스 가입여부
            if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                viewBankHolder.imageIconDsgt.setVisibility(View.VISIBLE);
            }else{
                viewBankHolder.imageIconDsgt.setVisibility(View.GONE);
            }


            String accountNum = info.getACNO();
            if (!TextUtils.isEmpty(accountNum)) {
                String account = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
                viewBankHolder.textAccountNum.setText(Const.BANK_ALS_NAME + " "  + account);
            }

            //출금가능금액
            String WTCH_POSB_AMT = info.getWTCH_POSB_AMT();
            if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                viewBankHolder.textWithdrawPosibleAmount.setText(amount + " 원");
            }

            //잔액
            String ACCO_BLNC = info.getACCO_BLNC();
            if (!TextUtils.isEmpty(ACCO_BLNC)) {
                String amount = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
                viewBankHolder.textAmount.setText(amount + " 원");
            }

            if(position+1 == getCount()){
                viewBankHolder.viewLine.setVisibility(View.INVISIBLE);
            }else{
                viewBankHolder.viewLine.setVisibility(View.VISIBLE);
            }

            return convertView;
        }
    }

    class ListAdapterOpenBank extends BaseAdapter {
        @Override
        public int getCount() {
            return mOpenBankAccountArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return mOpenBankAccountArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewBankHolder;

            if (convertView == null) {
                viewBankHolder = new ViewHolder();
                LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.dialog_sliding_expand_select_sender_account_list_item, null);

                viewBankHolder.layoutItem = convertView.findViewById(R.id.layout_item);
                viewBankHolder.imageIcon  = convertView.findViewById(R.id.iv_icon);
                viewBankHolder.textAccountName = convertView.findViewById(R.id.tv_account_name);
                viewBankHolder.textWithdrawPosibleAmount = convertView.findViewById(R.id.tv_withdraw_posible_amount);
                viewBankHolder.textAccountNum = convertView.findViewById(R.id.tv_account_num);
                viewBankHolder.textAmount = convertView.findViewById(R.id.tv_amount);
                viewBankHolder.viewLine = convertView.findViewById(R.id.v_line);
                viewBankHolder.imageLoading = convertView.findViewById(R.id.iv_loading_icon);

                viewBankHolder.layoutAmount = convertView.findViewById(R.id.layout_amount);
                viewBankHolder.layoutWithDrawAmount = convertView.findViewById(R.id.layout_withdraw_posible_amount);

                convertView.setTag(viewBankHolder);
            } else {
                viewBankHolder = (ViewHolder) convertView.getTag();
            }


            OpenBankAccountInfo info = mOpenBankAccountArrayList.get(position);

            String ACCO_ALS = info.PROD_NM;
            if(!TextUtils.isEmpty(info.ACCO_ALS))
                ACCO_ALS = info.ACCO_ALS;

            if(!TextUtils.isEmpty(ACCO_ALS) && ACCO_ALS.length() > 8){
                ACCO_ALS = ACCO_ALS.substring(0,8) + Const.ELLIPSIS;
            }
            viewBankHolder.textAccountName.setText(ACCO_ALS);


            String accountNum = info.ACNO;
            if(info.RPRS_FNLT_CD.equalsIgnoreCase("000")){
                if (!TextUtils.isEmpty(accountNum)) {
                    accountNum = accountNum.substring(0, 5) + "-" + accountNum.substring(5, 7) + "-" + accountNum.substring(7, accountNum.length());
                }
            }
            viewBankHolder.textAccountNum.setText(info.MNRC_BANK_NM + " " + accountNum);


            if(info.isFinishSearchAmount){
                //출금가능금액
                String WTCH_POSB_AMT = info.WTCH_POSB_AMT;
                if (!TextUtils.isEmpty(WTCH_POSB_AMT)) {
                    String amount = Utils.moneyFormatToWon(Double.valueOf(WTCH_POSB_AMT));
                    viewBankHolder.textWithdrawPosibleAmount.setText(amount + " 원");
                }

                //잔액
                String ACCO_BLNC = info.BLNC_AMT;
                if (!TextUtils.isEmpty(ACCO_BLNC)) {
                    String amount = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
                    viewBankHolder.textAmount.setText(amount + " 원");
                }

                viewBankHolder.layoutAmount.setVisibility(View.VISIBLE);
                viewBankHolder.layoutWithDrawAmount.setVisibility(View.VISIBLE);


                viewBankHolder.imageLoading.setVisibility(View.INVISIBLE);
            }else{
                viewBankHolder.layoutAmount.setVisibility(View.INVISIBLE);
                viewBankHolder.layoutWithDrawAmount.setVisibility(View.INVISIBLE);

                viewBankHolder.imageLoading.setVisibility(View.VISIBLE);
            }

            //은행 아이콘 출력
            Uri iconUri = ImgUtils.getIconUri(mContext,info.RPRS_FNLT_CD,info.DTLS_FNLT_CD);
            if(iconUri != null)
                viewBankHolder.imageIcon.setImageURI(iconUri);
            else
                viewBankHolder.imageIcon.setImageResource(R.drawable.img_logo_xxx);
            viewBankHolder.imageIcon.setVisibility(View.VISIBLE);


            if(position+1 == getCount()){
                viewBankHolder.viewLine.setVisibility(View.INVISIBLE);
            }else{
                viewBankHolder.viewLine.setVisibility(View.VISIBLE);
            }

            return convertView;
        }
    }

    public static class ViewHolder {
        LinearLayout layoutItem;
        ImageView    imageIcon;
        TextView     textAccountName;
        ImageView    imageIconCouple;
        ImageView    imageIconDelay;
        ImageView    imageIconDsgt;
        TextView     textWithdrawPosibleAmount;
        TextView     textAccountNum;
        TextView     textAmount;
        View         viewLine;
        ImageView    imageLoading;

        LinearLayout layoutAmount;
        LinearLayout layoutWithDrawAmount;

    }

    private void showProgressDialog(){
        if (mContext.isFinishing()) return;

        if(mProgressBarLayout.getVisibility() != View.VISIBLE)
            mProgressBarLayout.setVisibility(View.VISIBLE);
    }

    private void dismissProgressDialog(){
        if (mContext.isFinishing()) return;
        Logs.e("dismissProgressDialog - 1");

        mProgressBarLayout.setVisibility(View.GONE);
    }

    public interface OnSelectBankStockListener {
        void onSelectListener(String bankCd, String accountNo,Object info);
    }
}
