package com.sbi.saidabank.common.util;

import android.util.Log;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.define.Const;

/**
 * MLog : 로그확인
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-02-24
 */
public class MLog {

    public static final String TAG = MLog.class.getSimpleName();

    //private static final boolean LOG = false;//BuildConfig.DEBUG;
    private static final boolean LOG = BuildConfig.DEBUG;

    /**
     * Log.d에 대응한다.
     */
    public static void d() {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.d(TAG, getSimpleClassName(st[3].getClassName()) + " :: " + generateMessage(st[3], null));
        }
    }

    /**
     * Log.d에 대응한다.
     *
     * @param msg 메시지
     */
    public static void d(String msg) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.d(TAG, getSimpleClassName(st[3].getClassName()) + " :: " + generateMessage(st[3], null) + msg);
        }
    }

    /**
     * Log.v에 대응한다.
     *
     * @param msg 메시지
     */
    public static void v(String msg) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.v(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg));
        }
    }

    /**
     * Log.w에 대응한다.
     *
     * @param msg 메시지
     */
    public static void w(String msg) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.w(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg));
        }
    }

    /**
     * Log.w에 대응한다.
     *
     * @param msg 메시지
     * @param t   Throwable
     */
    public static void w(String msg, Throwable t) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.w(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg), t);
        }
    }

    /**
     * Log.w에 대응한다.
     *
     * @param msg 메시지
     * @param e   Exception
     */
    public static void w(String msg, Exception e) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.w(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg), e);
            e.getMessage();
        }
    }

    /**
     * Log.e에 대응한다.
     *
     * @param msg 메시지
     */
    public static void e(String msg) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.e(TAG, getSimpleClassName(st[3].getClassName()) + " :: " + generateMessage(st[3], msg));
        }
    }

    /**
     * Log.e에 대응한다.
     *
     * @param msg 메시지
     * @param t   Throwable
     */
    public static void e(String msg, Throwable t) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.e(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg), t);
        }
    }

    /**
     * Log.e에 대응
     *
     * @param msg 메시지
     * @param e   Exception
     */
    public static void e(String msg, Exception e) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.e(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg), e);
            e.getMessage();
        }
    }

    /**
     * Log.e에 대응한다.
     *
     * @param msg 메시지
     * @param e   Exception 메시지
     */
    public static void e(String msg, String e) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.e(getSimpleClassName(st[3].getClassName()), generateMessage(st[3], msg) + " >> " + e);
        }
    }

    /**
     * Log.e에 대응한다.
     *
     * @param e Exception 메시지
     */
    public static void e(Exception e) {
        if (LOG) {
            Log.e(TAG, e.getMessage());
        }
    }

    /**
     * Log.i에 대응한다.
     *
     * @param msg 메시지
     */
    public static void i(String msg) {
        if (LOG) {
            StackTraceElement[] st = Thread.currentThread().getStackTrace();
            Log.i(TAG, getSimpleClassName(st[3].getClassName()) + " :: " + generateMessage(st[3], " " + msg));
        }
    }

    public static void info(String msg) {
        if (LOG)
            Log.i(TAG, msg);
    }

    public static void line() {
        if (LOG)
            Log.i(TAG, "==================================================");
    }

    /**
     * from full class name to simple class name
     *
     * @param className
     * @return
     */
    private static String getSimpleClassName(String className) {
        int idx = className.lastIndexOf(".");
        return idx >= 0 ? className.substring(idx + 1) : className;
    }

    /**
     * 메세지 포멧으로 문자열 만든다.
     *
     * @param ste 마지막에 호출된 StackTrace
     * @param msg 작성할 로그메시지
     * @return
     */
    private static String generateMessage(StackTraceElement ste, String msg) {
        StringBuilder sb = new StringBuilder(ste.getMethodName());
        sb.append("[").append(ste.getLineNumber()).append("]");
        sb.append("");
        if (msg != null)
            sb.append(msg);
        return sb.toString();
    }

    /**
     * 로그 메시지에 기타 정보를 포함하여 반환한다.
     *
     * @param msg 메시지
     * @return
     */
    protected static String getFullMsg(String msg) {
        if (msg == null)
            msg = "";
        StackTraceElement stackTraceElement = Thread.currentThread().getStackTrace()[4];
        String className = stackTraceElement.getClassName();
        className = className.substring(className.lastIndexOf(Const.DOT) + 1);
        String methodName = stackTraceElement.getMethodName();
        msg = className + "." + methodName + " : " + msg;
        return msg;
    }
}
