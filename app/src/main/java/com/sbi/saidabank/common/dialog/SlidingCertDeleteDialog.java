package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: SlidingCertDeleteDialog
 * Created by 950469 on 2018. 10. 11..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 계좌선택 다이얼로그
 */
public class SlidingCertDeleteDialog extends SlidingBaseDialog implements View.OnClickListener{
    private Context  context;
    private String   certName;
    private String   certType;
    private String   certValidityDate;

    public View.OnClickListener listener;

    public SlidingCertDeleteDialog(@NonNull Context context, String certName, String certType, String certValidityDate, View.OnClickListener listener) {
        super(context);
        this.context = context;
        this.certName = certName;
        this.certType = certType;
        this.certValidityDate = certValidityDate;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.dialog_cert_delete);
        setDialogWidth();

        TextView textName = (TextView) findViewById(R.id.textview_cert_name);
        if (!TextUtils.isEmpty(certName))
            textName.setText(certName);

        TextView textType = (TextView) findViewById(R.id.textview_cert_type);
        if (!TextUtils.isEmpty(certType))
            textType.setText(certType);

        TextView textValidityDate = (TextView) findViewById(R.id.textview_validity_date);
        if (!TextUtils.isEmpty(certValidityDate))
            textValidityDate.setText(certValidityDate);

        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancel:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }
                break;

            case R.id.btn_ok:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }

                if (listener != null) {
                    listener.onClick(v);
                }
                break;

            default:
                break;
        }
    }
}
