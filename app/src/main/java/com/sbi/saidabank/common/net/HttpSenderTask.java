package com.sbi.saidabank.common.net;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.ParseUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.solution.jex.JexAESEncrypt;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * HttpSenderTask : 전문 비동기 통신
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class HttpSenderTask extends AsyncTask<String, Void, String> {

    private String mUrl;
    private HttpRequestListener mListener;

    public interface HttpRequestListener {
        void endHttpRequest(String ret);
    }

    public interface HttpRequestListener2 {
        void endHttpRequest(boolean result,String ret);
    }

    public HttpSenderTask(String url, HttpRequestListener listener) {
        this.mUrl = url;
        this.mListener = listener;
    }

    @Override
    protected String doInBackground(String... params) {

        String retString = null;

        /** WAS로 전송하는 데이타는 구간 암호화를 하기위해 Jex파일만 암호화 하도록 수정 */
        if (mUrl.contains(SaidaUrl.DEV_SERVER) || mUrl.contains(SaidaUrl.OPER_SERVER) || mUrl.contains(SaidaUrl.TEST_SERVER)) {
            if (mUrl.contains(".act") || mUrl.contains(".jct")) {
                /** SBI서버로 접근하는 데이타는 Json형식을 유지해야됨. 암호화 하기 전에 데이타 체크필요. */
                try {
                    String encText = JexAESEncrypt.encrypt(params[0]);
                    String encRetText = HttpSender.requestJexEncryptPost(mUrl, encText);
                    // 기기상태 체크 전문 + 응답값이 HTML일 경우
                    if (mUrl.equals(WasServiceUrl.CMM0010300A01.getServiceUrl()) && Utils.isHtmlFormat(encRetText)) {
                        retString = encRetText;
                    } else {
                        retString = JexAESEncrypt.decrypt(encRetText);
                    }
                } catch (Exception e) {
                    MLog.e(e);
                }
            } else {
                if (TextUtils.isEmpty(params[0])) {
                    retString = HttpSender.requestGet(mUrl);
                } else {
                    retString = HttpSender.requestPost(mUrl, params[0]);
                }
            }
        } else {
            if (TextUtils.isEmpty(params[0])) {
                retString = HttpSender.requestGet(mUrl);
            } else {
                retString = HttpSender.requestPost(mUrl, params[0]);
            }
        }

        Logs.printJsonLog(mUrl, params[0], retString);
        //20210512 - 폰 메모리 덤프뜨면 파라미터 값이 출력되어서 초기화 코드 추가.
        params[0] = "";
        return retString;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);
        Logs.e("sendHttp reuslt : " + response);
        mListener.endHttpRequest(response);
    }


}
