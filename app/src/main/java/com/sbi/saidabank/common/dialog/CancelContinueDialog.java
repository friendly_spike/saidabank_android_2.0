package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;


public class CancelContinueDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;

    public String msg;

    public String mNBtText = "";
    public String mPBtText = "";

    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;

    public CancelContinueDialog(Context context) {
        super(context);
        mContext = context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_cancel_continue);

        setDialogWidth();

        initView();
    }

    private void initView(){
        setCancelable(false);

        TextView textDesc = (TextView) findViewById(R.id.textview_cancel_desc);
        if (!TextUtils.isEmpty(msg)){
            String sourceString = msg + "하시겠습니까?";
            sourceString += mContext.getString(R.string.msg_cancel_cotinue);
            textDesc.setText(sourceString);
        }

        Button mBtNegative = (Button) findViewById(R.id.bt_negative);
        Button mBtPositive = (Button) findViewById(R.id.bt_positive);
        mBtNegative.setOnClickListener(this);
        mBtPositive.setOnClickListener(this);

        if (mNBtText != null && !"".equals(mNBtText)) {
            mBtNegative.setText(mNBtText);
        }

        if (mPBtText != null && !"".equals(mPBtText)) {
            mBtPositive.setText(mPBtText);
        }

        if (mNListener == null) {
            mBtNegative.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        } else {
            mBtNegative.setBackgroundResource(R.drawable.selector_radius_left_cancelbtn);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_right_okbtn);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_negative:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }

                if (mNListener != null) {
                    mNListener.onClick(view);
                }
                break;

            case R.id.bt_positive:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }

                if (mPListener != null) {
                    mPListener.onClick(view);
                }
                break;

            default:
                break;
        }
    }
}
