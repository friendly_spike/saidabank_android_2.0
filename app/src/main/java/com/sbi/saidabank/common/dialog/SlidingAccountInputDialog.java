package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.safedeal.CustomNumPadLayout;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

import static android.view.View.VISIBLE;

/**
 * Saidabank_android
 * Class: SlidingAccountInputDialog
 * Created on 2021. 01. 11..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 계좌입력 다이얼로그
 */
public class SlidingAccountInputDialog extends SlidingBaseDialog implements View.OnClickListener,CustomNumPadLayout.OnNumPadClickListener{
    private Context  context;
    private String   bankName;
    private String   bankCd;
    private String   accountNum;

    public OnFinishAccountInputListener listener;

    //은행증권사영역
    private LinearLayout mLayoutBankStock;
    private ImageView mIvBankIcon;
    private TextView  mTvBankName;

    //계좌입력부
    private EditText mEtAccNum;
    private Button mBtnOk;
    private boolean isOpenBank;

    public SlidingAccountInputDialog(@NonNull Context context, String bank_cd, String bank_name,boolean isOpenBank,String account_num,OnFinishAccountInputListener listener) {
        super(context);
        this.context = context;
        this.bankCd = bank_cd;
        this.bankName = bank_name;
        this.listener = listener;
        this.isOpenBank = isOpenBank;
        this.accountNum = account_num;
    }

    public SlidingAccountInputDialog(@NonNull Context context, String bank_cd, String bank_name, OnFinishAccountInputListener listener) {
        super(context);
        this.context = context;
        this.bankCd = bank_cd;
        this.bankName = bank_name;
        this.listener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.dialog_account_input);
        setDialogWidth();

        initUX();
    }

    private void initUX(){

        int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);
        findViewById(R.id.layout_body).setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,0,0}));

        mIvBankIcon = findViewById(R.id.iv_bank_img);
        Uri iconUri = ImgUtils.getIconUri(getContext(),bankCd);
        if(iconUri != null){
            mIvBankIcon.setImageURI(iconUri);
        }else{
            mIvBankIcon.setImageResource(R.drawable.img_logo_xxx);
        }

        mTvBankName = findViewById(R.id.tv_bank_name);
        mTvBankName.setText(bankName);

        mLayoutBankStock = findViewById(R.id.layout_bank_stock);
        mLayoutBankStock.setOnClickListener(this);


        mEtAccNum = findViewById(R.id.et_account_num);
        mEtAccNum.setTextIsSelectable(true);
        //포커스시 키패드 올라오지 않도록...
        mEtAccNum.setShowSoftInputOnFocus(false);
        mEtAccNum.requestFocus();

        if(!TextUtils.isEmpty(accountNum)){
            mEtAccNum.setText(accountNum);
        }

        mEtAccNum.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

            }
        });

        mEtAccNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (!TextUtils.isEmpty(mTvBankName.getText().toString()) && mIvBankIcon.getVisibility() == VISIBLE) {

                    if (editable.length() >= Const.MAX_TRANSFER_ACOOUNT_LENGTH) {
                        mBtnOk.setEnabled(true);
                    } else{
                        mBtnOk.setEnabled(false);
                    }
                }
            }
        });

        mBtnOk = findViewById(R.id.btn_ok);
        mBtnOk.setOnClickListener(this);
        if(!TextUtils.isEmpty(accountNum)){
            mBtnOk.setEnabled(true);
        }

        CustomNumPadLayout numPadLayout = findViewById(R.id.numpad);
        numPadLayout.setNumPadClickListener(this);

        if(!isOpenBank){
            findViewById(R.id.tv_myaccount_noti).setVisibility(View.GONE);
            int height = (int)Utils.dpToPixel(getContext(),512);
            setDialogHeight(height);
        }

        //입력창 레이아웃
        RelativeLayout editTextLayout = findViewById(R.id.layout_edittext);
        int radius_et = (int) Utils.dpToPixel(getContext(),7f);
        editTextLayout.setBackground(GraphicUtils.getRoundCornerDrawable(ContextCompat.getColor(getContext(),R.color.colorF5F5F5),new int[]{radius_et,radius_et,radius_et,radius_et},1,ContextCompat.getColor(getContext(),R.color.colorE5E5E5)));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.layout_bank_stock:
                dismiss();
                if (listener != null) {
                    listener.onClickBack();
                }

                dismiss();
                break;
            case R.id.btn_ok:
                dismiss();
                if (listener != null) {
                    listener.onEditFinishListener(bankCd, bankName,mEtAccNum.getText().toString());
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onNumPadClick(String numStr) {
        switch (numStr){
            case "cancel": {
                mEtAccNum.setText("");
                break;
            }
            case "del": {
                int cursor_start = mEtAccNum.getSelectionStart();
                int cursor_end = mEtAccNum.getSelectionEnd();

                String inputText = mEtAccNum.getText().toString();

                String frontStr = inputText.substring(0,cursor_start);
                String backStr = inputText.substring(cursor_end,inputText.length());

                if(cursor_start == cursor_end){
                    if(cursor_start == 0) return;

                    String newFrontStr = frontStr.substring(0,cursor_start-1);
                    mEtAccNum.setText(newFrontStr+backStr);
                    mEtAccNum.setSelection(newFrontStr.length());
                }else{
                    mEtAccNum.setText(frontStr+backStr);
                    mEtAccNum.setSelection(frontStr.length());
                }
                break;
            }
            default: {
                int cursor_start = mEtAccNum.getSelectionStart();
                int cursor_end = mEtAccNum.getSelectionEnd();

                String inputText = mEtAccNum.getText().toString();

                if(inputText.length() >= 20) return;

                String frontStr = inputText.substring(0,cursor_start);
                String backStr = inputText.substring(cursor_end,inputText.length());

                String newStr = frontStr + numStr + backStr;

                mEtAccNum.setText(newStr);
                mEtAccNum.setSelection(frontStr.length() + 1);
                break;
            }
        }
    }

    public interface OnFinishAccountInputListener {
        void onClickBack();
        void onEditFinishListener(String bankCode, String bankName, String accountNo);
    }
}
