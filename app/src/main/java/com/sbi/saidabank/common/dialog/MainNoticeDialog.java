package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

/**
 * MainNoticeDialog : 공지사항 알림 팝업 다이얼로그
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-10-06
 */
public class MainNoticeDialog extends Dialog {

    public String mLeftText = Const.EMPTY;
    public String mRightText = Const.EMPTY;
    private MainPopupInfo mMainPopupInfo;
    private Transformation mTransformation;
    private Context mContext;
    private OnListener mListener;

    public interface OnListener {
        void onImageClick();
        void onCloseClick();
        void onNoShowClick();
    }

    public MainNoticeDialog(Context context, MainPopupInfo mainPopupInfo, OnListener clickListener) {
        super(context, R.style.TransparentProgressDialog);
        this.mContext = context;
        this.mMainPopupInfo = mainPopupInfo;
        this.mListener = clickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_main_notice);
        setDialogSize();
        initUX();
    }

    @Override
    public void show() {
        MLog.d();
        super.show();
    }

    private void initUX() {

        ImageView imageNotice = (ImageView) findViewById(R.id.imageview_main_notice);
        TextView textNotice = (TextView) findViewById(R.id.textview_main_notice);
        Button btnClose = (Button) findViewById(R.id.btn_close_main_notice);
        Button btnNoShow = (Button) findViewById(R.id.btn_no_show_today);

        if (DataUtil.isNotNull(mMainPopupInfo)) {
            if ("I".equalsIgnoreCase(mMainPopupInfo.getPUP_CNTN_DVCD())) {
                if (DataUtil.isNotNull(mMainPopupInfo.getIMG_URL_ADDR())) {
                    String url = SaidaUrl.getBaseWebUrl() + Const.SLASH + mMainPopupInfo.getIMG_URL_ADDR();
                    if (mTransformation.key().equals("resizeTransformation")) {
                        Picasso.with(mContext)
                                .load(url)
                                .placeholder(R.drawable.background_radius_notice_box)
                                .transform(mTransformation)
                                .into(imageNotice);
                    } else {
                        Picasso.with(mContext)
                                .load(url)
                                .placeholder(R.drawable.background_radius_notice_box)
                                .fit()
                                .transform(mTransformation)
                                .into(imageNotice);
                    }
                }
                imageNotice.setVisibility(View.VISIBLE);
                textNotice.setVisibility(View.GONE);
            } else {
                imageNotice.setVisibility(View.GONE);
                textNotice.setVisibility(View.VISIBLE);
                textNotice.setText(DataUtil.isNotNull(mMainPopupInfo.getPUP_CNTN()) ? mMainPopupInfo.getPUP_CNTN() : Const.EMPTY);
            }
        }

        if (DataUtil.isNotNull(mLeftText) && DataUtil.isNotNull(mRightText)) {
            btnClose.setText(mLeftText);
            btnNoShow.setText(mRightText);
        } else if (DataUtil.isNotNull(mLeftText) && DataUtil.isNull(mRightText)) {
            btnClose.setText(mLeftText);
            btnClose.setBackgroundResource(R.drawable.selector_radius_cancelbtn);
            btnClose.setVisibility(View.VISIBLE);
            btnNoShow.setVisibility(View.GONE);
        } else if (DataUtil.isNull(mLeftText) && DataUtil.isNotNull(mRightText)) {
            btnNoShow.setText(mRightText);
            btnNoShow.setBackgroundResource(R.drawable.selector_radius_okbtn);
            btnNoShow.setVisibility(View.VISIBLE);
            btnClose.setVisibility(View.GONE);
        } else {
            btnClose.setVisibility(View.GONE);
            btnNoShow.setVisibility(View.GONE);
        }

        imageNotice.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onImageClick();
                }
                dismiss();
            }
        });

        btnClose.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onCloseClick();
                }
                dismiss();
            }
        });

        btnNoShow.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onNoShowClick();
                }
                dismiss();
            }
        });
    }

    public  void setTransformation(Transformation transformation) {
        this.mTransformation = transformation;
    }

    private void setDialogSize() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        getWindow().setAttributes(lp);
    }
}
