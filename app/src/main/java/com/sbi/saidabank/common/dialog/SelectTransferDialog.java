package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;

/**
 * Saidabank_android
 * Class: SlidingConfirmSingleTransferDialog
 * Created by 950485 on 2019. 04. 17..
 * <p>
 * Description:이체 방법 선택 다이얼로그
 */
public class SelectTransferDialog extends BaseDialog {

    private Context mContext;
    private OnConfirmListener mListener;

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
    }

    public SelectTransferDialog(@NonNull Context context, OnConfirmListener listener) {
        super(context);
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_transfer);
        setDialogWidth();
        setCancelable(true);
        RelativeLayout btnAccount = (RelativeLayout) findViewById(R.id.layout_transfer_account);
        btnAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onConfirmPress(1);
                }
            }
        });

        RelativeLayout btnPhone = (RelativeLayout) findViewById(R.id.layout_transfer_phone);
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onConfirmPress(2);
                }
            }
        });

        RelativeLayout btnSafeDeal = (RelativeLayout) findViewById(R.id.layout_transfer_safedeal);
        btnSafeDeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onConfirmPress(3);
                }
            }
        });
    }
}
