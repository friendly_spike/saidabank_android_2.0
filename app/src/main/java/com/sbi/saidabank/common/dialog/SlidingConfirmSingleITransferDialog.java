package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRemitteeInfo;

/**
 * SlidingConfirmSingleTransferDialog : 단건이체 확인 다이얼로그
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class SlidingConfirmSingleITransferDialog extends SlidingBaseDialog {

    private Context mContext;
    private FinishListener mFinishListener;

    public interface FinishListener {
        void OnOKListener();
        void OnCancelListener();
    }

    public SlidingConfirmSingleITransferDialog(@NonNull Context context, FinishListener finishListener) {
        super(context);
        this.mContext = context;
        this.mFinishListener = finishListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_confirm_single_itransfer);
        setDialogWidth();
        setCanceledOnTouchOutside(false);
        initLayout();
    }
// ==============================================================================================================
// UI
// ==============================================================================================================
    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {

        if(ITransferDataMgr.getInstance().getRemitteInfoArrayList().size() == 0) return;

        ITransferRemitteeInfo remitteeInfo = ITransferDataMgr.getInstance().getRemitteInfoArrayList().get(0);

        Button mBtnCancel = (Button) findViewById(R.id.btn_cancel_confirm_single_transfer);
        mBtnCancel.setOnClickListener(onCancelClickListener);

        Button mBtnOK = (Button) findViewById(R.id.btn_ok_confirm_single_transfer);
        mBtnOK.setOnClickListener(onOkClickListener);

        TextView mTvName = (TextView) findViewById(R.id.textview_recipient_name_single);
        mTvName.setText(remitteeInfo.getRECV_NM());

        TextView mTvAccount = (TextView) findViewById(R.id.textview_recipient_account_single);
        mTvAccount.setText(getAccountNm(remitteeInfo));

        TextView textAmount = (TextView) findViewById(R.id.textview_amount_single_account);

        String amount = "0";
        if(!TextUtils.isEmpty(remitteeInfo.getTRN_AMT())){
            amount = Utils.moneyFormatToWon(Double.valueOf(remitteeInfo.getTRN_AMT()));
        }
        textAmount.setText(amount);

        TextView textFee = (TextView) findViewById(R.id.textview_fee_single_account);
        if(remitteeInfo.getFEE().equalsIgnoreCase("0")){
            textFee.setText("면제");
        }else{
            textFee.setText(remitteeInfo.getFEE() + " 원");
        }


        //출금계좌
        String bankAccount = "";
//        String WTCH_BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();

        String accountNo = ITransferDataMgr.getInstance().getWTCH_ACNO();
        String shortAccNo = accountNo.substring(accountNo.length()-4,accountNo.length());

        String accName = ITransferDataMgr.getInstance().getWTCH_ACNO_NM();
        if(!TextUtils.isEmpty(accName) && accName.length() > 14){
            accName = accName.substring(0,14) + Const.ELLIPSIS;
        }
        bankAccount = accName + "[" + shortAccNo + "]";
        TextView textWithdrawBankName = (TextView) findViewById(R.id.textview_withdraw_single_bank_name);
        textWithdrawBankName.setText(bankAccount);



        RelativeLayout layoutSendDate = (RelativeLayout) findViewById(R.id.layout_send_date);
        TextView textSendDate = (TextView) findViewById(R.id.textview_send_date);
        RelativeLayout layoutSendTime = (RelativeLayout) findViewById(R.id.layout_send_time);
        TextView textSendTime = (TextView) findViewById(R.id.textview_send_time);
        LoginUserInfo loginUserInfo = LoginUserInfo.getInstance();
        String code = remitteeInfo.getTRNF_DVCD();
        if (Const.REQUEST_WAS_YES.equalsIgnoreCase(loginUserInfo.getDLY_TRNF_SVC_ENTR_YN())) {

            if ("2".equalsIgnoreCase(code)) {
                String sendTime = getDate(remitteeInfo);
                if (!TextUtils.isEmpty(sendTime)) {
                    textSendDate.setText(sendTime);
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.VISIBLE);
                } else {
                    textSendTime.setText("1".equalsIgnoreCase(remitteeInfo.getTRNF_DVCD()) ? mContext.getString(R.string.Immediately) : mContext.getString(R.string.msg_after_3_hour));
                    layoutSendTime.setVisibility(View.VISIBLE);
                    layoutSendDate.setVisibility(View.GONE);
                }
            } else if ("1".equalsIgnoreCase(code)) {
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.GONE);
            } else if ("3".equalsIgnoreCase(code)) {
                String sendTime = getDate(remitteeInfo);
                if (!TextUtils.isEmpty(sendTime)) {
                    textSendDate.setText(getDate(remitteeInfo));
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.VISIBLE);
                }
            }
        } else if (Const.REQUEST_WAS_YES.equalsIgnoreCase(loginUserInfo.getDSGT_MNRC_ACCO_SVC_ENTR_YN())) {
            String sendTime = getDate(remitteeInfo);
            if (!TextUtils.isEmpty(sendTime)) {
                textSendDate.setText(sendTime);
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.VISIBLE);
            } else {
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.GONE);
            }
        } else {
            if ("3".equalsIgnoreCase(code)) {
                String sendTime = getDate(remitteeInfo);
                if (!TextUtils.isEmpty(sendTime)) {
                    textSendDate.setText(sendTime);
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.VISIBLE);
                } else {
                    layoutSendTime.setVisibility(View.GONE);
                    layoutSendDate.setVisibility(View.GONE);
                }
            } else {
                layoutSendTime.setVisibility(View.GONE);
                layoutSendDate.setVisibility(View.GONE);
            }
        }

        findViewById(R.id.layout_delay_service_noti).setVisibility(View.GONE);
        if(!ITransferDataMgr.getInstance().getWTCH_BANK_CD().equalsIgnoreCase("028")){
            if(LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                findViewById(R.id.layout_delay_service_noti).setVisibility(View.VISIBLE);
            }else if(LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                findViewById(R.id.layout_delay_service_noti).setVisibility(View.VISIBLE);
                ((TextView)findViewById(R.id.tv_service_noti)).setText("오픈뱅킹 서비스 중에는\n입금계좌지정서비스는 제공하지 않습니다.");
            }
        }
    }
// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    /**
     * 계좌명, 계좌번호를 얻는다.
     *
     * @return 계좌명 && 계좌번호
     */
    private String getAccountNm(ITransferRemitteeInfo remitteeInfo) {

        String accNo;
        String cntpFinInstCd = remitteeInfo.getCNTP_FIN_INST_CD();
        String cntpBankAcno = remitteeInfo.getCNTP_BANK_ACNO();
        if ("028".equalsIgnoreCase(cntpFinInstCd)) {
            accNo = cntpBankAcno.substring(0, 5) + Const.DASH + cntpBankAcno.substring(5, 7) + Const.DASH + cntpBankAcno.substring(7, cntpBankAcno.length());
        } else {
            accNo = cntpBankAcno;
        }
        return remitteeInfo.getMNRC_BANK_NM() + " " + accNo + "으로";
    }

    /**
     * 날짜정보를 얻는다.
     *
     * @return 날짜정보
     */
    private String getDate(ITransferRemitteeInfo remitteeInfo) {

        String dt = remitteeInfo.getTRNF_DMND_DT();
        String tm = remitteeInfo.getTRNF_DMND_TM();
        if ((DataUtil.isNotNull(dt) && dt.length() <= 8) && (DataUtil.isNotNull(tm) && tm.length() > 2)) {
            return dt.substring(0, 4) + "." + dt.substring(4, 6) + "." + dt.substring(6, 8) + " " + tm.substring(0, 2) + ":00";
        } else {
            return Const.EMPTY;
        }
    }

    /**
     * 에러 메시지를 출력
     *
     * @param msg 에러내용
     */
    public void showErrorMessage(String msg) {
        if (DataUtil.isNull(msg)) {
            return;
        }
        AlertDialog alertDialog = new AlertDialog(mContext);
        alertDialog.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        };
        alertDialog.msg = msg.replaceAll("\\\\n", "\n");
        alertDialog.show();
    }
// ==============================================================================================================
// LISTENER
// ==============================================================================================================

    /**
     * 취소 클릭 이벤트 리스너
     */
    private View.OnClickListener onCancelClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View view) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            if (DataUtil.isNotNull(mFinishListener)) {
                mFinishListener.OnCancelListener();
            }
            dismiss();
        }
    };

    /**
     * 이체 클릭 이벤트 리스너
     */
    private View.OnClickListener onOkClickListener = new OnSingleClickListener() {
        @Override
        public void onSingleClick(View view) {
            if (mContext instanceof Activity && ((Activity) mContext).isFinishing()) {
                return;
            }
            if (DataUtil.isNotNull(mFinishListener)) {
                mFinishListener.OnOKListener();
            }
            dismiss();
        }
    };
// ==============================================================================================================
// API
// ==============================================================================================================
    /**
     * 선택된 이체목록 제외처리 요청
     */
//    private void requestRemoveTransfer() {
//
//        MLog.d();
//
//        if (DataUtil.isNull(mTransferManager) || mTransferManager.size() < 1 || DataUtil.isNull(mTransferManager.get(0).getUUID_NO())) {
//            return;
//        }
//
//        Map param = new HashMap();
//        param.put("UUID_NO", mTransferManager.get(0).getUUID_NO());
//
//        HttpUtils.sendHttpTask(WasServiceUrl.TRA0010400A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
//            @Override
//            public void endHttpRequest(String ret) {
//
//                if (DataUtil.isNull(ret)) {
//                    showErrorMessage(mContext.getString(R.string.msg_debug_no_response));
//                    return;
//                }
//
//                try {
//                    JSONObject object = new JSONObject(ret);
//                    if (DataUtil.isNull(object)) {
//                        showErrorMessage(mContext.getString(R.string.msg_debug_err_response));
//                        return;
//                    }
//
//                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
//                    if (objectHead == null) {
//                        showErrorMessage(mContext.getString(R.string.common_msg_no_reponse_value_was));
//                        return;
//                    }
//
//                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
//                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
//                        String msg = objectHead.optString(Const.REQUEST_COMMON_MESSAGE);
//                        if (TextUtils.isEmpty(msg))
//                            msg = mContext.getString(R.string.common_msg_no_reponse_value_was);
//                        showErrorMessage(msg);
//                        String errCode = objectHead.optString(Const.REQUEST_COMMON_CODE);
//                        ((BaseActivity) mContext).showCommonErrorDialog(msg, errCode, "", objectHead, true);
//                        return;
//                    }
//
//                    mTransferManager.clear();
//
//                    dismiss();
//
//                } catch (JSONException e) {
//                    MLog.e(e);
//                }
//            }
//        });
//    }
}
