package com.sbi.saidabank.common.download;

public interface DownloadFileCallback {
    void updateProgress(int per);
    void onDownloadComplete(boolean result,String pathName, String fileName);
}
