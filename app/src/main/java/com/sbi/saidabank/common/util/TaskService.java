package com.sbi.saidabank.common.util;

import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import com.sbi.saidabank.solution.v3.V3Manager;

/**
 * TaskService : 앱 종료 서비스
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-04-22
 */
public class TaskService extends Service {

    private boolean isRunning = false;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logs.d("onStartCommand : " + Build.VERSION.SDK_INT);
        if (isRunning)
            return START_NOT_STICKY;
        isRunning = true;
        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        MLog.d();
        //노티바의 V3가 남아있는 경우가 존재하여 앱이 종료되면 사라지도록 코드 추가
        V3Manager.getInstance(getApplicationContext()).stopV3MobilePlus();
        stopSelf();
    }

    @Override
    public void onDestroy() {
        MLog.d();
    }
}
