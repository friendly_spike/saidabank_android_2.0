package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.widget.ImageView;

import com.sbi.saidabank.R;

public class ProgressExDialog extends Dialog {

    public ProgressExDialog(Context context) {
        super(context);
        initUX();
    }

    private void initUX(){
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        getWindow().setDimAmount(0.1f);
        setContentView(R.layout.dialog_progress);
        ImageView imageview = (ImageView) findViewById(R.id.img_progress);
        ((AnimationDrawable) imageview.getBackground()).start();

    }
}
