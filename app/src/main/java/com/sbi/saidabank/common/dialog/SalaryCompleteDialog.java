package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;


public class SalaryCompleteDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;


    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;

    public String mTitle;
    public String mMsg;

    public SalaryCompleteDialog(Context context) {
        super(context);
        mContext = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_salary_complete);

        setDialogWidth();

        initView();
    }

    private void initView(){
        setCancelable(false);

        if(ITransferSalayMgr.getInstance().getCompleteCount() == ITransferSalayMgr.getInstance().getSalayArrayListCount()){

            mTitle = "이체완료";
            mMsg = "정상적으로 이체가 완료되었습니다.\n거래내역을 확인해주세요.";
            ((TextView)findViewById(R.id.tv_title)).setText(mTitle);
        }else{
            ((ImageView)findViewById(R.id.iv_icon)).setImageResource(R.drawable.ico_fail_blue);
            int completeCnt = ITransferSalayMgr.getInstance().getCompleteCount();
            mTitle = "일부성공(" + completeCnt + "/" + (ITransferSalayMgr.getInstance().getSalayArrayListCount()-1) + ")";
            mMsg = "이체가 일부 성공했습니다.\n거래내역을 확인해주세요.";

            Utils.setTextWithSpan( ((TextView)findViewById(R.id.tv_title)),mTitle,String.valueOf(completeCnt),Typeface.NORMAL,"#00ebff");

            ((TextView)findViewById(R.id.tv_title)).setText(mTitle);
        }


        ((TextView)findViewById(R.id.tv_msg)).setText(mMsg);



        Button mBtNegative = (Button)findViewById(R.id.bt_negative);
        mBtNegative.setVisibility(View.GONE);


        Button mBtPositive = (Button)findViewById(R.id.bt_positive);
        mBtPositive.setOnClickListener(this);


        mBtNegative.setVisibility(View.GONE);
        mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_negative:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {

                    dismiss();
                }
                if(mNListener != null) {
                    mNListener.onClick(view);
                }
                break;
            case R.id.bt_positive:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                break;
            default:
                break;
        }
    }
}
