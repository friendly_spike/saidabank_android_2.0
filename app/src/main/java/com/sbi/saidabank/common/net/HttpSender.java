package com.sbi.saidabank.common.net;

import android.annotation.SuppressLint;
import android.os.Build;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import static org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

/**
 * HttpSender : 전문 통신 요청객체
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-28
 */
public class HttpSender {

	private static final int DEFAULT_CONNECT_TIMEOUT = 60 * 1000;    // HTTP 연결에 대한 기본 Timeout
	private static final int DEFAULT_READ_TIMEOUT = 60 * 1000;       // HTTP 연결이후 Input Stream 읽기에 대한 기본 Timeout

	/**
	 * HTTP GET 요청을 수행한다.
	 *
	 * @param urlStr 요청 URL
	 * @return 응답 문자열
	 */
	@SuppressLint("AllowAllHostnameVerifier")
	public static String requestGet(String urlStr) {

		StringBuilder sBuilder = new StringBuilder();
		String body = "";
		URL url = null;
		HttpURLConnection mConn = null;

		try {

			url = new URL(urlStr);

			if (url.getProtocol().toLowerCase().equals("https")) {

				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
					trustAllHosts();

				HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
					https.setHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);
				} else {
					https.setHostnameVerifier(ALWAYS_VERIFY);
				}
				mConn = https;
			} else {
				mConn = (HttpURLConnection) url.openConnection();
			}

			if (mConn != null) {
				mConn.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT);
				mConn.setReadTimeout(DEFAULT_READ_TIMEOUT);
				mConn.setRequestMethod(Const.GET);
				mConn.setUseCaches(false);
				HttpUtils.setCooKie(urlStr, mConn);
				mConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
				mConn.setRequestProperty("User-Agent", HttpUtils.mUserAgent);

				int responseCode = mConn.getResponseCode();
				if (responseCode != HttpURLConnection.HTTP_OK) {
					mConn.disconnect();
					return body;
				}

				HttpUtils.saveCookie(mConn);
				BufferedReader br = new BufferedReader(new InputStreamReader(mConn.getInputStream(), Const.EUC_KR));

				for (; ; ) {
					String line = br.readLine();
					if (line == null) {
						break;
					}
					sBuilder.append(line).append('\n');
				}
				br.close();
				body = sBuilder.toString();
			}
		} catch (Exception e) {
			MLog.e(e);
		} finally {
			if (DataUtil.isNotNull(mConn))
				mConn.disconnect();
		}
		return body.trim();
	}

	/**
	 * HTTP POST 요청을 수행한다.
	 *
	 * @param urlStr 요청 URL
	 * @param param  요청 파라미터 문자열
	 * @return 응답 문자열
	 */
	@SuppressLint("AllowAllHostnameVerifier")
	public static String requestPost(String urlStr, String param) {

		StringBuilder sBuilder = new StringBuilder();
		String body = "";
		URL url = null;
		HttpURLConnection mConn = null;

		try {

			url = new URL(urlStr);

			if (url.getProtocol().toLowerCase().equals("https")) {

				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
					trustAllHosts();

				HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
					https.setHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);
				} else {
					https.setHostnameVerifier(ALWAYS_VERIFY);
				}
				mConn = https;

			} else {
				mConn = (HttpURLConnection) url.openConnection();
			}

			if (mConn != null) {
				mConn.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT);
				mConn.setReadTimeout(DEFAULT_READ_TIMEOUT);
				mConn.setRequestMethod(Const.POST);
				mConn.setUseCaches(false);
				mConn.setDoInput(true);
				mConn.setDoOutput(true);
				HttpUtils.setCooKie(urlStr, mConn);
				mConn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
				if (DataUtil.isNotNull(HttpUtils.mUserAgent)) {
					mConn.setRequestProperty("User-Agent", HttpUtils.mUserAgent);
				}
				if (DataUtil.isNotNull(param)) {
					OutputStream os = mConn.getOutputStream();
					os.write(param.getBytes(Const.EUC_KR));
					os.flush();
				}
//				int responseCode = mConn.getResponseCode();
//				if (responseCode != HttpURLConnection.HTTP_OK) {
//					mConn.disconnect();
//					return body;
//				}
				HttpUtils.saveCookie(mConn);
				BufferedReader br = new BufferedReader(new InputStreamReader(mConn.getInputStream(), Const.EUC_KR));
				//BufferedReader br = new BufferedReader(	new InputStreamReader(mConn.getInputStream(), "UTF-8"));

				for (; ; ) {
					String line = br.readLine();
					if (line == null) {
						break;
					}
					sBuilder.append(line).append('\n');
				}
				br.close();
				body = sBuilder.toString();
			}
		} catch (Exception e) {
			MLog.e(e);
		} finally {
			if (DataUtil.isNotNull(mConn))
				mConn.disconnect();
		}
		return body.trim();
	}

	/**
	 * HTTP POST 구간암호화를 적용한 요청을 수행한다.
	 *
	 * @param urlStr 요청 URL
	 * @param param  요청 파라미터 문자열
	 * @return 응답 문자열
	 */
	@SuppressLint("AllowAllHostnameVerifier")
	public static String requestJexEncryptPost(String urlStr, String param) {
		StringBuilder sBuilder = new StringBuilder();
		String body = "";
		URL url = null;
		HttpURLConnection mConn = null;

		try {

			url = new URL(urlStr);

			if (url.getProtocol().toLowerCase().equals("https")) {
				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
					trustAllHosts();
				HttpsURLConnection https = (HttpsURLConnection) url.openConnection();
				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
					https.setHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);
				} else {
					https.setHostnameVerifier(ALWAYS_VERIFY);
				}
				mConn = https;
			} else {
				mConn = (HttpURLConnection) url.openConnection();
			}

			if (mConn != null) {
				mConn.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT);
				mConn.setReadTimeout(DEFAULT_READ_TIMEOUT);
				mConn.setRequestMethod("POST");
				mConn.setUseCaches(false);
				mConn.setDoInput(true);
				mConn.setDoOutput(true);
				HttpUtils.setCooKie(urlStr, mConn);
				mConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;jex-encrypt=true");
				mConn.setRequestProperty("Content-encoding", "AES256");
				mConn.setRequestProperty("User-Agent", HttpUtils.mUserAgent);

				if (param != null && param.length() > 0) {
					String encText = "_JSON_=" + param;
					OutputStream os = mConn.getOutputStream();
					os.write(encText.getBytes(Const.EUC_KR));
					os.flush();
				}

				int responseCode = mConn.getResponseCode();
				if (responseCode != HttpURLConnection.HTTP_OK) {
					mConn.disconnect();
					// 응답코드가 303이 올 경우 리다이렉트가 필요. 헤더의 Location을 읽어서 강제로 리다이렉트를 수행함.
					// https://testapp.saidabank.co.kr 로 요청시 http://testapp.saidabank.co.kr 로 리다이렉트 필요. iOS는 내부적으로 처리가 되는 것으로 보임.
					if (responseCode == HttpURLConnection.HTTP_SEE_OTHER || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
						String redirectedUrl = mConn.getHeaderField("Location");
						url = new URL(redirectedUrl);
						mConn = (HttpURLConnection)url.openConnection();
					} else {
						return body;
					}
				}

//				기존소스
//				int responseCode = mConn.getResponseCode();
//				if (responseCode != HttpURLConnection.HTTP_OK) {
//					mConn.disconnect();
//					return body;
//				}

				HttpUtils.saveCookie(mConn);
				BufferedReader br = new BufferedReader(new InputStreamReader(mConn.getInputStream(), Const.EUC_KR));

				for (; ; ) {
					String line = br.readLine();
					if (line == null) {
						break;
					}
					sBuilder.append(line).append('\n');
				}
				br.close();
				body = sBuilder.toString();
			}
		} catch (Exception e) {
			MLog.e(e);
		} finally {
			if (DataUtil.isNotNull(mConn)) {
				HttpUtils.printHeader("", mConn);
				mConn.disconnect();
			}
		}
		return body.trim();
	}

	// always verify the host
	static final HostnameVerifier ALWAYS_VERIFY = new HostnameVerifier() {
		public boolean verify(String hostname, SSLSession session) {
			return true;
		}
	};

	/**
	 * Trust every server
	 */
	private static void trustAllHosts() {
		// Create a trust manager
		TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return new java.security.cert.X509Certificate[]{};
			}
			@Override
			public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
			@Override
			public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException { }
		}};
		// Install the all-trusting trust manager
		try {
			SSLContext sc = SSLContext.getInstance("TLS");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		} catch (Exception e) {
			MLog.e(e);
		}
	}
}
