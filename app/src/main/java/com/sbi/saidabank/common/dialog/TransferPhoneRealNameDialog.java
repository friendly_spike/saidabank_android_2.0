package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

/**
 * Saidabank_android
 * Class: TransferPhoneRealNameDialog
 * Created by 950546
 * Date: 2019-01-30
 * Time: 오후 2:41
 * Description: 휴대폰번호 이체 시 이름입력 팝업
 */
public class TransferPhoneRealNameDialog extends BaseDialog implements View.OnClickListener, TextWatcher {
    Context mContext;
    String mName;
    String mTLNO;
    EditText mEtName;
    TextView mTvErrMsg;
    boolean  mIsFromContactList;
    InputMethodManager mImm;
    private TransferPhoneRealNameDialog.OnConfirmListener _listener;
    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;

    public TransferPhoneRealNameDialog(Context context, String name, String TLNO, boolean isFromContactList) {
        super(context);
        mContext = context;
        mName = name;
        mTLNO = TLNO;
        if (!TextUtils.isEmpty(mName) && mName.length() > 15)
            mName = mName.substring(0, 15);

        mIsFromContactList = isFromContactList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        setContentView(R.layout.dialog_transfer_phone_realname);
        setDialogWidth();

        initUX();

//        Handler delayHandler = new Handler();
//        delayHandler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mEtName.requestFocus();
//                mImm.showSoftInput(mEtName, 0);
//            }
//        }, 300);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibtn_erase: {
                mEtName.setText("");
                break;
            }
            case R.id.btn_ok: {
                String name = mEtName.getText().toString();
                if(TextUtils.isEmpty(name)){
                    Logs.showToast(getContext(),"받는 분의 실명을 입력하세요.");
                    return;
                }

                if ((Utils.isKorean(name) != Const.STATE_VALID_STR_CHAR_ERR)) {
                    if (_listener != null) {
                        _listener.onConfirmPress(mEtName.getText().toString(), mTLNO, mIsFromContactList);
                    }
                    mImm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
                    dismiss();
                }
                break;
            }

            case R.id.btn_cancel: {
                mImm.hideSoftInputFromWindow(mEtName.getWindowToken(), 0);
                dismiss();
                break;
            }

            default:
                break;
        }
    }

    public void setOnConfirmListener(TransferPhoneRealNameDialog.OnConfirmListener listener) {
        _listener = listener;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String name = s.toString();
        if (!name.matches("^[ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]*$")) {
            mEtName.removeTextChangedListener(this);
            mEtName.setText(name.replaceAll("[^ㄱ-ㅎㅏ-ㅣ가-힣\\u318D\\u119E\\u11A2\\u2022\\u2025\\u00B7\\uFE55]", ""));
            mEtName.setSelection(mEtName.getText().length());
            mEtName.addTextChangedListener(this);
            name = mEtName.getText().toString();
        }

        if ((TextUtils.isEmpty(name)) || (Utils.isKorean(name) != Const.STATE_VALID_STR_CHAR_ERR)) {
            mTvErrMsg.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 화면 초기화
     */
    private void initUX() {
        mEtName = findViewById(R.id.et_name);
        mTvErrMsg = findViewById(R.id.tv_errmsg);
        if (!TextUtils.isEmpty(mName)) {
            mEtName.setText(mName);
            mEtName.setSelection(mName.length());
            mEtName.addTextChangedListener(this);

            if (Utils.isKorean(mEtName.getText().toString()) == Const.STATE_VALID_STR_CHAR_ERR) {
                mTvErrMsg.setVisibility(View.VISIBLE);
            }
        }

        findViewById(R.id.ibtn_erase).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);

        mImm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public interface OnConfirmListener {
        void onConfirmPress(String name, String TLNO, boolean isFromContactList);
    }
}
