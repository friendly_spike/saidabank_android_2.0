package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.common.CheatUseAccountInfo;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 사기이용계좌 명의인 등록고객 거래제한 다이얼로그
 */
public class CheatUseAccountDialog extends BaseDialog {

    private CheatUseAccountDialog.OnConfirmListener mListener;
    private Context mContext;

    private SharedPreferences mPreferences;
    private final String mPF_NM_Day = "Day";
    private String mToDay = "0";

    public interface OnConfirmListener {
        void onConfirmPress();
    }

    public void setOnCofirmListener(OnConfirmListener listener) {
        this.mListener = listener;
    }

    public CheatUseAccountDialog(Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_cheat_use_account);
        setDialogWidth();

        TextView txtMsg = (TextView) findViewById(R.id.txt_cheat_use);
        Button btnCheatUse = (Button) findViewById(R.id.btn_ok_confirm_cheat_use);

        String FinancialInstitutionNM = CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_FIN_INST_NM(); //금융기관명
        if (!FinancialInstitutionNM.isEmpty()){
            String msg = "고객님께서는 " + FinancialInstitutionNM + "에 전자금융거래제한자로";
            Utils.setTextWithSpan(txtMsg,msg,FinancialInstitutionNM, Typeface.BOLD,"#000000");
        }

        btnCheatUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPreferences != null){
                    SharedPreferences.Editor SPreferencesEditor = mPreferences.edit();
                    SPreferencesEditor.putString(mPF_NM_Day, mToDay);
                    SPreferencesEditor.commit();
                }
                mListener.onConfirmPress();
                dismiss();
            }
        });
    }

    public Boolean oneDayAppInfo() {
        long CurrentTime = System.currentTimeMillis();

        Date TodayDate = new Date(CurrentTime);
        SimpleDateFormat SDFormat = new SimpleDateFormat("dd");
        mToDay = SDFormat.format(TodayDate);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String strSPreferencesDay = mPreferences.getString(mPF_NM_Day, "0");

        //사기계좌 당일 등록->해지->등록 되는경우를 위해 초기화
        if (CheatUseAccountInfo.getInstance().getFRD_ACCO_REG_YN().equals("N")){
            if (!strSPreferencesDay.equals("0")){
                SharedPreferences.Editor SPreferencesEditor = mPreferences.edit();
                SPreferencesEditor.remove("Day");
                SPreferencesEditor.commit();
            }
            return false;
        }

        if((Integer.parseInt(mToDay) - Integer.parseInt(strSPreferencesDay)) != 0) {
            return true;
        }
        return false;
    }
}
