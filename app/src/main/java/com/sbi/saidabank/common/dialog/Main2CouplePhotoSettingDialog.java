package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;

public class Main2CouplePhotoSettingDialog extends BaseDialog {

    private Context mContext;
    private OnConfirmListener mListener;

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
    }

    public Main2CouplePhotoSettingDialog(@NonNull Context context, OnConfirmListener listener) {
        super(context);
        this.mContext = context;
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_main2_couple_photo_setting);
        setCancelable(true);

        findViewById(R.id.layout_setting_default).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    dismiss();
                    mListener.onConfirmPress(1);
                }
            }
        });
        findViewById(R.id.layout_setting_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    dismiss();
                    mListener.onConfirmPress(2);
                }
            }
        });
        findViewById(R.id.layout_setting_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    dismiss();
                    mListener.onConfirmPress(3);
                }
            }
        });
    }

}
