package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.customview.CustomFakeApp;
import com.secuchart.android.sdk.base.model.FakeAppResultStatus;
import com.secuchart.android.sdk.base.model.fake_app.FakeAppResult;

import java.util.List;

/**
 * SlidingFakeAppsInfoDialog : 악성 앱 감지 목록 팝업
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-11-16
 */
public class SlidingFakeAppsListDialog extends SlidingBaseDialog {

    /** 생성자 */
    private Context mContext;

    /** 이벤트 리스너 */
    private SlidingFakeAppsListDialog.OnItemListener mListener;

    /** 악성 앱 목록 */
    private List<FakeAppResult> mFakeAppsList;

    /** 아이템 선택 인터페이스 */
    public interface OnItemListener {
        void onContinue();
        void onExitApp();
    }

    /** 아이템 선택 리스너 */
    public void setOnItemListener(OnItemListener listener) {
        this.mListener = listener;
    }

    public SlidingFakeAppsListDialog(Context context, List<FakeAppResult> list) {
        super(context);
        this.mContext = context;
        this.mFakeAppsList = list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_fake_apps_list);
        setDialogWidth();
        LinearLayout mFakeAppsArea = (LinearLayout) findViewById(R.id.ll_fake_apps_list);
        if (DataUtil.isNotNull(mFakeAppsList) && !mFakeAppsList.isEmpty()) {
            for (FakeAppResult result : mFakeAppsList) {
                if (isMalign(result.getResult())) {
                    CustomFakeApp item = new CustomFakeApp(mContext);
                    item.setAppIcon(result.getPackageId(), R.drawable.ico_profile);
                    item.setText(result.getPackageId());
                    mFakeAppsArea.addView(item);
                }
            }
        }
        Button mOkBtn = (Button) findViewById(R.id.btn_ok);
        mOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onContinue();
                    dismiss();
                }
            }
        });
        Button mCancelBtn = (Button) findViewById(R.id.btn_cancel);
        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onExitApp();
                    dismiss();
                }
            }
        });
    }

    /**
     * 악성 앱 여부를 체크
     *
     * @param result
     * @return
     */
    private boolean isMalign(FakeAppResultStatus result) {
        return (result == FakeAppResultStatus.DANGER);
    }
}
