package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import java.util.Locale;

/**
 * Saidabank_android
 * Class: SlidingConfirmPhoneTransferDialog
 * Created by 950485 on 2019. 02. 20..
 * <p>
 * Description:아래에서 위로 올라오는 단건이체 확인 다이얼로그
 */

public class SlidingConfirmPhoneTransferDialog extends SlidingBaseDialog {
    private Context   context;

    private String                     withdrawAccount;
    private ContactsInfo contactsInfo;
    private FinishListener             finishListener;

    public SlidingConfirmPhoneTransferDialog(@NonNull Context context, String withdrawAccount, ContactsInfo contactsInfo, FinishListener finishListener) {
        super(context);
        this.context = context;
        this.withdrawAccount = withdrawAccount;
        this.contactsInfo = contactsInfo;
        this.finishListener = finishListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_confirm_phone_transfer);
        setDialogWidth();
        setCanceledOnTouchOutside(true);

        initUX();
    }

    private void initUX() {
        TextView textName = (TextView) findViewById(R.id.textview_recipient_name_phone);
        String FLNM = contactsInfo.getFLNM();
        if (!TextUtils.isEmpty(FLNM))
            textName.setText(FLNM);

        TextView textPhone = (TextView) findViewById(R.id.textview_recipient_number_phone);
        String TLNO = contactsInfo.getTLNO();
        if (!TextUtils.isEmpty(TLNO)) {
            TLNO = PhoneNumberUtils.formatNumber(TLNO, Locale.getDefault().getCountry());
            TLNO += "으로";
            textPhone.setText(TLNO);
        }

        TextView textAmount = (TextView) findViewById(R.id.textview_amount_phone);
        String TRN_AMT = contactsInfo.getTRN_AMT();
        if (!TextUtils.isEmpty(TRN_AMT)) {
            TRN_AMT = Utils.moneyFormatToWon(Double.valueOf(TRN_AMT));
            textAmount.setText(TRN_AMT);
        }

        TextView textFee = (TextView) findViewById(R.id.textview_fee_phone);
        String fee = contactsInfo.getTRNF_FEE();
        if (!TextUtils.isEmpty(fee))
            textFee.setText(fee);

        TextView textWithdrawAccount = (TextView) findViewById(R.id.textview_withdraw_account_phone);
        if (!TextUtils.isEmpty(withdrawAccount))
            textWithdrawAccount.setText(withdrawAccount);

        TextView msg = (TextView) findViewById(R.id.textview_send_msg_phone);
        String DEPO_BNKB_MRK_NM = contactsInfo.getDEPO_BNKB_MRK_NM();
        if (!TextUtils.isEmpty(DEPO_BNKB_MRK_NM))
            msg.setText(DEPO_BNKB_MRK_NM);

        Button btnCancel = (Button) findViewById(R.id.btn_cancel_confirm_transfer_phone);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnCancelListener();
                dismiss();
            }
        });

        Button btnOK = (Button) findViewById(R.id.btn_ok_confirm_transfer_phone);
        btnOK.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnOKListener();
                dismiss();
            }
        });

        setOnCancelListener(new OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (context instanceof Activity && ((Activity) context).isFinishing())
                    return;

                finishListener.OnCancelListener();
                dismiss();
            }
        });
    }

    public interface FinishListener {
        void OnCancelListener();
        void OnOKListener();
    }
}
