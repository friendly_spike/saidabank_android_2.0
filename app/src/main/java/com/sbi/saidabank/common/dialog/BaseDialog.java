package com.sbi.saidabank.common.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import androidx.annotation.NonNull;

import com.sbi.saidabank.common.util.DataUtil;

/**
 * BaseDialog : 공통 다이얼로그
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
public class BaseDialog extends Dialog {

    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        if (DataUtil.isNotNull(getWindow())) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    /**
     * 화면의 넓이만큼 다이얼로그를 채워준다.
     */
    protected void setDialogWidth() {
        if (DataUtil.isNotNull(getWindow())) {
            WindowManager windowManager = getWindow().getWindowManager();
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            // 다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
            LayoutParams lp = getWindow().getAttributes();
            lp.width = size.x;
            getWindow().setAttributes(lp);
        }
    }
}
