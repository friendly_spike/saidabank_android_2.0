package com.sbi.saidabank.common.download;

import android.app.Activity;
import android.os.AsyncTask;
import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.SaidaCodeUtil;

public class DownloadRSCUtil {

    public static void DownloadResource(final Activity context, String rscUrl, final String rscVersion){
        //리소스가 변경되었다는 말은 은행 코드도 추가되었을수도 있기에 코드값 다운로드 받아둔다.
        SaidaCodeUtil.requestBankList(context);
        SaidaCodeUtil.requestStockList(context);

        if(TextUtils.isEmpty(rscUrl)) return;

        DownloadRSCAsync daa = new DownloadRSCAsync(context, new DownloadFileCallback() {
            @Override
            public void updateProgress(int per) {
                Logs.e("DownloadResouceAsync - updateProgress : " + per);
            }

            @Override
            public void onDownloadComplete(boolean result, String pathName, String fileName) {
                Logs.e("DownloadResouceAsync - onDownloadComplete : " + pathName + "/" + fileName);
                Prefer.setResourceIconVersion(context,rscVersion);
            }
        });
        daa.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,rscUrl,"Icons");
    }



}
