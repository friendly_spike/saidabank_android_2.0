package com.sbi.saidabank.common.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;

import com.sbi.saidabank.define.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Prefer : 로컬스토리지에 저장
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class Prefer {

    private static final String PREF_PERMISSION_SHOW_STATUS          = "pref_permission_show_status";
    private static final String PREF_TUTORIAL_STATUS                 = "pref_tutorial_status";
    private static final String PREF_JOIN_ACCOUNT                    = "pref_join_account";
    private static final String PREF_AUTH_PHONE_CI                   = "pref_auth_phone_ci";
    private static final String PREF_AUTH_PHONE_DI                   = "pref_auth_phone_di";
    private static final String PREF_SOLUTION_WEB_ADDRESS            = "pref_solution_web_address";
    private static final String PREF_FIDO_REG_STATUS                 = "pref_fido_reg_status";
    private static final String PREF_PATTERN_REG_STATUS              = "pref_pattern_reg_status";
    private static final String PREF_PATTERN_ERR_COUNT               = "pref_pattern_error_count";
    private static final String PREF_PATTERN_USE_STEALTH             = "pref_use_pattern_stealth";
    private static final String PREF_PINCODE_REG_STATUS              = "pref_pincode_reg_status";
    private static final String PREF_PINCODE_ERR_COUNT               = "pref_pin_error_count";
    private static final String PREF_SERVER_APP_VERSION              = "pref_server_app_version";
    private static final String PREF_PUSH_GCM_TOKEN                  = "pref_push_gcm_token";
    private static final String PREF_PUSH_REGID                      = "pref_push_regid";
    private static final String PREF_FIRST_SHOW_NOTI_MSGBOX          = "pref_first_show_noti_msgbox";
    private static final String PREF_SHOW_DEV_MODE_DIALOG            = "pref_show_devmode_dialog";
    private static final String PREF_FINGERPRINT_CHANGE              = "pref_fingerprint_chagne";
    private static final String PREF_OPEN_CDDEDD_FOR_1WEEK           = "pref_open_cddedd_for_1week";
    private static final String PREF_JOIN_DEBUG_SERVER_INDEX         = "pref_join_debug_server_index";
    private static final String PREF_BANK_LIST                       = "pref_bank_list";
    private static final String PREF_STOCK_LIST                      = "pref_stock_list";
    private static final String PREF_OPENBANK_LIST                   = "pref_openbank_list";
    private static final String PREF_LAST_CLIPBOARD_TEXT             = "pref_last_clipboard_text";
    private static final String PREF_PROFILE_IMAGE_PATH              = "pref_profile_image_path";
    private static final String PREF_COUPLE_TITLE_BG_IMAGE_PATH      = "pref_couple_title_bg_image_path";
    private static final String PREF_VOICE_PHISHING_ASK_POPUP_DATE   = "pref_voice_phishing_ask_popup_date";
    private static final String PREF_RESOURCE_ICON_VERSION           = "pref_resource_icon_version";



    public static SharedPreferences getSharedPreferences(Context context){
        return context.getSharedPreferences(context.getPackageName() + "_preferences", Context.MODE_PRIVATE);
    }

    /*
     * 문자열 형식 저장하기 위한 함수
     * */
    public static String getStringPreference(Context context,String key) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(key,"");
    }

    public static void setStringPreference(Context context,String key, String val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, val);
        editor.apply();
    }

    /*
     * Intro Permission Dialog출력여부
     * */
    public static boolean getPermissionGuideShowStatus(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_PERMISSION_SHOW_STATUS, false);
    }

    public static void setPermissionGuideShowStatus(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_PERMISSION_SHOW_STATUS, val);
        editor.apply();
    }

    /*
     * 생체등록 사용자 아이디 저장
     * 센스톤 지문,패턴,핀 로그인시 사용되는 사용자 아이디.
     * 휴대폰 본인인증하면 전달 받는다.
     * 32자리
     * Connecting Information
     * */
    public static String getAuthPhoneCI(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_AUTH_PHONE_CI, Const.EMPTY);
    }

    public static void setAuthPhoneCI(Context context, String ci) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_AUTH_PHONE_CI, ci);
        editor.apply();
    }

    /*
     * 휴대폰 본인인증하면 전달 받는다.
     * Duplication Information
     * */
    public static String getAuthPhoneDI(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_AUTH_PHONE_DI, "DI0000032Len");
    }

    public static void setAuthPhoneDI(Context context, String ci) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_AUTH_PHONE_DI, ci);
        editor.apply();
    }

    /*
     For Test.추후 삭제 요망
     */
    public static String getSolutionWebAddress(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_SOLUTION_WEB_ADDRESS, "https://devapp.saidabank.co.kr/index.jsp");
    }

    public static void setSolutionWebAddress(Context context, String url) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_SOLUTION_WEB_ADDRESS, url);
        editor.apply();
    }

    /**
     * 지문 등록 상태 체크
     */
    public static boolean getFidoRegStatus(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_FIDO_REG_STATUS, false);
    }

    public static void setFidoRegStatus(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_FIDO_REG_STATUS, val);
        editor.apply();
    }

    /**
     * 패턴 등록 상태 체크
     */
    public static boolean getPatternRegStatus(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_PATTERN_REG_STATUS, false);
    }

    public static void setPatternRegStatus(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_PATTERN_REG_STATUS, val);
        editor.apply();
    }

    /**
     * 핀코드 등록 상태 체크
     */
    public static boolean getPincodeRegStatus(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_PINCODE_REG_STATUS, false);
    }

    public static void setPincodeRegStatus(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_PINCODE_REG_STATUS, val);
        editor.apply();
    }

    /**
     * 서버 버전
     */
    public static String getServerAppVersion(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_SERVER_APP_VERSION, "0.0.0");
    }

    public static void setServerAppVersion(Context context, String version) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_SERVER_APP_VERSION, version);
        editor.apply();
    }

    /*
     푸시 관련
     */
    public static String getPushGCMToken(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_PUSH_GCM_TOKEN, Const.EMPTY);
    }

    public static void setPushGCMToken(Context context, String str) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_PUSH_GCM_TOKEN, str);
        editor.apply();
    }

    public static String getPushRegID(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_PUSH_REGID, Const.EMPTY);
    }

    public static void setPushRegID(Context context, String str) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_PUSH_REGID, str);
        editor.apply();
    }

    /*
     * 푸시 알림 다이얼로그 여부 저장
     * */
    public static boolean getFlagShowFirstNotiMsgBox(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_FIRST_SHOW_NOTI_MSGBOX, false);
    }

    public static void setFlagShowFirstNotiMsgBox(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_FIRST_SHOW_NOTI_MSGBOX, val);
        editor.apply();
    }

    /*
     * 디버깅 모드 체크 다이얼로그 보이기 여부 저장
     * */
    public static boolean getFlagDontShowDevModeCheckDialog(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_SHOW_DEV_MODE_DIALOG, false);
    }

    public static void setFlagDontShowDevModeCheckDialog(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_SHOW_DEV_MODE_DIALOG, val);
        editor.apply();
    }

    /*
     * 지문정보 변경 여부 저장
     * */
    public static boolean getFlagFingerPrintChange(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_FINGERPRINT_CHANGE, false);
    }

    public static void setFlagFingerPrintChange(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_FINGERPRINT_CHANGE, val);
        editor.apply();
    }

    /*
     * cdd/edd 고객정보 재확인 일주일간 열지않기 선택 여부
     * */
    public static String getCDDEDDNotOpenFor1Week(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_OPEN_CDDEDD_FOR_1WEEK, Const.EMPTY);
    }

    public static void setCDDEDDNotOpenFor1Week(Context context, String val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_OPEN_CDDEDD_FOR_1WEEK, val);
        editor.apply();
    }

    /*
     * 패턴 오류 횟수 저장/조회
     * */
    public static int getPatternErrorCount(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getInt(PREF_PATTERN_ERR_COUNT, 0);
    }

    public static void setPatternErrorCount(Context context, int val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREF_PATTERN_ERR_COUNT, val);
        editor.apply();
    }

    /*
     * 핀 오류 횟수 저장/조회
     * */
    public static int getPinErrorCount(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getInt(PREF_PINCODE_ERR_COUNT, 0);
    }

    public static void setPinErrorCount(Context context, int val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREF_PINCODE_ERR_COUNT, val);
        editor.apply();
    }

    /*
     * 입력패턴 숨기기 여부
     */
    public static boolean getUsePatternStealth(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_PATTERN_USE_STEALTH, false);
    }

    public static void setUsePatternStealth(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_PATTERN_USE_STEALTH, val);
        editor.apply();
    }

    /**
     * Tutorial 상태 체크
     */
    public static boolean getTutorialStatus(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_TUTORIAL_STATUS, false);
    }

    public static void setTutorialStatus(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_TUTORIAL_STATUS, val);
        editor.apply();
    }

    /*
     * 저장된 인증관련 프리퍼런스를 삭제. 회원 탈퇴 시에만 호출
     */
    public static void clearAuthSharedPreferences(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(PREF_AUTH_PHONE_CI).commit();
        editor.remove(PREF_FIDO_REG_STATUS).commit();
        editor.remove(PREF_PATTERN_REG_STATUS).commit();
        editor.remove(PREF_PATTERN_ERR_COUNT).commit();
        editor.remove(PREF_PATTERN_USE_STEALTH).commit();
        editor.remove(PREF_PINCODE_REG_STATUS).commit();
        editor.remove(PREF_FINGERPRINT_CHANGE).commit();
        editor.remove(PREF_OPEN_CDDEDD_FOR_1WEEK).commit();
    }

    public static void setJoinAccount(Context context, boolean val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREF_JOIN_ACCOUNT, val);
        editor.apply();
    }

    public static boolean getJoinAccount(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getBoolean(PREF_JOIN_ACCOUNT, false);
    }

    /**
     * 디버깅모드에서 서버선택을하고 회원가입을 하게되면 다른 서버로 변경시 센스톤에서 에러가 발생한다.
     * 그래서 어떤 서버로 회원가입한것인지 저장하고 있다가 서버가 변경되면 개발자에게 알려준다.
     * @param context
     * @return
     */
    public static int getJoinDebugServerIndex(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getInt(PREF_JOIN_DEBUG_SERVER_INDEX, -1);
    }

    public static void setJoinDebugServerIndex(Context context, int severIndex) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(PREF_JOIN_DEBUG_SERVER_INDEX, severIndex);
        editor.apply();
    }

    /*
    디버깅 모드에서 서버 변경시 이전 서버 설정값 유지 하기 위해 백업해둠.
    * */
    public static String getDebugBackupAuthStatus(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString("pref_backup_auth_status", Const.EMPTY);
    }

    public static void setDebugBackupAuthStatus(Context context) {
        String retStr = "";
        JSONObject obj = new JSONObject();
        try {
            obj.put("PATTERN_STATUS",getPatternRegStatus(context));
            obj.put("FIDO_STATUS",getFidoRegStatus(context));
            obj.put("PINCODE_STATUS",getPincodeRegStatus(context));
            obj.put("FIDO_CHANGE_STATUS",getFlagFingerPrintChange(context));
            retStr = obj.toString();
        } catch (JSONException e) {
            Logs.printException(e);
        }

        setDebugBackupAuthStatus(context,retStr);
    }

    public static void setDebugBackupAuthStatus(Context context,String valStr) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_backup_auth_status", valStr);
        editor.apply();
    }

    //은행과 증권사의 리스트를 저장해 둔다. 이체시 은행/증권사 검색할때마다 업데이트 된다.
    public static String getBankList(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_BANK_LIST, Const.EMPTY);
    }

    public static void setBankList(Context context, String val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_BANK_LIST, val);
        editor.apply();
    }

    public static String getStockList(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_STOCK_LIST, Const.EMPTY);
    }

    public static void setStockList(Context context, String val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_STOCK_LIST, val);
        editor.apply();
    }

    public static String getOpenBankList(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_OPENBANK_LIST, Const.EMPTY);
    }

    public static void setOpenBankList(Context context, String val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_OPENBANK_LIST, val);
        editor.apply();
    }

    /**
     * 빠른이체를 위해 클립보드에 은행명과 계좌를 복사했을때 한번 처리후
     * 두번째에 같은 문자열이면 빠른이체 패스하기 위해 추가함.
     * @param context
     * @return
     */
    public static String getLastCheckClipboardDesc(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_LAST_CLIPBOARD_TEXT, Const.EMPTY);
    }

    public static void setLastCheckClipboardDesc(Context context, String val) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_LAST_CLIPBOARD_TEXT, val);
        editor.apply();
    }

    /**
     * 사용자 프로파일 이미지의 경로를 저장한다.
     */
    public static String getProfileImagePath(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String imagePath =  prefs.getString(PREF_PROFILE_IMAGE_PATH, Const.EMPTY);
        if (DataUtil.isNotNull(imagePath)) {
            return imagePath;
        } else {
            File storageDir = context.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
            if (!storageDir.exists()) {
                storageDir.mkdir();
            }
            File file = new File(storageDir, "sbiprofile.jpg");
            return Uri.fromFile(file).toString();
        }
    }

    public static void setProfileImagePath(Context context, String path) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_PROFILE_IMAGE_PATH, path);
        editor.apply();
    }

    /**
     * 사용자 프로파일 이미지의 경로를 저장한다.
     */
    public static String getCoupleBackgroundImagePath(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        String imagePath =  prefs.getString(PREF_COUPLE_TITLE_BG_IMAGE_PATH, Const.EMPTY);
        if(DataUtil.isNotNull(imagePath)){
            return imagePath;
        }else{
            File storageDir = context.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
            if (!storageDir.exists()) {
                storageDir.mkdir();
            }

            File file = new File(storageDir, "sbiprofile.jpg");
            return Uri.fromFile(file).toString();
        }
    }

    public static void setCoupleBackgroundImagePath(Context context, String path) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_COUPLE_TITLE_BG_IMAGE_PATH, path);
        editor.apply();
    }

    /**
     * 보이스피싱 문진팝업 노출 날짜를 얻는다.
     *
     * @param context
     * @return
     */
    public static String getPrefVoicePhishingAskPopupDate(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_VOICE_PHISHING_ASK_POPUP_DATE, Const.EMPTY);
    }

    /**
     * 보이스피싱 문진팝업 노출 날짜를 저장한다.
     *
     * @param context
     * @param date
     */
    public static void setPrefVoicePhishingAskPopupDate(Context context, String date) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_VOICE_PHISHING_ASK_POPUP_DATE, date);
        editor.apply();
    }


    /*
     * 은행,증권 리소스 이미지 버전 관리
     * */
    public static String getResourceIconVersion(Context context) {
        SharedPreferences prefs = getSharedPreferences(context);
        return prefs.getString(PREF_RESOURCE_ICON_VERSION, "");
    }

    public static void setResourceIconVersion(Context context, String rscVersion) {
        SharedPreferences prefs = getSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREF_RESOURCE_ICON_VERSION, rscVersion);
        editor.apply();
    }



}
