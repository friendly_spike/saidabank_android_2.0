package com.sbi.saidabank.common.util;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.define.Const;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Saidabank_android
 *
 * Class: ImgUtils
 * Created by 950469 on 2018. 8. 28..
 * <p>
 * Description:
 * Bitmap작업 관련 함수들의 모음.
 */
public class ImgUtils {
    public static Bitmap byteArrayToBitmap(byte[] mbyteArray ) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inMutable = true;

        Bitmap bitmap = BitmapFactory.decodeByteArray( mbyteArray, 0, mbyteArray.length, options) ;
        return bitmap ;
    }

    public static byte[] bitmapToByteArray( Bitmap mbitmap ) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream() ;
        mbitmap.compress( Bitmap.CompressFormat.JPEG, 70, stream) ;
        byte[] byteArray = stream.toByteArray() ;
        return byteArray ;
    }

    public static Bitmap cropBitmap( Bitmap mbitmap, Rect mrect) {
        Bitmap cropImage = Bitmap.createBitmap(mbitmap, mrect.left, mrect.top, mrect.width(), mrect.height());

        return cropImage;
    }

    public static Bitmap combineBitmap(Bitmap original,Bitmap overlay){

        //결과값 저장을 위한 Bitmap
        Bitmap resultOverlayBmp = Bitmap.createBitmap(original.getWidth()
                , original.getHeight()
                , original.getConfig());

        //캔버스를 통해 비트맵을 겹치기한다.
        Canvas canvas = new Canvas(resultOverlayBmp);
        canvas.drawBitmap(original, new Matrix(), null);

        int left = original.getWidth()/2 - overlay.getWidth()/2;
        int top  = original.getHeight()/2 - overlay.getHeight()/2;

        Rect overlayRect = new Rect(left, top, left+overlay.getWidth(), top + overlay.getHeight());


        canvas.drawBitmap(overlay,null,overlayRect,null);

        return resultOverlayBmp;

    }

    public static boolean captureSave(Activity activity,String fileName){
        View activityView = activity.getWindow().getDecorView().getRootView();
        activityView.setDrawingCacheEnabled(true);
        activityView.buildDrawingCache();
        Bitmap activityBmp = Bitmap.createBitmap(activityView.getDrawingCache());


        if(activityBmp != null){
            //이미지 파일 생성
            File imageFile = new File(Files.getOutputMediaPath(activity,activity.getString(R.string.app_eng_name)),fileName);
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(imageFile);

                activityBmp.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (FileNotFoundException e) {
                Logs.printException(e);
            } catch (IOException e) {
                Logs.printException(e);
            }
            activityView.destroyDrawingCache();
            activityView.setDrawingCacheEnabled(false);
            activityBmp.recycle();
            return true;
        }

        return false;

    }

    /**
     * 화면 캡쳐 후 byte로 리턴
     * @param view 캡쳐할 화면뷰
     * @return img bytearray
     */
    public static byte[] captureSave(View view) {
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();

        if (viewBmp != null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            viewBmp.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            return byteArrayOutputStream.toByteArray();
        }

        return null;
    }

    /**
     * 화면 캡쳐 후 로컬 저장
     * @param activity context
     * @param view 캡쳐할 화면뷰
     * @param fileName 저장할 파일이름
     * @return 저장 성공 여부
     */
    public static boolean captureSave(Activity activity,View view,String fileName){
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();

        if(viewBmp != null){
            //이미지 파일 생성
            File storageDir = Environment.getExternalStoragePublicDirectory(android.os.Environment.DIRECTORY_PICTURES);
            if (!storageDir.exists()) {
                storageDir.mkdir();
            }
            File imageFile = new File(storageDir, fileName);
            //File imageFile = new File(Files.getOutputMediaPath(activity,activity.getString(R.string.app_eng_name)),fileName);
            FileOutputStream outputStream = null;
            try {
                outputStream = new FileOutputStream(imageFile);

                viewBmp.compress(Bitmap.CompressFormat.JPEG, 30, outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (FileNotFoundException e) {
                Logs.printException(e);
            } catch (IOException e) {
                Logs.printException(e);
            }
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            Uri uri = Uri.fromFile(imageFile);
            activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
            return true;
        }

        return false;

    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;




        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static Bitmap getRoundedCornerBitmap(@NonNull Bitmap bitmap, float topLeftCorner, float topRightCorner, float bottomRightCorner, float bottomLeftCorner) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);


        final int color = Color.WHITE;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        Path path = new Path();
        float[] radii = new float[]{
                topLeftCorner, topLeftCorner,
                topRightCorner, topRightCorner,
                bottomRightCorner, bottomRightCorner,
                bottomLeftCorner, bottomLeftCorner
        };

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        path.addRoundRect(rectF, radii, Path.Direction.CW);
        canvas.drawPath(path, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        return output;
    }


    public static Bitmap getBitmapFromView(View view,int width,int height)   {

        //이코드는 반드시 들어가 줘야 이미지가 만들어 진다.
        view.measure(View.MeasureSpec.makeMeasureSpec(width,  View.MeasureSpec.EXACTLY),View.MeasureSpec.makeMeasureSpec(width,  View.MeasureSpec.EXACTLY));
        view.layout(0, 0, width, height);
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }



    @SuppressLint("SimpleDateFormat")
    public static File createNewImageFilePathName(Context context, String type){
        File photoFile = null;
        try {
            String imageFileName = "sbi_";
            if(!TextUtils.isEmpty(type)){
                imageFileName = imageFileName + type + "_";
            }
            imageFileName = imageFileName + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            Logs.i("createNewImageFileName - imageFileName : " + imageFileName);
            File storageDir = context.getExternalFilesDir(android.os.Environment.DIRECTORY_PICTURES);
            if (!storageDir.exists()) {
                storageDir.mkdir();
            }
            photoFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        } catch (IOException e) {
            MLog.e(e);
        }

        return photoFile;
    }

    public static void deleteImageFile(final Context context,Uri photoUri) {
        MLog.d();
        if (DataUtil.isNotNull(photoUri)) {
            try {

                File file = new File(photoUri.getPath());
                if (DataUtil.isNotNull(file) && file.exists()) {
                    file.delete();
                    context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, photoUri));

//                    new SingleMediaScanner(context, file).setListener(new SingleMediaScanner.onScannerProcess() {
//                        @Override
//                        public void onScanCompleted(Uri uri) {
//                            context.getContentResolver().delete(uri, null, null);
//                            context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, uri));
//                        }
//                    });
                }
            } catch (Exception e) {
                MLog.e(e);
            }
        }
    }

    /**
     * 메모리에 저장되어 있는 아이콘 이미지의 Uri를 가져온다.
     *
     * @param context
     * @param code          은행코드
     * @param detailCode    저축은행코드
     * @return
     */
    public static Uri getIconUri(Context context, String code, String detailCode) {

        if(TextUtils.isEmpty(code)) return null;

        String iconPath = context.getExternalFilesDir("Icons").getPath();

        String resourcename = "img_logo_" + code;


        if("000".equalsIgnoreCase(code)){
            resourcename = "img_logo_028";
        }else if ("032".equalsIgnoreCase(code)) {//032-부산은행
            resourcename = "img_logo_224";
        }else if ("050".equalsIgnoreCase(code)) {//050-저축은행
            if(!TextUtils.isEmpty(detailCode)){
                resourcename = resourcename + "_" + detailCode;
            }
        }

        File iconFile = new File(iconPath,resourcename + ".png");
        if(iconFile.exists()){
            return Uri.fromFile(iconFile);
        }else{
            if ("050".equalsIgnoreCase(code)) {
                iconFile = new File(iconPath,"img_logo_050.png");
                if(iconFile.exists()){
                    return Uri.fromFile(iconFile);
                }
            }
        }

        return null;
    }
    public static Uri getIconUri(Context context, String code) {
        return getIconUri(context,code,"");
    }
}
