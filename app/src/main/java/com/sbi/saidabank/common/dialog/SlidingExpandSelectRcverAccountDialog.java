package com.sbi.saidabank.common.dialog;

import android.Manifest;
import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.main2.common.progress.ProgressBarLayout;
import com.sbi.saidabank.activity.transaction.ITransferSelectReceiverActivity;
import com.sbi.saidabank.activity.transaction.ITransferSendMultiActivity;
import com.sbi.saidabank.activity.transaction.util.TransferUtils;
import com.sbi.saidabank.common.util.PermissionUtils;
import com.sbi.saidabank.define.datatype.transfer.ITransferDataMgr;
import com.sbi.saidabank.activity.transaction.adater.ITransferSelectRcvMyAccountAdapter;
import com.sbi.saidabank.activity.transaction.adater.ITransferRecentlyAdapter;
import com.sbi.saidabank.common.contacts.ContactsInfoAdapter;
import com.sbi.saidabank.common.contacts.ContactsUtil;
import com.sbi.saidabank.common.contacts.GetContactListAsyncTask;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankAccountInfo;
import com.sbi.saidabank.define.datatype.openbank.OpenBankDataMgr;
import com.sbi.saidabank.define.datatype.transfer.ITransferRecentlyInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Saidabank_android
 * Class: SlidingExpandExampleDialog
 *
 * <p>
 * Description: 받는분 선택할때 사용하는 다이얼로그 이다.
 */

public class SlidingExpandSelectRcverAccountDialog extends SlidingExpandBaseDialog implements View.OnClickListener{
    public static final int TAB_RECENTLY  = 0;
    public static final int TAB_MYACCOUNT = 1;
    public static final int TAB_CONTACT   = 2;

    private String[] perList = new String[]{
            Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private Activity mContext;

    private ProgressBarLayout mProgressBarLayout;

    //탭영역
    private TextView mTvTabRecently;
    private TextView mTvTabMyAccount;
    private TextView mTvTabContact;

    //최근,즐겨찾기기 관련
    private RelativeLayout mLayoutRecently;
    private ListView mListViewRecently;
    private LinearLayout      mLayoutEmptyRecently;
    private ArrayList<ITransferRecentlyInfo> mRecentlyArrayList;
    private ITransferRecentlyAdapter mRecentlyAdapter;

    //내계좌 관련
    private RelativeLayout    mLayoutMyAccount;
    private ListView      mListViewMyAccount;
    private ArrayList<MyAccountInfo> mMyAccountArrayList;
    private ArrayList<OpenBankAccountInfo> mOpenBankAccountArrayList;//오픈뱅킹계좌목록
    private ITransferSelectRcvMyAccountAdapter  mMyAccountAdapter;
    private LinearLayout      mLayoutEmptyMyAccount;

    //연락처 관련
    private RelativeLayout    mLayoutContact;
    private ListView mListViewContact;
    private LinearLayout    mLayoutEmptycontact;
    private ContactsInfoAdapter mContactAdapter;
    private TextView            mTvContactCount;
    private ArrayList<ContactsInfo> mContactArrayList;

    //직접입력버튼
    private LinearLayout        mLayoutDirectInput;
    private TextView            mTvDirecctInput;

    //선택된 현재 탭
    private int            mCurrentTab = TAB_RECENTLY;

    private OnSelectItemListener mListener;


    //리스트뷰 확장을 위한 변수.
    private int mRecentlyListBackupIndex=-1;
    private int mMyAccountListBackupIndex=-1;
    private int mContactListBackupIndex=-1;

    /**
     * 생성자
     *
     * @param context
     */
    public SlidingExpandSelectRcverAccountDialog(@NonNull Activity context, OnSelectItemListener listener) {
        super(context);
        this.mContext = context;
        this.mListener = listener;


        mRecentlyArrayList = new ArrayList<ITransferRecentlyInfo>();
        if(ITransferDataMgr.getInstance().getRecentlyArrayList().size() > 0){
            mRecentlyArrayList.addAll(ITransferDataMgr.getInstance().getRecentlyArrayList());
        }

        mMyAccountArrayList = new ArrayList<MyAccountInfo>() ;
        mOpenBankAccountArrayList = new ArrayList<OpenBankAccountInfo>() ;

        mContactArrayList = new ArrayList<ContactsInfo>();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_sliding_expand_select_rcver);

        setDialogWidth();

        //화면을 늘리기위한 기본 뷰를 셋팅한다.
        setDialogBody((LinearLayout) findViewById(R.id.layout_body));
        setExpandMoveBar(findViewById(R.id.layout_movebar));

        initUX();
    }

    @Override
    public void onBackPressed() {
        mListener.onDismissDialog();
        dismiss();
    }

    private void initUX() {

        //백그라운드 터치시 다이얼로그 종료
        findViewById(R.id.v_background).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onDismissDialog();
                dismiss();
            }
        });

        //전체화면 프로그레스바 레이아웃
        //프로그레스 다이얼로그를 출력할수 없기에...
        mProgressBarLayout = findViewById(R.id.ll_progress);

        //탭영역
        mTvTabRecently = findViewById(R.id.tv_tab_recently);
        mTvTabMyAccount = findViewById(R.id.tv_tab_myaccount);
        mTvTabContact = findViewById(R.id.tv_tab_contact);
        mTvTabRecently.setOnClickListener(this);
        mTvTabMyAccount.setOnClickListener(this);
        mTvTabContact.setOnClickListener(this);        //컨텐츠 영역 배경
        int radius = (int) Utils.dpToPixel(mContext,20);
        findViewById(R.id.layout_content_area).setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));


        //최근,즐겨찾기
        mLayoutRecently = findViewById(R.id.layout_recently);
        mListViewRecently = findViewById(R.id.listview_recently);
        mLayoutEmptyRecently = findViewById(R.id.layout_empty_recently);
        mRecentlyAdapter = new ITransferRecentlyAdapter(mContext, mRecentlyArrayList, new ITransferRecentlyAdapter.OnFavoriteItemClickListener() {
            @Override
            public void onItemClick(int position) {
                ITransferRecentlyInfo accountInfo = mRecentlyArrayList.get(position);
                requestFavoriteAccount(mContext, accountInfo);
            }
        });
        mListViewRecently.setAdapter(mRecentlyAdapter);
        mListViewRecently.addHeaderView(getEmptyHeaderFooterView(false));
        mListViewRecently.addFooterView(getEmptyHeaderFooterView(true));
        mListViewRecently.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int realPos = position - 1;
                //Header와 Footer를 추가했더니.. Header는 0번, Footer는 마지막번으로 넘어오는 문제가 발생한다.
                if(realPos < 0 || realPos >= mRecentlyArrayList.size()) return;
                ITransferRecentlyInfo recentlyInfo = mRecentlyArrayList.get(realPos);

                if(!TextUtils.isEmpty(recentlyInfo.getMNRC_TLNO())){
                    //휴대폰이체 *******
                    final String MNRC_TLNO = recentlyInfo.getMNRC_TLNO();
                    final String MNRC_ACCO_DEPR_NM = recentlyInfo.getMNRC_ACCO_DEPR_NM();

                    mListener.onSelectPhoneItem(MNRC_ACCO_DEPR_NM,MNRC_TLNO);


                }else if(recentlyInfo.getGRP_YN().equalsIgnoreCase("Y")){
                    mListener.onSelectGroupItem(recentlyInfo.getGRP_SRNO(),recentlyInfo.getFAVO_ACCO_YN(),recentlyInfo.getGRP_CCNT());
                }else{
                    mListener.onSelectAccountItem(recentlyInfo.getMNRC_BANK_CD(),recentlyInfo.getDTLS_FNLT_CD(),recentlyInfo.getMNRC_ACNO(),recentlyInfo.getMNRC_ACCO_DEPR_NM());
                }
                dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mListViewRecently.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Logs.e("onScrollChange - getFirstVisiblePosition : " + mListViewRecently.getFirstVisiblePosition());
                    Logs.e("onScrollChange - getLastVisiblePosition : " + mListViewRecently.getLastVisiblePosition());
                    Logs.e("onScrollChange - mListBackupIndex : " + mRecentlyListBackupIndex);
                    if(mRecentlyListBackupIndex < 0 || mRecentlyListBackupIndex == mListViewRecently.getFirstVisiblePosition()){
                        mRecentlyListBackupIndex = mListViewRecently.getFirstVisiblePosition();
                        return;
                    }
                    if(mRecentlyListBackupIndex < mListViewRecently.getFirstVisiblePosition()){
                        Logs.e("draw MAX");
                        if(mListViewRecently.getLastVisiblePosition() > 7) {
                            resizeDialogMax();
                        }
                    }else{
                        Logs.e("draw ORIGIN");
                        if(mListViewRecently.getFirstVisiblePosition() < 1) {
                            resizeDialogOrigin();
                        }
                    }
                    mRecentlyListBackupIndex = mListViewRecently.getFirstVisiblePosition();
                }
            });
        }
        if(ITransferDataMgr.getInstance().getRecentlyArrayList().size() == 0){
            mLayoutEmptyRecently.setVisibility(View.GONE);
            requestRecentlyList();
        }


        //내계좌 관련
        mLayoutMyAccount= findViewById(R.id.layout_myaccount);
        mListViewMyAccount = findViewById(R.id.listview_myaccount);
        mMyAccountAdapter = new ITransferSelectRcvMyAccountAdapter(mContext);
        mListViewMyAccount.setAdapter(mMyAccountAdapter);
        View myAccountHeader = getLayoutInflater().inflate(R.layout.layout_listview_header_myaccount, null, false) ;
        mListViewMyAccount.addHeaderView(myAccountHeader);
        mListViewMyAccount.addFooterView(getEmptyHeaderFooterView(true));
        mListViewMyAccount.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int realPos = position - 1;
                //Header와 Footer를 추가했더니.. Header는 0번, Footer는 마지막번으로 넘어오는 문제가 발생한다.
                if(realPos < 0 || realPos >= mRecentlyArrayList.size()) return;

                Object accountInfo = mMyAccountAdapter.getAccountInfo(realPos);

                String BANK_CD = "";
                String DETAIL_BANK_CD = "";
                String ACNO = "";
                if(accountInfo instanceof MyAccountInfo){
                    BANK_CD = "028";
                    ACNO = ((MyAccountInfo)accountInfo).getACNO();
                }else{
                    BANK_CD = ((OpenBankAccountInfo)accountInfo).RPRS_FNLT_CD;
                    DETAIL_BANK_CD = ((OpenBankAccountInfo)accountInfo).DTLS_FNLT_CD;
                    ACNO = ((OpenBankAccountInfo)accountInfo).ACNO;
                }

                mListener.onSelectAccountItem(BANK_CD,DETAIL_BANK_CD,ACNO,LoginUserInfo.getInstance().getCUST_NM());
                dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mListViewMyAccount.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Logs.e("onScrollChange - getFirstVisiblePosition : " + mListViewMyAccount.getFirstVisiblePosition());
                    Logs.e("onScrollChange - getLastVisiblePosition : " + mListViewMyAccount.getLastVisiblePosition());
                    Logs.e("onScrollChange - mListBackupIndex : " + mMyAccountListBackupIndex);
                    if(mMyAccountListBackupIndex < 0 || mMyAccountListBackupIndex == mListViewMyAccount.getFirstVisiblePosition()){
                        mMyAccountListBackupIndex = mListViewMyAccount.getFirstVisiblePosition();
                        return;
                    }
                    if(mMyAccountListBackupIndex < mListViewMyAccount.getFirstVisiblePosition()){
                        Logs.e("draw MAX");
                        if(mListViewMyAccount.getLastVisiblePosition() > 7) {
                            resizeDialogMax();
                        }
                    }else{
                        Logs.e("draw ORIGIN");
                        if(mListViewMyAccount.getFirstVisiblePosition() < 1) {
                            resizeDialogOrigin();
                        }
                    }
                    mMyAccountListBackupIndex = mListViewMyAccount.getFirstVisiblePosition();
                }
            });
        }
        mLayoutEmptyMyAccount = findViewById(R.id.layout_empty_myaccount);

        if(OpenBankDataMgr.getInstance().getAccountCnt() > 0) {
            makeMyAccountList();
        }else{
            requestOpenBankList();
        }

        //연락처 관련
        mTvContactCount = findViewById(R.id.tv_contact_count);
        mContactAdapter = new ContactsInfoAdapter(mContext,mContactArrayList,null);
        mLayoutContact  = findViewById(R.id.layout_contact);
        mListViewContact = findViewById(R.id.listview_contact);
        mListViewContact.setAdapter(mContactAdapter);
        mListViewContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactsInfo contactsInfo = mContactArrayList.get(position);

                final String TLNO = contactsInfo.getTLNO();
                final String FLNM = contactsInfo.getFLNM();
                mListener.onSelectPhoneItem(FLNM,TLNO);
                dismiss();
            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            mListViewContact.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    Logs.e("onScrollChange - getFirstVisiblePosition : " + mListViewContact.getFirstVisiblePosition());
                    Logs.e("onScrollChange - getLastVisiblePosition : " + mListViewContact.getLastVisiblePosition());
                    Logs.e("onScrollChange - mListBackupIndex : " + mContactListBackupIndex);
                    if(mContactListBackupIndex < 0 || mContactListBackupIndex == mListViewContact.getFirstVisiblePosition()){
                        mContactListBackupIndex = mListViewContact.getFirstVisiblePosition();
                        return;
                    }
                    if(mContactListBackupIndex < mListViewContact.getFirstVisiblePosition()){
                        Logs.e("draw MAX");
                        if(mListViewContact.getLastVisiblePosition() > 7) {
                            resizeDialogMax();
                        }
                    }else{
                        Logs.e("draw ORIGIN");
                        if(mListViewContact.getFirstVisiblePosition() < 1) {
                            resizeDialogOrigin();
                        }
                    }
                    mContactListBackupIndex = mListViewContact.getFirstVisiblePosition();
                }
            });
        }
        findViewById(R.id.layout_btn_resync).setOnClickListener(this);
        mLayoutEmptycontact = findViewById(R.id.layout_empty_contacts);
        getContactListFromStorageFile();

        //은행/계좌 직접입력 버튼
        mLayoutDirectInput = findViewById(R.id.layout_direct_input);
        mTvDirecctInput = findViewById(R.id.btn_direct_input);
        mTvDirecctInput.setOnClickListener(this);
        radius = (int) Utils.dpToPixel(mContext,6);
        mTvDirecctInput.setBackground(GraphicUtils.getRoundCornerDrawable("#00ebff",new int[]{radius,radius,radius,radius}));

        changeTabState();
        showCurrentTabList();

        if(ITransferDataMgr.getInstance().getTRANSFER_ACCESS_TYPE() != (ITransferDataMgr.ACCESS_TYPE_NORMAL)){
            resizeDialogMax();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_tab_recently:
                hideCurrentTabList();
                mCurrentTab = TAB_RECENTLY;
                changeTabState();
                showCurrentTabList();
                break;
            case R.id.tv_tab_myaccount:
                hideCurrentTabList();
                mCurrentTab = TAB_MYACCOUNT;
                changeTabState();
                showCurrentTabList();
                break;
            case R.id.tv_tab_contact:
                //사이다이체가 아니면...
                if( !TransferUtils.isSaidaAccount()
                    ||LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN().equalsIgnoreCase("Y")
                    ||LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN().equalsIgnoreCase("Y")){
                    //오픈뱅킹 계좌일경우엔 휴대폰 이체 사용불가
                    mListener.onShowErrorAlertMsg();
                    dismiss();
                    return;
                }

                //다건에서 호출된 것이면 에러 출력.
                if(mContext instanceof ITransferSendMultiActivity){
                    mListener.onShowErrorAlertMsg();
                    dismiss();
                    return;
                }

                if (!PermissionUtils.checkPermission(mContext, perList)) {
                    mListener.onAskPhonePermission();
                    dismiss();
                    return;
                }

                hideCurrentTabList();
                mCurrentTab = TAB_CONTACT;
                changeTabState();
                showCurrentTabList();
                break;
            case R.id.layout_btn_resync:
                getContactListFromContactApp(true);
                break;
            case R.id.btn_direct_input:
                mListener.onClickDirectInput();
                dismiss();
                break;
        }
    }

    private void hideCurrentTabList(){
        switch (mCurrentTab){
            case TAB_RECENTLY:
                mLayoutRecently.setVisibility(View.GONE);
                break;
            case TAB_MYACCOUNT:
                mLayoutMyAccount.setVisibility(View.GONE);
                break;
            case TAB_CONTACT:
                mLayoutContact.setVisibility(View.GONE);
                break;
        }
    }

    private void showCurrentTabList(){
        Logs.e("showCurrentTabList : " + mCurrentTab);
        mLayoutDirectInput.setVisibility(View.GONE);
        switch (mCurrentTab){
            case TAB_RECENTLY:
                if(mRecentlyArrayList.size() > 0){
                    mListViewRecently.setVisibility(View.VISIBLE);
                    mLayoutEmptyRecently.setVisibility(View.GONE);
                }else{
                    mListViewRecently.setVisibility(View.GONE);
                    mLayoutEmptyRecently.setVisibility(View.VISIBLE);
                }
                mLayoutRecently.setVisibility(View.VISIBLE);
                mLayoutDirectInput.setVisibility(View.VISIBLE);
                break;
            case TAB_MYACCOUNT:
                mLayoutMyAccount.setVisibility(View.VISIBLE);
                mLayoutDirectInput.setVisibility(View.VISIBLE);
                break;
            case TAB_CONTACT:
                Logs.e("showCurrentTabList - mContactArrayList: " + mContactArrayList.size());
                if(mContactArrayList.size() > 0){
                    mListViewContact.setVisibility(View.VISIBLE);
                    mLayoutEmptycontact.setVisibility(View.GONE);
                }else{
                    mListViewContact.setVisibility(View.GONE);
                    mLayoutEmptycontact.setVisibility(View.VISIBLE);
                }
                mLayoutContact.setVisibility(View.VISIBLE);
                break;
        }
    }

    private void changeTabState(){
        int radius = (int) Utils.dpToPixel(mContext,15);
        mTvTabRecently.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mTvTabRecently.setTextColor(Color.parseColor("#999999"));
        mTvTabMyAccount.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mTvTabMyAccount.setTextColor(Color.parseColor("#999999"));
        mTvTabContact.setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,radius,radius}));
        mTvTabContact.setTextColor(Color.parseColor("#999999"));
        switch (mCurrentTab){
            case TAB_RECENTLY:
                mTvTabRecently.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{radius,radius,radius,radius}));
                mTvTabRecently.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            case TAB_MYACCOUNT:
                mTvTabMyAccount.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{radius,radius,radius,radius}));
                mTvTabMyAccount.setTextColor(Color.parseColor("#FFFFFF"));
                break;
            case TAB_CONTACT:
                mTvTabContact.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{radius,radius,radius,radius}));
                mTvTabContact.setTextColor(Color.parseColor("#FFFFFF"));
                break;
        }
    }
    /**
     * 각 리스트뷰의 TOP 마진값이 위치상 이상하여 헤더와 푸터를 넣어준다.
     *
     * @param isFooter true - 리스트뷰의 풋터, false - 리스트뷰의 헤더뷰
     * @return
     */
    private View getEmptyHeaderFooterView(boolean isFooter){

        int height = (int)Utils.dpToPixel(mContext,19f);
        if(isFooter){
            height = (int)Utils.dpToPixel(mContext,80f);
        }

        View footer = getLayoutInflater().inflate(R.layout.layout_listview_header_footer_empty, null, false) ;

        View view = footer.findViewById(R.id.view);
        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.height = height;
        view.setLayoutParams(params);


        return footer;
    }

    /**
     * 오픈뱅킹 계좌 조회
     */
    private void requestOpenBankList(){
        Map param = new HashMap();
        param.put("BLNC_INQ_YN", "N");

        HttpUtils.sendHttpTask(WasServiceUrl.MAI0080100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                try {

                    JSONObject object = new JSONObject(ret);

                    //여기는 입금이 가능한 리스트만 보여주면 되기에 모델링한 값은 건들지 않도록.
                    //모델링한 값은 출금계좌에 사용한다.
                    OpenBankDataMgr.getInstance().parsorAccountList(object);
                    makeMyAccountList();

                } catch (JSONException e) {
                    Logs.printException(e);
                }

            }
        });
    }

    /**
     * 물고 들어온 계좌를 리스트에서 보이지 않기위해...
     */
    private void makeMyAccountList(){
        mMyAccountArrayList.clear();
        mMyAccountArrayList.addAll(LoginUserInfo.getInstance().getMyAccountInfoArrayList());

        //입이 가능한 계좌를 모두 보여주도록 한다.
        mOpenBankAccountArrayList.clear();
        mOpenBankAccountArrayList.addAll(OpenBankDataMgr.getInstance().getOpenBankInOutAccountList());


        //물고들어온 출금계좌가 있으면 리스트에서 삭제해준다.
        String BANK_CD = ITransferDataMgr.getInstance().getWTCH_BANK_CD();
        String ACNO    = ITransferDataMgr.getInstance().getWTCH_ACNO();

        if(BANK_CD.equalsIgnoreCase("028")){
            for(int i=0;i<mMyAccountArrayList.size();i++){
                MyAccountInfo accountInfo = mMyAccountArrayList.get(i);
                if(accountInfo.getACNO().equalsIgnoreCase(ACNO)){
                    mMyAccountArrayList.remove(i);
                }
            }
        }else{
            for(int i=0;i<mOpenBankAccountArrayList.size();i++){
                OpenBankAccountInfo accountInfo = mOpenBankAccountArrayList.get(i);
                if(accountInfo.RPRS_FNLT_CD.equalsIgnoreCase(BANK_CD)
                        &&accountInfo.ACNO.equalsIgnoreCase(ACNO)){
                    mOpenBankAccountArrayList.remove(i);
                }
            }
        }

        mMyAccountAdapter.setMyAccOpenBankAccArray(mMyAccountArrayList,mOpenBankAccountArrayList);

        if(mMyAccountArrayList.size() + mOpenBankAccountArrayList.size() == 0){
            mListViewMyAccount.setVisibility(View.GONE);
            mLayoutEmptyMyAccount.setVisibility(View.VISIBLE);
        }else{
            mListViewMyAccount.setVisibility(View.VISIBLE);
            mLayoutEmptyMyAccount.setVisibility(View.GONE);
        }
    }

    /**
     * 연락처 처리 관련 함수
     */
    //이미 서버와 싱크해둔 파일에 스토리지에 저장되어 있을때
    private void getContactListFromStorageFile(){
        getContactList(GetContactListAsyncTask.GET_TYPE_STORAGE_SAVEFILE,false,false);
    }

    /**
     * 처음 연락처 권한을 설정하고 연락처 앱에서 주소록을 가져올때.
     * @param syncServer - 사이다 서버와 동기화를 진행 할것인지.
     */
    private void getContactListFromContactApp(boolean syncServer){
        boolean showProgress = false;
        if(syncServer){
            showProgress = true;
        }
        getContactList(GetContactListAsyncTask.GET_TYPE_CONTACT_APP,syncServer,showProgress);
    }

    /**
     * 연락처를 가져온다.
     *
     * @param getType
     * GetContactListAsyncTask.GET_TYPE_SAIDA_SERVER - 핸드폰에 저장된 연락처앱에서 가져온다. 서버와 싱크가 필요하다.
     * GetContactListAsyncTask.GET_TYPE_STORAGE_SAVEFILE - 이미 싱크가 되어 저장되어 있는 파일에서 가져온다.
     *
     * @param syncServer : 연락처 앱에서 조회후에 사이다 서버와 싱크할지 그냥 보여줄지.
     * @param showProgress
     */
    private void getContactList(final int getType,final boolean syncServer,final boolean showProgress){

        GetContactListAsyncTask gpbat = new GetContactListAsyncTask(mContext,
                getType,
                new GetContactListAsyncTask.OnFinishReadPhoneBook() {
                    @Override
                    public void onFinishReadPhoneBook(ArrayList<ContactsInfo> array) {
                        Logs.e("getContactList end - size : " + array.size());
                        mContactArrayList.clear();
                        if(array != null && array.size() > 0){

                            if(getType == GetContactListAsyncTask.GET_TYPE_CONTACT_APP){
                                if(syncServer){
                                    //모바일폰북에서 전화번호를 가져왔으면 사이다서버와 싱크를 맞춘다.
                                    requestContactListCompareSaidaServer(array,showProgress);
                                    return;
                                }
                            }
                            mContactArrayList.addAll(array);
                            mContactAdapter.setContactList(mContactArrayList);

                            String msg = "전체 " + mContactArrayList.size()+ "건";
                            Utils.setTextWithSpan(mTvContactCount,msg,String.valueOf(mContactArrayList.size()), Typeface.BOLD,"#a1a9bb");

                            if(mCurrentTab == TAB_CONTACT){
                                showCurrentTabList();
                            }
                        }
                    }
                });
        gpbat.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    /**
     * 즐겨찾기 계좌관리 요청
     *
     * @param activity
     * @param accountInfo 자주/즐겨찾기 정보
     */
    private void requestFavoriteAccount(final Activity activity, ITransferRecentlyInfo accountInfo) {
        MLog.d();

        // TODO. 현재 임시 통장이라 예금주가 없음
        if (DataUtil.isNull(accountInfo.getFAVO_ACCO_YN())
                || DataUtil.isNull(accountInfo.getMNRC_BANK_CD())
                || DataUtil.isNull(accountInfo.getMNRC_ACNO())) {
            return;
        }

        Map param = new HashMap();
        param.put("REG_STCD", Const.REQUEST_WAS_YES.equalsIgnoreCase(accountInfo.getFAVO_ACCO_YN()) ? "1" : "0");
        param.put("CNTP_FIN_INST_CD", accountInfo.getMNRC_BANK_CD());
        param.put("CNTP_BANK_ACNO", accountInfo.getMNRC_ACNO());
        param.put("CNTP_ACCO_DEPR_NM", accountInfo.getMNRC_ACCO_DEPR_NM());
        if (DataUtil.isNotNull(accountInfo.getMNRC_ACCO_ALS())) {
            param.put("CNTP_ACCO_ALS", accountInfo.getMNRC_ACCO_ALS());
        }

        showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0010700A02.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                if(onCheckHttpError(ret,false)){
                    dismissProgressDialog();
                    return;
                }

                requestRecentlyList();
            }
        });
    }

    /**
     * 최근,즐겨찾기 리스트 조회
     */
    private void requestRecentlyList(){
        Map param = new HashMap();
        param.put("INQ_DVCD", "9"); // 9번이 전체조회이다.-계좌,휴대폰,그룹 모두 조회. 값 않넣어줘도 된다.


        showProgressDialog();
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0140100A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC");
                    if (array == null) {
                        if(mCurrentTab == TAB_RECENTLY){
                            showCurrentTabList();
                        }
                        return;
                    }

                    mRecentlyArrayList.clear();

                    for (int i = 0; i < array.length(); i++) {
                        JSONObject obj = array.getJSONObject(i);
                        mRecentlyArrayList.add(new ITransferRecentlyInfo(obj));
                    }

                    mRecentlyAdapter.setRecentlyArrayList(mRecentlyArrayList);

                    //추후 받는분선택 다이얼로그에서 사용하기 위해 넣어둔다.
                    ITransferDataMgr.getInstance().setRecentlyArrayList(mRecentlyArrayList);

                    //화면을 Visible한다.
                    if(mCurrentTab == TAB_RECENTLY){
                        showCurrentTabList();
                    }
                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }

    /**
     * 휴대전화번호로 사이다 가입여부 조회
     * @param listContactsInPhonebook 주소록에 저장된 주소 리스트
     */
    void requestContactListCompareSaidaServer(final ArrayList<ContactsInfo> listContactsInPhonebook,final boolean showProgress) {

        JSONArray jsonArray = ContactsUtil.convertToJsonArray(listContactsInPhonebook);
        //폰에 저장된 연락처가 없으면 동기화 하지 않는다.
        if(jsonArray.length() == 0 ){
            return;
        }

        Map param = new HashMap();
        param.put("REC_IN_TLNO", jsonArray);
        if(showProgress)
            showProgressDialog();

        HttpUtils.sendHttpTask(WasServiceUrl.TRA0020200A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                Logs.i("TRA0020200A01 : " + ret);

                if(showProgress)
                    dismissProgressDialog();

                if(onCheckHttpError(ret,false)){
                    return;
                }

                try {
                    JSONObject object = new JSONObject(ret);

                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null) {
                        mContactArrayList = listContactsInPhonebook;
                        return;
                    }

                    ContactsUtil.saveSyncContactData(mContext,ret);

                    mContactArrayList.clear();

                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);
                        mContactArrayList.add(contactsInfo);
                    }

                    mContactAdapter.setContactList(mContactArrayList);

                    String msg = "전체 " + mContactArrayList.size()+ "건";
                    Utils.setTextWithSpan(mTvContactCount,msg,String.valueOf(mContactArrayList.size()), Typeface.BOLD,"#a1a9bb");

                    //화면을 보여준다.
                    if(mCurrentTab == TAB_CONTACT){
                        showCurrentTabList();
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                }
            }
        });
    }

    private void showProgressDialog(){
        if (mContext.isFinishing()) return;

        if(mProgressBarLayout.getVisibility() != View.VISIBLE)
            mProgressBarLayout.setVisibility(View.VISIBLE);
    }

    private void dismissProgressDialog(){
        if (mContext.isFinishing()) return;
        Logs.e("dismissProgressDialog - 1");

        mProgressBarLayout.setVisibility(View.GONE);
    }

    public interface OnSelectItemListener {
        void onSelectAccountItem(String bankCd,String detailBankCd, String accountNo, String recvNm);
        void onSelectPhoneItem(String name, String phoneNo);
        void onSelectGroupItem(String groupId,String favorite,String groupCntText);
        void onClickDirectInput();
        void onShowErrorAlertMsg();
        void onAskPhonePermission();
        void onDismissDialog();
    }
}
