package com.sbi.saidabank.common.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Prefer;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Saidabank_android
 * Class: SelectDevDialog
 * Created by 950546 on 2019. 02. 26..
 * <p>
 * Description: 디버깅용 서버 선택 다이얼로그
 *
 * 현재 가입한 서버는 체크박스로 표시해준다.
 * 현재 가입된 서버가 아닌 다른 서버를 선택하면 새로 가입해야 한다.
 * -이유는 센스톤에서는 1개만 유효한 접근을 허용한다.
 *  가입자 정보를 같게 올려서 서버에서 ok를 내려보내도 서버 데이타인 uafRequest을
 *  라이브러리에 넣어주면 잘못된 파라미터라고 나와 인증에 실패한다.
 *
 */
public class SelectDevDialog extends BaseDialog implements View.OnClickListener {

    private Context mContext;
    private SelectDevDialog.OnConfirmListener mListener;
    private int mTitleClickCnt;

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
        void onIamDeveloper();
    }

    public void setOnCofirmListener(OnConfirmListener listener) {
        this.mListener = listener;
    }

    public SelectDevDialog(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @SuppressLint({"WifiManagerPotentialLeak", "SetTextI18n"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_select_dev);
        setDialogWidth();

        findViewById(R.id.title).setOnClickListener(this);

        TextView devBtn = findViewById(R.id.btn_dev);
        devBtn.setOnClickListener(this);
        TextView testBtn = findViewById(R.id.btn_test);
        testBtn.setOnClickListener(this);
        TextView operBtn = findViewById(R.id.btn_op);
        operBtn.setOnClickListener(this);


        ((TextView) findViewById(R.id.tv_version)).setText("app version : " + Utils.getVersionName(mContext) + " / " + Utils.getVersionCode(mContext));

        int joinServerIdx = Prefer.getJoinDebugServerIndex(getContext());
        if(joinServerIdx == Const.DEBUGING_SERVER_OPERATION){
            findViewById(R.id.layout_opera).setVisibility(View.VISIBLE);
        }

        //이것을 넣어준이유!
        //다른 서버 선택하고 서비스 가입 화면으로 이동하면 아래 값들이 모두 false가 되기에...
        //이미 어느 서버에 가입되어 있으면 최소 패턴과 핀은 true일것이므로 바꿔준다.
        if(joinServerIdx != Const.DEBUGING_SERVER_NONE){
            String objStr = Prefer.getDebugBackupAuthStatus(getContext());
            if(!TextUtils.isEmpty(objStr)){
                Logs.e("SelectDevDialog : "  + objStr);
                try {
                    JSONObject obj = new JSONObject(objStr);

                    Prefer.setPatternRegStatus(getContext(), (boolean)obj.get("PATTERN_STATUS"));
                    Prefer.setPincodeRegStatus(getContext(), (boolean)obj.get("PINCODE_STATUS"));
                    Prefer.setFidoRegStatus(getContext(), (boolean)obj.get("FIDO_STATUS"));
                    Prefer.setFlagFingerPrintChange(getContext(), (boolean)obj.get("FIDO_CHANGE_STATUS"));
                } catch (JSONException e) {
                    Logs.printException(e);
                }

                Prefer.setDebugBackupAuthStatus(getContext(),"");
            }

            //현재 서비스 가입한 서버에 체크박스를 보여준다.
            int joinServerIndex = Prefer.getJoinDebugServerIndex(getContext());
            switch (joinServerIndex){
                case Const.DEBUGING_SERVER_DEV:
                    findViewById(R.id.check_dev).setVisibility(View.VISIBLE);
                    break;
                case Const.DEBUGING_SERVER_TEST:
                    findViewById(R.id.check_test).setVisibility(View.VISIBLE);
                    break;
                case Const.DEBUGING_SERVER_OPERATION:
                    findViewById(R.id.check_op).setVisibility(View.VISIBLE);
                    break;
            }
        }

        /*
         * 디버깅 모드에서 개발서버는 공유기로만 접근이 가능하다.
         * 테스트 서버는 공유기, 모바일데이타 두가지로 접근이 가능하다.
         */
        HttpUtils.NetState state = HttpUtils.checkNetworkState(getContext());
        if (state == HttpUtils.NetState.NET_STATE_WIFI) {
            WifiManager manager = (WifiManager) mContext.getSystemService(Context.WIFI_SERVICE);
            WifiInfo wifiInfo = manager.getConnectionInfo();
            if (DataUtil.isNotNull(wifiInfo.getSSID())) {
                ((TextView) findViewById(R.id.tv_wifiname)).setText("연결된 WiFi : " + wifiInfo.getSSID());

                /*
                if(wifiInfo.getSSID().contains("MPAPT")){
                    devBtn.setBackgroundColor(getContext().getResources().getColor(R.color.color00EBFF));
                    testBtn.setText("테스트 서버 접근 불가");
                    testBtn.setEnabled(false);
                    testBtn.setTextColor(Color.parseColor("#BDBDBD"));
                    testBtn.setBackgroundColor(getContext().getResources().getColor(R.color.colorE4E9EC));
                }
                 */
            }
        }else if(state == HttpUtils.NetState.NET_STATE_MOBILE) {
            ((TextView) findViewById(R.id.tv_wifiname)).setText("Mobile Data 4G/5G 사용중입니다.");
            devBtn.setText("개발 서버 접근 불가");
            devBtn.setEnabled(false);
            devBtn.setTextColor(Color.parseColor("#BDBDBD"));
        }
    }

    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);
        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            return false;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }

    @Override
    public void onBackPressed() {
        if (DataUtil.isNotNull(mListener)) {
            mListener.onConfirmPress(Const.DEBUGING_SERVER_NONE);
        }
        dismiss();
        //super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title:
                mTitleClickCnt++;
                if(mTitleClickCnt >= 2){
                    findViewById(R.id.layout_opera).setVisibility(View.VISIBLE);
                    mListener.onIamDeveloper();
                }
                break;

            case R.id.btn_dev:
                if (DataUtil.isNotNull(mListener)) {
                    SaidaUrl.serverIndex = Const.DEBUGING_SERVER_DEV;
                    mListener.onConfirmPress(SaidaUrl.serverIndex);
                }
                dismiss();
                break;

            case R.id.btn_test:
                if (DataUtil.isNotNull(mListener)) {
                    SaidaUrl.serverIndex = Const.DEBUGING_SERVER_TEST;
                    mListener.onConfirmPress(SaidaUrl.serverIndex);
                }
                dismiss();
                break;
            case R.id.btn_op:
                if (DataUtil.isNotNull(mListener)) {
                    SaidaUrl.serverIndex = Const.DEBUGING_SERVER_OPERATION;
                    mListener.onConfirmPress(SaidaUrl.serverIndex);
                }
                dismiss();
                break;
            default:
                break;
        }
    }

    /**
     * 노티에 접속한 개발 서버를 알려준다.
     */
    private void showNotification(int id){
        /*
        String channelId = "SBK";
        String msg = "[개발] 서버 접속중입니다.";
        if(id == R.id.btn_test){
            msg = "[테스트] 서버 접속중입니다.";
        }
        NotificationManager nm = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channelId, channelId, NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setDescription(channelId);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.GREEN);
            notificationChannel.enableVibration(false);
            notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            nm.createNotificationChannel(notificationChannel);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), channelId)
                    .setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.drawable.flk_service_icon)
                    .setColor(0xff009beb)
                    .setColorized(true)
                    //.setContentTitle(title)
                    .setContentText(msg)
                    .setAutoCancel(true);
            nm.notify(1234567, mBuilder.build());
        }else{
            Notification.Builder mBuilder = new Notification.Builder(getContext());
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(getContext().getResources(), R.mipmap.ic_launcher));
            mBuilder.setSmallIcon(R.drawable.flk_service_icon);
            mBuilder.setColor(0xff009beb);
            mBuilder.setContentText(msg);
            mBuilder.setStyle(new Notification.BigTextStyle().bigText(msg));
            mBuilder.setAutoCancel(true);
            mBuilder.setPriority(Notification.PRIORITY_DEFAULT);
            nm.notify(1234567, mBuilder.build());
        }

         */
    }
}
