package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.MotionEvent;
import android.view.View;
import android.widget.CheckBox;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Prefer;

/**
 * Saidabank_android
 * Class: SlidingDevModeCheckDialog
 * Created by 950546 on 2018. 11. 02..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 디버깅모드 확인 다이얼로그
 */
public class SlidingDevModeCheckDialog extends SlidingBaseDialog implements View.OnClickListener {

    private Context mcontext;

    public SlidingDevModeCheckDialog(@NonNull Context context) {
        super(context);
        mcontext = context;
    }

    public View.OnClickListener mPListener;
    public View.OnClickListener mNListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_devmodecheck);
        setDialogWidth();

        findViewById(R.id.btn_confirm).setOnClickListener(this);
        findViewById(R.id.btn_cancel).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Prefer.setFlagDontShowDevModeCheckDialog(mcontext, ((CheckBox) findViewById(R.id.cb_devmodecheck)).isChecked());

        switch (v.getId()) {
            case R.id.btn_cancel: {
                if (mNListener != null) {
                    mNListener.onClick(v);
                }
                dismiss();
                break;
            }
            case R.id.btn_confirm: {
                if (mPListener != null) {
                    mPListener.onClick(v);
                }
                dismiss();
                break;
            }

            default:
                break;
        }
    }

    // 다이얼로그 바깥 영역 터치 시 종료되지 않도록 처리
    @Override
    public boolean dispatchTouchEvent(@NonNull MotionEvent ev) {
        Rect dialogBounds = new Rect();
        getWindow().getDecorView().getHitRect(dialogBounds);

        if (!dialogBounds.contains((int) ev.getX(), (int) ev.getY())) {
            return false;
        } else {
            return super.dispatchTouchEvent(ev);
        }
    }
}
