package com.sbi.saidabank.common.dialog;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.transaction.safedeal.SlideBaseLayout;
import com.sbi.saidabank.activity.transaction.safedeal.SlideMyAccountLayout;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.WasServiceUrl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class KCBProgressDialog extends Dialog {

    private boolean isReqeustEnd;
    private boolean isTimerEnd;
    private OnKCBResultListener mResultListener;
    private boolean isResult;
    private String  mErrorCode;
    private String  mErrorMsg;
    public KCBProgressDialog(Context context,OnKCBResultListener listener) {
        super(context);
        mResultListener = listener;
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);

        getWindow().setDimAmount(0.3f);
        setContentView(R.layout.dialog_kcb_progress);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
            if(isReqeustEnd){
                closeDialog();
            }
            isTimerEnd = true;
            }
        }, 2000);

    }

    public void setRequestEnd(boolean result,String errorCode,String errorMsg){
        isResult = result;
        mErrorCode = errorCode;
        mErrorMsg = errorMsg;

        isReqeustEnd = true;
        if(isTimerEnd){
            closeDialog();
        }
    }

    private void closeDialog(){
        if(mResultListener != null){
            mResultListener.onKCBResult(isResult,mErrorCode,mErrorMsg);
        }
        dismiss();
    }

    public interface OnKCBResultListener{
        void onKCBResult(boolean result,String errorCode,String errorMsg);
    }
}
