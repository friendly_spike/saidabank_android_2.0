package com.sbi.saidabank.common.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.define.datatype.common.MyAccountInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: TransferAccountAdapter
 * Created by 950485 on 2019. 01. 02..
 * <p>
 * Description:아래에서 위로 올라오는 아래에서 위로 올라오는 최근/자주 이체한 계좌번호 다이얼로그 adapter
 */
public class MyAccountAdapter extends BaseAdapter {

    private Context context;
    //    private boolean isNotClick = false;
    private int selectedIndex = -1;
    private ArrayList<MyAccountInfo> dataList;

    public MyAccountAdapter(Context context, ArrayList<MyAccountInfo> dataList) {
        this.context = context;
        this.dataList = dataList;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        MyAccountAdapter.AccountViewHolder viewHolder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dialog_my_account_list_item, null);
            viewHolder = new MyAccountAdapter.AccountViewHolder();
            viewHolder.imageRepresentation = (ImageView) convertView.findViewById(R.id.imageview_representation);
            viewHolder.layoutItem = (LinearLayout) convertView.findViewById(R.id.layout_my_account_list_item);
            viewHolder.textName = (TextView) convertView.findViewById(R.id.textview_name_my_account);
            viewHolder.imageDelay = (ImageView) convertView.findViewById(R.id.imageview_delay);
            viewHolder.imageShare = (ImageView) convertView.findViewById(R.id.imageview_share);
            viewHolder.textWithdrawableAmt = (TextView) convertView.findViewById(R.id.textview_withdrawable_my_account);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (MyAccountAdapter.AccountViewHolder) convertView.getTag();
        }

//        viewHolder.layoutItem.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//                return isNotClick ? true : false;
//            }
//        });

        MyAccountInfo accountInfo = (MyAccountInfo) dataList.get(position);
        if (DataUtil.isNotNull(accountInfo)) {

            viewHolder.imageRepresentation.setVisibility((selectedIndex == position) ? View.VISIBLE : View.INVISIBLE);
            viewHolder.textName.setTextColor((selectedIndex == position) ? ContextCompat.getColor(context, R.color.black) : ContextCompat.getColor(context, R.color.color888888));
            viewHolder.textWithdrawableAmt.setTextColor((selectedIndex == position) ? ContextCompat.getColor(context, R.color.color009BEB) : ContextCompat.getColor(context, R.color.color888888));

            String ACCO_ALS = accountInfo.getACCO_ALS();
            if (TextUtils.isEmpty(ACCO_ALS)) {
                ACCO_ALS = accountInfo.getPROD_NM();
            }

            String ACNO = accountInfo.getACNO();
            if (!TextUtils.isEmpty(ACNO)) {
                int lenACNO = ACNO.length();
                if (lenACNO > 4)
                    ACNO = ACNO.substring(lenACNO - 4, lenACNO);
            }

            String name = ACCO_ALS + " [" + ACNO + "]";
            viewHolder.textName.setText(name);

            String ACCO_BLNC = accountInfo.getACCO_BLNC();
            if (!TextUtils.isEmpty(ACCO_BLNC)) {
                String amount = Utils.moneyFormatToWon(Double.valueOf(ACCO_BLNC));
                viewHolder.textWithdrawableAmt.setText(amount + context.getString(R.string.won));
            }

            String DLY_TRNF_SVC_ENTR_YN = LoginUserInfo.getInstance().getDLY_TRNF_SVC_ENTR_YN();
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DLY_TRNF_SVC_ENTR_YN)) {
                viewHolder.imageDelay.setImageResource(R.drawable.ico_clock);
                viewHolder.imageDelay.setVisibility(View.VISIBLE);
            }

            //공유형 통장 정보 출력
            String SHRN_ACCO_YN = accountInfo.getSHRN_ACCO_YN();
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(SHRN_ACCO_YN)) {
                viewHolder.imageShare.setVisibility(View.VISIBLE);
            }else{
                viewHolder.imageShare.setVisibility(View.GONE);
            }


            String DSGT_MNRC_ACCO_SVC_ENTR_YN = LoginUserInfo.getInstance().getDSGT_MNRC_ACCO_SVC_ENTR_YN();
            if (Const.REQUEST_WAS_YES.equalsIgnoreCase(DSGT_MNRC_ACCO_SVC_ENTR_YN)) {
                viewHolder.imageDelay.setImageResource(R.drawable.ico_check_fill);
                viewHolder.imageDelay.setVisibility(View.VISIBLE);
            }
        }
        return convertView;
    }

//    public void setIsNotClick(boolean isNotClick) {
//        this.isNotClick = isNotClick;
//    }

    public void setSelectedIndex(int selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    class AccountViewHolder {
        LinearLayout layoutItem;
        ImageView imageRepresentation;
        TextView textName;
        TextView textWithdrawableAmt;
        ImageView imageDelay;
        ImageView imageShare;
    }
}
