package com.sbi.saidabank.common.util;

import androidx.core.content.FileProvider;

/**
 * siadabank_android
 * Class: ImageFileProvider
 * Created by 950546.
 * Date: 2019-02-01
 * Time: 오전 9:38
 * Description: manifests파일에 Provider 다중 선언을 위한 dummy class
 */
public class ImageFileProvider extends FileProvider {
}
