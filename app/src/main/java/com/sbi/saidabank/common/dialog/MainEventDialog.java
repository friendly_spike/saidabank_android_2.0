package com.sbi.saidabank.common.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.WindowManager;
import android.widget.FrameLayout;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.BaseWebView;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.web.JavaScriptBridgeEvent;

public class MainEventDialog extends Dialog {

    private Context mContext;
    private String mEventId;

    public interface OnListener {
        void onFinishEvent(String moveUrl);
    }

    public MainEventDialog(Context context, String eventID, OnListener clickListener) {
        super(context, R.style.TransparentProgressDialog);
        this.mContext = context;
        this.mEventId = eventID;
        setCancelable(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_main_event);
        setDialogSize();
        initUX();
    }

    @Override
    public void show() {
        MLog.d();
        super.show();
    }

    private void setDialogSize() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        getWindow().setAttributes(lp);
    }

    @SuppressLint("JavascriptInterface")
    private void initUX() {
        FrameLayout mContainer = (FrameLayout) findViewById(R.id.container_webview);
        JavaScriptBridgeEvent mJsBridge = new JavaScriptBridgeEvent(MainEventDialog.this, mContext, mContainer);
        BaseWebView webView = (BaseWebView) findViewById(R.id.webview_main_event);
        webView.addJavascriptInterface(mJsBridge, JavaScriptBridgeEvent.CALL_NAME);
        if (DataUtil.isNotNull(mEventId)) {
            String url = WasServiceUrl.MAI0010500.getServiceUrl();
            String param = "EVNT_ID=" + mEventId;
            webView.postUrl(url, param.getBytes());
        }
    }
}
