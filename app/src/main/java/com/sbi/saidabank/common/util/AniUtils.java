package com.sbi.saidabank.common.util;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewGroup;

/**
 * AniUtils : 애니메이션 관련 유틸
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class AniUtils {

    private static final int ANIMATION_TIME = 400;

    /**
     * 해당뷰를 좌우로 흔드는 에니메이션
     *
     * @param view : 흔들 뷰
     */
    public static void shakeView(View view) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", 50f, -50f, 50f, -50f, 0);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 해당뷰를 좌우로 흔드는 에니메이션
     *
     * @param view   : 흔들 뷰
     * @param offset : 흔들리는 범위
     */
    public static void shakeView(View view, float offset) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", offset, -offset, offset, -offset, 0);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 해당뷰를 X좌표 시작 위치에서 설정한 위치로 이동시키는 애니에션
     *
     * @param view     : 이동시킬 뷰
     * @param startPos : 애니메에션 시작 위치 +,- 값을 넣어주면 됨.
     */
    public static void moveHorizontalView(View view, float startPos,float endPos) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationX", startPos, endPos);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 해당뷰를 Y좌표 시작 위치에서 설정한 위치로 이동시키는 애니에션
     *
     * @param view     : 이동시킬 뷰
     * @param startPos : 애니메에션 시작 위치 +,- 값을 넣어주면 됨.
     */
    public static void moveVerticalView(View view, float startPos,float endPos) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "translationY", startPos, endPos);
        animation.setDuration(ANIMATION_TIME);
        animation.start();
    }

    /**
     * 뷰의 높이 사이즈 변경시 에니메이션 적용.
     *
     * @param toSize
     */
    public static  void changeViewHeightSizeAnimation(final View view,int toSize,int aniTime){
        changeViewHeightSizeAnimation(view,toSize,aniTime,null);
    }
    public static  void changeViewHeightSizeAnimation(final View view,int toSize,int aniTime, Animator.AnimatorListener listener){
        ValueAnimator anim = ValueAnimator.ofInt(view.getHeight(), toSize);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.height = val;
                view.setLayoutParams(layoutParams);
            }
        });
        if(listener != null){
            anim.addListener(listener);
        }

        anim.setDuration(aniTime);
        anim.start();
    }

    /**
     * 뷰의 넓이 사이즈 변경시 애니메이션
     * @param view
     * @param toSize
     * @param aniTime
     * @param listener
     */
    public static  void changeViewWidthSizeAnimation(final View view,int toSize,int aniTime, Animator.AnimatorListener listener){
        ValueAnimator anim = ValueAnimator.ofInt(view.getWidth(), toSize);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
                layoutParams.width = val;
                view.setLayoutParams(layoutParams);
            }
        });
        if(listener != null){
            anim.addListener(listener);
        }

        anim.setDuration(aniTime);
        anim.start();
    }

}
