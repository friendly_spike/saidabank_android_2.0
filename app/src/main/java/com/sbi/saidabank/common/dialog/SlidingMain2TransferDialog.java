package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

public class SlidingMain2TransferDialog extends SlidingBaseDialog {

    private OnConfirmListener mListener;

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
    }

    public SlidingMain2TransferDialog(@NonNull Context context, OnConfirmListener listener) {
        super(context);
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_main2_transfer);
        setDialogWidth();
        setCanceledOnTouchOutside(true);
        initLayout();
    }

    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {
        int radius = (int) Utils.dpToPixel(getContext(),20);
        findViewById(R.id.layout_body).setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,0,0}));


        findViewById(R.id.layout_transfer_account).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onConfirmPress(1);
                    dismiss();
                }
            }
        });
        findViewById(R.id.layout_transfer_safe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    mListener.onConfirmPress(2);
                    dismiss();
                }
            }
        });
    }
}