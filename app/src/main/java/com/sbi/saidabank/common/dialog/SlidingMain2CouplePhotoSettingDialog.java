package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;

public class SlidingMain2CouplePhotoSettingDialog extends SlidingBaseDialog {

    private OnConfirmListener mListener;

    public interface OnConfirmListener {
        void onConfirmPress(int selectIndex);
    }

    public SlidingMain2CouplePhotoSettingDialog(@NonNull Context context, OnConfirmListener listener) {
        super(context);
        this.mListener = listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_main2_couple_photo_setting);

        setDialogWidth();
        setCanceledOnTouchOutside(true);
        initLayout();

    }

    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {
        int radius = (int) Utils.dpToPixel(getContext(),20);
        findViewById(R.id.layout_body).setBackground(GraphicUtils.getRoundCornerDrawable("#ffffff",new int[]{radius,radius,0,0}));


        findViewById(R.id.layout_setting_default).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    dismiss();
                    mListener.onConfirmPress(1);
                }
            }
        });
        findViewById(R.id.layout_setting_album).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    dismiss();
                    mListener.onConfirmPress(2);
                }
            }
        });
        findViewById(R.id.layout_setting_camera).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (DataUtil.isNotNull(mListener)) {
                    dismiss();
                    mListener.onConfirmPress(3);
                }
            }
        });
    }
}