package com.sbi.saidabank.common.dialog;

import android.animation.Animator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.util.AniUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.WasServiceUrl;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Saidabank_android
 * Class: SlidingBaseDialog
 * Created by 950469 on 2018. 10. 11..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 다이얼로그 Base
 */
public class SlidingExpandBaseDialog extends SlidingBaseDialog implements View.OnTouchListener{

    protected LinearLayout mExDialogBody;
    protected int mOriginViewHeight;
    protected int mOriginViewYPos;

    public SlidingExpandBaseDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setGravity(Gravity.BOTTOM);


        setDialogFullHeight(getContext());
    }

    /**
     * 다이얼로그의 최상의 레이아웃을 셋팅한다.
     * @param dialogBody
     */
    protected void setDialogBody(LinearLayout dialogBody){
        mExDialogBody = dialogBody;

        int radius = (int)getContext().getResources().getDimension(R.dimen.safe_deal_slide_redius);
        mExDialogBody.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,0,0}));

        if(mExDialogBody != null){
            mExDialogBody.getViewTreeObserver()
                    .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            mOriginViewHeight = mExDialogBody.getHeight();
                            mOriginViewYPos = (int) mExDialogBody.getY();
                            Logs.e("mOriginViewHeight : " + mOriginViewHeight);
                            Logs.e("mOriginViewYPos : " + mOriginViewYPos);


                            mExDialogBody.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                    });
        }
    }

    /**
     * 터치 이벤트를 처리할 뷰를 등록한다.
     * @param view
     */
    protected void setExpandMoveBar(View view){
        view.setOnTouchListener(this);
    }



    //Move bar를 이용한 레이아웃 조절.
    private float startdY;
    private float moveY;
    private int   startViewHeight;
    private boolean isMove;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v.getId() != R.id.layout_movebar){
            return false;
        }

        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_DOWN - y : " + event.getRawY());
                startdY = event.getRawY();
                isMove = false;
                startViewHeight = mExDialogBody.getHeight();
                break;
            case MotionEvent.ACTION_MOVE:
                moveY =  startdY - event.getRawY();
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - moveY : " + moveY);

                if(moveY == 0){
                    return false;
                }

                //확장하려고 하지만 이미 모두 확장했당.
                if(moveY > 0 && mScreenHeight == startViewHeight){
                    return false;
                }

                isMove = true;
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_MOVE - isMove : " + isMove);
                ViewGroup.LayoutParams params = mExDialogBody.getLayoutParams();
                params.height = startViewHeight + (int)moveY;
                mExDialogBody.setLayoutParams(params);
                mExDialogBody.requestLayout();

                break;

            case MotionEvent.ACTION_UP:
                Logs.e("dispatchTouchEvent - MotionEvent.ACTION_UP - y : " + event.getRawY());
                if(event.getRawY() > mOriginViewYPos){
                    AniUtils.changeViewHeightSizeAnimation(mExDialogBody,mOriginViewHeight,250);
                    return false;
                }

                if(isMove && moveY != 0){
                    int toSize = 0;
                    if(moveY > 0){ //확장
                        if(moveY > 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }else if(moveY < 0){ //복귀
                        moveY = moveY * -1;
                        if(moveY < 100){
                            toSize = mScreenHeight;
                        }else{
                            toSize = mOriginViewHeight;
                        }
                    }
                    AniUtils.changeViewHeightSizeAnimation(mExDialogBody,toSize,250);

                }
                isMove = false;
                break;

            default:
                return false;
        }
        return true;
    }

    private boolean isNowAnimation;
    public void resizeDialogOrigin(){
        if(mExDialogBody.getHeight() == mOriginViewHeight) return;


//        ViewGroup.LayoutParams layoutParams = mExDialogBody.getLayoutParams();
//        layoutParams.height = mOriginViewHeight;
//        mExDialogBody.setLayoutParams(layoutParams);

        if(isNowAnimation) return;
        Logs.e("resizeDialogOrigin");

        AniUtils.changeViewHeightSizeAnimation(mExDialogBody, mOriginViewHeight, 250, new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isNowAnimation = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isNowAnimation = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    public void resizeDialogMax(){
        if(mExDialogBody.getHeight() == mScreenHeight) return;

//        ViewGroup.LayoutParams layoutParams = mExDialogBody.getLayoutParams();
//        layoutParams.height = mScreenHeight;
//        mExDialogBody.setLayoutParams(layoutParams);

        if(isNowAnimation) return;
        Logs.e("resizeDialogMax");

        AniUtils.changeViewHeightSizeAnimation(mExDialogBody,mScreenHeight,250,new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                isNowAnimation = true;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                isNowAnimation = false;
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


    /**
     * Http request시 에러 체크사항 공통으로 만듬.
     * 다이얼로그에서 Request 날릴때는 에러나도 메세지 보여주지 않도록 한다.
     *      *
     * @param ret
     * @param isFinish
     * @return
     */
    public boolean onCheckHttpError(String ret, final boolean isFinish) {
        if (DataUtil.isNull(ret)) {
            return true;
        }

        try {
            JSONObject object = new JSONObject(ret);
            if (DataUtil.isNull(object)) {
                return true;
            }

            JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
            String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
            if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                return true;
            }

        } catch (JSONException e) {
            MLog.e(e);
        }

        return false;
    }
}
