package com.sbi.saidabank.common.dialog;

import android.content.Context;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.adapter.VoicePhishingPreventAskAdapter;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.customview.CustomLinearLayoutManager;

/**
 * SlidingVoicePhishingPreventAskDialog : 보이스피싱 문진 팝업
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-11-04
 */
public class SlidingVoicePhishingPreventAskDialog extends SlidingBaseDialog {

	/** 생성자 */
	private Context mContext;

	/** 이벤트 리스너 */
	private OnItemListener mListener;

	/** 아이템 선택 인터페이스 */
	public interface OnItemListener {
		void onConfirm(boolean result);
	}

	/** 리스트뷰 */
	private RecyclerView mAuthRecyclerView;

	/** 어댑터뷰 */
	private VoicePhishingPreventAskAdapter mAdapter;

	/** 아이템 선택 리스너 */
	public void setOnItemListener(OnItemListener listener) {
		this.mListener = listener;
	}

	public SlidingVoicePhishingPreventAskDialog(Context context) {
		super(context);
		this.mContext = context;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		MLog.d();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_voice_phishing_prevent_ask);
		setDialogWidth();
		mAuthRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
		CustomLinearLayoutManager lm = new CustomLinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
		mAdapter = new VoicePhishingPreventAskAdapter(mContext, mListener);
		mAuthRecyclerView.setLayoutManager(lm);
		mAuthRecyclerView.setAdapter(mAdapter);
	}
}
