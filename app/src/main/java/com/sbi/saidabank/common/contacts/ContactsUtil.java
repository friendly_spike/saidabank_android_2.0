package com.sbi.saidabank.common.contacts;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ContactsUtil {
    private static final char HANGUL_BEGIN_UNICODE = 44032;	// 가
    //private static final char HANGUL_LAST_UNICODE = 55203;	// 힣
    private static final char HANGUL_BASE_UNIT = 588;		// 각자음 마다 가지는 글자수
    // 자음
    private static final char[] INITIAL_SOUND = { 'ㄱ', 'ㄲ', 'ㄴ', 'ㄷ', 'ㄸ', 'ㄹ', 'ㅁ'
            , 'ㅂ', 'ㅃ', 'ㅅ', 'ㅆ', 'ㅇ', 'ㅈ', 'ㅉ'
            , 'ㅊ', 'ㅋ', 'ㅌ', 'ㅍ', 'ㅎ' };

    public static boolean isMatch(String value, String search) {
        int matchCount = 0;
        int searchLen = search.length();
        int seof = value.length() - searchLen;
        // 검색어가 더 길면 false 리턴
        if( seof < 0 ) {
            return false;
        }

        boolean match = false;
        // 검색어 char length
        for (char chValue:value.toCharArray()) {
            for (int i = 0; i < searchLen; i++) {
                int index = i + matchCount;
                if (searchLen <= index || index > matchCount)
                    break;

                char chSearch = (char) search.charAt(index);
                if (isMatchChar(chValue, chSearch)) {
                    matchCount++;
                    match = true;
                    break;
                } else {
                    match = false;
                }
            }

            if (matchCount == searchLen)
                break;

            // matchCount가 올라간 다음 match가 false이면 순차적으로 같은 단어가 아님.
            if (matchCount > 0 && !match)
                break;
        }

        if (matchCount == searchLen)
            return true;

        return false;
    }

    private static boolean isMatchChar(char value, char search) {
        char[] initS = getHangleChar(search);
        char[] initV = getHangleChar(value);
        int length = initS.length;

        if (initV.length < length)
            return false;

        for (int index = 0; index < length; index++) {
            if (initS[index] != initV[index])
                return false;
        }

        return true;
    }

    private static char[] getHangleChar(char value) {
        char[] temp = new char[3];
        int charCount = 0;
        char uniVal = (char) (value - 0xAC00);

        // 한글인경우
        if (uniVal >= 0 && uniVal <= 11172) {
            char choV = (char) ((((uniVal - (uniVal % 28)) / 28) / 21) + 0x1100);
            char jungV = (char) ((((uniVal - (uniVal % 28)) / 28) % 21) + 0x1161);
            char jongV = (char) ((uniVal % 28) + 0x11a7);

            if (choV != 4519) {
                temp[charCount] = getInitialSound(value);
                charCount++;
            }

            if (jungV != 4519) {
                temp[charCount] = jungV;
                charCount++;
            }

            if (jongV != 4519) {
                temp[charCount] = jongV;
                charCount++;
            }
        } else {
            temp[charCount] = value ;
            charCount++;
        }

        char[] result = new char[charCount];
        System.arraycopy(temp, 0, result, 0, charCount);

        return result;
    }

    private static char getInitialSound(char c) {
        int hanBegin = (c - HANGUL_BEGIN_UNICODE);
        int index = hanBegin / HANGUL_BASE_UNIT;
        return INITIAL_SOUND[index];
    }

    /**
     * 연락처 리스트를 사아다 서버와 싱크를 하기 위해 파라미터 값으로 만든다.
     *
     * @param listContacts
     * @return
     */
    public static JSONArray convertToJsonArray(ArrayList<ContactsInfo> listContacts){
        JSONArray jsonArray = new JSONArray();
        for (int index = 0; index < listContacts.size(); index++) {
            ContactsInfo contactsInfo = listContacts.get(index);
            String TLNO = contactsInfo.getTLNO();
            if (TextUtils.isEmpty(TLNO))
                continue;

            try {
                JSONObject itemObject = new JSONObject();
                itemObject.put("TLNO", TLNO);

                String FLNM = contactsInfo.getFLNM();
                if (TextUtils.isEmpty(FLNM)) FLNM = "";
                itemObject.put("FLNM", FLNM);

                jsonArray.put(itemObject);
            } catch (JSONException e) {
            }
        }

        return jsonArray;
    }

    /**
     * Saida 서버와 싱크가 끝난 리턴값을 파일로 저장한다.
     * 매번 싱크를 하면 느리기에 한번 싱크한 값을 파일로 저장하여 재사용하도록 한다.
     *
     * @param context
     * @param ret
     */
    public static void saveSyncContactData(Context context, String ret){
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        if (!storageDir.exists())
            storageDir.mkdir();

        String path =  storageDir + "/" +  Const.CONTACTS_INFO_PATH;
        try {
            FileWriter fw = new FileWriter(path, false);
            fw.write(ret);
            fw.flush();
            fw.close();
        } catch(Exception e){
            Logs.printException(e);
        }
    }
}