package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.datatype.transfer.ITransferSalayMgr;


public class SlidingRetrySalaryDialog extends SlidingBaseDialog implements View.OnClickListener{
    private Context  context;

    public View.OnClickListener pListener;
    public View.OnClickListener nListener;

    public SlidingRetrySalaryDialog(@NonNull Context context, View.OnClickListener pListener, View.OnClickListener nListener) {
        super(context);
        this.context = context;
        this.pListener = pListener;
        this.nListener = nListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView( R.layout.dialog_slide_retry_salary);
        setDialogWidth();

        findViewById(R.id.btn_cancel).setOnClickListener(this);
        findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_cancel:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }
                if (nListener != null) {
                    nListener.onClick(v);
                }
                break;

            case R.id.btn_ok:
                if (context instanceof Activity && !((Activity)context).isFinishing()) {
                    dismiss();
                }

                if (pListener != null) {
                    pListener.onClick(v);
                }
                break;

            default:
                break;
        }
    }
}
