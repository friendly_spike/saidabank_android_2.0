package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sbi.saidabank.R;

/**
 * Saidabank_android
 * Class: SlidingInputTextDialog
 * Created by 950485 on 2018. 11. 28..
 * <p>
 * Description:아래에서 위로 올라오는 문자 입력 다이얼로그
 */

public class SlidingInputTextDialog extends SlidingBaseDialog {
    private TextView  mTextTitle;
    private EditText  mEditInput;

    private Context   context;

    private String    title;
    private String    hint;
    private int       maxLength;
    private String    input;

    private FinishInputListener finishInputListener;

    public SlidingInputTextDialog(@NonNull Context context, String title, String hint, int maxLength, String input, FinishInputListener finishInputListener) {
        super(context);
        this.context = context;
        this.title = title;
        this.hint = hint;
        this.maxLength = maxLength;
        this.input = input;

        this.finishInputListener = finishInputListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_input_text);
        setDialogWidth();

        initUX();
    }

    private void initUX() {
        mTextTitle = (TextView) findViewById(R.id.textview_input_dialog);
        mEditInput = (EditText) findViewById(R.id.edittext_input_dialog);

        if (maxLength > 0) {
            InputFilter[] filterArray = new InputFilter[1];
            filterArray[0] = new InputFilter.LengthFilter(maxLength);
            mEditInput.setFilters(filterArray);
        }

        if (!TextUtils.isEmpty(title))
            mTextTitle.setText(title);

        if (!TextUtils.isEmpty(hint))
            mEditInput.setHint(hint);

        if (!TextUtils.isEmpty(input)) {
            mEditInput.setText(input);
            mEditInput.setSelection(input.length());
        }

        mEditInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                int result = actionId & EditorInfo.IME_MASK_ACTION;

                switch (result) {
                    case EditorInfo.IME_ACTION_DONE :
                        finishDialog();
                        return true;

                    default:
                        break;
                }
                return false;
            }
        });

        Button btnCancel = (Button) findViewById(R.id.layout_cancel_input);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });

        Button btnOK = (Button) findViewById(R.id.textview_ok_input_dialog);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finishDialog();
            }
        });
    }

    private void finishDialog() {
        if (context instanceof Activity && ((Activity) context).isFinishing())
            return;

        String inputString = mEditInput.getText().toString();
        inputString = inputString.trim();
        finishInputListener.OnFinishInputListener(inputString);
        dismiss();
    }

    public interface FinishInputListener {
        void OnFinishInputListener(String inputString);
    }
}
