package com.sbi.saidabank.common.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.BuildConfig;
import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.WebMainActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.customview.OnSingleClickListener;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.common.LoginUserInfo;
import com.sbi.saidabank.solution.ssenstone.StonePassManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * PinErrorDialog : 핀코드 오류 다이얼로그
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-03-20
 */
public class PinErrorDialog extends BaseDialog {

    private String mScrnId;
    private String mErrCode;
    private JSONObject mObjectHead;
    private LinearLayout mLayoutSendErr;
    private ImageView mLoadingImageView;
    private Button mBtChatbot;
    private Button mBtPositive;
    private Context mContext;

    public PinErrorDialog(Context context, String errCode, String scrnId, JSONObject objectHead) {
        super(context);
        this.mContext = context;
        this.mScrnId = scrnId;
        this.mErrCode = errCode;
        this.mObjectHead = objectHead;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setContentView(R.layout.dialog_pin_error);
        setDialogWidth();
        setCancelable(false);
        initView();
    }
// ==============================================================================================================
// VIEW
// ==============================================================================================================
    @SuppressLint("SetTextI18n")
    private void initView() {

        mLoadingImageView = (ImageView) findViewById(R.id.img_progress_webview);

        //릴리즈 버전에서 P002 오류 추가 정보 표시하지 않음
        if ("P002".equals(mErrCode) && BuildConfig.DEBUG) {
            TextView tvDesc = findViewById(R.id.tv_desc);
            String desc = StonePassManager.getInstance(((Activity) mContext).getApplication()).getErrorDesc();
            tvDesc.setText(DataUtil.isNotNull(desc) ? desc : "No Description");
            tvDesc.setVisibility(View.VISIBLE);
            TextView tvIP = findViewById(R.id.tv_ip);
            String ip = StonePassManager.getInstance(((Activity) mContext).getApplication()).getErrorIP();
            if (DataUtil.isNotNull(ip)) {
                tvIP.setText(ip);
                tvIP.setVisibility(View.VISIBLE);
            }
            TextView tvDate = findViewById(R.id.tv_date);
            tvDate.setText(Utils.getCurrentDate(".") + " " + Utils.getCurrentTime(":"));
            tvDate.setVisibility(View.VISIBLE);
        }

        String errMsg;
        if ("o130".equalsIgnoreCase(mErrCode.toLowerCase())) {
            errMsg = mContext.getString(R.string.msg_ssenstone_error_o130) + " (" + mErrCode + ")";
        } else if ("g301".equalsIgnoreCase(mErrCode.toLowerCase())) {
            errMsg = mContext.getString(R.string.msg_ssenstone_error_g301) + " (" + mErrCode + ")";
        } else if ("p002".equalsIgnoreCase(mErrCode.toLowerCase()) || "p004".equalsIgnoreCase(mErrCode.toLowerCase())) {
            errMsg = mContext.getString(R.string.msg_ssenstone_error_p002) + " (" + mErrCode + ")";
        } else {
            errMsg = DataUtil.isNotNull(mErrCode) ? mErrCode : "에러코드 없음";
        }
        ((TextView) findViewById(R.id.tv_errcode)).setText(errMsg);

        mBtChatbot = (Button) findViewById(R.id.btn_chatbot);
        mBtChatbot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    try {
                        if (SaidaUrl.serverIndex == Const.DEBUGING_SERVER_TEST) {
                            Logs.showToast(mContext, "테스트 서버에서는 챗봇 사용 불가입니다.");
                        } else {
                            String TRN_CD = "";
                            if (mObjectHead != null && mObjectHead.has("DATA")) {
                                JSONObject data = mObjectHead.optJSONObject("DATA");
                                if (data.has("__INFO__")) {
                                    JSONObject info;
                                    info = data.optJSONObject("__INFO__");
                                    TRN_CD = info.optString("TRN_CD");
                                }
                            }
                            StringBuilder param = new StringBuilder();
                            param.append("TRN_CD" + "=" + URLEncoder.encode(TRN_CD, "EUC-KR"));
                            param.append("&");
                            param.append("EROR_CD" + "=" + URLEncoder.encode(mErrCode, "EUC-KR"));
                            param.append("&");
                            param.append("ERROR_MESSAGE" + "=" + URLEncoder.encode("인증비밀번호 인증 중 문제가 발생했습니다."));

                            String url = SaidaUrl.ReqUrl.URL_CHATBOT.getReqUrl() + "?" + param.toString();

                            Intent intent = new Intent(mContext, WebMainActivity.class);
                            intent.putExtra("url", url);
                            mContext.startActivity(intent);
                            dismiss();
                        }
                    } catch (Exception e) {
                        MLog.e(e);
                    }
                }
            }
        });

        mBtPositive = (Button) findViewById(R.id.btn_confirm);
        mBtPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContext instanceof Activity && !((Activity) mContext).isFinishing()) {
                    dismiss();
                }
            }
        });

        mLayoutSendErr = (LinearLayout) findViewById(R.id.ll_sendimage);
        mLayoutSendErr.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                captureScreen();
            }
        });

        if (!LoginUserInfo.getInstance().isLogin()) {
            mLayoutSendErr.setVisibility(View.GONE);
            mBtChatbot.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_okbtn);
        }
    }
// ==============================================================================================================
// FUNCTION
// ==============================================================================================================
    private static byte[] captureSave(View view, View view2) {
        MLog.d();
        view.setDrawingCacheEnabled(true);
        view.buildDrawingCache();
        Bitmap viewBmp = view.getDrawingCache();
        view2.setDrawingCacheEnabled(true);
        view2.buildDrawingCache();
        Bitmap viewBmp2 = view2.getDrawingCache();
        if (viewBmp != null && viewBmp2 != null) {
            Bitmap combineBmp = ImgUtils.combineBitmap(viewBmp, viewBmp2);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            combineBmp.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
            view.setDrawingCacheEnabled(false);
            view.destroyDrawingCache();
            viewBmp.recycle();
            view2.setDrawingCacheEnabled(false);
            view2.destroyDrawingCache();
            viewBmp2.recycle();
            combineBmp.recycle();
            return byteArrayOutputStream.toByteArray();
        }
        return null;
    }

    private void showProgress() {
        if (DataUtil.isNotNull(mLayoutSendErr))
            mLayoutSendErr.setClickable(false);
        if (DataUtil.isNotNull(mBtChatbot))
            mBtChatbot.setClickable(false);
        if (DataUtil.isNotNull(mBtPositive))
            mBtPositive.setClickable(false);
        if (DataUtil.isNotNull(mLoadingImageView) && mLoadingImageView.getVisibility() != View.VISIBLE) {
            mLoadingImageView.setVisibility(View.VISIBLE);
            ((AnimationDrawable) mLoadingImageView.getBackground()).start();
        }
    }

    private void dismissProgress() {
        if (DataUtil.isNotNull(mLayoutSendErr))
            mLayoutSendErr.setClickable(true);
        if (DataUtil.isNotNull(mBtChatbot))
            mBtChatbot.setClickable(true);
        if (DataUtil.isNotNull(mBtPositive))
            mBtPositive.setClickable(true);
        if (DataUtil.isNotNull(mLoadingImageView)) {
            mLoadingImageView.setVisibility(View.GONE);
        }
    }
// ==============================================================================================================
// API
// ==============================================================================================================
    /**
     * 에러 다이얼로그가 호출되면 화면을 캡쳐하여 WAS로 전달한다.
     */
    private void captureScreen() {

        MLog.d();

        Map param = new HashMap();
        param.put("SCRN_ID", mScrnId);
        param.put("ERO_CD", mErrCode);

        // 채널 WAS와 오류났을 경우
        if (DataUtil.isNotNull(mObjectHead) && mObjectHead.has("DATA")) {
            JSONObject data = mObjectHead.optJSONObject("DATA");
            if (DataUtil.isNotNull(data) && data.has("__INFO__") && DataUtil.isNotNull(data.optJSONObject("__INFO__"))) {
                JSONObject info = data.optJSONObject("__INFO__");
                param.put("CH_GLOBAL_ID", info.optString("CH_GLOBAL_ID"));
                param.put("TRN_DT", info.optString("TRN_DT"));
                param.put("TRN_DTTM", info.optString("TRN_DTTM"));
                param.put("GLOBAL_ID", info.optString("GLOBAL_ID"));
            }
        }

        View activityView = ((Activity) mContext).getWindow().getDecorView().getRootView();
        View dialogView = getWindow().getDecorView().getRootView();
        byte[] imgData = captureSave(activityView, dialogView);
        param.put("IMG_CNTN", Base64.encodeToString(imgData, Base64.DEFAULT));

        showProgress();

        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011300A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {

                dismissProgress();

                if (DataUtil.isNull(ret)) {
                    Logs.showToast(mContext, "이미지 전송 실패하였습니다.");
                    return;
                }

                try {

                    JSONObject object = new JSONObject(ret);

                    if (DataUtil.isNull(object)) {
                        Logs.showToast(mContext, "이미지 전송 실패하였습니다.");
                        return;
                    }

                    JSONObject objectHead = object.optJSONObject(Const.REQUEST_COMMON_HEAD);
                    String error = objectHead.optString(Const.REQUEST_COMMON_ERROR);
                    if (Const.REQUEST_COMMON_TRUE.equalsIgnoreCase(error)) {
                        Logs.showToast(mContext, "이미지 전송 실패하였습니다.");
                        return;
                    }

                    Logs.showToast(mContext, "이미지 전송 완료하였습니다.");

                } catch (JSONException e) {
                    MLog.e(e);
                }
            }
        });
    }
}
