package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.customview.OnSingleClickListener;


public class TransferEditAliasDialog extends BaseDialog{
    private Context mContext;

    private EditText mEditAlias;
    private OnBtnClickListener mBtnClickListener;

    public TransferEditAliasDialog(Context context,OnBtnClickListener listener) {
        super(context);
        mContext = context;
        mBtnClickListener = listener;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_edit_transfer_account);

        setDialogWidth();

        setCancelable(false);

        mEditAlias = (EditText) findViewById(R.id.edittext_edit_transfer_account);

        findViewById(R.id.btn_negative).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.btn_positive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alias = mEditAlias.getText().toString();
                if (DataUtil.isNull(alias)) {
                    Toast.makeText(getContext(), getContext().getString(R.string.msg_no_alias_input), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(mBtnClickListener != null) {
                    mBtnClickListener.onBtnClick(v,alias);
                }
                dismiss();
            }
        });

        //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
    }

    public interface OnBtnClickListener{
        void onBtnClick(View v,String alias);
    }
}
