package com.sbi.saidabank.common.contacts;

import android.app.Activity;
import android.content.Context;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.CoupleContactInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Saidabanking_android
 *
 * Class: PincodeRegActivity
 * Created by 950485 on 2018. 12. 5..
 * <p>
 * Description: 주소록 정보 리스트 처리
 */

public class CoupleContactsInfoAdapter extends ArrayAdapter {

	private Context context;

	private ArrayList<CoupleContactInfo> listContact = null;
	private ArrayList<CoupleContactInfo> listFilter = null;

	//private TextView textResultSearchCount = null;

	private ContactsNameFilter filterName = null;
	private ContactsPhoneFilter filterPhone = null;

	private OnFilterPublishListener mFilterPublishListener;

	public CoupleContactsInfoAdapter(Context context,OnFilterPublishListener listener) {
		super(context, R.layout.activity_contacts_pick_list_item);

		this.listContact = new ArrayList<>();

		this.listFilter = new ArrayList<>();
		this.listFilter.addAll(this.listContact);

		this.context = context;
		mFilterPublishListener = listener;
	}

	@Override
	public int getCount() {
		if (listFilter == null) {
			return 0;
		}
		
		return listFilter.size();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View viewRow = convertView;
		ContactsHolder holder;
		final CoupleContactInfo contactsInfo = listFilter.get(position);

		String name = contactsInfo.getFLNM();
		String phone = contactsInfo.getTLNO();
		String OWBK_CUST_YN = contactsInfo.getOWBK_CUST_YN();

		if (viewRow == null) {
			LayoutInflater aInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			viewRow = aInflater.inflate(R.layout.activity_contacts_pick_list_item, parent, false);

			holder = new ContactsHolder();
			holder.textName = (TextView) viewRow.findViewById(R.id.textview_name_contacts_pick);
			holder.textPhone = (TextView) viewRow.findViewById(R.id.textview_phone_contacts_pick);
			holder.imageBank = (ImageView) viewRow.findViewById(R.id.imageview_contacts_bank);
			viewRow.setTag(holder);
		} else {
			holder = (ContactsHolder) viewRow.getTag();
		}
		holder.textName.setText(name);

		phone = PhoneNumberUtils.formatNumber(phone, Locale.getDefault().getCountry());
		holder.textPhone.setText(phone);

		name = holder.textName.getText() + ". " + holder.textPhone.getText();
		viewRow.setContentDescription(name);

		if (!TextUtils.isEmpty(OWBK_CUST_YN) && Const.REQUEST_WAS_YES.equalsIgnoreCase(OWBK_CUST_YN))
			holder.imageBank.setVisibility(View.VISIBLE);
		else
			holder.imageBank.setVisibility(View.INVISIBLE);


		return viewRow;
	}

	public Filter getNameFilter() {
 	   if (filterName == null) {
		   filterName = new ContactsNameFilter();
 	   }
 	   return filterName;
    }

	public Filter getPhoneFilter() {
		if (filterPhone== null) {
			filterPhone = new ContactsPhoneFilter();
		}
		return filterPhone;
	}
	
	public void release() {
		filterName = null;

		if (listFilter != null) {
			listFilter.clear();
			listFilter = null;
		}

		if (listContact != null) {
			listContact.clear();
			listContact = null;
		}
	}

	public ArrayList<CoupleContactInfo> getAllItems() {
		return listContact;
	}

	private class ContactsNameFilter extends Filter {
		protected FilterResults performFiltering(CharSequence s) {
		    String constraint = s.toString().trim();
		    FilterResults result = new FilterResults();

		    if (constraint != null && constraint.length() > 0) {
		    	ArrayList<CoupleContactInfo> filteredItems = new ArrayList<>();
		    	
		    	for (int i = 0, l = listContact.size(); i < l; i++) {
					CoupleContactInfo fc = (CoupleContactInfo) listContact.get(i);
		    		String name = fc.getFLNM();
		    		boolean ini = ContactsUtil.isMatch(name, constraint);
		    		if (ini || name.contains(constraint) || name.indexOf(constraint) != -1) {
		    			filteredItems.add(fc);
		    		}
		    	}
		    	result.count = filteredItems.size();
		    	result.values = filteredItems;
		    } else {
		    	synchronized(this) {
		    		result.count = listContact.size();
		    		result.values = listContact;
		    	}
		    }
		   	return result;
		}

		@Override
		protected void publishResults(CharSequence constraint,  FilterResults results) {
			List<CoupleContactInfo> listResult = (ArrayList<CoupleContactInfo>) results.values;
			notifyDataSetChanged();

			listFilter.clear();
			listFilter.addAll(listResult);

			notifyDataSetInvalidated();

			Logs.e("ContactsNameFilter - publishResults - end");

			if(mFilterPublishListener != null){
				mFilterPublishListener.onFilterPublishEnd();
			}

//			if (textResultSearchCount != null) {
//				if (listFilter.size() > 0)
//					textResultSearchCount.setText(context.getString(R.string.contacts_result_search_count, String.valueOf(listFilter.size())));
//				else
//					textResultSearchCount.setText(context.getString(R.string.contacts_result_no_search_count));
//			}
		}
	}

	private class ContactsPhoneFilter extends Filter {
		protected FilterResults performFiltering(CharSequence s) {
			String constraint = s.toString().trim();
			FilterResults result = new FilterResults();

			if (constraint != null && constraint.length() > 0) {
				ArrayList<CoupleContactInfo> filteredItems = new ArrayList<>();

				for (int i = 0, l = listContact.size(); i < l; i++) {
					CoupleContactInfo fc = (CoupleContactInfo) listContact.get(i);
					String number = fc.getTLNO();
					if (TextUtils.isEmpty(number))
						continue;

					if (number.contains(constraint) || number.indexOf(constraint) != -1) {
						filteredItems.add(fc);
					}
				}
				result.count = filteredItems.size();
				result.values = filteredItems;
			} else {
				synchronized(this) {
					result.count = listContact.size();
					result.values = listContact;
				}
			}
			return result;
		}

		@Override
		protected void publishResults(CharSequence constraint,  FilterResults results) {
			List<CoupleContactInfo> listResult = (ArrayList<CoupleContactInfo>) results.values;
			notifyDataSetChanged();

			listFilter.clear();
			listFilter.addAll(listResult);

			notifyDataSetInvalidated();

			Logs.e("ContactsPhoneFilter - publishResults - end");

			if(mFilterPublishListener != null){
				mFilterPublishListener.onFilterPublishEnd();
			}

//			if (textResultSearchCount != null) {
//				if (listFilter.size() > 0)
//					textResultSearchCount.setText(context.getString(R.string.contacts_result_search_count, String.valueOf(listFilter.size())));
//				else
//					textResultSearchCount.setText(context.getString(R.string.contacts_result_no_search_count));
//			}
		}
	}

	public ArrayList<CoupleContactInfo> getFilterList() { return listFilter; }

	public void setArrayList(ArrayList<CoupleContactInfo> array) {
		this.listContact.clear();
		this.listContact.addAll(array);
		this.listFilter.clear();
		this.listFilter.addAll(array);

		notifyDataSetChanged();
	}

	private static class ContactsHolder {
		TextView  textName;
		TextView  textPhone;
		ImageView imageBank;
	}

	public interface OnFilterPublishListener{
		void onFilterPublishEnd();
	}
}
