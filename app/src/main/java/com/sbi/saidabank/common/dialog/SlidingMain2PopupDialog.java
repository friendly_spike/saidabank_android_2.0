package com.sbi.saidabank.common.dialog;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.Display;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.activity.BaseActivity;
import com.sbi.saidabank.common.net.HttpSenderTask;
import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.SaidaUrl;
import com.sbi.saidabank.define.WasServiceUrl;
import com.sbi.saidabank.define.datatype.main.MainPopupInfo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * SlidingConfirmSafeDealTransferDialog : 메인2 팝업 다이얼로그
 */
public class SlidingMain2PopupDialog extends SlidingBaseDialog {

    //이미지 최적화 사이즈 정의
    private static final int IMG_X_SIZE = 360;
    private static final int IMG_Y_SIZE = 261;

    //이미지 상단 둥글게..
    private static final int LAYOUT_RADIUS_DIMEN = 24;

    private Context mContext;
    private ArrayList<MainPopupInfo> mListPopupInfo;
    private RelativeLayout[] mLayoutArray;
    private int mCurShowIdx;
    private int mDlgHeight;
    private int mImgLayoutHeight;
    private OnPopupListener mListener;

    public interface OnPopupListener {
        void onImageClick(String url);
        void onShowComplete(boolean ret);
    }

    public SlidingMain2PopupDialog(@NonNull Context context, ArrayList<MainPopupInfo> arrayList, OnPopupListener listener) {
        super(context);
        this.mContext = context;
        this.mListener = listener;

        if (arrayList.isEmpty())
            return;

        // 팝업은 최대 2개초과 출력할수 없다.
        int size = Math.min(arrayList.size(), 2);
        mListPopupInfo = new ArrayList<>();
        for (int i=0; i < size; i++) {
            mListPopupInfo.add(arrayList.get(i));
        }

        if (DataUtil.isNotNull(mListPopupInfo) && !mListPopupInfo.isEmpty()) {
            // 데이타가 제대로 되어 있는지 체크한다.
            for (int i = mListPopupInfo.size() - 1; i >= 0; i--) {
                MainPopupInfo popupInfo = mListPopupInfo.get(i);
                if ("I".equalsIgnoreCase(popupInfo.getPUP_CNTN_DVCD())) {
                    if (DataUtil.isNull(popupInfo.getIMG_URL_ADDR())) {
                        mListPopupInfo.remove(i);
                    }
                    loadBitmap2(i, SaidaUrl.getBaseWebUrl() + popupInfo.getIMG_URL_ADDR());
                } else {
                    if (DataUtil.isNull(popupInfo.getPUP_CNTN())) {
                        mListPopupInfo.remove(i);
                    }
                    mListPopupInfo.get(i).setLOAD_COMPLETE(true);
                    checkLoadComplete();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_main2_popup_dialog);
        setDialogWidth();
        setCanceledOnTouchOutside(false);
        initLayout();
    }

    @Override
    protected void onStop() {
        MLog.d();
        super.onStop();
        eraseBitmap();
    }

    @Override
    protected void setDialogWidth() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = 0;
        float xDip = Utils.pixelToDp(getContext(), width);

        if (xDip > Const.MAX_WIDTH_DIP) {
            xDip = Const.MAX_WIDTH_DIP;
        }

        // ImageView의 Width에대한 변경된 Height를 구한다.
        float yDip = xDip * IMG_Y_SIZE / IMG_X_SIZE;
        height = (int) Utils.dpToPixel(getContext(), yDip);
        mImgLayoutHeight = height;

        int offset = (int) Utils.dpToPixel(getContext(), 60);//(int)((mListPopupInfo.size()-1) * Utils.dpToPixel(getContext(),60));
        mDlgHeight = height + offset;

        // 다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        lp.width = width;
        lp.height = height + offset;
        getWindow().setAttributes(lp);
    }

    /**
     * 화면 레이아웃을 구성한다.
     */
    private void initLayout() {

        // 위에서 데이타 오류로 삭제되었을 경우 사이즈 체크하여 리턴한다.
        if (DataUtil.isNull(mListPopupInfo) || mListPopupInfo.isEmpty())
            return;

        mLayoutArray = new RelativeLayout[mListPopupInfo.size()];

        if (DataUtil.isNotNull(mListPopupInfo)) {
            for (int i = 0; i < mListPopupInfo.size(); i++) {
                String layoutName = "@id/layout_main_popup_" + i;
                String imageViewName = "@id/iv_main_popup_" + i;
                String textViewName = "@id/tv_main_popup_" + i;

                // 결론적으로 이미지 리소스 이름은 img_1, img_2, img_3 이 되겠다;
                int layoutResID = getContext().getResources().getIdentifier(layoutName, "id", getContext().getPackageName());
                int ivResResID = getContext().getResources().getIdentifier(imageViewName, "id", getContext().getPackageName());
                int tvResID = getContext().getResources().getIdentifier(textViewName, "id", getContext().getPackageName());

                final RelativeLayout layout = (RelativeLayout) findViewById(layoutResID);
                mLayoutArray[i] = layout;
                final ImageView imageView = (ImageView) layout.findViewById(ivResResID);
                imageView.setTag(i);
                final TextView textView = (TextView) layout.findViewById(tvResID);

                // 이미지 눌렀을때 이동하도록.
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int pos = (int) v.getTag();
                        if (!TextUtils.isEmpty(mListPopupInfo.get(pos).getLINK_URL_ADDR())) {
                            mListener.onImageClick(mListPopupInfo.get(pos).getLINK_URL_ADDR());
                            dismiss();
                        }
                    }
                });

                RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mLayoutArray[i].getLayoutParams();
                lp.height = mImgLayoutHeight;
                mLayoutArray[i].setLayoutParams(lp);
                int sCorner = (int) Utils.dpToPixel(getContext(), LAYOUT_RADIUS_DIMEN);
                mLayoutArray[i].setBackground(GraphicUtils.getRoundCornerDrawable("#00FFFFFF", new int[]{sCorner, sCorner, 0, 0}));

                MainPopupInfo popupInfo = mListPopupInfo.get(i);
                if ("I".equalsIgnoreCase(popupInfo.getPUP_CNTN_DVCD())) {
                    textView.setVisibility(View.GONE);
                    imageView.setImageBitmap(popupInfo.getLOAD_BITMAP());
                    imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                } else {
                    imageView.setVisibility(View.GONE);
                    textView.setText(popupInfo.getPUP_CNTN());
                }

                if (i > 0) {
                    scaleSmall(mLayoutArray[i], 0);
                }
            }
        }

        TextView btnNoShow = (TextView) findViewById(R.id.tv_never_show);
        btnNoShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestNoShowPopup(mListPopupInfo.get(mCurShowIdx));
                removePopup();
            }
        });

        LinearLayout layoutClose = (LinearLayout) findViewById(R.id.layout_close);
        layoutClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removePopup();
            }
        });
    }

    private void removePopup() {
        if (mCurShowIdx == mListPopupInfo.size() - 1) {
            dismiss();
        } else {
            slidingDown(250);
            mCurShowIdx++;
            scaleOrigin(250, 100);
        }
    }

    private void loadBitmap2(final int index, final String strUrl) {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            if (DataUtil.isNull(mListPopupInfo) || mListPopupInfo.isEmpty() || mListPopupInfo.size() <= index)
                                return;

                            MLog.line();
                            MLog.i("index >> " + mListPopupInfo.size() + " | " + index + " || strUrl >> " + strUrl);
                            MLog.line();

                            URL url = new URL(strUrl);
                            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                            connection.setDoInput(true);
                            connection.connect();
                            InputStream input = connection.getInputStream();
                            Bitmap myBitmap = BitmapFactory.decodeStream(input);

                            if (DataUtil.isNotNull(myBitmap) && !myBitmap.isRecycled()) {

                                float sCorner = Utils.dpToPixel(getContext(), LAYOUT_RADIUS_DIMEN);
                                Bitmap roundCornerBmp = ImgUtils.getRoundedCornerBitmap(myBitmap, sCorner, sCorner, 0, 0);

                                if (DataUtil.isNotNull(roundCornerBmp)) {
                                    mListPopupInfo.get(index).setLOAD_BITMAP(roundCornerBmp);
                                    mListPopupInfo.get(index).setLOAD_COMPLETE(true);
                                    checkLoadComplete();
                                    myBitmap.recycle();
                                } else {
                                    mListPopupInfo.get(index).setLOAD_BITMAP(myBitmap);
                                    mListPopupInfo.get(index).setLOAD_COMPLETE(true);
                                    checkLoadComplete();
                                }
                                return;
                            }

                            mListPopupInfo.get(index).setLOAD_BITMAP(null);
                            mListPopupInfo.get(index).setLOAD_COMPLETE(true);
                            checkLoadComplete();

                        } catch (IOException e) {
                            MLog.e(e);
                        } catch (IndexOutOfBoundsException e) {
                            MLog.e(e);
                        } catch (RuntimeException e) {
                            MLog.e(e);
                        }
                    }
                }
        ).start();
    }

    private void checkLoadComplete() {
        if (DataUtil.isNotNull(mListPopupInfo) && !mListPopupInfo.isEmpty()) {
            for (int i = 0; i < mListPopupInfo.size(); i++) {
                if (!mListPopupInfo.get(i).isLOAD_COMPLETE()) {
                    return;
                }
            }
            for (int i = 0; i < mListPopupInfo.size(); i++) {
                if (mListPopupInfo.get(i).getLOAD_BITMAP() == null) {
                    mListPopupInfo.remove(i);
                }
            }
            ((BaseActivity) mContext).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (DataUtil.isNotNull(mContext) && !((BaseActivity) mContext).isFinishing()) {
                                show();
                                mListener.onShowComplete(true);
                            } else {
                                eraseBitmap();
                                mListener.onShowComplete(false);
                            }
                        }
                    }, 200);
                }
            });
        }
    }

    private void eraseBitmap() {
        if (DataUtil.isNotNull(mListPopupInfo) && !mListPopupInfo.isEmpty()) {
            MLog.d();
            for (int i = mListPopupInfo.size() - 1; i >= 0; i--) {
                if (DataUtil.isNotNull(mListPopupInfo.get(i).getLOAD_BITMAP())) {
                    mListPopupInfo.get(i).getLOAD_BITMAP().recycle();
                    mListPopupInfo.get(i).setLOAD_BITMAP(null);
                }
            }
        }
    }

    /**
     * 뷰를 바닥으로 내림
     */
    public void slidingDown(final int aniTime) {
        mLayoutArray[mCurShowIdx].animate()
                .translationY(mDlgHeight)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                    }
                });
    }

    /**
     * 뷰를 작은 사이즈로 스케일함
     *
     * @param aniTime 밀리세컨드 초
     */
    public void scaleSmall(final RelativeLayout layout, final int aniTime) {
        float offset = (mImgLayoutHeight * 0.1f) / 2;
        layout.animate()
                .scaleX(0.9f)
                .scaleY(0.9f)
                .alpha(0.3f)
                .translationY(offset * -1)
                .setDuration(aniTime)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                    }
                })
                .start();
    }

    /**
     * 뷰를 원래 사이즈로 스케일함
     *
     * @param aniTime   밀리세컨드 초
     * @param delayTime 밀리세컨드 초
     */
    public void scaleOrigin(final int aniTime, int delayTime) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewPropertyAnimator animator = mLayoutArray[mCurShowIdx].animate();
                //animator.translationY(mDlgHeight - mImgLayoutHeight);
                animator.translationY((int) Utils.dpToPixel(getContext(), 12f));
                animator.scaleX(1f)
                        .scaleY(1f)
                        .alpha(1f)
                        .setDuration(aniTime)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationStart(Animator animation) {
                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                            }
                        })
                        .start();
            }
        }, delayTime);
    }

    private void requestNoShowPopup(MainPopupInfo mainPopupInfo) {
        MLog.d();
        if (DataUtil.isNull(mainPopupInfo) || DataUtil.isNull(mainPopupInfo.getPUP_SRNO()))
            return;
        Map param = new HashMap();
        param.put("PUP_SRNO", mainPopupInfo.getPUP_SRNO());
        HttpUtils.sendHttpTask(WasServiceUrl.CMM0011900A01.getServiceUrl(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                MLog.i("requestNoShowPopup >> " + ret);
            }
        });
    }
}
