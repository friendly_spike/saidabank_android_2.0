package com.sbi.saidabank.common.util;

import android.Manifest;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.sbi.saidabank.activity.SaidaApplication;
import com.sbi.saidabank.define.Const;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.TELEPHONY_SERVICE;
import static com.kakao.util.helper.Utility.getPackageInfo;

public class Utils {

    /**
     * 앱 버전명을 가져온다.
     *
     * @param context
     * @return
     */
    public static String getVersionName(Context context) {
        try {
            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return i.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            return "";
        }
    }

    /**
     * 앱 버전 코드를 가져온다.
     *
     * @param context
     * @return
     */
    public static int getVersionCode(Context context) {
        int code = 0;
        try {

            PackageInfo i = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);

            code = i.versionCode;

            Logs.e("version code : " + code);

        } catch (PackageManager.NameNotFoundException e) {
        }

        return code;
    }

    /**
     * 해당 기기의 통신사를 반환한다
     *
     * @param context
     * @return String
     */
    public static String getOperator(Context context) {
        TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        String operator = tm.getSimOperatorName();
        if (operator == null || operator.length() <= 0)
            operator = tm.getNetworkOperatorName();
        return operator;
    }

    /**
     * 전화를 건다.
     *
     * @param activity
     * @param telNo    전화번호 문자열
     */
    public static void makeCall(Activity activity, String telNo) {

        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ((SaidaApplication) activity.getApplicationContext()).setTempTelNo(telNo);
            String[] perList = new String[]{Manifest.permission.CALL_PHONE};
            ActivityCompat.requestPermissions(activity, perList, Const.REQUEST_PERMISSION_CALL);
        } else {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + telNo));
            activity.startActivity(intent);
        }

    }

    /**
     * 오늘 날짜를 가져온다.
     *
     * @param gubun 년월일의 구분문자열
     * @return
     */
    public static String getCurrentDate(String gubun) {
        Calendar mCalendar = new GregorianCalendar();
        String currentTime;

        Integer year = mCalendar.get(Calendar.YEAR);
        Integer month = mCalendar.get(Calendar.MONTH) + 1;//월은 0~11까지 그래서 1을 더해줘야함.
        Integer dayOfMonth = mCalendar.get(Calendar.DAY_OF_MONTH);

        String strMonth = String.format("%02d", month);
        String strDay = String.format("%02d", dayOfMonth);

        if (!TextUtils.isEmpty(gubun))
            return year + gubun + strMonth + gubun + strDay;
        else
            return year + strMonth + strDay;
    }

    /**
     * 이전달, 다음달 년월을 가져온다.
     *
     * @param add_month
     * @return
     */
    public static String getAddMonth(int add_month) {
        Calendar mCalendar = new GregorianCalendar();

        mCalendar.add(Calendar.MONTH,add_month);


        Integer year = mCalendar.get(Calendar.YEAR);
        Integer month = mCalendar.get(Calendar.MONTH) + 1;//월은 0~11까지 그래서 1을 더해줘야함.
        Integer dayOfMonth = mCalendar.get(Calendar.DAY_OF_MONTH);

        String strMonth = String.format("%02d", month);
        String strDay = String.format("%02d", dayOfMonth);

        return year + strMonth;
    }

    /**
     * 현재 시간을 가져온다.
     *
     * @param gubun 년월일의 구분문자열
     * @return
     */
    public static String getCurrentTime(String gubun) {
        Calendar mCalendar = new GregorianCalendar();
        String currentTime;

        Integer hour = mCalendar.get(Calendar.HOUR_OF_DAY);
        Integer min = mCalendar.get(Calendar.MINUTE);
        Integer sec = mCalendar.get(Calendar.SECOND);

        String strHour = String.format("%02d", hour);
        String strMin = String.format("%02d", min);
        String strSec = String.format("%02d", sec);

        if (!TextUtils.isEmpty(gubun))
            return strHour + gubun + strMin + gubun + strSec;
        else
            return strHour + strMin + strSec;
    }


    /**
     * Convert byte -> hex
     *
     * @param data byte array
     * @return hex String
     */
    public static String bytesToHex(byte[] data) {
        if (data == null || data.length == 0)
            return null;

        StringBuilder sb = new StringBuilder();
        for (final byte b : data)
            sb.append(String.format("%02x ", b & 0xff));
        return sb.toString();
    }

    /**
     * Convert hex -> byte
     *
     * @param str Hex String
     * @return byte arry
     */
    public static byte[] hexToBytes(String str) {

        if (str == null || str.length() < 2) {
            return null;
        }

        byte[] buffer = new byte[str.length() / 2];
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = (byte) Integer.parseInt(str.substring(i * 2, i * 2 + 2), 16);
        }

        return buffer;
    }

    /**
     * 앱의 캐시를 지운다. 모든 캐쉬를 지우면 문제가 되니 필요한것만 지음.
     *
     * @param context
     */
    public static void clearAppCash(Context context) {
        File cache = context.getCacheDir();
        File cachedir = new File(cache.getParent());

        if (cachedir.exists()) {
            //더 자세히 작성하도록.
            String[] dirs = cachedir.list();
            for (String s : dirs) {
                if (s.equals("cache") || s.equals("app_appcache")) {
                    Files.deleteDir(new File(cachedir, s));
                }
            }
        }
    }

    /**
     * 앱의 모든 캐시를 지운다.Prefer,Database등등 모두 지워버림.
     *
     * @param context
     */
    public static void clearAllCash(Context context) {
        File cache = context.getCacheDir();
        File cachedir = new File(cache.getParent());

        if (cachedir.exists()) {
            //더 자세히 작성하도록.
            String[] dirs = cachedir.list();
            for (String s : dirs) {
                Files.deleteDir(new File(cachedir, s));
            }
        }
    }

    public static float dpToPixel(Context c, float dp) {
        if(dp == 0) return 0;

        float density = c.getResources().getDisplayMetrics().density;
        float pixel = dp * density;
        return pixel;
    }

    public static float pixelToDp(Context context, float px) {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static String strConcat(String... arg) {
        StringBuilder sb = new StringBuilder();
        for (String str : arg) {
            sb.append(str);
        }
        return sb.toString();
    }

    public static boolean isInstalledV3MobilePlus(Context ctx) {
        boolean result = true;
        PackageManager packMgr = ctx.getPackageManager();
        try {
            packMgr.getPackageInfo("com.ahnlab.v3mobileplus", PackageManager.GET_CONFIGURATIONS);
        } catch (PackageManager.NameNotFoundException e) {
            result = false;
        }
        return result;
    }

    public static boolean isValidatePwPattern(String target, String userId, int minLen, int maxLen) {
        String pwPattern = "^(?=.*\\d)(?=.*[~`!@#$%\\^&*()-])(?=.*[a-z])(?=.*[A-Z]).{";
        pwPattern += minLen;
        pwPattern += ",";
        pwPattern += maxLen;
        pwPattern += "}$";

        // 정규식 (영문(대소문자 구분), 숫자, 특수문자 조합, 9~12자리)
        Matcher matcher = Pattern.compile(pwPattern).matcher(target);
        if (!matcher.matches()) {
            return false;
        }

        // 정규식 (같은 문자 4개 이상 사용 불가)
        String pwRepeat = "(.)\\1\\1\\1";
        Matcher matcher2 = Pattern.compile(pwRepeat).matcher(target);
        if (matcher2.find())
            return false;

        // 아이디 포함 여부
        if (target.contains(userId))
            return false;

        // 공백문자 포함 여부
        if (target.contains(" "))
            return false;

        return true;
    }

    /*
     * 이체금액 숫자로 입력시 자동으로 한글로 변경
     * */
    /*
    public static String convertHangul(String moneyStr) {
        String[] han = {"", "만", "억", "조", "경"};

        if (moneyStr.contains(",")) {
            moneyStr = moneyStr.replace(",", "");
        }

        StringBuffer result = new StringBuffer();

        int len = moneyStr.length();
        for (int i = len - 1; i >= 0; i--) {
            result.append(moneyStr.substring(len - i - 1, len - i));
            if (Integer.parseInt(moneyStr.substring(len - i - 1, len - i)) > 0) {
                if (i % 4 == 3)
                    result.append(",");
            }

            if (i % 4 == 0) {
                result.append(han[i / 4]);
            }
        }
        return result.toString();
    }
    */

    /**
     * 이체금액 숫자로 입력시 자동으로 한글로 변경 : 1000억단위까지 가능
     *
     * @param moneyStr 금액 문자열
     * @return 한글 금액 문자열
     */
    public static String convertHangul(String moneyStr) {
        StringBuffer result = new StringBuffer();

        if (moneyStr.contains(",")) {
            moneyStr = moneyStr.replace(",", "");
        }

        int len = moneyStr.length();
        if (len < 5) {
            if (moneyStr.length() == 4) {
                moneyStr = moneyStr.substring(0, 1) + "," + moneyStr.substring(1, 4);
            }
            result.append(moneyStr);
        } else if (len < 9) {
            String partialStr = moneyStr.substring(0, len - 4);
            if (partialStr.length() == 4) {
                partialStr = partialStr.substring(0, 1) + "," + partialStr.substring(1, 4);
            }
            result.append(partialStr + "만");

            partialStr = moneyStr.substring(len - 4, len);
            int moneyInt = Integer.parseInt(partialStr);
            if (moneyInt != 0) {
                partialStr = String.valueOf(moneyInt);
                if (partialStr.length() == 4) {
                    partialStr = partialStr.substring(0, 1) + "," + partialStr.substring(1, 4);
                }
                result.append(partialStr);
            }
        } else {
            String partialStr = moneyStr.substring(0, len - 8);
            if (partialStr.length() == 4) {
                partialStr = partialStr.substring(0, 1) + "," + partialStr.substring(1, 4);
            }
            result.append(partialStr + "억");

            partialStr = moneyStr.substring(len - 8, len - 4);
            int moneyInt = Integer.parseInt(partialStr);
            if (moneyInt != 0) {
                partialStr = String.valueOf(moneyInt);
                if (partialStr.length() == 4) {
                    partialStr = partialStr.substring(0, 1) + "," + partialStr.substring(1, 4);
                }
                result.append(partialStr + "만");
            }

            partialStr = moneyStr.substring(len - 4, len);
            moneyInt = Integer.parseInt(partialStr);
            if (moneyInt != 0) {
                partialStr = String.valueOf(moneyInt);
                if (partialStr.length() == 4) {
                    partialStr = partialStr.substring(0, 1) + "," + partialStr.substring(1, 4);
                }
                result.append(partialStr);
            }
        }

        return result.toString();
    }

    /*
     * 에디트 텍스트에서 키보드 보이기
     */
    public static void showKeyboard(Context context, View target) {
        if (context == null || target == null) {
            return;
        }

        InputMethodManager imm = getInputMethodManager(context);
        imm.showSoftInput(target, InputMethodManager.SHOW_IMPLICIT);
    }

    /*
     * 에디트 텍스트에서 키보드 감추기
     */
    public static void hideKeyboard(Context context, View target) {
        if (context == null || target == null) {
            return;
        }

        InputMethodManager imm = getInputMethodManager(context);
        imm.hideSoftInputFromWindow(target.getWindowToken(), 0);
    }

    private static InputMethodManager getInputMethodManager(Context context) {
        return (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public static void finishAffinity(final Activity activity) {
        activity.setResult(Activity.RESULT_CANCELED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.finishAffinity();
        }
    }

    /**
     * 현재 기기의 전화번호 가져오기
     *
     * @param context
     * @return String
     */
    @SuppressLint("HardwareIds")
    public static String getPhoneNumber(Context context) {
        TelephonyManager mTelephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
        if ((ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)
                && DataUtil.isNotNull(mTelephonyManager)
                && DataUtil.isNotNull(mTelephonyManager.getLine1Number())
                && mTelephonyManager.getLine1Number().length() >= 10) {
            return mTelephonyManager.getLine1Number().replace("+82", "0").replace("-", "");

        }
        return Const.EMPTY;
    }

    /**
     * 현재 기기의 Sim카드 존재 여부
     *
     * @param context
     */
    @SuppressLint("MissingPermission")
    public static boolean isExistSimCard(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            SubscriptionManager sManager = (SubscriptionManager) context.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
            if (sManager == null)
                return false;

            List<SubscriptionInfo> clsList = sManager.getActiveSubscriptionInfoList();

            if(clsList == null || clsList.isEmpty()) return false;

            Logs.e("isExistSimCard : clsList : " + clsList.toString());

            return true;

//            SubscriptionInfo infoSim1 = sManager.getActiveSubscriptionInfoForSimSlotIndex(0);
//            SubscriptionInfo infoSim2 = sManager.getActiveSubscriptionInfoForSimSlotIndex(1);
//            if(infoSim1 != null || infoSim2 != null)
//                return true;

        }
        return false;
    }

    /**
     * 현재 기기의 USB 디버깅 모드 활성화 상태 가져오기
     *
     * @param context
     */
    public static boolean isUsbDebuggingEnable(Context context) {
        return Settings.Global.getInt(context.getContentResolver(), Settings.Global.ADB_ENABLED, 0) != 0;
    }

    /**
     * 현재 기기의 개발자옵션 메뉴 활성화 상태 가져오기
     *
     * @param context
     */
    public static boolean isDevelopmentSettingsEnable(Context context) {
        return Settings.Secure.getInt(context.getContentResolver(), Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0) != 0;
    }

    /**
     * 이름 유효성 체크(키보드 입력 후 완성형 한글만)
     *
     * @param strname 이름
     * @return STATE_VALID_STR_NORMAL : 정상, STATE_VALID_STR_LENGTH_ERR : 길이 오류, STATE_VALID_STR_CHAR_ERR : 한글아님 오류
     */
    public static int isKorean(String strname) {
        Logs.i("isKorean");
        if (strname.length() > 0) {
            if (strname.matches("^[가-힣]*$")) {  // 완성형 한글 입력 ok
                if (strname.length() >= 2 && strname.length() < 15) {
                    return Const.STATE_VALID_STR_NORMAL;
                } else {
                    return Const.STATE_VALID_STR_LENGTH_ERR;
                }
            } else {
                return Const.STATE_VALID_STR_CHAR_ERR;
            }
        } else {
            return Const.STATE_VALID_STR_NORMAL;
        }
    }

    /**
     * double형 원화 표시
     *
     * @param inputMoney 금액
     * @return
     */
    public static String moneyFormatToWon(Double inputMoney) {
        if(inputMoney == null)
            inputMoney = 0d;

        DecimalFormat decimalFormat = new DecimalFormat("#,###");
        return decimalFormat.format(inputMoney);
    }

    /**
     * 특정 글자를 스타일 변경하여 출력
     *
     * @param textView 텍스트 뷰
     * @param text     전체 문자
     * @param spanText 변경할 글자
     *
     */
    public static void setTextWithSpan(TextView textView, String text, String spanText,int typeface,String colorStr) {
        SpannableStringBuilder span = new SpannableStringBuilder(text);
        if(!TextUtils.isEmpty(spanText)){
            int start = text.indexOf(spanText);
            int end = start + spanText.length();

            if(start < 0){
                textView.setText(text);
                return;
            }

            span.setSpan(new StyleSpan(typeface), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

            if(!TextUtils.isEmpty(colorStr)){
                int color = Color.parseColor(colorStr);
                span.setSpan(new ForegroundColorSpan(color), start, end, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            textView.setText(span);
        }else{
            textView.setText(text);
        }
    }

    /**
     * 앱 사인키 해쉬값 가져오기
     *
     * @param context
     * @return
     */
    public static String getKeyHash(final Context context) {
        PackageInfo packageInfo = getPackageInfo(context, PackageManager.GET_SIGNATURES);
        if (packageInfo == null)
            return null;

        for (Signature signature : packageInfo.signatures) {
            try {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                return Base64.encodeToString(md.digest(), Base64.NO_WRAP);
            } catch (NoSuchAlgorithmException e) {
                Logs.printException(e);
            }
        }
        return null;
    }

    /**
     * 문장 내, 같은 글자 반복 확인
     *
     * @param target    전체문장
     * @param duplicate 반복 횟수
     * @return
     */
    public static boolean isDuplicate(String target, int duplicate) {
        for (int index = 0; index < target.length(); index++) {
            char toCheck = target.charAt(index);
            int countCheck = 0;
            boolean isStart = false;
            for (char ch : target.toCharArray()) {
                if (ch == toCheck) {
                    if (!isStart) isStart = true;
                    if (isStart) countCheck++;
                } else {
                    isStart = false;
                    countCheck = 0;
                }

                if (countCheck == duplicate)
                    return true;
            }
        }
        return false;
    }

    /**
     * 문장 내, 세숫자가 연속적인 숫자인지 확인
     *
     * @param target 전체문장
     * @param limit  체크할 연속 숫자 길이
     * @return limit길이만큼 연속된 숫자가 있으면 true
     */
    public static boolean isContinueNum(String target, int limit) {
        int o = 0;
        int d = 0;
        int p = 0;
        int n = 0;

        // 체크할 문자길이가 최소 3자 이상이어야 함
        if (limit < 3)
            return false;

        for (int i = 0; i < target.length(); i++) {
            char c = target.charAt(i);
            p = o - c;
            if (i > 0 && (p == -1 || p == 1) && (n = p == d ? n + 1 : 0) > limit - 3)
                return true;
            d = p;
            o = c;
        }

        return false;
    }

    public static boolean isInstallApp(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        boolean appInstalled = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            appInstalled = true;
        } catch (PackageManager.NameNotFoundException e) {
            appInstalled = false;
        }
        return appInstalled;
    }

    /**
     * 앱 백그라운드 상태 체크
     *
     * @param context
     * @return false면 백그라운드
     */
    public static boolean isAppOnForeground(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> appProcesses = activityManager.getRunningAppProcesses();
        if (DataUtil.isNull(appProcesses)) {
            return false;
        }
        final String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo appProcess : appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName.equals(packageName)) {
                return true;
            }
        }
        return false;
    }

    /**
     * 알림 허용 상태 값 가져오기
     */
    public static boolean isNotificationsEnabled(Context context) {
        return NotificationManagerCompat.from(context).areNotificationsEnabled();
    }

    /**
     * 알림 허용 설정 메뉴로 이동
     */
    public static void openNotificationSettingMenu(Context context) {
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            intent.setAction(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, context.getPackageName());
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.setAction("android.settings.APP_NOTIFICATION_SETTINGS");
            intent.putExtra("app_package", context.getPackageName());
            intent.putExtra("app_uid", context.getApplicationInfo().uid);
        } else {
            intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.setData(Uri.parse("package:" + context.getPackageName()));
        }
        context.startActivity(intent);
    }

    /**
     * 만나이 18세 미만 체크
     *
     * @param birth  주민등록번호 앞번호 6자리
     * @param gender 뒷번호 1자리
     * @return
     */
    public static boolean isAgeCheck(String birth, String gender) {

        String day = null;
        if ("1".equalsIgnoreCase(gender) || "2".equalsIgnoreCase(gender)) {
            day = new StringBuilder("19").append(birth).toString();
        } else if ("3".equalsIgnoreCase(gender) || "4".equalsIgnoreCase(gender)) {
            day = new StringBuilder("20").append(birth).toString();
        }
        Calendar birthCalendar = getCalendarFromDateString(day);
        Calendar currentCalendar = Calendar.getInstance();
        int birthYear = birthCalendar.get(Calendar.YEAR);
        int birthMonth = birthCalendar.get(Calendar.MONTH) + 1;
        int birthDay = birthCalendar.get(Calendar.DAY_OF_MONTH);

        int currentYear = currentCalendar.get(Calendar.YEAR);
        int currentMonth = currentCalendar.get(Calendar.MONTH) + 1;
        int currentDay = currentCalendar.get(Calendar.DAY_OF_MONTH);

        int age = currentYear - birthYear;
        if (birthMonth * 100 + birthDay > currentMonth * 100 + currentDay) {
            age--;
        }
        return age < 17 ? false : true;
    }

    /**
     * 날짜 String -> Calendar 변경 함수
     *
     * @param dateString yyyyMMdd 형식
     * @return Calendar
     */
    public static Calendar getCalendarFromDateString(String dateString) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(dateString.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(dateString.substring(4, 6)) - 1);
        cal.set(Calendar.DATE, Integer.parseInt(dateString.substring(6, 8)));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        return cal;
    }

    /**
     * 생년월일 포맷 체크
     *
     * @param day 주민등록번호 앞6자리
     * @return
     */
    public static boolean isBirthFormat(String day) {
        String regExp = "^\\d{2}(0[1-9]|1[0-2])(0[1-9]|[12][0-9]|[3][0-1])";
        return day.matches(regExp);
    }

    /**
     * HTML 포맷 체크
     *
     * @param str 문자열
     * @return
     */
    public static boolean isHtmlFormat(String str) {
//        String regExp = "<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)</\1>";
//        return str.matches(regExp);

        // 해당 문자열이 포함되어 있는지 체크. 임시 페이지의 index.html이 해당 문자열을 포함하고 있음.
        String checkString = "EMERGENCY_MESSAGE";
        return str.contains(checkString);
    }

    /**
     * 접근성 사용여부 체크
     *
     * @param context
     * @return
     */
    public static boolean isAccessibilityServiceEnabled(Context context) {
        AccessibilityManager am = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
        if (am != null && am.isEnabled()) {
            List<AccessibilityServiceInfo> serviceInfoList = am.getEnabledAccessibilityServiceList(AccessibilityServiceInfo.FEEDBACK_SPOKEN);
            if (!serviceInfoList.isEmpty())
                return true;
        }

        return false;
    }

    /**
     * Toast 출력
     *
     * @param context
     * @param resId   문자열ID
     */
    public static void showToast(Context context, int resId) {
        showToast(context, context.getString(resId));
    }

    /**
     * Toast 출력
     *
     * @param context
     * @param msg     문자열내용
     */
    public static void showToast(Context context, String msg) {
        Toast toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM, 0, 400);
        toast.show();
    }

    /**
     * onClick 시 클릭 연타를 방지해 주는 메소드
     *
     * @return 클릭 가능 여부
     */
    private static long mPreviousBtnClickTime = 0;

    //웹 인터페이스 더블클릭 방지에서 더블클릭을 하지 아니하였지만 더블클릭으로 인식하여 다음 화면 안뜨는 문제.
    public  static void clearPreviousBtnClickTime(){
        mPreviousBtnClickTime = 0;
    }

    public static boolean isEnableClickEvent() {
        // 동시 클릭 방지 (0.6초)
        if (SystemClock.uptimeMillis() - mPreviousBtnClickTime <= 600)
            return false;
        mPreviousBtnClickTime = SystemClock.uptimeMillis();
        return true;
    }

    public static String changeFormatTelephoneNumber(String phoneNum,String dash) {
        if(TextUtils.isEmpty(phoneNum))
            return phoneNum;

        String telNum = phoneNum;
        if (phoneNum.length() == 8) {
            telNum = phoneNum.replaceFirst("^([0-9]{4})([0-9]{4})$", "$1"+dash+"$2");
        } else if (phoneNum.length() == 12) {
            telNum =  phoneNum.replaceFirst("(^[0-9]{4})([0-9]{4})([0-9]{4})$", "$1"+dash+"$2"+dash+"$3");
        }else{
            telNum =  phoneNum.replaceFirst("(^02|[0-9]{3})([0-9]{3,4})([0-9]{4})$", "$1"+dash+"$2"+dash+"$3");
        }

        return telNum;
    }

    /**
     * key가 존재하는지 체크
     *
     * @param json
     * @param key
     * @return
     */
    public static String getJsonString(JSONObject json, String key) {
        String outValue = null;
        if (json.has(key)) {
            try {
                outValue = json.getString(key);
            } catch (JSONException e) {
                MLog.e(e);
            }
        }
        return outValue;
    }

    /**
     * String Resource를 배열로 얻는다.
     *
     * @param resId
     * @return
     * */
    public static ArrayList<String> getStringArray(String[] resId) {
        ArrayList<String> str = new ArrayList<>();
        for (int cnt = 0; cnt < resId.length; cnt++) {
            str.add(resId[cnt]);
        }
        return str;
    }
}
