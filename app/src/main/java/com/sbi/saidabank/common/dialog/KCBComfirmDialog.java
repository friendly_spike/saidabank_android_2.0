package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Utils;


public class KCBComfirmDialog extends BaseDialog implements View.OnClickListener{
    private Context mContext;


    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;

    private String mMsg;

    public KCBComfirmDialog(Context context,String msg) {
        super(context);
        mContext = context;
        mMsg = msg;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_kcb_comfirm);

        setDialogWidth();

        initView();
    }

    private void initView(){
        setCancelable(false);

        String strongStr = "동일함을 확인";
        Utils.setTextWithSpan((TextView) findViewById(R.id.tv_msg),mMsg,strongStr, Typeface.NORMAL,"#00a2bc");

        Button mBtNegative = (Button)findViewById(R.id.bt_negative);
        mBtNegative.setVisibility(View.GONE);
        Button mBtPositive = (Button)findViewById(R.id.bt_positive);
        mBtNegative.setOnClickListener(this);
        mBtPositive.setOnClickListener(this);


        mBtPositive.setBackgroundColor(getContext().getResources().getColor(R.color.color1E2733));
        mBtPositive.setTextColor(getContext().getResources().getColor(R.color.color00EBFF));

        int radius = (int)Utils.dpToPixel(getContext(),5f);
        mBtPositive.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{0,0,radius,radius}));
//        mBtPositive.setBackground(GraphicUtils.getRoundCornerDrawable("#293542",new int[]{0,0,radius,0}));
//        mBtNegative.setBackground(GraphicUtils.getRoundCornerDrawable("#e4e9ec",new int[]{0,0,0,radius}));
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_negative:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {

                    dismiss();
                }
                if(mNListener != null) {
                    mNListener.onClick(view);
                }
                break;
            case R.id.bt_positive:
                if(mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    dismiss();
                }
                if(mPListener != null) {
                    mPListener.onClick(view);
                }
                break;
            default:
                break;
        }
    }
}
