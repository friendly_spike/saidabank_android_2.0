package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;

import androidx.annotation.NonNull;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.Logs;

/**
 * Saidabank_android
 * Class: SlidingBaseDialog
 * Created by 950469 on 2018. 10. 11..
 * <p>
 * Description:
 * 아래에서 위로 올라오는 다이얼로그 Base
 */
public class SlidingBaseDialog extends BaseDialog {
    protected int mScreenHeight;

    public SlidingBaseDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setGravity(Gravity.BOTTOM);
    }

    /**
     * 다이얼로그 넓이를 LCD에 Full로 채우고자 할때호출
     * Basedialog의 함수를 오버라이드한다.
     */
    @Override
    protected void setDialogWidth() {
        if (DataUtil.isNotNull(getWindow())) {
            WindowManager windowManager = getWindow().getWindowManager();
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            // 다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
            LayoutParams lp = getWindow().getAttributes();
            lp.windowAnimations = R.style.SlidingDialogAnimation;
            lp.width = size.x;
            getWindow().setAttributes(lp);
        }
    }

    /**
     * 다이얼로그의 높이를 액티비티 화면 가득 채우고자 할때 호출
     *
     */
    protected void setDialogFullHeight(Context context) {

        int activityHeight = GraphicUtils.getActivityHeight(context);
        mScreenHeight = activityHeight - GraphicUtils.getStatusBarHeight(context);
        // 다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
        LayoutParams lp = getWindow().getAttributes();
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        lp.height = mScreenHeight;
        getWindow().setAttributes(lp);
    }

    /**
     * 다이얼로그의 높이를 액티비티 화면 가득 채우고자 할때 호출
     */
    protected void setDialogHeight(int height) {

        // 다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
        LayoutParams lp = getWindow().getAttributes();
        lp.windowAnimations = R.style.SlidingDialogAnimation;
        lp.height = height;
        getWindow().setAttributes(lp);
    }
}
