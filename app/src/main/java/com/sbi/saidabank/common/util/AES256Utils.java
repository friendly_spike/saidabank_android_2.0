package com.sbi.saidabank.common.util;

import android.text.TextUtils;
import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * AES256Utils : AES256 암/복호화 유틸
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class AES256Utils {

    byte[] keyData;
    private static final String ENCRYPT_KEY = "FAFBA8B5916DDEA9AED7637BC04B81C2".substring(0, 128 / 8);
    private Key keySpec;

    private String getCharSet() {
        return StandardCharsets.UTF_8.name();
    }

    public static String encodeAES(String str) {
        String endcodeStr = "";
        if (!TextUtils.isEmpty(str)) {
            try {
                endcodeStr = new AES256Utils().aesEncode(str);
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                    | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
                MLog.e(e);
            }
        }
        return endcodeStr;
    }

    public static String decodeAES(String str) {
        String decodeStr = "";
        if (!TextUtils.isEmpty(str)) {
            try {
                decodeStr = new AES256Utils().aesDecode(str);
            } catch (UnsupportedEncodingException | NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                    | InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException e) {
                MLog.e(e);
            }
        }
        return decodeStr;
    }

    private AES256Utils() throws UnsupportedEncodingException {
        keyData = ENCRYPT_KEY.getBytes(getCharSet());
        this.keySpec = new SecretKeySpec(keyData, "AES");
    }

    // 암호화
    private String aesEncode(String str) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        Cipher cipher = getCipher();
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(keyData));
        byte[] encrypted = cipher.doFinal(str.getBytes(getCharSet()));
        return Base64.encodeToString(encrypted, Base64.NO_WRAP);
    }

    //복호화
    private String aesDecode(String str) throws java.io.UnsupportedEncodingException, NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException {
        byte[] byteStr = Base64.decode(str.getBytes(getCharSet()), Base64.NO_WRAP);
        Cipher cipher = getCipher();
        cipher.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(keyData));
        return new String(cipher.doFinal(byteStr), getCharSet());
    }

    private Cipher getCipher() throws NoSuchAlgorithmException, NoSuchPaddingException {
        return Cipher.getInstance("AES/CBC/PKCS5Padding");
    }
}
