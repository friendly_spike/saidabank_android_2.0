package com.sbi.saidabank.common.download;

import android.app.Activity;
import android.text.TextUtils;

import com.sbi.saidabank.common.util.Files;
import com.sbi.saidabank.common.util.Logs;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Create 20210113
 * 은행/증권사 이미지 리소스를 Zip파일 형식으로 다운로드 받는다.
 */
public class DownloadRSCAsync extends DownloadFileAsync{

	private ZipFile mZipFile;

	public DownloadRSCAsync(Activity context, DownloadFileCallback l) {
		super(context, l);
	}

	public DownloadRSCAsync(Activity context) {
		super(context, null);
	}

	@Override
	protected void onPreExecute() {
		//super.onPreExecute();
	}
	
	@Override
	protected Boolean doInBackground(String... param) {

		boolean downloadResult = startDownloadFile(param);
		Logs.e("downloadResult : " + downloadResult);
		if(downloadResult){
			try{
				mZipFile = new ZipFile(mSaveFilePath + "/" + mSaveFileName);
			}catch(Exception e){
				e.printStackTrace();
			}
			if(mZipFile != null) {
				int imgCnt = extractZipFileList();

				if (imgCnt > 0)
					return true;
			}
		}
		return false;
	}
	
	@Override
	protected void onCancelled(Boolean result) {
		Logs.e("onCancelled : " + result);
		super.onCancelled(result);
	}
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
	}
	
	@Override
	protected void onPostExecute(Boolean result) {

		Logs.e("onPostExecute : " + result);
		//다운로드가 실패하면 다운받던 파일을 지운다.
		Files.deleteFile(mSaveFilePath + "/" + mSaveFileName);

		//Activity context = activityReference.get();
		//if(context.isFinishing()) return;

		if(mListener != null)
			mListener.onDownloadComplete(result,mSaveFilePath,mSaveFileName);
	}

	/**
	 * Zip파일의 파일 리스트를 만든다.
	 * @return
	 */
	private int extractZipFileList() {
		int item_cnt=0;
		Enumeration entries=null;

		try{
			entries = mZipFile.entries();
		}catch(Exception e){
			e.printStackTrace();
			return item_cnt;
		}

		if(entries == null) return item_cnt;

		while (entries.hasMoreElements()) {
			ZipEntry entry = (ZipEntry) entries.nextElement();
			String entry_name = entry.getName();

			int slashPos = entry_name.lastIndexOf("/");
			if( slashPos > -1){
				entry_name = entry_name.substring(slashPos+1);
			}

			if(TextUtils.isEmpty(entry_name) || !entry_name.contains(".png")) continue;
			if(entry_name.indexOf(".")==0) continue;//파일 이름 앞에 .이있으면 숨길 파일로 되기에 그냥 패스!

			try{
				InputStream is = mZipFile.getInputStream(entry);
				boolean result = writeRscFile(mSaveFilePath,entry_name,is);
				if(result){
					item_cnt++;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		Logs.e("Zip Entry Item Count : " + item_cnt);

		return item_cnt;
	}


	public static boolean writeRscFile(String path, String filename, InputStream instream){
		File listFile=null;

		File file = new File(path);
		if(!file.exists()){
			boolean boolMkdir = file.mkdir();
			if(!boolMkdir){
				return false;
			}
		}

		listFile = new File(path + File.separator + filename);

		if(listFile.exists()){
			listFile.delete();
		}
		try{
			listFile.createNewFile();
		}catch (IOException e) {
			e.printStackTrace();
			return false;
		}


		try {
			int length = 0;
			FileOutputStream fos = new FileOutputStream(listFile);
			int c;
			while ((c = instream.read()) != -1){
				fos.write((byte)c);
				length+= c;
			}
			fos.close();
			Logs.e(listFile.getAbsolutePath() + ", length : " + length);
			if(length <= 0){
				listFile.delete();
				return false;
			}
		} catch(Exception e){
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
