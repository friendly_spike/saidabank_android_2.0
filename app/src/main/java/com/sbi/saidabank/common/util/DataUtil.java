package com.sbi.saidabank.common.util;

import android.os.Bundle;
import android.os.Parcel;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Set;

/**
 * DataUtil : 데이터 관련 유틸
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-02-24
 */
public class DataUtil {

    /**
     * 주어진 스트링이 null 이거나 nullstring(사이즈=0) 인지 체크한다.
     *
     * @param str
     * @return
     */
    public static boolean isNull(Object str) {
        return isNull(str, false);
    }

    public static boolean isNotNull(Object str) {
        return !isNull(str, false);
    }

    /**
     * 주어진 스트링이 null 이거나 nullstring(사이즈=0) 인지 체크한다.
     *
     * @param str
     * @param checkTrim 좌우여백 제거하고 체크할지 여부
     * @return
     */
    public static boolean isNull(Object str, boolean checkTrim) {
        return str == null || (checkTrim ? str.toString().trim().length() : str.toString().length()) == 0;
    }

    public static String checkNull(String str) {
        return checkNull(str, "");
    }

    /**
     * 주어진 스트링이 null 이면 defaultStr 를 반환하고, 다른 경우엔 그냥 str 그대로 반환
     *
     * @param str
     * @param defaultStr
     * @return
     */
    public static String checkNull(String str, String defaultStr) {
        return str == null ? defaultStr : str;
    }

    /**
     * 주어진 스트링이 null 또는 "null" str 이면 defaultStr 를 반환하고, 다른 경우엔 그냥 str 그대로 반환
     *
     * @param str
     * @param defaultStr
     * @return
     */
    public static String checkNullStr(String str, String defaultStr) {
        return str == null ? defaultStr : "null".equals(str.toLowerCase()) ? defaultStr : str;
    }

    /**
     * 주어진 스트링이 "null" 과 같은지 체크한다.
     *
     * @param str
     * @return
     */
    public static boolean isNullStr(Object str) {
        return "null".equals(str);
    }

    /**
     * 주어진 문자열을 반환한다. 파싱이 실패한 경우 -1 을 반환한다.
     *
     * @param str
     * @return
     */
    public static int parseInt(String str) {
        return parseInt(str, -1);
    }

    /**
     * 주어진 문자열을 숫자로 반환한다. 파싱이 실패할 경우 defaultValue 를 반환한다.
     *
     * @param str
     * @param defaultValue
     * @return
     */
    public static int parseInt(String str, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (RuntimeException ex) {
            return defaultValue;
        }
    }

    /**
     * 주어진 숫자를 문자열로 변환한다.
     *
     * @param i
     * @return
     */
    public static String parseString(int i) {
        return parseString(i, "");
    }

    /**
     * 주어진 숫자를 문자열로 변환한다. 파싱이 실패할 경우 defaultValue 를 반환한다.
     *
     * @param i
     * @param defaultValue
     * @return
     */
    public static String parseString(int i, String defaultValue) {
        try {
            return String.valueOf(i);
        } catch (RuntimeException ex) {
            return defaultValue;
        }
    }

    /**
     * decode
     *
     * @param value
     * @return
     */
    public static String decode(String value) {
        if (value == null)
            return null;
        value = value.replace("&#x27;", "'");
        value = value.replace("&amp;", "&");
        value = value.replace("&quot;", "\"");
        value = value.replace("&lt;", "<");
        value = value.replace("&gt;", ">");
        value = value.replace("&#x2F;", "/");
        return value;
    }

    public static String encode(String value) {
        if (value == null)
            return null;
        value = value.replace("'", "&#x27;");
        value = value.replace("&", "&amp;");
        value = value.replace("\"", "&quot;");
        value = value.replace("<", "&lt;");
        value = value.replace(">", "&gt;");
        value = value.replace("/", "&#x2F;");
        return value;
    }

    public static String urldecode(String src) {
        return urldecode(src, "utf-8");
    }

    public static String urldecode(String src, String encoding) {
        try {
            return URLDecoder.decode(src, encoding);
        } catch (Exception ex) {
            MLog.e("DataUtil.urlencode()", ex);
            return null;
        }
    }

    public static String urlencode(String src) {
        return urlencode(src, "utf-8");
    }

    public static String urlencode(String src, String encoding) {
        try {
            return URLEncoder.encode(src, encoding);
        } catch (Exception ex) {
            MLog.e("DataUtil.urlencode()", ex);
            return null;
        }
    }

    /**
     * 숫자를 천단위마다 ','로 끊어서 스트링으로 반환 1234 --> 1,234
     *
     * @param num
     * @return
     */
    public static String toCommaNumber(int num) {
        if (num < 1000) {
            return Integer.toString(num);
        } else {
            int tail = num % 1000;
            String tailStr = Integer.toString(tail);
            if (tail < 10)
                tailStr = leftPadding(tailStr, "0", 2);
            else if (tail < 99)
                tailStr = leftPadding(tailStr, "0", 1);
            int head = (int) ((num - tail) / 1000);
            return toCommaNumber(head) + "," + tailStr;
        }
    }

    /**
     * String 앞에 주어진 String을 count 번 덧붙인다.
     *
     * @param src
     * @param c
     * @param count
     * @return
     */
    public static String leftPadding(String src, String c, int count) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < count; i++) {
            sb.append(c);
        }
        sb.append(src);
        return sb.toString();
    }

    public static String trim(String body) {
        if (body == null)
            return null;
        return body.trim();
    }

    public static void writeStrMapToParcelable(HashMap<String, String> map, Parcel out) {
        if (map != null && map.size() > 0) {
            Set<String> keySet = map.keySet();
            Bundle b = new Bundle();
            for (String key : keySet) {
                b.putString(key, map.get(key));
            }
            String[] array = keySet.toArray(new String[keySet.size()]);
            out.writeStringArray(array);
            out.writeBundle(b);
        } else {
            out.writeStringArray(new String[0]);
            out.writeBundle(Bundle.EMPTY);
        }
    }

    public static HashMap<String, String> readStrMapFromParcelable(Parcel in) {
        HashMap<String, String> map = new HashMap<String, String>();
        if (in != null) {
            String[] keys = in.createStringArray();
            Bundle bundle = in.readBundle();
            for (String key : keys) {
                map.put(key, bundle.getString(key));
            }
        }
        return map;
    }

    public static String getUrlKeyValue(String url, String key) {
        String value = null;
        String refUrl = null;

        if (url == null)
            return null;

        int startIdx = url.indexOf("?");
        refUrl = url.substring(startIdx + 1);

        String[] srcData = refUrl.split("[&|#]");
        String[] resultData;

        for (String data : srcData) {
            if (data.trim().indexOf(key) == 0) {
                resultData = data.trim().split("=");

                if (resultData.length > 1) {
                    value = resultData[1];
                }
            }
        }
        return value;
    }

    /**
     * 특수문자 입력 체크
     *
     * @param str
     * @return
     */
    public static boolean checkString(String str) {
        char[] checkChar = {'.', '^', '$', '-', '+', '*', '=', '<', '>', '\\', '/', '[', ']', '%', '~', '!', '#', '&', '(', ')', '_', '\'', '"'};
        boolean bFlag = false;
        char[] arr = str.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            for (int k = 0; k < checkChar.length; k++) {
                if (arr[i] == checkChar[k]) {
                    bFlag = true;
                    break;
                }
            }
        }
        return bFlag;
    }

}
