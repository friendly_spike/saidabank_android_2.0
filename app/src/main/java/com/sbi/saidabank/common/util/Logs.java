package com.sbi.saidabank.common.util;

import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;
import com.sbi.saidabank.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

public class Logs {
	private static String TAG = "SaidaLog";
	private static String TAG2 = "TestLog";

	/**
	 * 로그 출력 무조건 TRUE로변경. 개발 완료후 변경할것임.
	 */

	//public static boolean isDebug = false;//BuildConfig.DEBUG;
	public static boolean isDebug = BuildConfig.DEBUG;

	private static Toast mToast;

	private static final int LEVEL_VERBOSE = 0;
	private static final int LEVEL_DEBUG = 1;
	private static final int LEVEL_INFO = 2;
	private static final int LEVEL_WARNING = 3;
	private static final int LEVEL_ERROR = 4;
	private static final int LEVEL_ERROR_HANGUL = 5;

	public static void v(Object... args) {
		Logs.log(LEVEL_VERBOSE, args);
	}


	public static void d(Object... args) {
		Logs.log(LEVEL_DEBUG, args);
	}


	public static void i(Object... args) {
		Logs.log(LEVEL_INFO, args);
	}


	public static void w(Object... args) {
		Logs.log(LEVEL_WARNING, args);
	}

	public static void e(Object... args) {
		Logs.log(LEVEL_ERROR, args);
	}

	public static void f(Object... args) {
		Log.e(TAG2,args[0].toString());
	}

	private static synchronized void log(int level, Object[] args) {
		if (isDebug) {
			if (args == null || args.length == 0) {
				return;
			}
		} else {
			return;
		}


		String className = Thread.currentThread().getStackTrace()[4].getClassName();
		String methodName = Thread.currentThread().getStackTrace()[4].getMethodName();
		int nLineNumber = Thread.currentThread().getStackTrace()[4].getLineNumber();

		if (className != null) {
			int lastDotPos = className.lastIndexOf(".");
			className = className.substring(lastDotPos + 1);
		}

		// format
		String format = "" + args[0];
		format = format.replaceAll("%d", "%s");
		format = format.replaceAll("%f", "%s");
		format = format.replaceAll("%c", "%s");
		format = format.replaceAll("%b", "%s");
		format = format.replaceAll("%x", "%s");
		format = format.replaceAll("%l", "%s");

		// argument
		String argument = "";

		switch (args.length - 1) {
			case 0:
				argument = format;
				break;
			case 1:
				argument = String.format(format, "" + args[1]);
				break;
			case 2:
				argument = String.format(format, "" + args[1], "" + args[2]);
				break;
			case 3:
				argument = String.format(format, "" + args[1], "" + args[2], "" + args[3]);
				break;
			case 4:
				argument = String.format(format, "" + args[1], "" + args[2], "" + args[3], "" + args[4]);
				break;
			case 5:
				argument = String.format(format, "" + args[1], "" + args[2], "" + args[3], "" + args[4], "" + args[5]);
				break;
			default:
				break;
		}

		String printLog = String.format("[%s] %s() [%5d] >> %s\n", className, methodName, nLineNumber, argument);

		// Level
		switch (level) {
			case LEVEL_ERROR:
				Log.e(TAG, printLog);
				break;
			case LEVEL_WARNING:
				Log.w(TAG, printLog);
				break;
			case LEVEL_INFO:
				Log.i(TAG, printLog);
				break;
			case LEVEL_VERBOSE:
				Log.v(TAG, printLog);
				break;
			case LEVEL_ERROR_HANGUL:
				Log.e(TAG2,printLog);
				break;
			case LEVEL_DEBUG:
			default:
				Log.d(TAG, printLog);
				break;
		}
	}

	/**
	 * 길이가 무지 긴 인포로그 볼때 사용
	 * @param msg 내용
	 */
	public static void li(String msg){
		int maxLogSize = 1000;
		for(int i = 0; i <= msg.length() / maxLogSize; i++) {
			int start = i * maxLogSize;
			int end = (i+1) * maxLogSize;
			end = end > msg.length() ? msg.length() : end;
			if (isDebug) {
				Log.i(TAG, msg.substring(start, end));
			}
		}
	}


	/**
	 * Exeption로그
	 *
	 * @param e Exeption정보
	 */
	public static void printException(Exception e) {
		if(isDebug){
			Log.e(TAG, "printException >> " + Log.getStackTraceString(e));
		}
    }

	/**
	 * Exeption로그
	 *
	 * @param msg Exeption내용
	 */
	public static void printException(String msg) {
		if(isDebug){
			Log.e(TAG, "printException >> " + msg);
		}
	}

	/**
	 * Exeption로그
	 *
	 * @param tag 태그
	 * @param msg Exeption내용
	 */
	public static void printException(String tag, String msg) {
		if(isDebug){
			Log.e(TAG, tag + ", printException >> " + msg);
		}
	}

	/**
	 * Toast 출력
	 *
	 * @param context
	 * @param resId 문자열ID
	 */
	public static void showToast(Context context, int resId){
		showToast(context,context.getString(resId));
	}

	/**
	 * Toast 출력
	 *
	 * @param context
	 * @param msg	문자열내용
	 */
	public static void showToast(Context context, String msg){
		if(mToast!=null){
			mToast.cancel();
			mToast = null;
		}
		mToast = Toast.makeText(context,msg, Toast.LENGTH_LONG);
		mToast.setGravity(Gravity.BOTTOM,0,0);
		mToast.show();
	}

	public static void printJsonLog(String url, String params, String response) {
		if(isDebug){
			try {
				if (DataUtil.isNotNull(response)) {
					ParseUtil.parseServerData(url, params, new JSONObject(response.trim()));
				} else {
					ParseUtil.parseClientCall(url, params);
				}
			} catch (JSONException e) {
				MLog.e(e);
			}
		}

	}
}
