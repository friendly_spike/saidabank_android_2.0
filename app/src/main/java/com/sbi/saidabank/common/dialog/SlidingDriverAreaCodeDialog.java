package com.sbi.saidabank.common.dialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.datatype.common.RequestCodeInfo;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: SlidingDriverAreaCodeDialog
 * Created by 950485 on 2018. 12. 26..
 * <p>
 * Description: 아래에서 위로 올라오는 운전면허증 지역코드 다이얼로그
 */

public class SlidingDriverAreaCodeDialog extends SlidingBaseDialog implements AdapterView.OnItemClickListener {
    private Context         context;

    private ArrayList<RequestCodeInfo>  driverCodeItems;
    private AdapterView.OnItemClickListener onClickListener;

    public SlidingDriverAreaCodeDialog(@NonNull Context context,
                                       ArrayList<RequestCodeInfo> dataList,
                                       AdapterView.OnItemClickListener onClickListener) {
        super(context);
        this.context = context;
        this.driverCodeItems = dataList;
        this.onClickListener = onClickListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_driver_area_code);
        setDialogWidth();

        SlidingDriverAreaCodeDialog.DriverCodeAdapter driverCodeAdapter = new SlidingDriverAreaCodeDialog.DriverCodeAdapter();
        ListView listView = (ListView) findViewById(R.id.listview_driver_area_code);
        listView.setAdapter(driverCodeAdapter);
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onClickListener.onItemClick(parent, view, position, id);

        if (context instanceof Activity && ((Activity) context).isFinishing())
            return;

        dismiss();
    }

     class DriverCodeAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return driverCodeItems.size();
        }

        @Override
        public Object getItem(int position) {
            return driverCodeItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup viewGroup) {
            final DriverCodeAdapter.AreaCodeViewHolder viewHolder;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.dialog_driver_area_code_list_item, null);

                viewHolder = new DriverCodeAdapter.AreaCodeViewHolder();
                viewHolder.textName = (TextView) convertView.findViewById(R.id.dialog_driver_area_code_name);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (DriverCodeAdapter.AreaCodeViewHolder) convertView.getTag();
            }

            final RequestCodeInfo driverCodeAInfo = (RequestCodeInfo) driverCodeItems.get(position);
            if (driverCodeAInfo != null) {
                String name = driverCodeAInfo.getCD_NM();
                String desc = driverCodeAInfo.getCD_ABRV_NM();

                if (!TextUtils.isEmpty(name)) {
                    if (!TextUtils.isEmpty(desc))
                        name += "\n" + desc;

                    viewHolder.textName.setText(name);
                }
            }

            return convertView;
        }

        class AreaCodeViewHolder {
            TextView textName;
        }
    }
}
