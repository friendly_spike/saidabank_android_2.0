package com.sbi.saidabank.common.contacts;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;

import com.sbi.saidabank.activity.common.CertifyPhoneActivity;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.define.Const;
import com.sbi.saidabank.define.datatype.common.ContactsInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class GetContactListAsyncTask extends AsyncTask<String, Void, Boolean> {
    public static final int  GET_TYPE_CONTACT_APP       = 0; //연락처앱에서 전화번호 가져온후 사이다서버와 비교작업후 조회
    public static final int  GET_TYPE_STORAGE_SAVEFILE  = 1; //사이다서버와 비교작업이 끝난 후 저장되어 있는 파일에서 조회


    private WeakReference<Activity> mActivity;
    private ArrayList<ContactsInfo> mListContacts;
    private OnFinishReadPhoneBook   mListener;
    private int mGetType;

    public interface OnFinishReadPhoneBook{
        void onFinishReadPhoneBook(ArrayList<ContactsInfo> array);
    }

    public GetContactListAsyncTask(Activity activity,int getType, OnFinishReadPhoneBook listener) {
        mActivity = new WeakReference<Activity>(activity);
        mGetType = getType;
        mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mListContacts = new ArrayList<>();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        if(mGetType == GET_TYPE_CONTACT_APP){
            getContactListFromMobilApp();
        }else{
            getContactListFromSaveSyncFile();
        }
        return true;
    }

    @Override
    protected void onPostExecute(Boolean response) {
        super.onPostExecute(response);
        mListener.onFinishReadPhoneBook(mListContacts);
    }

    private void getContactListFromMobilApp(){
        Activity activity = mActivity.get();
        ContactsInfo contactsInfo;
        Cursor contactCursor = null;

        String name;
        String phonenumber;

        try {
            Uri uContactsUri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;

            String[] projection = new String[] {
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID,
                    ContactsContract.CommonDataKinds.Phone.NUMBER,
                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                    ContactsContract.Contacts.PHOTO_ID};

            String sortOrder = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

            contactCursor = activity.getContentResolver().query(uContactsUri, projection, null, null, sortOrder);
            if (contactCursor!= null && contactCursor.moveToFirst()) {
                do {
                    phonenumber = contactCursor.getString(1);
                    if (phonenumber.length() <= 0)
                        continue;

                    name = contactCursor.getString(2);
                    phonenumber = phonenumber.replace("-", "");
                    phonenumber = phonenumber.replace(" ", "");
                    Logs.e("phonenumber : " + phonenumber);

                    if (TextUtils.isEmpty(phonenumber) || phonenumber.length() < 3) {
                        continue;
                    }

                    if(phonenumber.indexOf("+82") == 0){
                        phonenumber = phonenumber.replace("+82","0");
                    }else if(phonenumber.indexOf("82") == 0){
                        phonenumber = phonenumber.replace("82","0");
                    }

                    // 리스트에 추가할 조건 : 이름이 null이 아니고 리스트 내 동일 이름항목이 없으며 핸드폰 번호인 경우
                    if (!TextUtils.isEmpty(name) && !mListContacts.contains(new ContactsInfo(name)) && "010".equals(phonenumber.substring(0, 3))) {
                        contactsInfo = new ContactsInfo();
                        contactsInfo.setTLNO(phonenumber);
                        contactsInfo.setFLNM(name);
                        mListContacts.add(contactsInfo);
                    }
                } while (contactCursor.moveToNext());
            }
        } catch (Exception e) {
            //e.printStackTrace();
            Logs.printException(e);
            mListContacts.clear();
        } finally {
            if (contactCursor != null) {
                contactCursor.close();
            }
        }
    }

    private void getContactListFromSaveSyncFile(){
        Activity activity = mActivity.get();
        StringBuffer sb = new StringBuffer("");
        try {
            File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            String path = storageDir + "/" + Const.CONTACTS_INFO_PATH;
            FileInputStream fIn = new FileInputStream(path);
            InputStreamReader isr = new InputStreamReader(fIn);
            BufferedReader buffreader = new BufferedReader(isr);
            String readString = buffreader.readLine();
            while (readString != null) {
                sb.append(readString);
                readString = buffreader.readLine();
            }
            isr.close();
            buffreader.close();

            if (!TextUtils.isEmpty(sb)) {
                try {
                    JSONObject object = new JSONObject(sb.toString());
                    JSONArray array = object.optJSONArray("REC_OUT_TLNO");
                    if (array == null)
                        return;

                    ArrayList<ContactsInfo> listContacts = new ArrayList<>();
                    for (int index = 0; index < array.length(); index++) {
                        JSONObject objItem = array.getJSONObject(index);
                        ContactsInfo contactsInfo = new ContactsInfo(objItem);

                        mListContacts.add(contactsInfo);
                    }
                } catch (JSONException e) {
                    Logs.printException(e);
                    mListContacts.clear();
                }
            }
        } catch (IOException e) {
            Logs.printException(e);
            mListContacts.clear();
        }
    }
}
