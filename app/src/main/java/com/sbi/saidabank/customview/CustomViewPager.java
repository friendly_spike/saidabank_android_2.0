package com.sbi.saidabank.customview;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {
    private boolean mIsEnabled;

    public CustomViewPager(Context context) {
        super(context);
        mIsEnabled = true;
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        mIsEnabled = true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (mIsEnabled) {
            try {
                return super.onInterceptTouchEvent(ev);
            } catch (IllegalArgumentException e) {
                return false;
            }
        } else {
             return false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (mIsEnabled) {
            return super.onTouchEvent(ev);
        } else {
            return false;
        }

    }

    public void setPagingEnabled(boolean enabled) {
        this.mIsEnabled = enabled;
    }
}
