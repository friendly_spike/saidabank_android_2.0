package com.sbi.saidabank.customview;

import android.view.View;

import com.sbi.saidabank.common.util.Utils;

/**
 * OnSingleClickListener : 버튼 중복클릭 방지용 클릭리스너
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public abstract class OnSingleClickListener implements View.OnClickListener {

    public abstract void onSingleClick(View v);

    @Override
    public void onClick(View view) {
        if (Utils.isEnableClickEvent()) {
            onSingleClick(view);
        }
    }
}
