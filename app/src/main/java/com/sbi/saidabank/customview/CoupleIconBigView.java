package com.sbi.saidabank.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.GraphicUtils;
import com.sbi.saidabank.common.util.ImgUtils;
import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;

public class CoupleIconBigView extends RelativeLayout {

    private FrameLayout mLayoutOwner;
    private ImageView   mIvOwner;
    private TextView    mTvOwner;

    private FrameLayout mLayoutSharener;
    private ImageView   mIvSharener;
    private TextView    mTvSharener;

    private String mOwerName;
    private String mSharerName;
    private String mOwnerImgData;
    private String mSharerImgData;
    private boolean isDispFamilyName;
    public CoupleIconBigView(Context context) {
        super(context);
        initUX(context);
    }

    public CoupleIconBigView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX(context);
    }

    private void initUX(Context context){
        View layout = View.inflate(context, R.layout.layout_main2_couple_icon_big, null);
        addView(layout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        mLayoutOwner = layout.findViewById(R.id.layout_owner);
        mIvOwner     = layout.findViewById(R.id.iv_owner);
        mTvOwner     = layout.findViewById(R.id.tv_owner);

        mLayoutSharener = layout.findViewById(R.id.layout_sharener);
        mIvSharener     = layout.findViewById(R.id.iv_sharener);
        mTvSharener     = layout.findViewById(R.id.tv_sharener);
    }

    public void setData(String owner_img_data,final String owner_name,String share_img_data,final String sharener_name){
        mOwerName = owner_name;
        mSharerName = sharener_name;
        mOwnerImgData = owner_img_data;
        mSharerImgData = share_img_data;

        //계좌주
        drawAccountOwer(owner_name,owner_img_data);

        //공유자
        drawAccountShare(sharener_name,share_img_data);
    }


    private void drawAccountOwer(String name,String img_data){
        int radius = (int) Utils.dpToPixel(getContext(),56);
        if(!TextUtils.isEmpty(img_data)){
            byte[] byteArray = Base64.decode(img_data,Base64.DEFAULT);
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            if(bmp != null){
                Bitmap roundBmp = ImgUtils.getRoundedCornerBitmap(bmp,50);
                if(roundBmp != null){
                    mIvOwner.setImageBitmap(roundBmp);
                    return;
                }

            }
        }

        mLayoutOwner.setBackground(GraphicUtils.getRoundCornerDrawable("#FFEB00",new int[]{radius,radius,radius,radius}));

        if(!TextUtils.isEmpty(name)){
            //String familyName = name.substring(0,1);
            if(name.length() > 3){
                name = name.substring(0,3) + "...";
            }

            mTvOwner.setText(name);
        }
    }

    //공유자 아이콘 그리자.
    private void drawAccountShare(String name,String img_data){
        int radius = (int) Utils.dpToPixel(getContext(),56);
        if(!TextUtils.isEmpty(img_data)){
            //mLayoutSharener.setBackground(GraphicUtils.getRoundCornerDrawable("#FFFFFF",new int[]{radius,radius,radius,radius},0,"#FFFFFF"));

            //Logs.e("setData - share_img_data : " + share_img_data);
            byte[] byteArray = Base64.decode(img_data,Base64.DEFAULT);
            //Logs.e("setData - byteArray : " + byteArray);
            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            if(bmp != null){
                Bitmap roundBmp = ImgUtils.getRoundedCornerBitmap(bmp,50);
                if(roundBmp != null){
                    mIvSharener.setImageBitmap(roundBmp);
                    return;
                }
            }
        }

        mTvSharener.setBackground(GraphicUtils.getRoundCornerDrawable("#47597f",new int[]{radius,radius,radius,radius},0,"#FFFFFF"));
        if(!TextUtils.isEmpty(name)){
            //String familyName = name.substring(0,1);
            if(name.length() > 3){
                name = name.substring(0,3) + "...";
            }
            mTvSharener.setText(name);
        }
    }

    public void displayFamilyName(boolean flag){
        String name="";
        if(flag){
            if(isDispFamilyName) return;

            if(!TextUtils.isEmpty(mOwerName) && TextUtils.isEmpty(mOwnerImgData)){
                String familyName = mOwerName.substring(0,1);
                mTvOwner.setText(familyName);
                mTvOwner.setTextSize(24);
            }
            if(!TextUtils.isEmpty(mSharerName) && TextUtils.isEmpty(mSharerImgData)){
                String familyName = mSharerName.substring(0,1);
                mTvSharener.setText(familyName);
                mTvSharener.setTextSize(24);
            }
            isDispFamilyName = true;
        }else{
            if(!isDispFamilyName) return;

            name = mOwerName;
            if(!TextUtils.isEmpty(name) && TextUtils.isEmpty(mOwnerImgData)){
                if(name.length() > 3){
                    name = name.substring(0,3) + "...";
                }
                mTvOwner.setText(name);
                mTvOwner.setTextSize(14);
            }
            name = mSharerName;
            if(!TextUtils.isEmpty(name) && TextUtils.isEmpty(mSharerImgData)){
                if(name.length() > 3){
                    name = name.substring(0,3) + "...";
                }
                mTvSharener.setText(name);
                mTvSharener.setTextSize(14);
            }
            isDispFamilyName = false;
        }
    }
}
