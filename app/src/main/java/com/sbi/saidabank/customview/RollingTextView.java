package com.sbi.saidabank.customview;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.Utils;
import com.sbi.saidabank.define.Const;

import java.util.Calendar;

public class RollingTextView extends RelativeLayout {
    private Context mContext;
    private Activity mActivity;
    private String[] mInputStr;
    private TextView mRollTest;
    private TextView mRollTest1;
    private final TranslateAnimation slide;
    private final TranslateAnimation slide1;
    private Thread mThread;
    private int mSelectedIndex;

    private IRollingTextSelected rollingTextListener;


    public RollingTextView(ViewGroup root, String[] inputString) {
        super(root.getContext());
        mContext = root.getContext();
        mActivity = ((Activity) mContext);
        mInputStr = inputString;
        mSelectedIndex = 0;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.rollingtextview_container, this, true);

        mRollTest = findViewById(R.id.tv_input01);
        mRollTest1 = findViewById(R.id.tv_input02);
        mRollTest1.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (rollingTextListener != null)
                    rollingTextListener.onRollingTextSelect(mSelectedIndex);
            }
        });
        root.addView(this);

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
                    (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            mRollTest.setTextColor(ContextCompat.getColor(mContext, R.color.white));
            mRollTest1.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        } else {
            mRollTest.setTextColor(ContextCompat.getColor(mContext, R.color.black));
            mRollTest1.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }

        slide = new TranslateAnimation(0, 0, 0, -Utils.dpToPixel(mContext, 35.0f));
        slide.setDuration(2000);
        slide.setFillAfter(true);
        slide.setInterpolator(new LinearInterpolator());

        slide1 = new TranslateAnimation(0, 0, Utils.dpToPixel(mContext, 35.0f), 0);
        slide1.setDuration(2000);
        slide1.setFillAfter(true);
        slide1.setInterpolator(new LinearInterpolator());

        if (mInputStr == null || mInputStr.length < 1) {
            return;
        } else if (mInputStr.length < 2) {
            mRollTest.setText(mInputStr[0]);
            return;
        }

        mRollTest.setText(mInputStr[0]);

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mThread == null)
                    runTextRolling();
            }
        }, 2000);
    }

    private void runTextRolling() {

        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                final boolean[] isRolling = {true};
                final boolean[] isRestart = {true};
                final int[] count = {0};

                while (true) {
                    if (mContext == null || mActivity.isFinishing()) {
                        mThread.interrupt();
                        mThread = null;
                        break;
                    }

                    if (mRollTest == null || mRollTest1 == null) {
                        mThread.interrupt();
                        mThread = null;
                        break;
                    }

                    if (!mRollTest.isShown() || !mRollTest1.isShown()) {
                        count[0] = 0;
                        isRolling[0] = true;
                        isRestart[0] = false;
                        continue;
                    } else {
                        if (!isRestart[0]) {
                            isRestart[0] = true;
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                //e.printStackTrace();
                            }
                        }
                    }

                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (count[0] == 0) {
                                if (isRolling[0]) {
                                    mRollTest.setText(mInputStr[count[0]]);
                                    mRollTest1.setText(mInputStr[count[0] + 1]);
                                } else {
                                    mRollTest1.setText(mInputStr[count[0]]);
                                    mRollTest.setText(mInputStr[count[0] + 1]);
                                }
                                count[0]++;
                            } else if (count[0] < mInputStr.length - 1) {
                                if (isRolling[0]) {
                                    mRollTest1.setText(mInputStr[count[0] + 1]);
                                    mRollTest.setText(mInputStr[count[0]]);
                                } else {
                                    mRollTest.setText(mInputStr[count[0] + 1]);
                                    mRollTest1.setText(mInputStr[count[0]]);
                                }
                                count[0]++;
                            } else {
                                if (isRolling[0]) {
                                    mRollTest.setText(mInputStr[count[0]]);
                                    mRollTest1.setText(mInputStr[0]);
                                } else {
                                    mRollTest1.setText(mInputStr[count[0]]);
                                    mRollTest.setText(mInputStr[0]);
                                }
                                count[0] = 0;
                            }
                            mSelectedIndex = count[0];
                            if (isRolling[0]) {
                                isRolling[0] = false;
                                mRollTest.startAnimation(slide);
                                mRollTest1.startAnimation(slide1);
                            } else {
                                isRolling[0] = true;
                                mRollTest1.startAnimation(slide);
                                mRollTest.startAnimation(slide1);
                            }
                        }
                    });

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }
                }
            }
        });
        mThread.start();
    }

    public void setOnRollingTextListener(IRollingTextSelected listener) {
        rollingTextListener = listener;
    }

    public interface IRollingTextSelected {
        void onRollingTextSelect(int index);
    }
}
