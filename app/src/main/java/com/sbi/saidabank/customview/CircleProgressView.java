package com.sbi.saidabank.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.SweepGradient;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import com.sbi.saidabank.common.util.Logs;
import com.sbi.saidabank.common.util.Utils;

/**
 * siadabank_android
 * Class: CircleProgressView
 * Created by 950546
 * Date: 2018-12-12
 * Time: 오후 5:04
 * Description: 원형 그래프뷰
 */
public class CircleProgressView extends View {

    private Context mContext;
    private Paint mpaint;
    private Paint mpaint1;
    private float mPercent;
    private int mCount;
    private RectF mOval;
    private Bitmap mBitmap;
    private int mStartColor;
    private int mEndColor;
    private float mStrokeWidth;
    private float mStartAngle;
    private float mInnerSize;
    private long mSpeed;
    private Handler mHandler;
    private int mWidth;
    private int mHeight;
    private float mPer;
    private Matrix mMatrix;
    private Shader gradient;

    public CircleProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mpaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mpaint1 = new Paint(Paint.ANTI_ALIAS_FLAG);
        mOval = new RectF();
        mPercent = 0.0F;
        mCount = 360;
        mStartColor = Color.GREEN;
        mEndColor = Color.BLUE;
        mStrokeWidth = Utils.dpToPixel(mContext, 5);
        mStartAngle = 270.0F;
        mInnerSize = 0.0F;
        mSpeed = 1L;
        mWidth = 0;
        mHeight = 0;
        mPer = 0;
        mpaint.setStyle(Paint.Style.STROKE);
        mpaint.setStrokeCap(Paint.Cap.ROUND);
        mMatrix = new Matrix();
        mHandler = new Handler();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mPer = 360 * mPercent / 360;

        // 그래프 그리기
        canvas.drawArc(mOval, mStartAngle, mPer, false, mpaint);

        // 이너써클 이미지가 있으면 그리기
        if (mBitmap != null) {
            float a = mWidth / 2 + mInnerSize * (float) Math.cos(Math.toRadians(360 - mStartAngle - mPer)) - mBitmap.getWidth() / 2;
            float b = mWidth / 2 - mInnerSize * (float) Math.sin(Math.toRadians(360 - mStartAngle - mPer)) - mBitmap.getHeight() / 2;
            canvas.drawBitmap(mBitmap, a, b, mpaint1);
        }
    }

    //
    final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (mPercent <= mCount) {
                mPercent = mPercent + 1.0F;
                mHandler.postDelayed(runnable, mSpeed);
            }
            invalidate();
        }
    };

    /**
     * 그래프 그리기 시작
     */
    public void startDraw() {
        //++ 초기화
        if (mWidth == 0 || mHeight == 0) {
            Logs.e( "ondraw");
            mWidth = getWidth();
            mHeight = getHeight();
            // 이너써클의 사이즈가 그래프보다 크면 그래프사이즈로 수정
            if (mInnerSize == 0 || mInnerSize > mWidth)
                mInnerSize = mWidth;
        }
        mpaint.setStrokeWidth(mStrokeWidth);
        mOval.set(mStrokeWidth / 2, mStrokeWidth / 2, mWidth - mStrokeWidth / 2, mHeight - mStrokeWidth / 2);
        int[] mColor = {mStartColor, mEndColor, mStartColor};
        float[] mPostion = {0, 0.9f, 1f};
        gradient = new SweepGradient(mWidth / 2, mHeight / 2, mColor, mPostion);
        mMatrix.setRotate(mStartAngle, mWidth / 2, mHeight / 2);
        gradient.setLocalMatrix(mMatrix);
        mpaint.setShader(gradient);
        //--
        mHandler.post(runnable);
    }

    /**
     * 그리기 스피드 설정
     * default : 0
     *
     * @param speed : 0~5 step (1L ~ 80L)
     */
    public void setSpeed(int speed) {
        int mSpeed = 0;
        if (speed < 0)
            speed = 0;
        else if (speed > 4)
            speed = 4;

        switch (speed) {
            case 0:
                this.mSpeed = 1L;
                break;
            case 1:
                this.mSpeed = 5L;
                break;
            case 2:
                this.mSpeed = 10L;
                break;
            case 3:
                this.mSpeed = 20L;
                break;
            case 4:
                this.mSpeed = 40L;
                break;
            case 5:
                this.mSpeed = 80L;
                break;
            default:
                this.mSpeed = mSpeed;
                break;
        }
    }

    /**
     * 내부 써클 사이즈 설정
     * default : 그래프 지름
     *
     * @param size 내부 써클 반지름
     */
    public void setInnerSize(float size) {
        mInnerSize = Utils.dpToPixel(mContext, size);
    }

    /**
     * 내부 써클 이미지 설정
     * default :  내부 이미지 그리지 않음
     *
     * @param drawable 내부 이미지
     */
    public void setInnerImage(int drawable) {
        mBitmap = BitmapFactory.decodeResource(getResources(), drawable);
    }

    /**
     * 그래프 시작 각도 설정
     * default : 270도
     *
     * @param startAngle 그래프 시작 각도 (0 - 360)
     */
    public void setStartAngle(float startAngle) {
        mStartAngle = startAngle;
    }

    /**
     * 그래프 컬러 설정
     * default : Green, Blue
     *
     * @param startColor : 시작 컬러
     * @param endColor   종료 컬러
     */
    public void setColor(int startColor, int endColor) {
        mStartColor = startColor;
        mEndColor = endColor;
    }

    /**
     * 그래프 두께 설정
     * default : 0
     *
     * @param width : 두께(0 - 4)
     */
    public void setStorkeWidth(int width) {
        int mWidth = 5;

        if (width < 0)
            mWidth = 5;
        else if (width > 4)
            mWidth = 25;

        switch (width) {
            case 0:
                mWidth = 5;
                break;
            case 1:
                mWidth = 10;
                break;
            case 2:
                mWidth = 15;
                break;
            case 3:
                mWidth = 20;
                break;
            case 4:
                mWidth = 25;
                break;
            default:
            break;
        }
        mStrokeWidth = Utils.dpToPixel(mContext, mWidth);
    }

    /**
     * 그래프 범위 설정
     * default : 100
     *
     * @param percent : 0 - 100
     */
    public void setPercent(int percent) {
        int mPercent = percent;

        if (percent < 0)
            mPercent = 0;
        else if (percent > 100)
            mPercent = 100;

        mCount = 360 * mPercent / 100;
        this.mPercent = 0.0F;
    }
}
