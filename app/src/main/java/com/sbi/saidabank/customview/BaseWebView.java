package com.sbi.saidabank.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.sbi.saidabank.common.net.HttpUtils;
import com.sbi.saidabank.common.util.MLog;

/**
 * BaseWebView : 커스텀 웹뷰
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-26
 */
@SuppressLint("NewApi")
public class BaseWebView extends WebView {

	public Context mContext;

	public BaseWebView(Context context) {
		super(context);
		if (!isInEditMode())
			init(context);
	}

	public BaseWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		if (!isInEditMode())
			init(context);
	}

	public BaseWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		if (!isInEditMode())
			init(context);
	}

	@Override
	public void destroy() {
		ViewParent parent = this.getParent();
		if (parent instanceof ViewGroup) {
			((ViewGroup) parent).removeView(this);
		}
		loadUrl("about:blank");
		try {
			removeAllViews();
		} catch (Exception e) {
			MLog.e(e);
		}
		super.destroy();
	}

	public void init(Context context) {
		this.mContext = context;
		setDefaultWebSettings();
	}

	public void setDefaultWebSettings() {
		WebSettings settings = getSettings();
		settings.setUserAgentString(HttpUtils.makeUserAgentString(mContext, settings.getUserAgentString()));
		settings.setJavaScriptEnabled(true);
		settings.setJavaScriptCanOpenWindowsAutomatically(true);
		//settings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setBuiltInZoomControls(false);
		settings.setSupportZoom(false);
		settings.setSaveFormData(false);
		settings.setDisplayZoomControls(false);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setLoadsImagesAutomatically(true);
		settings.setAllowFileAccess(true);
		settings.setGeolocationEnabled(true);
		settings.setSupportMultipleWindows(true);
		settings.setNeedInitialFocus(false);
		settings.setAllowContentAccess(false);

		// HTML5 API flags
		settings.setDomStorageEnabled(true);
		settings.setDatabaseEnabled(true);
		settings.setAppCacheEnabled(true);

		// Https로 로드된 페이지에서 http링크로된 리소스 연결할 때 block 방지
		settings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

		settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);
		settings.setTextZoom(100);


		// HTML5 Configuration Parameter Settings.
		settings.setAppCachePath(mContext.getDir("appcache", 0).getPath());
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
			settings.setGeolocationDatabasePath(mContext.getDir("geolocation", 0).getPath());
		}

		// Origin policy for file access
		settings.setAllowUniversalAccessFromFileURLs(false);
		settings.setAllowFileAccessFromFileURLs(false);

		//쿠키 동기화
		CookieManager cookieManager = CookieManager.getInstance();
		cookieManager.setAcceptCookie(true);
		CookieManager.getInstance().setAcceptThirdPartyCookies(this, true);

		setLayerType(View.LAYER_TYPE_HARDWARE, null);


		setHorizontalScrollBarEnabled(true);
		setVerticalScrollBarEnabled(true);
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			setHorizontalScrollbarOverlay(true);
			setVerticalScrollbarOverlay(true);
		}
		setScrollbarFadingEnabled(true);

		WebView.setWebContentsDebuggingEnabled(true);

	}
}
