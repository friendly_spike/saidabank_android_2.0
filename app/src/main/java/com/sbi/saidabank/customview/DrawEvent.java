package com.sbi.saidabank.customview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.view.View;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.MLog;
import com.sbi.saidabank.common.util.Utils;

import static com.sbi.saidabank.customview.patternlockview.utils.RandomUtils.randInt;

/**
 * DrawEvent : 터치 이벤트
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.1
 * @since 2020-02-24
 */
public class DrawEvent extends View {

    Bitmap mBitmap;
    private int minX = 0;
    private int minY = 0;
    private int maxX;
    private int maxY;
    private float eventWidth;
    private float eventHeight;
    private float eventX;
    private float eventY;
    private float eventSpeedX = 10;
    private float eventSpeedY = 10;
    private Paint paint;
    private TouchViewListener touchViewListener;

    public interface TouchViewListener {
        void onTouchView();
    }

    public DrawEvent(Context context, TouchViewListener touchViewListener) {
        super(context);
        mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_event_gift_default);
        eventWidth = Utils.dpToPixel(context, 70);
        eventHeight = Utils.dpToPixel(context, 70);
        eventY = eventHeight + randInt() % 1000;
        eventX = eventWidth + randInt() % 500;
        paint = new Paint();
        this.setFocusableInTouchMode(true);
        this.touchViewListener = touchViewListener;
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        update();
        canvas.drawBitmap(mBitmap, eventX, eventY, paint);
        try {
            Thread.sleep(5);
        } catch (InterruptedException e) {
            MLog.e(e);
            Thread.currentThread().interrupt();
        }
        invalidate();
    }

    // Detect collision and update the position of the ball.
    private void update() {
        // Get new (x,y) position
        eventX += eventSpeedX;
        eventY += eventSpeedY;
        // Detect collision and react
        if (eventX + eventWidth > maxX) {
            eventSpeedX = (7 + randInt() % 5);
            eventSpeedX = -eventSpeedX;
            eventX = maxX - eventWidth;
        } else if (eventX < minX) {
            eventSpeedX = -(7 + randInt() % 5);
            eventSpeedX = -eventSpeedX;
            eventX = minX;
        }
        if (eventY + eventHeight > maxY) {
            eventSpeedY = (7 + randInt() % 5);
            eventSpeedY = -eventSpeedY;
            eventY = maxY - eventHeight;
        } else if (eventY < minY) {
            eventSpeedY = -(7 + randInt() % 5);
            eventSpeedY = -eventSpeedY;
            eventY = minY;
        }
    }

    // Called back when the view is first created or its size changes.
    @Override
    public void onSizeChanged(int w, int h, int oldW, int oldH) {
        maxX = w - 1;
        maxY = h - 1;
    }

    // Touch-input handler
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            float currentX = event.getX();
            float currentY = event.getY();
            if (currentX > eventX
                    && currentX < (eventX + eventWidth)
                    && currentY > eventY
                    && currentY < (eventY + eventHeight)) {
                mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_event_gift_pressed);
                touchViewListener.onTouchView();
                return true;
            }
        }
        return false;
    }
}
