package com.sbi.saidabank.customview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ScrollView;

import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.common.util.MLog;

@SuppressLint("NewApi")
public class CustomScrollView extends ScrollView implements View.OnScrollChangeListener {

    private boolean isScroll = false;
    private boolean mCallScrollTo;

    private Handler mHandler;
    private OnScrollActionListener mActionListener;

    public void setOnScrollActionListener(OnScrollActionListener listener) {
        mActionListener = listener;
    }

    public interface OnScrollActionListener {
        void onScrollStart(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY);
        void onScrollStop(boolean isBottom);
        void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY);
    }

    public CustomScrollView(Context context) {
        super(context);
        initUX();
    }

    public CustomScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUX();
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initUX();
    }

    public CustomScrollView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initUX();
    }

    private void initUX() {
        mHandler = new Handler();
        setOnScrollChangeListener(this);
        setOverScrollMode(OVER_SCROLL_NEVER);   // 헤더 및 풋터의 엣지를 사라지게 한다.
    }

    @Override
    public void scrollTo(int x, int y) {
        mCallScrollTo = true;
        super.scrollTo(x, y);
    }

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if (DataUtil.isNull(mActionListener) || mCallScrollTo) {
            mCallScrollTo = false;
            return;
        }

        if ((scrollY - oldScrollY) == 0) return;

//        int moveY = scrollY - oldScrollY;
//        if (moveY < 0) {
//            moveY = moveY * -1;
//        }

        if (!isScroll) {
            mActionListener.onScrollStart(v, scrollX, scrollY, oldScrollX, oldScrollY);
            isScroll = true;
        } else {
            mActionListener.onScrollChange(v, scrollX, scrollY, oldScrollX, oldScrollY);
        }

        mHandler.removeCallbacks(runnable);
        mHandler.postDelayed(runnable, 150);
    }

    /**
     * 타임체크로 스크롤을 확인한다.
     */
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            View view = getChildAt(getChildCount() - 1);
            int bottomDetector = view.getBottom() - (getHeight() + getScrollY());
            boolean isBottom = false;
            if (bottomDetector == 0) {
                MLog.i("Scroll View bottom reached");
                isBottom = true;
            }
            mActionListener.onScrollStop(isBottom);
            isScroll = false;
        }
    };
}
