package com.sbi.saidabank.customview;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import com.sbi.saidabank.common.util.Logs;

import java.util.ArrayList;

/**
 * Saidabank_android
 * Class: KeyboardDetectorRelativeLayout
 * Created by 950546
 * Date: 2019-01-30
 * Time: 오전 10:38
 * Description: 키보드 show/hide 상태 체크용 레이아웃
 */
public class KeyboardDetectorRelativeLayout extends RelativeLayout {

    private int totalHeight=0;
    private boolean keyboardShow=false;

    public interface IKeyboardChanged {
        void onKeyboardShown();
        void onKeyboardHidden();
    }

    private ArrayList<IKeyboardChanged> keyboardListener = new ArrayList<IKeyboardChanged>();

    public KeyboardDetectorRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public KeyboardDetectorRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KeyboardDetectorRelativeLayout(Context context) {
        super(context);
    }

    public void addKeyboardStateChangedListener(IKeyboardChanged listener) {
        keyboardListener.add(listener);
    }

    public void removeKeyboardStateChangedListener(IKeyboardChanged listener) {
        keyboardListener.remove(listener);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int proposedheight = MeasureSpec.getSize(heightMeasureSpec);
        final int actualHeight = getHeight();

        if(proposedheight > 0 && actualHeight == 0 ){
            if(totalHeight < proposedheight)
                totalHeight = proposedheight;
        }

        //Logs.e("onMeasure - totalHeight : " + totalHeight);
        //Logs.e("onMeasure - proposedheight : " + proposedheight);
        //Logs.e("onMeasure - actualHeight : " + actualHeight);

        int resourceId = getResources().getIdentifier("config_showNavigationBar", "bool", "android");   // softkey 표시 여부
        int softKeyHeight = 0;
        if (resourceId > 0 && getResources().getBoolean(resourceId)) {
            resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android"); // softkey 높이
            if (resourceId > 0) {
                //Logs.e("softkeyheight : " + getResources().getDimensionPixelSize(resourceId));
                softKeyHeight = getResources().getDimensionPixelSize(resourceId);
            }
        }

        if (softKeyHeight == 0)
            softKeyHeight = 75;

        if (totalHeight - proposedheight > 300) {
            notifyKeyboardShown();
        } else if (totalHeight - proposedheight <= softKeyHeight) {
            notifyKeyboardHidden();
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    private void notifyKeyboardHidden() {
        for (IKeyboardChanged listener : keyboardListener) {
            if(keyboardShow){
                listener.onKeyboardHidden();
                keyboardShow=false;
            }
        }
    }

    private void notifyKeyboardShown() {
        for (IKeyboardChanged listener : keyboardListener) {
            if(!keyboardShow){
                listener.onKeyboardShown();
                keyboardShow=true;
            }

        }
    }
}
