package com.sbi.saidabank.customview;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.define.Const;

import java.util.Calendar;

public class RollingMainTextView extends RelativeLayout {
    private Context   mContext;
    private String[]  mSlideEvent;
    private TextView   mRollingTextView;
    private Thread    mThread;
    private int      mRollingTextIndex;

    private IRollingTextSelected rollingTextListener;

    public RollingMainTextView(ViewGroup root, String[] slideEvent) {
        super(root.getContext());
        mContext = root.getContext();
        mSlideEvent = slideEvent;

        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.rolling_main_textview_container, this, true);

        mRollingTextView = (TextView) findViewById(R.id.textview_rolling);
        mRollingTextView.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                rollingTextListener.OnRollingTextSelect(mRollingTextIndex);
            }
        });
        root.addView(this);

        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if ((timeOfDay >= 0 && timeOfDay < Const.NIGHTTIME_END) ||
                    (timeOfDay >= Const.NIGHTTIME_START && timeOfDay < 24)) {
            mRollingTextView.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        } else {
            mRollingTextView.setTextColor(ContextCompat.getColor(mContext, R.color.black));
        }

        if (slideEvent == null || slideEvent.length < 1) {
            return;
        } else if (slideEvent.length < 2) {
            mRollingTextView.setText(slideEvent[0]);
            return;
        }

        mRollingTextView.setText(slideEvent[0]);

        Handler delayHandler = new Handler();
        delayHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mThread == null)
                    runTextRolling();
            }
        }, 2000);
    }

    private void runTextRolling() {
        mThread = new Thread(new Runnable() {
            @Override
            public void run() {
                final boolean[] isRestart = {true};

                while (true) {
                    if (mContext == null || ((Activity) mContext).isFinishing()) {
                        mThread.interrupt();
                        mThread = null;
                        break;
                    }

                    if (mRollingTextView == null) {
                        mThread.interrupt();
                        mThread = null;
                        break;
                    }

                    if (!mRollingTextView.isShown()) {
                        mRollingTextIndex = 0;
                        isRestart[0] = false;
                        continue;
                    } else {
                        if (!isRestart[0]) {
                            isRestart[0] = true;
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                //e.printStackTrace();
                            }
                        }
                    }

                    ((Activity) mContext).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (mRollingTextIndex < mSlideEvent.length - 1)
                                mRollingTextIndex++;
                            else
                                mRollingTextIndex = 0;

                            mRollingTextView.setText(mSlideEvent[mRollingTextIndex]);
                        }
                    });

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }
                }
            }
        });
        mThread.start();
    }

    public void setOnRollingTextListener(IRollingTextSelected listener) {
        rollingTextListener = listener;
    }

    public interface IRollingTextSelected {
        void OnRollingTextSelect(int index);
    }
}
