package com.sbi.saidabank.customview;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;

import com.sbi.saidabank.common.util.Logs;

/**
 * Saidabanking_android
 * Class: CustomEditText
 * Created by 950546
 * Date: 2018-10-19
 * Time: 오후 4:18
 * Description:
 */
public class CustomEditText extends androidx.appcompat.widget.AppCompatEditText {

    private OnBackPressListener _listener;

    public CustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_NO);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_BACK && _listener != null) {
            // User has pressed Back key. So hide the keyboard
            //Hide your view as you do it in your activity
            Logs.i("hide soft keyboard");
            _listener.onKeyboardBackPress();
        }
        return super.onKeyPreIme(keyCode, event);
    }

    /**
     * 키보드 back키 입력 시 전달받을 리스터 등록
     * @param listener
     */
    public void setOnBackPressListener(OnBackPressListener listener) {
        _listener = listener;
    }

    public interface OnBackPressListener {
        void onKeyboardBackPress();
    }

}
