package com.sbi.saidabank.customview;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sbi.saidabank.R;
import com.sbi.saidabank.common.util.DataUtil;
import com.sbi.saidabank.define.Const;

/**
 * CustomFakeApp : 악성 앱 아이템
 *
 * @author Jung.Seung.Jin(950868)
 * @version 1.0.0
 * @since 2020-11-16
 */
public class CustomFakeApp extends RelativeLayout {

    /** 생성자 */
    private Context mContext;

    /** 악성 앱 아이콘 */
    private ImageView mAppIcon;

    /** 악성 앱 패키지명 */
    private TextView mAppNm;

    public CustomFakeApp(Context context) {
        super(context);
        init(context);
    }

    public CustomFakeApp(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomFakeApp(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        this.mContext = context;
        View view = LayoutInflater.from(mContext).inflate(R.layout.view_fake_app_item, this, true);
        mAppIcon = (ImageView) view.findViewById(R.id.iv_app_icon);
        mAppNm = (TextView) view.findViewById(R.id.tv_app_name);
    }

    public void setAppIcon(String packagename, int defaultImage) {
        try {
            if (!DataUtil.isNull(packagename)) {
                Drawable icon = mContext.getPackageManager().getApplicationIcon(packagename);
                mAppIcon.setBackground(icon);
            } else {
                mAppIcon.setBackgroundResource(defaultImage);
            }
        } catch (PackageManager.NameNotFoundException e) {
            mAppIcon.setBackgroundResource(defaultImage);
        }
    }

    public void setText(String str) {
        mAppNm.setText(DataUtil.isNotNull(str) ? str : Const.EMPTY);
    }
}
