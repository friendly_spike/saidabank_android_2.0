# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

# Uncomment this to preserve the line number information for
# debugging stack traces.
-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
-renamesourcefileattribute SourceFile

# AhnLab_MobileRootChecker
-keep public class com.ahnlab.enginesdk.ST {*;}

-keepclasseswithmembers class * {
    native <methods>;
}

# AppsFlyer
-dontwarn com.android.installreferrer
-dontwarn com.appsflyer.*

# ssenstone
## BEGIN GSON
# Gson uses generic type information stored in a class file when working with fields. Proguard
# removes such information by default, so configure it to keep all of it.
-keepattributes Signature

# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
-keep class com.google.gson.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.ssenstone.stonepass.libstonepass_sdk.op.** { *; }
-keep class com.ssenstone.stonepass.libstonepass_sdk.msg.** { *; }
-keepclassmembers enum * { *; }
## END GSON

-keep class org.bouncycastle.** { *; }
-dontwarn org.bouncycastle.**

-keep class org.spongycastle.** { *; }
-dontwarn org.spongycastle.**

-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**

-keep class com.ahnlab.enginesdk.** { *; }
-dontwarn com.ahnlab.enginesdk.**

-keep class com.atsolutions.tapagent.** { *; }
-dontwarn com.atsolutions.tapagent.**

-keep class com.interezen.mobile.** { *; }
-dontwarn com.interezen.mobile.**

-keep class com.squareup.picasso.** { *; }
-dontwarn com.squareup.picasso.**

-keep class com.webcash.sws.db.** { *; }
-dontwarn com.webcash.sws.db.**


# 20200609-FireBase
-keep class com.crashlytics.android.** { *; }
-keep class com.google.firebase.** { *; }

# falldownnumbereditor
-keep class com.mangosteen.falldownnumbereditor.** { *; }